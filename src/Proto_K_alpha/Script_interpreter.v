Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_K_alpha.Script_typed_ir.
Require TezosOfOCaml.Proto_K_alpha.Script_repr.
Require TezosOfOCaml.Proto_K_alpha.Michelson_v1_primitives.

Module OldScR := TezosOfOCaml.Proto_K.Script_repr.
Module NewScR := TezosOfOCaml.Proto_alpha.Script_repr.

(** Migrate some errors defined in this file. *)
Definition migrate_error (err : _error) : _error :=
  match err with
  | Build_extensible tag _ payload =>
    if String.eqb tag "Overflow" then
      let '(t1, t2):=
        cast (OldScR.location *
              M* Proto_K.Script_typed_ir.execution_trace) payload in
      Build_extensible "Overflow" _
        (cast NewScR.location t1,
         let* ' t2' := t2 in
         return* Script_typed_ir.migrate_execution_trace t2')
    else if String.eqb tag "Reject" then
      let '(t1, t2, t3):=
        cast (OldScR.location *
              V6.Micheline.canonical Proto_K.Alpha_context.Script.prim *
              M* Proto_K.Script_typed_ir.execution_trace) payload in
      Build_extensible "Reject" _
        (t1,
         Script_repr.map_node
           Michelson_v1_primitives.migrate_prim t2,
         let* ' t3' := t3 in
         return* Script_typed_ir.migrate_execution_trace t3')
    else
      err
  end.
