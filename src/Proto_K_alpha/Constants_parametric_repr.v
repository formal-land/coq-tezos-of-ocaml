Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_K.Constants_repr.
Require TezosOfOCaml.Proto_K.Ratio_repr.
Require TezosOfOCaml.Proto_alpha.Constants_repr.
Require TezosOfOCaml.Proto_alpha.Constants_parametric_repr.
Require TezosOfOCaml.Proto_alpha.Ratio_repr.

Require TezosOfOCaml.Proto_K_alpha.Tez_repr.
Require TezosOfOCaml.Proto_K_alpha.Ratio_repr.

Module Old := TezosOfOCaml.Proto_K.Constants_parametric_repr.
Module New := TezosOfOCaml.Proto_alpha.Constants_parametric_repr.

(** Migrate [Constants_parametric_repr.dal] *)
Module dal.
  Definition migrate (p : Old.dal) : New.dal :=
    {|
      New.dal.feature_enable := p.(Old.dal.feature_enable);
      New.dal.number_of_slots := p.(Old.dal.number_of_slots);
      New.dal.number_of_shards := p.(Old.dal.number_of_shards);
      New.dal.endorsement_lag := p.(Old.dal.endorsement_lag);
      New.dal.availability_threshold := p.(Old.dal.availability_threshold);
      New.dal.slot_size := 65536;
      (** Field was added in Proto_alpha. Used in tests (tezos),
          as expected value. Here : https://gitlab.com/tezos/tezos/-/blob/
          master/tezt/tests/expected/sc_rollup.ml/
          Alpha-%20wasm_2_0_0%20-%20rollup%20node%20-
          %20correct%20handling%20of%20commitments%20(no_commitment.out
      *)
      New.dal.redundancy_factor := 4;
      (** Field was added in Proto_alpha. Used in tests (tezos),
          as expected value. Here : https://gitlab.com/tezos/tezos/-/blob/
          master/tezt/tests/expected/sc_rollup.ml/
          Alpha-%20wasm_2_0_0%20-%20rollup%20node%20-
          %20correct%20handling%20of%20commitments%20(no_commitment.out
      *)
      New.dal.segment_size := 4096 (* no place in Tezos Code, where it used. *)
    |}.
End dal.

(** Migrate [Constants_parametric_repr.tx_rollup] *)
Module tx_rollup.
  Definition migrate (p : Old.tx_rollup) : New.tx_rollup :=
    {|
      New.tx_rollup.enable := p.(Old.tx_rollup.enable);
      New.tx_rollup.origination_size := p.(Old.tx_rollup.origination_size);
      New.tx_rollup.hard_size_limit_per_inbox :=
        p.(Old.tx_rollup.hard_size_limit_per_inbox);
      New.tx_rollup.hard_size_limit_per_message :=
        p.(Old.tx_rollup.hard_size_limit_per_message);
      New.tx_rollup.commitment_bond :=
        Tez_repr.migrate p.(Old.tx_rollup.commitment_bond);
      New.tx_rollup.finality_period := p.(Old.tx_rollup.finality_period);
      New.tx_rollup.withdraw_period := p.(Old.tx_rollup.withdraw_period);
      New.tx_rollup.max_inboxes_count := p.(Old.tx_rollup.max_inboxes_count);
      New.tx_rollup.max_messages_per_inbox :=
        p.(Old.tx_rollup.max_messages_per_inbox);
      New.tx_rollup.max_commitments_count :=
        p.(Old.tx_rollup.max_commitments_count);
      New.tx_rollup.cost_per_byte_ema_factor :=
        p.(Old.tx_rollup.cost_per_byte_ema_factor);
      New.tx_rollup.max_ticket_payload_size :=
        p.(Old.tx_rollup.max_ticket_payload_size);
      New.tx_rollup.max_withdrawals_per_batch :=
        p.(Old.tx_rollup.max_withdrawals_per_batch);
      New.tx_rollup.rejection_max_proof_size :=
        p.(Old.tx_rollup.rejection_max_proof_size);
      New.tx_rollup.sunset_level := p.(Old.tx_rollup.sunset_level);
    |}.
End tx_rollup.

(** Migrate [Constants_parametric_repr.sc_rollup] *)
Module sc_rollup.
  Definition migrate (p : Old.sc_rollup) : New.sc_rollup :=
    {|
      New.sc_rollup.enable := p.(Old.sc_rollup.enable);
      New.sc_rollup.origination_size := p.(Old.sc_rollup.origination_size);
      New.sc_rollup.challenge_window_in_blocks :=
        p.(Old.sc_rollup.challenge_window_in_blocks);
      New.sc_rollup.max_number_of_messages_per_commitment_period :=
        axiom "TODO";
      New.sc_rollup.stake_amount :=
        Tez_repr.migrate p.(Old.sc_rollup.stake_amount);
      New.sc_rollup.commitment_period_in_blocks := axiom "TODO"; (* TODO *)
      New.sc_rollup.max_lookahead_in_blocks := axiom "TODO"; (* TODO *)
      New.sc_rollup.max_active_outbox_levels := axiom "TODO"; (* TODO *)
      New.sc_rollup.max_outbox_messages_per_level := axiom "TODO"; (* TODO *)
      New.sc_rollup.number_of_sections_in_dissection := axiom "TODO"; (* TODO *)
      New.sc_rollup.timeout_period_in_blocks := axiom "TODO"; (* TODO *)
      New.sc_rollup.max_number_of_stored_cemented_commitments :=
        axiom "TODO" (* TODO *)
    |}.
End sc_rollup.  

(** Migrate [Constants_parametric_repr.t] *)
Definition migrate (p : Old.t) : New.t :=
 {|
  New.t.preserved_cycles := p.(Old.t.preserved_cycles);
  New.t.blocks_per_cycle := p.(Old.t.blocks_per_cycle);
  New.t.blocks_per_commitment := p.(Old.t.blocks_per_commitment);
  New.t.nonce_revelation_threshold :=
    axiom "nonce_revelation_threshold"; (* TODO *)
  New.t.blocks_per_stake_snapshot := p.(Old.t.blocks_per_stake_snapshot);
  New.t.cycles_per_voting_period := p.(Old.t.cycles_per_voting_period);
  New.t.hard_gas_limit_per_operation := p.(Old.t.hard_gas_limit_per_operation);
  New.t.hard_gas_limit_per_block := p.(Old.t.hard_gas_limit_per_block);
  New.t.proof_of_work_threshold := p.(Old.t.proof_of_work_threshold);
  New.t.minimal_stake := axiom "minimal_stake";
  New.t.vdf_difficulty := axiom "vdf_difficulty"; (* TODO *)
  New.t.seed_nonce_revelation_tip :=
     Tez_repr.migrate p.(Old.t.seed_nonce_revelation_tip);
  New.t.origination_size := p.(Old.t.origination_size);
  New.t.baking_reward_fixed_portion :=
     Tez_repr.migrate p.(Old.t.baking_reward_fixed_portion);
  New.t.baking_reward_bonus_per_slot :=
     Tez_repr.migrate p.(Old.t.baking_reward_bonus_per_slot);
  New.t.endorsing_reward_per_slot :=
     Tez_repr.migrate p.(Old.t.endorsing_reward_per_slot);
  New.t.cost_per_byte := Tez_repr.migrate p.(Old.t.cost_per_byte);
  New.t.hard_storage_limit_per_operation :=
    p.(Old.t.hard_storage_limit_per_operation);
  New.t.quorum_min := p.(Old.t.quorum_min);
  New.t.quorum_max := p.(Old.t.quorum_max);
  New.t.min_proposal_quorum := p.(Old.t.min_proposal_quorum);
  New.t.liquidity_baking_subsidy :=
     Tez_repr.migrate p.(Old.t.liquidity_baking_subsidy);
  New.t.liquidity_baking_sunset_level :=
    p.(Old.t.liquidity_baking_sunset_level);
  New.t.liquidity_baking_toggle_ema_threshold :=
    p.(Old.t.liquidity_baking_toggle_ema_threshold);
  New.t.max_operations_time_to_live := p.(Old.t.max_operations_time_to_live);
  New.t.minimal_block_delay := p.(Old.t.minimal_block_delay);
  New.t.delay_increment_per_round := p.(Old.t.delay_increment_per_round);
  New.t.minimal_participation_ratio :=
     Ratio_repr.migrate p.(Old.t.minimal_participation_ratio);
  New.t.consensus_committee_size := p.(Old.t.consensus_committee_size);
  New.t.consensus_threshold := p.(Old.t.consensus_threshold);
  New.t.max_slashing_period := p.(Old.t.max_slashing_period);
  New.t.frozen_deposits_percentage := p.(Old.t.frozen_deposits_percentage);
  New.t.double_baking_punishment := Tez_repr.migrate p
                                     .(Old.t.double_baking_punishment);
  New.t.ratio_of_frozen_deposits_slashed_per_double_endorsement :=
     Ratio_repr.migrate
       p.(Old.t.ratio_of_frozen_deposits_slashed_per_double_endorsement);
  New.t.testnet_dictator := axiom "testnet_dictator" (* TODO *);
  New.t.initial_seed := p.(Old.t.initial_seed);
  New.t.cache_script_size := p.(Old.t.cache_script_size);
  New.t.cache_stake_distribution_cycles :=
    p.(Old.t.cache_stake_distribution_cycles);
  New.t.cache_sampler_state_cycles := p.(Old.t.cache_sampler_state_cycles);
  New.t.tx_rollup := axiom "TODO";
  New.t.dal := axiom "TODO";
  New.t.sc_rollup := axiom "TODO";
  New.t.zk_rollup := axiom "TODO"
 |}.
