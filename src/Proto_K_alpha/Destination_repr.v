Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_K.Destination_repr.
Require TezosOfOCaml.Proto_alpha.Destination_repr.
Require TezosOfOCaml.Proto_K_alpha.Contract_repr.

Module Old := TezosOfOCaml.Proto_K.Destination_repr.
Module New := TezosOfOCaml.Proto_alpha.Destination_repr.

(** Migrate [Destination_repr.t]. *)
Definition migrate (x : Old.t) : New.t :=
  match x with
  | Old.Contract x => New.Contract (Contract_repr.migrate x)
  | Old.Tx_rollup x => New.Tx_rollup x
  | Old.Sc_rollup x => New.Sc_rollup x
  end.
