Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_K.Constants_repr.
Require TezosOfOCaml.Proto_K.Ratio_repr.
Require TezosOfOCaml.Proto_alpha.Constants_repr.
Require TezosOfOCaml.Proto_alpha.Constants_parametric_repr.
Require TezosOfOCaml.Proto_alpha.Ratio_repr.

Require TezosOfOCaml.Proto_K_alpha.Tez_repr.
Require TezosOfOCaml.Proto_K_alpha.Constants_parametric_repr.

Module Old := TezosOfOCaml.Proto_K.Constants_repr.
Module New := TezosOfOCaml.Proto_alpha.Constants_repr.

(** Migrate [Constants_repr.t] *)
Module t.
  Definition migrate (p : Old.t) : New.t :=
    {|
     New.t.fixed := p.(Old.t.fixed);
     New.t.parametric := Constants_parametric_repr.migrate p.(Old.t.parametric)
    |}.
End t.
