Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_K.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Saturation_repr.

(** [Proto_K.Saturation_repr.safe_int] is backward compatible *)
Lemma safe_int_is_backward_compatible : 
  Proto_K.Saturation_repr.safe_int =
    Proto_alpha.Saturation_repr.safe_int.
Proof.
  apply FunctionalExtensionality.functional_extensionality; intros.
  unfold 
    Saturation_repr.safe_int, Proto_K.Saturation_repr.safe_int,
    Saturation_repr.saturate_if_undef, Saturation_repr.of_int_opt,
    Proto_K.Saturation_repr.saturate_if_undef,
    Proto_K.Saturation_repr.of_int_opt,
    Saturation_repr.saturated, Proto_K.Saturation_repr.saturated,
    Pervasives.max_int, V5.Pervasives.max_int; cbn.
  now step. 
Qed.

(** [add] is backward compatible *)
Lemma add_is_backward_compatible :
  Proto_K.Saturation_repr.add = Proto_alpha.Saturation_repr.add.
Proof.
  trivial.
Qed.
