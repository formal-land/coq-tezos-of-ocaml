Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_K_alpha.Local_gas_counter.
Require TezosOfOCaml.Proto_K_alpha.Proofs.Local_gas_counter.
Require TezosOfOCaml.Proto_K_alpha.Proofs.Raw_context.
Require TezosOfOCaml.Proto_K_alpha.Proofs.Storage_functors.

(** The [get_voting_power] function is backward compatible. *)
Lemma get_voting_power_is_backward_compatible l ctxt s
  : Error.migrate_monad
      (Proto_K.Vote_storage.get_voting_power
         (Proto_K.Local_gas_counter.update_context l ctxt) s)
      (fun '(ctxt, v) => (Raw_context.migrate ctxt, v))
    = Vote_storage.get_voting_power
        (Local_gas_counter.update_context
           (Local_gas_counter.migrate_local_gas_counter l)
           (Local_gas_counter.migrate_outdated_context ctxt)) s.
Proof.
  unfold
    Proto_K.Vote_storage.get_voting_power,
    Vote_storage.get_voting_power.
  rewrite
    <- Local_gas_counter.update_context_is_backward_compatible,
    <- Raw_context.consume_gas_is_backward_compatible.
  destruct Proto_K.Raw_context.consume_gas; [|reflexivity]; simpl.
  match goal with
  | |- context [
          Proto_K
          .Storage_functors
          .Make_indexed_data_storage.find (H:=?HArgs0) ]
    =>
      rewrite <-
        (Storage_functors
         .Make_indexed_data_storage
         .find_is_backward_compatible (HFArgs0:=HArgs0))
  end.
  destruct Proto_K.Storage_functors.Make_indexed_data_storage.find;
    [|reflexivity].
  simpl; step; reflexivity.
Qed.

(** The [get_total_voting_power_free] function is backward compatible. *)
Lemma get_total_voting_power_free_is_backward_compatible ctxt
  : Error.migrate_monad
      (Proto_K.Vote_storage.get_total_voting_power_free ctxt) id
    = Vote_storage.get_total_voting_power_free (Raw_context.migrate ctxt).
Proof.
  unfold
    Vote_storage.get_total_voting_power_free,
    Proto_K.Vote_storage.get_total_voting_power_free.
  simpl.
  match goal with
  | |- context [
          Proto_K
          .Storage_functors
          .Make_single_data_storage.get (H:=?HArgs0) ]
    =>
      rewrite <-
        (Storage_functors
         .Make_single_data_storage
         .get_is_backward_compatible (HFArgs0:=HArgs0))
  end.
  reflexivity.
Qed.

(** The [get_total_voting_power] function is backward compatible. *)
Lemma get_total_voting_power_is_backward_compatible l ctxt
  : Error.migrate_monad
      (Proto_K.Vote_storage.get_total_voting_power
         (Proto_K.Local_gas_counter.update_context l ctxt))
      (fun '(ctxt, v) => (Raw_context.migrate ctxt, v))
    = Vote_storage.get_total_voting_power
        (Local_gas_counter.update_context
           (Local_gas_counter.migrate_local_gas_counter l)
           (Local_gas_counter.migrate_outdated_context ctxt)).
Proof.
  unfold
    Proto_K.Vote_storage.get_total_voting_power,
    Vote_storage.get_total_voting_power.
  rewrite
    <- Local_gas_counter.update_context_is_backward_compatible,
    <- Raw_context.consume_gas_is_backward_compatible.
  destruct Proto_K.Raw_context.consume_gas; [|reflexivity].
  simpl.
  rewrite <- get_total_voting_power_free_is_backward_compatible.
  step; reflexivity.
Qed.
