Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Environment.V5.Proofs.Data_encoding.

Require TezosOfOCaml.Proto_K.Script_ir_translator.
Require TezosOfOCaml.Proto_alpha.Script_ir_translator.

Require TezosOfOCaml.Proto_K.Simulations.Script_ir_translator.
Require TezosOfOCaml.Proto_alpha.Simulations.Script_ir_translator.
Require TezosOfOCaml.Proto_alpha.Simulations.Script_big_map.

Require TezosOfOCaml.Environment.V7.Proofs.Map.
Require Import TezosOfOCaml.Proto_K_alpha.Raw_context.
Require Import TezosOfOCaml.Proto_K_alpha.Script_ir_translator.
Require Import TezosOfOCaml.Proto_K_alpha.Simulations.Script_ir_translator.
Require Import TezosOfOCaml.Proto_K_alpha.Simulations.Script_typed_ir.
Require TezosOfOCaml.Proto_K_alpha.Error.
Require TezosOfOCaml.Proto_K_alpha.Lazy_storage_diff.
Require TezosOfOCaml.Proto_K_alpha.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_K_alpha.Script_repr.
Require TezosOfOCaml.Proto_K_alpha.Raw_context.
Require TezosOfOCaml.Proto_K_alpha.Local_gas_counter.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_family.

Require Import TezosOfOCaml.Proto_K_alpha.Proofs.Raw_context.
Require Import TezosOfOCaml.Proto_K_alpha.Proofs.Script_repr.

Import Option.Notations.

Module Old := TezosOfOCaml.Proto_K.Script_ir_translator.
Module New := TezosOfOCaml.Proto_alpha.Script_ir_translator.

Module OldS := TezosOfOCaml.Proto_K.Simulations.Script_ir_translator.
Module NewS := TezosOfOCaml.Proto_alpha.Simulations.Script_ir_translator.

(** [pack_node] is backward compatible. *)
Lemma pack_node_is_backward_compatible
  (node : Proto_K.Alpha_context.Script.node)
  (ctxt : Proto_K.Alpha_context.context)
  : Error.migrate_monad
      (Old.pack_node node ctxt)
      (fun '(bs, ctxt0) => (bs, migrate ctxt0)) =
      New.pack_node
        (Script_repr.migrate_node node) (migrate ctxt).
Proof.
  unfold
    Proto_K.Script_ir_translator.pack_node,
    Script_ir_translator.pack_node,
    Script_repr.migrate_node; simpl.
  rewrite
    <- Raw_context.consume_gas_is_backward_compatible,
    Script_repr.strip_locations_cost_is_backward_compatible.
  change
    (Old.consume_gas ctxt
       (Alpha_context.Script.strip_locations_cost
          (Script_repr.map_node Michelson_v1_primitives.migrate_prim node)))
    with
    (Proto_K.Alpha_context.Gas.consume
       ctxt (New.strip_locations_cost
               (Script_repr.map_node
                  Michelson_v1_primitives.migrate_prim node))).
  step; [|reflexivity]; simpl.
  rewrite
    <- Raw_context.consume_gas_is_backward_compatible,
    Script_repr.serialized_cost_is_backward_compatible,
    Script_repr.expr_encoding_migrate_prim_is_backward_compatible.
  change
    (Old.consume_gas t
       (Alpha_context.Script.serialized_cost
          (Binary.to_bytes_exn
             None Alpha_context.Script.expr_encoding
             (strip_locations
                (Script_repr.map_node
                   Michelson_v1_primitives.migrate_prim node)))))
    with
    (Proto_K.Alpha_context.Gas.consume t
       (New.serialized_cost
          (Binary.to_bytes_exn
             None Alpha_context.Script.expr_encoding
             (strip_locations
                (Script_repr.map_node
                   Michelson_v1_primitives.migrate_prim node))))).
  step; reflexivity.
Qed.

(** [unparse_data_aux] with mode set to Optimized
    and A set to Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a
    is backward compatible *)
Lemma unparse_data_aux_special_case_is_backward_compatible
  {a : Proto_K.Simulations.Script_family.Ty.t}
  (ctxt : Proto_K.Alpha_context.context) (stack_depth : int)
  (ty : Proto_K.Script_typed_ir.ty)
  (x : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a) :
  Error.migrate_monad
    (Old.unparse_data_aux ctxt stack_depth Old.Optimized ty x)
    (fun '(sn, ctxt) =>
       (Script_repr.migrate_node sn, Raw_context.migrate ctxt)) =
  Script_ir_translator.unparse_data_aux (migrate ctxt) 0
    Script_ir_translator.Optimized (Script_typed_ir.migrate_ty ty)
    (Script_family.Ty.migrate_to_Set (Old.With_family.to_value x)).
Proof.
  destruct ctxt. cbn.
  unfold Proto_K.Alpha_context.Gas.consume, Alpha_context.Gas.consume.
  unfold Proto_K.Raw_context.consume_gas, Raw_context.consume_gas.
  unfold Proto_K.Gas_limit_repr.raw_consume, Gas_limit_repr.raw_consume.
  unfold Proto_K.Gas_limit_repr.Arith.sub_opt,
         Gas_limit_repr.Arith.sub_opt.
  unfold Proto_K.Gas_limit_repr.S.sub_opt,
         Gas_limit_repr.S.sub_opt.
  unfold Proto_K.Gas_limit_repr.cost_to_milligas,
         Gas_limit_repr.cost_to_milligas.
  cbn.
  (* TODO *)
Admitted.

(** [collect_lazy_storage] is backward compatible
    for A :=
      Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a *)
Lemma collect_lazy_storage_is_backward_compatible_for_ty_to_dep_Set
  {a : Proto_K.Simulations.Script_family.Ty.t}
  (ctxt : Proto_K.Alpha_context.context)
  (ty : Proto_K.Script_typed_ir.ty)
  (t : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a) :
  Error.migrate_monad
  (Old.collect_lazy_storage ctxt ty t)
  (fun '(is, c) =>
     (Lazy_storage_kind.IdSet.migrate is, Raw_context.migrate c)) =
  New.collect_lazy_storage (Raw_context.migrate ctxt)
    (Script_typed_ir.migrate_ty ty)
    (Script_typed_ir.With_family.to_value
      (Script_typed_ir.With_family.migrate_value t)).
Proof.
  (* TODO: define Script_typed_ir.migrate_ty *)
Admitted.

(** [extract_lazy_storage_diff] is backward compatible for A :=
    Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
    a, mode := Proto_K.Script_ir_translator.Optimized, temporary
    := true *)
Lemma extract_lazy_storage_diff_is_backward_compatible_special_case
  {a : Proto_K.Simulations.Script_family.Ty.t}
  (ctxt : Proto_K.Raw_context.t)
  (t : Proto_K.Simulations.Script_typed_ir.With_family.ty a)
  (x :
    Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
      a)
  (idset : Proto_K.Alpha_context.Lazy_storage.IdSet.t) :
  let migrate '(x, opt_diff, ctxt) :=
    (Script_family.Ty.migrate_to_Set
      (Proto_K.Simulations.Script_typed_ir.With_family.to_value
        x),
    Option.map Lazy_storage_diff.migrate_diffs opt_diff,
    Raw_context.migrate ctxt) in
  Error.migrate_monad
    (Old.extract_lazy_storage_diff ctxt
      Old.Optimized true idset
      Old.no_lazy_storage_id
      (Proto_K.Simulations.Script_typed_ir.With_family.to_ty t)
        x)
    migrate =
  New.extract_lazy_storage_diff
    (Raw_context.migrate ctxt)
    Script_ir_translator.Optimized true
    (Lazy_storage_kind.IdSet.migrate idset)
    (Lazy_storage_kind.IdSet.migrate
       Old.no_lazy_storage_id)
    (Script_typed_ir.migrate_ty
       (Script_typed_ir.Old.With_family.to_ty t))
    (Script_typed_ir.With_family.to_value
       (Script_typed_ir.With_family.migrate_value x)).
Proof.
Admitted.

(** [unparse_data] with [mode] set to [Optimized] and [A] set to
    [Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a]
    is backward compatible *)
Lemma unparse_data_special_case_is_backward_compatible
  {a} (ctxt : Proto_K.Alpha_context.context)
  (ty : Proto_K.Script_typed_ir.ty)
  (x : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a) :
  Error.migrate_monad (Old.unparse_data ctxt Old.Optimized ty x)
    (fun '(sn, ctxt) => (Script_repr.migrate_node sn, Raw_context.migrate ctxt)) =
  Script_ir_translator.unparse_data
    (Raw_context.migrate ctxt) Script_ir_translator.Optimized
    (Script_typed_ir.migrate_ty ty)
    (Script_family.Ty.migrate_to_Set
       (Proto_K.Simulations.Script_typed_ir.With_family.to_value x)).
Proof.
  unfold Old.unparse_data, New.unparse_data.
  apply unparse_data_aux_special_case_is_backward_compatible.
Qed.

(** [dep_comb_witness2] is backward compatible *)
Lemma dep_comb_witness2_is_backward_compatible
  (a : Proto_K.Simulations.Script_family.Ty.t)
  (fp : Proto_K.Simulations.Script_typed_ir.With_family.ty a) :
    migrate_comb_witness (OldS.dep_comb_witness2 fp) =
      NewS.dep_comb_witness2 (With_family.migrate_ty fp).
Proof.
  destruct fp; simpl; trivial.
  destruct fp2; simpl; trivial.
Qed.

(** [dep_pack_comparable_data] is backward compatible. *)
Lemma dep_pack_comparable_data_is_backward_compatible {a b}
  ctxt
  (l : Proto_K.Local_gas_counter.local_gas_counter)
  (s : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a)
  (t : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
      (Proto_K.Simulations.Script_family.Ty.Big_map a b)) :
  Error.migrate_monad
    (Proto_K.Simulations.Script_ir_translator.dep_pack_comparable_data
       (Proto_K.Local_gas_counter.update_context l ctxt)
       t.(Proto_K.Simulations.Script_typed_ir.With_family.big_map.key_type)
           s Proto_K.Script_ir_translator.Optimized_legacy)
    (fun '(b, c) => (b, Raw_context.migrate c))
  =
  Script_ir_translator.dep_pack_comparable_data
    (Local_gas_counter.update_context
       (Local_gas_counter.migrate_local_gas_counter l)
       (Local_gas_counter.migrate_outdated_context ctxt))
    (With_family.migrate_big_map_skeleton
       With_family.migrate_value With_family.migrate_value t)
    .(Script_typed_ir.With_family.big_map.key_type)
       (With_family.migrate_value s).
Proof.
Admitted.

(** [dep_hash_comparable_data] is backward compatible. *)
Lemma dep_hash_comparable_data_is_backward_compatible {a b}
  ctxt
  (l : Proto_K.Local_gas_counter.local_gas_counter)
  (s : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a)
  (t : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
      (Proto_K.Simulations.Script_family.Ty.Big_map a b)) :
  Error.migrate_monad
    (Proto_K.Simulations.Script_ir_translator.dep_hash_comparable_data
       (Proto_K.Local_gas_counter.update_context l ctxt)
       t.(Proto_K.Simulations.Script_typed_ir.With_family.big_map.key_type)
       s)
    (fun '(hash, c) => (hash, Raw_context.migrate c))
  =
  Script_ir_translator.dep_hash_comparable_data
    (Local_gas_counter.update_context
       (Local_gas_counter.migrate_local_gas_counter l)
       (Local_gas_counter.migrate_outdated_context ctxt))
    (With_family.migrate_big_map_skeleton
       With_family.migrate_value With_family.migrate_value t)
    .(Script_typed_ir.With_family.big_map.key_type)
       (With_family.migrate_value s).
Proof.
Admitted.

(** [dep_parse_packable_ty_aux] is backward compatible *)
Lemma dep_parse_packable_ty_aux_is_backward_compatible
  ctxt (stack_depth : int) (legacy : bool)
  (node_value : Proto_K.Alpha_context.Script.node) :
  Error.migrate_monad
    (OldS.dep_parse_packable_ty_aux ctxt stack_depth legacy node_value)
    (fun '(t, ctxt) => (migrate_dep_ex_ty t, migrate ctxt)) =
  NewS.dep_parse_packable_ty_aux (migrate ctxt) stack_depth legacy
    (Script_repr.map_node Michelson_v1_primitives.migrate_prim node_value).
Proof.
  (* TODO *)
Admitted.

(** [dep_parse_contract_for_script] is backward compatible *)
Lemma dep_parse_contract_for_script_is_backward_compatible
  {a : Proto_K.Simulations.Script_family.Ty.t}
  (ctxt : Proto_K.Alpha_context.context)
  (loc_value : Proto_K.Alpha_context.Script.location)
  (arg : Proto_K.Simulations.Script_typed_ir.With_family.ty a)
  (contract : Proto_K.Alpha_context.Destination.t)
  (entrypoint : Proto_K.Alpha_context.Entrypoint.t) :
  Error.migrate_monad
    (OldS.dep_parse_contract_for_script ctxt loc_value arg contract entrypoint)
    (fun '(ctxt', oContract) =>
      (Raw_context.migrate ctxt',
       let* 'contract' := oContract in
       return* Script_typed_ir.With_family.migrate_typed_contract contract')
    ) =
  NewS.dep_parse_contract_for_script (Raw_context.migrate ctxt) loc_value
    (Script_typed_ir.With_family.migrate_ty arg)
    (Destination_repr.migrate contract) entrypoint.
Proof.
  (* TODO *)
Admitted.

(** [dep_unparse_data_aux_fuel]
    with mode set to Optimized is backward compatible *)
Lemma dep_unparse_data_aux_fuel_optimized_is_backward_compatible :
  forall (a : Proto_K.Simulations.Script_family.Ty.t)
    (fuel : nat)
    (ctxt : Proto_K.Alpha_context.context)
    (ty : Proto_K.Simulations.Script_typed_ir.With_family.ty a)
    (a_value
      : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a),
  Error.migrate_monad
    (OldS.dep_unparse_data_aux_fuel fuel ctxt Old.Optimized ty a_value)
    (fun '(sn, ctxt) => (Script_repr.migrate_node sn, Raw_context.migrate ctxt))
  = NewS.dep_unparse_data_aux_fuel fuel (Raw_context.migrate ctxt)
      New.Optimized (With_family.migrate_ty ty)
      (With_family.migrate_value (t := a) a_value).
Proof.
  fix IHfuel 2. destruct fuel; simpl; intros.
  { cbn.
    unfold Proto_K.Alpha_context.Gas.consume, Alpha_context.Gas.consume.
    rewrite <- consume_gas_is_backward_compatible.
    unfold Error_monad.op_gtgtquestion.
    destruct Old.consume_gas; simpl; trivial.
  }
  { cbn. rewrite <- consume_gas_is_backward_compatible. destruct ctxt.
    unfold Proto_K.Alpha_context.Gas.consume, Old.consume_gas. cbn.
    destruct Proto_K.Gas_limit_repr.raw_consume eqn:RC; cbn.
    { destruct ty; cbn; trivial; try now destruct a_value.
      1,2,3,5: (
        unfold Proto_K.Raw_context.update_remaining_operation_gas,
               Proto_K.Raw_context.t.with_remaining_operation_gas;
        cbn; rewrite <- consume_gas_is_backward_compatible;
        unfold Old.consume_gas; cbn;
        unfold Old.update_remaining_operation_gas,
               Old.t.with_remaining_operation_gas;
        cbn; destruct (Proto_K.Gas_limit_repr.raw_consume t _); cbn;
        [ now destruct a_value
        | unfold Old.unlimited_operation_gas; simpl;
          destruct back.(Old.back.unlimited_operation_gas); cbn;
          now destruct a_value
        ]
      ).
      { unfold Proto_K.Raw_context.update_remaining_operation_gas,
               Proto_K.Raw_context.t.with_remaining_operation_gas.
        cbn. rewrite <- consume_gas_is_backward_compatible.
        unfold Old.consume_gas. cbn.
        unfold Old.update_remaining_operation_gas,
               Old.t.with_remaining_operation_gas.
        cbn. destruct (Proto_K.Gas_limit_repr.raw_consume t _); cbn.
        { apply axiom. (* TODO: Destination_repr.migrate *) }
        { unfold Old.unlimited_operation_gas. simpl.
          destruct back.(Old.back.unlimited_operation_gas); cbn;
          destruct a_value; cbn; trivial.
          apply axiom. (* TODO: Destination_repr.migrate *)
        }
      }
      { unfold Proto_K.Raw_context.update_remaining_operation_gas,
               Proto_K.Raw_context.t.with_remaining_operation_gas.
        cbn.
        unfold Proto_K.Script_ir_translator.unparse_pair,
               Script_ir_translator.unparse_pair.
        cbn. destruct a_value. cbn. rewrite <- IHfuel.
        destruct OldS.dep_unparse_data_aux_fuel; cbn; trivial.
        destruct p. cbn. rewrite <- IHfuel.
        destruct OldS.dep_unparse_data_aux_fuel; cbn; trivial.
        destruct p. cbn. rewrite <- dep_comb_witness2_is_backward_compatible.
        destruct OldS.dep_comb_witness2; cbn; trivial.
        destruct c1; cbn.
        { destruct n0; cbn; trivial.
          destruct p; cbn; trivial.
          destruct l0; cbn; trivial.
          destruct l0; cbn; trivial.
          destruct n1; cbn; trivial.
          destruct p; cbn; trivial.
          destruct l2; cbn; trivial.
          destruct l2; cbn; trivial.
          destruct l2; cbn; trivial.
          destruct a0; cbn; trivial.
          destruct l0; cbn; trivial.
          destruct a; cbn; trivial.
        }
        { destruct n0; cbn; trivial. }
      }
      { unfold Proto_K.Raw_context.update_remaining_operation_gas,
               Proto_K.Raw_context.t.with_remaining_operation_gas.
        cbn.
        unfold Proto_K.Script_ir_translator.unparse_union,
               Script_ir_translator.unparse_union.
        cbn. destruct a_value; cbn; rewrite <- IHfuel;
        destruct OldS.dep_unparse_data_aux_fuel; cbn; trivial;
        destruct p; cbn; f_equal.
      }
      { unfold Proto_K.Raw_context.update_remaining_operation_gas,
               Proto_K.Raw_context.t.with_remaining_operation_gas.
        cbn. destruct a_value. cbn.
        clear remaining_operation_gas arg ret ty1 ty2 t0 s RC.
        revert fuel back n t.
        fix IHfuel_code 1. destruct n, fuel; intros; cbn;
        unfold Proto_K.Alpha_context.Gas.consume,
               Alpha_context.Gas.consume,
               Proto_K.Raw_context.consume_gas,
               Raw_context.consume_gas,
               Proto_K.Raw_context.remaining_operation_gas,
               Raw_context.remaining_operation_gas,
               Proto_K.Gas_limit_repr.raw_consume,
               Gas_limit_repr.raw_consume,
               Proto_K.Gas_limit_repr.Arith.sub_opt,
               Gas_limit_repr.Arith.sub_opt,
               Proto_K.Gas_limit_repr.S.sub_opt,
               Gas_limit_repr.S.sub_opt,
               Proto_K.Gas_limit_repr.cost_to_milligas,
               Gas_limit_repr.cost_to_milligas; cbn;
        destruct (_ >=? _); cbn; trivial;
        try
        ( unfold Proto_K.Raw_context.unlimited_operation_gas; cbn;
          now
          destruct back.(Proto_K.Raw_context.back.unlimited_operation_gas)
        ).
        { destruct l0; cbn;
          [ destruct p; cbn; trivial
          | destruct l0; cbn;
            [ destruct p; cbn; trivial
            | destruct l0; cbn
            ]
          ].
          { destruct p; cbn; trivial.
            rewrite <- update_remaining_operation_gas_is_backward_compatible.
            rewrite <- dep_parse_packable_ty_aux_is_backward_compatible.
            unfold OldS.fuel_to_stack_depth, NewS.fuel_to_stack_depth.
            destruct OldS.dep_parse_packable_ty_aux; cbn; trivial.
            destruct p, d. cbn. trivial.
          }
          { clear back n n0 n1 t. revert l0 l p a. induction l0.
            { cbn. destruct p; cbn; trivial. }
            { cbn. trivial. }
          }
        }
        { destruct l0; cbn;
          unfold Proto_K.Raw_context.unlimited_operation_gas; cbn.
          { destruct
              back.(Proto_K.Raw_context.back.unlimited_operation_gas);
            cbn; trivial.
            destruct p; cbn; trivial.
          }
          apply axiom. (* TODO *) (* using axioms to check that Qed works at the end *)
        }
        all: apply axiom. (* TODO *) (* using axioms to check that Qed works at the end *)
      }
      all: apply (axiom "TODO"). (* TODO *) (* using axioms to check that Qed works at the end *)
    }
    apply (axiom "TODO"). (* TODO *) (* using axioms to check that Qed works at the end *)
  }
Qed.

(** [dep_unparse_data_aux] with mode set to Optimized is backward compatible *)
Lemma dep_unparse_data_aux_optimized_is_backward_compatible
  (a : Proto_K.Simulations.Script_family.Ty.t)
  (ctxt : Proto_K.Alpha_context.context)
  (stack_depth : int)
  (ty : Proto_K.Simulations.Script_typed_ir.With_family.ty a)
  (a_value
    : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a) :
  Error.migrate_monad
    (OldS.dep_unparse_data_aux ctxt stack_depth Old.Optimized ty a_value)
    (fun '(sn, ctxt) => (Script_repr.migrate_node sn, Raw_context.migrate ctxt))
  = NewS.dep_unparse_data_aux (Raw_context.migrate ctxt) stack_depth
      New.Optimized (With_family.migrate_ty ty)
      (With_family.migrate_value (t := a) a_value).
Proof.
  unfold OldS.dep_unparse_data_aux, NewS.dep_unparse_data_aux.
  apply dep_unparse_data_aux_fuel_optimized_is_backward_compatible.
Qed.

(** [dep_unparse_data_aux] is backward compatible *)
Lemma dep_unparse_data_aux_is_backward_compatible
  (ctxt : Proto_K.Alpha_context.context)
  (a : Proto_K.Simulations.Script_family.Ty.t)
  (ty_a : Proto_K.Simulations.Script_typed_ir.With_family.ty a)
  (dep_v_a :
    Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a)
  (mode : Proto_K.Script_ir_translator.unparsing_mode)
  : Error.migrate_monad
      (OldS.dep_unparse_data_aux ctxt 0 mode ty_a dep_v_a)
      (fun '(node, ctxt) =>
         (Script_repr.migrate_node node, Raw_context.migrate ctxt))
    =
      NewS.dep_unparse_data_aux (migrate ctxt) 0 (migrate_unparsing_mode mode)
        (With_family.migrate_ty ty_a) (With_family.migrate_value dep_v_a).
Proof.
Admitted.

(** [dep_pack_data_with_mode] is backward compatible *)
Lemma dep_pack_data_with_mode_is_backward_compatible
  (ctxt : Proto_K.Alpha_context.context)
  (a : Proto_K.Simulations.Script_family.Ty.t)
  (ty_a : Proto_K.Simulations.Script_typed_ir.With_family.ty a)
  (dep_v_a :
    Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a)
  (mode : Proto_K.Script_ir_translator.unparsing_mode)
  : Error.migrate_monad
      (OldS.dep_pack_data_with_mode ctxt ty_a dep_v_a mode)
      (fun '(bs, ctxt) => (bs, Raw_context.migrate ctxt)) =
    NewS.dep_pack_data_with_mode
      (Raw_context.migrate ctxt)
      (Script_typed_ir.With_family.migrate_ty ty_a)
      (Script_typed_ir.With_family.migrate_value dep_v_a)
      (Script_ir_translator.migrate_unparsing_mode mode).
Proof.
  unfold
    OldS.dep_pack_data_with_mode,
    NewS.dep_pack_data_with_mode.
  rewrite <- dep_unparse_data_aux_is_backward_compatible.
  step; [|reflexivity]; Tactics.destruct_pairs; simpl.
  apply pack_node_is_backward_compatible.
Qed.

(** [dep_pack_data_with_mode] is backward compatible *)
Lemma dep_pack_data_is_backward_compatible
  (ctxt : Proto_K.Alpha_context.context)
  (a : Proto_K.Simulations.Script_family.Ty.t)
  (ty_a : Proto_K.Simulations.Script_typed_ir.With_family.ty a)
  (dep_v_a :
    Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a)
  : Error.migrate_monad
      (OldS.dep_pack_data ctxt ty_a dep_v_a)
      (fun '(bs, ctxt) => (bs, Raw_context.migrate ctxt)) =
    NewS.dep_pack_data
      (Raw_context.migrate ctxt)
      (Script_typed_ir.With_family.migrate_ty ty_a)
      (Script_typed_ir.With_family.migrate_value dep_v_a).
Proof.
  unfold OldS.dep_pack_data, NewS.dep_pack_data;
  rewrite dep_pack_data_with_mode_is_backward_compatible;
  reflexivity.
Qed.

(** [Big_map.mem] is backward compatible. *)
Lemma Big_map_mem_is_backward_compatible c t t0 :
  Error.migrate_monad (Proto_K.Alpha_context.Big_map.mem c t t0)
    (fun '(x, b) => (migrate x, b)) =
  Alpha_context.Big_map.mem (migrate c) t t0.
Proof.
Admitted.

(** [dep_big_map_get_by_hash] is backward compatible. *)
Lemma dep_big_map_get_by_hash_is_backward_compatible {a b}
  c t0 (t : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
      (Proto_K.Simulations.Script_family.Ty.Big_map a b)) :
  Error.migrate_monad (OldS.dep_big_map_get_by_hash c t0 t)
    (fun v =>
      match v with
      | (Some x, y) => (Some (With_family.migrate_value x), migrate y)
      | (None, y) => (None, migrate y)
      end)
  =
  Simulations.Script_big_map.dep_big_map_get_by_hash (migrate c) t0
    (With_family.migrate_big_map_skeleton
      With_family.migrate_value
      With_family.migrate_value t).
Proof.
Admitted.

(** [dep_big_map_mem] is backward compatible. *)
Lemma dep_big_map_mem_is_backward_compatible {a b}
  ctxt
  (l : Proto_K.Local_gas_counter.local_gas_counter)
  (s : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a)
  (t : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
      (Proto_K.Simulations.Script_family.Ty.Big_map a b)) :
  Error.migrate_monad
    (Proto_K.Simulations.Script_ir_translator.dep_big_map_mem
    (Proto_K.Local_gas_counter.update_context l ctxt) s t)
    (fun '(b, c) => (b, Raw_context.migrate c))
  =
  Script_big_map.dep_big_map_mem
    (Local_gas_counter.update_context (Local_gas_counter.migrate_local_gas_counter l)
      (Local_gas_counter.migrate_outdated_context ctxt))
    (Script_typed_ir.With_family.migrate_value s)
    (Script_typed_ir.With_family.migrate_big_map_skeleton
       Script_typed_ir.With_family.migrate_value
       Script_typed_ir.With_family.migrate_value t).
Proof.
  unfold Proto_K.Simulations.Script_ir_translator.dep_big_map_mem.
  unfold Script_big_map.dep_big_map_mem.
  rewrite <- dep_hash_comparable_data_is_backward_compatible.
  destruct Proto_K.Simulations.Script_ir_translator.dep_hash_comparable_data;
    [|reflexivity].
  Tactics.destruct_pairs; simpl.
  unfold Script_typed_ir.Big_map_overlay;
  unfold Proto_K.Script_typed_ir.Big_map_overlay.
  with_strategy transparent [V5.Map.Make]
    unfold V5.Map.Make; simpl.
  erewrite <- (Map.map_list_eq_fargs
    (fun p => (With_family.migrate_value (fst p),
    match snd p with
    | Some b0 => Some (With_family.migrate_value b0)
    | None => None
    end))); try (now intros k []).
  rewrite <- Map.find_map_fargs.
  destruct Map.Make.find.
  { Tactics.destruct_pairs; simpl.
    now destruct o.
  }
  { destruct t; simpl.
    destruct id; [|reflexivity].
    unfold Error_monad.op_gtgtquestion.
    rewrite <- Big_map_mem_is_backward_compatible.
    destruct Proto_K.Alpha_context.Big_map.mem;
    Tactics.destruct_pairs; reflexivity.
  }
Qed.

(** [dep_big_map_get] is backward compatible. *)
Lemma dep_big_map_get_is_backward_compatible {a b}
  ctxt
  (l : Proto_K.Local_gas_counter.local_gas_counter)
  (s : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a)
  (t : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
      (Proto_K.Simulations.Script_family.Ty.Big_map a b)) :
  Error.migrate_monad
    (Proto_K.Simulations.Script_ir_translator.dep_big_map_get
       (Proto_K.Local_gas_counter.update_context l ctxt) s t)
    (fun '(o, c) =>
      match o with
      | Some v =>
          (Some (Script_typed_ir.With_family.migrate_value v),
            Raw_context.migrate c)
      | None => (None, Raw_context.migrate c)
      end)
  =
  Script_big_map.dep_big_map_get
    (Local_gas_counter.update_context (Local_gas_counter.migrate_local_gas_counter l)
      (Local_gas_counter.migrate_outdated_context ctxt))
    (Script_typed_ir.With_family.migrate_value s)
    (Script_typed_ir.With_family.migrate_big_map_skeleton
       Script_typed_ir.With_family.migrate_value
       Script_typed_ir.With_family.migrate_value t).
Proof.
  unfold Proto_K.Simulations.Script_ir_translator.dep_big_map_get.
  unfold Script_big_map.dep_big_map_get.
  rewrite <- dep_hash_comparable_data_is_backward_compatible.
  unfold V5.Error_monad.op_gtgtquestion.
  unfold Error.migrate_monad.
  destruct Proto_K.Simulations.Script_ir_translator.dep_hash_comparable_data;
    [|reflexivity].
  Tactics.destruct_pairs; simpl.
  rewrite <- dep_big_map_get_by_hash_is_backward_compatible.
  destruct OldS.dep_big_map_get_by_hash; [|reflexivity].
  Tactics.destruct_pairs; simpl.
  now destruct o.
Qed.

(** [dep_big_map_update] is backward compatible. *)
Lemma dep_big_map_update_is_backward_compatible {a b}
  ctxt
  (l : Proto_K.Local_gas_counter.local_gas_counter)
  (s : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a)
  (t : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
      (Proto_K.Simulations.Script_family.Ty.Option b))
  (t0 : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
       (Proto_K.Simulations.Script_family.Ty.Big_map a b)) :
  Error.migrate_monad
    (Proto_K.Simulations.Script_ir_translator.dep_big_map_update
      (Proto_K.Local_gas_counter.update_context l ctxt) s t t0)
    (fun '(m, c) =>
      (@Script_typed_ir.With_family.migrate_value
        (Proto_K.Simulations.Script_family.Ty.Big_map a b) m,
       Raw_context.migrate c))
  =
  Script_big_map.dep_big_map_update
    (Local_gas_counter.update_context
      (Local_gas_counter.migrate_local_gas_counter l)
      (Local_gas_counter.migrate_outdated_context ctxt))
    (Script_typed_ir.With_family.migrate_value s)
    (match t with
     | Some x =>
         Some (Script_typed_ir.With_family.migrate_value x)
     | None => None
     end)
    (Script_typed_ir.With_family.migrate_big_map_skeleton
      Script_typed_ir.With_family.migrate_value
      Script_typed_ir.With_family.migrate_value t0).
Proof.
  unfold OldS.dep_big_map_update.
  unfold Script_big_map.dep_big_map_update.
  rewrite <- dep_hash_comparable_data_is_backward_compatible.
  destruct Proto_K.Simulations.Script_ir_translator.dep_hash_comparable_data;
    [|reflexivity].
  Tactics.destruct_pairs; simpl.
  unfold Script_big_map.dep_big_map_update_by_hash; simpl.
  unfold With_family.migrate_big_map_skeleton.
  unfold Script_typed_ir.With_family.big_map.with_diff.
  unfold With_family.migrate_big_map_overlay.
  repeat f_equal; simpl.
  { unfold Script_typed_ir.Big_map_overlay.
    unfold Proto_K.Script_typed_ir.Big_map_overlay.
    with_strategy transparent [V5.Map.Make]
      unfold V5.Map.Make; simpl.
    repeat rewrite <- (Map.map_list_eq_fargs
      (fun p => (With_family.migrate_value (fst p),
        match snd p with
        | Some x => Some (With_family.migrate_value x)
        | None => None
        end))); try (now intros k []).
    now rewrite <- Map.add_map_fargs.
  }
  { unfold Script_typed_ir.Big_map_overlay.
    unfold Proto_K.Script_typed_ir.Big_map_overlay.
    with_strategy transparent [V5.Map.Make]
      unfold V5.Map.Make; simpl.
    rewrite <- (Map.map_list_eq_fargs
      (fun p => (With_family.migrate_value (fst p),
        match snd p with
        | Some x => Some (With_family.migrate_value x)
        | None => None
        end))); try (now intros k []).
    now rewrite Map.mem_map_fargs.
  }
Qed.

(** [dep_big_map_get_and_update] is backward compatible. *)
Lemma dep_big_map_get_and_update_is_backward_compatible {a b}
  ctxt
  (l : Proto_K.Local_gas_counter.local_gas_counter)
  (s : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a)
  (t : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
      (Proto_K.Simulations.Script_family.Ty.Option b))
  (t0 : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
       (Proto_K.Simulations.Script_family.Ty.Big_map a b)) :
  Error.migrate_monad
    (Proto_K.Simulations.Script_ir_translator.dep_big_map_get_and_update
       (Proto_K.Local_gas_counter.update_context l ctxt) s t t0)
    (fun '((o, m), c) =>
      match o with
      | Some x =>
          ((Some (Script_typed_ir.With_family.migrate_value x),
             @Script_typed_ir.With_family.migrate_value
               (Proto_K.Simulations.Script_family.Ty.Big_map a b) m),
            Raw_context.migrate c)
      | None => ((None, @Script_typed_ir.With_family.migrate_value
          (Proto_K.Simulations.Script_family.Ty.Big_map a b) m),
          Raw_context.migrate c)
      end)
  =
  Script_big_map.dep_big_map_get_and_update
    (Local_gas_counter.update_context
      (Local_gas_counter.migrate_local_gas_counter l)
      (Local_gas_counter.migrate_outdated_context ctxt))
    (Script_typed_ir.With_family.migrate_value s)
    (match t with
     | Some x =>
         Some (Script_typed_ir.With_family.migrate_value x)
     | None => None
     end)
    (Script_typed_ir.With_family.migrate_big_map_skeleton
      Script_typed_ir.With_family.migrate_value
      Script_typed_ir.With_family.migrate_value t0).
Proof.
  unfold OldS.dep_big_map_get_and_update.
  unfold Script_big_map.dep_big_map_get_and_update.
  rewrite <- dep_hash_comparable_data_is_backward_compatible.
  destruct Proto_K.Simulations.Script_ir_translator.dep_hash_comparable_data;
    [|reflexivity].
  Tactics.destruct_pairs; simpl.
  rewrite <- dep_big_map_get_by_hash_is_backward_compatible.
  unfold Error.migrate_monad.
  unfold V5.Error_monad.op_gtgtquestion.
  destruct OldS.dep_big_map_get_by_hash; [|reflexivity].
  Tactics.destruct_pairs; simpl.
  unfold With_family.migrate_big_map_skeleton.
  unfold Script_typed_ir.With_family.big_map.with_diff.
  unfold With_family.migrate_big_map_overlay.
  destruct o.
  { repeat f_equal.
    { repeat rewrite <- (Map.map_list_eq_fargs
        (fun p => (With_family.migrate_value (fst p),
          match snd p with
          | Some x => Some (With_family.migrate_value x)
          | None => None
          end))); try (now intros k []).
      unfold Proto_K.Script_typed_ir.Big_map_overlay.
      with_strategy transparent [V5.Map.Make]
        unfold V5.Map.Make; simpl.
      now rewrite <- Map.add_map_fargs.
    }
    { repeat rewrite <- (Map.map_list_eq_fargs
        (fun p => (With_family.migrate_value (fst p),
          match snd p with
          | Some x => Some (With_family.migrate_value x)
          | None => None
          end))); try (now intros k []).
      unfold Proto_K.Script_typed_ir.Big_map_overlay.
      with_strategy transparent [V5.Map.Make]
        unfold V5.Map.Make; simpl.
      now erewrite <- Map.mem_map_fargs.
    }
  }
  { repeat f_equal.
    { repeat rewrite <- (Map.map_list_eq_fargs
        (fun p => (With_family.migrate_value (fst p),
          match snd p with
          | Some x => Some (With_family.migrate_value x)
          | None => None
          end))); try (now intros k []).
      unfold Proto_K.Script_typed_ir.Big_map_overlay.
      with_strategy transparent [V5.Map.Make]
        unfold V5.Map.Make; simpl.
      now rewrite <- Map.add_map_fargs.
    }
    { repeat rewrite <- (Map.map_list_eq_fargs
        (fun p => (With_family.migrate_value (fst p),
          match snd p with
          | Some x => Some (With_family.migrate_value x)
          | None => None
          end))); try (now intros k []).
      unfold Proto_K.Script_typed_ir.Big_map_overlay.
      with_strategy transparent [V5.Map.Make]
        unfold V5.Map.Make; simpl.
      now erewrite <- Map.mem_map_fargs.
    }
  }
Qed.

(** [dep_unparse_data] with mode set to Optimized is backward compatible *)
Lemma dep_unparse_data_optimized_is_backward_compatible
  (a : Proto_K.Simulations.Script_family.Ty.t)
  (ctxt : Proto_K.Alpha_context.context)
  (ty : Proto_K.Simulations.Script_typed_ir.With_family.ty a)
  (a_value
    : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a) :
  Error.migrate_monad (OldS.dep_unparse_data ctxt Old.Optimized ty a_value)
    (fun '(sn, ctxt) => (Script_repr.migrate_node sn, Raw_context.migrate ctxt))
  = NewS.dep_unparse_data (Raw_context.migrate ctxt)
      New.Optimized (With_family.migrate_ty ty)
      (With_family.migrate_value (t := a) a_value).
Proof.
  unfold OldS.dep_unparse_data, NewS.dep_unparse_data.
  apply dep_unparse_data_aux_optimized_is_backward_compatible.
Qed.
