Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.

Require TezosOfOCaml.Proto_K.Raw_context.
Require TezosOfOCaml.Proto_K.Storage_functors.
Require TezosOfOCaml.Proto_K_alpha.Storage_functors.

(** [Indexed_data_storage] backward compatibility *)
Module Make_indexed_data_storage.
  (** [find] is backward compatible *)
  Lemma find_is_backward_compatible
    {I_t V_t : Set} {I_ipath : Set -> Set}
    `{HFArgs0 : Proto_K.Storage_functors.Make_indexed_data_storage.FArgs
        (C_t := Proto_K.Raw_context.t) (V_t := V_t) (I_t:=I_t)
        (I_ipath:=I_ipath)}
    `{HFArgs1 : Storage_functors.Make_indexed_data_storage.FArgs
        (C_t := Raw_context.New.t) (V_t := V_t) (I_t:=I_t) (I_ipath:=I_ipath)}
     ctxt s
    : Error.migrate_monad
        (Proto_K.Storage_functors.Make_indexed_data_storage.find
           (H:=HFArgs0) ctxt s) id
      = Storage_functors.Make_indexed_data_storage.find
          (H:=HFArgs1) (Raw_context.migrate ctxt) s.
    Proof. Admitted.
End Make_indexed_data_storage.

(** [Single_data_storage] backward compatibility *)
Module Make_single_data_storage.
  (** [get] is backward compatible *)
  Lemma get_is_backward_compatible {value : Set}
    `{HFArgs0 : Proto_K.Storage_functors.Make_single_data_storage.FArgs
        (C_t := Proto_K.Raw_context.t) (V_t := value)}
    `{HFArgs1 : Storage_functors.Make_single_data_storage.FArgs
        (C_t := Raw_context.New.t) (V_t := value)}
    (ctxt : Proto_K.Raw_context.t)
    : Error.migrate_monad
        (Proto_K.Storage_functors.Make_single_data_storage.get ctxt) id
      = Storage_functors.Make_single_data_storage.get
          (Raw_context.migrate ctxt).
  Proof. Admitted.
End Make_single_data_storage.
