Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.

Require TezosOfOCaml.Proto_K.Script_comparable.
Require TezosOfOCaml.Proto_alpha.Script_comparable.

Require TezosOfOCaml.Proto_K.Script_typed_ir.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Require TezosOfOCaml.Proto_K.Simulations.Script_comparable.
Require TezosOfOCaml.Proto_alpha.Simulations.Script_comparable.

Require TezosOfOCaml.Proto_K.Simulations.Script_family.
Require TezosOfOCaml.Proto_alpha.Simulations.Script_family.

Require TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.
Require TezosOfOCaml.Proto_alpha.Simulations.Script_typed_ir.

Require Import TezosOfOCaml.Proto_K_alpha.Script_typed_ir.
Require Import TezosOfOCaml.Proto_K_alpha.Simulations.Script_family.

Module Old := TezosOfOCaml.Proto_K.Script_comparable.
Module New := TezosOfOCaml.Proto_alpha.Script_comparable.

Module OldIR := TezosOfOCaml.Proto_K.Script_typed_ir.
Module NewIR := TezosOfOCaml.Proto_alpha.Script_typed_ir.

Lemma compare_adress_is_backward_compatible
  (fp fp' : OldIR.address)
  : Old.compare_address fp fp' =
      New.compare_address (migrate_address fp) (migrate_address fp').
Proof.
  destruct fp, fp'. unfold Old.compare_address, New.compare_address.
  simpl. unfold Compare.Int.op_eq. simpl.
  unfold Alpha_context.Destination.compare,
         Proto_K.Alpha_context.Destination.compare.
  simpl. destruct destination, destination0; simpl; trivial.
  now destruct t, t0.
Qed.

Module Simulations.
  Module Old := TezosOfOCaml.Proto_K.Simulations.Script_comparable.
  Module New := TezosOfOCaml.Proto_alpha.Simulations.Script_comparable.

  Module OldSF := TezosOfOCaml.Proto_K.Simulations.Script_family.
  Module NewSF := TezosOfOCaml.Proto_alpha.Simulations.Script_family.

  Module Compare_comparable.
    Module Old :=
      TezosOfOCaml.Proto_K.Simulations.Script_comparable.Compare_comparable.
    Module New :=
      TezosOfOCaml.Proto_alpha.Simulations.Script_comparable.Compare_comparable.

    (** [dep_compare_comparable] is backward compatible. *)
    Theorem dep_compare_comparable_is_backward_compatible
      (kind : OldSF.Ty.t) (k : int) (x y : OldSF.Ty.to_Set kind) :
      Old.dep_compare_comparable kind k x y =
        New.dep_compare_comparable (Ty.migrate kind) k (Ty.migrate_to_Set x)
          (Ty.migrate_to_Set y).
    Proof.
      revert k. induction kind; intros; simpl; trivial; destruct x, y; simpl; trivial.
      { rewrite compare_adress_is_backward_compatible.
        destruct New.compare_address; trivial.
      }
      { now rewrite IHkind1, IHkind2. }
    Qed.

  End Compare_comparable.

  (** [dep_compare_comparable] is backward compatible. *)
  Lemma dep_compare_comparable_is_backward_compatible
    (kind : OldSF.Ty.t) (x y : OldSF.Ty.to_Set kind) :
    Old.dep_compare_comparable kind x y =
      New.dep_compare_comparable (Ty.migrate kind) (Ty.migrate_to_Set x)
        (Ty.migrate_to_Set y).
  Proof.
    unfold Old.dep_compare_comparable, New.dep_compare_comparable.
    apply Compare_comparable.dep_compare_comparable_is_backward_compatible.
  Qed.
End Simulations.

(** [compare_keys] is backward compatible. *)
Lemma compare_keys_is_backward_compatible {a}
  (s : Proto_K.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a)
  (k : V5.Map.Make.key) :
    @Make.compare_keys (Script_family.Ty.to_Set (Ty.migrate a))
      (@Make.Build_FArgs (Script_family.Ty.to_Set (Ty.migrate a))
         (COMPARABLE.Build_signature (Script_family.Ty.to_Set (Ty.migrate a))
            (Script_comparable.dep_compare_comparable (Ty.migrate a))))
      (@Ty.migrate_to_Set a (@Proto_K.Simulations.Script_typed_ir.With_family.to_value a s))
      (@Ty.migrate_to_Set a k)
  =
    @V5.Map.Make.compare_keys (Proto_K.Simulations.Script_family.Ty.to_Set a)
      (@V5.Map.Make.Build_FArgs (Proto_K.Simulations.Script_family.Ty.to_Set a)
         (V5.Compare.COMPARABLE.Build_signature (Proto_K.Simulations.Script_family.Ty.to_Set a)
            (Proto_K.Simulations.Script_comparable.dep_compare_comparable a)))
      (@Proto_K.Simulations.Script_typed_ir.With_family.to_value a s) k.
Proof.
  destruct a; simpl; destruct s, k; auto.
  all: 
    unfold Make.compare_keys; simpl;
    now rewrite Simulations.dep_compare_comparable_is_backward_compatible.
Qed.
