Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_alpha.Sapling_storage.
Require TezosOfOCaml.Proto_K_alpha.Raw_context.
Require TezosOfOCaml.Proto_K_alpha.Sapling_storage.
Require TezosOfOCaml.Proto_K.Proofs.Sapling_storage.

(* @TODO *)
(** [root_mem] is backward compatible *)
Lemma root_mem_is_backward_compatible (ctxt : Proto_K.Raw_context.t)
  (state : Proto_K.Sapling_storage.state)
  (hash : V5.Sapling.Hash.t) :
  let x := Proto_K.Sapling_storage.root_mem ctxt state hash in
  Error.migrate_monad x id =
  Proto_alpha.Sapling_storage.root_mem
    (Raw_context.migrate ctxt) 
    (Sapling_storage.migrate_state state) 
    hash.
Proof.
  unfold Proto_K.Sapling_storage.root_mem,
    Sapling_storage.root_mem.
  destruct state; cbn.
  ez destruct id; cbn.
  with_strategy transparent [Proto_K.Storage.Sapling.Roots_pos] 
    unfold Proto_K.Sapling_storage.Roots.mem,
    Sapling_storage.Roots.mem,
    Storage.Sapling.Roots_pos,
    Proto_K.Storage.Sapling.Roots_pos,
    Proto_alpha.Sapling_storage.Roots.mem; cbn.
Admitted.
