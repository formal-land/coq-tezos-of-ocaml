Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_K.Gas_limit_repr.
Require TezosOfOCaml.Proto_K.Raw_context.
Require TezosOfOCaml.Proto_alpha.Raw_context.

Require TezosOfOCaml.Proto_K_alpha.Error.
Require TezosOfOCaml.Proto_K_alpha.Origination_nonce.
Require Import TezosOfOCaml.Proto_K_alpha.Raw_context.

Module Old := TezosOfOCaml.Proto_K.Raw_context.
Module New := TezosOfOCaml.Proto_alpha.Raw_context.

(** [increment_origination_nonce] is backward compatible *)
Lemma increment_origination_nonce_is_backward_compatible
  (ctxt : Old.t) :
  Error.migrate_monad
    (Old.increment_origination_nonce ctxt)
    (fun '(c, on) => (migrate c, Origination_nonce.migrate on))
  = New.increment_origination_nonce (migrate ctxt).
Proof.
  destruct ctxt, back.
  unfold Old.increment_origination_nonce, Old.origination_nonce.
  unfold New.increment_origination_nonce, New.origination_nonce.
  simpl. now destruct origination_nonce.
Qed.

(** [fresh_internal_nonce] is backward compatible *)
Lemma fresh_internal_nonce_is_backward_compatible
  (ctxt : Old.t) :
  Error.migrate_monad
    (Old.fresh_internal_nonce ctxt)
    (fun '(c, i) => (migrate c, i))
  = New.fresh_internal_nonce (migrate ctxt).
Proof.
  destruct ctxt, back.
  unfold Old.fresh_internal_nonce, New.fresh_internal_nonce.
  unfold Old.internal_nonce, New.internal_nonce.
  simpl.
  now destruct Compare.Int.op_gteq.
Qed.

(** [consume_gas] is backward compatible *)
Lemma consume_gas_is_backward_compatible
  (ctxt : Old.t) (cost : Proto_K.Gas_limit_repr.cost) :
  Error.migrate_monad (Old.consume_gas ctxt cost) migrate =
    New.consume_gas (migrate ctxt) cost.
Proof.
  destruct ctxt. unfold Old.consume_gas, New.consume_gas. simpl.
  unfold Proto_K.Gas_limit_repr.cost,
         Proto_K.Gas_limit_repr.S.t in cost.
  unfold Proto_K.Gas_limit_repr.Arith.fp,
         Proto_K.Gas_limit_repr.Arith.t,
         Proto_K.Gas_limit_repr.S.t in remaining_operation_gas.
  unfold Proto_K.Gas_limit_repr.raw_consume, Gas_limit_repr.raw_consume.
  unfold Proto_K.Gas_limit_repr.Arith.sub_opt,
         Gas_limit_repr.Arith.sub_opt.
  unfold Proto_K.Gas_limit_repr.S.sub_opt, Gas_limit_repr.S.sub_opt.
  unfold Proto_K.Gas_limit_repr.cost_to_milligas,
         Gas_limit_repr.cost_to_milligas.
  destruct
    (if Compare.Int.op_gteq
        (Pervasives.op_minus remaining_operation_gas cost) 0
     then Some (Pervasives.op_minus remaining_operation_gas cost)
     else None); trivial.
  unfold Old.unlimited_operation_gas, New.unlimited_operation_gas. simpl.
  destruct back.(Old.back.unlimited_operation_gas); trivial.
Qed.

(** [update_remaining_operation_gas] is backward compatible *)
Lemma update_remaining_operation_gas_is_backward_compatible
  (ctxt : Old.t) (remaining_operation_gas : Gas_limit_repr.Arith.fp)
  : migrate (Old.update_remaining_operation_gas ctxt remaining_operation_gas) =
    New.update_remaining_operation_gas (migrate ctxt) remaining_operation_gas.
Proof.
  destruct ctxt.
  unfold Old.update_remaining_operation_gas,
         New.update_remaining_operation_gas.
  unfold Old.t.with_remaining_operation_gas,
         New.t.with_remaining_operation_gas.
  unfold migrate. simpl. trivial.
Qed.
