Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_K.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_K_alpha.Raw_context.
Require TezosOfOCaml.Proto_K_alpha.Sapling_storage.

Module Gas.
  (** The [remaining_operation_gas] function is backward compatible. *)
  Lemma remaining_operation_gas_is_backward_compatible t :
    Proto_K.Alpha_context.Gas.remaining_operation_gas t =
      Proto_alpha.Alpha_context.Gas.remaining_operation_gas (Raw_context.migrate t).
  Proof.
    reflexivity.
  Qed.
End Gas.

Module Sapling.
  Module Legacy.
    (** [verify_update] is backward compatible *)
    Lemma verify_update_is_backward_compatible 
      (ctxt : Proto_K.Raw_context.t) 
      (s : Proto_K.Sapling_storage.state) 
      (tx : Proto_K.Alpha_context.Sapling.Legacy.transaction)
      (str : string) :
      (let migrate '(ctxt', o) :=
         (Raw_context.migrate ctxt', Option.map (fun '(i, s) =>
           (i, Sapling_storage.migrate_state s)) o) in
      let x := Proto_K.Alpha_context.Sapling.Legacy.verify_update
        ctxt s tx str in
      Error.migrate_monad x migrate) =
      Proto_alpha.Alpha_context.Sapling.Legacy.verify_update
        (Raw_context.migrate ctxt) (Sapling_storage.migrate_state s)
        tx str.
    Proof.
    Admitted.
  End Legacy.
End Sapling.
