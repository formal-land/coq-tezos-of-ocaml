Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.

Require TezosOfOCaml.Proto_K.Local_gas_counter.
Require TezosOfOCaml.Proto_alpha.Local_gas_counter.

Require Import TezosOfOCaml.Proto_K_alpha.Local_gas_counter.
Require TezosOfOCaml.Proto_K_alpha.Raw_context.

Module Old := TezosOfOCaml.Proto_K.Local_gas_counter.
Module New := TezosOfOCaml.Proto_alpha.Local_gas_counter.

(** [outdated_context_value] is backward compatitble *)
Lemma outdated_context_value_is_backward_compatible
  (ctxt : Proto_K.Alpha_context.context) :
  Local_gas_counter.migrate_outdated_context
    (Old.outdated_context_value ctxt) =
  New.outdated_context_value (Raw_context.migrate ctxt).
Proof.
  reflexivity.
Qed.

(** [update_context] from Local_gas_counter is backward compatible *)
Lemma update_context_is_backward_compatible
  (l : Old.local_gas_counter) (ctxt : Old.outdated_context) :
  Raw_context.migrate (Old.update_context l ctxt) =
    New.update_context (migrate_local_gas_counter l)
      (migrate_outdated_context ctxt).
Proof.
  destruct l, ctxt. simpl.
  unfold Proto_K.Alpha_context.context, Proto_K.Alpha_context.t in c.
  unfold Proto_K.Alpha_context.Gas.fp_of_milligas_int,
         Alpha_context.Gas.fp_of_milligas_int.
  unfold Proto_K.Saturation_repr.safe_int, Saturation_repr.safe_int.
  unfold Proto_K.Saturation_repr.of_int_opt, Saturation_repr.of_int_opt.
  unfold Proto_K.Saturation_repr.op_gteq, Saturation_repr.op_lt.
  trivial.
Qed.

(** [local_gas_counter_value] is backward compatible *)
Lemma local_gas_counter_value_is_backward_compatible
  (ctxt : Proto_K.Alpha_context.context) :
  Local_gas_counter.migrate_local_gas_counter
    (Old.local_gas_counter_value ctxt) =
  Local_gas_counter.local_gas_counter_value (Raw_context.migrate ctxt).
Proof.
  reflexivity.
Qed.

(* [use_gas_counter_in_context] is backward compatible *)
Lemma use_gas_counter_in_context_is_backward_compatible
  {A : Set}
  (ctxt : Proto_K.Local_gas_counter.outdated_context)
  (lgc : Proto_K.Local_gas_counter.local_gas_counter)
  (f_old :
    Proto_K.Alpha_context.context ->
    M? (A * Proto_K.Alpha_context.context))
  (f_new :
    Proto_alpha.Alpha_context.context ->
    M? (A * Proto_alpha.Alpha_context.context)) :
  (forall ctxt,
    Error.migrate_monad (f_old ctxt) (fun '(a, ctxt) => (a, Raw_context.migrate ctxt)) =
    f_new (Raw_context.migrate ctxt)) ->
  Error.migrate_monad
    (Proto_K.Local_gas_counter.use_gas_counter_in_context ctxt lgc f_old)
    (fun '(a, oc, lgc) => (
      a,
      Local_gas_counter.migrate_outdated_context oc,
      Local_gas_counter.migrate_local_gas_counter lgc
    )) =
  Proto_alpha.Local_gas_counter.use_gas_counter_in_context
    (Local_gas_counter.migrate_outdated_context ctxt)
    (Local_gas_counter.migrate_local_gas_counter lgc)
    f_new.
Proof.
  intro H;
  unfold
    Result.map,
    Proto_K.Local_gas_counter.use_gas_counter_in_context,
    Local_gas_counter.use_gas_counter_in_context.
  rewrite
    <- update_context_is_backward_compatible, <- H.
  change
    (Local_gas_counter.Old.update_context lgc ctxt)
    with
    (Proto_K.Local_gas_counter.update_context lgc ctxt).
  step; Tactics.destruct_pairs; reflexivity.
Qed.

(** [consume_opt] is backward compatible *)
Lemma consume_opt_is_backward_compatible
  (local_gas_counter_value : Old.local_gas_counter)
  (cost : Proto_K.Alpha_context.Gas.cost) :
    Option.map migrate_local_gas_counter
      (Old.consume_opt local_gas_counter_value cost) =
      New.consume_opt (migrate_local_gas_counter local_gas_counter_value) cost.
Proof.
  unfold Old.consume_opt, New.consume_opt. destruct local_gas_counter_value.
  simpl. now destruct Compare.Int.op_lt.
Qed.

(** [consume] is backward compatible *)
Lemma consume_is_backward_compatible
  (local_gas_counter_value : Old.local_gas_counter)
  (cost : Proto_K.Alpha_context.Gas.cost) :
    Error.migrate_monad (Old.consume local_gas_counter_value cost)
      migrate_local_gas_counter =
      New.consume (migrate_local_gas_counter local_gas_counter_value) cost.
Proof.
  unfold Old.consume, New.consume.
  rewrite <- consume_opt_is_backward_compatible. now destruct Old.consume_opt.
Qed.
