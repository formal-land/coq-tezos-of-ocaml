Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_K.Simulations.Script_interpreter.
Require TezosOfOCaml.Proto_alpha.Simulations.Script_interpreter.
Require TezosOfOCaml.Proto_K_alpha.Simulations.Script_typed_ir.
Require TezosOfOCaml.Proto_K_alpha.Local_gas_counter.
Require TezosOfOCaml.Proto_K_alpha.Proofs.Raw_context.

Module OldIR := TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.
Module NewIR := TezosOfOCaml.Proto_alpha.Simulations.Script_typed_ir.

Module Old := TezosOfOCaml.Proto_K.Simulations.Script_interpreter_defs.
Module New := TezosOfOCaml.Proto_alpha.Simulations.Script_interpreter_defs.

Module Interp_costs.
  Module Old := Old.Interp_costs.
  Module New := New.Interp_costs.

  (** [concat_string] is backward compatible *)
  Lemma concat_string_is_backward_compatible
    (total_bytes : Proto_K.Michelson_v1_gas.S.t) :
      Old.concat_string total_bytes = New.concat_string total_bytes.
  Proof.
    trivial.
  Qed.
End Interp_costs.

(** [dep_consume_instr] is backward compatible. *)
Lemma dep_consume_instr_is_backward_compatible {s f} gas
  (i : OldIR.With_family.kinstr s f) stack :
  (let* new_gas :=
  Old.dep_consume_instr gas i stack in
  return* (Local_gas_counter.migrate_local_gas_counter new_gas)) =
  New.dep_consume_instr
    (Local_gas_counter.migrate_local_gas_counter gas)
    (Script_typed_ir.With_family.migrate_kinstr i)
    (Script_typed_ir.With_family.migrate_stack_value stack).
Proof.
Admitted.

(** [dep_kundip] is backward_compatible *)
Lemma dep_kundip_is_backward_compatible
  (s z u w t : Proto_K.Simulations.Script_family.Stack_ty.t)
  (st : Script_typed_ir.Old.With_family.stack_ty u)
  (w_value : Proto_K.Simulations.Script_typed_ir.With_family
               .stack_prefix_preservation_witness s z u w)
  (accu_stack : Proto_K.Simulations.Script_typed_ir
                  .With_family.stack_ty_to_dep_Set u)
  (k_value : Proto_K.Simulations.Script_typed_ir.With_family.kinstr w t) :
  (fun '(st, k) =>
    (Script_typed_ir.With_family.migrate_stack_value st,
     Script_typed_ir.With_family.migrate_kinstr k))
    (Old.dep_kundip w_value accu_stack k_value) =
  New.dep_kundip
    (Script_typed_ir.With_family.migrate_stack_prefix_preservation_witness
      st w_value)
    (Script_typed_ir.With_family.migrate_stack_value accu_stack)
    (Script_typed_ir.With_family.migrate_kinstr k_value).
Proof.
  induction w_value; cbn; trivial.
  destruct accu_stack. cbn.
  rewrite IHw_value with (st := Script_typed_ir.With_family.stack_ty_tail st).
  cbn. f_equal.
  { unfold Script_typed_ir.With_family.migrate_stack_value. now destruct s1. }
  { admit. (* TODO: axiom "ty" *) }
Admitted.

(** [dep_unpack] is backward compatible *)
Fixpoint dep_unpack_is_backward_compatible
  {ty : Proto_K.Simulations.Script_family.Ty.t}
  (ctxt : Raw_context.Old.t) 
  (dep_ty : OldIR.With_family.ty ty)
  (b : bytes) 
  {struct ty} :
  let migrate '(o, ctxt) :=
     (Option.map (fun x => Script_typed_ir.With_family.migrate_value x) 
       o, Raw_context.migrate ctxt) in
  let x := Old.dep_unpack ctxt dep_ty b in
  Error.migrate_monad x migrate =
  New.dep_unpack
    (Raw_context.migrate ctxt) (Script_typed_ir.With_family.migrate_ty dep_ty) b.
Proof.
  cbn.
  unfold Old.dep_unpack, New.dep_unpack,
    Proto_K.Alpha_context.Gas.consume.
  rewrite <- Raw_context.consume_gas_is_backward_compatible; cbn.
  do 2 match goal with
  | |- context [if ?cond' then _ else _] => 
      let cond := fresh "cond" in
      set (cond := cond'); symmetry
  end.
  assert (H_cond : cond = cond0).
  { subst cond cond0.
    rewrite Saturation_repr.safe_int_is_backward_compatible.
    unfold land, Bytes.length.
    reflexivity.
  }
  rewrite H_cond; clear cond H_cond.
  destruct cond0; cbn;
  unfold Bytes.length, get_uint8;
  match goal with
  | |- context [if ?cond' then _ else _] => 
      destruct cond'; cbn
  end;
    unfold "*i";
    try (
    rewrite Saturation_repr.safe_int_is_backward_compatible;
    destruct (Proto_K.Raw_context.consume_gas ctxt _)); cbn;
    trivial.
  (* @TODO finish the proof *)
Admitted.

(** [dep_transfer] is backward compatible *)
Lemma dep_transfer_is_backward_compatible
  {a : Proto_K.Simulations.Script_family.Ty.t}
  (g : Proto_K.Local_gas_counter.outdated_context *
         Proto_K.Script_typed_ir.step_constants)
  (gas : Proto_K.Local_gas_counter.local_gas_counter)
  (amount : Proto_K.Alpha_context.Tez.tez)
  (location : Proto_K.Alpha_context.Script.location)
  (parameters_ty : Proto_K.Simulations.Script_typed_ir.With_family.ty a)
  (parameters : Proto_K.Simulations.Script_typed_ir.With_family
                  .ty_to_dep_Set a)
  (destination : Proto_K.Alpha_context.Destination.t)
  (entrypoints : Proto_K.Alpha_context.Entrypoint.t) :
  Error.migrate_monad
    (Old.dep_transfer g gas amount location parameters_ty parameters
       destination entrypoints)
    (fun '(op, oc, lgc) =>
      (Script_typed_ir.migrate_operation op,
       Local_gas_counter.migrate_outdated_context oc,
       Local_gas_counter.migrate_local_gas_counter lgc)
    ) =
    New.dep_transfer
      ((fun '(ctxt, sc) =>
         (Local_gas_counter.migrate_outdated_context ctxt,
          Script_typed_ir.migrate_step_constants sc)
      ) g)
      (Local_gas_counter.migrate_local_gas_counter gas)
      (Tez_repr.migrate amount) location
      (Script_typed_ir.With_family.migrate_ty parameters_ty)
      (Script_typed_ir.With_family.migrate_value parameters)
      (Destination_repr.migrate destination) entrypoints.
Proof.
  (* TODO *)
Admitted.
