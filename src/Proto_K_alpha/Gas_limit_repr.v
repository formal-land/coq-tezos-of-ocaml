Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_K.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.

Module Old := TezosOfOCaml.Proto_K.Gas_limit_repr.
Module New := TezosOfOCaml.Proto_alpha.Gas_limit_repr.

Module ConstructorRecords_t.
  Module t.
    Module Limited.
      Module Old := Old.ConstructorRecords_t.t.Limited.
      Module New := New.ConstructorRecords_t.t.Limited.
      Definition migrate {remaining : Set} (x : Old.record remaining)
      : New.record remaining :=
      {| New.remaining := x.(Old.remaining); |}.
    End Limited.
  End t.
End ConstructorRecords_t.
Import ConstructorRecords_t.

(** Migrate [Gas_limit_repr.t]. *)
Definition migrate (x : Old.t) : New.t :=
  match x with
  | Old.Unaccounted => New.Unaccounted
  | Old.Limited l => New.Limited (t.Limited.migrate l)
  end.
