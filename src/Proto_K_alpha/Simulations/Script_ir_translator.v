Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_K.Script_ir_translator.
Require TezosOfOCaml.Proto_alpha.Script_ir_translator.

Require TezosOfOCaml.Proto_K.Simulations.Script_ir_translator.
Require TezosOfOCaml.Proto_alpha.Simulations.Script_ir_translator.

Require TezosOfOCaml.Proto_K_alpha.Simulations.Script_typed_ir.

Module Old := TezosOfOCaml.Proto_K.Simulations.Script_ir_translator.
Module New := TezosOfOCaml.Proto_alpha.Simulations.Script_ir_translator.

(** Migrate [dep_ex_ty] *)
Definition migrate_dep_ex_ty (ty : Old.dep_ex_ty) : New.dep_ex_ty :=
  let 'Old.Dep_ex_ty ty := ty in
  New.Dep_ex_ty (Script_typed_ir.With_family.migrate_ty ty).
