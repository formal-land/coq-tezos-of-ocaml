Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_K_alpha.Tez_repr.
Require TezosOfOCaml.Proto_K_alpha.Script_interpreter.

(** Migrate all the errors. *)
Definition migrate (err : _error) : _error :=
  List.fold_right (fun f err => f err)
    [
      Tez_repr.migrate_error;
      Script_interpreter.migrate_error
    ]
    err.

(** Migrate an expression in the error monad. *)
Definition migrate_monad {a a' : Set} (x : M? a) (f : a -> a') : M? a' :=
  match x with
  | Pervasives.Ok v => return? f v
  | Pervasives.Error errs => Pervasives.Error (List.map migrate errs)
  end.
