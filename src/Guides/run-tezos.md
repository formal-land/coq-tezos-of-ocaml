Sometimes it is easier to run the code to understand what it does and how to verify it. Running Tezos code in a REPL is possible by

* Building Tezos: The instructions on how to build Tezos can be found [here](https://tezos.gitlab.io/introduction/howtoget.html#compiling-with-make) but you want to run `make build-dev-deps` instead of only `make build-deps`. 
* Following https://gitlab.com/tezos/tezos/-/blob/master/src/tooling/tztop/README.md

Here is an example of running Tezos code on utop :
![image](run-tezos/image.png)

Another option is to use unit tests to execute code and see how it behaves. Test helpers can be useful to run code that has a lot of dependencies. [Here](https://gitlab.com/dhilst/tezos/-/merge_requests/2) is an example MR a made adding a test. To run the tests you usually run something like `dune exec src/proto_alpha/lib_protocol/test/integration/main.exe test storage` replacing `storage` by the test suite that you edited.