Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.

Require TezosOfOCaml.Environment.V8.Proofs.S.

Axiom Included_HASH_is_valid
  : S.HASH.Valid.t (fun _ => True) Context_hash.Included_HASH.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Context_hash.encoding.
Proof.
  apply Included_HASH_is_valid.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Module Version.
  Axiom Included_S_is_valid : Compare.S.Valid.t Context_hash.Version.Included_S.

  Axiom encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True) Context_hash.Version.encoding.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Version.
