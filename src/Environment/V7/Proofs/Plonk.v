Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Environment.V7.Proofs.Data_encoding.

(** [scalar_array_encoding] is valid *)
Axiom scalar_array_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Plonk.scalar_array_encoding.
#[global] Hint Resolve scalar_array_encoding_is_valid : Data_encoding_db.

(** [public_parameters_encoding] is valid *)
Axiom public_parameters_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Plonk.public_parameters_encoding.
#[global] Hint Resolve public_parameters_encoding_is_valid : Data_encoding_db.
