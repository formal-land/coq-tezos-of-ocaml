Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.

Require TezosOfOCaml.Environment.V7.Proofs.Data_encoding.

Axiom canonical_encoding_is_valid
  : forall {a : Set} (title : string) {encoding : Data_encoding.t a},
    Data_encoding.Valid.t (fun _ => True) encoding ->
    Data_encoding.Valid.t (fun _ => True) (Micheline.canonical_encoding title encoding).
#[global] Hint Resolve canonical_encoding_is_valid : Data_encoding_db.

Axiom canonical_location_encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True) Micheline.canonical_location_encoding.
#[global] Hint Resolve canonical_location_encoding_is_valid : Data_encoding_db.
