Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.

Require TezosOfOCaml.Environment.V7.Proofs.Compare.
Require TezosOfOCaml.Environment.V7.Proofs.Data_encoding.

Axiom compare_is_valid : Compare.Valid.t (fun _ => True) id Chain_id.compare.

Axiom encoding_is_valid : Data_encoding.Valid.t (fun _ => True) Chain_id.encoding.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

(** Composition of [to_bytes] and [of_bytes_opt] is an identity function. *)
Axiom to_bytes_of_bytes_opt :
  forall {s t}, Chain_id.of_bytes_opt s = Some t -> Chain_id.to_bytes t = s.

(** Composition of [of_bytes_opt] and [to_bytes] is an identity function. *)
Axiom of_bytes_opt_to_bytes :
  forall {t}, Chain_id.of_bytes_opt (Chain_id.to_bytes t) = Some t.
