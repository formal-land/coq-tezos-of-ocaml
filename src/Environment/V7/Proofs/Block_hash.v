Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.

Require TezosOfOCaml.Environment.V7.Proofs.S.

Axiom Included_HASH_is_valid
  : S.HASH.Valid.t (fun _ => True) Block_hash.Included_HASH.

Lemma encoding_is_valid: Data_encoding.Valid.t (fun _ => True)
  Block_hash.encoding.
Proof.
  apply Included_HASH_is_valid.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.