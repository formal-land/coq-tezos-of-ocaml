Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.

Require TezosOfOCaml.Environment.V7.Proofs.Data_encoding.

Axiom shell_header_encoding_is_valid
  : Data_encoding.Valid.t (fun _ => True) Operation.shell_header_encoding.
#[global] Hint Resolve shell_header_encoding_is_valid : Data_encoding_db.

Axiom Included_HASHABLE_encoding_is_valid : 
  Data_encoding.Valid.t (fun _ : Operation.t => True)
  Operation.Included_HASHABLE.(S.HASHABLE.encoding).
#[global] Hint Resolve Included_HASHABLE_encoding_is_valid : Data_encoding_db.
