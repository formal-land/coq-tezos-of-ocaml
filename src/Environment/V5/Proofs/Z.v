Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.

Require TezosOfOCaml.Environment.V5.Proofs.Compare.
Require TezosOfOCaml.Environment.V6.Proofs.Int32.
Require TezosOfOCaml.Environment.V6.Proofs.Int64.

Lemma compare_is_valid
  : Proofs.Compare.Valid.t (fun _ => True) id Z.compare.
  exact Compare.z_is_valid.
Qed.

(** When in bounds, the conversion to [int] is the identity. *)
Lemma to_int_eq (z : Z.t) :
  Pervasives.Int.Valid.t z ->
  Z.to_int z = z.
Proof.
  intros.
  unfold to_int.
  do 2 (destruct (_ <=? _) eqn:? in |- *); simpl;
    try reflexivity;
    lia.
Qed.

(** When in bounds, the conversion to [int32] is the identity. *)
Lemma to_int32_eq (z : Z.t) :
  Int32.Valid.t z ->
  Z.to_int32 z = z.
Proof.
  intros.
  unfold to_int32.
  do 2 (destruct (_ <=? _) eqn:? in |- *); simpl;
    try reflexivity;
    lia.
Qed.

(** When in bounds, the conversion to [int64] is the identity. *)
Lemma to_int64_eq (z : Z.t) :
  Int64.Valid.t z ->
  Z.to_int64 z = z.
Proof.
  intros.
  unfold to_int64.
  do 2 (destruct (_ <=? _) eqn:? in |- *); simpl;
    try reflexivity;
    lia.
Qed.

Axiom to_string_of_string_eq : forall s,
  to_string (of_string s) = s.
