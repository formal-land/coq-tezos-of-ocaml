Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.

Require TezosOfOCaml.Environment.V5.Proofs.S.

Axiom Included_HASH_is_valid
  : S.HASH.Valid.t (fun _ => True) Protocol_hash.Included_HASH.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Protocol_hash.encoding.
  apply Included_HASH_is_valid.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

(* TODO: make this axiom more precise and verify it. *)
Axiom Map_encoding_is_valid : forall {a : Set} domain encoding,
  Data_encoding.Valid.t domain encoding ->
  Data_encoding.Valid.t (fun _ => True)
    (Protocol_hash.Map.(INDEXES_MAP.encoding) (a := a) encoding).
#[global] Hint Resolve Map_encoding_is_valid : Data_encoding_db.
