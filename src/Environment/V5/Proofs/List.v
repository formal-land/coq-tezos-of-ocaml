Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Require Coq.Sorting.Sorted.

Require Import TezosOfOCaml.Environment.V5.
Require Import TezosOfOCaml.Environment.V5.Proofs.Compare.
Require Import TezosOfOCaml.Environment.V5.Proofs.Pervasives.
Require Import TezosOfOCaml.Environment.V5.Proofs.Utils.

Module Int_length.
  (** Lists whose length fits in an OCaml's [int]. This condition is required
      for all operations involving [length] or [nth] primitives. *)
  Definition t {a : Set} (l : list a) : Prop :=
    Pervasives.Int.Valid.t (Z.of_nat (Lists.List.length l)).
  #[global] Hint Unfold t : tezos_z.
End Int_length.

(** The [length] operator in [int] behaves as the length in [nat]. *)
Lemma length_eq {a : Set} (l : list a) :
  List.length l = Pervasives.normalize_int (Z.of_nat (Lists.List.length l)).
Proof.
  induction l; simpl; lia.
Qed.

(** The [nth] element in a list concatenation. *)
Lemma nth_app_eq {a : Set} (l1 l2 : list a) :
  Int_length.t l1 ->
  List.nth (l1 ++ l2) (List.length l1) = List.hd l2.
Proof.
  unfold Int_length.t; intros; induction l1; simpl in *.
  { destruct l2; reflexivity. }
  { rewrite length_eq in *.
    rewrite Pervasives.normalize_identity in * by lia.
    unfold "+i".
    rewrite Pervasives.normalize_identity by lia.
    destruct (_ + _)%Z eqn:H_eq; [lia | | lia].
    rewrite <- H_eq.
    rewrite <- IHl1 by lia.
    f_equal; lia.
  }
Qed.

Lemma length_destruct {a : Set} (x : a) (l : list a) :
  List.length (x :: l) = List.length l +i 1.
Proof.
  induction l; simpl; lia.
Qed.

Lemma length_is_valid {a : Set} (l : list a) :
  Pervasives.Int.Valid.t (List.length l).
Proof.
  induction l; simpl; lia.
Qed.

Fixpoint length_app_sum {a : Set} (l1 l2 : list a)
  : List.length (l1 ++ l2) =
    List.length l1 +i List.length l2.
  assert (H_l1l2 := length_is_valid (l1 ++ l2)).
  destruct l1; simpl.
  { now rewrite Pervasives.normalize_identity. }
  { rewrite length_app_sum.
    lia.
  }
Qed.

(** The [reverse] operation preserves the [length] of a list. *)
Lemma length_rev_eq {a : Set} (l : list a) :
  List.length (List.rev l) = List.length l.
Proof.
  induction l; simpl; [reflexivity|].
  unfold List.rev, Lists.List.rev'; simpl.
  rewrite List.rev_append_rev, List.rev_alt.
  rewrite length_app_sum.
  now rewrite <- IHl.
Qed.

Fixpoint mem_eq_dec {a : Set} {eq_dec : a -> a -> bool}
  (H_eq_dec : forall x y, eq_dec x y = true -> x = y) (v : a) (l : list a)
  : List.mem eq_dec v l = true ->
    List.In v l.
  destruct l; simpl; [congruence|].
  destruct (eq_dec _ _) eqn:H; simpl; [left | right].
  { symmetry.
    now apply H_eq_dec.
  }
  { now apply (mem_eq_dec _ eq_dec). }
Qed.

Definition in_list_string (s : string) (list : list string) : bool :=
  List.mem String.eqb s list.

Fixpoint all_distinct_strings (l : list string) : bool :=
  match l with
  | [] => true
  | h :: tl =>
    if in_list_string h tl then
      false
    else
      all_distinct_strings tl
  end.

(** [List.Forall] is the trivial case where the property is always true. *)
Fixpoint Forall_True {a : Set} {P : a -> Prop} {l : list a}
  : (forall x, P x) -> List.Forall P l.
  intro; destruct l; constructor; trivial; now apply Forall_True.
Qed.
(* These lemmas are often useful, especially for data-encoding proofs. *)
#[global] Hint Resolve Forall_True Forall_impl : core.

Lemma nil_has_no_last : forall (a : Set), last_opt (nil : List.t a) = None.
  reflexivity.
Qed.

Lemma tz_rev_append_rev {a : Set} : forall (l l' : list a),
  CoqOfOCaml.List.rev_append l l' = (Lists.List.rev l ++ l')%list.
  intro l; induction l; simpl; auto; intros.
  rewrite <- app_assoc; firstorder.
Qed.

Lemma lst_lst : forall (a : Set) (l : list a) (x : a),
  last_opt (l ++ [x])%list = Some x.
  intros.
  induction l; auto.
  simpl; rewrite IHl.
  destruct l; reflexivity.
Qed.

Lemma head_is_last_of_rev : forall (a : Set) (l : list a) (x : a),
  List.last_opt (List.rev l) = List.hd l.
Proof.
  unfold rev; unfold List.rev, Lists.List.rev'.
  intros.
  induction l; simpl; [reflexivity|].
  rewrite List.rev_append_rev, List.rev_alt.
  apply lst_lst.
Qed.

Lemma head_of_init : forall {a trace : Set} (f : int -> a) (x : trace),
  init_value x 1 f = Pervasives.Ok [f(0)].
  intros.
  unfold init_value.
  reflexivity.
Qed.

Lemma in_map : forall {a b : Set} (f : a -> b) (xs : list a) (x : a),
  In x xs -> In (f x) (List.map f xs).
  induction xs; intros x Hin; destruct Hin.
  - left; congruence.
  - right; apply IHxs; auto.
Qed.

Lemma fold_right_f_eq {a b : Set} (f g : a -> b -> b) (l : list a) (acc : b) :
  (forall x acc, f x acc = g x acc) ->
  fold_right f l acc = fold_right g l acc.
Proof.
  induction l; sfirstorder.
Qed.

(** [fold_left] gives equal results if functions used are equal *)
Lemma fold_left_f_eq {a b : Set} (f g : b -> a -> b) (l : list a) (acc : b) :
  f = g ->
  fold_left f acc l = fold_left g acc l.
Proof.
  intros H_func_ext. now rewrite H_func_ext.
Qed.

Lemma fold_right_opt_map : forall {a b c : Set} (f : a -> option b)
  (g : b -> c -> c) (xs : list a) (z : c),
  List.fold_right
    (fun x acc =>
      match f x with
      | Some y => g y acc
      | None => acc
      end) xs z =
  List.fold_right g (List.filter_map f xs) z.
  induction xs; hauto lq: on.
Qed.

Lemma fold_right_opt_no_None : forall {a b c : Set} (f : a -> option b)
  (g : b -> c -> c) (xs : list a) (z z_t : c),
  (forall x, In x xs -> exists y, f x = Some y) ->
  List.fold_right
    (fun x acc =>
      match f x with
      | Some y => g y acc
      | None => z_t
      end) xs z =
  List.fold_right g (List.filter_map f xs) z.
  induction xs; qauto l: on.
Qed.

Lemma fold_right_cons_nil : forall {a : Set} (xs : list a),
  fold_right cons xs [] = xs.
  induction xs; intros.
  - reflexivity.
  - simpl; congruence.
Qed.

Lemma In_to_In_fold_add : forall {a b c : Set}
  (f : a -> option b) (g : b -> c) (add : b -> list c -> list c)
  (xs : list a) (x : a) (y : b),
  (forall x xs, In (g x) (add x xs)) ->
  (forall x y xs, In (g x) xs -> In (g x) (add y xs)) ->
  In x xs -> f x = Some y ->
    In (g y) (fold_right
      (fun x' acc =>
        match f x' with
        | Some y' => add y' acc
        | None => acc
        end) xs []).
  induction xs; intros x y add_In1 add_In2 Hxxs Hfx;
  destruct Hxxs; simpl.
  - rewrite H, Hfx.
    apply add_In1.
  - destruct (f a0); [apply add_In2|idtac]; 
    apply (IHxs x); assumption.
Qed.

Lemma In_fold_add_to_In : forall {a b c : Set}
  (f : a -> option b) (g : b -> c) (add : b -> list c -> list c),
  (forall x x' xs, In (g x) (add x' xs) -> x = x' \/ In (g x) xs) ->
  (forall x x' y, f x = Some y -> f x' = Some y -> x = x') ->
  forall (xs : list a) (x : a) (y : b),
  f x = Some y ->
  In (g y) (fold_right
    (fun x' acc =>
          match f x' with
          | Some y' => add y' acc
          | None => acc
          end) xs []) -> In x xs.
  induction xs; intros.
  - exact H2.
  - destruct (f a0) eqn:G.
    + simpl in H2.
      rewrite G in H2.
      destruct (H _ _ _ H2).
      * rewrite H3 in H1; left.
        eapply H0; eassumption.
      * right; eapply IHxs; eassumption.
    + right; eapply IHxs; auto.
      * exact H1.
      * simpl in H2.
        rewrite G in H2.
        exact H2.
Qed.

(** When [fold_left_e] encounters an element yielding an error,
    the entire fold yields that error *)
Lemma fold_left_e_short_circuit : forall
  {a b tr : Set} (f : a -> b -> result a tr)
  x y t ys,
  f x y = Pervasives.Error t ->
  fold_left_e f x (y :: ys) = Pervasives.Error t.
Proof.
  intros.
  unfold fold_left_e; simpl.
  rewrite H.
  induction ys; auto.
Qed.

(** If a function preserves a property, then folding with that function
    preserves that property *)
Lemma fold_left_e_lemma {A B : Set}
  (f : A -> B -> M? A)
  (xs : list B) : forall
  (a1 a2 : A)
  (P : A -> Prop)
  (fP : forall a1 a2 b, P a1 -> f a1 b = Pervasives.Ok a2 -> P a2),
  fold_left_e f a1 xs = Pervasives.Ok a2 -> P a1 -> P a2.
  Proof.
    induction xs; intros a1 a2 P fP Hf.
    { inversion Hf; congruence. }
    { destruct (f a1 a) eqn:F.
      { unfold fold_left_e in *.
        intro.
        simpl in Hf.
        eapply IHxs; auto.
        { rewrite F in Hf; eauto. }
        { eapply fP; eauto. }
      }
      { erewrite fold_left_e_short_circuit in Hf;
        sauto.
      }
    }
  Qed.

(** When the list has a length in [int] then the [List.fold_left_i] operation
    behaves as if its index was in [Z]. *)
Lemma fold_left_i_in_int {a b : Set} f (acc : a) (l : list b) :
  Int_length.t l ->
  List.fold_left_i f acc l =
  snd (
    fold_left
      (fun '(index, acc) item =>
        let acc := f index acc item in
        ((index + 1)%Z, acc)
      )
      (0, acc)
      l
  ).
Proof.
  intros.
  unfold List.fold_left_i; f_equal; [hauto lq: on|].
  match goal with
  | |- List.fold_left ?f1 _ _ = List.fold_left ?f2 _ _ =>
    assert (H_aux : forall l index acc,
      0 <= index ->
      Pervasives.Int.Valid.t (index +Z Z.of_nat (Lists.List.length l)) ->
      List.fold_left f1 (index, acc) l = List.fold_left f2 (index, acc) l
    )
  end. {
    clear acc l H.
    induction l; simpl; intros; [reflexivity|].
    replace (index +i 1) with (index +Z 1) by lia.
    apply IHl; lia.
  }
  apply H_aux; lia.
Qed.

Lemma hd_Some_to_cons : forall {a : Set} (x : a) xs, hd xs = Some x ->
  exists ys, xs = x :: ys.
  intros X x [|y ys] Hxs.
  - discriminate.
  - exists ys; auto.
    inversion Hxs; reflexivity.
Qed.

Lemma rev_append_last : forall (a : Set) (l l' : list a),
    CoqOfOCaml.List.rev_append l l' =
      (CoqOfOCaml.List.rev_append l [] ++ l')%list.
Proof.
  intros a l l'; revert l';
  induction l as [|x l IHl]; intro l'; [reflexivity |];
  simpl; rewrite IHl, (IHl [x]), <- app_assoc; reflexivity.
Qed.

Lemma rev_head_app_eq {a : Set} (acc l : list a) (x : a) :
    (List.rev (x :: acc) ++ l)%list = (List.rev acc ++ x :: l)%list.
Proof.
  intros; unfold List.rev, Lists.List.rev'; simpl.
  rewrite List.rev_append_rev, List.rev_alt.
  rewrite <- app_assoc; reflexivity.
Qed.

Lemma rev_app_eq {a : Set} (l : list a) (x : a) :
  (List.rev (x :: l))%list = (List.rev l ++ [x])%list.
Proof.
  specialize (rev_head_app_eq l [] x) as Hl.
  rewrite app_nil_r in Hl; auto.
Qed.  

(** We create auxillary definition z_length and prove that 
our z_length equals to length.

Why do we need z_length? With it we do not have to work with modular arithmetic,
we work with integers and it is much easier to prove something
on integers. 

Recomendation : when you face (length l) in the lemma to be proved,
just rewrite length with z_length by length_z_length_eq. *)

Fixpoint z_length {a : Set} (l : list a) {struct l} : Z.t :=
  match l with
  | [] => 0
  | _ :: l' => ((z_length l') + 1)%Z
  end.

Lemma Z_le_z_length {a : Set} (l : list a) : 0 <= z_length l.
Proof.
  induction l; simpl; lia.
Qed.

Lemma z_length_plus_one : forall {a : Set} (l : list a),
      ((z_length l + 1)%Z <=i 0) = false.
Proof.
  intros a l; assert (0 <= z_length l) by apply Z_le_z_length; lia.
Qed.

Lemma length_z_length_eq : forall (a : Set) (l : list a),
    z_length l <= Pervasives.max_int ->
    List.length l = z_length l.
Proof.
  intros a l H. 
  induction l; simpl; trivial;
  unfold op_plus;
  simpl in H.
  assert (G : z_length l <= Pervasives.max_int) by lia.
  apply IHl in G. rewrite G.
  rewrite Pervasives.normalize_identity. reflexivity.
  split. assert (G' : 0 <= z_length l) by apply Z_le_z_length.
  lia. trivial.
Qed.

Lemma z_length_plus_one_minus_one : forall {a : Set} (l : list a),
    z_length l <= Pervasives.max_int ->
    (z_length l + 1)%Z -i 1 = z_length l.
Proof.
  intros a l H;
  assert (G : 0 <= z_length l) by apply Z_le_z_length;
  unfold op_minus; rewrite Pervasives.normalize_identity; lia. 
Qed.

Lemma length_map {a b : Set} (f : a -> b) (xs : list a) :
  List.length (List.map f xs) = List.length xs.
  induction xs.
  - reflexivity.
  - simpl.
    repeat (rewrite length_cons).
    congruence.
Qed.

(** The [equal] function over the lists is valid as long as its parameter [eqb]
    is valid too. *)
Lemma equal_is_valid {a : Set} (domain : a -> Prop) (eqb : a -> a -> bool)
  (l1 l2 : list a) :
  Compare.Equal.Valid.t domain eqb ->
  Compare.Equal.Valid.t (List.Forall domain) (List.equal eqb).
Proof.
  unfold Equal.Valid.t.
  intros H_eqb x y H_x H_y.
  split; generalize y H_y; clear y H_y.
  { induction x; simpl; intros; destruct y; trivial; try discriminate.
    sauto lq: on.
  }
  { induction x; simpl; intros; destruct y; trivial; try discriminate.
    match goal with
    | H : _ |- _ => destruct (andb_prop _ _ H)
    end.
    sauto lq: on rew: off.
  }
Qed.

Module Sorted.
  Lemma filter_map {a b : Set}
    (R_a : a -> a -> Prop) (R_b : b -> b -> Prop)
    (Htr : forall x y z, R_a x y -> R_a y z -> R_a x z)
    (f : a -> option b) (l : list a) :
    (forall x1 x2,
      match f x1, f x2 with
      | Some y1, Some y2 => R_a x1 x2 -> R_b y1 y2
      | _, _ => True
      end) ->
    Sorting.Sorted.Sorted R_a l ->
    Sorting.Sorted.Sorted R_b (List.filter_map f l).
  Proof.
    intro H.
    induction l as [|x]; simpl; auto; intro Hl.
    inversion_clear Hl.
    specialize (IHl H0).
    specialize (H x).
    destruct (f x); auto.
    induction l as [|y]; simpl; auto.
    simpl in IHl.
    inversion_clear H1.
    inversion_clear H0.
    specialize (H y).
    destruct (f y); auto.
    apply IHl0; auto.
    clear IHl IHl0  H b0.
    destruct l; auto.
    inversion_clear H1; inversion_clear H3.
    constructor.
    apply Htr with y; auto.
  Qed.
End Sorted.

Lemma filter_map_In_invert {A B : Set}
  {f : A -> M* B} {xs : list A} :
  forall {y}, In y (filter_map f xs) ->
  exists x, In x xs /\ f x = Some y.
Proof.
  induction xs; hauto.
Qed.

Lemma filter_map_In {A B : Set}
  {f : A -> M* B} {xs : list A} :
  forall x y, In x xs -> f x = Some y ->
  In y (filter_map f xs).
Proof.
  induction xs; intros.
  { destruct H. }
  { simpl.
    destruct H.
    { rewrite <- H in H0.
      rewrite H0.
      left; reflexivity.
    }
    { destruct (f a).
      { right; eapply IHxs; eauto. }
      { eapply IHxs; eauto. }
    }
  }
Qed.

Lemma In_map_invert {A B : Set} {f : A -> B}
  {xs : list A} : forall {y}, In y (List.map f xs) ->
  exists x, f x = y /\ In x xs.
Proof.
  induction xs; intros y Hin; destruct Hin.
  { exists a; hauto. }
  { destruct (IHxs y H) as [x [Hx1 Hx2]].
    exists x; hauto.
  }
Qed.

(** [fold_left g] after [map f] is equivalent to [fold_left (g . f)] *)
Lemma fold_left_map_eq
  {A B C: Set} (f1 : C -> B -> C) (f2 : A -> B) 
  (init : C) (l : list A) :
  fold_left f1 init (List.map f2 l) = 
  fold_left (fun acc x => f1 acc (f2 x)) init l.
Proof.
  generalize init; clear init.
  induction l; sfirstorder.
Qed.

(** The same as the above but for [fold_left_e] *)
Lemma fold_left_e_map_eq {A B C: Set}
  (f1 : C -> B -> M? C) (f2 : A -> B) 
  (init : C) (l : list A) :
  fold_left_e f1 init (List.map f2 l) = 
  fold_left_e (fun acc x => f1 acc (f2 x)) init l.
Proof.
  unfold fold_left_e.
  now rewrite fold_left_map_eq.
Qed.

(** Same as [fold_left_map_eq] but for [fold_left_es] *)
Lemma fold_left_es_map_eq {A B C: Set}
  (f1 : C -> B -> M? C) (f2 : A -> B) 
  (init : C) (l : list A) :
  fold_left_es f1 init (List.map f2 l) = 
  fold_left_es (fun acc x => f1 acc (f2 x)) init l.
Proof.
  rewrite fold_left_e_map_eq; auto.
Qed.  

(** CoqOfOcaml [fold_right] is equivalent to Coq [fold_right] *)
Lemma fold_right_eq {A B : Set}
  (f : A -> B -> B) l acc :
  fold_right f l acc = Coq.Lists.List.fold_right f acc l.
Proof.
  induction l; sfirstorder.
Qed.

(** CoqOfOcaml [rev] is equivalent to Coq [rev] *)
Lemma rev_eq {A : Set} (l : list A) :
  rev l = Coq.Lists.List.rev l.
Proof.
  unfold rev, rev'.
  now erewrite rev_alt.
Qed.

(** [fold_left] is equivalent to [fold_right] on the reversed list *)
Lemma fold_left_rev_right_eq {A B : Set}
  (f : A -> B -> A)
  (l : list B) (acc : A) :
  fold_left f acc l = fold_right (fun x acc => f acc x) (rev l) acc.
Proof.
  unfold fold_left. rewrite fold_right_eq.
  rewrite <- fold_left_rev_right.
  rewrite rev_eq. induction l; sfirstorder.
Qed.

(** [fold_right f (map g l)] is the same as [fold_right (fun x acc => (f (g x) acc)) l acc] *)
Lemma fold_right_map_eq {A B C : Set} (f : B -> C -> C) (l : list A) (acc : C)
  (g : A -> B) :
  fold_right f (List.map g l) acc =
  fold_right (fun x acc => (f (g x) acc)) l acc.
Proof.
  induction l; sfirstorder.
Qed.

(** Apply [f] to each element of a list and reverse it is
    the same of reverse it and then apply [f]. *)
Lemma rev_map_eq {a b : Set} (f : a -> b) l :
  List.rev (List.map f l) = List.map f (List.rev l).
Proof.
  induction l; auto; simpl.
  repeat rewrite rev_app_eq; rewrite IHl.
  unfold List.map; rewrite map_app; auto.
Qed.

(** [rev] preverves [P] in the list [l] *)
Lemma Forall_P_rev {a : Set} P (l : list a) :
  Forall P l ->
  Forall P (rev l).
Proof.
  induction l; simpl; auto.
  inversion_clear 1.
  rewrite rev_app_eq.
  apply Forall_app; split; auto.
Qed.

(** If [P] holds for all elements in [x :: l] it holds for [l] *)
Lemma Forall_cons {a : Set} P x (l : list a) :
  Forall P (x :: l) ->
  Forall P l. 
Proof.
  sauto lq: on dep: on.
Qed.

(** If [P] holds forall elements in [x :: l] it holds for [x] *)
Lemma Forall_head {a : Set} P x (l : list a) :
  Forall P (x :: l) ->
  P x.
Proof.
  sauto lq: on.
Qed.
