Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.

Require TezosOfOCaml.Environment.V6.Proofs.S.

Axiom Make_is_valid
  : forall (R : Blake2B.Register) (N : Blake2B.PrefixedName),
    S.HASH.Valid.t (fun _ => True) (Blake2B.Make R N).
