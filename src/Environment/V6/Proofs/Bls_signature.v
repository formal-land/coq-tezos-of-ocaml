Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.

Module Valid.
  Definition t (x :  Bls_signature.signature) : Prop :=
    Bytes.length (signature_to_bytes x) = signature_size_in_bytes.
End Valid. 

Axiom signature_valid_of_bytes_opt_to_bytes :
  forall (x : Bls_signature.signature),
    Valid.t x ->
    signature_of_bytes_opt (signature_to_bytes x) = Some x.
