Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.

Require TezosOfOCaml.Environment.V6.Proofs.Compare.
Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Environment.V6.Proofs.Int32.

Module Int32.
  Module BOUNDS.
    Import Environment.V6.Bounded.Int32.BOUNDS.

    (** The validity condition for [BOUNDS]. *)
    Module Valid.
      Record t (B : Bounded.Int32.BOUNDS) : Prop := {
        min_int : Int32.Valid.t B.(min_int);
        max_int : Int32.Valid.t B.(max_int);
      }.
    End Valid.
  End BOUNDS.

  Module S.
    Import Environment.V6.Bounded.Int32.S.

    Definition to_BOUNDS {t}
      (B : Bounded.Int32.S (t := t))
      : Bounded.Int32.BOUNDS := {|
        Bounded.Int32.BOUNDS.min_int := B.(min_int);
        Bounded.Int32.BOUNDS.max_int := B.(max_int);
      |}.

    Definition to_Compare_S {t} (B : Bounded.Int32.S (t := t)) 
      : Compare.S (t := t) := {|
        Compare.S.op_eq := B.(Bounded.Int32.S.op_eq);
        Compare.S.op_ltgt := B.(Bounded.Int32.S.op_ltgt);
        Compare.S.op_lt := B.(Bounded.Int32.S.op_lt);
        Compare.S.op_lteq := B.(Bounded.Int32.S.op_lteq);
        Compare.S.op_gteq := B.(Bounded.Int32.S.op_gteq);
        Compare.S.op_gt := B.(Bounded.Int32.S.op_gt);
        Compare.S.compare := B.(Bounded.Int32.S.compare);
        Compare.S.equal := B.(Bounded.Int32.S.equal);
        Compare.S.max := B.(Bounded.Int32.S.max);
        Compare.S.min := B.(Bounded.Int32.S.min);
      |}.

    (** The validity condition for [S] *)
    Module Valid.
      Record t {t} (B : Bounded.Int32.S (t := t)) : Prop := {
        BOUNDS : BOUNDS.Valid.t (to_BOUNDS B);
        Compare_S : Compare.S.Valid.t (to_Compare_S B);
        encoding : Data_encoding.Valid.t 
          (fun _ => True) B.(Bounded.Int32.S.encoding);
        to_of_int32 i b :
          B.(Bounded.Int32.S.of_int32) i = Some b ->
          B.(Bounded.Int32.S.to_int32) b = i;
        of_to_int32 b : 
          B.(Bounded.Int32.S.of_int32) 
            (B.(Bounded.Int32.S.to_int32) b) = Some b;
      }.
      Arguments t {_}.
    End Valid.
  End S.
  (** The [Make] axiom creates valid bounds. *)
  Axiom Make_is_valid
    : forall (B : Bounded.Int32.BOUNDS),
    Int32.BOUNDS.Valid.t B ->
    Int32.S.Valid.t (Bounded.Int32.Make B).
End Int32.

