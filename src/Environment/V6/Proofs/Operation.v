Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.

Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.

Axiom shell_header_encoding_is_valid
  : Data_encoding.Valid.t (fun _ => True) Operation.shell_header_encoding.
#[global] Hint Resolve shell_header_encoding_is_valid : Data_encoding_db.
