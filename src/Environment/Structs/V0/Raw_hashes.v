Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Parameter blake2b : bytes -> bytes.

Parameter sha256 : bytes -> bytes.

Parameter sha512 : bytes -> bytes.

Parameter keccak256 : bytes -> bytes.

Parameter sha3_256 : bytes -> bytes.

Parameter sha3_512 : bytes -> bytes.
