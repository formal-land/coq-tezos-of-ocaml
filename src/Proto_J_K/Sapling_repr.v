Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Sapling_repr.
Require TezosOfOCaml.Proto_K.Sapling_repr.

Module Old := TezosOfOCaml.Proto_J.Sapling_repr.
Module New := TezosOfOCaml.Proto_K.Sapling_repr.

(** Migrate [diff]. *)
Definition migrate_diff (x : Old.diff) : New.diff :=
  {|
    New.diff.commitments_and_ciphertexts :=
      x.(Old.diff.commitments_and_ciphertexts);
    New.diff.nullifiers := x.(Old.diff.nullifiers);
  |}.
