Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_J.Seed_repr.
Require TezosOfOCaml.Proto_K.Seed_repr.

Module Old := TezosOfOCaml.Proto_J.Seed_repr.
Module New := TezosOfOCaml.Proto_K.Seed_repr.

(** Migrate [Seed_repr.seed] *)
Definition migrate_seed '(Old.B sh as seed : Old.seed) : New.seed := New.B sh.
