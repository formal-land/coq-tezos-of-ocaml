Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Simulations.Script_family.
Require TezosOfOCaml.Proto_K.Simulations.Script_family.

Require TezosOfOCaml.Proto_J_K.Script_int_repr.
Require TezosOfOCaml.Proto_J_K.Script_typed_ir.
Require TezosOfOCaml.Proto_J_K.Script_string_repr.
Require TezosOfOCaml.Proto_J_K.Sapling_storage.

Module Old := TezosOfOCaml.Proto_J.Simulations.Script_family.
Module New := TezosOfOCaml.Proto_K.Simulations.Script_family.

Module Ty.
  Module Num.
    (** Migrate [Ty.Num.t]. *)
    Definition migrate (num : Old.Ty.Num.t) : New.Ty.Num.t :=
      match num with
      | Old.Ty.Num.Int => New.Ty.Num.Int
      | Old.Ty.Num.Nat => New.Ty.Num.Nat
      end.
  End Num.

  (** Migrate [Ty.t]. *)
  Fixpoint migrate (t : Old.Ty.t) : New.Ty.t :=
    match t with
    | Old.Ty.Unit => New.Ty.Unit
    | Old.Ty.Num num => New.Ty.Num (Num.migrate num)
    | Old.Ty.Signature => New.Ty.Signature
    | Old.Ty.String => New.Ty.String
    | Old.Ty.Bytes => New.Ty.Bytes
    | Old.Ty.Mutez => New.Ty.Mutez
    | Old.Ty.Key_hash => New.Ty.Key_hash
    | Old.Ty.Key => New.Ty.Key
    | Old.Ty.Timestamp => New.Ty.Timestamp
    | Old.Ty.Address => New.Ty.Address
    | Old.Ty.Tx_rollup_l2_address => New.Ty.Tx_rollup_l2_address
    | Old.Ty.Bool => New.Ty.Bool
    | Old.Ty.Pair t1 t2 => New.Ty.Pair (migrate t1) (migrate t2)
    | Old.Ty.Union t1 t2 => New.Ty.Union (migrate t1) (migrate t2)
    | Old.Ty.Lambda t1 t2 => New.Ty.Lambda (migrate t1) (migrate t2)
    | Old.Ty.Option t => New.Ty.Option (migrate t)
    | Old.Ty.List t => New.Ty.List (migrate t)
    | Old.Ty.Set_ t => New.Ty.Set_ (migrate t)
    | Old.Ty.Map t1 t2 => New.Ty.Map (migrate t1) (migrate t2)
    | Old.Ty.Big_map t1 t2 => New.Ty.Big_map (migrate t1) (migrate t2)
    | Old.Ty.Contract t => New.Ty.Contract (migrate t)
    | Old.Ty.Sapling_transaction => New.Ty.Sapling_transaction
    | Old.Ty.Sapling_transaction_deprecated =>
      New.Ty.Sapling_transaction_deprecated
    | Old.Ty.Sapling_state => New.Ty.Sapling_state
    | Old.Ty.Operation => New.Ty.Operation
    | Old.Ty.Chain_id => New.Ty.Chain_id
    | Old.Ty.Never => New.Ty.Never
    | Old.Ty.Bls12_381_g1 => New.Ty.Bls12_381_g1
    | Old.Ty.Bls12_381_g2 => New.Ty.Bls12_381_g2
    | Old.Ty.Bls12_381_fr => New.Ty.Bls12_381_fr
    | Old.Ty.Ticket t => New.Ty.Ticket (migrate t)
    | Old.Ty.Chest_key => New.Ty.Chest_key
    | Old.Ty.Chest => New.Ty.Chest
    end.

  (** Migrate [to_Set]. *)
   Fixpoint migrate_to_Set {t : Old.Ty.t} : Old.Ty.to_Set t -> New.Ty.to_Set
    (Ty.migrate t) :=
    match t with
    | Old.Ty.Unit => fun x => x
    | Old.Ty.Num _ => Script_int_repr.migrate
    | Old.Ty.Signature => Script_typed_ir.Script_signature.migrate
    | Old.Ty.String => Script_string_repr.migrate
    | Old.Ty.Bytes => fun x => x
    | Old.Ty.Mutez => Tez_repr.migrate
    | Old.Ty.Key_hash => fun x => x
    | Old.Ty.Key => fun x => x
    | Old.Ty.Timestamp => Script_timestamp_repr.migrate
    | Old.Ty.Address => Script_typed_ir.migrate_address
    | Old.Ty.Tx_rollup_l2_address =>
      Script_typed_ir.migrate_tx_rollup_l2_address
    | Old.Ty.Bool => fun x => x
    | Old.Ty.Pair ty1 ty2 => fun '(x1, x2) =>
      (@migrate_to_Set ty1 x1, @migrate_to_Set ty2 x2)
    | Old.Ty.Union ty1 ty2 =>
      Script_typed_ir.migrate_union migrate_to_Set migrate_to_Set
    | Old.Ty.Lambda ty_arg ty_res => axiom "TODO: Lambda"
    | Old.Ty.Option ty => fun x =>
      match x with
      | None => None
      | Some x => Some (@migrate_to_Set ty x)
      end
    | Old.Ty.List ty => Script_typed_ir.migrate_boxed_list migrate_to_Set
    | Old.Ty.Set_ ty_k => axiom "TODO: Set_"
    | Old.Ty.Map tyk _ => axiom "TODO: Map"
    | Old.Ty.Big_map _ _ => axiom "TODO: Big_map"
    | Old.Ty.Contract ty => axiom "TODO: Contract"
    | Old.Ty.Sapling_transaction => fun x => x
    | Old.Ty.Sapling_transaction_deprecated => fun x => x
    | Old.Ty.Sapling_state => Sapling_storage.migrate_state
    | Old.Ty.Operation => Script_typed_ir.migrate_operation
    | Old.Ty.Chain_id => Script_typed_ir.Script_chain_id.migrate
    | Old.Ty.Never => fun x => match x with end
    | Old.Ty.Bls12_381_g1 => Script_typed_ir.Script_bls.G1.migrate
    | Old.Ty.Bls12_381_g2 => Script_typed_ir.Script_bls.G2.migrate
    | Old.Ty.Bls12_381_fr => Script_typed_ir.Script_bls.Fr.migrate
    | Old.Ty.Ticket ty => Script_typed_ir.migrate_ticket migrate_to_Set
    | Old.Ty.Chest_key => Script_typed_ir.Script_timelock.migrate_chest_key
    | Old.Ty.Chest => Script_typed_ir.Script_timelock.migrate_chest
    end.
End Ty.

Module Stack_ty.
  (** Migrate [Stack_ty.t]. *)
  Fixpoint migrate (ts : Old.Stack_ty.t) : New.Stack_ty.t :=
    match ts with
    | [] => []
    | t :: ts => Ty.migrate t :: migrate ts
    end.
End Stack_ty.
