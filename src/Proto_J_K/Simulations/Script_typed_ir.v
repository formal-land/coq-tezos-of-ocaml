Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Simulations.Script_typed_ir.
Require TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.
Require Import TezosOfOCaml.Proto_J_K.Simulations.Script_family.
Require TezosOfOCaml.Proto_J_K.Dependent_bool.
Require TezosOfOCaml.Proto_J_K.Sapling_storage.
Require TezosOfOCaml.Proto_J_K.Script_repr.
Require TezosOfOCaml.Proto_J_K.Script_string_repr.
Require TezosOfOCaml.Proto_J_K.Script_timestamp_repr.
Require TezosOfOCaml.Proto_J_K.Script_typed_ir.
Require TezosOfOCaml.Proto_J_K.Tez_repr.

Module Old := TezosOfOCaml.Proto_J.Simulations.Script_typed_ir.
Module New := TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.

Module With_family.
  (** Migrate [comb_gadt_witness]. *)
  Fixpoint migrate_comb_gadt_witness {s f}
    (w : Old.With_family.comb_gadt_witness s f) :
    New.With_family.comb_gadt_witness
      (Stack_ty.migrate s) (Stack_ty.migrate f) :=
    match w with
    | Old.With_family.Comb_one => New.With_family.Comb_one
    | Old.With_family.Comb_succ w' =>
      New.With_family.Comb_succ (migrate_comb_gadt_witness w')
    end.

  (** Migrate [uncomb_gadt_witness]. *)
  Fixpoint migrate_uncomb_gadt_witness {s f}
    (w : Old.With_family.uncomb_gadt_witness s f) :
    New.With_family.uncomb_gadt_witness
      (Stack_ty.migrate s) (Stack_ty.migrate f) :=
    match w with
    | Old.With_family.Uncomb_one => New.With_family.Uncomb_one
    | Old.With_family.Uncomb_succ w' =>
      New.With_family.Uncomb_succ (migrate_uncomb_gadt_witness w')
    end.

  (** Migrate [comb_get_gadt_witness]. *)
  Fixpoint migrate_comb_get_gadt_witness {a b}
    (w : Old.With_family.comb_get_gadt_witness a b) :
    New.With_family.comb_get_gadt_witness (Ty.migrate a) (Ty.migrate b) :=
    match w with
    | Old.With_family.Comb_get_zero => New.With_family.Comb_get_zero
    | Old.With_family.Comb_get_one => New.With_family.Comb_get_one
    | Old.With_family.Comb_get_plus_two w' =>
      New.With_family.Comb_get_plus_two (migrate_comb_get_gadt_witness w')
    end.

  (** Migrate [comb_set_gadt_witness]. *)
  Fixpoint migrate_comb_set_gadt_witness {a b c}
    (w : Old.With_family.comb_set_gadt_witness a b c) :
    New.With_family.comb_set_gadt_witness
      (Ty.migrate a) (Ty.migrate b) (Ty.migrate c) :=
    match w with
    | Old.With_family.Comb_set_zero => New.With_family.Comb_set_zero
    | Old.With_family.Comb_set_one => New.With_family.Comb_set_one
    | Old.With_family.Comb_set_plus_two w' => New.With_family.Comb_set_plus_two
      (migrate_comb_set_gadt_witness w')
    end.

  (** Migrate [dup_n_gadt_witness]. *)
  Fixpoint migrate_dup_n_gadt_witness {b s}
    (w : Old.With_family.dup_n_gadt_witness s b) :
    New.With_family.dup_n_gadt_witness (Stack_ty.migrate s) (Ty.migrate b) :=
    match w with
    | Old.With_family.Dup_n_zero => New.With_family.Dup_n_zero
    | Old.With_family.Dup_n_succ w' =>
      New.With_family.Dup_n_succ (migrate_dup_n_gadt_witness w')
    end.

  (** Migrate [ty]. *)
  Fixpoint migrate_ty {a} (ty : Old.With_family.ty a) :
    New.With_family.ty (Ty.migrate a) :=
    match
      ty
      in Old.With_family.ty a
      return New.With_family.ty (Ty.migrate a)
    with
    | Old.With_family.Unit_t => New.With_family.Unit_t
    | Old.With_family.Int_t => New.With_family.Int_t
    | Old.With_family.Nat_t => New.With_family.Nat_t
    | Old.With_family.Signature_t => New.With_family.Signature_t
    | Old.With_family.String_t => New.With_family.String_t
    | Old.With_family.Bytes_t => New.With_family.Bytes_t
    | Old.With_family.Mutez_t => New.With_family.Mutez_t
    | Old.With_family.Key_hash_t => New.With_family.Key_hash_t
    | Old.With_family.Key_t => New.With_family.Key_t
    | Old.With_family.Timestamp_t => New.With_family.Timestamp_t
    | Old.With_family.Address_t => New.With_family.Address_t
    | Old.With_family.Tx_rollup_l2_address_t =>
      New.With_family.Tx_rollup_l2_address_t
    | Old.With_family.Bool_t => New.With_family.Bool_t
    | Old.With_family.Pair_t ty1 ty2 m b =>
      New.With_family.Pair_t
        (migrate_ty ty1) (migrate_ty ty2)
        (Script_typed_ir.migrate_ty_metadata m)
        (Dependent_bool.migrate_dand b)
    | Old.With_family.Union_t ty1 ty2 m b =>
      New.With_family.Union_t
        (migrate_ty ty1) (migrate_ty ty2)
        (Script_typed_ir.migrate_ty_metadata m)
        (Dependent_bool.migrate_dand b)
    | Old.With_family.Lambda_t ty1 ty2 m =>
      New.With_family.Lambda_t
        (migrate_ty ty1) (migrate_ty ty2)
        (Script_typed_ir.migrate_ty_metadata m)
    | Old.With_family.Option_t ty m b =>
      New.With_family.Option_t
        (migrate_ty ty)
        (Script_typed_ir.migrate_ty_metadata m)
        (Dependent_bool.migrate_dbool b)
    | Old.With_family.List_t ty m =>
      New.With_family.List_t
        (migrate_ty ty)
        (Script_typed_ir.migrate_ty_metadata m)
    | Old.With_family.Set_t ty m =>
      New.With_family.Set_t
        (migrate_ty ty)
        (Script_typed_ir.migrate_ty_metadata m)
    | Old.With_family.Map_t ty1 ty2 m =>
      New.With_family.Map_t
        (migrate_ty ty1) (migrate_ty ty2)
        (Script_typed_ir.migrate_ty_metadata m)
    | Old.With_family.Big_map_t ty1 ty2 m =>
      New.With_family.Big_map_t
        (migrate_ty ty1) (migrate_ty ty2)
        (Script_typed_ir.migrate_ty_metadata m)
    | Old.With_family.Contract_t ty m =>
      New.With_family.Contract_t
        (migrate_ty ty)
        (Script_typed_ir.migrate_ty_metadata m)
    | Old.With_family.Sapling_transaction_t size =>
      New.With_family.Sapling_transaction_t size
    | Old.With_family.Sapling_transaction_deprecated_t size =>
      New.With_family.Sapling_transaction_deprecated_t size
    | Old.With_family.Sapling_state_t size =>
      New.With_family.Sapling_state_t size
    | Old.With_family.Operation_t => New.With_family.Operation_t
    | Old.With_family.Chain_id_t => New.With_family.Chain_id_t
    | Old.With_family.Never_t => New.With_family.Never_t
    | Old.With_family.Bls12_381_g1_t => New.With_family.Bls12_381_g1_t
    | Old.With_family.Bls12_381_g2_t => New.With_family.Bls12_381_g2_t
    | Old.With_family.Bls12_381_fr_t => New.With_family.Bls12_381_fr_t
    | Old.With_family.Ticket_t ty m =>
      New.With_family.Ticket_t
        (migrate_ty ty)
        (Script_typed_ir.migrate_ty_metadata m)
    | Old.With_family.Chest_key_t => New.With_family.Chest_key_t
    | Old.With_family.Chest_t => New.With_family.Chest_t
    end.

  (** Migrate [stack_ty]. *)
  Fixpoint migrate_stack_ty {s} (ts : Old.With_family.stack_ty s) :
    New.With_family.stack_ty (Stack_ty.migrate s) :=
    match ts with
    | Old.With_family.Item_t t rest =>
      New.With_family.Item_t (migrate_ty t) (migrate_stack_ty rest)
    | Old.With_family.Bot_t => New.With_family.Bot_t
    end.

  (** Extract the location field from [kinfo]. *)
  Definition kinfo_to_location {s} (x : Old.With_family.kinfo s) :
    Proto_K.Alpha_context.Script.location :=
    match x with
    | Old.With_family.Kinfo loc _ => loc
    end.

  (** Extract the stack ty field from [kinfo]. *)
  Definition kinfo_to_stack_ty {s} (x : Old.With_family.kinfo s) :
    New.With_family.stack_ty (Stack_ty.migrate s) :=
    match x with
    | Old.With_family.Kinfo _ sty => migrate_stack_ty sty
    end.

  Definition stack_ty_head {a s} (sty : Old.With_family.stack_ty (a :: s)) :
    Old.With_family.ty a :=
    match sty with
    | Old.With_family.Item_t t _ => t
    end.

  Definition stack_ty_tail {a s} (sty : Old.With_family.stack_ty (a :: s)) :
    Old.With_family.stack_ty s :=
    match sty with
    | Old.With_family.Item_t _ sty => sty
    end.

  (** Migrate [stack_prefix_preservation_witness]. *)
  Fixpoint migrate_stack_prefix_preservation_witness {s t u v}
    (sty : Old.With_family.stack_ty u)
    (w : Old.With_family.stack_prefix_preservation_witness s t u v) :
    New.With_family.stack_prefix_preservation_witness
      (Stack_ty.migrate s)
      (Stack_ty.migrate t)
      (Stack_ty.migrate u)
      (Stack_ty.migrate v) :=
    match
      w in Old.With_family.stack_prefix_preservation_witness s t u v,
      sty
    with
    | Old.With_family.KRest, _ => New.With_family.KRest
    | Old.With_family.KPrefix kinfo w', _ =>
      New.With_family.KPrefix
        (kinfo_to_location kinfo)
        (migrate_ty (stack_ty_head sty))
        (migrate_stack_prefix_preservation_witness (stack_ty_tail sty) w')
    end.

  (** Migrate [view_signature]. *)
  Definition migrate_view_signature {a b}
    (signature : Old.With_family.view_signature a b) :
    New.With_family.view_signature (Ty.migrate a) (Ty.migrate b) :=
    match signature with
    | Old.With_family.View_signature name input_ty output_ty =>
      New.With_family.View_signature
        (Script_string_repr.migrate name)
        (migrate_ty input_ty)
        (migrate_ty output_ty)
    end.

  (** Migrate [typed_contract]. *)
  Definition migrate_typed_contract {arg}
    (x : Old.With_family.typed_contract arg) :
    New.With_family.typed_contract (Ty.migrate arg) :=
    match x with
    | Old.With_family.Typed_contract ty address =>
      New.With_family.Typed_contract
        (migrate_ty ty) (Script_typed_ir.migrate_address address)
    end.

  (** Migrate [set]. *)
  Definition migrate_set {a} (x : Old.With_family.set a) :
    New.With_family.set (Ty.migrate a) :=
    List.map (fun '(k, v) => (Script_family.Ty.migrate_to_Set k, v)) x.

  (** Migrate [map]. *)
  Definition migrate_map {k : Old.Ty.t} {v v' : Set}
    (migrate : v -> v') (x : Old.With_family.map k v) :
    New.With_family.map (Ty.migrate k) v' :=
    List.map (fun '(k, v) =>
      (Script_family.Ty.migrate_to_Set k, migrate v)) x.

  (** Migrate [big_map_overlay]. *)
  Definition migrate_big_map_overlay {a b}
    (migrate_a : Old.With_family.ty_to_dep_Set a ->
      New.With_family.ty_to_dep_Set (Ty.migrate a))
    (migrate_b : Old.With_family.ty_to_dep_Set b ->
      New.With_family.ty_to_dep_Set (Ty.migrate b))
    (diff : Proto_J.Script_typed_ir.big_map_overlay
         (Old.With_family.ty_to_dep_Set a)
         (Old.With_family.ty_to_dep_Set b)) :
    Script_typed_ir.big_map_overlay
      (New.With_family.ty_to_dep_Set (Ty.migrate a))
      (New.With_family.ty_to_dep_Set (Ty.migrate b)) := {|
    Script_typed_ir.big_map_overlay.map :=
      List.map (fun '(k, (a, ob)) => (k, (migrate_a a,
        match ob with
        | Some b => Some (migrate_b b)
        | None => None
        end)))
      (Proto_J.Script_typed_ir.big_map_overlay.map diff);
    Script_typed_ir.big_map_overlay.size :=
      Proto_J.Script_typed_ir.big_map_overlay.size diff
    |}.

  (** Migrate [big_map_skeleton]. *)
  Definition migrate_big_map_skeleton {a b}
    (migrate_a : Old.With_family.ty_to_dep_Set a ->
      New.With_family.ty_to_dep_Set (Ty.migrate a))
    (migrate_b : Old.With_family.ty_to_dep_Set b ->
      New.With_family.ty_to_dep_Set (Ty.migrate b))
    (skel : Old.With_family.big_map.skeleton a b
      (Old.With_family.ty_to_dep_Set a)
      (Old.With_family.ty_to_dep_Set b)) :
    New.With_family.big_map.skeleton (Ty.migrate a) 
      (Ty.migrate b) (New.With_family.ty_to_dep_Set (Ty.migrate a))
      (New.With_family.ty_to_dep_Set (Ty.migrate b)) := {|
    New.With_family.big_map.id := Old.With_family.big_map.id skel;
    New.With_family.big_map.diff := migrate_big_map_overlay
      migrate_a migrate_b
      (Old.With_family.big_map.diff skel);
    New.With_family.big_map.key_type :=
      migrate_ty (Old.With_family.big_map.key_type skel);
    New.With_family.big_map.value_type :=
      migrate_ty (Old.With_family.big_map.value_type skel);
    |}.

  (** Migrate [kinstr]. *)
  #[bypass_check(guard)]
  Fixpoint migrate_kinstr {s f} (i : Old.With_family.kinstr s f) {struct i} :
    New.With_family.kinstr (Stack_ty.migrate s) (Stack_ty.migrate f) :=
    match
      i
      in Old.With_family.kinstr s f
      return New.With_family.kinstr (Stack_ty.migrate s) (Stack_ty.migrate f)
    with
    (*
      Stack
      -----
    *)
    | Old.With_family.IDrop info k =>
      New.With_family.IDrop (kinfo_to_location info) (migrate_kinstr k)
    | Old.With_family.IDup info k =>
      New.With_family.IDup (kinfo_to_location info) (migrate_kinstr k)
    | Old.With_family.ISwap info k =>
      New.With_family.ISwap (kinfo_to_location info) (migrate_kinstr k)
    | Old.With_family.IConst info v k =>
      New.With_family.IConst
        (kinfo_to_location info)
        (axiom "ty")
        (migrate_value v)
        (migrate_kinstr k)
    (*
      Pairs
      -----
    *)
    | Old.With_family.ICons_pair info k =>
      New.With_family.ICons_pair (kinfo_to_location info) (migrate_kinstr k)
    | Old.With_family.ICar info k =>
      New.With_family.ICar (kinfo_to_location info) (migrate_kinstr k)
    | Old.With_family.ICdr info k =>
      New.With_family.ICdr (kinfo_to_location info) (migrate_kinstr k)
    | Old.With_family.IUnpair info k =>
      New.With_family.IUnpair (kinfo_to_location info) (migrate_kinstr k)
    (*
      Options
      -------
    *)
    | Old.With_family.ICons_some info k =>
      New.With_family.ICons_some (kinfo_to_location info) (migrate_kinstr k)
    | Old.With_family.ICons_none info k =>
      New.With_family.ICons_none
        (kinfo_to_location info) (axiom "ty") (migrate_kinstr k)
    | Old.With_family.IIf_none info branch_if_none branch_if_some k =>
      New.With_family.IIf_none
        (kinfo_to_location info)
        (migrate_kinstr branch_if_none)
        (migrate_kinstr branch_if_some)
        (migrate_kinstr k)
    | Old.With_family.IOpt_map info body k =>
      New.With_family.IOpt_map
        (kinfo_to_location info) (migrate_kinstr body) (migrate_kinstr k)
    (*
      Unions
      ------
    *)
    | Old.With_family.ICons_left info k =>
      New.With_family.ICons_left
        (kinfo_to_location info) (axiom "ty") (migrate_kinstr k)
    | Old.With_family.ICons_right info k =>
      New.With_family.ICons_right
        (kinfo_to_location info) (axiom "ty") (migrate_kinstr k)
    | Old.With_family.IIf_left info branch_if_left branch_if_right k =>
      New.With_family.IIf_left
        (kinfo_to_location info)
        (migrate_kinstr branch_if_left)
        (migrate_kinstr branch_if_right)
        (migrate_kinstr k)
    (*
      Lists
      -----
    *)
    | Old.With_family.ICons_list info k =>
      New.With_family.ICons_list (kinfo_to_location info) (migrate_kinstr k)
    | Old.With_family.INil info k =>
      New.With_family.INil
        (kinfo_to_location info) (axiom "ty") (migrate_kinstr k)
    | Old.With_family.IIf_cons info branch_if_cons branch_if_nil k =>
      New.With_family.IIf_cons
        (kinfo_to_location info)
        (migrate_kinstr branch_if_cons)
        (migrate_kinstr branch_if_nil)
        (migrate_kinstr k)
    | Old.With_family.IList_map info body k =>
      New.With_family.IList_map
        (kinfo_to_location info)
        (migrate_kinstr body)
        (axiom "ty")
        (migrate_kinstr k)
    | Old.With_family.IList_iter info body k =>
      New.With_family.IList_iter
        (kinfo_to_location info)
        (axiom "ty")
        (migrate_kinstr body)
        (migrate_kinstr k)
    | Old.With_family.IList_size info k =>
      New.With_family.IList_size (kinfo_to_location info) (migrate_kinstr k)
    (*
    Sets
    ----
    *)
    | Old.With_family.IEmpty_set info t instr =>
      New.With_family.IEmpty_set
        (kinfo_to_location info) (migrate_ty t) (migrate_kinstr instr)
    | Old.With_family.ISet_iter info instr1 instr2 =>
      New.With_family.ISet_iter
        (kinfo_to_location info)
        (axiom "ty")
        (migrate_kinstr instr1)
        (migrate_kinstr instr2)
    | Old.With_family.ISet_mem info instr =>
      New.With_family.ISet_mem (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISet_update info instr =>
      New.With_family.ISet_update (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISet_size info instr =>
      New.With_family.ISet_size (kinfo_to_location info) (migrate_kinstr instr)
    (*
      Maps
      ----
    *)
    | Old.With_family.IEmpty_map info key instr =>
      New.With_family.IEmpty_map
        (kinfo_to_location info)
        (migrate_ty key)
        (axiom "ty")
        (migrate_kinstr instr)
    | Old.With_family.IMap_map info instr1 instr2 =>
      New.With_family.IMap_map
        (kinfo_to_location info)
        (axiom "ty")
        (migrate_kinstr instr1)
        (migrate_kinstr instr2)
    | Old.With_family.IMap_iter info instr1 instr2 =>
      New.With_family.IMap_iter
        (kinfo_to_location info)
        (axiom "ty")
        (migrate_kinstr instr1)
        (migrate_kinstr instr2)
    | Old.With_family.IMap_mem info instr =>
      New.With_family.IMap_mem (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IMap_get info instr =>
      New.With_family.IMap_get (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IMap_update info instr =>
      New.With_family.IMap_update (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IMap_get_and_update info instr =>
      New.With_family.IMap_get_and_update
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IMap_size info instr =>
      New.With_family.IMap_size (kinfo_to_location info) (migrate_kinstr instr)
    (*
      Big maps
      --------
    *)
    | Old.With_family.IEmpty_big_map info key val instr =>
      New.With_family.IEmpty_big_map
        (kinfo_to_location info)
        (migrate_ty key)
        (migrate_ty val)
        (migrate_kinstr instr)
    | Old.With_family.IBig_map_mem info instr =>
      New.With_family.IBig_map_mem (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IBig_map_get info instr =>
      New.With_family.IBig_map_get (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IBig_map_update info instr =>
      New.With_family.IBig_map_update
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IBig_map_get_and_update info instr =>
      New.With_family.IBig_map_get_and_update
        (kinfo_to_location info) (migrate_kinstr instr)
    (*
      Strings
      -------
    *)
    | Old.With_family.IConcat_string info instr =>
      New.With_family.IConcat_string (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IConcat_string_pair info instr =>
      New.With_family.IConcat_string_pair
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISlice_string info instr =>
      New.With_family.ISlice_string (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IString_size info instr =>
      New.With_family.IString_size (kinfo_to_location info) (migrate_kinstr instr)
    (*
      Bytes
      -----
    *)
    | Old.With_family.IConcat_bytes info instr =>
      New.With_family.IConcat_bytes (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IConcat_bytes_pair info instr =>
      New.With_family.IConcat_bytes_pair
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISlice_bytes info instr =>
      New.With_family.ISlice_bytes (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IBytes_size info instr =>
      New.With_family.IBytes_size (kinfo_to_location info) (migrate_kinstr instr)
    (*
      Timestamps
      ----------
    *)
    | Old.With_family.IAdd_seconds_to_timestamp info instr =>
      New.With_family.IAdd_seconds_to_timestamp
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IAdd_timestamp_to_seconds info instr =>
      New.With_family.IAdd_timestamp_to_seconds
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISub_timestamp_seconds info instr =>
      New.With_family.ISub_timestamp_seconds
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IDiff_timestamps info instr =>
      New.With_family.IDiff_timestamps
        (kinfo_to_location info) (migrate_kinstr instr)
    (*
      Tez
      ---
    *)
    | Old.With_family.IAdd_tez info instr =>
      New.With_family.IAdd_tez (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISub_tez info instr =>
      New.With_family.ISub_tez (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISub_tez_legacy info instr =>
      New.With_family.ISub_tez_legacy
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IMul_teznat info instr =>
      New.With_family.IMul_teznat (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IMul_nattez info instr =>
      New.With_family.IMul_nattez (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IEdiv_teznat info instr =>
      New.With_family.IEdiv_teznat (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IEdiv_tez info instr =>
      New.With_family.IEdiv_tez (kinfo_to_location info) (migrate_kinstr instr)
    (*
      Booleans
      --------
    *)
    | Old.With_family.IOr info k =>
      New.With_family.IOr (kinfo_to_location info) (migrate_kinstr k)
    | Old.With_family.IAnd info k =>
      New.With_family.IAnd (kinfo_to_location info) (migrate_kinstr k)
    | Old.With_family.IXor info k =>
      New.With_family.IXor (kinfo_to_location info) (migrate_kinstr k)
    | Old.With_family.INot info k =>
      New.With_family.INot (kinfo_to_location info) (migrate_kinstr k)
    (*
      Integers
      --------
    *)
    | Old.With_family.IIs_nat info instr =>
      New.With_family.IIs_nat (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.INeg info instr =>
      New.With_family.INeg (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IAbs_int info instr =>
      New.With_family.IAbs_int (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IInt_nat info instr =>
      New.With_family.IInt_nat (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IAdd_int info instr =>
      New.With_family.IAdd_int (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IAdd_nat info instr =>
      New.With_family.IAdd_nat (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISub_int info instr =>
      New.With_family.ISub_int (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IMul_int info instr =>
      New.With_family.IMul_int (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IMul_nat info instr =>
      New.With_family.IMul_nat (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IEdiv_int info instr =>
      New.With_family.IEdiv_int (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IEdiv_nat info instr =>
      New.With_family.IEdiv_nat (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ILsl_nat info instr =>
      New.With_family.ILsl_nat (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ILsr_nat info instr =>
      New.With_family.ILsr_nat (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IOr_nat info instr =>
      New.With_family.IOr_nat (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IAnd_nat info instr =>
      New.With_family.IAnd_nat (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IAnd_int_nat info instr =>
      New.With_family.IAnd_int_nat (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IXor_nat info instr =>
      New.With_family.IXor_nat (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.INot_int info instr =>
      New.With_family.INot_int (kinfo_to_location info) (migrate_kinstr instr)
    (*
      Control
      -------
    *)
    | Old.With_family.IIf info branch_if_true branch_if_false k =>
      New.With_family.IIf
        (kinfo_to_location info)
        (migrate_kinstr branch_if_true)
        (migrate_kinstr branch_if_false)
        (migrate_kinstr k)
    | Old.With_family.ILoop info body k =>
      New.With_family.ILoop
        (kinfo_to_location info)
        (migrate_kinstr body)
        (migrate_kinstr k)
    | Old.With_family.ILoop_left info bl br =>
      New.With_family.ILoop_left
        (kinfo_to_location info)
        (migrate_kinstr bl)
        (migrate_kinstr br)
    | Old.With_family.IDip info b k =>
      New.With_family.IDip
        (kinfo_to_location info)
        (migrate_kinstr b)
        (axiom "ty")
        (migrate_kinstr k)
    | Old.With_family.IExec info k =>
      New.With_family.IExec
        (kinfo_to_location info)
        (axiom "stack_ty")
        (migrate_kinstr k)
    | Old.With_family.IApply info capture_ty k =>
      New.With_family.IApply
        (kinfo_to_location info)
        (migrate_ty capture_ty)
        (migrate_kinstr k)
    | Old.With_family.ILambda info lam k =>
      New.With_family.ILambda
        (kinfo_to_location info)
        (migrate_lambda lam)
        (migrate_kinstr k)
    | Old.With_family.IFailwith _ kloc tv =>
      New.With_family.IFailwith
        kloc
        (migrate_ty tv)
    (*
      Comparison
      ----------
    *)
    | Old.With_family.ICompare info t instr =>
      New.With_family.ICompare
        (kinfo_to_location info)
        (migrate_ty t)
        (migrate_kinstr instr)
    (*
      Comparators
      -----------
    *)
    | Old.With_family.IEq info instr =>
      New.With_family.IEq (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.INeq info instr =>
      New.With_family.INeq (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ILt info instr =>
      New.With_family.ILt (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IGt info instr =>
      New.With_family.IGt (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ILe info instr =>
      New.With_family.ILe (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IGe info instr =>
      New.With_family.IGe (kinfo_to_location info) (migrate_kinstr instr)
    (*
      Protocol
      --------
    *)
    | Old.With_family.IAddress info instr =>
      New.With_family.IAddress (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IContract info t str instr =>
      New.With_family.IContract
        (kinfo_to_location info)
        (migrate_ty t)
        str
        (migrate_kinstr instr)
    | Old.With_family.IView info sign instr =>
      New.With_family.IView
        (kinfo_to_location info)
        (migrate_view_signature sign)
        (axiom "stack_ty")
        (migrate_kinstr instr)
    | Old.With_family.ITransfer_tokens info instr =>
      New.With_family.ITransfer_tokens
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IImplicit_account info instr =>
      New.With_family.IImplicit_account
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ICreate_contract info storage_type code instr =>
      New.With_family.ICreate_contract
        (kinfo_to_location info)
        (migrate_ty storage_type)
        (Script_repr.migrate_expr code)
        (migrate_kinstr instr)
    | Old.With_family.ISet_delegate info instr =>
      New.With_family.ISet_delegate
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.INow info instr =>
      New.With_family.INow (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IMin_block_time info instr =>
      New.With_family.IMin_block_time
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IBalance info instr =>
      New.With_family.IBalance (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ILevel info instr =>
      New.With_family.ILevel (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ICheck_signature info instr =>
      New.With_family.ICheck_signature
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IHash_key info instr =>
      New.With_family.IHash_key (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IPack info t instr =>
      New.With_family.IPack
        (kinfo_to_location info) (migrate_ty t) (migrate_kinstr instr)
    | Old.With_family.IUnpack info t instr =>
      New.With_family.IUnpack
        (kinfo_to_location info) (migrate_ty t) (migrate_kinstr instr)
    | Old.With_family.IBlake2b info instr =>
      New.With_family.IBlake2b (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISha256 info instr =>
      New.With_family.ISha256 (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISha512 info instr =>
      New.With_family.ISha512 (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISource info instr =>
      New.With_family.ISource (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISender info instr =>
      New.With_family.ISender (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISelf info t str instr =>
      New.With_family.ISelf
        (kinfo_to_location info)
        (migrate_ty t)
        str
        (migrate_kinstr instr)
    | Old.With_family.ISelf_address info instr =>
      New.With_family.ISelf_address (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IAmount info instr =>
      New.With_family.IAmount (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISapling_empty_state info sz instr =>
      New.With_family.ISapling_empty_state
        (kinfo_to_location info)
        sz
        (migrate_kinstr instr)
    | Old.With_family.ISapling_verify_update info instr =>
      New.With_family.ISapling_verify_update
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISapling_verify_update_deprecated info instr =>
      New.With_family.ISapling_verify_update_deprecated
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IDig info v spref instr =>
      New.With_family.IDig
        (kinfo_to_location info)
        v
        (migrate_stack_prefix_preservation_witness (axiom "stack_ty") spref)
        (migrate_kinstr instr)
    | Old.With_family.IDug info v spref instr =>
      New.With_family.IDug
        (kinfo_to_location info)
        v
        (migrate_stack_prefix_preservation_witness (axiom "stack_ty") spref)
        (migrate_kinstr instr)
    | Old.With_family.IDipn info v spref instr1 instr2 =>
      New.With_family.IDipn
        (kinfo_to_location info)
        v
        (migrate_stack_prefix_preservation_witness (axiom "stack_ty") spref)
        (migrate_kinstr instr1)
        (migrate_kinstr instr2)
    | Old.With_family.IDropn info v spref instr =>
      New.With_family.IDropn
        (kinfo_to_location info)
        v
        (migrate_stack_prefix_preservation_witness (axiom "stack_ty") spref)
        (migrate_kinstr instr)
    | Old.With_family.IChainId info instr =>
      New.With_family.IChainId (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.INever info =>
      New.With_family.INever (kinfo_to_location info)
    | Old.With_family.IVoting_power info instr =>
      New.With_family.IVoting_power (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ITotal_voting_power info instr =>
      New.With_family.ITotal_voting_power
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IKeccak info instr =>
      New.With_family.IKeccak (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.ISha3 info instr =>
      New.With_family.ISha3 (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IAdd_bls12_381_g1 info instr =>
      New.With_family.IAdd_bls12_381_g1
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IAdd_bls12_381_g2 info instr =>
      New.With_family.IAdd_bls12_381_g2
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IAdd_bls12_381_fr info instr =>
      New.With_family.IAdd_bls12_381_fr
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IMul_bls12_381_g1 info instr =>
      New.With_family.IMul_bls12_381_g1
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IMul_bls12_381_g2 info instr =>
      New.With_family.IMul_bls12_381_g2
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IMul_bls12_381_fr info instr =>
      New.With_family.IMul_bls12_381_fr
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IMul_bls12_381_z_fr info instr =>
      New.With_family.IMul_bls12_381_z_fr
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IMul_bls12_381_fr_z info instr =>
      New.With_family.IMul_bls12_381_fr_z
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IInt_bls12_381_fr info instr =>
      New.With_family.IInt_bls12_381_fr
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.INeg_bls12_381_g1 info instr =>
      New.With_family.INeg_bls12_381_g1
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.INeg_bls12_381_g2 info instr =>
      New.With_family.INeg_bls12_381_g2
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.INeg_bls12_381_fr info instr =>
      New.With_family.INeg_bls12_381_fr
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IPairing_check_bls12_381 info instr =>
      New.With_family.IPairing_check_bls12_381
        (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IComb info v comb instr =>
      New.With_family.IComb
        (kinfo_to_location info)
        v
        (migrate_comb_gadt_witness comb)
        (migrate_kinstr instr)
    | Old.With_family.IUncomb info v comb instr =>
      New.With_family.IUncomb
        (kinfo_to_location info)
        v
        (migrate_uncomb_gadt_witness comb)
        (migrate_kinstr instr)
    | Old.With_family.IComb_get info v comb instr =>
      New.With_family.IComb_get
        (kinfo_to_location info)
        v
        (migrate_comb_get_gadt_witness comb)
        (migrate_kinstr instr)
    | Old.With_family.IComb_set info v comb instr =>
      New.With_family.IComb_set
        (kinfo_to_location info)
        v
        (migrate_comb_set_gadt_witness comb)
        (migrate_kinstr instr)
    | Old.With_family.IDup_n info v dup_n instr =>
      New.With_family.IDup_n
        (kinfo_to_location info)
        v
        (migrate_dup_n_gadt_witness dup_n)
        (migrate_kinstr instr)
    | Old.With_family.ITicket info instr =>
      New.With_family.ITicket
        (kinfo_to_location info)
        (axiom "ty")
        (migrate_kinstr instr)
    | Old.With_family.IRead_ticket info instr =>
      New.With_family.IRead_ticket
        (kinfo_to_location info)
        (axiom "ty")
        (migrate_kinstr instr)
    | Old.With_family.ISplit_ticket info instr =>
      New.With_family.ISplit_ticket (kinfo_to_location info) (migrate_kinstr instr)
    | Old.With_family.IJoin_tickets info t instr =>
      New.With_family.IJoin_tickets
        (kinfo_to_location info)
        (migrate_ty t)
        (migrate_kinstr instr)
    | Old.With_family.IOpen_chest info instr =>
      New.With_family.IOpen_chest (kinfo_to_location info) (migrate_kinstr instr)
    (*
      Internal control instructions
      -----------------------------
    *)
    | Old.With_family.IHalt info =>
      New.With_family.IHalt (kinfo_to_location info)
    end

  (** Migrate [lambda]. *)
  with migrate_lambda {arg ret} (lam : Old.With_family.lambda arg ret)
    {struct lam} : New.With_family.lambda (Ty.migrate arg) (Ty.migrate ret) :=
    let '(descr, code) := lam in
    (migrate_kdescr descr, Script_repr.migrate_node code)

  (** Migrate [kdescr]. *)
  with migrate_kdescr {s f} (descr : Old.With_family.kdescr s f)
    {struct descr} :
    New.With_family.kdescr (Stack_ty.migrate s) (Stack_ty.migrate f) :=
    {|
      New.With_family.kdescr.kloc := descr.(Old.With_family.kdescr.kloc);
      New.With_family.kdescr.kbef :=
        migrate_stack_ty descr.(Old.With_family.kdescr.kbef);
      New.With_family.kdescr.kaft :=
        migrate_stack_ty descr.(Old.With_family.kdescr.kaft);
      New.With_family.kdescr.kinstr :=
        migrate_kinstr descr.(Old.With_family.kdescr.kinstr);
    |}

  (** Migrate a Michelson value. *)
  with migrate_value {t : Old.Ty.t} {struct t} :
    Old.With_family.ty_to_dep_Set t ->
    New.With_family.ty_to_dep_Set (Ty.migrate t) :=
    match
      t
      return
        Old.With_family.ty_to_dep_Set t ->
        New.With_family.ty_to_dep_Set (Ty.migrate t)
    with
    | Old.Ty.Unit => fun x => x
    | Old.Ty.Num _ => Script_int_repr.migrate
    | Old.Ty.Signature => Script_typed_ir.Script_signature.migrate
    | Old.Ty.String => Script_string_repr.migrate
    | Old.Ty.Bytes => fun x => x
    | Old.Ty.Mutez => Tez_repr.migrate
    | Old.Ty.Key_hash => fun x => x
    | Old.Ty.Key => fun x => x
    | Old.Ty.Timestamp => Script_timestamp_repr.migrate
    | Old.Ty.Address => Script_typed_ir.migrate_address
    | Old.Ty.Tx_rollup_l2_address =>
      Script_typed_ir.migrate_tx_rollup_l2_address
    | Old.Ty.Bool => fun x => x
    | Old.Ty.Pair ty1 ty2 => fun '(x1, x2) =>
      (@migrate_value ty1 x1, @migrate_value ty2 x2)
    | Old.Ty.Union ty1 ty2 =>
      Script_typed_ir.migrate_union migrate_value migrate_value
    | Old.Ty.Lambda ty_arg ty_res => migrate_lambda
    | Old.Ty.Option ty => fun x =>
      match x with
      | None => None
      | Some x => Some (@migrate_value ty x)
      end
    | Old.Ty.List ty => Script_typed_ir.migrate_boxed_list migrate_value
    | Old.Ty.Set_ ty_k => migrate_set
    | Old.Ty.Map tyk _ => migrate_map migrate_value
    | Old.Ty.Big_map _ _ => migrate_big_map_skeleton migrate_value migrate_value
    | Old.Ty.Contract ty => migrate_typed_contract
    | Old.Ty.Sapling_transaction => fun x => x
    | Old.Ty.Sapling_transaction_deprecated => fun x => x
    | Old.Ty.Sapling_state => Sapling_storage.migrate_state
    | Old.Ty.Operation => Script_typed_ir.migrate_operation
    | Old.Ty.Chain_id => Script_typed_ir.Script_chain_id.migrate
    | Old.Ty.Never => fun x => match x with end
    | Old.Ty.Bls12_381_g1 => Script_typed_ir.Script_bls.G1.migrate
    | Old.Ty.Bls12_381_g2 => Script_typed_ir.Script_bls.G2.migrate
    | Old.Ty.Bls12_381_fr => Script_typed_ir.Script_bls.Fr.migrate
    | Old.Ty.Ticket ty => Script_typed_ir.migrate_ticket migrate_value
    | Old.Ty.Chest_key => Script_typed_ir.Script_timelock.migrate_chest_key
    | Old.Ty.Chest => Script_typed_ir.Script_timelock.migrate_chest
    end.

  (** Migrate the head of a stack. *)
  Definition migrate_stack_value_head {tys : Old.Stack_ty.t}
    (head : Old.With_family.stack_ty_to_dep_Set_head tys) :
    New.With_family.stack_ty_to_dep_Set_head (Stack_ty.migrate tys) :=
    match tys, head with
    | [], _ => Proto_K.Script_typed_ir.EmptyCell
    | ty :: _, head => @migrate_value ty head
    end.

  (** Migrate the tail of a stack. *)
  Fixpoint migrate_stack_value_tail {tys : Old.Stack_ty.t}
    (tail : Old.With_family.stack_ty_to_dep_Set_tail tys) :
    New.With_family.stack_ty_to_dep_Set_tail (Stack_ty.migrate tys) :=
    match tys, tail with
    | [], _ => Proto_K.Script_typed_ir.EmptyCell
    | _ :: tys, (head, tail) =>
      (@migrate_stack_value_head tys head, @migrate_stack_value_tail tys tail)
    end.

  (** Migrate the output of [stack_ty_to_dep_Set]. *)
  Definition migrate_stack_value {s}
    (v : Old.With_family.stack_ty_to_dep_Set s) :
    New.With_family.stack_ty_to_dep_Set (Stack_ty.migrate s) :=
  (migrate_stack_value_head (fst v), migrate_stack_value_tail (snd v)).

  (** Migrate [continuation]. *)
  Fixpoint migrate_continuation {s t} (c : Old.With_family.continuation s t) :
    New.With_family.continuation (Stack_ty.migrate s) (Stack_ty.migrate t) :=
    match c with
    | Old.With_family.KNil =>
      New.With_family.KNil
    | Old.With_family.KCons instr c =>
      New.With_family.KCons (migrate_kinstr instr) (migrate_continuation c)
    | Old.With_family.KReturn s c =>
      New.With_family.KReturn
        (migrate_stack_value s)
        (axiom "stack_ty")
        (migrate_continuation c)
    | Old.With_family.KMap_head c =>
      New.With_family.KMap_head (migrate_continuation c)
    | Old.With_family.KUndip b c =>
      New.With_family.KUndip
        (migrate_value b)
        (axiom "ty")
        (migrate_continuation c)
    | Old.With_family.KLoop_in i c =>
      New.With_family.KLoop_in (migrate_kinstr i) (migrate_continuation c)
    | Old.With_family.KLoop_in_left i c =>
      New.With_family.KLoop_in_left (migrate_kinstr i) (migrate_continuation c)
    | Old.With_family.KIter i xs c =>
      New.With_family.KIter
        (migrate_kinstr i)
        (axiom "ty")
        (List.map migrate_value xs)
        (migrate_continuation c)
    | Old.With_family.KList_enter_body i xs ys n c =>
      New.With_family.KList_enter_body
        (migrate_kinstr i)
        (List.map migrate_value xs)
        (List.map migrate_value ys)
        (axiom "ty")
        n
        (migrate_continuation c)
    | Old.With_family.KList_exit_body i xs ys n c =>
      New.With_family.KList_exit_body
        (migrate_kinstr i)
        (List.map migrate_value xs)
        (List.map migrate_value ys)
        (axiom "ty")
        n
        (migrate_continuation c)
    | Old.With_family.KMap_enter_body i xs m c =>
      New.With_family.KMap_enter_body
        (migrate_kinstr i)
        (List.map (fun '(x1, x2) => (migrate_value x1, migrate_value x2)) xs)
        (migrate_value m)
        (axiom "ty")
        (migrate_continuation c)
    | Old.With_family.KMap_exit_body i xs m x c =>
      New.With_family.KMap_exit_body
        (migrate_kinstr i)
        (List.map (fun '(x1, x2) => (migrate_value x1, migrate_value x2)) xs)
        (migrate_value m)
        (migrate_value x)
        (axiom "ty")
        (migrate_continuation c)
    | Old.With_family.KView_exit step c =>
      New.With_family.KView_exit
        (Script_typed_ir.migrate_step_constants step)
        (migrate_continuation c)
    end.
End With_family.
