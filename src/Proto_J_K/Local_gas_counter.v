Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Local_gas_counter.
Require TezosOfOCaml.Proto_K.Local_gas_counter.
Require TezosOfOCaml.Proto_J_K.Raw_context.
Require TezosOfOCaml.Proto_J_K.Proofs.Saturation_repr.
Require TezosOfOCaml.Proto_K.Proofs.Saturation_repr.

Module Old := TezosOfOCaml.Proto_J.Local_gas_counter.
Module New := TezosOfOCaml.Proto_K.Local_gas_counter.

(** Migrate [local_gas_counter]. *)
Definition migrate_local_gas_counter (gas_counter : Old.local_gas_counter) :
  New.local_gas_counter :=
  match gas_counter with
  | Old.Local_gas_counter gas_counter => New.Local_gas_counter gas_counter
  end.

(** Migrate [outdated_context]. *)
Definition migrate_outdated_context (ctxt : Old.outdated_context) :
  New.outdated_context :=
  match ctxt with
  | Old.Outdated_context ctxt =>
    New.Outdated_context (Raw_context.migrate ctxt)
  end.
