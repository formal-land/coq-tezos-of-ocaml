Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Script_typed_ir.
Require TezosOfOCaml.Proto_K.Script_typed_ir.
Require TezosOfOCaml.Proto_J_K.Contract_repr.
Require TezosOfOCaml.Proto_J_K.Destination_repr.
Require TezosOfOCaml.Proto_J_K.Gas_limit_repr.
Require TezosOfOCaml.Proto_J_K.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_J_K.Script_int_repr.
Require TezosOfOCaml.Proto_J_K.Script_repr.
Require TezosOfOCaml.Proto_J_K.Script_timestamp_repr.
Require TezosOfOCaml.Proto_J_K.Tez_repr.
Require TezosOfOCaml.Proto_J_K.Tx_rollup_l2_address.

Module Old := TezosOfOCaml.Proto_J.Script_typed_ir.
Module New := TezosOfOCaml.Proto_K.Script_typed_ir.

(** Migrate [step_constants]. *)
Definition migrate_step_constants (sc : Old.step_constants) :
  New.step_constants :=
  let '{|
    Old.step_constants.source := source;
    Old.step_constants.payer := payer;
    Old.step_constants.self := self;
    Old.step_constants.amount := amount;
    Old.step_constants.balance := balance;
    Old.step_constants.chain_id := chain_id;
    Old.step_constants.now := now;
    Old.step_constants.level := level;
  |} := sc in
  {|
    New.step_constants.source := Contract_repr.migrate source;
    New.step_constants.payer := Contract_repr.migrate payer;
    New.step_constants.self :=
      match self with
      | Proto_J.Contract_repr.Implicit _ => axiom "contract_implicit"
      | Proto_J.Contract_repr.Originated hash => hash
      end;
    New.step_constants.amount := Tez_repr.migrate amount;
    New.step_constants.balance := Tez_repr.migrate balance;
    New.step_constants.chain_id := chain_id;
    New.step_constants.now := Script_timestamp_repr.migrate now;
    New.step_constants.level := Script_int_repr.migrate level;
  |}.

(** Migrate [address]. *)
Definition migrate_address (a : Old.address) : New.address :=
  {|
    New.address.destination :=
      Destination_repr.migrate a.(Old.address.destination);
    New.address.entrypoint := a.(Old.address.entrypoint);
  |}.

Module Script_signature.
  (** Migrate [Script_signature.t]. *)
  Definition migrate (s : Old.Script_signature.t) : New.Script_signature.t :=
    match s with
    | Old.Script_signature.Signature_tag s =>
      New.Script_signature.Signature_tag s
    end.
End Script_signature.

(** Migrate [tx_rollup_l2_address]. *)
Definition migrate_tx_rollup_l2_address (x : Old.tx_rollup_l2_address) :
  New.tx_rollup_l2_address :=
  Tx_rollup_l2_address.Indexable.migrate_value x.

(** Migrate [union]. *)
Definition migrate_union {a a' b b' : Set}
  (migrate_left : a -> a')
  (migrate_right : b -> b')
  (x : Old.union a b) :
  New.union a' b' :=
  match x with
  | Old.L x => New.L (migrate_left x)
  | Old.R x => New.R (migrate_right x)
  end.

Module Script_chain_id.
  (** Migrate [Script_chain_id.t]. *)
  Definition migrate (x : Old.Script_chain_id.t) : New.Script_chain_id.t :=
    match x with
    | Old.Script_chain_id.Chain_id_tag x => New.Script_chain_id.Chain_id_tag x
    end.
End Script_chain_id.

Module Script_bls.
  Module Fr.
    (** Migrate [Script_bls.Fr.t]. *)
    Definition migrate (x : Old.Script_bls.Fr.t) : New.Script_bls.Fr.t :=
      match x with
      | Old.Script_bls.Fr.Fr_tag x => New.Script_bls.Fr.Fr_tag x
      end.
  End Fr.

  Module G1.
    (** Migrate [Script_bls.G1.t]. *)
    Definition migrate (x : Old.Script_bls.G1.t) : New.Script_bls.G1.t :=
      match x with
      | Old.Script_bls.G1.G1_tag x => New.Script_bls.G1.G1_tag x
      end.
  End G1.

  Module G2.
    (** Migrate [Script_bls.G2.t]. *)
    Definition migrate (x : Old.Script_bls.G2.t) : New.Script_bls.G2.t :=
      match x with
      | Old.Script_bls.G2.G2_tag x => New.Script_bls.G2.G2_tag x
      end.
  End G2.
End Script_bls.

Module Script_timelock.
  (** Migrate [chest_key]. *)
  Definition migrate_chest_key (x : Old.Script_timelock.chest_key) :
    New.Script_timelock.chest_key :=
    match x with
    | Old.Script_timelock.Chest_key_tag x => New.Script_timelock.Chest_key_tag x
    end.

  (** Migrate [chest]. *)
  Definition migrate_chest (x : Old.Script_timelock.chest) :
    New.Script_timelock.chest :=
    match x with
    | Old.Script_timelock.Chest_tag x => New.Script_timelock.Chest_tag x
    end.
End Script_timelock.

(** Migrate [ty] *)
Fixpoint migrate_ty (x : Old.ty) : New.ty. (* TODO *)
Admitted.

(** Migrate [ticket]. *)
Definition migrate_ticket {a a' : Set} (migrate : a -> a') (x : Old.ticket a) :
  New.ticket a' :=
  {|
    New.ticket.ticketer := Contract_repr.migrate x.(Old.ticket.ticketer);
    New.ticket.contents := migrate x.(Old.ticket.contents);
    New.ticket.amount := Script_int_repr.migrate x.(Old.ticket.amount);
  |}.

(** Migrate [ty_metadata]. *)
Definition migrate_ty_metadata (m : Old.ty_metadata) : New.ty_metadata :=
  let '{| Old.ty_metadata.size := size |} := m in
  {| New.ty_metadata.size := size |}.

(** Migrate [boxed_list]. *)
Definition migrate_boxed_list {a a' : Set} (migrate : a -> a')
  (x : Old.boxed_list a) : New.boxed_list a' :=
  {|
    New.boxed_list.elements := List.map migrate x.(Old.boxed_list.elements);
    New.boxed_list.length := x.(Old.boxed_list.length);
  |}.

(** Migrate [execution_trace]. *)
Definition migrate_execution_trace (x : Old.execution_trace) :
  New.execution_trace :=
  List.map (
    fun '(l, g, e) =>
      (l, Gas_limit_repr.migrate g, List.map (Script_repr.map_node Michelson_v1_primitives.migrate_prim) e)
  ) x.

(** Migrate [operation]. *)
Definition migrate_operation (x : Old.operation) : New.operation.
Admitted.
