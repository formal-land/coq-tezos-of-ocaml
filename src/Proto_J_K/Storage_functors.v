Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require Import TezosOfOCaml.Proto_J_K.Simulations.Script_family.
Require Import TezosOfOCaml.Proto_J_K.Proofs.Script_interpreter_defs.

Module Old := TezosOfOCaml.Proto_J.Simulations.Script_interpreter.
Module New := TezosOfOCaml.Proto_K.Simulations.Script_interpreter.

Module Make_single_data_storage.
  (** Migration of single data storage values *)
  Definition migrate_value
     {key value : Set}
     `{HFargs0 : Storage_functors.Make_single_data_storage.FArgs
         (C_t := (Raw_context.New.t * key)) (V_t := value)}
     `{HFArgs1 : Proto_J.Storage_functors.Make_single_data_storage.FArgs
         (C_t := (Proto_J.Raw_context.t * key)) (V_t := value)} :
      M? Proto_J.Storage_functors.Make_single_data_storage.V
        .(Proto_J.Storage_sigs.VALUE.t) ->
      M? Storage_functors.Make_single_data_storage.V.(Storage_sigs.VALUE.t).
    Admitted.
End Make_single_data_storage.
