Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_J.Tx_rollup_inbox_repr.
Require TezosOfOCaml.Proto_K.Tx_rollup_inbox_repr.

Require TezosOfOCaml.Proto_J_K.Merkle_list.

Module Old := TezosOfOCaml.Proto_J.Tx_rollup_inbox_repr.
Module New := TezosOfOCaml.Proto_K.Tx_rollup_inbox_repr.

Module Merkle.
  Module Old := Old.Merkle.
  Module New := New.Merkle.

  (** Migrate [Tx_rollup_inbox_repr.Merkle.tree] *)
  Definition migrate_tree (ml : Old.tree) : New.tree :=
    Merkle_list.Make.migrate ml.
End Merkle.
