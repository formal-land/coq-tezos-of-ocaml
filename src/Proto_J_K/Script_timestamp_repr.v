Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Script_timestamp_repr.
Require TezosOfOCaml.Proto_K.Script_timestamp.

Module Old := TezosOfOCaml.Proto_J.Script_timestamp_repr.
Module New := TezosOfOCaml.Proto_K.Script_timestamp.

(** Migrate [Script_timestamp_repr.t]. *)
Definition migrate (x : Old.t) : New.t :=
  match x with
  | Old.Timestamp_tag x => New.Timestamp_tag x
  end.
