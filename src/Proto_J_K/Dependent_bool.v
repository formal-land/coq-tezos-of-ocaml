Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Dependent_bool.
Require TezosOfOCaml.Proto_K.Dependent_bool.

Module Old := TezosOfOCaml.Proto_J.Dependent_bool.
Module New := TezosOfOCaml.Proto_K.Dependent_bool.

(** Migrate [dbool]. *)
Definition migrate_dbool (b : Old.dbool) : New.dbool :=
  match b with
  | Old.No => New.No
  | Old.Yes => New.Yes
  end.

(** Migrate [dand]. *)
Definition migrate_dand (b : Old.dand) : New.dand :=
  match b with
  | Old.NoNo => New.NoNo
  | Old.NoYes => New.NoYes
  | Old.YesNo => New.YesNo
  | Old.YesYes => New.YesYes
  end.
