Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Script_repr.
Require TezosOfOCaml.Proto_K.Script_repr.
Require TezosOfOCaml.Proto_J_K.Michelson_v1_primitives.

Module Old := TezosOfOCaml.Proto_J.Script_repr.
Module New := TezosOfOCaml.Proto_K.Script_repr.

(** Apply a function [f] on all the primitives of a node. *)
Fixpoint map_node {l p1 p2 : Set} (f : p1 -> p2) (node : Micheline.node l p1)
  {struct node} : Micheline.node l p2 :=
  match node with
  | Micheline.Int l z => Micheline.Int l z
  | Micheline.String l s => Micheline.String l s
  | Micheline.Bytes l b => Micheline.Bytes l b
  | Micheline.Prim l p ns annot =>
    Micheline.Prim l (f p) (List.map (map_node f) ns) annot
  | Micheline.Seq l ns =>
    Micheline.Seq l (List.map (map_node f) ns)
  end.

(** Migrate [expr]. *)
Definition migrate_expr (node : Old.expr) : New.expr :=
  map_node Michelson_v1_primitives.migrate_prim node.

(** Migrate [node]. *)
Definition migrate_node (node : Old.node) : New.node :=
  map_node Michelson_v1_primitives.migrate_prim node.
