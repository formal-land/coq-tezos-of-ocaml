Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Alpha_context.
Require TezosOfOCaml.Proto_J.Proofs.Saturation_repr.
Require TezosOfOCaml.Proto_K.Raw_context.
Require TezosOfOCaml.Proto_J.Raw_context.

Require TezosOfOCaml.Proto_J_K.Constants_repr.
Require TezosOfOCaml.Proto_J_K.Lazy_storage_kind.
Require TezosOfOCaml.Proto_J_K.Level_repr.
Require TezosOfOCaml.Proto_J_K.Origination_nonce.
Require TezosOfOCaml.Proto_J_K.Round_repr.
Require TezosOfOCaml.Proto_J_K.Seed_repr.
Require TezosOfOCaml.Proto_J_K.Tez_repr.
Require TezosOfOCaml.Proto_J_K.Tx_rollup_inbox_repr.

Module Old := TezosOfOCaml.Proto_J.Raw_context.
Module New := TezosOfOCaml.Proto_K.Raw_context.

Module Raw_consensus.
  Module Old := Old.Raw_consensus.
  Module New := New.Raw_consensus.

  (** Module [Raw_context.Raw_consensus.t] *)
  Definition migrate (r : Old.t) : New.t :=
  {|
    New.t.current_endorsement_power := r.(Old.t.current_endorsement_power);
    New.t.allowed_endorsements := r.(Old.t.allowed_endorsements);
    New.t.allowed_preendorsements := r.(Old.t.allowed_preendorsements);
    New.t.grand_parent_endorsements_seen :=
      r.(Old.t.grand_parent_endorsements_seen);
    New.t.endorsements_seen := r.(Old.t.endorsements_seen);
    New.t.preendorsements_seen := r.(Old.t.preendorsements_seen);
    New.t.locked_round_evidence := r.(Old.t.locked_round_evidence);
    New.t.preendorsements_quorum_round :=
      r.(Old.t.preendorsements_quorum_round);
    New.t.endorsement_branch := r.(Old.t.endorsement_branch);
    New.t.grand_parent_branch := r.(Old.t.grand_parent_branch);
  |}.
End Raw_consensus.

(** Migrate [Raw_context.back]. *)
Definition migrate_back (b : Old.back) : New.back :=
{|
  New.back.context := b.(Old.back.context);
  New.back.constants :=
    Constants_repr.parametric.migrate b.(Old.back.constants);
  New.back.round_durations :=
    Round_repr.Durations.migrate b.(Old.back.round_durations);
  New.back.cycle_eras := Level_repr.migrate_cycle_eras b.(Old.back.cycle_eras);
  New.back.level := Level_repr.migrate b.(Old.back.level);
  New.back.predecessor_timestamp := b.(Old.back.predecessor_timestamp);
  New.back.timestamp := b.(Old.back.timestamp);
  New.back.fees := Tez_repr.migrate b.(Old.back.fees);
  New.back.origination_nonce :=
    let* on := b.(Old.back.origination_nonce) in
    return* Origination_nonce.migrate on;
  New.back.temporary_lazy_storage_ids :=
    Lazy_storage_kind.Temp_ids.migrate b.(Old.back.temporary_lazy_storage_ids);
  New.back.internal_nonce := b.(Old.back.internal_nonce);
  New.back.internal_nonces_used := b.(Old.back.internal_nonces_used);
  New.back.remaining_block_gas := b.(Old.back.remaining_block_gas);
  New.back.unlimited_operation_gas := b.(Old.back.unlimited_operation_gas);
  New.back.consensus := Raw_consensus.migrate b.(Old.back.consensus);
  New.back.non_consensus_operations_rev :=
    b.(Old.back.non_consensus_operations_rev);
  New.back.sampler_state :=
    Map.Make.map
      (fun '(seed, sampler) =>
         (Seed_repr.migrate_seed seed,
          axiom "TODO" (* TODO *))
      )
      b.(Old.back.sampler_state);
  New.back.stake_distribution_for_current_cycle :=
    let* 'x := axiom "TODO" (* TODO *) in return* x;
  New.back.tx_rollup_current_messages :=
    List.map
    (fun '(x, tree) =>
       (x, Tx_rollup_inbox_repr.Merkle.migrate_tree tree)
    )
    b.(Old.back.tx_rollup_current_messages);
  New.back.sc_rollup_current_messages := axiom "TODO"; (* TODO *)
  New.back.dal_slot_fee_market := axiom "TODO"; (* TODO *)
  New.back.dal_endorsement_slot_accountability := axiom "TODO"; (* TODO *)
|}.

(** Migrate [Raw_context.t]. *)
Definition migrate (ctxt : Old.t) : New.t :=
{|
  New.t.remaining_operation_gas := ctxt.(Old.t.remaining_operation_gas);
  New.t.back := migrate_back ctxt.(Old.t.back);
|}.
