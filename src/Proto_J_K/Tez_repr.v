Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Tez_repr.
Require TezosOfOCaml.Proto_K.Tez_repr.

Module Old := TezosOfOCaml.Proto_J.Tez_repr.
Module New := TezosOfOCaml.Proto_K.Tez_repr.

(** Migrate [Tez_repr.t]. *)
Definition migrate (x : Old.t) : New.t :=
  match x with
  | Old.Tez_tag n => New.Tez_tag n
  end.

(** Migrate errors defined in this file. *)
Definition migrate_error (err : _error) : _error :=
  match err with
  | Build_extensible tag _ payload =>
    if String.eqb tag "Addition_overflow" then
      let '(t1, t2) := cast (Old.t * Old.t) payload in
      Build_extensible "Addition_overflow" _ (migrate t1, migrate t2)
    else if String.eqb tag "Subtraction_underflow" then
      let '(t1, t2) := cast (Old.t * Old.t) payload in
      Build_extensible "Subtraction_underflow" _ (migrate t1, migrate t2)
    else if String.eqb tag "Multiplication_overflow" then
      let '(t1, t2) := cast (Old.t * int64) payload in
      Build_extensible "Multiplication_overflow" _ (migrate t1, t2)
    else if String.eqb tag "Negative_multiplicator" then
      let '(t1, t2) := cast (Old.t * int64) payload in
      Build_extensible "Negative_multiplicator" _ (migrate t1, t2)
    else
      err
  end.
