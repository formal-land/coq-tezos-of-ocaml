Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_J.Merkle_list.
Require TezosOfOCaml.Proto_K.Merkle_list.

Module Old := TezosOfOCaml.Proto_J.Merkle_list.
Module New := TezosOfOCaml.Proto_K.Merkle_list.

(** Migrate [Merkle_list.S_El] *)
Definition migrate_S_El {t : Set} (e : Old.S_El (t:=t)) : New.S_El :=
{|
  New.S_El.to_bytes := e.(Old.S_El.to_bytes);
|}.

Module Make.
  Module Old := Old.Make.
  Module New := New.Make.

  (** Migrate [Merkle_list.Make.FArgs] *)
  Definition migrate_FArgs {El_t H_t H_Set_t : Set} {H_Map_t : Set -> Set}
    (HH : Old.FArgs (El_t := El_t) (H_t := H_t) (H_Set_t := H_Set_t)
            (H_Map_t := H_Map_t)) : New.FArgs :=
    let 'Old.Build_FArgs El H := HH in
    {|
      New.El := migrate_S_El El;
      New.H := H;
    |}.

  (** Migrate [Merkle_list.Make.tree] *)
  Fixpoint migrate_tree `{H : Old.FArgs} (t : Old.tree) : New.tree :=
    let '_ := migrate_FArgs H in
    match t with
    | Old.Empty => New.Empty
    | Old.Leaf h => New.Leaf h
    | Old.Node (h, l, r) => New.Node (h, migrate_tree l, migrate_tree r)
    end.

  (** Migrate [Merkle_list.Make.t] *)
  Definition migrate `{H : Old.FArgs} (ml : Old.t) : New.t :=
    let '_ := migrate_FArgs H in
    {|
      New.t.tree := migrate_tree ml.(Old.t.tree);
      New.t.depth := ml.(Old.t.depth);
      New.t.next_pos := ml.(Old.t.next_pos);
    |}.
End Make.
