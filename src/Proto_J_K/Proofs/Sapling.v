Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Raw_context.
Require TezosOfOCaml.Proto_J.Sapling_storage.
Require TezosOfOCaml.Proto_K.Raw_context.
Require TezosOfOCaml.Proto_J_K.Raw_context.
Require TezosOfOCaml.Proto_J_K.Sapling_storage.

Module Verification.
  (** [with_verification f2] is backward compatible to [with_verification f1]
      if [f2] is backward compatible to w.r.t [f1] *)
  Axiom with_verification_is_backward_compatible : forall
    (f1 : V5.Sapling.Verification.t ->
       Proto_J.Raw_context.t * 
         M* (V5.Int64.t * Proto_J.Sapling_storage.state)) 
    (f2 : V6.Sapling.Verification.t -> 
          Proto_K.Raw_context.t * M* (V6.Int64.t * Sapling_storage.state))
    (H_func_ext : forall (v' : V5.Sapling.Verification.t),
      (let '(ctxt, opt) := f1 v' in
      (Raw_context.migrate ctxt, Option.map (fun '(i, s) => 
        (i, Sapling_storage.migrate_state s)) opt)) =
      f2 v'),
    (let '(ctxt, opt) := V5.Sapling.Verification.with_verification_ctx f1 in
      (Raw_context.migrate ctxt, Option.map (fun '(i, s) => 
        (i, Sapling_storage.migrate_state s)) opt)) =
    V6.Sapling.Verification.with_verification_ctx f2.
End Verification.
