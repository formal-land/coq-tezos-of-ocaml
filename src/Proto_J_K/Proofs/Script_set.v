Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.

Require Proto_J.Simulations.Script_typed_ir.
Require Proto_J.Simulations.Script_set.

Require Proto_K.Simulations.Script_typed_ir.
Require Proto_K.Simulations.Script_set.

Require Proto_J_K.Simulations.Script_typed_ir.
Require Proto_J_K.Proofs.Script_typed_ir.
Require Proto_J_K.Proofs.Script_comparable.

Module OldIR := Proto_J.Simulations.Script_typed_ir.
Module NewIR := Proto_K.Simulations.Script_typed_ir.

Lemma dep_fold_is_backward_compatible_aux {a}
  (l : list (OldIR.With_family.ty_to_dep_Set a))
  (s : OldIR.With_family.ty_to_dep_Set
       (Proto_J.Simulations.Script_family.Ty.Set_ a))
  :
  Script_set.dep_fold (fun e acc => e :: acc) (Script_typed_ir.With_family.migrate_set s)
  (List.map Script_typed_ir.With_family.migrate_value l)
  = Lists.List.map Script_typed_ir.With_family.migrate_value
      (Proto_J.Simulations.Script_set.dep_fold (fun e acc => e :: acc) s l).
Proof.
  revert l.
  unfold Script_set.dep_fold,
    Proto_J.Simulations.Script_set.dep_fold; simpl.
  with_strategy transparent [V5.Map.Make]
    unfold V5.Map.Make; simpl.
  unfold V5.Map.Make.fold.
  induction s; intros; [reflexivity|].
  simpl; Tactics.destruct_pairs.
  rewrite Script_typed_ir.of_value_is_backward_compatible.
  destruct OldIR.With_family.of_value; sauto.
Qed.

(** [dep_fold] is backward compatible. *)
Lemma dep_fold_is_backward_compatible {a}
  (s : OldIR.With_family.ty_to_dep_Set
       (Proto_J.Simulations.Script_family.Ty.Set_ a)) :
  Script_set.dep_fold (fun e acc => e :: acc) (Script_typed_ir.With_family.migrate_set s) nil
  = Lists.List.map Script_typed_ir.With_family.migrate_value
      (Proto_J.Simulations.Script_set.dep_fold (fun e acc => e :: acc) s nil).
Proof.
  apply (dep_fold_is_backward_compatible_aux []).
Qed.

(** [dep_mem] is backward compatible. *)
Lemma dep_mem_is_backward_compatible {a} (s : OldIR.With_family.ty_to_dep_Set a)
  (t : OldIR.With_family.set a) :
  Script_set.dep_mem
    (Script_typed_ir.With_family.migrate_value s)
    (Script_typed_ir.With_family.migrate_set t) =
  Proto_J.Simulations.Script_set.dep_mem s t.
Proof.
  unfold Script_set.dep_mem.
  unfold Proto_J.Simulations.Script_set.dep_mem.
  simpl.
  with_strategy transparent [V5.Map.Make]
    unfold V5.Map.Make; simpl.
  unfold Script_typed_ir.With_family.migrate_set.
  induction t.
  { reflexivity. }
  { simpl.
    destruct a0; simpl.
    rewrite <- Script_typed_ir.to_value_is_backward_compatible.
    rewrite Script_comparable.compare_keys_is_backward_compatible.
    destruct V5.Map.Make.compare_keys; auto.
    rewrite Script_typed_ir.to_value_is_backward_compatible.
    auto.
  }
Qed.

(** [dep_update] is backward compatible. *)
Lemma dep_update_is_backward_compatible {a} (s0 : OldIR.With_family.ty_to_dep_Set a) (t : bool)
  (t0 : Proto_J.Simulations.Script_typed_ir.With_family.set a) :
  Script_typed_ir.With_family.migrate_set
    (Proto_J.Simulations.Script_set.dep_update s0 t t0) =
  Script_set.dep_update (Script_typed_ir.With_family.migrate_value s0) t
    (Script_typed_ir.With_family.migrate_set t0).
Proof.
  unfold Script_typed_ir.With_family.migrate_set.
  unfold Proto_J.Simulations.Script_set.dep_update.
  unfold Script_set.dep_update; simpl.
  with_strategy transparent [V5.Map.Make]
    unfold V5.Map.Make; simpl.
  destruct t; simpl.
  { induction t0.
    { simpl. repeat f_equal.
      now rewrite Script_typed_ir.to_value_is_backward_compatible.
    }
    { simpl. destruct a0.
      rewrite <- Script_typed_ir.to_value_is_backward_compatible.
      rewrite Script_comparable.compare_keys_is_backward_compatible.
      destruct V5.Map.Make.compare_keys; try reflexivity.
      simpl; rewrite IHt0.
      now rewrite Script_typed_ir.to_value_is_backward_compatible.
    }
  }
  { induction t0.
    { reflexivity. }
    { simpl. destruct a0.
      rewrite <- Script_typed_ir.to_value_is_backward_compatible.
      rewrite Script_comparable.compare_keys_is_backward_compatible.
      destruct V5.Map.Make.compare_keys; try reflexivity.
      simpl; rewrite IHt0.
      now rewrite Script_typed_ir.to_value_is_backward_compatible.
    }
  }
Qed.
