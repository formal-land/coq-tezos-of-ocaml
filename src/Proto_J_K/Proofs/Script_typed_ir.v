Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require Import TezosOfOCaml.Environment.V6.Proofs.List.

Require TezosOfOCaml.Proto_J.Simulations.Script_interpreter.
Require TezosOfOCaml.Proto_K.Simulations.Script_interpreter.
Require Import TezosOfOCaml.Proto_J_K.Simulations.Script_family.
Require TezosOfOCaml.Proto_J_K.Simulations.Script_typed_ir.
Require TezosOfOCaml.Proto_J_K.Script_typed_ir.
Require TezosOfOCaml.Proto_J.Script_interpreter_defs.
Require TezosOfOCaml.Proto_J.Simulations.Script_interpreter_defs.
Require TezosOfOCaml.Proto_K.Simulations.Script_interpreter_defs.

Require Import TezosOfOCaml.Proto_J_K.Simulations.Script_typed_ir.
Require Import TezosOfOCaml.Proto_J_K.Script_typed_ir.

Module OldIR := TezosOfOCaml.Proto_J.Simulations.Script_typed_ir.
Module NewIR := TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.

Module Old := TezosOfOCaml.Proto_J.Simulations.Script_interpreter_defs.
Module New := TezosOfOCaml.Proto_K.Simulations.Script_interpreter_defs.

(** [migrate_set] respects [cardinal]. *)
Lemma migrate_set_cardinal {a} (x : Old.With_family.set a) : 
  (New.With_family.Script_Set (Ty.migrate a)).(_Set.S.cardinal)
  (With_family.migrate_set x) =
  (Old.With_family.Script_Set a).(_Set.S.cardinal) x.
Proof.
  simpl.
  with_strategy transparent [V5.Map.Make] unfold V5.Map.Make.
  unfold With_family.migrate_set; simpl.
  unfold V5.Map.Make.cardinal.
  now rewrite length_map.
Qed.

(** [migrate_map] respects [cardinal]. *)
Lemma migrate_map_cardinal {a b} (s : Old.With_family.map a
 (Old.With_family.ty_to_dep_Set b)) : 
  (New.With_family.Script_Map (Ty.migrate a)).(Map.S.cardinal)
  (With_family.migrate_map With_family.migrate_value
 s) =
  (Old.With_family.Script_Map a).(Map.S.cardinal) s.
Proof.
  unfold New.With_family.Script_Map.
  unfold Old.With_family.Script_Map.
  with_strategy transparent [V5.Map.Make] unfold V5.Map.Make.
  unfold With_family.migrate_map; simpl.
  unfold V5.Map.Make.cardinal.
  now rewrite length_map.
Qed.

(** [step_constants] validity predicate
    [step_constants] is valid when [self] field is not [Implicit] *)
Module Step_constants.
  Module Valid.
    Definition t (sc : Proto_J.Script_typed_ir.step_constants) : Prop := 
      match sc.(Script_typed_ir.Old.step_constants.self) with
      | Proto_J.Alpha_context.Contract.Implicit _ => False
      | Proto_J.Alpha_context.Contract.Originated _ => True
      end.
  End Valid.
End Step_constants.

(** [continuation] validity predicate *) 
Module Continuation.
  Module Valid.
    Fixpoint t {t1 t2} (k : OldIR.With_family.continuation t1 t2) {struct k} : Prop :=
      match k with
      | OldIR.With_family.KView_exit sc' k' =>
        Script_typed_ir.Step_constants.Valid.t sc' /\ t k'
      | OldIR.With_family.KCons _ k'
      | OldIR.With_family.KReturn _ k'
      | OldIR.With_family.KMap_head k'
      | OldIR.With_family.KUndip _ k'
      | OldIR.With_family.KLoop_in _ k'
      | OldIR.With_family.KLoop_in_left _ k'
      | OldIR.With_family.KIter _ _ k'
      | OldIR.With_family.KList_enter_body _ _ _ _ k'
      | OldIR.With_family.KList_exit_body _ _ _ _ k'
      | OldIR.With_family.KMap_enter_body _ _ _ k'
      | OldIR.With_family.KMap_exit_body _ _ _ _ k'
        => t k'
      | OldIR.With_family.KNil => True
      end.
    End Valid.
End Continuation.

(** Module [Script_bls]. We try follow to original code structure,
    hence there are this module below and modules inside [Script_bls]. *)
Module Script_bls.
  Module G1.
    (** A fact about [add] migration. *)
    Lemma migrate_add_eq a b :
      Script_typed_ir.Script_bls.G1.migrate
        (Script_typed_ir.Old.Script_bls.G1.add a b) =
        Script_typed_ir.Script_bls.G1.add
          (Script_typed_ir.Script_bls.G1.migrate a)
          (Script_typed_ir.Script_bls.G1.migrate b).
    Proof.
      destruct a, b; reflexivity.
    Qed.

    (** A fact about [Script_bls.G1.mul] migration. *)
    Lemma migrate_mul_eq a b :
      Script_typed_ir.Script_bls.G1.migrate
        (Script_typed_ir.Old.Script_bls.G1.mul a b) =
        Script_typed_ir.Script_bls.G1.mul
          (Script_typed_ir.Script_bls.G1.migrate a)
          (Script_typed_ir.Script_bls.Fr.migrate b).
    Proof.
      destruct a, b; reflexivity.
    Qed.

    (** A fact about negation migration. *)
    Lemma migrate_negate a :
      Script_typed_ir.Script_bls.G1.migrate
        (Proto_J.Script_typed_ir.Script_bls.G1.negate a) =
        Script_typed_ir.Script_bls.G1.negate
          (Script_typed_ir.Script_bls.G1.migrate a).
    Proof.
      destruct a; reflexivity.
    Qed.
  End G1.

  Module G2.
    (** A fact about [Script_bls.G2.add] migration. *)
    Lemma migrate_add_eq a b :
      Script_typed_ir.Script_bls.G2.migrate
        (Script_typed_ir.Old.Script_bls.G2.add a b) =
        Script_typed_ir.Script_bls.G2.add
          (Script_typed_ir.Script_bls.G2.migrate a)
          (Script_typed_ir.Script_bls.G2.migrate b).
    Proof.
      destruct a, b; reflexivity.
    Qed.

    (** A fact about [Script_bls.G2.mul] migration. *)
    Lemma migrate_mul_eq a b :
      Script_typed_ir.Script_bls.G2.migrate
        (Script_typed_ir.Old.Script_bls.G2.mul a b) =
        Script_typed_ir.Script_bls.G2.mul
          (Script_typed_ir.Script_bls.G2.migrate a)
          (Script_typed_ir.Script_bls.Fr.migrate b).
    Proof.
      destruct a, b; reflexivity.
    Qed.

    (** A fact about negation migration. *)
    Lemma migrate_negate a :
      Script_typed_ir.Script_bls.G2.migrate
        (Proto_J.Script_typed_ir.Script_bls.G2.negate a) =
        Script_typed_ir.Script_bls.G2.negate
          (Script_typed_ir.Script_bls.G2.migrate a).
    Proof.
      destruct a; reflexivity.
    Qed.
  End G2.

  Module Fr.
    (** A fact about [Script_bls.Fr.add] migration. *)
    Lemma migrate_add_eq a b :
      Script_typed_ir.Script_bls.Fr.migrate
        (Script_typed_ir.Old.Script_bls.Fr.add a b) =
        Script_typed_ir.Script_bls.Fr.add
          (Script_typed_ir.Script_bls.Fr.migrate a)
          (Script_typed_ir.Script_bls.Fr.migrate b).
    Proof.
      destruct a, b; reflexivity.
    Qed.

    (** A fact about [Script_bls.Fr.mul] migration. *)
    Lemma migrate_mul_eq a b :
      Script_typed_ir.Script_bls.Fr.migrate
        (Script_typed_ir.Old.Script_bls.Fr.mul a b) =
        Script_typed_ir.Script_bls.Fr.mul
          (Script_typed_ir.Script_bls.Fr.migrate a)
          (Script_typed_ir.Script_bls.Fr.migrate b).
    Proof.
      destruct a, b; reflexivity.
    Qed.

    (** A fact about multiplication migration. *)
    Lemma migrate_mul_z_eq a b:
      Script_typed_ir.Script_bls.Fr.migrate
        (Proto_J.Script_typed_ir.Script_bls.Fr.Fr_tag
          (V5.Bls12_381.Fr.(V5.S.PRIME_FIELD.mul)
            (V5.Bls12_381.Fr.(V5.S.PRIME_FIELD.of_z)
              (Script_int_repr.to_zint b))
            a))
      = Script_typed_ir.Script_bls.Fr.Fr_tag
          (Fr.(PRIME_FIELD.mul)
            (Fr.(PRIME_FIELD.of_z)
              (Script_int.to_zint (Script_int_repr.migrate b)))
            a).
    Proof.
      destruct b; reflexivity.
    Qed.

    (** A fact about negation migration. *)
    Lemma migrate_negate a :
      Script_typed_ir.Script_bls.Fr.migrate
        (Proto_J.Script_typed_ir.Script_bls.Fr.negate a) =
        Script_typed_ir.Script_bls.Fr.negate
          (Script_typed_ir.Script_bls.Fr.migrate a).
    Proof.
      destruct a; reflexivity.
    Qed.
  End Fr.
End Script_bls.

(** [to_value] is backward_compatible *)
Lemma to_value_is_backward_compatible
  (t : Proto_J.Simulations.Script_family.Ty.t)
  (x : OldIR.With_family.ty_to_dep_Set t) :
  Ty.migrate_to_Set (OldIR.With_family.to_value x) =
  NewIR.With_family.to_value (Script_typed_ir.With_family.migrate_value x).
Proof.
  induction t; simpl; trivial.
  { destruct x. now f_equal. }
  { destruct x; simpl; now f_equal. }
  { admit. } (* TODO: Lambda *)
  { destruct x; simpl; now f_equal. }
  { cbn in x. destruct x. simpl. unfold Script_typed_ir.migrate_boxed_list.
    simpl. f_equal. induction elements; trivial. simpl. now f_equal.
  }
  { admit. } (* TODO: Set_ *)
  { admit. } (* TODO: Map *)
  { admit. } (* TODO: Big_map *)
  { admit. } (* TODO: Contract *)
  { cbn in x. destruct x.
    unfold Script_typed_ir.migrate_ticket, NewIR.With_family.ticket_map.
    simpl. now f_equal.
  }
Admitted.

(** [of_value] is backward compatible. *)
Lemma of_value_is_backward_compatible {a} (x : Old.Ty.to_Set a) :
  NewIR.With_family.of_value (Ty.migrate_to_Set x) =
  match OldIR.With_family.of_value x with
  | Some v => Some (Script_typed_ir.With_family.migrate_value v)
  | None => None
  end.
Proof.
  induction a; sauto.
Qed.

Module With_family.
  Module Old := Old.With_family.
  Module New := New.With_family.

  (** [With_family.to_ty] is backward compatible *)
  Lemma to_ty_is_backward_compatible
    {a : Proto_J.Simulations.Script_family.Ty.t} (t : Old.ty a) :
    Script_typed_ir.migrate_ty (Old.With_family.to_ty t) =
    New.With_family.to_ty (Script_typed_ir.With_family.migrate_ty t).
  Proof.
    (* TODO: define Script_typed_ir.migrate_ty *)
  Admitted.
End With_family.
