Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Tez_repr.
Require TezosOfOCaml.Proto_K.Tez_repr.
Require TezosOfOCaml.Proto_J_K.Tez_repr.

Module Old := TezosOfOCaml.Proto_J.Tez_repr.
Module New := TezosOfOCaml.Proto_K.Tez_repr.

(** The function [of_mutez] is backward compatible. *)
Lemma of_mutez_is_backward_compatible (n : int64) :
  (let* n := Old.of_mutez n in
  return* Tez_repr.migrate n) =
  New.of_mutez n.
Proof.
  unfold Old.of_mutez, New.of_mutez.
  destruct (_ <i64 _) eqn:?; reflexivity.
Qed.
