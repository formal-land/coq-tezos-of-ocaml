Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.

Require Proto_J.Simulations.Script_typed_ir.
Require Proto_J.Simulations.Script_map.

Require Proto_K.Simulations.Script_typed_ir.
Require Proto_K.Simulations.Script_map.

Require Proto_J_K.Simulations.Script_typed_ir.
Require Proto_J_K.Proofs.Script_typed_ir.
Require Proto_J_K.Proofs.Script_comparable.

Module OldIR := Proto_J.Simulations.Script_typed_ir.
Module NewIR := Proto_K.Simulations.Script_typed_ir.

Lemma dep_fold_is_backward_compatible_aux {a b}
  l
  (s : OldIR.With_family.ty_to_dep_Set
       (Proto_J.Simulations.Script_family.Ty.Map a b))
  :
  Script_map.dep_fold (fun k v acc => (k, v) :: acc)
  (Script_typed_ir.With_family.migrate_map Script_typed_ir.With_family.migrate_value s)
  (Lists.List.map (fun '(k, v) => (Script_typed_ir.With_family.migrate_value k, Script_typed_ir.With_family.migrate_value v)) l)
  = Lists.List.map (fun '(k,v) => 
      (Script_typed_ir.With_family.migrate_value k, Script_typed_ir.With_family.migrate_value v))
      (Proto_J.Simulations.Script_map.dep_fold (fun k v acc => (k, v) :: acc) s l).
Proof.
  revert l.
  unfold Script_map.dep_fold,
    Proto_J.Simulations.Script_map.dep_fold; simpl.
  unfold Script_typed_ir.With_family.Script_Map.
  unfold Proto_J.Simulations.Script_typed_ir.With_family.Script_Map.
  with_strategy transparent [V5.Map.Make]
    unfold V5.Map.Make; simpl.
  unfold Make.fold; simpl.
  unfold V5.List.fold_left.
  induction s; intros; [reflexivity|].
  simpl; Tactics.destruct_pairs.
  rewrite Script_typed_ir.of_value_is_backward_compatible.
  sauto.
Qed.

(** [dep_fold] is backward compatible. *)
Lemma dep_fold_is_backward_compatible {a b}
  (s : OldIR.With_family.ty_to_dep_Set
       (Proto_J.Simulations.Script_family.Ty.Map a b))
  :
  Script_map.dep_fold (fun k v acc => (k, v) :: acc)
  (Script_typed_ir.With_family.migrate_map Script_typed_ir.With_family.migrate_value s)
  nil
  = Lists.List.map (fun '(k,v) => 
      (Script_typed_ir.With_family.migrate_value k, Script_typed_ir.With_family.migrate_value v))
      (Proto_J.Simulations.Script_map.dep_fold (fun k v acc => (k, v) :: acc) s nil).
Proof.
  apply (dep_fold_is_backward_compatible_aux []).
Qed.

(** [dep_mem] is backward compatible. *)
Lemma dep_mem_is_backward_compatible {a b}
  (s : Proto_J.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a)
  (t : Proto_J.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
      (Proto_J.Simulations.Script_family.Ty.Map a b)) :
  Script_map.dep_mem
    (Script_typed_ir.With_family.migrate_value s)
    (Script_typed_ir.With_family.migrate_map
      Script_typed_ir.With_family.migrate_value t) =
  Proto_J.Simulations.Script_map.dep_mem s t.
Proof.
  unfold Script_map.dep_mem.
  unfold Proto_J.Simulations.Script_map.dep_mem.
  unfold Script_typed_ir.With_family.Script_Map.
  unfold Proto_J.Simulations.Script_typed_ir.With_family.Script_Map.
  with_strategy transparent [V5.Map.Make]
    unfold V5.Map.Make; simpl.
  unfold Script_typed_ir.With_family.migrate_map.
  induction t.
  { reflexivity. }
  { simpl.
    destruct a0; simpl.
    rewrite <- Script_typed_ir.to_value_is_backward_compatible.
    rewrite Script_comparable.compare_keys_is_backward_compatible.
    destruct V5.Map.Make.compare_keys; simpl;
    auto.
    rewrite Script_typed_ir.to_value_is_backward_compatible.
    exact IHt.
  }
Qed.

(** [dep_update] is backward compatible. *)
Lemma dep_update_is_backward_compatible {a b}
  (s0 : Proto_J.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a)
  (t : Proto_J.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
      (Proto_J.Simulations.Script_family.Ty.Option b))
  (t0 : Proto_J.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
       (Proto_J.Simulations.Script_family.Ty.Map a b)) :
  Script_typed_ir.With_family.migrate_map
    Script_typed_ir.With_family.migrate_value
    (Proto_J.Simulations.Script_map.dep_update s0 t t0) =
  Script_map.dep_update (Script_typed_ir.With_family.migrate_value s0)
    (match t with
     | Some x => Some (Script_typed_ir.With_family.migrate_value x)
     | None => None
     end)
    (Script_typed_ir.With_family.migrate_map
      Script_typed_ir.With_family.migrate_value t0).
Proof.
  unfold Script_typed_ir.With_family.migrate_map.
  unfold Proto_J.Simulations.Script_map.dep_update.
  unfold Script_map.dep_update; simpl.
  unfold Script_typed_ir.With_family.Script_Map.
  unfold Proto_J.Simulations.Script_typed_ir.With_family.Script_Map.
  with_strategy transparent [V5.Map.Make]
    unfold V5.Map.Make; simpl.
  destruct t; simpl.
  { induction t0.
    { simpl. repeat f_equal.
      now rewrite Script_typed_ir.to_value_is_backward_compatible.
    }
    { simpl. destruct a0.
      rewrite <- Script_typed_ir.to_value_is_backward_compatible.
      rewrite Script_comparable.compare_keys_is_backward_compatible.
      destruct V5.Map.Make.compare_keys; try reflexivity.
      simpl; rewrite IHt0.
      now rewrite Script_typed_ir.to_value_is_backward_compatible.
    }
  }
  { induction t0.
    { reflexivity. }
    { simpl. destruct a0.
      rewrite <- Script_typed_ir.to_value_is_backward_compatible.
      rewrite Script_comparable.compare_keys_is_backward_compatible.
      destruct V5.Map.Make.compare_keys; try reflexivity.
      simpl; rewrite IHt0.
      now rewrite Script_typed_ir.to_value_is_backward_compatible.
    }
  }
Qed.

(** [dep_get] is backward compatible. *)
Lemma dep_get_is_backward_compatible {a b}
  (s : Proto_J.Simulations.Script_typed_ir.With_family.ty_to_dep_Set a)
  (t : Proto_J.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
      (Proto_J.Simulations.Script_family.Ty.Map a b)) :
  match Proto_J.Simulations.Script_map.dep_get s t with
  | Some x => Some (Script_typed_ir.With_family.migrate_value x)
  | None => None
  end =
  Script_map.dep_get (Script_typed_ir.With_family.migrate_value s)
    (Script_typed_ir.With_family.migrate_map
       Script_typed_ir.With_family.migrate_value t).
Proof.
  unfold Script_typed_ir.With_family.migrate_map.
  unfold Proto_J.Simulations.Script_map.dep_get.
  unfold Script_map.dep_get; simpl.
  unfold Script_typed_ir.With_family.Script_Map.
  unfold Proto_J.Simulations.Script_typed_ir.With_family.Script_Map.
  with_strategy transparent [V5.Map.Make]
    unfold V5.Map.Make; simpl.
  induction t; [reflexivity|].
  simpl.
  Tactics.destruct_pairs.
  rewrite <- Script_typed_ir.to_value_is_backward_compatible.
  rewrite Script_comparable.compare_keys_is_backward_compatible.
  destruct V5.Map.Make.compare_keys; try reflexivity.
  rewrite IHt.
  now rewrite Script_typed_ir.to_value_is_backward_compatible.
Qed.
