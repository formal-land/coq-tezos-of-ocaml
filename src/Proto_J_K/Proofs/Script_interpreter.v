Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Environment.V6.Proofs.Bls12_381.
Require Import TezosOfOCaml.Proto_J_K.Proofs.Script_interpreter_defs.
Require Import TezosOfOCaml.Proto_J_K.Simulations.Script_family.
Require TezosOfOCaml.Proto_K.Proofs.Saturation_repr.
Require TezosOfOCaml.Proto_K.Proofs.Script_int.
Require TezosOfOCaml.Proto_K.Simulations.Script_interpreter.
Require TezosOfOCaml.Proto_J_K.Error.
Require TezosOfOCaml.Proto_J_K.Local_gas_counter.
Require TezosOfOCaml.Proto_J_K.Proofs.Alpha_context.
Require TezosOfOCaml.Proto_J_K.Proofs.Local_gas_counter.
Require TezosOfOCaml.Proto_J_K.Proofs.Sapling.
Require TezosOfOCaml.Proto_J_K.Proofs.Sapling_storage.
Require TezosOfOCaml.Proto_J_K.Proofs.Sapling_validator.
Require TezosOfOCaml.Proto_J_K.Proofs.Saturation_repr.
Require TezosOfOCaml.Proto_J_K.Proofs.Script_map.
Require TezosOfOCaml.Proto_J_K.Proofs.Script_set.
Require TezosOfOCaml.Proto_J_K.Proofs.Script_int_repr.
Require TezosOfOCaml.Proto_J_K.Proofs.Script_repr.
Require TezosOfOCaml.Proto_J_K.Proofs.Script_string_repr.
Require TezosOfOCaml.Proto_J_K.Proofs.Script_typed_ir.
Require TezosOfOCaml.Proto_J_K.Proofs.Tez_repr.
Require TezosOfOCaml.Proto_J_K.Proofs.Vote_storage.
Require TezosOfOCaml.Proto_J_K.Script_typed_ir.
Require TezosOfOCaml.Proto_J_K.Simulations.Script_typed_ir.
Require TezosOfOCaml.Proto_J.Proofs.Sapling_storage.
Require TezosOfOCaml.Proto_J.Sapling_storage.
Require TezosOfOCaml.Proto_J.Simulations.Script_interpreter.

Require Import TezosOfOCaml.Proto_J_K.Proofs.Script_ir_translator.
Require Import TezosOfOCaml.Proto_J_K.Proofs.Script_comparable.
Import Script_comparable.Simulations.

Require Import TezosOfOCaml.Proto_J.Script_interpreter.

Module OldIR := TezosOfOCaml.Proto_J.Simulations.Script_typed_ir.
Module NewIR := TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.

Module Old := TezosOfOCaml.Proto_J.Simulations.Script_interpreter.
Module New := TezosOfOCaml.Proto_K.Simulations.Script_interpreter.

(** [migrate_comb_gadt_witness] is backward_compatible *)
Fixpoint migrate_comb_gadt_witness
  (s u : Proto_J.Simulations.Script_family.Stack_ty.t)
  (c : Proto_J.Simulations.Script_typed_ir.With_family.comb_gadt_witness
         s u)
  (s0 : Proto_J.Simulations.Script_typed_ir
   .With_family.stack_ty_to_dep_Set_head s)
  (s1 : Proto_J.Simulations.Script_typed_ir
   .With_family.stack_ty_to_dep_Set_tail s)
  {struct c} :
  (New.eval_comb_gadt_witness
     (Script_typed_ir.With_family.migrate_comb_gadt_witness c)
          (Script_typed_ir.With_family.migrate_stack_value (s0, s1))) =
    (Script_typed_ir.With_family.migrate_stack_value
       (Old.eval_comb_gadt_witness c (s0, s1))).
Proof.
  destruct c as [|?x b s s' c0] eqn:?E; [reflexivity|]; simpl in *.
  destruct s1 as [m m0].
  change (Script_typed_ir.With_family.migrate_stack_value_head m,
           Script_typed_ir.With_family.migrate_stack_value_tail m0) with
    (Script_typed_ir.With_family.migrate_stack_value (m, m0)).
  pose proof (migrate_comb_gadt_witness _ _ c0 m m0).
  destruct
    New.eval_comb_gadt_witness eqn:Enew,
    Old.eval_comb_gadt_witness eqn:Eold.
  unfold  Script_typed_ir.With_family.migrate_stack_value.
  unfold Script_typed_ir.With_family.migrate_stack_value in H.
  simpl in H. injection H as H.
  simpl; repeat f_equal. apply H. apply H0.
Qed.
  
(** [migrate_uncomb_gadt_witness] is backward_compatible *)
Fixpoint migrate_uncomb_gadt_witness
  (s u : Proto_J.Simulations.Script_family.Stack_ty.t)
  (u0 : Proto_J.Simulations.Script_typed_ir.With_family.uncomb_gadt_witness
          s u)
  (s0 : Proto_J.Simulations.Script_typed_ir
   .With_family.stack_ty_to_dep_Set_head s)
  (s1 : Proto_J.Simulations.Script_typed_ir
   .With_family.stack_ty_to_dep_Set_tail s)
  {struct u0} :
  let accu_stack := (Old.eval_uncomb_gadt_witness u0 (s0, s1)) in
  (New.eval_uncomb_gadt_witness
     (Script_typed_ir.With_family.migrate_uncomb_gadt_witness u0)
          (Script_typed_ir.With_family.migrate_stack_value (s0, s1))) =
         (Script_typed_ir.With_family.migrate_stack_value accu_stack).
Proof.
  simpl in *.
  destruct u0 eqn:E. reflexivity.
  simpl in *. destruct s1. destruct s0. simpl in *.
  unfold Script_typed_ir.With_family.migrate_stack_value. simpl.
  f_equal.
  change (Script_typed_ir.With_family.migrate_stack_value_head m,
           Script_typed_ir.With_family.migrate_stack_value_tail m0) with
    (Script_typed_ir.With_family.migrate_stack_value (m, m0)).
  pose proof (migrate_uncomb_gadt_witness _ _ u1 t0 (m, m0)).
  simpl in *. destruct  Old.eval_uncomb_gadt_witness eqn:E1.
  simpl in *. hauto lq: on.
Qed.

(** The Michelson interpreter is backward compatible. *)
(** We assume that the gas cost for the continuations is backward compatible. *)
Axiom consume_control_is_backward_compatible :
  forall {s f}
    (gas : Proto_J.Local_gas_counter.local_gas_counter)
    (ks : Proto_J.Simulations.Script_typed_ir.With_family.continuation s f),
  (let* gas :=
    Proto_J.Script_interpreter_defs.consume_control gas
      (Proto_J.Simulations.Script_typed_ir.With_family.to_continuation
        ks) in
  Some (Local_gas_counter.migrate_local_gas_counter gas)) =
  Proto_K.Script_interpreter_defs.consume_control
    (Local_gas_counter.migrate_local_gas_counter gas)
    (Script_typed_ir.With_family.to_continuation
      (Script_typed_ir.With_family.migrate_continuation ks)).

Ltac apply_dep_step H_ind :=
  match goal with
  | |- context[Old.dep_step ?fuel (?ctxt, ?sc) ?gas ?i ?ks ?accu_stack] =>
    epose proof (
      H_ind _ _ _ fuel ctxt sc gas i ks accu_stack
    ) as H_step;
    rewrite H_step; f_equal;
    clear H_step
  end.

(** [eval_sppw_for_IDig] is backward compatible *)
Lemma eval_sppw_for_IDig_is_backward_compatible
  (X Y : Set) (s t u v : Proto_J.Simulations.Script_family.Stack_ty.t)
  (st : Script_typed_ir.Old.With_family.stack_ty u)
  (w : Proto_J.Simulations.Script_typed_ir.With_family
         .stack_prefix_preservation_witness s t u v)
  (f : Proto_J.Simulations.Script_typed_ir.With_family
         .stack_ty_to_dep_Set s ->
       X * Proto_J.Simulations.Script_typed_ir.With_family
         .stack_ty_to_dep_Set t)
  (g : Script_typed_ir.With_family.stack_ty_to_dep_Set
         (Script_family.Stack_ty.migrate s) ->
       Y * Script_typed_ir.With_family.stack_ty_to_dep_Set
             (Script_family.Stack_ty.migrate t))
  (migr : X -> Y)
  (x : Proto_J.Simulations.Script_typed_ir.With_family
         .stack_ty_to_dep_Set u) :
  (forall a,
    (let '(l,r) := f a
      in (migr l, Script_typed_ir.With_family.migrate_stack_value r)) =
    g (Script_typed_ir.With_family.migrate_stack_value a)) ->
  (let '(a,b) := Old.eval_sppw_for_IDig w f x
   in (migr a, Script_typed_ir.With_family.migrate_stack_value b)) =
  New.eval_sppw_for_IDig
    (Script_typed_ir.With_family.migrate_stack_prefix_preservation_witness
      st w)
    g (Script_typed_ir.With_family.migrate_stack_value x).
Proof.
  intros. revert f g x H. induction w; intros; cbn; trivial.
  specialize IHw
    with (Script_typed_ir.With_family.stack_ty_tail st) f g (snd x).
  destruct x, s1.
  unfold Script_typed_ir.With_family.migrate_stack_value in IHw.
  cbn in *. rewrite <- IHw; trivial.
  destruct Old.eval_sppw_for_IDig. f_equal. destruct s1.
  unfold Script_typed_ir.With_family.migrate_stack_value. cbn. trivial.
Qed.

(** [eval_stack_prefix_preservation_witness] is backward compatible *)
Lemma eval_stack_prefix_preservation_witness_is_backward_compatible
  (s t u v : Proto_J.Simulations.Script_family.Stack_ty.t)
  (st : Script_typed_ir.Old.With_family.stack_ty u)
  (w : Proto_J.Simulations.Script_typed_ir.With_family
         .stack_prefix_preservation_witness s t u v)
  (f : (Proto_J.Simulations.Script_typed_ir.With_family
         .stack_ty_to_dep_Set s ->
         Proto_J.Simulations.Script_typed_ir.With_family
           .stack_ty_to_dep_Set t))
  (g : (Simulations.Script_typed_ir.With_family.stack_ty_to_dep_Set
          (Script_family.Stack_ty.migrate s) ->
        Simulations.Script_typed_ir.With_family.stack_ty_to_dep_Set
          (Script_family.Stack_ty.migrate  t)))
  (x : Proto_J.Simulations.Script_typed_ir.With_family
         .stack_ty_to_dep_Set u)
  : (forall a, Script_typed_ir.With_family.migrate_stack_value (f a)
               = g (Script_typed_ir.With_family.migrate_stack_value a)) ->
    Script_typed_ir.With_family.migrate_stack_value
      (Old.eval_stack_prefix_preservation_witness w f x) =
      New.eval_stack_prefix_preservation_witness
        (Script_typed_ir.With_family
           .migrate_stack_prefix_preservation_witness st w)
        g (Script_typed_ir.With_family.migrate_stack_value x).
Proof.
  intros. revert x. induction w; cbn; intros; trivial.
  destruct x, p. simpl.
  specialize IHw
    with (Script_typed_ir.With_family.stack_ty_tail st) f g (m, m0).
  unfold Script_typed_ir.With_family.migrate_stack_value.
  unfold Script_typed_ir.With_family.migrate_stack_value in IHw. cbn in *.
  f_equal. rewrite <- IHw; trivial.
  now destruct Old.eval_stack_prefix_preservation_witness.
Qed.

(** [eval_sspw_for_IDropn] is backward compatible *)
Lemma eval_sspw_for_IDropn_is_backward_compatible
  (u s : Proto_J.Simulations.Script_family.Stack_ty.t)
  (st : Script_typed_ir.Old.With_family.stack_ty s)
  (w : Proto_J.Simulations.Script_typed_ir.With_family
         .stack_prefix_preservation_witness u u s s)
  (x : Proto_J.Simulations.Script_typed_ir.With_family
         .stack_ty_to_dep_Set s) :
  Script_typed_ir.With_family.migrate_stack_value
    (Old.eval_sspw_for_IDropn w x) =
  New.eval_sspw_for_IDropn
    (Script_typed_ir.With_family.migrate_stack_prefix_preservation_witness 
      st w)
    (Script_typed_ir.With_family.migrate_stack_value x).
Proof.
  revert x. dependent induction w; cbn; trivial.
  intros. destruct x. cbn.
  rewrite IHw with (st := Script_typed_ir.With_family.stack_ty_tail st);
  trivial.
  f_equal. now destruct p.
Qed.

(** The [dep_step] function is backward compatible, meaning that the dependent
    version of the Michelson interpreter is backward compatible. *)
Fixpoint dep_step_is_backward_compatible {s t f}
  fuel ctxt sc gas
  (i_value : Proto_J.Simulations.Script_typed_ir.With_family.kinstr s t)
  (ks : Proto_J.Simulations.Script_typed_ir.With_family.continuation t f)
  (accu_stack :
    Proto_J.Simulations.Script_typed_ir.With_family.stack_ty_to_dep_Set s)
  (H_valid_sc : Script_typed_ir.Step_constants.Valid.t sc)
  (H_valid_ks : Script_typed_ir.Continuation.Valid.t ks)
  {struct fuel} :
  Error.migrate_monad (Old.dep_step fuel (ctxt, sc) gas i_value ks accu_stack)
    (fun '(output, ctxt, gas) => (
      Script_typed_ir.With_family.migrate_stack_value output,
      Local_gas_counter.migrate_outdated_context ctxt,
      Local_gas_counter.migrate_local_gas_counter gas
    )) =
  New.dep_step
    fuel
    (
      Local_gas_counter.migrate_outdated_context ctxt,
      Script_typed_ir.migrate_step_constants sc
    )
    (Local_gas_counter.migrate_local_gas_counter gas)
    (Script_typed_ir.With_family.migrate_kinstr i_value)
    (Script_typed_ir.With_family.migrate_continuation ks)
    (Script_typed_ir.With_family.migrate_stack_value accu_stack)

(** The [dep_next] function is backward compatible. *)
with dep_next_is_backward_compatible {s f}
  fuel ctxt sc gas
  (ks : Proto_J.Simulations.Script_typed_ir.With_family.continuation s f)
  (accu_stack :
    Proto_J.Simulations.Script_typed_ir.With_family.stack_ty_to_dep_Set s)
  (H_valid_sc : Script_typed_ir.Step_constants.Valid.t sc)
  (H_valid_ks : Script_typed_ir.Continuation.Valid.t ks)
  {struct fuel} :
  Error.migrate_monad (Old.dep_next fuel (ctxt, sc) gas ks accu_stack)
    (fun '(output, ctxt, gas) => (
      Script_typed_ir.With_family.migrate_stack_value output,
      Local_gas_counter.migrate_outdated_context ctxt,
      Local_gas_counter.migrate_local_gas_counter gas
    )) =
  New.dep_next
    fuel
    (
      Local_gas_counter.migrate_outdated_context ctxt,
      Script_typed_ir.migrate_step_constants sc
    )
    (Local_gas_counter.migrate_local_gas_counter gas)
    (Script_typed_ir.With_family.migrate_continuation ks)
    (Script_typed_ir.With_family.migrate_stack_value accu_stack).
Proof.  
  (* dep_step *)
{ destruct fuel; [reflexivity|].
  pose proof (dep_consume_instr_is_backward_compatible
    gas i_value accu_stack)
    as Hconsume.
  destruct accu_stack; destruct i_value.
  { grep @OldIR.With_family.IDrop.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IDup.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ISwap.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IConst.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ICons_pair.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ICar.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    step.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ICdr.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    step.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IUnpair.

    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    step.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ICons_some.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ICons_none.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IIf_none.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    step; now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IOpt_map.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    step; now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ICons_left.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ICons_right.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IIf_left.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    step; now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ICons_list.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.INil.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IIf_cons.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    step; now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IList_map.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_next_is_backward_compatible.
  }
  { grep @OldIR.With_family.IList_iter.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_next_is_backward_compatible.
  }
  { grep @OldIR.With_family.IList_size.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IEmpty_set.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ISet_iter.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    fold @Script_family.Ty.migrate.
    rewrite dep_next_is_backward_compatible; [|auto|apply axiom]. (*TODO*)
    rewrite Script_set.dep_fold_is_backward_compatible; simpl.
    now rewrite <- List.rev_map_eq.
  }
  { grep @OldIR.With_family.ISet_mem.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite dep_step_is_backward_compatible; [|auto|apply axiom]. (*TODO*)
    rewrite Script_set.dep_mem_is_backward_compatible.
    reflexivity.
  }
  { grep @OldIR.With_family.ISet_update.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    fold @Script_family.Ty.migrate.
    rewrite dep_step_is_backward_compatible; [|auto|apply axiom]. (*TODO*)
    now rewrite <- Script_set.dep_update_is_backward_compatible.
  }
  { grep @OldIR.With_family.ISet_size.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    unfold Script_set.dep_size_value.
    rewrite Script_typed_ir.migrate_set_cardinal.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IEmpty_map.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IMap_map.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite dep_next_is_backward_compatible; [|auto|apply axiom]. (*TODO*)
    rewrite Script_map.dep_fold_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_continuation.
    now rewrite <- List.rev_map_eq.
  }
  { grep @OldIR.With_family.IMap_iter.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite dep_next_is_backward_compatible; [|auto|apply axiom]. (*TODO*)
    rewrite Script_map.dep_fold_is_backward_compatible; simpl.
    now rewrite <- List.rev_map_eq.
  }
  { grep @OldIR.With_family.IMap_mem.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite dep_step_is_backward_compatible; [|auto|apply axiom]. (*TODO*)
    now rewrite Script_map.dep_mem_is_backward_compatible.
  }
  { grep @OldIR.With_family.IMap_get.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite dep_step_is_backward_compatible; [|auto|apply axiom]. (*TODO*)
    unfold Script_typed_ir.With_family.migrate_stack_value; simpl.
    now rewrite <- Script_map.dep_get_is_backward_compatible.
  }
  { grep @OldIR.With_family.IMap_update.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite dep_step_is_backward_compatible; [|auto|apply axiom]. (*TODO*)
    unfold Script_typed_ir.With_family.migrate_stack_value; simpl.
    now rewrite Script_map.dep_update_is_backward_compatible.
  }
  { grep @OldIR.With_family.IMap_get_and_update.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    fold @Script_family.Ty.migrate.
    rewrite dep_step_is_backward_compatible; [|auto|apply axiom]. (*TODO*)
    unfold Script_typed_ir.With_family.migrate_stack_value; simpl.
    rewrite <- Script_map.dep_get_is_backward_compatible.
    now rewrite Script_map.dep_update_is_backward_compatible.
  }
  { grep @OldIR.With_family.IMap_size.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite dep_step_is_backward_compatible; [|auto|apply axiom]. (*TODO*)
    unfold Script_typed_ir.With_family.migrate_stack_value.
    simpl.
    unfold Script_map.dep_size_value.
    now rewrite Script_typed_ir.migrate_map_cardinal.
  }
  { grep @OldIR.With_family.IEmpty_big_map.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now rewrite dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IBig_map_mem.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    unfold Proto_J.Local_gas_counter.use_gas_counter_in_context.
    unfold Local_gas_counter.use_gas_counter_in_context.
    rewrite <- dep_big_map_mem_is_backward_compatible.
    destruct Proto_J.Simulations.Script_ir_translator.dep_big_map_mem;
      [|reflexivity].
    Tactics.destruct_pairs; simpl.
    now rewrite dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IBig_map_get.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    unfold Local_gas_counter.use_gas_counter_in_context.
    unfold Proto_J.Local_gas_counter.use_gas_counter_in_context.
    rewrite <- dep_big_map_get_is_backward_compatible; simpl.
    destruct Proto_J.Simulations.Script_ir_translator.dep_big_map_get;
      [simpl|reflexivity].
    Tactics.destruct_pairs.
    destruct o; simpl;
    now rewrite dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IBig_map_update.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    unfold Local_gas_counter.use_gas_counter_in_context.
    unfold Proto_J.Local_gas_counter.use_gas_counter_in_context.
    rewrite <- (dep_big_map_update_is_backward_compatible
        ctxt l s0 t); simpl.
    destruct Proto_J.Simulations.Script_ir_translator.dep_big_map_update;
      [simpl|reflexivity].
    Tactics.destruct_pairs; simpl.
    now rewrite dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IBig_map_get_and_update.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    unfold Local_gas_counter.use_gas_counter_in_context.
    unfold Proto_J.Local_gas_counter.use_gas_counter_in_context.
    rewrite <- (dep_big_map_get_and_update_is_backward_compatible
        ctxt l s0 t); simpl.
    destruct Proto_J.Simulations.Script_ir_translator.dep_big_map_get_and_update;
      [simpl|reflexivity].
    Tactics.destruct_pairs; simpl.
    destruct o; simpl;
    now rewrite dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IConcat_string.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite <- Local_gas_counter.consume_is_backward_compatible.
    unfold Proto_J.Saturation_repr.zero, Saturation_repr.zero.
    rewrite List.fold_left_map_eq.
    rewrite List.fold_left_f_eq
      with (g :=
        (fun (acc : int)
             (x : Script_typed_ir.Old.With_family.ty_to_dep_Set
                    Proto_J.Simulations.Script_family.Ty.String) =>
           Saturation_repr.add acc
             (Saturation_repr.safe_int
                (Script_string.length (Script_string_repr.migrate x))))).
    { rewrite Script_interpreter_defs.Interp_costs
                .concat_string_is_backward_compatible.
      destruct Proto_J.Local_gas_counter.consume; simpl; trivial.
      rewrite <- Script_string_repr.concat_is_backward_compatible.
      now apply dep_step_is_backward_compatible.
    }
    { extensionality acc. extensionality x.
      now rewrite Script_string_repr.length_is_backward_compatible,
                  Saturation_repr.safe_int_is_backward_compatible,
                  Saturation_repr.add_is_backward_compatible.
    }
  }
  { grep @OldIR.With_family.IConcat_string_pair.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    apply_dep_step dep_step_is_backward_compatible; [|easy..].
    cbn in *.
    destruct_all Script_string_repr.t.
    now reflexivity.
  }
  { grep @OldIR.With_family.ISlice_string.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    match goal with
    | |- ?e1 = ?e2 =>
      match e1 with
      | context[if ?e1 then _ else _] =>
        match e2 with
        | context[if ?e2 then _ else _] =>
          replace e1 with e2;
          [destruct e2|]
        end
      end
    end;
      cbn in *;
      destruct_all Script_string_repr.t;
      destruct_all Script_int_repr.num;
      trivial;
      now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IString_size.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn in *.
    destruct_all Script_string_repr.t.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IConcat_bytes.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite <- Local_gas_counter.consume_is_backward_compatible.
    unfold Proto_J.Saturation_repr.zero, Saturation_repr.zero.
    rewrite List.fold_left_map_eq.
    rewrite List.fold_left_f_eq
      with (g :=
        (fun (acc : int)
             (x : Script_typed_ir.Old.With_family.ty_to_dep_Set
                  Proto_J.Simulations.Script_family.Ty.Bytes) =>
           Saturation_repr.add acc
             (Saturation_repr.safe_int (Bytes.length x)))).
    { rewrite Script_interpreter_defs.Interp_costs
                .concat_string_is_backward_compatible.
      destruct Proto_J.Local_gas_counter.consume; simpl; trivial.
      rewrite dep_step_is_backward_compatible; trivial. f_equal.
      unfold Script_typed_ir.With_family.migrate_stack_value. simpl. f_equal.
      f_equal. destruct s0. cbn. fold (@id V5.Bytes.t). clear.
      induction elements; simpl; trivial. f_equal. apply IHelements.
    }
    { intros. now rewrite Saturation_repr.safe_int_is_backward_compatible. }
  }
  { grep @OldIR.With_family.IConcat_bytes_pair.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ISlice_bytes.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn in *.
    destruct_all Script_int_repr.num.
    simpl.
    destruct (_ && _); now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IBytes_size.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IAdd_seconds_to_timestamp.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn in *.
    destruct_all Script_int_repr.num.
    destruct_all Script_timestamp_repr.t.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IAdd_timestamp_to_seconds.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn in *.
    destruct_all Script_int_repr.num.
    destruct_all Script_timestamp_repr.t.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ISub_timestamp_seconds.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn in *.
    destruct_all Script_int_repr.num.
    destruct_all Script_timestamp_repr.t.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IDiff_timestamps.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn in *.
    destruct_all Script_timestamp_repr.t.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IAdd_tez.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn in *.
    destruct_all Proto_J.Tez_repr.t.
    simpl.
    destruct (_ <? _); cbn.
    { now rewrite cast_eval. }
    { now apply dep_step_is_backward_compatible. }
  }
  { grep @OldIR.With_family.ISub_tez.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn in *.
    destruct_all Proto_J.Tez_repr.t.
    simpl.
    destruct (_ <=? _); now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ISub_tez_legacy.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn in *.
    destruct_all Proto_J.Tez_repr.t.
    simpl.
    destruct (_ <=? _); cbn.
    { now apply dep_step_is_backward_compatible. }
    { now rewrite cast_eval. }
  }
  { grep @OldIR.With_family.IMul_teznat.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn in *.
    destruct_all Proto_J.Script_int_repr.num.
    destruct_all Proto_J.Tez_repr.t.
    simpl.
    destruct Option.catch; cbn.
    { step; cbn; [now rewrite cast_eval|].
      step; cbn; [now apply dep_step_is_backward_compatible|].
      step; cbn; [now rewrite cast_eval|].
      now apply dep_step_is_backward_compatible.
    }
    { dep_destruct k; cbn.
      now repeat rewrite cast_eval.
    }
  }
  { grep @OldIR.With_family.IMul_nattez.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn in *.
    destruct_all Proto_J.Script_int_repr.num.
    destruct_all Proto_J.Tez_repr.t.
    simpl.
    step; cbn; [|dep_destruct k; cbn; now repeat rewrite cast_eval].
    step; cbn; [now rewrite cast_eval|].
    step; simpl; [now apply dep_step_is_backward_compatible|].
    step; cbn; [now rewrite cast_eval|].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IEdiv_teznat.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn in *.
    destruct_all Proto_J.Script_int_repr.num.
    destruct_all Proto_J.Tez_repr.t.
    simpl.
    apply_dep_step dep_step_is_backward_compatible; [|easy..].
    step; simpl.
    unfold Proto_J.Tez_repr.of_mutez, Proto_K.Tez_repr.of_mutez.
    repeat rewrite Option.catch_no_errors; cbn.
    step; cbn; [|easy].
    step; cbn; [|easy].
    destruct (_ <? _); [easy|].
    now destruct (_ <? _).
  }
  { grep @OldIR.With_family.IEdiv_tez.
    with_strategy opaque [Script_int_repr.ediv_n] (
      simpl in *; Tactics.destruct_pairs;
      rewrite <- Hconsume;
      destruct Old.dep_consume_instr; simpl; [|reflexivity];
      cbn in *
    ).
    unfold Script_int_repr.ediv_n.
    match goal with
    | |- context[Script_int_repr.ediv ?a ?b] =>
      pose proof (Script_int_repr.ediv_is_backward_compatible a b) as H_ediv
    end.
    destruct_all Proto_J.Tez_repr.t.
    with_strategy opaque [Script_int_repr.ediv] simpl in *.
    destruct Script_int_repr.ediv as [[quo rem]|]; simpl in *.
    { rewrite <- H_ediv; simpl.
      rewrite Script_int_repr.to_int64_is_backward_compatible.
      destruct Script_int.to_int64;
        [|now apply dep_step_is_backward_compatible].
      rewrite <- Tez_repr.of_mutez_is_backward_compatible.
      destruct Proto_J.Tez_repr.of_mutez;
        [|now apply dep_step_is_backward_compatible].
      now apply dep_step_is_backward_compatible.
    }
    { rewrite <- H_ediv.
      now apply dep_step_is_backward_compatible.
    }
  }
  { grep @OldIR.With_family.IOr.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IAnd.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IXor.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.INot.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IIs_nat.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    specialize dep_step_is_backward_compatible
      with _ _ _ fuel ctxt sc l i_value ks
           (Script_int_repr.is_nat s0, (m, m0)).
    unfold Script_int.is_nat.
    unfold Script_int_repr.is_nat.
    unfold Script_int_repr.is_nat in dep_step_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_stack_value
      in dep_step_is_backward_compatible.
    simpl in *. unfold Z.zero, V5.Z.zero in *. step.
    destruct (r <? 0) eqn:Heqb; simpl in *; rewrite Heqb;
      now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.INeg.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    specialize dep_step_is_backward_compatible
      with _ _ _ fuel ctxt sc l i_value ks
           (Script_int_repr.neg s0, (m, m0)).
    unfold Script_int.neg.
    unfold Script_int_repr.neg.
    unfold Script_int_repr.neg in dep_step_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_stack_value
      in dep_step_is_backward_compatible.
    simpl in *. unfold neg, V5.Z.neg in *. step. simpl in *.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IAbs_int.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    specialize dep_step_is_backward_compatible
      with _ _ _ fuel ctxt sc l i_value ks
           (Script_int_repr.abs s0, (m, m0)).
    unfold Script_int.abs.
    unfold Script_int_repr.abs.
    unfold Script_int_repr.abs in dep_step_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_stack_value
      in dep_step_is_backward_compatible.
    simpl in *. unfold abs, V5.Z.abs in *. step. simpl in *.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IInt_nat.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IAdd_int.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    specialize dep_step_is_backward_compatible
      with _ _ _ fuel ctxt sc l i_value ks
           (Script_int_repr.add s0 t, (m, m0)).
    unfold Script_int.add.
    unfold Script_int_repr.add.
    unfold Script_int_repr.add in dep_step_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_stack_value in *.
    simpl in *. unfold Script_int_repr.migrate in *. step. step.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IAdd_nat.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    specialize dep_step_is_backward_compatible
      with _ _ _ fuel ctxt sc l i_value ks
           (Script_int_repr.add_n s0 t, (m, m0)).
    unfold Script_int.add_n, Script_int.add.
    unfold Script_int_repr.add_n, Script_int_repr.add.
    unfold Script_int_repr.add_n, Script_int_repr.add
      in dep_step_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_stack_value in *.
    simpl in *. unfold Script_int_repr.migrate in *. step. step.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ISub_int.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    specialize dep_step_is_backward_compatible
      with _ _ _ fuel ctxt sc l i_value ks
           (Script_int_repr.sub s0 t, (m, m0)).
    unfold Script_int.sub.
    unfold Script_int_repr.sub.
    unfold Script_int_repr.sub in dep_step_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_stack_value in *.
    simpl in *. unfold Script_int_repr.migrate in *. step. step.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IMul_int.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    specialize dep_step_is_backward_compatible
      with _ _ _ fuel ctxt sc l i_value ks
           (Script_int_repr.mul s0 t, (m, m0)).
    unfold Script_int.mul.
    unfold Script_int_repr.mul.
    unfold Script_int_repr.mul in dep_step_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_stack_value in *.
    simpl in *. unfold Script_int_repr.migrate in *. step. step.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IMul_nat.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    specialize dep_step_is_backward_compatible
      with _ _ _ fuel ctxt sc l i_value ks
           (Script_int_repr.mul_n s0 t, (m, m0)).
    unfold Script_int.mul_n, Script_int.mul.
    unfold Script_int_repr.mul_n, Script_int_repr.mul.
    unfold Script_int_repr.mul_n, Script_int_repr.mul
      in dep_step_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_stack_value in *.
    simpl in *. unfold Script_int_repr.migrate in *. step. step.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IEdiv_int.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn in *.
    destruct_all Script_int_repr.num; simpl.
    repeat rewrite Option.catch_no_errors.
    destruct Z.ediv_rem.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IEdiv_nat.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn in *.
    destruct_all Script_int_repr.num; simpl.
    repeat rewrite Option.catch_no_errors.
    destruct Z.ediv_rem.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ILsl_nat.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    unfold Script_typed_ir.With_family.migrate_stack_value
      in dep_step_is_backward_compatible.
    step; step.
    { specialize dep_step_is_backward_compatible
        with _ _ _ fuel ctxt sc l i_value ks (n, (m, m0)).
      simpl in *.
      assert (Script_int_repr.migrate n = n0).
      { unfold Alpha_context.Script_int.shift_left_n,
               Alpha_context.Script_int.shift_left in Heqo.
        unfold Script_int.shift_left_n,
               Script_int.shift_left in Heqo0.
        unfold Script_int_repr.migrate in Heqo0.
        destruct s0, t.
        destruct (V5.Z.compare r0 (V5.Z.of_int 256) >i 0); [ discriminate |].
        inversion Heqo. inversion Heqo0. now simpl.
      }
      { rewrite H in dep_step_is_backward_compatible.
        now apply dep_step_is_backward_compatible.
      }
    }
    { unfold Alpha_context.Script_int.shift_left_n,
             Alpha_context.Script_int.shift_left in Heqo.
      unfold Script_int.shift_left_n,
             Script_int.shift_left in Heqo0.
      unfold Script_int_repr.migrate in Heqo0.
      destruct s0, t.
      destruct (V5.Z.compare r0 (V5.Z.of_int 256) >i 0); discriminate.
    }
    { unfold Alpha_context.Script_int.shift_left_n,
             Alpha_context.Script_int.shift_left in Heqo.
      unfold Script_int.shift_left_n,
             Script_int.shift_left in Heqo0.
      unfold Script_int_repr.migrate in Heqo0.
      destruct s0, t.
      destruct (V5.Z.compare r0 (V5.Z.of_int 256) >i 0); discriminate.
    }
    { clear dep_step_is_backward_compatible dep_next_is_backward_compatible
            Hconsume Heqo Heqo0.
      step. unfold V5.Error_monad.fail, V5.Error_monad.error_value,
                   Error_monad.fail, Error_monad.error_value.
      simpl. unfold Error.migrate. cbn. step. simpl.
      unfold Script_interpreter.OldScR.location in Heqp, l1.
      unfold Proto_J.Alpha_context.Script.location in l0.
      rewrite cast_eval in Heqp. inversion Heqp. simpl.
      unfold Script_interpreter.NewScR.location, canonical_location.
      f_equal. f_equal. f_equal. f_equal. apply cast_eval.
    }
  }
  { grep @OldIR.With_family.ILsr_nat.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    unfold Script_typed_ir.With_family.migrate_stack_value
      in dep_step_is_backward_compatible.
    step; step.
    { specialize dep_step_is_backward_compatible
        with _ _ _ fuel ctxt sc l i_value ks (n, (m, m0)).
      simpl in *.
      assert (Script_int_repr.migrate n = n0).
      { unfold Alpha_context.Script_int.shift_right_n,
               Alpha_context.Script_int.shift_right in Heqo.
        unfold Script_int.shift_right_n,
               Script_int.shift_right in Heqo0.
        unfold Script_int_repr.migrate in Heqo0.
        destruct s0, t.
        destruct (V5.Z.compare r0 (V5.Z.of_int 256) >i 0); [ discriminate |].
        inversion Heqo. inversion Heqo0. now simpl.
      }
      { rewrite H in dep_step_is_backward_compatible.
        now apply dep_step_is_backward_compatible.
      }
    }
    { unfold Alpha_context.Script_int.shift_right_n,
             Alpha_context.Script_int.shift_right in Heqo.
      unfold Script_int.shift_right_n,
             Script_int.shift_right in Heqo0.
      unfold Script_int_repr.migrate in Heqo0.
      destruct s0, t.
      destruct (V5.Z.compare r0 (V5.Z.of_int 256) >i 0); discriminate.
    }
    { unfold Alpha_context.Script_int.shift_right_n,
             Alpha_context.Script_int.shift_right in Heqo.
      unfold Script_int.shift_right_n,
             Script_int.shift_right in Heqo0.
      unfold Script_int_repr.migrate in Heqo0.
      destruct s0, t.
      destruct (V5.Z.compare r0 (V5.Z.of_int 256) >i 0); discriminate.
    }
    { clear dep_step_is_backward_compatible dep_next_is_backward_compatible
            Hconsume Heqo Heqo0.
      step. unfold Error_monad.fail, Error_monad.error_value.
      simpl. unfold Error.migrate. cbn. step. simpl.
      unfold Script_interpreter.OldScR.location in Heqp, l1.
      unfold Proto_J.Alpha_context.Script.location in l0.
      rewrite cast_eval in Heqp. inversion Heqp. simpl.
      unfold Script_interpreter.NewScR.location, canonical_location.
      f_equal. f_equal. f_equal. f_equal. apply cast_eval.
    }
  }
  { grep @OldIR.With_family.IOr_nat.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    specialize dep_step_is_backward_compatible
      with _ _ _ fuel ctxt sc l i_value ks
           (Script_int_repr.logor s0 t, (m, m0)).
    unfold Script_int.logor.
    unfold Script_int_repr.logor.
    unfold Script_int_repr.logor in dep_step_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_stack_value in *.
    simpl in *. unfold Script_int_repr.migrate in *. step. step.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IAnd_nat.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    specialize dep_step_is_backward_compatible
      with _ _ _ fuel ctxt sc l i_value ks
           (Script_int_repr.logand s0 t, (m, m0)).
    unfold Script_int.logand.
    unfold Script_int_repr.logand.
    unfold Script_int_repr.logand in dep_step_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_stack_value in *.
    simpl in *. unfold Script_int_repr.migrate in *. step. step.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IAnd_int_nat.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    specialize dep_step_is_backward_compatible
      with _ _ _ fuel ctxt sc l i_value ks
           (Script_int_repr.logand s0 t, (m, m0)).
    unfold Script_int.logand.
    unfold Script_int_repr.logand.
    unfold Script_int_repr.logand in dep_step_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_stack_value in *.
    simpl in *. unfold Script_int_repr.migrate in *. step. step.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IXor_nat.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    specialize dep_step_is_backward_compatible
      with _ _ _ fuel ctxt sc l i_value ks
           (Script_int_repr.logxor s0 t, (m, m0)).
    unfold Script_int.logxor.
    unfold Script_int_repr.logxor.
    unfold Script_int_repr.logxor in dep_step_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_stack_value in *.
    simpl in *. unfold Script_int_repr.migrate in *. step. step.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.INot_int.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    specialize dep_step_is_backward_compatible
      with _ _ _ fuel ctxt sc l i_value ks
           (Script_int_repr.lognot s0, (m, m0)).
    unfold Script_int.lognot.
    unfold Script_int_repr.lognot.
    unfold Script_int_repr.lognot in dep_step_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_stack_value in *.
    simpl in *. unfold Script_int_repr.migrate in *. step.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IIf.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    step; now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ILoop.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_next_is_backward_compatible.
  }
  { grep @OldIR.With_family.ILoop_left.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_next_is_backward_compatible.
  }
  { grep @OldIR.With_family.IDip.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IExec.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    step. simpl.
    specialize dep_step_is_backward_compatible
      with _ _ _ fuel ctxt sc l
      s1.(Proto_J.Simulations.Script_typed_ir.With_family.kdescr.kinstr)
      (Proto_J.Simulations.Script_typed_ir.With_family.KReturn (m, m0)
        (Proto_J.Simulations.Script_typed_ir.With_family.KCons i_value ks))
      (s0, (Proto_J.Script_typed_ir.EmptyCell,
            Proto_J.Script_typed_ir.EmptyCell)).
    rewrite dep_step_is_backward_compatible by easy; trivial.
    f_equal. destruct s1. now simpl.
  }
  { grep @OldIR.With_family.IApply.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    apply axiom. (* TODO: define Simulations.Script_interpreter.dep_apply *)
  }
  { grep @OldIR.With_family.ILambda.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IFailwith.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite <- Local_gas_counter.update_context_is_backward_compatible.
    rewrite <- dep_unparse_data_optimized_is_backward_compatible.
    destruct OldS.dep_unparse_data; cbn; [ step; cbn | trivial ].
    rewrite cast_eval. simpl. unfold Error_monad.fail, Error_monad.error_value.
    repeat f_equal.
    clear dep_step_is_backward_compatible dep_next_is_backward_compatible
      a s k l t s0 fuel ctxt sc gas m m0 l0 p c H_valid_sc Hconsume Heqp0.
    revert n. fix IHn 1.
    destruct n; simpl; trivial; f_equal; revert l0; fix IHl0 1; destruct l0;
    simpl in *; trivial; (f_equal; [ apply IHn | apply IHl0 ]).
  }
  { grep @OldIR.With_family.ICompare.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite dep_step_is_backward_compatible; trivial. f_equal.
    unfold Script_typed_ir.With_family.migrate_stack_value. simpl. f_equal.
    unfold V5.Z.of_int, Script_int.of_int, of_int. f_equal.
    rewrite dep_compare_comparable_is_backward_compatible.
    f_equal; apply Script_typed_ir.to_value_is_backward_compatible.
  }
  { grep @OldIR.With_family.IEq.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    unfold Script_int_repr.compare, Script_int.compare.
    step. simpl. unfold V5.Z.compare, V5.Z.zero, Z.compare, Z.zero.
    destruct (_ =i _); now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.INeq.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    unfold Script_int_repr.compare, Script_int.compare.
    step. simpl. unfold V5.Z.compare, V5.Z.zero, Z.compare, Z.zero.
    destruct (_ <>i _); now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ILt.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    unfold Script_int_repr.compare, Script_int.compare.
    step. simpl. unfold V5.Z.compare, V5.Z.zero, Z.compare, Z.zero.
    destruct (_ <i _); now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IGt.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    unfold Script_int_repr.compare, Script_int.compare.
    step. simpl. unfold V5.Z.compare, V5.Z.zero, Z.compare, Z.zero.
    destruct (_ >i _); now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ILe.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    unfold Script_int_repr.compare, Script_int.compare.
    step. simpl. unfold V5.Z.compare, V5.Z.zero, Z.compare, Z.zero.
    destruct (_ <=i _); now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IGe.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    unfold Script_int_repr.compare, Script_int.compare.
    step. simpl. unfold V5.Z.compare, V5.Z.zero, Z.compare, Z.zero.
    destruct (_ >=i _); now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IAddress.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    step. cbn.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IContract.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    cbn.
    destruct V5.String.compare; cbn;
    [| destruct V5.String.compare; cbn
    | destruct V5.String.compare; cbn ];
    (try now apply dep_step_is_backward_compatible);
    ( clear gas Hconsume; destruct k; cbn;
      rewrite <- Local_gas_counter.update_context_is_backward_compatible;
      rewrite <- Script_ir_translator
                   .dep_parse_contract_for_script_is_backward_compatible;
      destruct Script_ir_translator.Old.dep_parse_contract_for_script; cbn;
      trivial
    ); (destruct p0 || destruct p); now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IView.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    destruct v. cbn.
    apply axiom. (* TODO: define IView in New.dep_step *)
  }
  { grep @OldIR.With_family.ITransfer_tokens.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    dependent destruction k. destruct t0. cbn.
    pose
      (@Script_interpreter_defs.dep_transfer_is_backward_compatible
         a (ctxt, sc)).
    simpl in e. rewrite <- e. destruct Old.dep_transfer; cbn; trivial.
    destruct p, p. now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IImplicit_account.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ICreate_contract.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite <- Local_gas_counter.update_context_is_backward_compatible.
    rewrite <- Script_typed_ir.With_family.to_ty_is_backward_compatible.
    rewrite
      <- Script_ir_translator
           .collect_lazy_storage_is_backward_compatible_for_ty_to_dep_Set.
    destruct Old.collect_lazy_storage; cbn; trivial.
    destruct p. cbn.
    assert
      (Lazy_storage_kind.IdSet.migrate
         Proto_J.Script_ir_translator.no_lazy_storage_id
         = Script_ir_translator.no_lazy_storage_id) by hauto l: on.
    rewrite <- H.
    rewrite <- extract_lazy_storage_diff_is_backward_compatible_special_case.
    destruct Old.extract_lazy_storage_diff; cbn; trivial.
    destruct p, p.
    rewrite <- unparse_data_special_case_is_backward_compatible.
    destruct Old.unparse_data; cbn; trivial.
    destruct p.
    unfold Proto_J.Alpha_context.Gas.consume, Alpha_context.Gas.consume.
    rewrite <- Raw_context.consume_gas_is_backward_compatible.
    rewrite <- @Script_repr.strip_locations_cost_is_backward_compatible.
    destruct Raw_context.Old.consume_gas; cbn; trivial.
    unfold Proto_J.Contract_storage.fresh_contract_from_current_nonce,
           Contract_storage.fresh_contract_from_current_nonce.
    rewrite <- Raw_context.increment_origination_nonce_is_backward_compatible.
    destruct Raw_context.Old.increment_origination_nonce; simpl; trivial.
    destruct p. simpl.
    rewrite <- Raw_context.fresh_internal_nonce_is_backward_compatible.
    destruct Raw_context.Old.fresh_internal_nonce; simpl; trivial.
    destruct p. simpl.
    rewrite <- Local_gas_counter.local_gas_counter_value_is_backward_compatible.
    rewrite dep_step_is_backward_compatible; trivial. f_equal.
    unfold Script_typed_ir.With_family.migrate_stack_value. cbn. f_equal.
    { apply axiom. (* TODO: define Script_typed_ir.migrate_operation *) }
    { f_equal. unfold Script_typed_ir.migrate_address. simpl. f_equal. f_equal.
      f_equal. unfold Contract_hash.of_nonce.
      unfold Proto_J.Contract_hash.hash_bytes, Contract_hash.hash_bytes.
      unfold Proto_J.Contract_hash.H, Contract_hash.H. simpl.
      unfold Proto_J.Contract_hash.contract_hash,
             Contract_hash.contract_hash.
      f_equal. f_equal.
      unfold Proto_J.Origination_nonce.encoding,
             Origination_nonce.encoding.
      symmetry. unfold Binary.to_bytes_exn.
      now rewrite Data_encoding.Valid.conv_and_migrate_to_bytes_opt,
                  Data_encoding.Valid.conv_and_migrate_to_bytes_opt.
    }
  }
  { grep @OldIR.With_family.ISet_delegate.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite <- Local_gas_counter.update_context_is_backward_compatible.
    rewrite <- Raw_context.fresh_internal_nonce_is_backward_compatible.
    destruct Raw_context.Old.fresh_internal_nonce; simpl; trivial.
    destruct p. simpl.
    rewrite <- Local_gas_counter.outdated_context_value_is_backward_compatible.
    rewrite
      <- Local_gas_counter.local_gas_counter_value_is_backward_compatible.
    rewrite dep_step_is_backward_compatible; trivial. f_equal.
    unfold Script_typed_ir.With_family.migrate_stack_value. cbn. f_equal.
    apply axiom. (* TODO: define Script_typed_ir.migrate_operation *)
  }
  { grep @OldIR.With_family.INow.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IMin_block_time.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite <- Local_gas_counter.update_context_is_backward_compatible.
    unfold Proto_J.Constants_storage.minimal_block_delay,
           Constants_storage.minimal_block_delay.
    destruct Proto_J.Local_gas_counter.update_context.
    cbn. now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IBalance.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite <- Local_gas_counter.update_context_is_backward_compatible.
    rewrite <- Local_gas_counter.outdated_context_value_is_backward_compatible.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ILevel.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ICheck_signature.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    unfold Proto_J.Script_typed_ir.Script_signature.check,
           Script_typed_ir.Script_signature.check.
    destruct t. simpl. now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IHash_key.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IPack.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; [|reflexivity]; simpl.
    rewrite
      <- (Local_gas_counter.use_gas_counter_in_context_is_backward_compatible
           _ _ (fun ct : Proto_J.Alpha_context.context =>
                  (Proto_J.Simulations.Script_ir_translator.dep_pack_data
                     ct t s0)))
      by (
        intros;
        rewrite <- Script_ir_translator.dep_pack_data_is_backward_compatible;
        destruct OldS.dep_pack_data;
        reflexivity
      ).
    step; Tactics.destruct_pairs; [|reflexivity];
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IUnpack.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    cbn.
    unfold Proto_J.Local_gas_counter.use_gas_counter_in_context,
      Local_gas_counter.use_gas_counter_in_context.
    rewrite <- Local_gas_counter.update_context_is_backward_compatible.
    rewrite <- Script_interpreter_defs.dep_unpack_is_backward_compatible.
    cbn.
    ez destruct (
      Proto_J.Simulations.Script_interpreter_defs.dep_unpack
      _ _ _); cbn.
    Tactics.destruct_pairs; cbn.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IBlake2b.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    Tactics.destruct_pairs;
    now apply dep_step_is_backward_compatible.
  }
  {
    grep @OldIR.With_family.ISha256.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    Tactics.destruct_pairs;
    now apply dep_step_is_backward_compatible.
  }
  {
    grep @OldIR.With_family.ISha512.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    Tactics.destruct_pairs;
    now apply dep_step_is_backward_compatible.
  }
  {
    grep @OldIR.With_family.ISource.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    Tactics.destruct_pairs;
    now apply dep_step_is_backward_compatible.
  }
  {
    grep @OldIR.With_family.ISender.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    Tactics.destruct_pairs;
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ISelf.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    Tactics.destruct_pairs;
    rewrite dep_step_is_backward_compatible by easy.
    destruct sc, self; cbn in *; [destruct H_valid_sc|reflexivity].
  }
  {
    grep @OldIR.With_family.ISelf_address.
    simpl in *; rewrite <- Hconsume; cbn.
    ez destruct Old.dep_consume_instr.
    rewrite dep_step_is_backward_compatible by easy.
    destruct sc, self; cbn in *; [destruct H_valid_sc|reflexivity].
  }
  {
    grep @OldIR.With_family.IAmount.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    Tactics.destruct_pairs;
    now apply dep_step_is_backward_compatible.
  }
  {
    grep @OldIR.With_family.ISapling_empty_state.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    Tactics.destruct_pairs;
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ISapling_verify_update.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    Tactics.destruct_pairs; simpl.
    unfold Proto_J.Alpha_context.Sapling.verify_update,
      Alpha_context.Sapling.verify_update.
    do 2 match goal with
    | |- context [if ?cond' then _ else _] =>
        let cond := fresh "cond" in
        set (cond := cond'); symmetry
    end.
    assert (H_cond_eq : cond = cond0).
    { subst cond cond0.
      cbn. destruct_all
        (Proto_J.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
         Proto_J.Simulations.Script_family.Ty.Sapling_transaction); simpl.
      now induction outputs.
    }
    rewrite H_cond_eq; clear H_cond_eq cond; cbn in Hconsume;
      destruct cond0; cbn.
    { rewrite dep_step_is_backward_compatible by easy; repeat f_equal;
        [now rewrite Local_gas_counter.outdated_context_value_is_backward_compatible,
          Local_gas_counter.update_context_is_backward_compatible|
        now rewrite Local_gas_counter.local_gas_counter_value_is_backward_compatible,
          Local_gas_counter.update_context_is_backward_compatible].
    }
    { rewrite <- Local_gas_counter.update_context_is_backward_compatible.
      rewrite <- Sapling_storage.root_mem_is_backward_compatible.
      ez destruct (Proto_J.Sapling_storage.root_mem _ _ _).
      destruct b; cbn; [|
        now apply dep_step_is_backward_compatible].
      rewrite <- Sapling_validator.check_and_update_nullifiers_is_backward_compatible.
      ez destruct (Proto_J.Alpha_context.Sapling.check_and_update_nullifiers _ _ _) eqn:?.
      step.
      destruct_all (M* Proto_J.Sapling_storage.state);
        [cbn|now apply dep_step_is_backward_compatible].
      unfold Verification.with_verification_ctx, Verification.t.
      match goal with
      | |- (Error.migrate_monad match (_ ?f1) with _ => _ end _) = match (_ ?f2) with _ => _ end =>
          set (f1' := f1);
          set (f2' := f2)
      end.
      rewrite <-
       (Sapling.Verification.with_verification_is_backward_compatible f1' f2').
      { destruct (V5.Sapling.Verification.with_verification_ctx f1'); clear f1' f2'.
        destruct_all (M* (V5.Int64.t * Proto_J.Sapling_storage.state));
          [cbn|now apply dep_step_is_backward_compatible].
        Tactics.destruct_pairs.
        rewrite dep_step_is_backward_compatible by easy.
        repeat f_equal.
      }
      { intros. subst f1' f2'.
        cbn.
        match goal with
        | |- context [if ?e then _ else _] => set (cond := e)
        end.
        destruct cond; [reflexivity|].
        do 2 match goal with
        | |- context [if ?e then _ else _] =>
            let cond := fresh "cond" in
            set (cond := e); symmetry
        end.
        assert (H_cond : cond = cond0).
        { subst cond cond0.
          destruct_all (Proto_J.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
            Proto_J.Simulations.Script_family.Ty.Sapling_transaction); cbn.
          destruct sc; cbn.
          f_equal.
          induction inputs; [easy|].
          f_equal.
          apply FunctionalExtensionality.functional_extensionality; intros x.
          repeat f_equal.
          now destruct self.
        }
        rewrite H_cond; destruct cond0; [easy|].
        clear cond H_cond.
        do 2 match goal with
        | |- context [if ?e then _ else _] =>
            let cond := fresh "cond" in
            set (cond := e); symmetry
        end.
        assert (H_cond : cond = cond0).
        { subst cond cond0.
          repeat f_equal.
          destruct sc; cbn.
          now destruct self.
        }
        rewrite H_cond; now destruct cond0.
      }
    }
  }
  { grep @OldIR.With_family.ISapling_verify_update_deprecated.
    simpl in *; rewrite <- Hconsume; cbn in *;
    ez destruct Old.dep_consume_instr.
    Tactics.destruct_pairs; simpl.
    rewrite <- Local_gas_counter.update_context_is_backward_compatible.
    rewrite <-
      Alpha_context.Sapling.Legacy.verify_update_is_backward_compatible.
    match goal with
    | |- Error.migrate_monad _ ?f = _ =>
        set (error_f := f)
    end.
    match goal with 
    | |- Error.migrate_monad (let? '_ := ?e1' _ in _) _ = _ =>
        set (e1 := e1')
    end.
    do 2 match goal with
    | |- context [e1 ?arg'] => 
        let arg := fresh "arg" in
        set (arg := arg'); symmetry
    end.
    assert (H_arg : arg = arg0).
    { subst arg arg0.
      repeat f_equal.
      destruct sc, self; cbn; easy.
    }
    rewrite H_arg. clear H_arg arg.
    destruct (e1 arg0) eqn:?; [|easy].
    destruct_all (Proto_J.Raw_context.t * 
      M* (V5.Int64.t * Proto_J.Sapling_storage.state)).
    destruct_all (M* (V5.Int64.t * Proto_J.Sapling_storage.state)); cbn;
      [|now apply dep_step_is_backward_compatible].
    Tactics.destruct_pairs.
    now rewrite dep_step_is_backward_compatible by easy.
  }
  { grep @OldIR.With_family.IDig.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite dep_step_is_backward_compatible; trivial.
    f_equal. pose eval_sppw_for_IDig_is_backward_compatible.
    specialize e with (st := axiom "stack_ty") (w := s2)
    (f :=
    fun x :
      Proto_J.Simulations.Script_typed_ir.With_family
        .stack_ty_to_dep_Set (a::s)
      => x)
    (g :=
      fun x :
        Script_typed_ir.With_family.stack_ty_to_dep_Set
          (Stack_ty.migrate (a :: s))
        => x)
    (migr := Script_typed_ir.With_family.migrate_value) (x := (s0, s1)).
    destruct Old.eval_sppw_for_IDig.
    unfold Script_typed_ir.With_family.migrate_stack_value in *. destruct s3.
    cbn in *. rewrite e; [ trivial | now destruct a0, p ].
  }
  { grep @OldIR.With_family.IDug.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite dep_step_is_backward_compatible; trivial.
    f_equal.
    apply eval_stack_prefix_preservation_witness_is_backward_compatible.
    unfold Script_typed_ir.With_family.migrate_stack_value. simpl.
    now destruct a0.
  }
  { grep @OldIR.With_family.IDipn.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite <- Script_interpreter_defs.dep_kundip_is_backward_compatible.
    destruct Old.dep_kundip.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IDropn.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Hconsume.
    destruct Old.dep_consume_instr; simpl; [|reflexivity].
    rewrite <- eval_sspw_for_IDropn_is_backward_compatible.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IChainId.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity].
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.INever.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; step.
  }
  { grep @OldIR.With_family.IVoting_power.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    rewrite <- Vote_storage.get_voting_power_is_backward_compatible.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    rewrite
      <- Local_gas_counter.outdated_context_value_is_backward_compatible,
      <- Local_gas_counter.local_gas_counter_value_is_backward_compatible.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ITotal_voting_power.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; simpl.
    rewrite <- Vote_storage.get_total_voting_power_is_backward_compatible.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    rewrite
      <- Local_gas_counter.outdated_context_value_is_backward_compatible,
      <- Local_gas_counter.local_gas_counter_value_is_backward_compatible.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IKeccak.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ISha3.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IAdd_bls12_381_g1.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    rewrite <- Script_typed_ir.Script_bls.G1.migrate_add_eq.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IAdd_bls12_381_g2.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    rewrite <- Script_typed_ir.Script_bls.G2.migrate_add_eq.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IAdd_bls12_381_fr.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    rewrite <- Script_typed_ir.Script_bls.Fr.migrate_add_eq.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IMul_bls12_381_g1.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    rewrite <- Script_typed_ir.Script_bls.G1.migrate_mul_eq.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IMul_bls12_381_g2.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    rewrite <- Script_typed_ir.Script_bls.G2.migrate_mul_eq.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IMul_bls12_381_fr.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    rewrite <- Script_typed_ir.Script_bls.Fr.migrate_mul_eq.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IMul_bls12_381_z_fr.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    destruct_all
      (Proto_J.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
         Proto_J.Simulations.Script_family.Ty.Bls12_381_fr); simpl.
    rewrite <- Script_typed_ir.Script_bls.Fr.migrate_mul_z_eq.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IMul_bls12_381_fr_z.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    destruct_all
      (Proto_J.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
         Proto_J.Simulations.Script_family.Ty.Bls12_381_fr); simpl.
    rewrite <- Script_typed_ir.Script_bls.Fr.migrate_mul_z_eq.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IInt_bls12_381_fr.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    destruct_all
      (Proto_J.Simulations.Script_typed_ir.With_family.ty_to_dep_Set
         Proto_J.Simulations.Script_family.Ty.Bls12_381_fr); simpl.
    change
      (Script_int.of_zint (Fr.(PRIME_FIELD.to_z) t))
      with
      (Script_int_repr.migrate
         (Script_int_repr.of_zint
            (V5.Bls12_381.Fr.(V5.S.PRIME_FIELD.to_z) t))).
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.INeg_bls12_381_g1.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    rewrite <- Script_typed_ir.Script_bls.G1.migrate_negate.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.INeg_bls12_381_g2.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    rewrite <- Script_typed_ir.Script_bls.G2.migrate_negate.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.INeg_bls12_381_fr.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    rewrite <- Script_typed_ir.Script_bls.Fr.migrate_negate.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IPairing_check_bls12_381.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity]; Tactics.destruct_pairs; simpl.
    match goal with
    | |- context
          [New.dep_step
             _ _ _ _ _
             (Script_typed_ir.Script_bls.pairing_check
                (List.map ?f ?x), _)] =>
        replace
          (Proto_J.Script_typed_ir.Script_bls.pairing_check
             s0.(Proto_J.Script_typed_ir.boxed_list.elements))
        with
        (Script_typed_ir.Script_bls.pairing_check (List.map f x))
        by (
          remember s0.(Proto_J.Script_typed_ir.boxed_list.elements) as els;
          clear;
          induction els as [|[[] []] ? IHels]; [reflexivity|];
          unfold
            Script_typed_ir.Script_bls.pairing_check,
            Proto_J.Script_typed_ir.Script_bls.pairing_check,
            pairing_check in *; simpl;
          apply Bls12_381.pairing_check_cons;
          rewrite IHels;
          reflexivity
        )
    end.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IComb. 
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity].
    Tactics.destruct_pairs.
    ez destruct Old.dep_consume_instr. simpl.
    rewrite migrate_comb_gadt_witness.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IUncomb.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity].
    Tactics.destruct_pairs. 
    ez destruct Old.dep_consume_instr.
    rewrite migrate_uncomb_gadt_witness.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.IComb_get.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity].
    Tactics.destruct_pairs.
    ez destruct Old.dep_consume_instr. simpl in *.
    rewrite dep_step_is_backward_compatible.
    repeat f_equal. 2, 3 : trivial.
    clear H_valid_ks H_valid_sc ks f fuel ctxt Hconsume
     sc gas k i i_value ks l l0 Heqo dep_step_is_backward_compatible
       dep_next_is_backward_compatible f0.
    induction c; cbn; hauto lq: on. 
  }
  { grep @OldIR.With_family.IComb_set.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity].
    Tactics.destruct_pairs. 
    ez destruct Old.dep_consume_instr.
    simpl in *.
    rewrite dep_step_is_backward_compatible.
    repeat f_equal. 2,3 : trivial.
    clear H_valid_ks H_valid_sc ks f fuel ctxt Hconsume
      sc gas k i i_value ks l l0 Heqo dep_step_is_backward_compatible
      dep_next_is_backward_compatible f0.
    induction c0. cbn. reflexivity. hauto lq: on.
    cbn. destruct t. simpl in *.
    unfold Script_typed_ir.With_family.migrate_stack_value.
    cbn. pose proof (IHc0 s0 t0).
    f_equal. f_equal. unfold Script_typed_ir.With_family
    .migrate_stack_value in H.
    simpl in H. scongruence.
  } 
  { grep @OldIR.With_family.IDup_n.
    simpl in *; rewrite <- Hconsume.
    step; [|reflexivity].
    Tactics.destruct_pairs. 
    ez destruct Old.dep_consume_instr. simpl in *.
    rewrite dep_step_is_backward_compatible.
    repeat f_equal. 2,3 : trivial.
    clear H_valid_ks H_valid_sc ks f fuel ctxt Hconsume
      sc gas k i i_value ks l l0 Heqo dep_step_is_backward_compatible
      dep_next_is_backward_compatible f0.
    induction d; cbn; try reflexivity.
    destruct s1. unfold Script_typed_ir.With_family
    .migrate_stack_value.
    cbn. f_equal. unfold Script_typed_ir.With_family
    .migrate_stack_value in IHd.
    simpl in IHd. pose proof (IHd m m0). scongruence.
  }
  { grep @OldIR.With_family.ITicket.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    Tactics.destruct_pairs.
    rewrite dep_step_is_backward_compatible by easy.
    repeat f_equal.
    destruct sc; cbn;
    now destruct self.
  }
  { grep @OldIR.With_family.IRead_ticket.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    Tactics.destruct_pairs.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.ISplit_ticket.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    do 2 step; cbn.
    rewrite dep_step_is_backward_compatible by easy.
    repeat f_equal.
    do 2 match goal with
    | |- context [if ?e then _ else _] =>
        let cond := fresh "cond" in
        set (cond := e); symmetry
    end.
    assert (H_cond : cond = cond0).
    { subst cond cond0.
      cbv; hauto lq: on rew: off.
    }
    rewrite H_cond. clear cond H_cond.
    destruct cond0; cbn; now Tactics.destruct_pairs.
  }
  { grep @OldIR.With_family.IJoin_tickets.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    step; cbn.
    do 2 match goal with
    | |- context [if (?e && ?a) then _ else _] =>
        let cond := fresh "cond" in
        let and_cond := fresh "and_cond" in
        set (cond := e);
        set (and_cond := a); symmetry
    end.
    assert (H_cond : cond = cond0).
    { subst cond cond0; cbn.
      destruct t0; cbn.
      cbv; hauto lq: on rew: off.
    }
    rewrite H_cond; clear cond H_cond.
    assert (H_and_cond : and_cond = and_cond0).
    { subst and_cond and_cond0; cbn.
      match goal with
      | |- context [Script_comparable.dep_compare_comparable ?fix'] =>
          set (migrate_ty := fix')
      end.
      destruct t0, t1; cbn.
      repeat f_equal.
      change migrate_ty with (Ty.migrate a); clear migrate_ty.
      rewrite Simulations.dep_compare_comparable_is_backward_compatible.
      repeat f_equal;
        apply Script_typed_ir.to_value_is_backward_compatible.
    }
    rewrite H_and_cond; clear and_cond H_and_cond.
    destruct (cond0 && and_cond0); cbn;
      [|now rewrite dep_step_is_backward_compatible by easy].
    rewrite dep_step_is_backward_compatible by easy; repeat f_equal.
    Tactics.destruct_pairs.
    cbn. unfold Script_typed_ir.With_family.migrate_stack_value.
    cbn. repeat f_equal.
    unfold  Script_typed_ir.migrate_ticket; cbn.
    repeat f_equal.
    destruct t0, t1; cbn.
    unfold Alpha_context.Script_int.add_n,
      Alpha_context.Script_int.add,
      Script_int.add_n,
      Script_int.add.
    sauto lq: on rew: off.
  }
  { grep @OldIR.With_family.IOpen_chest.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr.
    do 2 step; cbn.
    rewrite dep_step_is_backward_compatible by easy.
    repeat f_equal.
    unfold Alpha_context.Script_int.to_int,
      Script_int.to_int.
    destruct t0; cbn.
    unfold Proto_J.Script_typed_ir.Script_timelock.open_chest,
      Script_typed_ir.Script_timelock.open_chest.
    destruct t, s0; cbn.
    unfold open_chest, to_int.
    Tactics.destruct_pairs.
    step; hauto l: on.
  }
  { grep @OldIR.With_family.IHalt.
    simpl in *; rewrite <- Hconsume; cbn;
    ez destruct Old.dep_consume_instr; cbn in *.
    now apply dep_next_is_backward_compatible.
  }
}
  (* dep_next *)
{ destruct fuel as [|fuel]; simpl; [reflexivity|].
  rewrite <- consume_control_is_backward_compatible.
  destruct Proto_J.Script_interpreter_defs.consume_control; simpl;
    [|reflexivity].
  destruct ks eqn:?; simpl in *; Tactics.destruct_pairs;
    try reflexivity;
    try now apply dep_step_is_backward_compatible;
    try now apply dep_next_is_backward_compatible.
  all : try now apply dep_next_is_backward_compatible.
  { grep @OldIR.With_family.KLoop_in.
    cbn. step; cbn;
      [now apply dep_step_is_backward_compatible|].
    now apply dep_next_is_backward_compatible.
  }
  { grep @OldIR.With_family.KLoop_in_left.
    cbn. step; cbn;
      [now apply dep_step_is_backward_compatible|].
    now apply dep_next_is_backward_compatible.
  }
  { grep @OldIR.With_family.KIter.
    induction l0; cbn.
    { now apply dep_next_is_backward_compatible. }
    { now apply  dep_step_is_backward_compatible. }
  }
  { grep @OldIR.With_family.KList_exit_body.
    induction l0; cbn; [
      |now rewrite dep_step_is_backward_compatible by easy].
    rewrite dep_next_is_backward_compatible by easy.
      repeat f_equal.
    unfold Script_typed_ir.With_family.migrate_stack_value;
      repeat f_equal.
    cbn. unfold Script_typed_ir.migrate_boxed_list.
    f_equal. cbn.
    now rewrite List.rev_map_eq.
  }
  { grep @OldIR.With_family.KMap_enter_body.
    induction l0; cbn;
      [now apply dep_next_is_backward_compatible|].
    Tactics.destruct_pairs.
    now apply dep_step_is_backward_compatible.
  }
  { grep @OldIR.With_family.KMap_exit_body.
    rewrite dep_next_is_backward_compatible by easy.
    simpl.
    repeat f_equal.
    fold @Script_family.Ty.migrate.
    unfold
      Proto_J.Simulations.Script_typed_ir.With_family.Script_Map,
      Simulations.Script_typed_ir.With_family.Script_Map; cbn.
    with_strategy transparent [V5.Map.Make Map.Make
      Script_typed_ir.With_family.migrate_map]
     unfold V5.Map.Make, Map.Make; simpl.
    clear Heqc.
    induction t; cbn.
    { repeat f_equal.
      apply Script_typed_ir.to_value_is_backward_compatible. }
    { Tactics.destruct_pairs.
      unfold Make.compare_keys.
      do 2 match goal with
      | |- context [if ?e then _ else _] =>
          let cond := fresh "cond" in
          set (cond := e); symmetry
      end.
      assert (H_cond : cond = cond0).
      { subst cond cond0.
        cbn.
        rewrite Simulations.dep_compare_comparable_is_backward_compatible.
        repeat f_equal.
        apply Script_typed_ir.to_value_is_backward_compatible.
      }
      rewrite H_cond; clear cond H_cond.
      destruct cond0.
      { cbn.
        repeat f_equal.
        apply Script_typed_ir.to_value_is_backward_compatible.
      }
      cbn.
      rewrite Simulations.dep_compare_comparable_is_backward_compatible.
      do 2 match goal with 
      | |- context [if ?e then _ else _] => 
          let cond := fresh "cond" in 
          set (cond := e); symmetry
      end.
      assert (H_cond : cond = cond0).
      { subst cond cond0.
        repeat f_equal.
        apply Script_typed_ir.to_value_is_backward_compatible.
      }
      rewrite H_cond; clear cond H_cond; destruct cond0; cbn;
        repeat f_equal;
        [apply Script_typed_ir.to_value_is_backward_compatible|].
      unfold Script_typed_ir.With_family.migrate_map in IHt.
      now rewrite <- IHt.
    }
  }
}
Qed.
