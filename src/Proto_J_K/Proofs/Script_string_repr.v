Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_J.Script_string_repr.
Require TezosOfOCaml.Proto_K.Script_string.

Require TezosOfOCaml.Proto_J_K.Script_string_repr.

Module Old := TezosOfOCaml.Proto_J.Script_string_repr.
Module New := TezosOfOCaml.Proto_K.Script_string.

(** [concat] is backward compatible *)
Lemma concat_is_backward_compatible (l : list Old.t) :
  Script_string_repr.migrate (Old.concat l) =
    New.concat (List.map Script_string_repr.migrate l).
Proof.
  unfold Old.concat, New.concat. cbn. f_equal. f_equal.
  induction l; simpl; trivial. destruct a. simpl. now f_equal.
Qed.

(** [length] is backward compatible *)
Lemma length_is_backward_compatible (l : Old.t) :
  New.length (Script_string_repr.migrate l) = Old.length l.
Proof.
  now destruct l.
Qed.
