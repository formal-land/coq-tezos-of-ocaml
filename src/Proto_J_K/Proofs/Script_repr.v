Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Environment.V5.Micheline.
Require TezosOfOCaml.Environment.V5.Proofs.Data_encoding.

Require TezosOfOCaml.Proto_J.Script_repr.
Require TezosOfOCaml.Proto_K.Script_repr.

Require TezosOfOCaml.Proto_J_K.Error.
Require TezosOfOCaml.Proto_J_K.Michelson_v1_primitives.
Require Import TezosOfOCaml.Proto_J_K.Script_repr.

Module Old := TezosOfOCaml.Proto_J.Script_repr.
Module New := TezosOfOCaml.Proto_K.Script_repr.

(** Function [strip_locations] is backward compatible. *)
Lemma strip_locations_is_backward_compatible
  {A} (node : Proto_J.Script_repr.michelson_node A)
  : (strip_locations
       (Script_repr.map_node Michelson_v1_primitives.migrate_prim node)) =
      Script_repr.map_node Michelson_v1_primitives.migrate_prim
        (strip_locations node).
Proof.
  revert node.
  fix IHtop 1.
  intros [| | |a p l annot|a l]; [reflexivity..| |].
  1:
    change
      (strip_locations
         (Script_repr.map_node
            Michelson_v1_primitives.migrate_prim
            (Micheline.Prim a p l annot)))
    with
    (Prim dummy_location (Michelson_v1_primitives.migrate_prim p)
       (List.map strip_locations
          (List.map
             (Script_repr.map_node Michelson_v1_primitives.migrate_prim) l))
       annot);
    change
      (Script_repr.map_node
         Michelson_v1_primitives.migrate_prim
         (strip_locations (Micheline.Prim a p l annot)))
    with
    (Prim dummy_location (Michelson_v1_primitives.migrate_prim p)
       (List.map (Script_repr.map_node Michelson_v1_primitives.migrate_prim)
          (List.map strip_locations l)) annot).
  2:
    change
      (strip_locations
         (Script_repr.map_node
            Michelson_v1_primitives.migrate_prim (Micheline.Seq a l)))
    with
    (Seq dummy_location
       (List.map strip_locations
          (List.map
             (Script_repr.map_node Michelson_v1_primitives.migrate_prim) l)));
    change
      (Script_repr.map_node
         Michelson_v1_primitives.migrate_prim
         (strip_locations (Micheline.Seq a l)))
    with
    (Seq dummy_location
       (List.map (Script_repr.map_node Michelson_v1_primitives.migrate_prim)
          (List.map strip_locations l))).
  all:
    f_equal; induction l as [|? ? IHl]; [reflexivity|];
    simpl; rewrite IHl, IHtop; reflexivity.
Qed.

(** [micheline_nodes] is backward compatible *)
Lemma micheline_nodes_is_backward_compatible
  {A : Set} (node_value : Old.michelson_node A) :
  Old.micheline_nodes node_value =
    New.micheline_nodes
      (map_node Michelson_v1_primitives.migrate_prim node_value).
Proof.
  (* TODO *)
Admitted.

(** Migrate [to_bytes_exn] with [expr_encoding] is backward comparable. *)
Axiom expr_encoding_migrate_prim_is_backward_compatible : forall
  {A} (node : Micheline.node A Proto_J.Michelson_v1_primitives.prim),
   (Data_encoding.Binary.to_bytes_exn
       None Proto_J.Alpha_context.Script.expr_encoding
       (Micheline.strip_locations node)) =
      (Binary.to_bytes_exn
         None Alpha_context.Script.expr_encoding
         (strip_locations
            (Script_repr.map_node Michelson_v1_primitives.migrate_prim node))).

(** [cost_micheline_strip_locations] is backward compatible *)
Lemma cost_micheline_strip_locations_is_backward_compatible
  (i : int) :
  Old.cost_micheline_strip_locations i =
    New.cost_micheline_strip_locations i.
Proof.
  (* TODO *)
Admitted.

(** [serialized_cost] is backward compatible *)
Lemma serialized_cost_is_backward_compatible bs :
  Old.serialized_cost bs = New.serialized_cost bs.
Proof.
Admitted.

(** [strip_locations_cost] is backward compatible *)
Lemma strip_locations_cost_is_backward_compatible
  {A : Set} (node_value : Old.michelson_node A) :
  Old.strip_locations_cost node_value =
    New.strip_locations_cost
      (map_node Michelson_v1_primitives.migrate_prim node_value).
Proof.
  unfold Old.strip_locations_cost, New.strip_locations_cost.
  rewrite cost_micheline_strip_locations_is_backward_compatible. f_equal.
  apply micheline_nodes_is_backward_compatible.
Qed.
