Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Script_typed_ir.

Require TezosOfOCaml.Proto_J.Simulations.Gas_comparable_input_size.
Require TezosOfOCaml.Proto_J.Simulations.Script_comparable.
Require Import TezosOfOCaml.Proto_J.Simulations.Script_family.
Require TezosOfOCaml.Proto_J.Simulations.Script_tc_errors.

Module Ty_metadata.
  (** A default empty metadata we use to make a canonical form of a type. *)
  Definition default : Script_typed_ir.ty_metadata :=
    {|
      Script_typed_ir.ty_metadata.size := 0;
    |}.
End Ty_metadata.

(** ** Michelson AST with dependent types 🌲
    We define a dependently typed version of the Michelson AST. We
    attempt to be complete with respect to the OCaml implementation. We follow
    the namings of the OCaml definitions.

    We parametrize our types using type families, which are values describing a
    class of types. In the GADT OCaml version we parameterize instructions by
    types. The advantage of using values rather than types in Coq is that we can
    pattern-match on it. These families are defined in [Script_family].

    For the [kinstr] type we merge the head and stack types. We do that in
    order to simplify the code. We still represent the empty stack as a couple,
    so that we can get always get a head and tail part from a stack.
*)

Module With_family.
  Inductive comb_gadt_witness : Stack_ty.t -> Stack_ty.t -> Set :=
  | Comb_one : forall {s}, comb_gadt_witness s s
  | Comb_succ : forall {a b s s'}, comb_gadt_witness s (b :: s') ->
      comb_gadt_witness (a :: s) (Ty.Pair a b :: s').

  Fixpoint to_comb_gadt_witness {s f} (w : comb_gadt_witness s f)
    : Script_typed_ir.comb_gadt_witness :=
    match w with
    | Comb_one => Script_typed_ir.Comb_one
    | Comb_succ w' => Script_typed_ir.Comb_succ (to_comb_gadt_witness w')
    end.

  Inductive uncomb_gadt_witness : Stack_ty.t -> Stack_ty.t -> Set :=
  | Uncomb_one : forall {s}, uncomb_gadt_witness s s
  | Uncomb_succ : forall {a b s s'}, uncomb_gadt_witness (b :: s) s' ->
      uncomb_gadt_witness (Ty.Pair a b :: s) (a :: s').

  Fixpoint to_uncomb_gadt_witness {s f} (w : uncomb_gadt_witness s f)
    : Script_typed_ir.uncomb_gadt_witness :=
    match w with
    | Uncomb_one => Script_typed_ir.Uncomb_one
    | Uncomb_succ w' => Script_typed_ir.Uncomb_succ (to_uncomb_gadt_witness w')
    end.

  Inductive comb_get_gadt_witness : Ty.t -> Ty.t -> Set :=
  | Comb_get_zero : forall {b}, comb_get_gadt_witness b b
  | Comb_get_one : forall {a b}, comb_get_gadt_witness (Ty.Pair a b) a
  | Comb_get_plus_two : forall {a b c},
      comb_get_gadt_witness b c ->
      comb_get_gadt_witness (Ty.Pair a b) c.

  Fixpoint to_comb_get_gadt_witness {a b} (w : comb_get_gadt_witness a b)
    : Script_typed_ir.comb_get_gadt_witness :=
    match w with
    | Comb_get_zero => Script_typed_ir.Comb_get_zero
    | Comb_get_one => Script_typed_ir.Comb_get_one
    | Comb_get_plus_two w' => Script_typed_ir.Comb_get_plus_two
      (to_comb_get_gadt_witness w')
    end.

  Inductive comb_set_gadt_witness : Ty.t -> Ty.t -> Ty.t -> Set :=
  | Comb_set_zero : forall {v a}, comb_set_gadt_witness v a v
  | Comb_set_one : forall {v a b},
      comb_set_gadt_witness v (Ty.Pair a b) (Ty.Pair v b)
  | Comb_set_plus_two : forall {v a b c},
      comb_set_gadt_witness v b c ->
      comb_set_gadt_witness v (Ty.Pair a b) (Ty.Pair a c).

  Fixpoint to_comb_set_gadt_witness {a b c} (w : comb_set_gadt_witness a b c)
    : Script_typed_ir.comb_set_gadt_witness :=
    match w with
    | Comb_set_zero => Script_typed_ir.Comb_set_zero
    | Comb_set_one => Script_typed_ir.Comb_set_one
    | Comb_set_plus_two w' => Script_typed_ir.Comb_set_plus_two
      (to_comb_set_gadt_witness w')
    end.

  Inductive dup_n_gadt_witness : Stack_ty.t -> Ty.t -> Set :=
  | Dup_n_zero : forall {a s}, dup_n_gadt_witness (a :: s) a
  | Dup_n_succ : forall {a b s}, dup_n_gadt_witness s b ->
      dup_n_gadt_witness (a :: s) b.

  Fixpoint to_dup_n_gadt_witness {b s} (w : dup_n_gadt_witness s b)
    : Script_typed_ir.dup_n_gadt_witness :=
    match w with
    | Dup_n_zero => Script_typed_ir.Dup_n_zero
    | Dup_n_succ w' => Script_typed_ir.Dup_n_succ (to_dup_n_gadt_witness w')
    end.

  Inductive ty : Ty.t -> Set :=
  | Unit_t : ty Ty.Unit
  | Int_t : ty (Ty.Num Ty.Num.Int)
  | Nat_t : ty (Ty.Num Ty.Num.Nat)
  | Signature_t : ty Ty.Signature
  | String_t : ty Ty.String
  | Bytes_t : ty Ty.Bytes
  | Mutez_t : ty Ty.Mutez
  | Key_hash_t : ty Ty.Key_hash
  | Key_t : ty Ty.Key
  | Timestamp_t : ty Ty.Timestamp
  | Address_t : ty Ty.Address
  | Tx_rollup_l2_address_t : ty Ty.Tx_rollup_l2_address
  | Bool_t : ty Ty.Bool
  | Pair_t {t1 t2} :
    ty t1 ->
    ty t2 ->
    Script_typed_ir.ty_metadata ->
    Dependent_bool.dand ->
    ty (Ty.Pair t1 t2)
  | Union_t {t1 t2} :
    ty t1 ->
    ty t2 ->
    Script_typed_ir.ty_metadata ->
    Dependent_bool.dand ->
    ty (Ty.Union t1 t2)
  | Lambda_t {arg ret} :
    ty arg ->
    ty ret ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Lambda arg ret)
  | Option_t {t} :
    ty t ->
    Script_typed_ir.ty_metadata ->
    Dependent_bool.dbool ->
    ty (Ty.Option t)
  | List_t {t} :
    ty t ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.List t)
  | Set_t {t} :
    ty t ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Set_ t)
  | Map_t {key value} :
    ty key ->
    ty value ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Map key value)
  | Big_map_t {key value} :
    ty key ->
    ty value ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Big_map key value)
  | Contract_t {arg} :
    ty arg ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Contract arg)
  | Sapling_transaction_t :
    Sapling_repr.Memo_size.t ->
    ty Ty.Sapling_transaction
  | Sapling_transaction_deprecated_t :
    Sapling_repr.Memo_size.t ->
    ty Ty.Sapling_transaction_deprecated
  | Sapling_state_t :
    Sapling_repr.Memo_size.t ->
    ty Ty.Sapling_state
  | Operation_t : ty Ty.Operation
  | Chain_id_t : ty Ty.Chain_id
  | Never_t : ty Ty.Never
  | Bls12_381_g1_t : ty Ty.Bls12_381_g1
  | Bls12_381_g2_t : ty Ty.Bls12_381_g2
  | Bls12_381_fr_t : ty Ty.Bls12_381_fr
  | Ticket_t {a} :
    ty a ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Ticket a)
  | Chest_key_t : ty Ty.Chest_key
  | Chest_t : ty Ty.Chest.

  Fixpoint to_ty {a} (t : ty a) : Script_typed_ir.ty :=
    match t with
    | Unit_t => Script_typed_ir.Unit_t
    | Int_t => Script_typed_ir.Int_t
    | Nat_t => Script_typed_ir.Nat_t
    | Signature_t => Script_typed_ir.Signature_t
    | String_t => Script_typed_ir.String_t
    | Bytes_t => Script_typed_ir.Bytes_t
    | Mutez_t => Script_typed_ir.Mutez_t
    | Key_hash_t => Script_typed_ir.Key_hash_t
    | Key_t => Script_typed_ir.Key_t
    | Timestamp_t => Script_typed_ir.Timestamp_t
    | Address_t => Script_typed_ir.Address_t
    | Tx_rollup_l2_address_t => Script_typed_ir.Tx_rollup_l2_address_t
    | Bool_t => Script_typed_ir.Bool_t
    | Pair_t t1 t2 m dand =>
      Script_typed_ir.Pair_t (to_ty t1) (to_ty t2) m dand
    | Union_t t1 t2 m dand =>
      Script_typed_ir.Union_t (to_ty t1) (to_ty t2) m dand
    | Lambda_t arg ret m => Script_typed_ir.Lambda_t (to_ty arg) (to_ty ret) m
    | Option_t t m dbool => Script_typed_ir.Option_t (to_ty t) m dbool
    | List_t t m => Script_typed_ir.List_t (to_ty t) m
    | Set_t t m => Script_typed_ir.Set_t (to_ty t) m
    | Map_t k v m => Script_typed_ir.Map_t (to_ty k) (to_ty v) m
    | Big_map_t key value m =>
      Script_typed_ir.Big_map_t (to_ty key) (to_ty value) m
    | Contract_t t m => Script_typed_ir.Contract_t (to_ty t) m
    | Sapling_transaction_t x => Script_typed_ir.Sapling_transaction_t x
    | Sapling_transaction_deprecated_t x =>
      Script_typed_ir.Sapling_transaction_deprecated_t x
    | Sapling_state_t x => Script_typed_ir.Sapling_state_t x
    | Operation_t => Script_typed_ir.Operation_t
    | Chain_id_t => Script_typed_ir.Chain_id_t
    | Never_t => Script_typed_ir.Never_t
    | Bls12_381_g1_t => Script_typed_ir.Bls12_381_g1_t
    | Bls12_381_g2_t => Script_typed_ir.Bls12_381_g2_t
    | Bls12_381_fr_t => Script_typed_ir.Bls12_381_fr_t
    | Ticket_t ty m => Script_typed_ir.Ticket_t (to_ty ty) m
    | Chest_key_t => Script_typed_ir.Chest_key_t
    | Chest_t => Script_typed_ir.Chest_t
    end.

  Inductive stack_ty : Stack_ty.t -> Set :=
  | Item_t {ty_ rest} :
    ty ty_ ->
    stack_ty rest ->
    stack_ty (ty_ :: rest)
  | Bot_t :
    stack_ty [].

  Fixpoint to_stack_ty {s} (ts : stack_ty s) : Script_typed_ir.stack_ty :=
    match ts with
    | Item_t t rest =>
      Script_typed_ir.Item_t (to_ty t) (to_stack_ty rest)
    | Bot_t => Script_typed_ir.Bot_t
    end.

  Inductive kinfo : Stack_ty.t -> Set :=
  | Kinfo {s} : Alpha_context.Script.location -> stack_ty s -> kinfo s.

  Definition to_kinfo {s} (info : kinfo s) : Script_typed_ir.kinfo :=
    let 'Kinfo iloc kstack_ty := info in
    {|
      Script_typed_ir.kinfo.iloc := iloc;
      Script_typed_ir.kinfo.kstack_ty := to_stack_ty kstack_ty;
    |}.

  Inductive stack_prefix_preservation_witness :
    Stack_ty.t -> Stack_ty.t -> Stack_ty.t -> Stack_ty.t -> Set :=
  | KRest : forall {s t}, stack_prefix_preservation_witness s t s t
  | KPrefix : forall {a s t u v},
      kinfo v ->
      stack_prefix_preservation_witness s t u v ->
      stack_prefix_preservation_witness s t (a :: u) (a :: v).

  Fixpoint to_stack_prefix_preservation_witness {s t u v}
    (w : stack_prefix_preservation_witness s t u v) :
    Script_typed_ir.stack_prefix_preservation_witness :=
    match w with
    | KRest => Script_typed_ir.KRest
    | KPrefix info w' =>
      Script_typed_ir.KPrefix
        (to_kinfo info)
        (to_stack_prefix_preservation_witness w')
    end.

  Inductive view_signature : Ty.t -> Ty.t -> Set :=
  | View_signature {a b} :
    (* name *)
    Script_string_repr.t ->
    (* input_ty *)
    ty a ->
    (* output_ty *)
    ty b ->
    view_signature a b.

  Definition to_view_signature {a b} (signature : view_signature a b) :
    Script_typed_ir.view_signature :=
    let 'View_signature name i_ty o_ty := signature in
    Script_typed_ir.View_signature {|
      Script_typed_ir.view_signature.View_signature.name := name;
      Script_typed_ir.view_signature.View_signature.input_ty := (to_ty i_ty);
      Script_typed_ir.view_signature.View_signature.output_ty := (to_ty o_ty);
    |}.

  (** Dependent version of a typed contract. *)
  Inductive typed_contract (arg : Ty.t) : Set :=
  | Typed_contract :
    ty arg ->
    Script_typed_ir.address ->
    typed_contract arg.
  Arguments Typed_contract {_}.

  (** Conversion of a [typed_contract] back to the OCaml type. *)
  Definition to_typed_contract {arg} (contract : typed_contract arg) :
    Script_typed_ir.typed_contract :=
    let 'Typed_contract arg_ty address := contract in
    Script_typed_ir.Typed_contract {|
      Script_typed_ir.typed_contract.Typed_contract.arg_ty := to_ty arg_ty;
      Script_typed_ir.typed_contract.Typed_contract.address := address;
    |}.

  (** The set module for a [Ty.t]. The [ty] parameter gives the compare
      function. *)
  Definition Script_Set (ty : Ty.t) : _Set.S (elt := Ty.to_Set ty) :=
    _Set.Make {|
      Compare.COMPARABLE.compare :=
        Script_comparable.dep_compare_comparable ty;
    |}.

  (** The set type for the dependent interpreter. *)
  Definition set (ty : Ty.t) : Set :=
    (Script_Set ty).(_Set.S.t).

  (** Conversion of a [set] back to the OCaml version. *)
  Definition to_set {ty_k : Ty.t} (x : set ty_k) :
    Script_typed_ir.set (Ty.to_Set ty_k) :=
    let _Set := Script_Set ty_k in
    let OPS := {|
      Script_typed_ir.Boxed_set_OPS.elt_size :=
        Gas_comparable_input_size.dep_size_of_comparable_value ty_k;
      Script_typed_ir.Boxed_set_OPS.empty := _Set.(_Set.S.empty);
      Script_typed_ir.Boxed_set_OPS.add := _Set.(_Set.S.add);
      Script_typed_ir.Boxed_set_OPS.mem := _Set.(_Set.S.mem);
      Script_typed_ir.Boxed_set_OPS.remove := _Set.(_Set.S.remove);
      Script_typed_ir.Boxed_set_OPS.fold _ := _Set.(_Set.S.fold);
    |} in
    Script_typed_ir.Set_tag (existS _ _ {|
      Script_typed_ir.Boxed_set.OPS := OPS;
      Script_typed_ir.Boxed_set.boxed := x;
      Script_typed_ir.Boxed_set.size_value := _Set.(_Set.S.cardinal) x;
    |}).

  (** The module for a map given a [Ty.t] to define the compare function. *)
  Definition Script_Map (ty_k : Ty.t) : Map.S (key := Ty.to_Set ty_k) :=
    Map.Make {|
      Compare.COMPARABLE.compare :=
        Script_comparable.dep_compare_comparable ty_k;
    |}.

  (** The map type for the dependent interpreter. *)
  Definition map (ty_k : Ty.t) (v : Set) : Set :=
    (Script_Map ty_k).(Map.S.t) v.

  (** Conversion of a [map] back to the OCaml version. This is an auxiliary
      function that we complete later in the file. *)
  Definition to_map_aux {ty_k : Ty.t} {v1 v2 : Set} (f : v1 -> v2)
    (x : map ty_k v1) : Script_typed_ir.map (Ty.to_Set ty_k) v2 :=
    let Map := Script_Map ty_k in
    let OPS := {|
      Script_typed_ir.Boxed_map_OPS.key_size :=
        Gas_comparable_input_size.dep_size_of_comparable_value ty_k;
      Script_typed_ir.Boxed_map_OPS.empty _ := Map.(Map.S.empty);
      Script_typed_ir.Boxed_map_OPS.add _ := Map.(Map.S.add);
      Script_typed_ir.Boxed_map_OPS.remove _ := Map.(Map.S.remove);
      Script_typed_ir.Boxed_map_OPS.find _ := Map.(Map.S.find);
      Script_typed_ir.Boxed_map_OPS.fold _ _ := Map.(Map.S.fold);
      Script_typed_ir.Boxed_map_OPS.fold_es _ _ := Map.(Map.S.fold_es);
    |} in
    Script_typed_ir.Map_tag (existS _ _ {|
      Script_typed_ir.Boxed_map.OPS := OPS;
      Script_typed_ir.Boxed_map.boxed := Map.(Map.S.map) f x;
      Script_typed_ir.Boxed_map.size_value := Map.(Map.S.cardinal) x;
      Script_typed_ir.Boxed_map.boxed_map_tag := tt;
    |}).

  
  (** Simulation of [view_map]. *)
  Definition view_map : Set := 
    With_family.map Ty.String Script_typed_ir.view.

  (** Conversion back to [view_map]. *)
  Definition to_view_map (v : view_map) : Script_typed_ir.view_map :=
    (With_family.to_map_aux id v).

  (** The record skeleton for dependent big maps. *)
  Module big_map.
    Record skeleton {ty_k ty_v : Ty.t} {k v : Set} : Set := {
      id : option Alpha_context.Big_map.Id.t;
      diff : Script_typed_ir.big_map_overlay k v;
      key_type : ty ty_k;
      value_type : ty ty_v;
    }.
    Arguments skeleton : clear implicits.

  (** Update the [diff] field. *)
  Definition with_diff {ty_k ty_v k v}
    (diff : Script_typed_ir.big_map_overlay k v)
    (r : skeleton ty_k ty_v k v) :
    skeleton ty_k ty_v k v :=
    {|
      id := r.(id);
      diff := diff;
      key_type := r.(key_type);
      value_type := r.(value_type);
    |}.
  End big_map.

  (** Auxiliary function for the conversion of dependent big maps back to
      the OCaml type. *)
  Definition to_big_map_aux {ty_k ty_v : Ty.t} {k1 k2 v1 v2 : Set}
    (f_k : k1 -> k2) (f_v : v1 -> v2)
    (m : big_map.skeleton ty_k ty_v k1 v1) : Script_typed_ir.big_map :=
    let diff := {|
      Script_typed_ir.big_map_overlay.map :=
        Script_typed_ir.Big_map_overlay.(Map.S.map)
          (fun '(key, value) => (f_k key, Option.map f_v value))
          m.(big_map.diff).(Script_typed_ir.big_map_overlay.map);
      Script_typed_ir.big_map_overlay.size :=
        m.(big_map.diff).(Script_typed_ir.big_map_overlay.size);
    |} in
    Script_typed_ir.Big_map {|
      Script_typed_ir.big_map.Big_map.id := m.(big_map.id);
      Script_typed_ir.big_map.Big_map.diff := diff;
      Script_typed_ir.big_map.Big_map.key_type := to_ty m.(big_map.key_type);
      Script_typed_ir.big_map.Big_map.value_type :=
        to_ty m.(big_map.value_type);
    |}.

  (** Record skeleton for dependent [kdescr]. *)
  Module kdescr.
    Record skeleton {s f : Stack_ty.t} {stack_ty : Stack_ty.t -> Set}
      {kinstr : Stack_ty.t -> Stack_ty.t -> Set} : Set := {
      kloc : Alpha_context.Script.location;
      kbef : stack_ty s;
      kaft : stack_ty f;
      kinstr : kinstr s f;
    }.
    Arguments skeleton : clear implicits.
  End kdescr.
  
  Reserved Notation "'ty_to_dep_Set".
  Reserved Notation "'lambda".
  Reserved Notation "'kdescr".

  (** Dependent definition of the Michelson's instructions. This is very similar
      to the GADT definition in the OCaml code, but here we do not enforce the
      stacks to be non-empty. *)
  #[bypass_check(positivity=yes)]
  Inductive kinstr : Stack_ty.t -> Stack_ty.t -> Set :=
  (*
    Stack
    -----
  *)
  | IDrop {a s f} :
    kinfo (a :: s) ->
    kinstr s f ->
    kinstr (a :: s) f
  | IDup {a s f} :
    kinfo (a :: s) ->
    kinstr (a :: a :: s) f  ->
    kinstr (a :: s) f
  | ISwap {a b s f} :
    kinfo (a :: b :: s) ->
    kinstr (b :: a :: s) f ->
    kinstr (a :: b :: s) f
  | IConst {s ty f} :
    kinfo s ->
    'ty_to_dep_Set ty ->
    kinstr (ty :: s) f ->
    kinstr s f
  (*
    Pairs
    -----
  *)
  | ICons_pair {a b s f} :
    kinfo (a :: b :: s) ->
    kinstr (Ty.Pair a b :: s) f ->
    kinstr (a :: b :: s) f
  | ICar {a b s f} :
    kinfo (Ty.Pair a b :: s) ->
    kinstr (a :: s) f ->
    kinstr (Ty.Pair a b :: s) f
  | ICdr {a b s f} :
    kinfo (Ty.Pair a b :: s) ->
    kinstr (b :: s) f ->
    kinstr (Ty.Pair a b :: s) f
  | IUnpair {a b s f} :
    kinfo (Ty.Pair a b :: s) ->
    kinstr (a :: b :: s) f ->
    kinstr (Ty.Pair a b :: s) f
  (*
    Options
    -------
  *)
  | ICons_some {v s f} :
    kinfo (v :: s) ->
    kinstr (Ty.Option v :: s) f ->
    kinstr (v :: s) f
  | ICons_none {s b f} :
    kinfo s ->
    kinstr (Ty.Option b :: s) f ->
    kinstr s f
  | IIf_none {a s t f} :
    kinfo (Ty.Option a :: s) ->
    kinstr s t ->
    kinstr (a :: s) t ->
    kinstr t f ->
    kinstr (Ty.Option a :: s) f
  | IOpt_map {a b s t} :
    kinfo (Ty.Option a :: s) ->
    kinstr (a :: s) (b :: s) ->
    kinstr (Ty.Option b :: s) t ->
    kinstr (Ty.Option a :: s) t
  (*
    Unions
    ------
  *)
  | ICons_left {a b s f} :
    kinfo (a :: s) ->
    kinstr (Ty.Union a b :: s) f ->
    kinstr (a :: s) f
  | ICons_right {a b s f} :
    kinfo (b :: s) ->
    kinstr (Ty.Union a b :: s) f ->
    kinstr (b :: s) f
  | IIf_left {a b s t f} :
    kinfo (Ty.Union a b :: s) ->
    kinstr (a :: s) t ->
    kinstr (b :: s) t ->
    kinstr t f ->
    kinstr (Ty.Union a b :: s) f
  (*
    Lists
    -----
  *)
  | ICons_list {a s f} :
    kinfo (a :: Ty.List a :: s) ->
    kinstr (Ty.List a :: s) f ->
    kinstr (a :: Ty.List a :: s) f
  | INil {b s f} :
    kinfo s ->
    kinstr (Ty.List b :: s) f ->
    kinstr s f
  | IIf_cons {a s f t} :
    kinfo (Ty.List a :: s) ->
    kinstr (a :: Ty.List a :: s) t ->
    kinstr s t ->
    kinstr t f ->
    kinstr (Ty.List a :: s) f
  | IList_map {a b s f} :
    kinfo (Ty.List a :: s) ->
    kinstr (a :: s) (b :: s) ->
    kinstr (Ty.List b :: s) f ->
    kinstr (Ty.List a :: s) f
  | IList_iter {a s f} :
    kinfo (Ty.List a :: s) ->
    kinstr (a :: s) s ->
    kinstr s f ->
    kinstr (Ty.List a :: s) f
  | IList_size {a s f} :
    kinfo (Ty.List a :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.List a :: s) f
  (*
    Sets
    ----
  *)
  | IEmpty_set {b s f} :
    kinfo s ->
    ty b ->
    kinstr (Ty.Set_ b :: s) f ->
    kinstr s f
  | ISet_iter {a s f} :
    kinfo (Ty.Set_ a :: s) ->
    kinstr (a :: s) s ->
    kinstr s f ->
    kinstr (Ty.Set_ a :: s) f
  | ISet_mem {a s f} :
    kinfo (a :: Ty.Set_ a :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (a :: Ty.Set_ a :: s) f
  | ISet_update {a s f} :
    kinfo (a :: Ty.Bool :: Ty.Set_ a :: s) ->
    kinstr (Ty.Set_ a :: s) f ->
    kinstr (a :: Ty.Bool :: Ty.Set_ a :: s) f
  | ISet_size {a s f} :
    kinfo (Ty.Set_ a :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Set_ a :: s) f
  (*
     Maps
     ----
   *)
  | IEmpty_map {b c s f} :
    kinfo s ->
    ty b ->
    kinstr (Ty.Map b c :: s) f ->
    kinstr s f
  | IMap_map {a b c s f} :
    kinfo (Ty.Map a b :: s) ->
    kinstr (Ty.Pair a b :: s) (c :: s) ->
    kinstr (Ty.Map a c :: s) f ->
    kinstr (Ty.Map a b :: s) f
  | IMap_iter {a b s f} :
    kinfo (Ty.Map a b :: s) ->
    kinstr (Ty.Pair a b :: s) s ->
    kinstr s f ->
    kinstr (Ty.Map a b :: s) f
  | IMap_mem {a b s f} :
    kinfo (a :: Ty.Map a b :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (a :: Ty.Map a b :: s) f
  | IMap_get {a b s f} :
    kinfo (a :: Ty.Map a b :: s) ->
    kinstr (Ty.Option b :: s) f ->
    kinstr (a :: Ty.Map a b :: s) f
  | IMap_update {a b s f} :
    kinfo (a :: Ty.Option b :: Ty.Map a b :: s) ->
    kinstr (Ty.Map a b :: s) f ->
    kinstr (a :: Ty.Option b :: Ty.Map a b :: s) f
  | IMap_get_and_update {a b s f} :
    kinfo (a :: Ty.Option b :: Ty.Map a b :: s) ->
    kinstr (Ty.Option b :: Ty.Map a b :: s) f ->
    kinstr (a :: Ty.Option b :: Ty.Map a b :: s) f
  | IMap_size {a b s f} :
    kinfo (Ty.Map a b :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Map a b :: s) f
  (*
     Big maps
     --------
  *)
  | IEmpty_big_map {b c s f} :
    kinfo s ->
    ty b ->
    ty c ->
    kinstr (Ty.Big_map b c :: s) f ->
    kinstr s f
  | IBig_map_mem {a b s f} :
    kinfo (a :: Ty.Big_map a b :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (a :: (Ty.Big_map a b) :: s) f
  | IBig_map_get {a b s f} :
    kinfo (a :: Ty.Big_map a b :: s) ->
    kinstr (Ty.Option b :: s) f ->
    kinstr (a :: Ty.Big_map a b :: s) f
  | IBig_map_update {a b s f} :
    kinfo (a :: Ty.Option b :: Ty.Big_map a b :: s) ->
    kinstr (Ty.Big_map a b :: s) f ->
    kinstr (a :: Ty.Option b :: Ty.Big_map a b :: s) f
  | IBig_map_get_and_update {a b s f} :
    kinfo (a :: Ty.Option b :: Ty.Big_map a b :: s) ->
    kinstr (Ty.Option b :: Ty.Big_map a b :: s) f ->
    kinstr (a :: Ty.Option b :: Ty.Big_map a b :: s) f
  (*
     Strings
     -------
  *)
  | IConcat_string {s f} :
    kinfo (Ty.List Ty.String :: s) ->
    kinstr (Ty.String :: s) f ->
    kinstr (Ty.List Ty.String :: s) f
  | IConcat_string_pair {s f} :
    kinfo (Ty.String :: Ty.String :: s) ->
    kinstr (Ty.String :: s) f ->
    kinstr (Ty.String :: Ty.String :: s) f
  | ISlice_string {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: Ty.String :: s) ->
    kinstr (Ty.Option Ty.String :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: Ty.String :: s) f
  | IString_size {s f} :
    kinfo (Ty.String :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.String :: s) f
  (*
     Bytes
     -----
  *)
  | IConcat_bytes {s f} :
    kinfo (Ty.List Ty.Bytes :: s) ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (Ty.List Ty.Bytes :: s) f
  | IConcat_bytes_pair {s f} :
    kinfo (Ty.Bytes :: Ty.Bytes :: s) ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (Ty.Bytes :: Ty.Bytes :: s) f
  | ISlice_bytes {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: Ty.Bytes :: s) ->
    kinstr (Ty.Option Ty.Bytes :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: Ty.Bytes :: s) f
  | IBytes_size {s f} :
    kinfo (Ty.Bytes :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Bytes :: s) f
  (*
     Timestamps
     ----------
  *)
  | IAdd_seconds_to_timestamp {s f} :
    kinfo (Ty.Num Ty.Num.Int :: Ty.Timestamp :: s) ->
    kinstr (Ty.Timestamp :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: Ty.Timestamp :: s) f
  | IAdd_timestamp_to_seconds {s f} :
    kinfo (Ty.Timestamp :: Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Timestamp :: s) f ->
    kinstr (Ty.Timestamp :: Ty.Num Ty.Num.Int :: s) f
  | ISub_timestamp_seconds {s f} :
    kinfo (Ty.Timestamp :: Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Timestamp :: s) f ->
    kinstr (Ty.Timestamp :: Ty.Num Ty.Num.Int :: s) f
  | IDiff_timestamps {s f} :
    kinfo (Ty.Timestamp :: Ty.Timestamp :: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Timestamp :: Ty.Timestamp :: s) f
  (*
     Tez
     ---
  *)
  | IAdd_tez {s f} :
    kinfo (Ty.Mutez :: Ty.Mutez :: s) ->
    kinstr (Ty.Mutez :: s) f ->
    kinstr (Ty.Mutez :: Ty.Mutez :: s) f
  | ISub_tez {s f} :
    kinfo (Ty.Mutez :: Ty.Mutez :: s) ->
    kinstr (Ty.Option Ty.Mutez :: s) f ->
    kinstr (Ty.Mutez :: Ty.Mutez :: s) f
  | ISub_tez_legacy {s f} :
    kinfo (Ty.Mutez :: Ty.Mutez :: s) ->
    kinstr (Ty.Mutez :: s) f ->
    kinstr (Ty.Mutez :: Ty.Mutez :: s) f
  | IMul_teznat {s f} :
    kinfo (Ty.Mutez :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Mutez :: s) f ->
    kinstr (Ty.Mutez :: Ty.Num Ty.Num.Nat :: s) f
  | IMul_nattez {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Mutez :: s) ->
    kinstr (Ty.Mutez :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Mutez :: s) f
  | IEdiv_teznat {s f} :
    kinfo (Ty.Mutez :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Option (Ty.Pair Ty.Mutez Ty.Mutez) :: s) f ->
    kinstr (Ty.Mutez :: Ty.Num Ty.Num.Nat :: s) f
  | IEdiv_tez {s f} :
    kinfo (Ty.Mutez :: Ty.Mutez :: s) ->
    kinstr (Ty.Option (Ty.Pair (Ty.Num Ty.Num.Nat) Ty.Mutez) :: s) f ->
    kinstr (Ty.Mutez :: Ty.Mutez :: s) f
  (*
    Booleans
    --------
  *)
  | IOr {s f} :
    kinfo (Ty.Bool :: Ty.Bool :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Bool :: Ty.Bool :: s) f
  | IAnd {s f} :
    kinfo (Ty.Bool :: Ty.Bool :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Bool :: Ty.Bool :: s) f
  | IXor {s f} :
    kinfo (Ty.Bool :: Ty.Bool :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Bool :: Ty.Bool :: s) f
  | INot {s f} :
    kinfo (Ty.Bool :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Bool :: s) f
  (*
     Integers
     --------
  *)
  | IIs_nat {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Option (Ty.Num Ty.Num.Nat) :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  | INeg {a s f} :
    kinfo (Ty.Num a :: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Num a :: s) f
  | IAbs_int {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  | IInt_nat {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f
  | IAdd_int {a b s f} :
    kinfo (Ty.Num a :: Ty.Num b :: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Num a :: Ty.Num b :: s) f
  | IAdd_nat {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) f
  | ISub_int {a b s f} :
    kinfo (Ty.Num a :: Ty.Num b:: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Num a :: Ty.Num b :: s) f
  | IMul_int {a b s f} :
    kinfo (Ty.Num a :: Ty.Num b :: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Num a :: Ty.Num b :: s) f
  | IMul_nat {a s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num a :: s) ->
    kinstr (Ty.Num a :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num a :: s) f
  | IEdiv_int {a b s f} :
    kinfo (Ty.Num a :: Ty.Num b :: s) ->
    kinstr
      (Ty.Option (Ty.Pair (Ty.Num Ty.Num.Int) (Ty.Num Ty.Num.Nat)) :: s) f ->
    kinstr (Ty.Num a :: Ty.Num b :: s) f
  | IEdiv_nat {a s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num a :: s) ->
    kinstr (Ty.Option (Ty.Pair (Ty.Num a) (Ty.Num Ty.Num.Nat)) :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num a :: s) f
  | ILsl_nat {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) f
  | ILsr_nat {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) f
  | IOr_nat {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) f
  | IAnd_nat {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) f
  | IAnd_int_nat {s f} :
    kinfo (Ty.Num Ty.Num.Int :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: Ty.Num Ty.Num.Nat :: s) f
  | IXor_nat {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) f
  | INot_int {a s f} :
    kinfo (Ty.Num a :: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Num a :: s) f
  (*
    Control
    -------
  *)
  | IIf {s u f} :
    kinfo (Ty.Bool :: s) ->
    kinstr s u ->
    kinstr s u ->
    kinstr u f ->
    kinstr (Ty.Bool :: s) f
  | ILoop {s f} :
    kinfo (Ty.Bool :: s) ->
    kinstr s (Ty.Bool :: s) ->
    kinstr s f ->
    kinstr (Ty.Bool :: s) f
  | ILoop_left {a b s f} :
    kinfo (Ty.Union a b :: s) ->
    kinstr (a :: s) (Ty.Union a b :: s) ->
    kinstr (b :: s) f ->
    kinstr (Ty.Union a b :: s) f
  | IDip {a s t f} :
    kinfo (a :: s) ->
    kinstr s t ->
    kinstr (a :: t) f ->
    kinstr (a :: s) f
  | IExec {a b s f} :
    kinfo (a :: Ty.Lambda a b :: s) ->
    kinstr (b :: s) f ->
    kinstr (a :: Ty.Lambda a b :: s) f
  | IApply {a b c s f} :
    kinfo (a :: Ty.Lambda (Ty.Pair a b) c :: s) ->
    ty a ->
    kinstr (Ty.Lambda b c :: s) f ->
    kinstr (a :: Ty.Lambda (Ty.Pair a b) c :: s) f
  | ILambda {s b c f} :
    kinfo s ->
    'lambda b c ->
    kinstr (Ty.Lambda b c :: s) f ->
    kinstr s f
  | IFailwith {a s f} :
    kinfo (a :: s) ->
    Alpha_context.Script.location ->
    ty a ->
    kinstr (a :: s) f
  (*
     Comparison
     ----------
  *)
  | ICompare {a s f} :
    kinfo (a :: a :: s) ->
    ty a ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (a :: a :: s) f
  (*
     Comparators
     -----------
  *)
  | IEq {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  | INeq {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  | ILt {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  | IGt {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  | ILe {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  | IGe {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  (*
     Protocol
     --------
  *)
  | IAddress {a s f} :
    kinfo (Ty.Contract a :: s) ->
    kinstr (Ty.Address :: s) f ->
    kinstr (Ty.Contract a :: s) f
  | IContract {a s f} :
    kinfo (Ty.Address :: s) ->
    ty a ->
    string ->
    kinstr (Ty.Option (Ty.Contract a) :: s) f ->
    kinstr (Ty.Address :: s) f
  | IView {a b s f} :
    kinfo (a :: Ty.Address :: s) ->
    view_signature a b ->
    kinstr (Ty.Option b :: s) f ->
    kinstr (a :: Ty.Address :: s) f
  | ITransfer_tokens {a s f} :
    kinfo (a :: Ty.Mutez :: Ty.Contract a :: s) ->
    kinstr (Ty.Operation :: s) f ->
    kinstr (a :: Ty.Mutez :: Ty.Contract a :: s) f
  | IImplicit_account {s f} :
    kinfo (Ty.Key_hash :: s) ->
    kinstr (Ty.Contract Ty.Unit :: s) f ->
    kinstr (Ty.Key_hash :: s) f
  | ICreate_contract {a s f} :
    kinfo (Ty.Option Ty.Key_hash :: Ty.Mutez :: a :: s) ->
    ty a ->
    Alpha_context.Script.expr ->
    kinstr (Ty.Operation :: Ty.Address :: s) f ->
    kinstr (Ty.Option Ty.Key_hash :: Ty.Mutez :: a :: s) f
  | ISet_delegate {s f} :
    kinfo (Ty.Option Ty.Key_hash :: s) ->
    kinstr (Ty.Operation :: s) f ->
    kinstr (Ty.Option Ty.Key_hash :: s) f
  | INow {s f} :
    kinfo s ->
    kinstr (Ty.Timestamp :: s) f ->
    kinstr s f
  | IMin_block_time {s f} :
    kinfo s ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr s f
  | IBalance {s f} :
    kinfo s ->
    kinstr (Ty.Mutez :: s) f ->
    kinstr s f
  | ILevel {s f} :
    kinfo s ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr s f
  | ICheck_signature {s f} :
    kinfo (Ty.Key :: Ty.Signature :: Ty.Bytes :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Key :: Ty.Signature :: Ty.Bytes :: s) f
  | IHash_key {s f} :
    kinfo (Ty.Key :: s) ->
    kinstr (Ty.Key_hash :: s) f ->
    kinstr (Ty.Key :: s) f
  | IPack {a s f} :
    kinfo (a :: s) ->
    ty a ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (a :: s) f
  | IUnpack {a s f} :
    kinfo (Ty.Bytes :: s) ->
    ty a ->
    kinstr (Ty.Option a :: s) f ->
    kinstr (Ty.Bytes :: s) f
  | IBlake2b {s f} :
    kinfo (Ty.Bytes :: s) ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (Ty.Bytes :: s) f
  | ISha256 {s f} :
    kinfo (Ty.Bytes :: s) ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (Ty.Bytes :: s) f
  | ISha512 {s f} :
    kinfo (Ty.Bytes :: s) ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (Ty.Bytes :: s) f
  | ISource {s f} :
    kinfo s ->
    kinstr (Ty.Address :: s) f ->
    kinstr s f
  | ISender {s f} :
    kinfo s ->
    kinstr (Ty.Address :: s) f ->
    kinstr s f
  | ISelf {b s f} :
    kinfo s ->
    ty b ->
    string ->
    kinstr (Ty.Contract b :: s) f ->
    kinstr s f
  | ISelf_address {s f} :
    kinfo s ->
    kinstr (Ty.Address :: s) f ->
    kinstr s f
  | IAmount {s f} :
    kinfo s ->
    kinstr (Ty.Mutez :: s) f ->
    kinstr s f
  | ISapling_empty_state {s f} :
    kinfo s ->
    Alpha_context.Sapling.Memo_size.t ->
    kinstr (Ty.Sapling_state :: s) f ->
    kinstr s f
  | ISapling_verify_update {s f} :
    kinfo (Ty.Sapling_transaction :: Ty.Sapling_state :: s) ->
    kinstr
      (Ty.Option (Ty.Pair
        Ty.Bytes
        (Ty.Pair (Ty.Num Ty.Num.Int) Ty.Sapling_state)) ::
        s)
      f ->
    kinstr (Ty.Sapling_transaction :: Ty.Sapling_state :: s) f
  | ISapling_verify_update_deprecated {s f} :
    kinfo (Ty.Sapling_transaction_deprecated :: Ty.Sapling_state :: s) ->
    kinstr
      (Ty.Option (Ty.Pair (Ty.Num Ty.Num.Int) Ty.Sapling_state) :: s)
      f ->
    kinstr (Ty.Sapling_transaction_deprecated :: Ty.Sapling_state :: s) f
  | IDig {a s t u f} :
    kinfo t ->
    int ->
    stack_prefix_preservation_witness (a :: s) s t u ->
    kinstr (a :: u) f  ->
    kinstr t f
  | IDug {a s t u f} :
    kinfo (a :: t) ->
    int ->
    stack_prefix_preservation_witness s (a :: s) t u ->
    kinstr u f ->
    kinstr (a :: t) f
  | IDipn {s t u v f} :
    kinfo s ->
    int ->
    stack_prefix_preservation_witness t v s u ->
    kinstr t v ->
    kinstr u f ->
    kinstr s f
  | IDropn {s u f} :
    kinfo s ->
    int ->
    stack_prefix_preservation_witness u u s s ->
    kinstr u f ->
    kinstr s f
  | IChainId {s f} :
    kinfo s ->
    kinstr (Ty.Chain_id :: s) f ->
    kinstr s f
  | INever {s f} :
    kinfo (Ty.Never :: s) ->
    kinstr (Ty.Never :: s) f
  | IVoting_power {s f} :
    kinfo (Ty.Key_hash :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Key_hash :: s) f
  | ITotal_voting_power {s f} :
    kinfo s ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr s f
  | IKeccak {s f} :
    kinfo (Ty.Bytes :: s) ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (Ty.Bytes :: s) f
  | ISha3 {s f} :
    kinfo (Ty.Bytes :: s) ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (Ty.Bytes :: s) f
  | IAdd_bls12_381_g1 {s f} :
    kinfo (Ty.Bls12_381_g1 :: Ty.Bls12_381_g1 :: s) ->
    kinstr (Ty.Bls12_381_g1 :: s) f ->
    kinstr (Ty.Bls12_381_g1 :: Ty.Bls12_381_g1 :: s) f
  | IAdd_bls12_381_g2 {s f} :
    kinfo (Ty.Bls12_381_g2 :: Ty.Bls12_381_g2 :: s) ->
    kinstr (Ty.Bls12_381_g2 :: s) f ->
    kinstr (Ty.Bls12_381_g2 :: Ty.Bls12_381_g2 :: s) f
  | IAdd_bls12_381_fr {s f} :
    kinfo (Ty.Bls12_381_fr :: Ty.Bls12_381_fr :: s) ->
    kinstr (Ty.Bls12_381_fr :: s) f ->
    kinstr (Ty.Bls12_381_fr :: Ty.Bls12_381_fr :: s) f
  | IMul_bls12_381_g1 {s f} :
    kinfo (Ty.Bls12_381_g1 :: Ty.Bls12_381_fr :: s) ->
    kinstr (Ty.Bls12_381_g1 :: s) f ->
    kinstr (Ty.Bls12_381_g1 :: Ty.Bls12_381_fr :: s) f
  | IMul_bls12_381_g2 {s f} :
    kinfo (Ty.Bls12_381_g2 :: Ty.Bls12_381_fr :: s) ->
    kinstr (Ty.Bls12_381_g2 :: s) f ->
    kinstr (Ty.Bls12_381_g2 :: Ty.Bls12_381_fr :: s) f
  | IMul_bls12_381_fr {s f} :
    kinfo (Ty.Bls12_381_fr :: Ty.Bls12_381_fr :: s) ->
    kinstr (Ty.Bls12_381_fr :: s) f ->
    kinstr (Ty.Bls12_381_fr :: Ty.Bls12_381_fr :: s) f
  | IMul_bls12_381_z_fr {a s f} :
    kinfo (Ty.Bls12_381_fr :: Ty.Num a :: s) ->
    kinstr (Ty.Bls12_381_fr :: s) f ->
    kinstr (Ty.Bls12_381_fr :: Ty.Num a :: s) f
  | IMul_bls12_381_fr_z {a s f} :
    kinfo (Ty.Num a :: Ty.Bls12_381_fr :: s) ->
    kinstr (Ty.Bls12_381_fr :: s) f ->
    kinstr (Ty.Num a :: Ty.Bls12_381_fr :: s) f
  | IInt_bls12_381_fr {s f} :
    kinfo (Ty.Bls12_381_fr :: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Bls12_381_fr :: s) f
  | INeg_bls12_381_g1 {s f} :
    kinfo (Ty.Bls12_381_g1 :: s) ->
    kinstr (Ty.Bls12_381_g1 :: s) f ->
    kinstr (Ty.Bls12_381_g1 :: s) f
  | INeg_bls12_381_g2 {s f} :
    kinfo (Ty.Bls12_381_g2 :: s) ->
    kinstr (Ty.Bls12_381_g2 :: s) f ->
    kinstr (Ty.Bls12_381_g2 :: s) f
  | INeg_bls12_381_fr {s f} :
    kinfo (Ty.Bls12_381_fr :: s) ->
    kinstr (Ty.Bls12_381_fr :: s) f ->
    kinstr (Ty.Bls12_381_fr :: s) f
  | IPairing_check_bls12_381 {s f} :
    kinfo (Ty.List (Ty.Pair Ty.Bls12_381_g1 Ty.Bls12_381_g2) :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.List (Ty.Pair Ty.Bls12_381_g1 Ty.Bls12_381_g2) :: s) f
  | IComb {s u f} :
    kinfo s ->
    int ->
    comb_gadt_witness s u ->
    kinstr u f ->
    kinstr s f
  | IUncomb {s u f} :
    kinfo s ->
    int ->
    uncomb_gadt_witness s u ->
    kinstr u f ->
    kinstr s f
  | IComb_get {s t v f} :
    kinfo (t :: s) ->
    int ->
    comb_get_gadt_witness t v ->
    kinstr (v :: s) f ->
    kinstr (t :: s) f
  | IComb_set {a b c s f} :
    kinfo (a :: b :: s) ->
    int ->
    comb_set_gadt_witness a b c ->
    kinstr (c :: s) f ->
    kinstr (a :: b :: s) f
  | IDup_n {s t f} :
    kinfo s ->
    int ->
    dup_n_gadt_witness s t ->
    kinstr (t :: s) f ->
    kinstr s f
  | ITicket {a s f} :
    kinfo (a :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Ticket a :: s) f ->
    kinstr (a :: Ty.Num Ty.Num.Nat :: s) f
  | IRead_ticket {a s f} :
    kinfo (Ty.Ticket a :: s) ->
    kinstr
      (Ty.Pair Ty.Address (Ty.Pair a (Ty.Num Ty.Num.Nat)) ::
        Ty.Ticket a ::
        s) f ->
    kinstr (Ty.Ticket a :: s) f
  | ISplit_ticket {a s f} :
    kinfo (Ty.Ticket a :: (Ty.Pair (Ty.Num Ty.Num.Nat) (Ty.Num Ty.Num.Nat)) :: s) ->
    kinstr (Ty.Option (Ty.Pair (Ty.Ticket a) (Ty.Ticket a)) :: s) f ->
    kinstr (Ty.Ticket a :: (Ty.Pair (Ty.Num Ty.Num.Nat) (Ty.Num Ty.Num.Nat)) :: s) f
  | IJoin_tickets {a s f} :
    kinfo (Ty.Pair (Ty.Ticket a) (Ty.Ticket a) :: s) ->
    ty a ->
    kinstr (Ty.Option (Ty.Ticket a) :: s) f ->
    kinstr (Ty.Pair (Ty.Ticket a) (Ty.Ticket a) :: s) f
  | IOpen_chest {s f} : 
    kinfo (Ty.Chest_key :: Ty.Chest :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Union Ty.Bytes Ty.Bool :: s) f ->
    kinstr (Ty.Chest_key :: Ty.Chest :: Ty.Num Ty.Num.Nat :: s) f
  (*
    Internal control instructions
    -----------------------------
  *)
  | IHalt {s} :
    kinfo s ->
    kinstr s s
  (* We choose to ignore the [ILog] function. *)

  (* We do not define [logger] and [logging_function] as we choose to ignore
     the logging. *)

  where "'kdescr" := (fun s f => kdescr.skeleton s f stack_ty kinstr)

  and "'lambda" := (fun arg ret =>
    'kdescr [arg] [ret] * Alpha_context.Script.node
  )

  and "'ty_to_dep_Set" := (
    Ty.to_Set_aux {|
      Ty.Parametrized_sets.lambda := 'lambda;
      Ty.Parametrized_sets.set ty_k _ := set ty_k;
      Ty.Parametrized_sets.map ty_k _ v := map ty_k v;
      Ty.Parametrized_sets.big_map := big_map.skeleton;
      Ty.Parametrized_sets.contract := typed_contract;
    |}
  ).

  (** The dependent [kdescr] type. *)
  Definition kdescr : Stack_ty.t -> Stack_ty.t -> Set := 'kdescr.

  (** The dependent [lambda] type. *)
  Definition lambda : Ty.t -> Ty.t -> Set := 'lambda.

  (** The [Set] implementing a [Ty.t] in the dependent version. *)
  Definition ty_to_dep_Set : Ty.t -> Set := 'ty_to_dep_Set.

  (** The dependent [big_map] type. *)
  Definition big_map (k v : Ty.t) : Set :=
    big_map.skeleton k v (ty_to_dep_Set k) (ty_to_dep_Set v).

  Definition stack_ty_to_dep_Set_head : Stack_ty.t -> Set :=
    Stack_ty.map_head ty_to_dep_Set.
  (* We automatically unfold this definition *)
  Arguments stack_ty_to_dep_Set_head _ /.

  Definition stack_ty_to_dep_Set_tail : Stack_ty.t -> Set :=
    Stack_ty.map_tail ty_to_dep_Set.
  (* We automatically unfold this definition *)
  Arguments stack_ty_to_dep_Set_tail _ /.

  (** The [Set] implementing a [Stack_ty.t] in the dependent version. *)
  Definition stack_ty_to_dep_Set (tys : Stack_ty.t) : Set :=
    stack_ty_to_dep_Set_head tys * stack_ty_to_dep_Set_tail tys.
  (* We automatically unfold this definition *)
  Arguments stack_ty_to_dep_Set _ /.

  (** Apply a function [f] on the contents of a ticket. *)
  Definition ticket_map {a b : Set} (f : a -> b)
    (ticket : Script_typed_ir.ticket a) : Script_typed_ir.ticket b :=
    {|
      Script_typed_ir.ticket.ticketer :=
        ticket.(Script_typed_ir.ticket.ticketer);
      Script_typed_ir.ticket.contents :=
        f ticket.(Script_typed_ir.ticket.contents);
      Script_typed_ir.ticket.amount :=
        ticket.(Script_typed_ir.ticket.amount);
    |}.

  (** Convert an instruction back to the OCaml version. *)
  #[bypass_check(guard)]
  Fixpoint to_kinstr {s f} (i : kinstr s f) {struct i} :
    Script_typed_ir.kinstr :=
    match i with
    (*
      Stack
      -----
    *)
    | IDrop info k =>
      Script_typed_ir.IDrop (to_kinfo info) (to_kinstr k)
    | IDup info k =>
      Script_typed_ir.IDup (to_kinfo info) (to_kinstr k)
    | ISwap info k =>
      Script_typed_ir.ISwap (to_kinfo info) (to_kinstr k)
    | IConst info v k =>
      Script_typed_ir.IConst (to_kinfo info) (to_value v) (to_kinstr k)
    (*
      Pairs
      -----
    *)
    | ICons_pair info k =>
      Script_typed_ir.ICons_pair (to_kinfo info) (to_kinstr k)
    | ICar info k =>
      Script_typed_ir.ICar (to_kinfo info) (to_kinstr k)
    | ICdr info k =>
      Script_typed_ir.ICdr (to_kinfo info) (to_kinstr k)
    | IUnpair info k =>
      Script_typed_ir.IUnpair (to_kinfo info) (to_kinstr k)
    (*
      Options
      -------
    *)
    | ICons_some info k =>
      Script_typed_ir.ICons_some (to_kinfo info) (to_kinstr k)
    | ICons_none info k =>
      Script_typed_ir.ICons_none (to_kinfo info) (to_kinstr k)
    | IIf_none info branch_if_none branch_if_some k =>
      Script_typed_ir.IIf_none {|
        Script_typed_ir.kinstr.IIf_none.kinfo := to_kinfo info;
        Script_typed_ir.kinstr.IIf_none.branch_if_none := to_kinstr branch_if_none;
        Script_typed_ir.kinstr.IIf_none.branch_if_some := to_kinstr branch_if_some;
        Script_typed_ir.kinstr.IIf_none.k := to_kinstr k;
      |}
    | IOpt_map info body k =>
      Script_typed_ir.IOpt_map {|
        Script_typed_ir.kinstr.IOpt_map.kinfo := to_kinfo info;
        Script_typed_ir.kinstr.IOpt_map.body := to_kinstr body;
        Script_typed_ir.kinstr.IOpt_map.k := to_kinstr k;
      |}
    (*
      Unions
      ------
    *)
    | ICons_left info k =>
      Script_typed_ir.ICons_left (to_kinfo info) (to_kinstr k)
    | ICons_right info k =>
      Script_typed_ir.ICons_right (to_kinfo info) (to_kinstr k)
    | IIf_left info branch_if_left branch_if_right k =>
      Script_typed_ir.IIf_left {|
        Script_typed_ir.kinstr.IIf_left.kinfo := to_kinfo info;
        Script_typed_ir.kinstr.IIf_left.branch_if_left := to_kinstr branch_if_left;
        Script_typed_ir.kinstr.IIf_left.branch_if_right := to_kinstr branch_if_right;
        Script_typed_ir.kinstr.IIf_left.k := to_kinstr k;
      |}
    (*
      Lists
      -----
    *)
    | ICons_list info k =>
      Script_typed_ir.ICons_list (to_kinfo info) (to_kinstr k)
    | INil info k =>
      Script_typed_ir.INil (to_kinfo info) (to_kinstr k)
    | IIf_cons info branch_if_cons branch_if_nil k =>
      Script_typed_ir.IIf_cons {|
        Script_typed_ir.kinstr.IIf_cons.kinfo := to_kinfo info;
        Script_typed_ir.kinstr.IIf_cons.branch_if_cons := to_kinstr branch_if_cons;
        Script_typed_ir.kinstr.IIf_cons.branch_if_nil := to_kinstr branch_if_nil;
        Script_typed_ir.kinstr.IIf_cons.k := to_kinstr k;
      |}
    | IList_map info body k =>
      Script_typed_ir.IList_map (to_kinfo info) (to_kinstr body) (to_kinstr k)
    | IList_iter info body k =>
      Script_typed_ir.IList_iter (to_kinfo info) (to_kinstr body) (to_kinstr k)
    | IList_size info k =>
      Script_typed_ir.IList_size (to_kinfo info) (to_kinstr k)
    (*
    Sets
    ----
     *)
    | IEmpty_set info ty instr =>
      Script_typed_ir.IEmpty_set
        (to_kinfo info) (to_ty ty) (to_kinstr instr)
    | ISet_iter info instr1 instr2 =>
      Script_typed_ir.ISet_iter
        (to_kinfo info) (to_kinstr instr1) (to_kinstr instr2)
    | ISet_mem info instr =>
      Script_typed_ir.ISet_mem (to_kinfo info) (to_kinstr instr)
    | ISet_update info instr =>
      Script_typed_ir.ISet_update (to_kinfo info) (to_kinstr instr)
    | ISet_size info instr =>
      Script_typed_ir.ISet_size (to_kinfo info) (to_kinstr instr)
    (*
     Maps
     ----
     *)
    | IEmpty_map info ty instr =>
      Script_typed_ir.IEmpty_map
        (to_kinfo info) (to_ty ty) (to_kinstr instr)
    | IMap_map info instr1 instr2 =>
      Script_typed_ir.IMap_map
        (to_kinfo info) (to_kinstr instr1) (to_kinstr instr2)
    | IMap_iter info instr1 instr2 =>
      Script_typed_ir.IMap_iter
        (to_kinfo info) (to_kinstr instr1) (to_kinstr instr2)
    | IMap_mem info instr =>
      Script_typed_ir.IMap_mem (to_kinfo info) (to_kinstr instr)
    | IMap_get info instr =>
      Script_typed_ir.IMap_get (to_kinfo info) (to_kinstr instr)
    | IMap_update info instr =>
      Script_typed_ir.IMap_update (to_kinfo info) (to_kinstr instr)
    | IMap_get_and_update info instr =>
      Script_typed_ir.IMap_get_and_update (to_kinfo info) (to_kinstr instr)
    | IMap_size info instr =>
      Script_typed_ir.IMap_size (to_kinfo info) (to_kinstr instr)
    (*
     Big maps
     --------
     *)
    | IEmpty_big_map info cty ty instr =>
      Script_typed_ir.IEmpty_big_map
        (to_kinfo info) (to_ty cty) (to_ty ty) (to_kinstr instr)
    | IBig_map_mem info instr =>
      Script_typed_ir.IBig_map_mem (to_kinfo info) (to_kinstr instr)
    | IBig_map_get info instr =>
      Script_typed_ir.IBig_map_get (to_kinfo info) (to_kinstr instr)
    | IBig_map_update info instr =>
      Script_typed_ir.IBig_map_update (to_kinfo info) (to_kinstr instr)
    | IBig_map_get_and_update info instr =>
      Script_typed_ir.IBig_map_get_and_update (to_kinfo info) (to_kinstr instr)
    (*
     Strings
     -------
     *)
    | IConcat_string info instr =>
      Script_typed_ir.IConcat_string (to_kinfo info) (to_kinstr instr)
    | IConcat_string_pair info instr =>
      Script_typed_ir.IConcat_string_pair (to_kinfo info) (to_kinstr instr)
    | ISlice_string info instr =>
      Script_typed_ir.ISlice_string (to_kinfo info) (to_kinstr instr)
    | IString_size info instr =>
      Script_typed_ir.IString_size (to_kinfo info) (to_kinstr instr)
    (*
     Bytes
     -----
     *)
    | IConcat_bytes info instr =>
      Script_typed_ir.IConcat_bytes (to_kinfo info) (to_kinstr instr)
    | IConcat_bytes_pair info instr =>
      Script_typed_ir.IConcat_bytes_pair (to_kinfo info) (to_kinstr instr)
    | ISlice_bytes info instr =>
      Script_typed_ir.ISlice_bytes (to_kinfo info) (to_kinstr instr)
    | IBytes_size info instr =>
      Script_typed_ir.IBytes_size (to_kinfo info) (to_kinstr instr)
    (*
     Timestamps
     ----------
     *)
    | IAdd_seconds_to_timestamp info instr =>
      Script_typed_ir.IAdd_seconds_to_timestamp
        (to_kinfo info) (to_kinstr instr)
    | IAdd_timestamp_to_seconds info instr =>
      Script_typed_ir.IAdd_timestamp_to_seconds
        (to_kinfo info) (to_kinstr instr)
    | ISub_timestamp_seconds info instr =>
      Script_typed_ir.ISub_timestamp_seconds (to_kinfo info) (to_kinstr instr)
    | IDiff_timestamps info instr =>
      Script_typed_ir.IDiff_timestamps (to_kinfo info) (to_kinstr instr)
    (*
     Tez
     ---
     *)
    | IAdd_tez info instr =>
      Script_typed_ir.IAdd_tez (to_kinfo info) (to_kinstr instr)
    | ISub_tez info instr =>
      Script_typed_ir.ISub_tez (to_kinfo info) (to_kinstr instr)
    | ISub_tez_legacy info instr =>
      Script_typed_ir.ISub_tez_legacy (to_kinfo info) (to_kinstr instr)
    | IMul_teznat info instr =>
      Script_typed_ir.IMul_teznat (to_kinfo info) (to_kinstr instr)
    | IMul_nattez info instr =>
      Script_typed_ir.IMul_nattez (to_kinfo info) (to_kinstr instr)
    | IEdiv_teznat info instr =>
      Script_typed_ir.IEdiv_teznat (to_kinfo info) (to_kinstr instr)
    | IEdiv_tez info instr =>
      Script_typed_ir.IEdiv_tez (to_kinfo info) (to_kinstr instr)
    (*
      Booleans
      --------
    *)
    | IOr info k =>
      Script_typed_ir.IOr (to_kinfo info) (to_kinstr k)
    | IAnd info k =>
      Script_typed_ir.IAnd (to_kinfo info) (to_kinstr k)
    | IXor info k =>
      Script_typed_ir.IXor (to_kinfo info) (to_kinstr k)
    | INot info k =>
      Script_typed_ir.INot (to_kinfo info) (to_kinstr k)
    (*
     Integers
     --------
     *)
    | IIs_nat info instr =>
      Script_typed_ir.IIs_nat (to_kinfo info) (to_kinstr instr)
    | INeg info instr =>
      Script_typed_ir.INeg (to_kinfo info) (to_kinstr instr)
    | IAbs_int info instr =>
      Script_typed_ir.IAbs_int (to_kinfo info) (to_kinstr instr)
    | IInt_nat info instr =>
      Script_typed_ir.IInt_nat (to_kinfo info) (to_kinstr instr)
    | IAdd_int info instr =>
      Script_typed_ir.IAdd_int (to_kinfo info) (to_kinstr instr)
    | IAdd_nat info instr =>
      Script_typed_ir.IAdd_nat (to_kinfo info) (to_kinstr instr)
    | ISub_int info instr =>
      Script_typed_ir.ISub_int (to_kinfo info) (to_kinstr instr)
    | IMul_int info instr =>
      Script_typed_ir.IMul_int (to_kinfo info) (to_kinstr instr)
    | IMul_nat info instr =>
      Script_typed_ir.IMul_nat (to_kinfo info) (to_kinstr instr)
    | IEdiv_int info instr =>
      Script_typed_ir.IEdiv_int (to_kinfo info) (to_kinstr instr)
    | IEdiv_nat info instr =>
      Script_typed_ir.IEdiv_nat (to_kinfo info) (to_kinstr instr)
    | ILsl_nat info instr =>
      Script_typed_ir.ILsl_nat (to_kinfo info) (to_kinstr instr)
    | ILsr_nat info instr =>
      Script_typed_ir.ILsr_nat (to_kinfo info) (to_kinstr instr)
    | IOr_nat info instr =>
      Script_typed_ir.IOr_nat (to_kinfo info) (to_kinstr instr)
    | IAnd_nat info instr =>
      Script_typed_ir.IAnd_nat (to_kinfo info) (to_kinstr instr)
    | IAnd_int_nat info instr =>
      Script_typed_ir.IAnd_int_nat (to_kinfo info) (to_kinstr instr)
    | IXor_nat info instr =>
      Script_typed_ir.IXor_nat (to_kinfo info) (to_kinstr instr)
    | INot_int info instr =>
      Script_typed_ir.INot_int (to_kinfo info) (to_kinstr instr)
    (*
      Control
      -------
    *)
    | IIf info branch_if_true branch_if_false k =>
      Script_typed_ir.IIf {|
        Script_typed_ir.kinstr.IIf.kinfo := to_kinfo info;
        Script_typed_ir.kinstr.IIf.branch_if_true := to_kinstr branch_if_true;
        Script_typed_ir.kinstr.IIf.branch_if_false := to_kinstr branch_if_false;
        Script_typed_ir.kinstr.IIf.k := to_kinstr k;
      |}
    | ILoop info body k =>
      Script_typed_ir.ILoop (to_kinfo info) (to_kinstr body) (to_kinstr k)
    | ILoop_left info bl br =>
      Script_typed_ir.ILoop_left (to_kinfo info) (to_kinstr bl) (to_kinstr br)
    | IDip info b k =>
      Script_typed_ir.IDip (to_kinfo info) (to_kinstr b) (to_kinstr k)
    | IExec info k =>
      Script_typed_ir.IExec (to_kinfo info) (to_kinstr k)
    | IApply info capture_ty k =>
      Script_typed_ir.IApply (to_kinfo info) (to_ty capture_ty) (to_kinstr k)
    | ILambda info lam k =>
      Script_typed_ir.ILambda (to_kinfo info) (to_lambda lam) (to_kinstr k)
    | IFailwith info kloc tv =>
      Script_typed_ir.IFailwith (to_kinfo info) kloc (to_ty tv)
    (*
     Comparison
     ----------
     *)
    | ICompare info ty instr =>
      Script_typed_ir.ICompare
        (to_kinfo info) (to_ty ty) (to_kinstr instr)
    (*
     Comparators
     -----------
     *)
    | IEq info instr =>
      Script_typed_ir.IEq (to_kinfo info) (to_kinstr instr)
    | INeq info instr =>
      Script_typed_ir.INeq (to_kinfo info) (to_kinstr instr)
    | ILt info instr =>
      Script_typed_ir.ILt (to_kinfo info) (to_kinstr instr)
    | IGt info instr =>
      Script_typed_ir.IGt (to_kinfo info) (to_kinstr instr)
    | ILe info instr =>
      Script_typed_ir.ILe (to_kinfo info) (to_kinstr instr)
    | IGe info instr =>
      Script_typed_ir.IGe (to_kinfo info) (to_kinstr instr)
    (*
     Protocol
     --------
     *)
    | IAddress info instr =>
      Script_typed_ir.IAddress (to_kinfo info) (to_kinstr instr)
    | IContract info ty str instr =>
      Script_typed_ir.IContract
        (to_kinfo info) (to_ty ty) str (to_kinstr instr)
    | IView info sign instr =>
      Script_typed_ir.IView (to_kinfo info) (to_view_signature sign) (to_kinstr instr)
    | ITransfer_tokens info instr =>
      Script_typed_ir.ITransfer_tokens (to_kinfo info) (to_kinstr instr)
    | IImplicit_account info instr =>
      Script_typed_ir.IImplicit_account (to_kinfo info) (to_kinstr instr)
    | ICreate_contract info storage_type code instr =>
      Script_typed_ir.ICreate_contract {|
          Script_typed_ir.kinstr.ICreate_contract.kinfo := to_kinfo info;
          Script_typed_ir.kinstr.ICreate_contract.storage_type :=
            to_ty storage_type;
          Script_typed_ir.kinstr.ICreate_contract.code := code;
          Script_typed_ir.kinstr.ICreate_contract.k := to_kinstr instr
        |}
    | ISet_delegate info instr =>
      Script_typed_ir.ISet_delegate (to_kinfo info) (to_kinstr instr)
    | INow info instr =>
      Script_typed_ir.INow (to_kinfo info) (to_kinstr instr)
    | IMin_block_time info instr =>
      Script_typed_ir.IMin_block_time (to_kinfo info) (to_kinstr instr)
    | IBalance info instr =>
      Script_typed_ir.IBalance (to_kinfo info) (to_kinstr instr)
    | ILevel info instr =>
      Script_typed_ir.ILevel (to_kinfo info) (to_kinstr instr)
    | ICheck_signature info instr =>
      Script_typed_ir.ICheck_signature (to_kinfo info) (to_kinstr instr)
    | IHash_key info instr =>
      Script_typed_ir.IHash_key (to_kinfo info) (to_kinstr instr)
    | IPack info ty instr =>
      Script_typed_ir.IPack
        (to_kinfo info) (to_ty ty) (to_kinstr instr)
    | IUnpack info ty instr =>
      Script_typed_ir.IUnpack
        (to_kinfo info) (to_ty ty) (to_kinstr instr)
    | IBlake2b info instr =>
      Script_typed_ir.IBlake2b (to_kinfo info) (to_kinstr instr)
    | ISha256 info instr =>
      Script_typed_ir.ISha256 (to_kinfo info) (to_kinstr instr)
    | ISha512 info instr =>
      Script_typed_ir.ISha512 (to_kinfo info) (to_kinstr instr)
    | ISource info instr =>
      Script_typed_ir.ISource (to_kinfo info) (to_kinstr instr)
    | ISender info instr =>
      Script_typed_ir.ISender (to_kinfo info) (to_kinstr instr)
    | ISelf info ty str instr =>
      Script_typed_ir.ISelf
        (to_kinfo info) (to_ty ty) str (to_kinstr instr)
    | ISelf_address info instr =>
      Script_typed_ir.ISelf_address (to_kinfo info) (to_kinstr instr)
    | IAmount info instr =>
      Script_typed_ir.IAmount (to_kinfo info) (to_kinstr instr)
    | ISapling_empty_state info sz instr =>
      Script_typed_ir.ISapling_empty_state (to_kinfo info) sz (to_kinstr instr)
    | ISapling_verify_update info instr =>
      Script_typed_ir.ISapling_verify_update (to_kinfo info) (to_kinstr instr)
    | ISapling_verify_update_deprecated info instr =>
      Script_typed_ir.ISapling_verify_update_deprecated (to_kinfo info) (to_kinstr instr)
    | IDig info v spref instr =>
      Script_typed_ir.IDig (to_kinfo info) v (to_stack_prefix_preservation_witness spref) (to_kinstr instr)
    | IDug info v spref instr =>
      Script_typed_ir.IDug (to_kinfo info) v (to_stack_prefix_preservation_witness spref) (to_kinstr instr)
    | IDipn info v spref instr1 instr2 =>
      Script_typed_ir.IDipn
        (to_kinfo info) v (to_stack_prefix_preservation_witness spref) (to_kinstr instr1) (to_kinstr instr2)
    | IDropn info v spref instr =>
      Script_typed_ir.IDropn (to_kinfo info) v (to_stack_prefix_preservation_witness spref) (to_kinstr instr)
    | IChainId info instr =>
      Script_typed_ir.IChainId (to_kinfo info) (to_kinstr instr)
    | INever info => Script_typed_ir.INever (to_kinfo info)
    | IVoting_power info instr =>
      Script_typed_ir.IVoting_power (to_kinfo info) (to_kinstr instr)
    | ITotal_voting_power info instr =>
      Script_typed_ir.ITotal_voting_power (to_kinfo info) (to_kinstr instr)
    | IKeccak info instr =>
      Script_typed_ir.IKeccak (to_kinfo info) (to_kinstr instr)
    | ISha3 info instr =>
      Script_typed_ir.ISha3 (to_kinfo info) (to_kinstr instr)
    | IAdd_bls12_381_g1 info instr =>
      Script_typed_ir.IAdd_bls12_381_g1 (to_kinfo info) (to_kinstr instr)
    | IAdd_bls12_381_g2 info instr =>
      Script_typed_ir.IAdd_bls12_381_g2 (to_kinfo info) (to_kinstr instr)
    | IAdd_bls12_381_fr info instr =>
      Script_typed_ir.IAdd_bls12_381_fr (to_kinfo info) (to_kinstr instr)
    | IMul_bls12_381_g1 info instr =>
      Script_typed_ir.IMul_bls12_381_g1 (to_kinfo info) (to_kinstr instr)
    | IMul_bls12_381_g2 info instr =>
      Script_typed_ir.IMul_bls12_381_g2 (to_kinfo info) (to_kinstr instr)
    | IMul_bls12_381_fr info instr =>
      Script_typed_ir.IMul_bls12_381_fr (to_kinfo info) (to_kinstr instr)
    | IMul_bls12_381_z_fr info instr =>
      Script_typed_ir.IMul_bls12_381_z_fr (to_kinfo info) (to_kinstr instr)
    | IMul_bls12_381_fr_z info instr =>
      Script_typed_ir.IMul_bls12_381_fr_z (to_kinfo info) (to_kinstr instr)
    | IInt_bls12_381_fr info instr =>
      Script_typed_ir.IInt_bls12_381_fr (to_kinfo info) (to_kinstr instr)
    | INeg_bls12_381_g1 info instr =>
      Script_typed_ir.INeg_bls12_381_g1 (to_kinfo info) (to_kinstr instr)
    | INeg_bls12_381_g2 info instr =>
      Script_typed_ir.INeg_bls12_381_g2 (to_kinfo info) (to_kinstr instr)
    | INeg_bls12_381_fr info instr =>
      Script_typed_ir.INeg_bls12_381_fr (to_kinfo info) (to_kinstr instr)
    | IPairing_check_bls12_381 info instr =>
      Script_typed_ir.IPairing_check_bls12_381 (to_kinfo info) (to_kinstr instr)
    | IComb info v comb instr =>
      Script_typed_ir.IComb (to_kinfo info) v
        (to_comb_gadt_witness comb)
        (to_kinstr instr)
    | IUncomb info v comb instr =>
      Script_typed_ir.IUncomb (to_kinfo info) v
      (to_uncomb_gadt_witness comb)
      (to_kinstr instr)
    | IComb_get info v comb instr =>
      Script_typed_ir.IComb_get (to_kinfo info) v
      (to_comb_get_gadt_witness comb)
      (to_kinstr instr)
    | IComb_set info v comb instr =>
      Script_typed_ir.IComb_set (to_kinfo info) v
      (to_comb_set_gadt_witness comb)
      (to_kinstr instr)
    | IDup_n info v dup_n instr =>
      Script_typed_ir.IDup_n (to_kinfo info) v
      (to_dup_n_gadt_witness dup_n)
      (to_kinstr instr)
    | ITicket info instr =>
      Script_typed_ir.ITicket (to_kinfo info) (to_kinstr instr)
    | IRead_ticket info instr =>
      Script_typed_ir.IRead_ticket (to_kinfo info) (to_kinstr instr)
    | ISplit_ticket info instr =>
      Script_typed_ir.ISplit_ticket (to_kinfo info) (to_kinstr instr)
    | IJoin_tickets info ty instr =>
      Script_typed_ir.IJoin_tickets
        (to_kinfo info) (to_ty ty) (to_kinstr instr)
    | IOpen_chest info instr =>
      Script_typed_ir.IOpen_chest (to_kinfo info) (to_kinstr instr)
    (*
      Internal control instructions
      -----------------------------
    *)
    | IHalt info =>
      Script_typed_ir.IHalt (to_kinfo info)
    end

  with to_lambda {arg ret} (lam : lambda arg ret) {struct lam} :
    Script_typed_ir.lambda :=
    let '(descr, node) := lam in
    Script_typed_ir.Lam (to_kdescr descr) node

  with to_kdescr {s f} (descr : kdescr s f) {struct descr} :
    Script_typed_ir.kdescr :=
    {|
      Script_typed_ir.kdescr.kloc := descr.(kdescr.kloc);
      Script_typed_ir.kdescr.kbef := to_stack_ty descr.(kdescr.kbef);
      Script_typed_ir.kdescr.kaft := to_stack_ty descr.(kdescr.kaft);
      Script_typed_ir.kdescr.kinstr := to_kinstr descr.(kdescr.kinstr);
    |}

  (** Conversion of Michelson values back to the OCaml version. *)
  with to_value {ty : Ty.t} {struct ty} : ty_to_dep_Set ty -> Ty.to_Set ty :=
    match ty return ty_to_dep_Set ty -> Ty.to_Set ty with
    | Ty.Unit => fun x => x
    | Ty.Num _ => fun x => x
    | Ty.Signature => fun x => x
    | Ty.String => fun x => x
    | Ty.Bytes => fun x => x
    | Ty.Mutez => fun x => x
    | Ty.Key_hash => fun x => x
    | Ty.Key => fun x => x
    | Ty.Timestamp => fun x => x
    | Ty.Address => fun x => x
    | Ty.Tx_rollup_l2_address => fun x => x
    | Ty.Bool => fun x => x
    | Ty.Pair ty1 ty2 => fun '(x1, x2) => (@to_value ty1 x1, @to_value ty2 x2)
    | Ty.Union ty1 ty2 => fun x =>
      match x with
      | Script_typed_ir.L x => Script_typed_ir.L (@to_value ty1 x)
      | Script_typed_ir.R x => Script_typed_ir.R (@to_value ty2 x)
      end
    | Ty.Lambda ty_arg ty_res => to_lambda
    | Ty.Option ty => fun x =>
      match x with
      | None => None
      | Some x => Some (@to_value ty x)
      end
    | Ty.List ty => fun x =>
      {|
        Script_typed_ir.boxed_list.elements :=
          List.map (@to_value ty) x.(Script_typed_ir.boxed_list.elements);
        Script_typed_ir.boxed_list.length :=
          x.(Script_typed_ir.boxed_list.length);
      |}
    | Ty.Set_ k => @to_set k
    | Ty.Map k v => @to_map_aux k (ty_to_dep_Set v) (Ty.to_Set v) to_value
    | Ty.Big_map k v => @to_big_map_aux k v _ _ _ _ to_value to_value
    | Ty.Contract ty => to_typed_contract
    | Ty.Sapling_transaction => fun x => x
    | Ty.Sapling_transaction_deprecated => fun x => x
    | Ty.Sapling_state => fun x => x
    | Ty.Operation => fun x => x
    | Ty.Chain_id => fun x => x
    | Ty.Never => fun x => x
    | Ty.Bls12_381_g1 => fun x => x
    | Ty.Bls12_381_g2 => fun x => x
    | Ty.Bls12_381_fr => fun x => x
    | Ty.Ticket ty => ticket_map (@to_value ty)
    | Ty.Chest_key => fun x => x
    | Ty.Chest => fun x => x
    end.

  (** A reverse conversion, from the OCaml definition back to the dependent one.
      This is only possible on a subset of [Ty.t], and we only define this
      function for comparable types. This is useful for retrieving keys from
      the dependent sets or maps. Indeed, we store the keys using their OCaml
      type, to break a dependency circle in the definitions. *)
  Fixpoint of_value {ty : Ty.t} {struct ty} :
    Ty.to_Set ty -> option (ty_to_dep_Set ty) :=
    match ty return Ty.to_Set ty -> option (ty_to_dep_Set ty) with
    | Ty.Unit => fun x => Some x
    | Ty.Num _ => fun x => Some x
    | Ty.Signature => fun x => Some x
    | Ty.String => fun x => Some x
    | Ty.Bytes => fun x => Some x
    | Ty.Mutez => fun x => Some x
    | Ty.Key_hash => fun x => Some x
    | Ty.Key => fun x => Some x
    | Ty.Timestamp => fun x => Some x
    | Ty.Address => fun x => Some x
    | Ty.Tx_rollup_l2_address => fun x => Some x
    | Ty.Bool => fun x => Some x
    | Ty.Pair ty1 ty2 => fun '(x1, x2) =>
      let* x1 := @of_value ty1 x1 in
      let* x2 := @of_value ty2 x2 in
      Some (x1, x2)
    | Ty.Union ty1 ty2 => fun x =>
      match x with
      | Script_typed_ir.L x =>
        let* x := @of_value ty1 x in
        Some (Script_typed_ir.L x)
      | Script_typed_ir.R x =>
        let* x := @of_value ty2 x in
        Some (Script_typed_ir.R x)
      end
    | Ty.Lambda ty_arg ty_res => fun _ => None
    | Ty.Option ty => fun x =>
      match x with
      | None => Some None
      | Some x =>
        let* x := @of_value ty x in
        Some (Some x)
      end
    | Ty.List ty => fun _ => None
    | Ty.Set_ k => fun _ => None
    | Ty.Map k v => fun _ => None
    | Ty.Big_map k v => fun _ => None
    | Ty.Contract ty => fun _ => None
    | Ty.Sapling_transaction => fun _ => None
    | Ty.Sapling_transaction_deprecated => fun _ => None
    | Ty.Sapling_state => fun _ => None
    | Ty.Operation => fun _ => None
    | Ty.Chain_id => fun x => Some x
    | Ty.Never => fun _ => None
    | Ty.Bls12_381_g1 => fun _ => None
    | Ty.Bls12_381_g2 => fun _ => None
    | Ty.Bls12_381_fr => fun _ => None
    | Ty.Ticket ty => fun _ => None
    | Ty.Chest_key => fun _ => None
    | Ty.Chest => fun _ => None
    end.

  (** Convert back a dependent [map] to its OCaml version. *)
  Definition to_map {ty_k ty_v : Ty.t} (x : map ty_k (ty_to_dep_Set ty_v)) :
    Script_typed_ir.map (Ty.to_Set ty_k) (Ty.to_Set ty_v) :=
    to_map_aux to_value x.

  (** Convert back a dependent [big_map] to its OCaml version. *)
  Definition to_big_map {ty_k ty_v : Ty.t} (x : big_map ty_k ty_v) :
    Script_typed_ir.big_map :=
    to_big_map_aux to_value to_value x.

  (** Convert back a dependent [ticket] to its OCaml version. *)
  Definition to_ticket {ty : Ty.t} :
    Script_typed_ir.ticket (ty_to_dep_Set ty) ->
    Script_typed_ir.ticket (Ty.to_Set ty) :=
    ticket_map to_value.

  Definition to_stack_value_head {tys : Stack_ty.t} :
    stack_ty_to_dep_Set_head tys -> Stack_ty.to_Set_head tys :=
    match tys with
    | [] => fun a => a
    | ty :: _ => fun a => @to_value ty a
    end.

  Fixpoint to_stack_value_tail {tys : Stack_ty.t} :
    stack_ty_to_dep_Set_tail tys -> Stack_ty.to_Set_tail tys :=
    match tys with
    | [] => fun s => s
    | _ :: tys => fun s =>
      (@to_stack_value_head tys (fst s), @to_stack_value_tail tys (snd s))
    end.

  (** Conversion of Michelson stack values back to the OCaml version. *)
  Definition to_stack_value {tys : Stack_ty.t}
    (s : stack_ty_to_dep_Set tys) : Stack_ty.to_Set tys :=
    (to_stack_value_head (fst s), to_stack_value_tail (snd s)).
  (* We automatically unfold this definition *)
  Arguments to_stack_value _ _ /.

  Module script.
    Record record {arg storage : Ty.t} : Set := {
      code :
        lambda (Ty.Pair arg storage) (Ty.Pair (Ty.List Ty.Operation) storage);
      arg_type : ty arg;
      storage_type : ty storage;
      storage : ty_to_dep_Set storage;
      views : Script_typed_ir.view_map;
      entrypoints : Script_typed_ir.entrypoints;
      code_size : Cache_memory_helpers.sint;
    }.
    Arguments record : clear implicits.
  End script.
  Definition script := script.record.

  (** Conversion back to [script]. *)
  Definition to_script {arg storage} (v : script arg storage) :
    Script_typed_ir.script :=
    Script_typed_ir.Script {|
      Script_typed_ir.script.Script.code := to_lambda v.(script.code);
      Script_typed_ir.script.Script.arg_type := to_ty v.(script.arg_type);
      Script_typed_ir.script.Script.storage_type := to_ty v.(script.storage_type);
      Script_typed_ir.script.Script.storage := to_value v.(script.storage);
      Script_typed_ir.script.Script.views := v.(script.views);
      Script_typed_ir.script.Script.entrypoints := v.(script.entrypoints);
      Script_typed_ir.script.Script.code_size := v.(script.code_size);
    |}.

  Inductive continuation : Stack_ty.t -> Stack_ty.t -> Set :=
  | KNil {f} :
    continuation f f
  | KCons {s t f} :
    kinstr s t -> continuation t f -> continuation s f
  | KReturn {a s f} :
    stack_ty_to_dep_Set s -> continuation (a :: s) f -> continuation [a] f
  (* We specialize this constructor on the [Some] function as it seems to be
     the only one used. This simplify our code later. *)
  | KMap_head {a s f} :
      continuation (Ty.Option a :: s) f ->
      continuation (a :: s) f
  | KUndip {b s f} :
      ty_to_dep_Set b ->
      continuation (b :: s) f ->
      continuation s f
  | KLoop_in {s f} :
      kinstr s (Ty.Bool :: s) ->
      continuation s f ->
      continuation (Ty.Bool :: s) f
  | KLoop_in_left {a b s f} :
      kinstr (a :: s) (Ty.Union a b :: s) ->
      continuation (b :: s) f ->
      continuation (Ty.Union a b :: s) f
  | KIter {a s f} :
      kinstr (a :: s) s ->
      list (ty_to_dep_Set a) ->
      continuation s f ->
      continuation s f
  | KList_enter_body {a b s f} :
      kinstr (a :: s) (b :: s) ->
      list (ty_to_dep_Set a) ->
      list (ty_to_dep_Set b) ->
      int ->
      continuation (Ty.List b :: s) f ->
      continuation s f
  | KList_exit_body {a b s f} :
      kinstr (a :: s) (b :: s) ->
      list (ty_to_dep_Set a) ->
      list (ty_to_dep_Set b) ->
      int ->
      continuation (Ty.List b :: s) f ->
      continuation (b :: s) f
  | KMap_enter_body {a b c s f} :
      kinstr (Ty.Pair a b :: s) (c :: s) ->
      list (ty_to_dep_Set a * ty_to_dep_Set b) ->
      ty_to_dep_Set (Ty.Map a c) ->
      continuation (Ty.Map a c :: s) f ->
      continuation s f
  | KMap_exit_body {a b c s f} :
      kinstr (Ty.Pair a b :: s) (c :: s) ->
      list (ty_to_dep_Set (Ty.Pair a b)) ->
      ty_to_dep_Set (Ty.Map a c) ->
      ty_to_dep_Set a ->
      continuation (Ty.Map a c :: s) f ->
      continuation (c :: s) f
  | KView_exit {s f} :
    Script_typed_ir.step_constants -> continuation s f -> continuation s f
  (* We choose to ignore the [KLog] instruction. *).

  Fixpoint to_continuation {arg ret} (c : continuation arg ret) :
    Script_typed_ir.continuation :=
    match c with 
    | KNil => Script_typed_ir.KNil
    | KCons instr c =>
      Script_typed_ir.KCons (to_kinstr instr) (to_continuation c)
    | KReturn s c =>
      Script_typed_ir.KReturn (to_stack_value s) (to_continuation c)
    | @KMap_head a _ _ c =>
      Script_typed_ir.KMap_head (a := Ty.to_Set a) Some (to_continuation c)
    | KUndip b c =>
      Script_typed_ir.KUndip (to_value b) (to_continuation c)
    | KLoop_in i c =>
      Script_typed_ir.KLoop_in (to_kinstr i) (to_continuation c)
    | KLoop_in_left i c =>
      Script_typed_ir.KLoop_in_left (to_kinstr i) (to_continuation c)
    | KIter i xs c =>
      Script_typed_ir.KIter
        (to_kinstr i)
        (List.map to_value xs)
        (to_continuation c)
    | KList_enter_body i xs ys n c =>
      Script_typed_ir.KList_enter_body
        (to_kinstr i)
        (List.map to_value xs)
        (List.map to_value ys)
        n
        (to_continuation c)
    | KList_exit_body i xs ys n c =>
      Script_typed_ir.KList_exit_body
        (to_kinstr i)
        (List.map to_value xs)
        (List.map to_value ys)
        n
        (to_continuation c)
    | KMap_enter_body i xs m c =>
      Script_typed_ir.KMap_enter_body
        (to_kinstr i)
        ((List.map (fun '(x1, x2) => (to_value x1, to_value x2)) xs))
        (to_map m)
        (to_continuation c)
    | KMap_exit_body i xs m x c =>
      Script_typed_ir.KMap_exit_body
        (to_kinstr i)
        ((List.map (fun '(x1, x2) => (to_value x1, to_value x2)) xs))
        (to_map m)
        (to_value x)
        (to_continuation c)
    | KView_exit step c =>
      Script_typed_ir.KView_exit step (to_continuation c)
    end.
End With_family.

Module Type_size.
  (** Simulation of [check_eq]. *)
  Definition dep_check_eq {error_trace}
    (error_details : Script_tc_errors.dep_error_details error_trace)
    (x_value : int) (y_value : int) :
    Pervasives.result unit
      (Script_tc_errors.Error_trace_family.to_Set error_trace) :=
    if x_value =i y_value then
      Result.return_unit
    else
      Pervasives.Error
        match error_details
          in Script_tc_errors.dep_error_details error_trace
          return Script_tc_errors.Error_trace_family.to_Set error_trace
        with
        | Script_tc_errors.Dep_fast =>
          Script_tc_errors.Inconsistent_types_fast
        | Script_tc_errors.Dep_informative =>
          Error_monad.trace_of_error
            (Build_extensible "Inconsistent_type_sizes" _
              (x_value, y_value))
        end.
End Type_size.

(** Simulation of [kinfo_of_kinstr]. *)
Definition dep_kinfo_of_kinstr {s f} (i_value : With_family.kinstr s f) :
  With_family.kinfo s :=
  match i_value with
  | With_family.IDrop kinfo_value _ => kinfo_value
  | With_family.IDup kinfo_value _ => kinfo_value
  | With_family.ISwap kinfo_value _ => kinfo_value
  | With_family.IConst kinfo_value _ _ => kinfo_value
  | With_family.ICons_pair kinfo_value _ => kinfo_value
  | With_family.ICar kinfo_value _ => kinfo_value
  | With_family.ICdr kinfo_value _ => kinfo_value
  | With_family.IUnpair kinfo_value _ => kinfo_value
  | With_family.ICons_some kinfo_value _ => kinfo_value
  | With_family.ICons_none kinfo_value _ => kinfo_value
  | With_family.IIf_none kinfo_value _ _ _ =>
    kinfo_value
  | With_family.IOpt_map kinfo_value _ _ =>
    kinfo_value
  | With_family.ICons_left kinfo_value _ => kinfo_value
  | With_family.ICons_right kinfo_value _ => kinfo_value
  | With_family.IIf_left kinfo_value _ _ _ =>
    kinfo_value
  | With_family.ICons_list kinfo_value _ => kinfo_value
  | With_family.INil kinfo_value _ => kinfo_value
  | With_family.IIf_cons kinfo_value _ _ _ =>
    kinfo_value
  | With_family.IList_map kinfo_value _ _ => kinfo_value
  | With_family.IList_iter kinfo_value _ _ => kinfo_value
  | With_family.IList_size kinfo_value _ => kinfo_value
  | With_family.IEmpty_set kinfo_value _ _ => kinfo_value
  | With_family.ISet_iter kinfo_value _ _ => kinfo_value
  | With_family.ISet_mem kinfo_value _ => kinfo_value
  | With_family.ISet_update kinfo_value _ => kinfo_value
  | With_family.ISet_size kinfo_value _ => kinfo_value
  | With_family.IEmpty_map kinfo_value _ _ => kinfo_value
  | With_family.IMap_map kinfo_value _ _ => kinfo_value
  | With_family.IMap_iter kinfo_value _ _ => kinfo_value
  | With_family.IMap_mem kinfo_value _ => kinfo_value
  | With_family.IMap_get kinfo_value _ => kinfo_value
  | With_family.IMap_update kinfo_value _ => kinfo_value
  | With_family.IMap_get_and_update kinfo_value _ => kinfo_value
  | With_family.IMap_size kinfo_value _ => kinfo_value
  | With_family.IEmpty_big_map kinfo_value _ _ _ => kinfo_value
  | With_family.IBig_map_mem kinfo_value _ => kinfo_value
  | With_family.IBig_map_get kinfo_value _ => kinfo_value
  | With_family.IBig_map_update kinfo_value _ => kinfo_value
  | With_family.IBig_map_get_and_update kinfo_value _ => kinfo_value
  | With_family.IConcat_string kinfo_value _ => kinfo_value
  | With_family.IConcat_string_pair kinfo_value _ => kinfo_value
  | With_family.ISlice_string kinfo_value _ => kinfo_value
  | With_family.IString_size kinfo_value _ => kinfo_value
  | With_family.IConcat_bytes kinfo_value _ => kinfo_value
  | With_family.IConcat_bytes_pair kinfo_value _ => kinfo_value
  | With_family.ISlice_bytes kinfo_value _ => kinfo_value
  | With_family.IBytes_size kinfo_value _ => kinfo_value
  | With_family.IAdd_seconds_to_timestamp kinfo_value _ => kinfo_value
  | With_family.IAdd_timestamp_to_seconds kinfo_value _ => kinfo_value
  | With_family.ISub_timestamp_seconds kinfo_value _ => kinfo_value
  | With_family.IDiff_timestamps kinfo_value _ => kinfo_value
  | With_family.IAdd_tez kinfo_value _ => kinfo_value
  | With_family.ISub_tez kinfo_value _ => kinfo_value
  | With_family.ISub_tez_legacy kinfo_value _ => kinfo_value
  | With_family.IMul_teznat kinfo_value _ => kinfo_value
  | With_family.IMul_nattez kinfo_value _ => kinfo_value
  | With_family.IEdiv_teznat kinfo_value _ => kinfo_value
  | With_family.IEdiv_tez kinfo_value _ => kinfo_value
  | With_family.IOr kinfo_value _ => kinfo_value
  | With_family.IAnd kinfo_value _ => kinfo_value
  | With_family.IXor kinfo_value _ => kinfo_value
  | With_family.INot kinfo_value _ => kinfo_value
  | With_family.IIs_nat kinfo_value _ => kinfo_value
  | With_family.INeg kinfo_value _ => kinfo_value
  | With_family.IAbs_int kinfo_value _ => kinfo_value
  | With_family.IInt_nat kinfo_value _ => kinfo_value
  | With_family.IAdd_int kinfo_value _ => kinfo_value
  | With_family.IAdd_nat kinfo_value _ => kinfo_value
  | With_family.ISub_int kinfo_value _ => kinfo_value
  | With_family.IMul_int kinfo_value _ => kinfo_value
  | With_family.IMul_nat kinfo_value _ => kinfo_value
  | With_family.IEdiv_int kinfo_value _ => kinfo_value
  | With_family.IEdiv_nat kinfo_value _ => kinfo_value
  | With_family.ILsl_nat kinfo_value _ => kinfo_value
  | With_family.ILsr_nat kinfo_value _ => kinfo_value
  | With_family.IOr_nat kinfo_value _ => kinfo_value
  | With_family.IAnd_nat kinfo_value _ => kinfo_value
  | With_family.IAnd_int_nat kinfo_value _ => kinfo_value
  | With_family.IXor_nat kinfo_value _ => kinfo_value
  | With_family.INot_int kinfo_value _ => kinfo_value
  | With_family.IIf kinfo_value _ _ _ => kinfo_value
  | With_family.ILoop kinfo_value _ _ => kinfo_value
  | With_family.ILoop_left kinfo_value _ _ => kinfo_value
  | With_family.IDip kinfo_value _ _ => kinfo_value
  | With_family.IExec kinfo_value _ => kinfo_value
  | With_family.IApply kinfo_value _ _ => kinfo_value
  | With_family.ILambda kinfo_value _ _ => kinfo_value
  | With_family.IFailwith kinfo_value _ _ => kinfo_value
  | With_family.ICompare kinfo_value _ _ => kinfo_value
  | With_family.IEq kinfo_value _ => kinfo_value
  | With_family.INeq kinfo_value _ => kinfo_value
  | With_family.ILt kinfo_value _ => kinfo_value
  | With_family.IGt kinfo_value _ => kinfo_value
  | With_family.ILe kinfo_value _ => kinfo_value
  | With_family.IGe kinfo_value _ => kinfo_value
  | With_family.IAddress kinfo_value _ => kinfo_value
  | With_family.IContract kinfo_value _ _ _ => kinfo_value
  | With_family.ITransfer_tokens kinfo_value _ => kinfo_value
  | With_family.IView kinfo_value _ _ => kinfo_value
  | With_family.IImplicit_account kinfo_value _ => kinfo_value
  | With_family.ICreate_contract kinfo_value _ _ _ =>
    kinfo_value
  | With_family.ISet_delegate kinfo_value _ => kinfo_value
  | With_family.INow kinfo_value _ => kinfo_value
  | With_family.IMin_block_time kinfo_value _ => kinfo_value
  | With_family.IBalance kinfo_value _ => kinfo_value
  | With_family.ILevel kinfo_value _ => kinfo_value
  | With_family.ICheck_signature kinfo_value _ => kinfo_value
  | With_family.IHash_key kinfo_value _ => kinfo_value
  | With_family.IPack kinfo_value _ _ => kinfo_value
  | With_family.IUnpack kinfo_value _ _ => kinfo_value
  | With_family.IBlake2b kinfo_value _ => kinfo_value
  | With_family.ISha256 kinfo_value _ => kinfo_value
  | With_family.ISha512 kinfo_value _ => kinfo_value
  | With_family.ISource kinfo_value _ => kinfo_value
  | With_family.ISender kinfo_value _ => kinfo_value
  | With_family.ISelf kinfo_value _ _ _ => kinfo_value
  | With_family.ISelf_address kinfo_value _ => kinfo_value
  | With_family.IAmount kinfo_value _ => kinfo_value
  | With_family.ISapling_empty_state kinfo_value _ _ => kinfo_value
  | With_family.ISapling_verify_update kinfo_value _ => kinfo_value
  | With_family.ISapling_verify_update_deprecated kinfo_value _ => kinfo_value
  | With_family.IDig kinfo_value _ _ _ => kinfo_value
  | With_family.IDug kinfo_value _ _ _ => kinfo_value
  | With_family.IDipn kinfo_value _ _ _ _ => kinfo_value
  | With_family.IDropn kinfo_value _ _ _ => kinfo_value
  | With_family.IChainId kinfo_value _ => kinfo_value
  | With_family.INever kinfo_value => kinfo_value
  | With_family.IVoting_power kinfo_value _ => kinfo_value
  | With_family.ITotal_voting_power kinfo_value _ => kinfo_value
  | With_family.IKeccak kinfo_value _ => kinfo_value
  | With_family.ISha3 kinfo_value _ => kinfo_value
  | With_family.IAdd_bls12_381_g1 kinfo_value _ => kinfo_value
  | With_family.IAdd_bls12_381_g2 kinfo_value _ => kinfo_value
  | With_family.IAdd_bls12_381_fr kinfo_value _ => kinfo_value
  | With_family.IMul_bls12_381_g1 kinfo_value _ => kinfo_value
  | With_family.IMul_bls12_381_g2 kinfo_value _ => kinfo_value
  | With_family.IMul_bls12_381_fr kinfo_value _ => kinfo_value
  | With_family.IMul_bls12_381_z_fr kinfo_value _ => kinfo_value
  | With_family.IMul_bls12_381_fr_z kinfo_value _ => kinfo_value
  | With_family.IInt_bls12_381_fr kinfo_value _ => kinfo_value
  | With_family.INeg_bls12_381_g1 kinfo_value _ => kinfo_value
  | With_family.INeg_bls12_381_g2 kinfo_value _ => kinfo_value
  | With_family.INeg_bls12_381_fr kinfo_value _ => kinfo_value
  | With_family.IPairing_check_bls12_381 kinfo_value _ => kinfo_value
  | With_family.IComb kinfo_value _ _ _ => kinfo_value
  | With_family.IUncomb kinfo_value _ _ _ => kinfo_value
  | With_family.IComb_get kinfo_value _ _ _ => kinfo_value
  | With_family.IComb_set kinfo_value _ _ _ => kinfo_value
  | With_family.IDup_n kinfo_value _ _ _ => kinfo_value
  | With_family.ITicket kinfo_value _ => kinfo_value
  | With_family.IRead_ticket kinfo_value _ => kinfo_value
  | With_family.ISplit_ticket kinfo_value _ => kinfo_value
  | With_family.IJoin_tickets kinfo_value _ _ => kinfo_value
  | With_family.IHalt kinfo_value => kinfo_value
  | With_family.IOpen_chest kinfo_value _ => kinfo_value
  end.

Definition dep_ty_size {a} (t_value : With_family.ty a)
  : Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.t) :=
  (Script_typed_ir.ty_metadata_value (With_family.to_ty t_value))
    .(Script_typed_ir.ty_metadata.size).

Definition dep_comparable_ty_size {a} (t_value : With_family.ty a)
  : Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.t) :=
  (Script_typed_ir.comparable_ty_metadata
     (With_family.to_ty t_value)).(Script_typed_ir.ty_metadata.size).

(** Simulation of [is_comparable]. *)
Definition dep_is_comparable {a : Ty.t}
  (function_parameter : With_family.ty a) : Dependent_bool.dbool :=
  match function_parameter with
  | With_family.Never_t => Dependent_bool.Yes
  | With_family.Unit_t => Dependent_bool.Yes
  | With_family.Int_t => Dependent_bool.Yes
  | With_family.Nat_t => Dependent_bool.Yes
  | With_family.Signature_t => Dependent_bool.Yes
  | With_family.String_t => Dependent_bool.Yes
  | With_family.Bytes_t => Dependent_bool.Yes
  | With_family.Mutez_t => Dependent_bool.Yes
  | With_family.Bool_t => Dependent_bool.Yes
  | With_family.Key_hash_t => Dependent_bool.Yes
  | With_family.Key_t => Dependent_bool.Yes
  | With_family.Timestamp_t => Dependent_bool.Yes
  | With_family.Chain_id_t => Dependent_bool.Yes
  | With_family.Address_t => Dependent_bool.Yes
  | With_family.Tx_rollup_l2_address_t => Dependent_bool.Yes
  | With_family.Pair_t _ _ _ dand_value =>
    Dependent_bool.dbool_of_dand dand_value
  | With_family.Union_t _ _ _ dand_value =>
    Dependent_bool.dbool_of_dand dand_value
  | With_family.Option_t _ _ cmp => cmp
  | With_family.Lambda_t _ _ _ => Dependent_bool.No
  | With_family.List_t _ _ => Dependent_bool.No
  | With_family.Set_t _ _ => Dependent_bool.No
  | With_family.Map_t _ _ _ => Dependent_bool.No
  | With_family.Big_map_t _ _ _ => Dependent_bool.No
  | With_family.Ticket_t _ _ => Dependent_bool.No
  | With_family.Contract_t _ _ => Dependent_bool.No
  | With_family.Sapling_transaction_t _ => Dependent_bool.No
  | With_family.Sapling_transaction_deprecated_t _ => Dependent_bool.No
  | With_family.Sapling_state_t _ => Dependent_bool.No
  | With_family.Operation_t => Dependent_bool.No
  | With_family.Bls12_381_g1_t => Dependent_bool.No
  | With_family.Bls12_381_g2_t => Dependent_bool.No
  | With_family.Bls12_381_fr_t => Dependent_bool.No
  | With_family.Chest_t => Dependent_bool.No
  | With_family.Chest_key_t => Dependent_bool.No
  end.

(** Dependent version of [ty_ex_c]. *)
Inductive dep_ty_ex_c (a : Ty.t) : Set :=
| Dep_ty_ex_c : With_family.ty a -> dep_ty_ex_c a.
Arguments Dep_ty_ex_c {_}.

(** Conversion of [dep_ty_ex_c] back to [ty_ex_c]. *)
Definition to_ty_ex_c {a} (v : dep_ty_ex_c a) : Script_typed_ir.ty_ex_c :=
  match v with
  | Dep_ty_ex_c ty => Script_typed_ir.Ty_ex_c (With_family.to_ty ty)
  end.

(** Simulation of [unit_t]. *)
Definition dep_unit_t : With_family.ty _ := With_family.Unit_t.

(** Simulation of [unit_key]. *)
Definition dep_unit_key := dep_unit_t.

(** Simulation of [int_t]. *)
Definition dep_int_t : With_family.ty _ := With_family.Int_t.

(** Simulation of [int_key]. *)
Definition dep_int_key : With_family.ty _ := dep_int_t.

(** Simulation of [nat_t]. *)
Definition dep_nat_t : With_family.ty _ := With_family.Nat_t.

(** Simulation of [nat_key]. *)
Definition dep_nat_key : With_family.ty _ := dep_nat_t.

(** Simulation of [signature_t]. *)
Definition dep_signature_t : With_family.ty _ := With_family.Signature_t.

(** Simulation of [signature_key]. *)
Definition dep_signature_key : With_family.ty _ := dep_signature_t.

(** Simulation of [string_t]. *)
Definition dep_string_t : With_family.ty _ := With_family.String_t.

(** Simulation of [string_key]. *)
Definition dep_string_key : With_family.ty _ := dep_string_t.

(** Simulation of [bytes_t]. *)
Definition dep_bytes_t : With_family.ty _ := With_family.Bytes_t.

(** Simulation of [bytes_key]. *)
Definition dep_bytes_key : With_family.ty _ := dep_bytes_t.

(** Simulation of [mutes_t]. *)
Definition dep_mutez_t : With_family.ty _ := With_family.Mutez_t.

(** Simulation of [mutes_key]. *)
Definition dep_mutez_key : With_family.ty _ := dep_mutez_t.

(** Simulation of [key_hash_t]. *)
Definition dep_key_hash_t : With_family.ty _ := With_family.Key_hash_t.

(** Simulation of [key_hash_key]. *)
Definition dep_key_hash_key : With_family.ty _ := dep_key_hash_t.

(** Simulation of [key_t]. *)
Definition dep_key_t : With_family.ty _ := With_family.Key_t.

(** Simulation of [key_key]. *)
Definition dep_key_key : With_family.ty _ := dep_key_t.

(** Simulation of [timestamp_t]. *)
Definition dep_timestamp_t : With_family.ty _ := With_family.Timestamp_t.

(** Simulation of [timestamp_key]. *)
Definition dep_timestamp_key : With_family.ty _ := dep_timestamp_t.

(** Simulation of [address_t]. *)
Definition dep_address_t : With_family.ty _ := With_family.Address_t.

(** Simulation of [address_key]. *)
Definition dep_address_key : With_family.ty _ := dep_address_t.

(** Simulation of [bool_t]. *)
Definition dep_bool_t : With_family.ty _ := With_family.Bool_t.

(** Simulation of [bool_key]. *)
Definition dep_bool_key : With_family.ty _ := dep_bool_t.

(** Simulation of [tx_rollup_l2_address_t]. *)
Definition dep_tx_rollup_l2_address_t
  : With_family.ty _ := With_family.Tx_rollup_l2_address_t.

(** Simulation of [tx_rollup_l2_address_key]. *)
Definition dep_tx_rollup_l2_address_key
  : With_family.ty _ := dep_tx_rollup_l2_address_t.

(** Simulation of [pair_t]. *)
Definition dep_pair_t {tl tr}
  (loc_value : Alpha_context.Script.location)
  (l_value : With_family.ty tl) (r_value : With_family.ty tr)
  : M? (dep_ty_ex_c (Ty.Pair tl tr)) :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound2) loc_value
      (dep_ty_size l_value)
      (dep_ty_size r_value) in
  let 'Dependent_bool.Ex_dand cmp :=
    Dependent_bool.dand_value
      (dep_is_comparable l_value) (dep_is_comparable r_value) in
  return?
    (Dep_ty_ex_c (
      With_family.Pair_t l_value r_value
        {| Script_typed_ir.ty_metadata.size := size_value |} cmp)).

(** Simulation of [comparable_pair_t]. *)
Definition dep_comparable_pair_t {tl tr}
  (loc_value : Alpha_context.Script.location)
  (l_value : With_family.ty tl)
  (r_value : With_family.ty tr)
  : M? With_family.ty (Ty.Pair tl tr) :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound2)
      loc_value (dep_ty_size l_value) (dep_ty_size r_value) in
  return?
    (With_family.Pair_t
       l_value r_value {| Script_typed_ir.ty_metadata.size := size_value |}
       Dependent_bool.YesYes).

(** Simulation of [pair_key]. *)
Definition dep_pair_key {tl tr}
  (loc_value : Alpha_context.Script.location)
  (l_value : With_family.ty tl) (r_value : With_family.ty tr)
  : M? With_family.ty (Ty.Pair tl tr) :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound2)
      loc_value (dep_ty_size l_value) (dep_ty_size r_value) in
  return?
    (With_family.Pair_t
       l_value r_value {| Script_typed_ir.ty_metadata.size := size_value |}
       Dependent_bool.YesYes).

(** Simulation of [pair_3_key]. *)
Definition dep_pair_3_key {tl tm tr}
  (loc_value : Alpha_context.Script.location)
  (l_value : With_family.ty tl) (m_value : With_family.ty tm)
  (r_value : With_family.ty tr)
  : M? With_family.ty (Ty.Pair tl (Ty.Pair tm tr)) :=
  let? r_value := dep_pair_key loc_value m_value r_value in
  dep_pair_key loc_value l_value r_value.

(** Simulation of [union_t]. *)
Definition dep_union_t {tl tr}
  (loc_value : Alpha_context.Script.location) (l_value : With_family.ty tl) (r_value : With_family.ty tr)
  : M? dep_ty_ex_c (Ty.Union tl tr) :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound2)
      loc_value (dep_ty_size l_value) (dep_ty_size r_value) in
  let 'Dependent_bool.Ex_dand cmp :=
    Dependent_bool.dand_value
      (dep_is_comparable l_value)
      (dep_is_comparable r_value) in
  return?
    (Dep_ty_ex_c
       (With_family.Union_t
          l_value r_value
          {| Script_typed_ir.ty_metadata.size := size_value |} cmp)).

(** Simulation of [comparable_union_t]. *)
Definition dep_comparable_union_t {tl tr}
  (loc_value : Alpha_context.Script.location)
  (l_value : With_family.ty tl)
  (r_value : With_family.ty tr)
  : M? With_family.ty (Ty.Union tl tr) :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound2)
      loc_value (dep_ty_size l_value) (dep_ty_size r_value) in
  return?
    (With_family.Union_t
       l_value r_value {| Script_typed_ir.ty_metadata.size := size_value |}
       Dependent_bool.YesYes).

(** Simulation of [union_bytes_bool_t]. *)
Definition dep_union_bytes_bool_t
  : With_family.ty (Ty.Union Ty.Bytes Ty.Bool) :=
  With_family.Union_t
    With_family.Bytes_t
    With_family.Bool_t
    {| Script_typed_ir.ty_metadata.size :=
        Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.three) |}
    Dependent_bool.YesYes.

(** Simulation of [union_key]. *)
Definition dep_union_key {tl tr}
  (loc_value : Alpha_context.Script.location)
  (l_value : With_family.ty tl)
  (r_value : With_family.ty tr)
  : M? With_family.ty (Ty.Union tl tr) :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound2)
      loc_value
      (Script_typed_ir.ty_size (With_family.to_ty l_value))
      (Script_typed_ir.ty_size (With_family.to_ty r_value)) in
  return?
    (With_family.Union_t
       l_value r_value
       {| Script_typed_ir.ty_metadata.size := size_value |}
      Dependent_bool.YesYes).

(** Simulation of [lambda_t]. *)
Definition dep_lambda_t {ta tb}
  (loc_value : Alpha_context.Script.location)
  (a : With_family.ty ta)
  (b : With_family.ty tb)
  : M? With_family.ty (Ty.Lambda ta tb)  :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound2)
    loc_value (dep_ty_size a) (dep_ty_size b) in
  return?
    (With_family.Lambda_t
       a b {| Script_typed_ir.ty_metadata.size := size_value |}).

(** Simulation of [option_t]. *)
Definition dep_option_t {a} (loc_value : Alpha_context.Script.location)
  (t_value : With_family.ty a) : M? (With_family.ty _) :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound1) loc_value
      (dep_ty_size t_value) in
  let cmp := dep_is_comparable t_value in
  return? (With_family.Option_t t_value
    {| Script_typed_ir.ty_metadata.size := size_value |} cmp).

(** Simulation of [option_mutez_t]. *)
Definition dep_option_mutez_t : With_family.ty _ :=
  With_family.Option_t
    dep_mutez_t
    {| Script_typed_ir.ty_metadata.size :=
        Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.two) |}
    Dependent_bool.Yes.

(** Simulation of [option_string_t]. *)
Definition dep_option_string_t : With_family.ty _ :=
  With_family.Option_t
    dep_string_t
    {| Script_typed_ir.ty_metadata.size :=
        Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.two) |}
    Dependent_bool.Yes.

(** Simulation of [option_bytes_t]. *)
Definition dep_option_bytes_t : With_family.ty _ :=
  With_family.Option_t
    dep_bytes_t
    {| Script_typed_ir.ty_metadata.size :=
        Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.two) |}
    Dependent_bool.Yes.

(** Simulation of [option_nat_t]. *)
Definition dep_option_nat_t : With_family.ty _ :=
  With_family.Option_t
    dep_nat_t
    {| Script_typed_ir.ty_metadata.size :=
        Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.two) |}
    Dependent_bool.Yes.

(** Simulation of [option_pair_nat_nat_t]. *)
Definition dep_option_pair_nat_nat_t : With_family.ty _ :=
  With_family.Option_t
    (With_family.Pair_t
       dep_nat_t dep_nat_t
       {| Script_typed_ir.ty_metadata.size :=
           Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.three) |}
      Dependent_bool.YesYes)
    {| Script_typed_ir.ty_metadata.size :=
        Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.four) |}
    Dependent_bool.Yes.

(** Simulation of [option_pair_nat_mutez_t]. *)
Definition dep_option_pair_nat_mutez_t : With_family.ty _ :=
  With_family.Option_t
    (With_family.Pair_t
       dep_nat_t
       dep_mutez_t
       {| Script_typed_ir.ty_metadata.size :=
           Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.three) |}
      Dependent_bool.YesYes)
    {| Script_typed_ir.ty_metadata.size :=
        Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.four) |}
    Dependent_bool.Yes.

(** Simulation of [option_pair_mutez_mutez_t]. *)
Definition dep_option_pair_mutez_mutez_t : With_family.ty _ :=
  With_family.Option_t
    (With_family.Pair_t
       dep_mutez_t
       dep_mutez_t
       {| Script_typed_ir.ty_metadata.size :=
           Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.three) |}
      Dependent_bool.YesYes)
    {| Script_typed_ir.ty_metadata.size :=
        Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.four) |}
    Dependent_bool.Yes.

(** Simulation of [option_pair_int_nat_t]. *)
Definition dep_option_pair_int_nat_t : With_family.ty _ :=
  With_family.Option_t
    (With_family.Pair_t
       dep_int_t
       dep_nat_t
       {| Script_typed_ir.ty_metadata.size :=
           Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.three) |}
      Dependent_bool.YesYes)
    {| Script_typed_ir.ty_metadata.size :=
        Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.four) |}
    Dependent_bool.Yes.

(** Simulation of [option_key]. *)
Definition dep_option_key {a}
  (loc_value : Alpha_context.Script.location)
  (t_value : With_family.ty a)
  : M? With_family.ty _ :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound1)
      loc_value (dep_ty_size t_value)
  in
  return?
    (With_family.Option_t
       t_value
       {| Script_typed_ir.ty_metadata.size := size_value |} Dependent_bool.Yes).

(** Simulation of [option_key]. *)
Definition dep_list_t {a}
  (loc_value : Alpha_context.Script.location)
  (t_value : With_family.ty a)
  : M? With_family.ty _ :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound1)
    loc_value (dep_ty_size t_value) in
  return?
    (With_family.List_t
       t_value {| Script_typed_ir.ty_metadata.size := size_value |}).

(** Simulation of [operation_t]. *)
Definition dep_operation_t : With_family.ty _ := With_family.Operation_t.

(** Simulation of [list_operation_t]. *)
Definition dep_list_operation_t : With_family.ty _ :=
  With_family.List_t dep_operation_t
    {| Script_typed_ir.ty_metadata.size :=
      Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.two)
    |}.

(** Simulation of [set_t]. *)
Definition dep_set_t {a}
  (loc_value : Alpha_context.Script.location) (t_value : With_family.ty a)
  : M? With_family.ty _ :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound1)
      loc_value (dep_comparable_ty_size t_value) in
  return?
    (With_family.Set_t
       t_value {| Script_typed_ir.ty_metadata.size := size_value |}).

(** Simulation of [map_t]. *)
Definition dep_map_t {a b}
  (loc_value : Alpha_context.Script.location)
  (l_value : With_family.ty a)
  (r_value : With_family.ty b) : M? With_family.ty _ :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound2)
      loc_value (dep_comparable_ty_size l_value) (dep_ty_size r_value) in
  return?
    (With_family.Map_t
       l_value r_value {| Script_typed_ir.ty_metadata.size := size_value |}).

(** Simulation of [big_map_t]. *)
Definition dep_big_map_t {a b}
  (loc_value : Alpha_context.Script.location)
  (l_value : With_family.ty a)
  (r_value : With_family.ty b) : M? With_family.ty _ :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound2)
      loc_value
      (dep_comparable_ty_size l_value)
      (dep_ty_size r_value) in
  return?
    (With_family.Big_map_t
       l_value r_value {| Script_typed_ir.ty_metadata.size := size_value |}).

(** Simulation of [contract_t]. *)
Definition dep_contract_t {a}
  (loc_value : Alpha_context.Script.location)
  (t_value : With_family.ty a)
  : M? With_family.ty _ :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound1)
    loc_value (dep_ty_size t_value) in
  return?
    (With_family.Contract_t
       t_value
       {| Script_typed_ir.ty_metadata.size := size_value |}).

(** Simulation of [contract_unit_t]. *)
Definition dep_contract_unit_t : With_family.ty _ :=
  With_family.Contract_t
    dep_unit_t
    {| Script_typed_ir.ty_metadata.size :=
        Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.two) |}.

(** Simulation of [sapling_transaction_t]. *)
Definition dep_sapling_transaction_t
  (memo_size : Alpha_context.Sapling.Memo_size.t)
  : With_family.ty _ := With_family.Sapling_transaction_t memo_size.

(** Simulation of [dep_sapling_transaction_depricated_t]. *)
Definition dep_sapling_transaction_deprecated_t
  (memo_size : Alpha_context.Sapling.Memo_size.t)
  : With_family.ty _ :=
  With_family.Sapling_transaction_deprecated_t memo_size.

(** Simulation of [sapling_state_t]. *)
Definition dep_sapling_state_t (memo_size : Alpha_context.Sapling.Memo_size.t)
  : With_family.ty _ := With_family.Sapling_state_t memo_size.

(** Simulation of [chain_id_t]. *)
Definition dep_chain_id_t : With_family.ty _ := With_family.Chain_id_t.

(** Simulation of [chain_id_key]. *)
Definition dep_chain_id_key : With_family.ty _ := dep_chain_id_t.

(** Simulation of [never_t]. *)
Definition dep_never_t : With_family.ty _ := With_family.Never_t.

(** Simulation of [never_key]. *)
Definition dep_never_key : With_family.ty _ := dep_never_t.

(** Simulation of [bls12_381_g1_t]. *)
Definition dep_bls12_381_g1_t : With_family.ty _ := With_family.Bls12_381_g1_t.

(** Simulation of [bls12_381_g2_t]. *)
Definition dep_bls12_381_g2_t : With_family.ty _ := With_family.Bls12_381_g2_t.

(** Simulation of [bls12_381_fr_t]. *)
Definition dep_bls12_381_fr_t : With_family.ty _ := With_family.Bls12_381_fr_t.

(** Simulation of [ticket_t]. *)
Definition dep_ticket_t {a}
  (loc_value : Alpha_context.Script.location)
  (t_value : With_family.ty a)
  : M? With_family.ty _ :=
  let? size_value :=
    Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.compound1)
      loc_value (dep_comparable_ty_size t_value) in
  return? (With_family.Ticket_t t_value {| Script_typed_ir.ty_metadata.size := size_value |}).

(** Simulation of [chest_key_t]. *)
Definition dep_chest_key_t : With_family.ty _ := With_family.Chest_key_t.

(** Simulation of [chest_t]. *)
Definition dep_chest_t : With_family.ty _ := With_family.Chest_t.

Module Script_list.
  Import Script_typed_ir.boxed_list.
  (** Apply [List.map f bl.elements] by boxing and unboxing the
       boxed_list *)
  Definition map {a b : Set} (f : a -> b) (bl : record a)
    : record b :=
  let '{| elements := elements; |} := bl in
  let l := List.map f elements in
     {| elements := l; length := List.length l |}.
End Script_list.
