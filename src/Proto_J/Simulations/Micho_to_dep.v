Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.

Require TezosOfOCaml.Environment.V5.Proofs.List.
Require TezosOfOCaml.Environment.V5.Proofs.String.
Require Import TezosOfOCaml.Michocoq.semantics.
Require TezosOfOCaml.Michocoq.syntax.
Require TezosOfOCaml.Michocoq.tez.
Require Import TezosOfOCaml.Proto_J.Simulations.Script_family.
Require TezosOfOCaml.Proto_J.Simulations.Script_interpreter.
Require Import TezosOfOCaml.Proto_J.Simulations.Script_typed_ir.

Definition Ty_of_micho_simple_comparable_ty
  (t : syntax_type.simple_comparable_type) : Ty.t :=
  match t with
  | syntax_type.never =>
      Ty.Never
  | syntax_type.string =>
      Ty.String
  | syntax_type.nat =>
      Ty.Num Ty.Num.Nat
  | syntax_type.int =>
      Ty.Num Ty.Num.Int
  | syntax_type.bytes =>
      Ty.Bytes
  | syntax_type.bool =>
      Ty.Bool
  | syntax_type.mutez =>
      Ty.Mutez
  | syntax_type.address =>
      Ty.Address
  | syntax_type.key_hash =>
      Ty.Key_hash
  | syntax_type.timestamp =>
      Ty.Timestamp
  | syntax_type.key =>
      Ty.Key
  | syntax_type.unit =>
      Ty.Unit
  | syntax_type.signature =>
      Ty.Signature
  | syntax_type.chain_id =>
      Ty.Chain_id
  end.

Fixpoint Ty_of_micho_comparable_ty (t : syntax_type.comparable_type) :
  Ty.t :=
  match t with
  | syntax_type.Comparable_type_simple a =>
      Ty_of_micho_simple_comparable_ty a
  | syntax_type.Cpair a b =>
      Ty.Pair
        (Ty_of_micho_comparable_ty a)
        (Ty_of_micho_comparable_ty b)
  | syntax_type.Coption a =>
      Ty.Option
        (Ty_of_micho_comparable_ty a)
  | syntax_type.Cor a b =>
      Ty.Union
        (Ty_of_micho_comparable_ty a)
        (Ty_of_micho_comparable_ty b)
  end.

Fixpoint Ty_of_micho_ty(t : syntax_type.type) : Ty.t :=
  match t with
  | syntax_type.Comparable_type c =>
      Ty_of_micho_simple_comparable_ty c
  | syntax_type.option a =>
      Ty.Option
        (Ty_of_micho_ty a)
  | syntax_type.list a =>
      Ty.List
        (Ty_of_micho_ty a)
  | syntax_type.set a =>
      Ty.Set_
        (Ty_of_micho_comparable_ty a)
  | syntax_type.contract a =>
      Ty.Contract
        (Ty_of_micho_ty a)
  | syntax_type.operation =>
      Ty.Operation
  | syntax_type.pair a b =>
      Ty.Pair
        (Ty_of_micho_ty a)
        (Ty_of_micho_ty b)
  | syntax_type.or a b =>
      Ty.Union
        (Ty_of_micho_ty a)
        (Ty_of_micho_ty b)
  | syntax_type.lambda a b =>
      Ty.Lambda
        (Ty_of_micho_ty a)
        (Ty_of_micho_ty b)
  | syntax_type.map a b =>
      Ty.Map
        (Ty_of_micho_comparable_ty a)
        (Ty_of_micho_ty b)
  | syntax_type.big_map a b =>
      Ty.Big_map
        (Ty_of_micho_comparable_ty a)
        (Ty_of_micho_ty b)
  | syntax_type.ticket a =>
      Ty.Ticket
        (Ty_of_micho_comparable_ty a)
  | syntax_type.sapling_state _ =>
      Ty.Sapling_state
  | syntax_type.sapling_transaction _ =>
      Ty.Sapling_transaction
  | syntax_type.bls12_381_fr =>
      Ty.Bls12_381_fr
  | syntax_type.bls12_381_g1 =>
      Ty.Bls12_381_g1
  | syntax_type.bls12_381_g2 =>
      Ty.Bls12_381_g2
  end.

Fixpoint seq_instr {s f g}
  (i : With_family.kinstr s f) : With_family.kinstr f g ->
  With_family.kinstr s g :=
  match i with
  | With_family.IDrop k i' => fun j =>
      With_family.IDrop k (seq_instr i' j)
  | With_family.IDup k i' => fun j =>
      With_family.IDup k (seq_instr i' j)
  | With_family.ISwap k i' => fun j =>
      With_family.ISwap k (seq_instr i' j)
  | With_family.IConst k x i' => fun j =>
      With_family.IConst k x (seq_instr i' j)
  | With_family.ICons_pair k i' => fun j =>
      With_family.ICons_pair k (seq_instr i' j)
  | With_family.ICar k i' => fun j =>
      With_family.ICar k (seq_instr i' j)
  | With_family.ICdr k i' => fun j =>
      With_family.ICdr k (seq_instr i' j)
  | With_family.IUnpair k i' => fun j =>
      With_family.IUnpair k (seq_instr i' j)
  | With_family.ICons_some k i' => fun j =>
      With_family.ICons_some k (seq_instr i' j)
  | With_family.ICons_none k i' => fun j =>
      With_family.ICons_none k (seq_instr i' j)
  | With_family.IIf_none k i1 i2 i3 => fun j =>
      With_family.IIf_none k i1 i2 (seq_instr i3 j)
  | With_family.IOpt_map k i1 i2 => fun j =>
      With_family.IOpt_map k i1 (seq_instr i2 j)
  | With_family.ICons_left k i' => fun j =>
      With_family.ICons_left k (seq_instr i' j)
  | With_family.ICons_right k i' => fun j =>
      With_family.ICons_right k (seq_instr i' j)
  | With_family.IIf_left k i1 i2 i3 => fun j =>
      With_family.IIf_left k i1 i2 (seq_instr i3 j)
  | With_family.ICons_list k i' => fun j =>
      With_family.ICons_list k (seq_instr i' j)
  | With_family.INil k i' => fun j =>
      With_family.INil k (seq_instr i' j)
  | With_family.IIf_cons k i1 i2 i3 => fun j =>
      With_family.IIf_cons k i1 i2 (seq_instr i3 j)
  | With_family.IList_map k i1 i2 => fun j =>
      With_family.IList_map k i1 (seq_instr i2 j)
  | With_family.IList_iter k i1 i2 => fun j =>
      With_family.IList_iter k i1 (seq_instr i2 j)
  | With_family.IList_size k i' => fun j =>
      With_family.IList_size k (seq_instr i' j)
  | With_family.IEmpty_set k x i' => fun j =>
      With_family.IEmpty_set k x (seq_instr i' j)
  | With_family.ISet_iter k i1 i2 => fun j =>
      With_family.ISet_iter k i1 (seq_instr i2 j)
  | With_family.ISet_mem k i' => fun j =>
      With_family.ISet_mem k (seq_instr i' j)
  | With_family.ISet_update k i' => fun j =>
      With_family.ISet_update k (seq_instr i' j)
  | With_family.ISet_size k i' => fun j =>
      With_family.ISet_size k (seq_instr i' j)
  | With_family.IEmpty_map k x i' => fun j =>
      With_family.IEmpty_map k x (seq_instr i' j)
  | With_family.IMap_map k i1 i2 => fun j =>
      With_family.IMap_map k i1 (seq_instr i2 j)
  | With_family.IMap_iter k i1 i2 => fun j =>
      With_family.IMap_iter k i1 (seq_instr i2 j)
  | With_family.IMap_mem k i' => fun j =>
      With_family.IMap_mem k (seq_instr i' j)
  | With_family.IMap_get k i' => fun j =>
      With_family.IMap_get k (seq_instr i' j)
  | With_family.IMap_update k i' => fun j =>
      With_family.IMap_update k (seq_instr i' j)
  | With_family.IMap_get_and_update k i' => fun j =>
      With_family.IMap_get_and_update k (seq_instr i' j)
  | With_family.IMap_size k i' => fun j =>
      With_family.IMap_size k (seq_instr i' j)
  | With_family.IEmpty_big_map k x y i' => fun j =>
      With_family.IEmpty_big_map k x y (seq_instr i' j)
  | With_family.IBig_map_mem k i' => fun j =>
      With_family.IBig_map_mem k (seq_instr i' j)
  | With_family.IBig_map_get k i' => fun j =>
      With_family.IBig_map_get k (seq_instr i' j)
  | With_family.IBig_map_update k i' => fun j =>
      With_family.IBig_map_update k (seq_instr i' j)
  | With_family.IBig_map_get_and_update k i' => fun j =>
      With_family.IBig_map_get_and_update k (seq_instr i' j)
  | With_family.IConcat_string k i' => fun j =>
      With_family.IConcat_string k (seq_instr i' j)
  | With_family.IConcat_string_pair k i' => fun j =>
      With_family.IConcat_string_pair k (seq_instr i' j)
  | With_family.ISlice_string k i' => fun j =>
      With_family.ISlice_string k (seq_instr i' j)
  | With_family.IString_size k i' => fun j =>
      With_family.IString_size k (seq_instr i' j)
  | With_family.IConcat_bytes k i' => fun j =>
      With_family.IConcat_bytes k (seq_instr i' j)
  | With_family.IConcat_bytes_pair k i' => fun j =>
      With_family.IConcat_bytes_pair k (seq_instr i' j)
  | With_family.ISlice_bytes k i' => fun j =>
      With_family.ISlice_bytes k (seq_instr i' j)
  | With_family.IBytes_size k i' => fun j =>
      With_family.IBytes_size k (seq_instr i' j)
  | With_family.IAdd_seconds_to_timestamp k i' => fun j =>
      With_family.IAdd_seconds_to_timestamp k (seq_instr i' j)
  | With_family.IAdd_timestamp_to_seconds k i' => fun j =>
      With_family.IAdd_timestamp_to_seconds k (seq_instr i' j)
  | With_family.ISub_timestamp_seconds k i' => fun j =>
      With_family.ISub_timestamp_seconds k (seq_instr i' j)
  | With_family.IDiff_timestamps k i' => fun j =>
      With_family.IDiff_timestamps k (seq_instr i' j)
  | With_family.IAdd_tez k i' => fun j =>
      With_family.IAdd_tez k (seq_instr i' j)
  | With_family.ISub_tez k i' => fun j =>
      With_family.ISub_tez k (seq_instr i' j)
  | With_family.ISub_tez_legacy k i' => fun j =>
      With_family.ISub_tez_legacy k (seq_instr i' j)
  | With_family.IMul_teznat k i' => fun j =>
      With_family.IMul_teznat k (seq_instr i' j)
  | With_family.IMul_nattez k i' => fun j =>
      With_family.IMul_nattez k (seq_instr i' j)
  | With_family.IEdiv_teznat k i' => fun j =>
      With_family.IEdiv_teznat k (seq_instr i' j)
  | With_family.IEdiv_tez k i' => fun j =>
      With_family.IEdiv_tez k (seq_instr i' j)
  | With_family.IOr k i' => fun j =>
      With_family.IOr k (seq_instr i' j)
  | With_family.IAnd k i' => fun j =>
      With_family.IAnd k (seq_instr i' j)
  | With_family.IXor k i' => fun j =>
      With_family.IXor k (seq_instr i' j)
  | With_family.INot k i' => fun j =>
      With_family.INot k (seq_instr i' j)
  | With_family.IIs_nat k i' => fun j =>
      With_family.IIs_nat k (seq_instr i' j)
  | With_family.INeg k i' => fun j =>
      With_family.INeg k (seq_instr i' j)
  | With_family.IAbs_int k i' => fun j =>
      With_family.IAbs_int k (seq_instr i' j)
  | With_family.IInt_nat k i' => fun j =>
      With_family.IInt_nat k (seq_instr i' j)
  | With_family.IAdd_int k i' => fun j =>
      With_family.IAdd_int k (seq_instr i' j)
  | With_family.IAdd_nat k i' => fun j =>
      With_family.IAdd_nat k (seq_instr i' j)
  | With_family.ISub_int k i' => fun j =>
      With_family.ISub_int k (seq_instr i' j)
  | With_family.IMul_int k i' => fun j =>
      With_family.IMul_int k (seq_instr i' j)
  | With_family.IMul_nat k i' => fun j =>
      With_family.IMul_nat k (seq_instr i' j)
  | With_family.IEdiv_int k i' => fun j =>
      With_family.IEdiv_int k (seq_instr i' j)
  | With_family.IEdiv_nat k i' => fun j =>
      With_family.IEdiv_nat k (seq_instr i' j)
  | With_family.ILsl_nat k i' => fun j =>
      With_family.ILsl_nat k (seq_instr i' j)
  | With_family.ILsr_nat k i' => fun j =>
      With_family.ILsr_nat k (seq_instr i' j)
  | With_family.IOr_nat k i' => fun j =>
      With_family.IOr_nat k (seq_instr i' j)
  | With_family.IAnd_nat k i' => fun j =>
      With_family.IAnd_nat k (seq_instr i' j)
  | With_family.IAnd_int_nat k i' => fun j =>
      With_family.IAnd_int_nat k (seq_instr i' j)
  | With_family.IXor_nat k i' => fun j =>
      With_family.IXor_nat k (seq_instr i' j)
  | With_family.INot_int k i' => fun j =>
      With_family.INot_int k (seq_instr i' j)
  | With_family.IIf k i1 i2 i3 => fun j =>
      With_family.IIf k i1 i2 (seq_instr i3 j)
  | With_family.ILoop k i1 i2 => fun j =>
      With_family.ILoop k i1 (seq_instr i2 j)
  | With_family.ILoop_left k i1 i2 => fun j =>
      With_family.ILoop_left k i1 (seq_instr i2 j)
  | With_family.IDip k i1 i2 => fun j =>
      With_family.IDip k i1 (seq_instr i2 j)
  | With_family.IExec k i' => fun j =>
      With_family.IExec k (seq_instr i' j)
  | With_family.IApply k x i' => fun j =>
      With_family.IApply k x (seq_instr i' j)
  | With_family.ILambda k x i' => fun j =>
      With_family.ILambda k x (seq_instr i' j)
  | With_family.IFailwith k x y => fun j =>
      With_family.IFailwith k x y
  | With_family.ICompare k x i' => fun j =>
      With_family.ICompare k x (seq_instr i' j)
  | With_family.IEq k i' => fun j =>
      With_family.IEq k (seq_instr i' j)
  | With_family.INeq k i' => fun j =>
      With_family.INeq k (seq_instr i' j)
  | With_family.ILt k i' => fun j =>
      With_family.ILt k (seq_instr i' j)
  | With_family.IGt k i' => fun j =>
      With_family.IGt k (seq_instr i' j)
  | With_family.ILe k i' => fun j =>
      With_family.ILe k (seq_instr i' j)
  | With_family.IGe k i' => fun j =>
      With_family.IGe k (seq_instr i' j)
  | With_family.IAddress k i' => fun j =>
      With_family.IAddress k (seq_instr i' j)
  | With_family.IContract k x y i' => fun j =>
      With_family.IContract k x y (seq_instr i' j)
  | With_family.IView k x i' => fun j =>
      With_family.IView k x (seq_instr i' j)
  | With_family.ITransfer_tokens k i' => fun j =>
      With_family.ITransfer_tokens k (seq_instr i' j)
  | With_family.IImplicit_account k i' => fun j =>
      With_family.IImplicit_account k (seq_instr i' j)
  | With_family.ICreate_contract k x y i' => fun j =>
      With_family.ICreate_contract k x y (seq_instr i' j)
  | With_family.ISet_delegate k i' => fun j =>
      With_family.ISet_delegate k (seq_instr i' j)
  | With_family.INow k i' => fun j =>
      With_family.INow k (seq_instr i' j)
  | With_family.IMin_block_time k i' => fun j =>
      With_family.IMin_block_time k (seq_instr i' j)
  | With_family.IBalance k i' => fun j =>
      With_family.IBalance k (seq_instr i' j)
  | With_family.ILevel k i' => fun j =>
      With_family.ILevel k (seq_instr i' j)
  | With_family.ICheck_signature k i' => fun j =>
      With_family.ICheck_signature k (seq_instr i' j)
  | With_family.IHash_key k i' => fun j =>
      With_family.IHash_key k (seq_instr i' j)
  | With_family.IPack k x i' => fun j =>
      With_family.IPack k x (seq_instr i' j)
  | With_family.IUnpack k x i' => fun j =>
      With_family.IUnpack k x (seq_instr i' j)
  | With_family.IBlake2b k i' => fun j =>
      With_family.IBlake2b k (seq_instr i' j)
  | With_family.ISha256 k i' => fun j =>
      With_family.ISha256 k (seq_instr i' j)
  | With_family.ISha512 k i' => fun j =>
      With_family.ISha512 k (seq_instr i' j)
  | With_family.ISource k i' => fun j =>
      With_family.ISource k (seq_instr i' j)
  | With_family.ISender k i' => fun j =>
      With_family.ISender k (seq_instr i' j)
  | With_family.ISelf k x y i' => fun j =>
      With_family.ISelf k x y (seq_instr i' j)
  | With_family.ISelf_address k i' => fun j =>
      With_family.ISelf_address k (seq_instr i' j)
  | With_family.IAmount k i' => fun j =>
      With_family.IAmount k (seq_instr i' j)
  | With_family.ISapling_empty_state k x i' => fun j =>
      With_family.ISapling_empty_state k x (seq_instr i' j)
  | With_family.ISapling_verify_update k i' => fun j =>
      With_family.ISapling_verify_update k (seq_instr i' j)
  | With_family.ISapling_verify_update_deprecated k i' => fun j =>
      With_family.ISapling_verify_update_deprecated k (seq_instr i' j)
  | With_family.IDig k x y i' => fun j =>
      With_family.IDig k x y (seq_instr i' j)
  | With_family.IDug k x y i' => fun j =>
      With_family.IDug k x y (seq_instr i' j)
  | With_family.IDipn k x y i1 i2 => fun j =>
      With_family.IDipn k x y i1 (seq_instr i2 j)
  | With_family.IDropn k x y i' => fun j =>
      With_family.IDropn k x y (seq_instr i' j)
  | With_family.IChainId k i' => fun j =>
      With_family.IChainId k (seq_instr i' j)
  | With_family.INever k => fun j =>
      With_family.INever k
  | With_family.IVoting_power k i' => fun j =>
      With_family.IVoting_power k (seq_instr i' j)
  | With_family.ITotal_voting_power k i' => fun j =>
      With_family.ITotal_voting_power k (seq_instr i' j)
  | With_family.IKeccak k i' => fun j =>
      With_family.IKeccak k (seq_instr i' j)
  | With_family.ISha3 k i' => fun j =>
      With_family.ISha3 k (seq_instr i' j)
  | With_family.IAdd_bls12_381_g1 k i' => fun j =>
      With_family.IAdd_bls12_381_g1 k (seq_instr i' j)
  | With_family.IAdd_bls12_381_g2 k i' => fun j =>
      With_family.IAdd_bls12_381_g2 k (seq_instr i' j)
  | With_family.IAdd_bls12_381_fr k i' => fun j =>
      With_family.IAdd_bls12_381_fr k (seq_instr i' j)
  | With_family.IMul_bls12_381_g1 k i' => fun j =>
      With_family.IMul_bls12_381_g1 k (seq_instr i' j)
  | With_family.IMul_bls12_381_g2 k i' => fun j =>
      With_family.IMul_bls12_381_g2 k (seq_instr i' j)
  | With_family.IMul_bls12_381_fr k i' => fun j =>
      With_family.IMul_bls12_381_fr k (seq_instr i' j)
  | With_family.IMul_bls12_381_z_fr k i' => fun j =>
      With_family.IMul_bls12_381_z_fr k (seq_instr i' j)
  | With_family.IMul_bls12_381_fr_z k i' => fun j =>
      With_family.IMul_bls12_381_fr_z k (seq_instr i' j)
  | With_family.IInt_bls12_381_fr k i' => fun j =>
      With_family.IInt_bls12_381_fr k (seq_instr i' j)
  | With_family.INeg_bls12_381_g1 k i' => fun j =>
      With_family.INeg_bls12_381_g1 k (seq_instr i' j)
  | With_family.INeg_bls12_381_g2 k i' => fun j =>
      With_family.INeg_bls12_381_g2 k (seq_instr i' j)
  | With_family.INeg_bls12_381_fr k i' => fun j =>
      With_family.INeg_bls12_381_fr k (seq_instr i' j)
  | With_family.IPairing_check_bls12_381 k i' => fun j =>
      With_family.IPairing_check_bls12_381 k (seq_instr i' j)
  | With_family.IComb k x y i' => fun j =>
      With_family.IComb k x y (seq_instr i' j)
  | With_family.IUncomb k x y i' => fun j =>
      With_family.IUncomb k x y (seq_instr i' j)
  | With_family.IComb_get k x y i' => fun j =>
      With_family.IComb_get k x y (seq_instr i' j)
  | With_family.IComb_set k x y i' => fun j =>
      With_family.IComb_set k x y (seq_instr i' j)
  | With_family.IDup_n k x y i' => fun j =>
      With_family.IDup_n k x y (seq_instr i' j)
  | With_family.ITicket k i' => fun j =>
      With_family.ITicket k (seq_instr i' j)
  | With_family.IRead_ticket k i' => fun j =>
      With_family.IRead_ticket k (seq_instr i' j)
  | With_family.ISplit_ticket k i' => fun j =>
      With_family.ISplit_ticket k (seq_instr i' j)
  | With_family.IJoin_tickets k x i' => fun j =>
      With_family.IJoin_tickets k x (seq_instr i' j)
  | With_family.IOpen_chest k i' => fun j =>
      With_family.IOpen_chest k (seq_instr i' j)
  | With_family.IHalt k => fun j => j
  end.

(** removes pair matches in goals *)
Ltac remove_lets :=
  repeat match goal with
  | [ _ : context
      [ match ?term with
        | pair _ _ => _
        end
      ] |- _ ] => destruct term
  | [ _ : context
      [ match ?term with
        | Some _ => _
        | None => _
        end
      ] |- _ ] => destruct term
  end.

Lemma seq_instr_compose {s f r} : forall
  (i : With_family.kinstr s f)
  (j : With_family.kinstr f r)
  fuel1 fuel2 g gas gas' gas''
  s1 s2 s3 s4,
  Script_interpreter.dep_step fuel1 g gas i
    With_family.KNil s1 = Pervasives.Ok s2 ->
  Script_interpreter.dep_step fuel2 g gas' j
    With_family.KNil (fst (fst s2)) = Pervasives.Ok s3 ->
  Script_interpreter.dep_step (fuel1 + fuel2) g gas''
    (seq_instr i j) With_family.KNil s1 = Pervasives.Ok s4 ->
  s3 = s4.
  (* TODO *)
  (*induction i eqn:G;
  destruct fuel1; try discriminate; intros;
  try (
   simpl in H1;
    destruct g;
    destruct Script_interpreter_defs.dep_consume_instr;
      try discriminate;
    remove_lets;
    simpl in H;
    destruct Script_interpreter_defs.dep_consume_instr;
      try discriminate;
    eapply IHk; eauto).*)
Admitted.

Axiom kinfos : forall {s},
  With_family.kinfo s.

Axiom tys : forall {t},
  With_family.ty t.

Fixpoint translate_comb_get {n s t} (w : syntax.comb_get n s t) :
  With_family.comb_get_gadt_witness (Ty_of_micho_ty s) (Ty_of_micho_ty t) :=
  match w with
  | syntax.comb_get_0 _ =>
      With_family.Comb_get_zero
  | syntax.comb_get_1 _ _ =>
      With_family.Comb_get_one
  | syntax.comb_get_SSn n0 _ _ _ w0 =>
      With_family.Comb_get_plus_two
        (translate_comb_get w0)
  end.

Fixpoint translate_comb_set {n s t u} (w : syntax.comb_update n s t u) :
  With_family.comb_set_gadt_witness 
    (Ty_of_micho_ty t) (Ty_of_micho_ty s) (Ty_of_micho_ty u) :=
  match w with
  | syntax.comb_update_0 _ _ => 
    With_family.Comb_set_zero
  | syntax.comb_update_1 _ _ _ => 
    With_family.Comb_set_one
  | syntax.comb_update_SSn _ _ _ _ _ w' =>
    With_family.Comb_set_plus_two 
      (translate_comb_set w')
  end.

Fixpoint gen_uncomb_gadt_witness (t : syntax_type.type)
  (s s' : list (syntax_type.type)) {struct s}
  : With_family.uncomb_gadt_witness
      (List.map Ty_of_micho_ty (syntax.pairn t s :: s'))
      (List.map Ty_of_micho_ty (t :: s ++ s')) :=
  match s with
  | [] => With_family.Uncomb_one
  | t :: s'' =>
    With_family.Uncomb_succ (gen_uncomb_gadt_witness t s'' s')
  end.

Fixpoint gen_comb_gadt_witness (t : syntax_type.type)
  (s s' : list (syntax_type.type)) {struct s}
  : With_family.comb_gadt_witness
      (List.map Ty_of_micho_ty (t :: s ++ s'))
      (List.map Ty_of_micho_ty (syntax.pairn t s :: s')) :=
  match s with
  | [] => With_family.Comb_one
  | t :: s'' =>
    With_family.Comb_succ (gen_comb_gadt_witness t s'' s')
  end.

Definition gen_comparable_ty_witness_simple
  (s : syntax_type.simple_comparable_type) :
  With_family.ty
  (Ty_of_micho_simple_comparable_ty s) :=
  match s with
  | syntax_type.never =>
      With_family.Never_t
  | syntax_type.string =>
      With_family.String_t
  | syntax_type.nat => With_family.Nat_t
  | syntax_type.int => With_family.Int_t
  | syntax_type.bytes =>
      With_family.Bytes_t
  | syntax_type.bool =>
      With_family.Bool_t
  | syntax_type.mutez =>
      With_family.Mutez_t
  | syntax_type.address =>
      With_family.Address_t
  | syntax_type.key_hash =>
      With_family.Key_hash_t
  | syntax_type.timestamp =>
      With_family.Timestamp_t
  | syntax_type.key => With_family.Key_t
  | syntax_type.unit =>
      With_family.Unit_t
  | syntax_type.signature =>
      With_family.Signature_t
  | syntax_type.chain_id =>
      With_family.Chain_id_t
  end.

Axiom metadata :
  Script_typed_ir.ty_metadata.

Fixpoint gen_comparable_ty_witness (c : syntax_type.comparable_type) :
  With_family.ty
    (Ty_of_micho_comparable_ty c). 
  destruct c.
  - simpl. apply gen_comparable_ty_witness_simple.
  - simpl; constructor; auto.
    apply metadata. exact Dependent_bool.YesYes.
  - simpl; constructor; auto.
    apply metadata. exact Dependent_bool.Yes.
  - simpl; constructor; auto.
    apply metadata. exact Dependent_bool.YesYes.
Defined.

Fixpoint gen_comparable_ty_witness2 (c : syntax_type.comparable_type) :
  With_family.ty 
    (Ty_of_micho_ty (syntax_type.comparable_type_to_type c)).
  destruct c.
  - simpl. apply gen_comparable_ty_witness_simple.
  - constructor; auto. apply metadata.
    exact Dependent_bool.NoNo. (*TODO: *)
  - constructor; auto. apply metadata.
    exact Dependent_bool.No. (**)
  - constructor; auto. apply metadata.
    exact Dependent_bool.NoNo. (**)
Defined.

Fixpoint gen_dup_n_gadt_witness l t l0 : With_family.dup_n_gadt_witness
  (@List.map syntax_type.type Ty.t Ty_of_micho_ty  (l ++ t :: l0)%list) 
  (Ty_of_micho_ty t) := 
  match l with
  | [] => With_family.Dup_n_zero
  | x :: xs => With_family.Dup_n_succ (gen_dup_n_gadt_witness xs t l0)
  end.

Fixpoint gen_sppw_for_IDig t s l {struct l} :
  With_family.stack_prefix_preservation_witness
  (Ty_of_micho_ty t :: List.map Ty_of_micho_ty s)
  (List.map Ty_of_micho_ty s)
  (List.map Ty_of_micho_ty (l ++ t :: s))
  (List.map Ty_of_micho_ty (l ++ s)).
  destruct l.
  simpl.
  apply With_family.KRest.
  apply With_family.KPrefix.
  { apply (axiom "kinfo"). (* TODO *) }
  { apply gen_sppw_for_IDig. }
Defined.

Fixpoint gen_sppw_for_IDug t s l {struct l} :
  With_family.stack_prefix_preservation_witness
  (List.map Ty_of_micho_ty s)
  (List.map Ty_of_micho_ty (t :: s))
  (List.map Ty_of_micho_ty (l ++ s))
  (List.map Ty_of_micho_ty (l ++ t :: s)).
  destruct l.
  simpl.
  apply With_family.KRest.
  apply With_family.KPrefix.
  { apply (axiom "kinfo"). (* TODO *) }
  { apply gen_sppw_for_IDug. }
Defined.

Fixpoint gen_sppw_for_IDropn s s' {struct s} :
  With_family.stack_prefix_preservation_witness
  (List.map Ty_of_micho_ty s')
  (List.map Ty_of_micho_ty s')
  (List.map Ty_of_micho_ty (s ++ s'))
  (List.map Ty_of_micho_ty (s ++ s')).
  destruct s.
  simpl.
  apply With_family.KRest.
  apply With_family.KPrefix.
  { apply (axiom "kinfo"). (* TODO *) }
  { apply gen_sppw_for_IDropn. }
Defined.

Lemma Ty_of_micho_ty_comparable_type_to_type :
  forall c, Ty_of_micho_ty (syntax_type.comparable_type_to_type c)
   = Ty_of_micho_comparable_ty c.
Proof.
  induction c; simpl; congruence.
Defined.

Definition dep_of_micho_opcode {self_type s s'}
  (i : @syntax.opcode self_type s s')(f : list syntax_type.type) :
    With_family.kinstr (List.map Ty_of_micho_ty s') (List.map Ty_of_micho_ty f) ->
    With_family.kinstr (List.map Ty_of_micho_ty s) (List.map Ty_of_micho_ty f).
  refine ( (*:=*)
  match i with
  | syntax.APPLY => With_family.IApply kinfos tys
  | syntax.DUP _ => With_family.IDup kinfos
  | syntax.SWAP => With_family.ISwap kinfos
  | syntax.UNIT => With_family.IConst kinfos _
  | syntax.EQ => With_family.IEq kinfos
  | syntax.NEQ => With_family.INeq kinfos
  | syntax.LT => With_family.ILt kinfos
  | syntax.GT => With_family.IGt kinfos
  | syntax.LE => With_family.ILe kinfos
  | syntax.GE => With_family.IGe kinfos
  | @syntax.OR _ _ v _ =>
    match v with
    | syntax.Bitwise_variant _ bv =>
      match bv with
      | syntax.Bitwise_variant_bool => With_family.IOr kinfos
      | syntax.Bitwise_variant_nat => With_family.IOr_nat kinfos
      end
    end
  | @syntax.AND _ _ _ v _ =>
    match v with
    | syntax.Mk_and _ _ _ vr => 
      match vr with
      | syntax.And_variant_bool => With_family.IAnd kinfos
      | syntax.And_variant_nat => With_family.IAnd_nat kinfos
      | syntax.And_variant_int => With_family.IAnd_int_nat kinfos
      end
    end
  | @syntax.XOR _ _ v _ =>
    match v with
    | syntax.Bitwise_variant _ bv =>
      match bv with
      | syntax.Bitwise_variant_bool =>
          With_family.IXor kinfos
      | syntax.Bitwise_variant_nat =>
          With_family.IXor_nat kinfos
      end
    end
  | @syntax.NOT _ _ v _ =>
    match v with
    | syntax.Mk_not _ _ vr => 
      match vr with
      | syntax.Not_variant_bool => With_family.INot kinfos
      | syntax.Not_variant_nat => With_family.INot_int kinfos
      | syntax.Not_variant_int => With_family.INot_int kinfos
      end
    end
  | @syntax.NEG _ _ v _ =>
    match v with
    | syntax.Mk_neg _ _ vr => 
      match vr with
      | syntax.Neg_variant_nat => With_family.INeg kinfos
      | syntax.Neg_variant_int => With_family.INeg kinfos
      | syntax.Neg_variant_fr => With_family.INeg_bls12_381_fr kinfos
      | syntax.Neg_variant_g1 => With_family.INeg_bls12_381_g1 kinfos
      | syntax.Neg_variant_g2 => With_family.INeg_bls12_381_g2 kinfos
      end
    end
  | syntax.ABS => With_family.IAbs_int kinfos
  | syntax.ISNAT => With_family.IIs_nat kinfos
  | @syntax.INT _ _ v _ =>
    match v with
    | syntax.Mk_int _ vr =>
      match vr with
      | syntax.Int_variant_nat => With_family.IInt_nat kinfos
      | syntax.Int_variant_fr => With_family.IInt_bls12_381_fr kinfos
      end
    end
  | @syntax.ADD _ _ _ v _ =>
    match v with
    | syntax.Mk_add _ _ _ vr => 
      match vr with
      | syntax.Add_variant_nat_nat => With_family.IAdd_nat kinfos
      | syntax.Add_variant_nat_int => With_family.IAdd_int kinfos
      | syntax.Add_variant_int_nat => With_family.IAdd_int kinfos
      | syntax.Add_variant_int_int => With_family.IAdd_int kinfos
      | syntax.Add_variant_timestamp_int =>
          With_family.IAdd_timestamp_to_seconds kinfos
      | syntax.Add_variant_int_timestamp =>
          With_family.IAdd_seconds_to_timestamp kinfos
      | syntax.Add_variant_tez_tez => With_family.IAdd_tez kinfos
      | syntax.Add_variant_fr => With_family.IAdd_bls12_381_fr kinfos
      | syntax.Add_variant_g1 => With_family.IAdd_bls12_381_g1 kinfos
      | syntax.Add_variant_g2 => With_family.IAdd_bls12_381_g2 kinfos
      end
    end
  | @syntax.SUB _ _ _ v _ =>
    match v with
    | syntax.Mk_sub _ _ _ vr => 
      match vr with
      | syntax.Sub_variant_nat_nat => With_family.ISub_int kinfos
      | syntax.Sub_variant_nat_int => With_family.ISub_int kinfos
      | syntax.Sub_variant_int_nat => With_family.ISub_int kinfos
      | syntax.Sub_variant_int_int => With_family.ISub_int kinfos
      | syntax.Sub_variant_timestamp_int =>
          With_family.ISub_timestamp_seconds kinfos
      | syntax.Sub_variant_timestamp_timestamp =>
          With_family.IDiff_timestamps kinfos
      | syntax.Sub_variant_tez_tez => With_family.ISub_tez_legacy kinfos
      end
    end
  | @syntax.MUL _ _ _ v _ =>
    match v with
    | syntax.Mk_mul _ _ _ vr =>
      match vr with
      | syntax.Mul_variant_nat_nat => With_family.IMul_nat kinfos
      | syntax.Mul_variant_nat_int => With_family.IMul_int kinfos
      | syntax.Mul_variant_int_nat => With_family.IMul_int kinfos
      | syntax.Mul_variant_int_int => With_family.IMul_int kinfos
      | syntax.Mul_variant_tez_nat => With_family.IMul_teznat kinfos
      | syntax.Mul_variant_nat_tez => With_family.IMul_nattez kinfos
      | syntax.Mul_variant_fr_fr => With_family.IMul_bls12_381_fr kinfos
      | syntax.Mul_variant_g1_fr => With_family.IMul_bls12_381_g1 kinfos
      | syntax.Mul_variant_g2_fr => With_family.IMul_bls12_381_g2 kinfos
      | syntax.Mul_variant_fr_nat =>
          With_family.IMul_bls12_381_z_fr kinfos
      | syntax.Mul_variant_fr_int =>
          With_family.IMul_bls12_381_z_fr kinfos
      | syntax.Mul_variant_nat_fr =>
          With_family.IMul_bls12_381_fr_z kinfos
      | syntax.Mul_variant_int_fr =>
          With_family.IMul_bls12_381_fr_z kinfos
      end
    end
  | @syntax.EDIV _ _ _ v _ =>
    match v with
    | syntax.Mk_ediv _ _ _ _ vr => 
      match vr with
      | syntax.Ediv_variant_nat_nat => With_family.IEdiv_nat kinfos
      | syntax.Ediv_variant_nat_int => With_family.IEdiv_int kinfos
      | syntax.Ediv_variant_int_nat => With_family.IEdiv_int kinfos
      | syntax.Ediv_variant_int_int => With_family.IEdiv_int kinfos
      | syntax.Ediv_variant_tez_nat => With_family.IEdiv_teznat kinfos
      | syntax.Ediv_variant_tez_tez => With_family.IEdiv_tez kinfos
      end
    end
  | syntax.LSL => With_family.ILsl_nat kinfos
  | syntax.LSR => With_family.ILsr_nat kinfos
  | @syntax.COMPARE _ v _ => 
    With_family.ICompare kinfos (gen_comparable_ty_witness2 v)
  | @syntax.CONCAT _ _ v _ =>
    match v with
    | syntax.Mk_stringlike _ vr =>
      match vr with
      | syntax.Stringlike_variant_string =>
        With_family.IConcat_string_pair kinfos
      | syntax.Stringlike_variant_bytes =>
        With_family.IConcat_bytes_pair kinfos
      end
    end
  | @syntax.CONCAT_list _ _ v _ =>
    match v with
    | syntax.Mk_stringlike _ vr =>
      match vr with
      | syntax.Stringlike_variant_string =>
        With_family.IConcat_string kinfos
      | syntax.Stringlike_variant_bytes =>
          With_family.IConcat_bytes kinfos
      end
    end
  | @syntax.SIZE _ _ v _=>
    match v with
    | syntax.Mk_size _ vr =>
      match vr with
      | syntax.Size_variant_set _ => With_family.ISet_size kinfos
      | syntax.Size_variant_map key val => With_family.IMap_size kinfos
      | syntax.Size_variant_list _ => With_family.IList_size kinfos
      | syntax.Size_variant_string => With_family.IString_size kinfos
      | syntax.Size_variant_bytes => With_family.IBytes_size kinfos
      end
    end
  | @syntax.SLICE _ _ v _ =>
    match v with
    | syntax.Mk_stringlike _ vr =>
      match vr with
      | syntax.Stringlike_variant_string =>
        With_family.ISlice_string kinfos
      | syntax.Stringlike_variant_bytes =>
        With_family.ISlice_bytes kinfos
      end
    end
  | syntax.PAIR => With_family.ICons_pair kinfos
  | syntax.CAR => With_family.ICar kinfos
  | syntax.CDR => With_family.ICdr kinfos
  | syntax.UNPAIR => With_family.IUnpair kinfos

  | @syntax.EMPTY_SET _ v _  =>
    With_family.IEmpty_set kinfos
      (gen_comparable_ty_witness v)
  | @syntax.MEM _ _ _ v _  => _

  | @syntax.UPDATE _ _ _ _ v _ => _

  | @syntax.EMPTY_MAP _ k _ _ =>
    With_family.IEmpty_map kinfos (gen_comparable_ty_witness k)
  | @syntax.EMPTY_BIG_MAP _ k _ _ _ =>
    With_family.IEmpty_big_map kinfos (gen_comparable_ty_witness k) tys
  | @syntax.GET _ _ _ _ _ => _
  | @syntax.GET_AND_UPDATE _ _ _ _ _ => _
  | syntax.SOME => With_family.ICons_some kinfos
  | @syntax.NONE _ v _ => With_family.ICons_none kinfos
  | @syntax.LEFT _ _ _ _ => With_family.ICons_left kinfos
  | @syntax.RIGHT _ _ _ _ => With_family.ICons_right kinfos
  | @syntax.CONS _ _ _ => With_family.ICons_list kinfos
  | @syntax.NIL _ v _ => With_family.INil kinfos
  | @syntax.TRANSFER_TOKENS _ _ _ _ => With_family.ITransfer_tokens kinfos
  | syntax.SET_DELEGATE => With_family.ISet_delegate kinfos
  | @syntax.BALANCE _ v => With_family.IBalance kinfos
  | syntax.ADDRESS => With_family.IAddress kinfos
  | @syntax.CONTRACT _ _ v _ _ => _
  | @syntax.SOURCE _ _ => With_family.ISource kinfos
  | @syntax.SENDER _ _ => With_family.ISender kinfos
  | @syntax.AMOUNT _ _ => With_family.IAmount kinfos
  | syntax.IMPLICIT_ACCOUNT => With_family.IImplicit_account kinfos
  | @syntax.NOW _ _ => With_family.INow kinfos
  | @syntax.LEVEL _ _ => With_family.ILevel kinfos
  | syntax.PACK _ => With_family.IPack kinfos tys
  | syntax.UNPACK _ _ => With_family.IUnpack kinfos tys
  | syntax.HASH_KEY => With_family.IHash_key kinfos
  | syntax.BLAKE2B => With_family.IBlake2b kinfos
  | syntax.SHA256 => With_family.ISha256 kinfos
  | syntax.SHA512 => With_family.ISha512 kinfos
  | syntax.KECCAK => With_family.IKeccak kinfos
  | syntax.SHA3 => With_family.ISha3 kinfos
  | syntax.CHECK_SIGNATURE => With_family.ICheck_signature kinfos
  | @syntax.DIG _ _ _ _ _ _ => _
  | @syntax.DUG _ _ _ _ _ _ => _
  | @syntax.DROP _ _ _ _ _ => _
  | @syntax.DUPN _ _ _ _ _ _ _ => _
  | @syntax.CHAIN_ID _ _ => With_family.IChainId kinfos
  | @syntax.SELF_ADDRESS _ _ => With_family.ISelf_address kinfos
  | syntax.VOTING_POWER => With_family.IVoting_power kinfos
  | @syntax.TOTAL_VOTING_POWER _ _ => With_family.ITotal_voting_power kinfos
  | @syntax.PAIRN _ n t u s s' _ => 
    With_family.IComb kinfos 
      (Z.of_nat n) (gen_comb_gadt_witness t (u :: s) s')
  | @syntax.UNPAIRN _ n t u s s' _ =>
    With_family.IUncomb kinfos 
      (Z.of_nat n) (gen_uncomb_gadt_witness t (u :: s) s')
  | syntax.GETN n w =>
      With_family.IComb_get kinfos (Z.of_nat n) (translate_comb_get w)
  | syntax.UPDATEN n w => 
      With_family.IComb_set kinfos (Z.of_nat n) _
  | @syntax.TICKET _ _ _ => _
  | @syntax.READ_TICKET _ _ _ => _
  | syntax.SPLIT_TICKET => With_family.ISplit_ticket kinfos
  | @syntax.JOIN_TICKETS _ _ _ => _
  | @syntax.SAPLING_EMPTY_STATE _ _ _  => _
  | syntax.SAPLING_VERIFY_UPDATE => _
  | syntax.PAIRING_CHECK => With_family.IPairing_check_bls12_381 kinfos
  end).
  { exact tt. }
  {
    destruct v.
    destruct mem_variant_field.
    {
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.ISet_mem; apply kinfos.
    }
    {
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IMap_mem; apply kinfos.
    }
    {
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IBig_map_mem; apply kinfos.
    }
  }
  {
    destruct v.
    destruct update_variant_field.
    { 
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.ISet_update; apply kinfos.
    }
    { 
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IMap_update; apply kinfos.
    }
    { 
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IBig_map_update; apply kinfos.
    }
  }
  {
    destruct g.
    destruct get_variant_field.
    {
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IMap_get; apply kinfos.
    }
    {
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IBig_map_get; apply kinfos.
    }
  }
  {
    destruct g.
    destruct get_variant_field.
    {
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IMap_get_and_update; apply kinfos.
    }
    {
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IBig_map_get_and_update; apply kinfos.
    }
  }
  {
    apply With_family.IContract. apply kinfos.
    { apply tys. }
    {
      destruct v.
      - apply a.
      - exact "". (* TODO: Need to confirm if empty string this is valid here *)
    }
  }
  {
    (* @syntax.DIG *)
    eapply With_family.IDig.
    { apply kinfos. }
    { exact (Z.of_nat n). }
    { apply gen_sppw_for_IDig. }
  }
  {
    (* @syntax.DUG *)
    eapply With_family.IDug.
    { apply kinfos. }
    { exact (Z.of_nat n). }
    { apply gen_sppw_for_IDug. }
  }
  {
    (* @syntax.DROP *)
    apply With_family.IDropn.
    { apply kinfos. }
    { exact (Z.of_nat n). }
    { apply gen_sppw_for_IDropn. }
  }
  {
    apply With_family.IDup_n. apply kinfos.
    { apply (Z.of_nat n). }
    { apply gen_dup_n_gadt_witness. }
  }
  { (*exact (translate_comb_set w).*)
    (* ^ this should work *)
    (*TODO: reconcile these differences *)
    apply axiom.
  }
  {
    simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
    apply With_family.ITicket; apply kinfos.
  }
  { pose (@With_family.IRead_ticket
      (Ty_of_micho_comparable_ty c)
      (List.map Ty_of_micho_ty l)
      (List.map Ty_of_micho_ty f)
      ).
    simpl.
    intro.
    apply k.
    { apply kinfos. }
    { apply With_family.ICons_pair.
      apply kinfos.
      rewrite Ty_of_micho_ty_comparable_type_to_type
        in H.
      apply axiom.
    }
    (* @syntax.READ_TICKET *)
  }
  {
    apply With_family.IJoin_tickets. apply kinfos.
    rewrite <- Ty_of_micho_ty_comparable_type_to_type.
    apply gen_comparable_ty_witness2.
  }
  {
    apply With_family.ISapling_empty_state. apply kinfos.
    (* G: Alpha_context.Sapling.Memo_size.t *)
    (* @syntax.SAPLING_EMPTY_STATE *)
    apply axiom.
  }
  { apply axiom. }
Defined.

Fixpoint dep_of_micho_iseq {self_type tff s f}
  (i : @syntax.instruction_seq self_type tff s f) : 
  With_family.kinstr (List.map Ty_of_micho_ty s) (List.map Ty_of_micho_ty f) :=
  match i with
  | syntax.SEQ j iseq =>
    dep_of_micho_inst j _ (dep_of_micho_iseq iseq)
  | _ => axiom
  end

  with dep_of_micho_inst {self_type tff s s'}
    (i : @syntax.instruction self_type tff s s') : forall f,
    With_family.kinstr (List.map Ty_of_micho_ty s') (List.map Ty_of_micho_ty f) ->
    With_family.kinstr (List.map Ty_of_micho_ty s) (List.map Ty_of_micho_ty f) :=
    match i with
    | syntax.Instruction_seq seq =>
        fun f => seq_instr (dep_of_micho_iseq seq)
    | syntax.Instruction_opcode o =>
        dep_of_micho_opcode o
    | _ => axiom
    end.

Module Correctness(C : semantics.ContractContext).

  Module S := semantics.Semantics C.

  Axiom cheat : nat -> forall {X},X.
  (* nat arg to help determine where
     axiom is coming from *)

  Definition convert_simple_data_comparable
    (c : syntax_type.simple_comparable_type)
    : comparable.simple_comparable_data c ->
      With_family.ty_to_dep_Set (Ty_of_micho_simple_comparable_ty c).
    destruct c eqn:G; simpl; try (exact (fun x => x)).
    - exact (fun e => match e with end).
    - exact Script_string_repr.String_tag.
    - exact (fun n => Script_int_repr.Num_tag (Z.of_N n)).
    - exact Script_int_repr.Num_tag.
    - exact (fun x => Tez_repr.Tez_tag (Uint63.to_Z x)).
    - refine (fun add => {|
          Script_typed_ir.address.destination :=
            Alpha_context.Destination.Contract _;
          Script_typed_ir.address.entrypoint :=
            Alpha_context.Entrypoint.default
        |}).
      destruct add.
      + apply Contract_repr.Implicit.
        destruct k.
        exact (cheat 0).
      + exact (cheat 0).
    - exact (cheat 1). (*TODO*)
    - exact Script_timestamp_repr.Timestamp_tag.
    - exact (cheat 2). (*TODO*)
    - exact (cheat 3). (*TODO*)
    - exact (fun x =>
        match x with
        | comparable.Mk_chain_id str =>
          Script_typed_ir.Script_chain_id.Chain_id_tag str
        end).
  Defined.

  Fixpoint convert_data_comparable {c}
    : comparable.comparable_data c ->
      With_family.ty_to_dep_Set (Ty_of_micho_comparable_ty c) :=
    match c with
    | syntax_type.Comparable_type_simple a => convert_simple_data_comparable a
    | syntax_type.Cpair a b => fun s => 
      match s with
      | (c0, c1) => (convert_data_comparable c0, convert_data_comparable c1)
      end
    | syntax_type.Coption a => fun s => 
      match s with
      | Some v => Some (convert_data_comparable v)
      | None => None
      end
    | syntax_type.Cor a b => fun s => 
      match s with
      | inl c0 => Script_typed_ir.L (convert_data_comparable c0)
      | inr c1 => Script_typed_ir.R (convert_data_comparable c1)
      end
    end.
  
  Axiom key_hash_constant_to_public_key_hash :
    comparable.key_hash_constant -> Signature.public_key_hash.
  
  Axiom smart_contract_address_constant_to_contract_hash_t :
    comparable.smart_contract_address_constant -> Contract_hash.t.

  Definition address_constant_to_contract_t : 
    comparable.address_constant -> Alpha_context.Contract.t.
    intros.
    destruct H eqn:G.
    - apply Alpha_context.Contract.Implicit.
      apply key_hash_constant_to_public_key_hash.
      exact k.
    - apply Alpha_context.Contract.Originated.
      apply smart_contract_address_constant_to_contract_hash_t.
      exact s.
  Defined.

  (** postulated conversion functions for cryptographic stuff *)
  Parameter convert_bls_fr : bls12_381_fr_repr -> Script_typed_ir.Script_bls.Fr.t.
  Parameter convert_bls_g1 : bls12_381_g1_repr -> Script_typed_ir.Script_bls.G1.t.
  Parameter convert_bls_g2 : bls12_381_g2_repr -> Script_typed_ir.Script_bls.G2.t.

  Module Valid.
    Record t {self_type} {env : @S.proto_env self_type} : Prop := {
      (* bls_fr *)
      bls_fr_negate_commute : forall x,
        Script_typed_ir.Script_bls.Fr.negate (convert_bls_fr x) =
        convert_bls_fr (env.(S.mul_fr_fr) env.(S.fr_neg_unit) x);
      (* we may want to have a more 'self-evident' axiomatization, e.g.
         only relate negation to negation, mul to mul, etc. whereas
         this is currently relating negation to multiplication by -1
      *)
      bls_fr_mul : forall x y,
        Script_typed_ir.Script_bls.Fr.mul 
          (convert_bls_fr x) (convert_bls_fr y) = 
        convert_bls_fr (env.(S.mul_fr_fr) x y);
      bls_fr_mul_int : forall x y,
        (let
          'Script_typed_ir.Script_bls.Fr.Fr_tag x_value := 
          convert_bls_fr x in
          Script_typed_ir.Script_bls.Fr.Fr_tag
            (Bls12_381.Fr.(S.PRIME_FIELD.mul)
            (Bls12_381.Fr.(S.PRIME_FIELD.of_z) y) x_value)) =
            convert_bls_fr (env.(S.mul_fr_int) x y);
      bls_fr_add : forall x y,
        Script_typed_ir.Script_bls.Fr.add 
          (convert_bls_fr x) (convert_bls_fr y) = 
        convert_bls_fr (env.(S.add_bls12_381_fr) x y);
      bls_fr_to_z : forall x,
        Script_typed_ir.Script_bls.Fr.to_z (convert_bls_fr x) 
        = (env.(S.int_of_fr) x);
      (* bls_g1 *)
      bls_g1_negate_commute : forall x,
        Script_typed_ir.Script_bls.G1.negate (convert_bls_g1 x) =
        convert_bls_g1 (env.(S.mul_fr_g1) env.(S.fr_neg_unit) x);
      bls_g1_mul : forall x y,
        Script_typed_ir.Script_bls.G1.mul
          (convert_bls_g1 x) (convert_bls_fr y) =
        convert_bls_g1 (env.(S.mul_fr_g1) y x);
      bls_g1_add : forall x y,
        Script_typed_ir.Script_bls.G1.add
          (convert_bls_g1 x) (convert_bls_g1 y) =
        convert_bls_g1 (env.(S.add_bls12_381_g1) x y);
      (* bls_g2 *)
      bls_g2_negate_commute : forall x,
        Script_typed_ir.Script_bls.G2.negate (convert_bls_g2 x) =
        convert_bls_g2 (env.(S.mul_fr_g2) env.(S.fr_neg_unit) x);
      bls_g2_mul : forall x y,
        Script_typed_ir.Script_bls.G2.mul
          (convert_bls_g2 x) (convert_bls_fr y) =
        convert_bls_g2 (env.(S.mul_fr_g2) y x);
      bls_g2_add : forall x y,
        Script_typed_ir.Script_bls.G2.add
          (convert_bls_g2 x) (convert_bls_g2 y) =
        convert_bls_g2 (env.(S.add_bls12_381_g2) x y);
      }.
      Arguments t {_}.
  End Valid.

  Fixpoint convert_data (t : syntax_type.type) :
    S.data t -> With_family.ty_to_dep_Set (Ty_of_micho_ty t).
    unfold S.data.
    destruct t; simpl.
    - apply convert_simple_data_comparable.
    - simpl.
      apply Option.map.
      apply convert_data.
    - exact (fun xs =>
        let ys := List.map (convert_data t) xs in
        Script_typed_ir.boxed_list.Build
          _ ys (Z.of_nat (Lists.List.length ys))).
    - exact (cheat 5). (*TODO*)
    - apply (cheat 6). (*TODO*)
    - apply (cheat 7). (*TODO*)
    - exact (fun '(x,y) =>
        (convert_data t1 x,
         convert_data t2 y)).
    - exact (fun s =>
        match s with
        | inl x => Script_typed_ir.L (convert_data t1 x)
        | inr y => Script_typed_ir.R (convert_data t2 y)
        end).
    - apply (cheat 8). (*TODO*)
    - apply (cheat 9). (*TODO*)
    - apply (cheat 10). (*TODO*)
    - intro ticket.
      destruct ticket.
      constructor.
      + simpl in ticketer.
        apply address_constant_to_contract_t.
        exact ticketer.
      + apply (convert_data_comparable content).
      + constructor.
        unfold Alpha_context.Script_int.repr.
        unfold Z.t.
        apply Z.of_N.
        exact tamount.
    - apply (cheat 12). (*TODO*)
    - apply (cheat 13). (*TODO*)
    - exact convert_bls_fr.
    - exact convert_bls_g1.
    - exact convert_bls_g2.
  Defined.

  Fixpoint Stack_ty_of_stack {f} {struct f} : S.stack f ->
    With_family.stack_ty_to_dep_Set (List.map Ty_of_micho_ty f) :=
    match f return
      S.stack f -> With_family.stack_ty_to_dep_Set (List.map Ty_of_micho_ty f)
    with
    | [] => fun _ => (Script_typed_ir.EmptyCell, Script_typed_ir.EmptyCell)
    | t :: g => fun '(x, ys) =>
      (convert_data t x, Stack_ty_of_stack ys)
    end.

  Definition to_error {A E} (e : Pervasives.result A E) : error.M A :=
    match e with
    | Pervasives.Ok a => error.Return a
    | Pervasives.Error _ =>
        error.Failed _ error.Parsing_Out_of_Fuel (*TODO: fix*)
    end.

  Definition error_map {A B} (f : A -> B)
    (e : error.M A) : error.M B :=
    match e with
    | error.Return a => error.Return (f a)
    | error.Failed _ e => error.Failed _ e
    end.

  Definition local_gas_counter_of_nat (n : nat) :
    Local_gas_counter.local_gas_counter :=
    Local_gas_counter.Local_gas_counter (Z.of_nat n).

  (* varying gas/fuel does not alter a successful outcome *)
  Lemma dep_step_returns_one_result : forall s t stack g gas1 gas2 s1 s2 fuel1 fuel2
    (i : With_family.kinstr s t),
    Script_interpreter.dep_step fuel1 g gas1 i With_family.KNil stack = Pervasives.Ok s1 ->
    Script_interpreter.dep_step fuel2 g gas2 i With_family.KNil stack = Pervasives.Ok s2 ->
    s1 = s2.
  Admitted.

  (*TODO: find a better location for this*)
  Fixpoint stack_app {s t} :
    Stack_ty.to_Set s ->
    Stack_ty.to_Set t ->
    Stack_ty.to_Set (s ++ t)%list :=
    match s with
    | [] => fun _ st => st
    | x :: s' => fun '(x, st) st' => (x, stack_app st st')
    end.

  Lemma eval_gen_sppw_IDig :
    forall s t s'
      (stack : S.stack (s ++ t :: s')%list)
      (st1 : S.stack s)
      (st2 : S.stack (t :: s')),
      stack = S.stack_app st1 st2 ->
       Script_interpreter.eval_sppw_for_IDig
          (gen_sppw_for_IDig t s' s)
          (fun x => x)
          (Stack_ty_of_stack stack) = 
       (convert_data _ (fst st2),
          (Stack_ty_of_stack (S.stack_app st1 (snd st2)))).
    induction s; intros.
    { simpl.
      destruct stack.
      simpl in *.
      rewrite <- H; reflexivity.
    }
    { simpl.
      destruct stack.
      simpl in *.
      destruct st1.
      rewrite (IHs _ _ _ s1 st2).
      { destruct st2; simpl.
        inversion H; reflexivity.
      }
      { inversion H; auto. }
    }
  Qed.

  Lemma eval_gen_sppw_IDug :
    forall s t s'
      (stack : S.stack (s ++ s')%list)
      (st1 : S.stack s)
      (st2 : S.stack s')
      (x : S.data t),
      stack = S.stack_app st1 st2 ->
        Script_interpreter.eval_stack_prefix_preservation_witness
          (gen_sppw_for_IDug t s' s)
          (pair (convert_data t x))
          (Stack_ty_of_stack stack) =
        Stack_ty_of_stack
          (@S.stack_app _ (t :: s') st1 (x, st2)).
    induction s; intros.
    { simpl in *. now rewrite H. }
    { simpl in *.
      destruct stack.
      destruct st1.
      inversion H.
      f_equal.
      apply IHs; auto.
    }
  Qed.

  Lemma eval_gen_sppw_IDropn :
    forall s s' (st1 : S.stack s) (st2 : S.stack s'),
      Script_interpreter.eval_sspw_for_IDropn
        (gen_sppw_for_IDropn s s')
        (Stack_ty_of_stack (S.stack_app st1 st2))
      = Stack_ty_of_stack st2.
    induction s; intros.
    { reflexivity. }
    { destruct st1; simpl. apply IHs. }
  Qed.

  Lemma eval_gen_dup_n :
    forall s s' t (st1 : S.stack s) (st2 : S.stack (t :: s')),
       Script_interpreter.eval_dup_n_gadt_witness
          (gen_dup_n_gadt_witness s t s')
          (Stack_ty_of_stack (S.stack_app st1 st2))
       = convert_data t (fst st2).
    induction s; intros.
    { simpl.
      destruct st2; reflexivity.
    }
    { simpl.
      destruct st1.
      apply IHs.
    }
  Qed.

  Lemma eval_gen_comb : forall s s' t
    (st1 : S.stack (t :: s)) (st2 : S.stack s'),
    Script_interpreter.eval_comb_gadt_witness
      (gen_comb_gadt_witness t s s')
      (Stack_ty_of_stack (S.stack_app st1 st2))
    = (convert_data (syntax.pairn t s) (S.pairn (fst st1) (snd st1))
      , Stack_ty_of_stack st2).
    induction s; intros.
    { destruct st1 as [x []]; reflexivity. }
    { destruct st1; simpl.
      destruct s0.
      pose (IHs s' a (d0, s0)).
      simpl in e.
      rewrite e; reflexivity.
    }
  Qed.

  Lemma eval_gen_uncomb : forall s s' t
    (st2 : S.stack s') (x : S.data (syntax.pairn t s)),
    Script_interpreter.eval_uncomb_gadt_witness
      (gen_uncomb_gadt_witness t s s')
      (Stack_ty_of_stack ((x, st2) : S.stack (_ :: s')))
    = Stack_ty_of_stack ((fst (S.unpairn x), S.stack_app (snd (S.unpairn x)) st2) : S.stack (_ :: _ ++ _)).
    induction s; intros.
    { reflexivity.
    }
    { simpl.
      destruct x.
      f_equal.
      epose proof (IHs _ _ _ d0) as H.
      destruct (S.unpairn d0).
      simpl in *.
      apply H.
    }
  Qed.

  Lemma eval_translate_comb : forall n t1 t2
    (c : syntax.comb_get n t1 t2) d,
    Script_interpreter.eval_comb_get_gadt_witness
      (translate_comb_get c)
      (convert_data t1 d)
    = convert_data t2 (S.getn d c).
    induction c; intros.
    { reflexivity. }
    { destruct d; reflexivity. }
    { destruct d; simpl.
      apply IHc.
    }
  Qed.

  Lemma stack_app_split : forall s s' (stack : S.stack (s ++ s')%list) st st',
    S.stack_split stack = (st, st') -> S.stack_app st st' = stack.
    induction s; intros.
    { inversion H; reflexivity. }
    { inversion H.
      destruct stack.
      destruct (S.stack_split s0) eqn:G.
      destruct st.
      inversion H1.
      pose (IHs _ _ _ _ G).
      simpl; f_equal; congruence.
    }
  Qed.

  Definition step_constants_of_env {self_type}
    (env : @S.proto_env self_type) : Script_typed_ir.step_constants. refine ({|
    Script_typed_ir.step_constants.source :=
      match env.(S.source)(*fixme*) with
      | comparable.Implicit k =>
        Contract_repr.Implicit
          match k with
          | comparable.Mk_key_hash _ => cheat 0
          end
     | comparable.Originated _ => cheat 0
     end;
    Script_typed_ir.step_constants.payer :=
      match env.(S.sender) with
      | comparable.Implicit k =>
        Contract_repr.Implicit
          match k with
          | comparable.Mk_key_hash _ => cheat 0
          end
      | comparable.Originated _ => cheat 0
      end;
    Script_typed_ir.step_constants.self := _;
    Script_typed_ir.step_constants.amount :=
      Tez_repr.Tez_tag (Uint63.to_Z env.(S.amount));
    Script_typed_ir.step_constants.balance :=
      Alpha_context.Tez.Tez_tag (Uint63.to_Z (env.(S.balance)));
    Script_typed_ir.step_constants.chain_id :=
      match env.(S.chain_id_) with
      | comparable.Mk_chain_id s => s
      end;
    Script_typed_ir.step_constants.now :=
      Script_timestamp_repr.Timestamp_tag env.(S.now);
    Script_typed_ir.step_constants.level :=
      Alpha_context.Script_int.Num_tag
        (Z.of_N env.(S.level))
    |}).
    pose (env.(S.self)).
    destruct self_type.
    + destruct p.
      unfold base.Is_true in y.
      unfold Bool.Is_true in y.
      unfold error.isSome in y.
      simpl in y.
      apply axiom.
    + constructor.
      apply (cheat 55).
  Defined.

  Lemma dep_of_micho_opcode_correct : forall
    {self_type} {A B C}
    (fuel2 fuel1 : nat)
    (env : @S.proto_env self_type)
    (env_valid : Valid.t env)
    (o : @syntax.opcode self_type A B)
    (oc : Local_gas_counter.outdated_context)
    (gas1 gas2 : Local_gas_counter.local_gas_counter)
    (i :
      With_family.kinstr (List.map Ty_of_micho_ty B) (List.map Ty_of_micho_ty C))
    (stack : S.stack A)
    s s1 s2,
      let g := (oc, step_constants_of_env env) in
    S.eval_opcode _ env o stack = error.Return s ->
      let s' := Stack_ty_of_stack s in
    Script_interpreter.dep_step
      fuel1 g gas1 i With_family.KNil s' = Pervasives.Ok s1 ->
    Script_interpreter.dep_step
      fuel2 g gas2 (dep_of_micho_opcode o C i) With_family.KNil
      (Stack_ty_of_stack stack) = Pervasives.Ok s2 -> s1 = s2.
    destruct fuel2; intros.
    { discriminate. }
    { destruct o.
      { (* APPLY *) 
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0.
        destruct d0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H0.
        (* TODO: figure out lambda stuff*)
        admit.
      }
      { (* DUP *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* SWAP *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl Stack_ty_of_stack in H0.
        inversion H.
        rewrite <- H3 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* UNIT *)
        simpl S.eval_opcode in H.
        simpl in H1;
        destruct g.
        simpl Stack_ty_of_stack in s'.
        destruct s.
        inversion H.
        assert ((tt, Stack_ty_of_stack stack) = s').
        {
          rewrite H3.
          rewrite H4.
          reflexivity.
        }
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        rewrite <- H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* EQ *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        assert (d =? 0 = Compare.Int.op_eq (
          Z.compare d Z.zero) 0).
        { destruct d; reflexivity. }
        rewrite H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* NEQ *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        assert (negb (d =? 0) = Compare.Int.op_ltgt (
          Z.compare d Z.zero) 0).
        { destruct d; reflexivity. }
        rewrite H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* LT *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        assert (d <? 0 = Compare.Int.op_lt (
          Z.compare d Z.zero) 0).
        { destruct d; reflexivity. }
        rewrite H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      {
        (* GT *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        assert (d >? 0 = Compare.Int.op_gt (
          Z.compare d Z.zero) 0).
        destruct d; reflexivity.
        rewrite H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* LE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        assert (d <=? 0 = Compare.Int.op_lteq (
          Z.compare d Z.zero) 0).
        { destruct d; reflexivity. }
        rewrite H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* GE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        assert (d >=? 0 = Compare.Int.op_gteq (
          Z.compare d Z.zero) 0).
        { destruct d; reflexivity. }
        rewrite H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* OR *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s3.
        inversion H.
        rewrite <- H3 in H0.
        destruct s0.
        destruct bvariant eqn:G;
        destruct g;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        { eapply dep_step_returns_one_result; eauto. }
        { 
          simpl Stack_ty_of_stack in H0.
          assert (forall a b,
            (Z.of_N (N.lor a b)) = (Z.logor (Z.of_N a) (Z.of_N b))).
            {
              intros.
              unfold Z.of_N.
              unfold N.lor.
              destruct a, b; auto.
            }
          rewrite H2 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* AND *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s3.
        inversion H.
        rewrite <- H3 in H0.
        destruct s0.
        destruct and_variant_field  eqn:G;
        destruct g;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        { eapply dep_step_returns_one_result; eauto. }
        { 
          simpl Stack_ty_of_stack in H0.
          assert (forall a b,
            Z.of_N (N.land a b) = Z.logand (Z.of_N a) (Z.of_N b)).
            {
              intros.
              unfold Z.of_N.
              unfold N.land.
              destruct a, b; auto.
            }
          rewrite H2 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
        { 
          simpl Stack_ty_of_stack in H0.
          assert (forall a b,
            Z.of_N (Z.to_N (Z.land a (Z.of_N b))) = Z.logand a (Z.of_N b)).
          {
            intros.
            destruct a, b; auto; simpl.
            { rewrite N2Z.id; auto. }
            { rewrite N2Z.id; auto. }
          }
          rewrite H2 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* XOR *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s3.
        inversion H.
        rewrite <- H3 in H0.
        destruct s0.
        destruct bvariant eqn:G;
        destruct g;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        { eapply dep_step_returns_one_result; eauto. }
        { 
          simpl Stack_ty_of_stack in H0.
          assert (forall a b,
            (Z.of_N (N.lxor a b)) = (Z.logxor (Z.of_N a) (Z.of_N b))).
            {
              intros.
              destruct a, b; auto.
            }
          rewrite H2 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* NOT *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0;
        destruct s0;
        destruct not_variant_field eqn:G;
        simpl in H1;
        destruct g;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0;
        destruct s;
        inversion H.
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H4 in H0.
          assert ((Z.lognot (Z.of_N d)) = d0).
          { pose (q := Z.of_N d).
            fold q in H3; fold q.
            destruct q.
            { exact H3. }
            { destruct p; exact H3. }
            { destruct p; exact H3. }
          }
          rewrite <- H2 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H4 in H0.
          assert ((Z.lognot d) = d0).
          {
            rewrite <- H3.
            destruct d; auto.
            destruct p; auto.
          }
          rewrite <- H2 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* NEG *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        destruct s0;
        destruct neg_variant_field eqn:G;
        simpl in H1;
        destruct g;
        simpl in H0;
        simpl Stack_ty_of_stack in H0;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        { eapply dep_step_returns_one_result; eauto. }
        { eapply dep_step_returns_one_result; eauto. }
        { rewrite (Valid.bls_fr_negate_commute env_valid) in H1.
          eapply dep_step_returns_one_result; eauto.
        }
        { rewrite (Valid.bls_g1_negate_commute env_valid) in H1.
          eapply dep_step_returns_one_result; eauto.
        }
        { rewrite (Valid.bls_g2_negate_commute env_valid) in H1.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* ABS *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s.
        inversion H.
        rewrite <- H3 in H0.
        rewrite <- H4 in H0.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl Stack_ty_of_stack in H0.
        assert ((Z.abs d) = (Z.of_N (Z.abs_N d))).
        { destruct d; auto. }
        rewrite <- H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* ISNAT *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g;
        destruct d;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0;
        eapply dep_step_returns_one_result; eauto.
      }
      { (* INT *)
        simpl S.eval_opcode in H;
        destruct stack;
        simpl dep_of_micho_opcode in H1;
        unfold s' in H0;
        unfold S.int_of in H;
        unfold S.int_of_v in H;
        destruct s0;
        destruct int_variant_field eqn:G;
        destruct g;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        { inversion H.
          rewrite <- H3 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
        { simpl Stack_ty_of_stack in H0.
          destruct s.
          inversion H.
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          rewrite (Valid.bls_fr_to_z env_valid) in H1.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* ADD *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0;
        destruct add_variant_field eqn:G;
        destruct s3;
        destruct s;
        simpl in H1;
        destruct g;
        inversion H;
        simpl Stack_ty_of_stack in H0;
        try rewrite <- H3 in H0;
        try rewrite <- H4 in H0;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        try eapply dep_step_returns_one_result; eauto.
        {
          assert (Z.of_N (d + d0) = Z.add (Z.of_N d) (Z.of_N d0)).
          { destruct d, d0; auto. }
          rewrite H2.
          eauto.
        }
        {
          assert ((Z.add d0 d) = (d + d0)%Z).
          { destruct d, d0; try lia. }
          rewrite <- H2.
          eauto.
        }
        {
          unfold error.bind in H3.
          destruct (S.add_tez d d0) eqn:Hdd0;
          try discriminate.
          destruct (Int64.add (Uint63.to_Z d) (Uint63.to_Z d0) <? Uint63.to_Z d) in H1;
          try discriminate.
          simpl in H1.
          unfold S.add_tez in Hdd0.
          unfold tez.of_Z in Hdd0.
          unfold tez.in_bound in Hdd0.
          unfold tez.to_Z in Hdd0.
          unfold Uint63.wB in Hdd0.
          destruct (((0 <=? Uint63.to_Z d + Uint63.to_Z d0) &&
            (Uint63.to_Z d + Uint63.to_Z d0 <? 2 ^ Z.of_nat Uint63.size))%bool) eqn:G0 in Hdd0;
          try discriminate.
          inversion Hdd0.
          inversion H3.
          inversion H4.
          assert (Uint63.to_Z d1 = Int64.add (Uint63.to_Z d) (Uint63.to_Z d0)).
          {
            rewrite <- H5.
            rewrite <- H4.
            rewrite Uint63.of_Z_spec.
            unfold Uint63.wB; simpl.
            unfold Uint63.size in G0.
            rewrite Bool.andb_true_iff in G0.
            destruct G0.
            rewrite Z.leb_le in H7.
            rewrite Z.ltb_lt in H8.
            unfold Int64.add.
            unfold Pervasives.normalize_int64.
            unfold Pervasives.two_pow_63.
            unfold Pervasives.two_pow_64.
            rewrite Z.mod_small; try lia.
          }
          rewrite -> H7.
          rewrite <- H6.
          eauto.
        }
        { rewrite (Valid.bls_fr_add env_valid) in H1.
          eauto.
        }
        { rewrite (Valid.bls_g1_add env_valid) in H1.
          eauto.
        }
        { rewrite (Valid.bls_g2_add env_valid) in H1.
          eauto.
        }
      }
      { (* SUB *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0;
        destruct sub_variant_field eqn:G;
        destruct s3;
        destruct s;
        destruct g;
        inversion H;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0;
        try rewrite <- H3 in H0;
        try rewrite <- H4 in H0;
        try eapply dep_step_returns_one_result; eauto.
        { unfold error.bind in H3.
          unfold Error_monad.op_gtgtquestion in *.
          destruct (tez.of_Z (tez.to_Z d - tez.to_Z d0)) eqn:Hdd0;
            try discriminate.
          destruct (Uint63.to_Z d0 <=? Uint63.to_Z d) in H1;
            try discriminate;
            simpl in H1.
          unfold tez.of_Z in Hdd0.
          unfold tez.in_bound in Hdd0.
          unfold tez.to_Z in Hdd0.
          unfold Uint63.wB in Hdd0.
          destruct (((0 <=? Uint63.to_Z d - Uint63.to_Z d0) &&
            (Uint63.to_Z d - Uint63.to_Z d0 <? 2 ^ Z.of_nat Uint63.size))%bool) eqn:G0 in Hdd0;
            try discriminate.
          inversion Hdd0.
          inversion H3.
          assert ((Uint63.to_Z d1) = (Int64.sub (Uint63.to_Z d) (Uint63.to_Z d0))).
          {
            rewrite <- H5.
            rewrite <- H4.
            rewrite Uint63.of_Z_spec.
            unfold Uint63.wB; simpl.
            unfold Uint63.size in G0.
            rewrite Bool.andb_true_iff in G0.
            destruct G0.
            rewrite Z.leb_le in H2.
            rewrite Z.ltb_lt in H7.
            unfold Int64.sub.
            unfold Pervasives.normalize_int64.
            unfold Pervasives.two_pow_63.
            unfold Pervasives.two_pow_64.
            rewrite Z.mod_small; try lia.
          }
          rewrite <- H6.
          rewrite -> H2.
          eauto.
        }
      }
      { (* MUL *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0;
        destruct s3;
        destruct mul_variant_field eqn:G;
        destruct s;
        destruct g;
        inversion H;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0.
        {
          assert ((Z.of_N d1) = Z.mul (Z.of_N d) (Z.of_N d0)).
          { destruct d, d0; lia. }
          rewrite H2 in H0.
          rewrite <- H4 in H0.
          try eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          try eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          try eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          try eapply dep_step_returns_one_result; eauto.
        }
        {
          (* TODO: With_family.IMul_teznat *)
          admit.
        }
        {
          (* TODO: With_family.IMul_nattez *)
          admit.
        }
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          rewrite (Valid.bls_fr_mul env_valid) in H1.
          eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          rewrite (Valid.bls_g1_mul env_valid) in H1.
          eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          rewrite (Valid.bls_g2_mul env_valid) in H1.
          eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          rewrite (Valid.bls_fr_mul_int env_valid) in H1.
          eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          rewrite (Valid.bls_fr_mul_int env_valid) in H1.
          eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          rewrite (Valid.bls_fr_mul_int env_valid) in H1.
          eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          rewrite (Valid.bls_fr_mul_int env_valid) in H1.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* EDIV *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0;
        destruct s0;
        destruct s3;
        destruct ediv_variant_field eqn:G;
        destruct s;
        destruct g;
        inversion H.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0.
        (* TODO: Handwrite ediv in Script_interpreter.v
                 so that we don't have these Option.catch's *)
        - admit.
        - admit.
        - admit.
        - admit.
        - admit.
        - admit.
      }
      { 
        (* LSL *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0;
        simpl in H1;
        destruct g;
        destruct s;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0.
        (* TODO: With_family.ILsl_nat *)
        admit.
      }
      { (* LSR *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0;
        simpl in H1;
        destruct g;
        destruct s;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0.
        (* TODO: With_family.ILsl_nat *)
        admit.
      }
      { (* COMPARE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        simpl in H1.
        destruct s0;
        destruct g;
        destruct s;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        (* TODO: With_family.ICompare *)
        admit.
      }
      { (* CONCAT *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct i0;
        destruct s0;
        destruct s;
        destruct g;
        destruct stringlike_variant_field;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        inversion H;
        rewrite <- H3 in H0;
        rewrite <- H4 in H0.
        { eapply dep_step_returns_one_result; eauto. }
        { eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* CONCAT_list *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H;
        rewrite <- H3 in H0;
        destruct i0;
        destruct stringlike_variant_field eqn:G;
        simpl in H1;
        destruct g;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        { 
          (* TODO: With_family.IConcat_string *)
          admit.
        }
        {
          (* TODO: With_family.IConcat_bytes *)
          admit.
        }
      }
      { (* SIZE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct i0;
        destruct size_variant_field eqn:G;
        simpl in H1;
        destruct g;
        destruct s;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0;
        simpl in H;
        inversion H.
        { 
          (* TODO: convert_data: sets *)
          admit.
        }
        { 
          (* TODO: convert_data: maps *)
          admit.
        }
        { unfold Script_int_repr.of_int, of_int in *.
          assert (H_length_map : forall {a b : Set} (f : a -> b) l,
            Lists.List.length (List.map f l) = Lists.List.length l
          ) by (intros ? ? ? l'; induction l'; scongruence).
          rewrite H_length_map in H1.
          rewrite <- H3 in H0.
          rewrite H4 in H1.
          rewrite Znat.nat_N_Z in H0.
          eapply dep_step_returns_one_result; eauto.
          admit.
        }
        {
          assert (
            Z.abs (Z.of_int (String.length d)) = (Z.of_N d0)).
          { rewrite <- H3.
            rewrite Z.abs_eq.
            { unfold Z.of_int.
              rewrite nat_N_Z.
              rewrite String.string_length_compat.
              reflexivity.
            }
            {
              unfold Z.of_int.
              apply String.string_length_non_neg.
            }
          }
          rewrite <- H2 in H0.
          rewrite <- H4 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
        { rewrite <- H4 in H0.
          assert (Z.of_N d0 = Z.abs (Z.of_int (Bytes.length d))).
          { rewrite <- H3.
            rewrite nat_N_Z.
            unfold Bytes.length.
            unfold Z.of_int.
            rewrite -> String.string_length_compat.
            unfold Z.abs.
            unfold BinInt.Z.abs.
            destruct (Z.of_nat (Strings.String.length d)) eqn:?;
            try reflexivity;
            rewrite <- Heqz;
            lia.
          }
          rewrite H2 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* SLICE *)
        simpl S.eval_opcode in H;
        destruct stack eqn:?;
        simpl dep_of_micho_opcode in H1;
        unfold s' in H0;
        destruct i0 eqn:?;
        destruct stringlike_variant_field eqn:G;
        unfold g in *;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
        try discriminate;
        destruct s0;
        destruct s0;
        inversion H;
        unfold Pervasives.op_andand in *;
        destruct N.leb eqn:G0.
        { simpl in H1.
          (* TODO: d1 should be small. Arith overflow. *)
          assert (Z.add (Z.of_N d) (Z.of_N d0) <=?
            Z.of_int (String.length d1) = true) by apply axiom.
          rewrite H2 in H1.
          destruct Z.ltb eqn:G1.
          { simpl in H1.
            rewrite <- H3 in H0.
            simpl in H0, H1.
            assert (
              (substring 
                (Z.to_nat (Z.to_int (Z.of_N d)))
                (Z.to_nat (Z.to_int (Z.of_N d0))) d1) =
              (substring 
                (N.to_nat d) 
                (N.to_nat d0) d1)).
              {
                assert (forall x, (Z.to_nat (Z.to_int (Z.of_N x))) = (N.to_nat x)) by  apply axiom.
                rewrite -> H4.
                rewrite -> H4.
                reflexivity.
              }
            rewrite <- H4 in H0.
            eapply dep_step_returns_one_result; eauto.
          }
          {
            (* TODO: empty substring? *)
            (* Empty is not allowed in .ml *)
            apply axiom.
          }
        }
        { (* TODO: d1 should be small. Arith overflow. *)
          assert (Z.add (Z.of_N d) (Z.of_N d0) <=?
            Z.of_int (String.length d1) = false) by apply axiom.
          simpl in H1. 
          rewrite H2 in H1.
          rewrite Bool.andb_false_r in H1.
          rewrite <- H3 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
        { simpl in H1.
          (* TODO: d1 should be small. Arith overflow. *)
          assert (Z.add (Z.of_N d) (Z.of_N d0) <=?
            Z.of_int (Bytes.length d1) = true) by apply axiom.
          rewrite H2 in H1.
          destruct Z.ltb eqn:G1.
          { simpl in H1.
            rewrite <- H3 in H0.
            simpl in H0, H1.
            assert (
              (substring 
                (Z.to_nat (Z.to_int (Z.of_N d)))
                (Z.to_nat (Z.to_int (Z.of_N d0))) d1) =
              (substring 
                (N.to_nat d) 
                (N.to_nat d0) d1)).
              {
                assert (forall x, (Z.to_nat (Z.to_int (Z.of_N x))) = (N.to_nat x)) by  apply axiom.
                rewrite -> H4.
                rewrite -> H4.
                reflexivity.
              }
            rewrite <- H4 in H0.
            eapply dep_step_returns_one_result; eauto.
          }
          {
            (* TODO: empty substring? *)
            (* Empty is not allowed in .ml *)
            apply axiom.
          }
        }
        { (* TODO: d1 should be small. Arith overflow. *)
          assert (Z.add (Z.of_N d) (Z.of_N d0) <=?
            Z.of_int (Bytes.length d1) = false) by apply axiom.
          simpl in H1. 
          rewrite H2 in H1.
          rewrite Bool.andb_false_r in H1.
          rewrite <- H3 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* PAIR *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl Stack_ty_of_stack in H0.
        destruct s.
        destruct d1.
        inversion H.
        rewrite <- H3 in H0.
        rewrite <- H4 in H0.
        rewrite <- H5 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* CAR *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct g.
        destruct s.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        destruct d.
        inversion H.
        rewrite <- H3 in H0.
        rewrite <- H4 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* CDR *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        destruct d.
        inversion H.
        rewrite <- H3 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* UNPAIR *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
          destruct d.
        inversion H.
        rewrite <- H3 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* EMPTY_SET *)
        simpl S.eval_opcode in H.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        inversion H.
        (* TODO: map implementation *)
        admit.
      }
      { (* MEM *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct i0.
        unfold g.
        destruct mem_variant_field eqn:G.
        destruct s0.
        unfold eq_rec_r in H1.
        unfold eq_rec in H1.
        unfold eq_rect in H1.
        unfold eq_sym in H1.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        (* TODO: implement set *)
        unfold Ty_of_micho_ty_comparable_type_to_type in H1.
        unfold syntax_type.comparable_type_ind in H1.
        simpl.
        destruct a; simpl in H1.
        - admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          destruct a2; destruct a1;
          (* TODO: remove axiom *)
          admit.
        - unfold f_equal in H1.
          unfold eq_trans in H1.
          (* TODO: remove axiom *)
          admit.
        - admit.
        - admit.
        - admit.
      }
      { (* UPDATE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct i0.
        destruct update_variant_field  eqn:G.
        unfold eq_rec_r in H1.
        unfold eq_rec in H1.
        unfold eq_rect in H1.
        unfold eq_sym in H1.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        unfold g.
        unfold Ty_of_micho_ty_comparable_type_to_type in H1.
        unfold syntax_type.comparable_type_ind in H1.
        (* TODO: implement set *)
        admit.
        admit.
        admit.
      }
      { (* EMPTY_MAP *)
        simpl S.eval_opcode in H.
        inversion H.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        (* TODO: implement map *)
        admit.
      }
      { (* EMPTY_BIG_MAP *)
        simpl S.eval_opcode in H.
        inversion H.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        (* TODO: implement map *)
        admit.
      }
      { (* GET *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct g.
        destruct i0.
        destruct s0.
        destruct get_variant_field eqn:G.
        unfold eq_rec_r in H1.
        unfold eq_rec in H1.
        unfold eq_rect in H1.
        unfold eq_sym in H1.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        unfold Ty_of_micho_ty_comparable_type_to_type in H1.
        unfold syntax_type.comparable_type_ind in H1.
        (* TODO: implement map *)
        admit.
        admit.
      }
      { (* GET_AND_UPDATE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct g.
        destruct i0.
        destruct s0.
        destruct get_variant_field eqn:G.
        unfold eq_rec_r in H1.
        unfold eq_rec in H1.
        unfold eq_rect in H1.
        unfold eq_sym in H1.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        unfold Ty_of_micho_ty_comparable_type_to_type in H1.
        unfold syntax_type.comparable_type_ind in H1.
        (* TODO: implement map *)
        admit.
        admit.
      }
      { (* SOME *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* NONE *)
        simpl S.eval_opcode in H.
        inversion H.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* LEFT *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* RIGHT *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* CONS *)
        simpl S.eval_opcode in H.
        destruct stack, s0.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        unfold Script_list.cons_value in H1.
        simpl in H1.
        (* TODO: stack cannot exceed 2^63 *)
        admit.
        (*
        rewrite length_cons in H0.
        unfold Pervasives.op_plus in H1.
        rewrite Z.add_comm in H0.
        eapply dep_step_returns_one_result; eauto; admit.
        *)
      }
      { (* NIL *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* TRANSFER_TOKENS *)
        simpl S.eval_opcode in H.
        simpl in H0,H1.
        admit. (*TODO: implement contract*) 
      }
      { (* SET_DELEGATE *)
        simpl in H0,H1.
        admit. (*TODO: implement keys*) 
      }
      {
       (* BALANCE *)
        simpl in H0,H1.
        admit. (*TODO: step constants *) 
      }
      { (* ADDRESS *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl Stack_ty_of_stack in *.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO: contract *) 
      }
      { (* CONTRACT *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl Stack_ty_of_stack in *.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO: contract*) 
      }
      { (* SOURCE *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        admit. (*TODO: step constants*) 
      }
      { (* SENDER *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO: step constants*) 
      }
      { (* AMOUNT *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* IMPLICIT_AMOUNT *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        admit. (*TODO: implement contracts *) 
      }
      { (* NOW *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* LEVEL *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* IMPLICIT_AMOUNT *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        unfold g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        unfold Error_monad.op_gtgtquestion in H1.
        destruct Local_gas_counter.use_gas_counter_in_context;
          try discriminate.
        destruct p.
        destruct p.
        eapply dep_step_returns_one_result; eauto.
        admit. (* figure out outdated_context *) 
      }
      { (* UNPACK *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        unfold Error_monad.op_gtgtquestion in H1.
        admit. (*TODO: implement unpack *) 
      }
      { (* HASH_KEY *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO: implement keys *) 
      }
      { (* BLAKE2B *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        unfold g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        (* TODO: blake2b *)
        admit.
      }
      { (* SHA256 *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        unfold g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO: sha256 *) 
      }
      { (* SHA512 *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        unfold g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO: sha512*) 
      }
      { (* KECCAK *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        unfold g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO: keccak*) 
      }
      { (* SHA3 *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        unfold g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO: sha3*)
      }
      { (* CHECK_SIGNATURE *)
        simpl S.eval_opcode in H.
        destruct stack.
        destruct s0.
        destruct s0.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        unfold g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO: keys*)
      }
      { (* DIG *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        unfold S.stack_dig in H0.
        destruct (S.stack_split stack) eqn:G.
        destruct s4.
        pose (stack_app_split _ _ _ _ _ G) as Happ.
        symmetry in Happ.
        pose proof eval_gen_sppw_IDig as H_eval_gen_sppw_IDig.
        simpl in H_eval_gen_sppw_IDig.
        erewrite H_eval_gen_sppw_IDig in H1.
        2:{ exact Happ. }
        eapply dep_step_returns_one_result; eauto.
      }
      { (* DUG *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl Script_interpreter.dep_step in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        unfold S.stack_dug in H0.
        destruct stack.
        destruct (S.stack_split s3) eqn:G.
        pose (stack_app_split _ _ _ _ _ G) as Happ.
        symmetry in Happ.
        pose proof (eval_gen_sppw_IDug _ _ _ s3 s4 s5 d Happ).
        (* TODO: repair the proof *)
        admit.
        (*fold @List.map in H1.
        simpl in H2.
        rewrite H2 in H1.
        eapply dep_step_returns_one_result; eauto.*)
      }
      { (* DROP *)
        simpl S.eval_opcode in H.
        destruct (S.stack_split stack) eqn:G.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl Script_interpreter.dep_step in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        pose (stack_app_split _ _ _ _ _ G) as Happ.
        rewrite <- Happ in H1.
        rewrite eval_gen_sppw_IDropn in H1.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* DUPN *)
        simpl S.eval_opcode in H.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        destruct (S.stack_split stack) eqn:G.
        destruct s3.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H0.
        pose (stack_app_split _ _ _ _ _ G) as Happ.
        rewrite <- Happ in *.
        (* TODO: repair the proof *)
        admit.
        (*rewrite eval_gen_dup_n in H1.
        eapply dep_step_returns_one_result; eauto.*)
      }
      { (* CHAIN_ID *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H0.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        destruct (env.(S.chain_id_)).
        eapply dep_step_returns_one_result; eauto.
      }
      { (* SELF_ADDRESS *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H0.
        simpl in H1.
        admit. (* TODO: contracts*)
      }
      { (* VOTING_POWER *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        unfold g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
        admit. (* TODO: With_family.IVoting_power *)
      }
      { (* TOTAL_VOTING_POWER *)
        simpl S.eval_opcode in H.
        admit. (* TODO: With_family.ITotal_voting_power *)
      }
      { (* PAIRN *)
        simpl S.eval_opcode in H.
        destruct stack.
        destruct s0.
        destruct (S.stack_split s0) eqn:G.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H0.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        pose (stack_app_split _ _ _ _ _ G) as Happ.
        rewrite <- Happ in *.
        pose (eval_gen_comb _ _ _ (d0, s3) s4).
        fold @List.map in H1.
        simpl in e0.
        (* TODO: repair proof *)
        admit.
        (*rewrite e0 in H1.
        eapply dep_step_returns_one_result; eauto.*)
      }
      { (* UNPAIRN *)
        simpl S.eval_opcode in H.
        destruct stack.
        destruct d.
        destruct (S.unpairn d0) eqn:G.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        pose (eval_gen_uncomb S1 S2 t2).
        simpl in e0.
        rewrite e0 in H1.
        simpl in H1.
        rewrite G in H1.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* GETN *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        rewrite eval_translate_comb in H1.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* UPDATEN *)
        simpl S.eval_opcode in H.
        destruct stack.
        destruct s0.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        (* TODO: depends on UPDATEN issue being fixed*)
        admit.
      }
      { (* TICKET *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0.
        unfold eq_rec_r in H1;
        unfold eq_rec in H1;
        unfold eq_rect in H1;
        unfold eq_sym in H1;
        unfold Ty_of_micho_ty_comparable_type_to_type in H1;
        unfold syntax_type.comparable_type_ind in H1;
        simpl in H1;
        destruct g;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        destruct a.
        - destruct s4;
          destruct s;
          simpl Stack_ty_of_stack in H0;
          inversion H;
          rewrite <- H3 in H0;
          rewrite <- H4 in H0;
          try eapply dep_step_returns_one_result; eauto;
          (* TODO: all cases uses cheat 11 *)
          admit.
        - unfold f_equal in H1.
          unfold eq_trans in H1.
          destruct a2; destruct a1;
          simpl in H1;
          inversion H;
          rewrite <- H3 in H0;
          simpl in H0;
          (* TODO: eliminate match fix *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: eliminate match fix *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: eliminate match fix *)
          admit.
      }
      { (* READ_TICKET *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        unfold eq_rect in H1.
        unfold Ty_of_micho_ty_comparable_type_to_type in H1.
        unfold syntax_type.comparable_type_ind in H1.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        destruct a.
        - admit. (* TODO: remove axiom *)
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: remove match fix *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: remove match fix *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: remove match fix *)
          admit.
      }
      { (* SPLIT_TICKET *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0.
        destruct d0.
        simpl Stack_ty_of_stack in H0.
        destruct s.
        (* TODO: remove axiom from H0 *)
        admit.
      }
      { (* JOIN_TICKETS *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct d.
        unfold eq_rect in H1.
        unfold Ty_of_micho_ty_comparable_type_to_type in H1.
        unfold syntax_type.comparable_type_ind in H1.
        destruct a.
        simpl Stack_ty_of_stack in H0.
        destruct s;
        simpl in H1;
        destruct g;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        - (* TODO: remove axiom from H0 *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: remove match fix *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: remove match fix *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: remove match fix *)
          admit.
      }
      { (* SAPLING_EMPTY_STATE *)
        simpl S.eval_opcode in H.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        unfold s' in H0.
        unfold Stack_ty_of_stack in H0.
        destruct s.
        (* TODO: remove match fix in H0 and axiom in H1 *)
        admit.
      }
      { (* SAPLING_VERIFY_UPDATE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        simpl Stack_ty_of_stack in H0.
        destruct g.
        destruct s.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        destruct s0.
        (* TODO: remove axiom in H0 and H1 *)
        admit.
      }
      { (* PAIRING_CHECK *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        (* TODO: remove axiom in H1 *)
        admit.
      }
    }
    Unshelve.
    all: apply axiom.
  Admitted.

  Lemma dep_of_micho_inst_correct : forall
    {self_type tff s f}
    (i : @syntax.instruction self_type tff s f)
    C (j : With_family.kinstr (List.map Ty_of_micho_ty _) (List.map Ty_of_micho_ty C))
    (env : S.proto_env)
    (env_valid : Valid.t env)
    (fuel1 fuel2 fuel3 : nat)
    (oc oc' : Local_gas_counter.outdated_context)
    (gas gas' : Local_gas_counter.local_gas_counter)
    (stack : S.stack s)
    s1 s2 s3,
      let g := (oc, step_constants_of_env env) in
    S.eval env i fuel1 stack = error.Return s1 ->
    Script_interpreter.dep_step fuel1 g gas j With_family.KNil
      (Stack_ty_of_stack s1) = Pervasives.Ok s2 ->
    Script_interpreter.dep_step fuel2 g gas (dep_of_micho_inst i _ j)
      With_family.KNil (Stack_ty_of_stack stack) = Pervasives.Ok s3 -> s2 = s3.
  Proof.
    induction fuel1; intros.
    { discriminate. }
    { destruct i eqn:G; admit. }
  Admitted.

  Lemma eval_seq_body_SEQ : forall
    {self_type tff A B C}
    eval
    (env : @S.proto_env self_type)
    (inst : syntax.instruction self_type false A B)
    (seq : syntax.instruction_seq self_type tff B C)
    stack,
    S.eval_seq_body eval env
      (syntax.SEQ inst seq) stack =
    error.bind (eval self_type false env A B inst stack)
      (S.eval_seq_body eval env seq).
  Proof.
    intros; reflexivity.
  Qed.

  Lemma dep_of_micho_iseq_correct : forall
    (fuel1 fuel2 : nat)
    {self_type tff s f}
    (i : @syntax.instruction_seq self_type tff s f)
    (env : S.proto_env)
    (env_valid : Valid.t env)
    (oc oc' : Local_gas_counter.outdated_context)
    (gas gas' : Local_gas_counter.local_gas_counter)
    (stack : S.stack s)
    s1 s2,
      let g := (oc, step_constants_of_env env) in
    S.eval_seq env i fuel1 stack = error.Return s1 ->
    Script_interpreter.dep_step fuel2 g gas (dep_of_micho_iseq i) With_family.KNil
      (Stack_ty_of_stack stack) = Pervasives.Ok s2 ->
      fst (fst s2) = Stack_ty_of_stack s1.
  Proof.
    unfold S.eval_seq.
    induction fuel2; intros.
    { discriminate. }
    { destruct i eqn:G.
      - simpl dep_of_micho_iseq in H0.
        admit.
      - admit.
      - destruct fuel1; try discriminate.
        rewrite eval_seq_body_SEQ in H.
        unfold error.bind in H.
        destruct S.eval eqn:G0; try discriminate.
        simpl dep_of_micho_iseq in H0.
        eapply IHfuel2.
        exact env_valid.
        admit.
        admit.
        exact H.
        simpl in H0.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit.
    }
  Admitted.
End Correctness.
