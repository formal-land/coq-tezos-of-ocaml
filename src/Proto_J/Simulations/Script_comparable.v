Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Script_interpreter_defs.

Require Import TezosOfOCaml.Proto_J.Simulations.Script_family.

Module Compare_comparable.
  (** Dependent version of [compare_comparable]. *)
  Fixpoint dep_compare_comparable
    (kind : Ty.t) (k : int) (x y : Ty.to_Set kind) {struct kind} : int :=
    let apply (ret : int) (k : int) : int :=
      match ret with
      | 0 => k
      | ret =>
        if ret >i 0 then
          1
        else
          (-1)
      end in
    match kind, x, y with
    | Ty.Unit, _, _ => 
      apply 0 k
    | Ty.Num _, _, _ =>
      apply (Alpha_context.Script_int.compare x y) k
    | Ty.Signature, _, _ => 
      apply (Script_typed_ir.Script_signature.compare x y) k
    | Ty.String, _, _ => 
      apply (Alpha_context.Script_string.compare x y) k
    | Ty.Bytes, _, _ => 
      apply (Compare.Bytes.(Compare.S.compare) x y) k
    | Ty.Mutez, _, _ => 
      apply (Alpha_context.Tez.compare x y) k
    | Ty.Key_hash, _, _ => 
      apply 
        (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare) x y)
        k
    | Ty.Key, _, _ => 
      apply
        (Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.compare) x y)
        k
    | Ty.Timestamp, _, _ => 
      apply (Alpha_context.Script_timestamp.compare x y) k
    | Ty.Address, _, _ => 
      apply (Script_comparable.compare_address x y) k
    | Ty.Tx_rollup_l2_address, _, _ => 
      apply (Script_comparable.compare_tx_rollup_l2_address x y) k
    | Ty.Bool, _, _ => 
      apply (Compare.Bool.(Compare.S.compare) x y) k
    | Ty.Pair tl tr, _, _ => 
      let '(lx, rx) := x in
      let '(ly, ry) := y in
      dep_compare_comparable tl (dep_compare_comparable tr k rx ry) lx ly
    | Ty.Union tl tr, _, _ =>
      match (x, y) with
      | (Script_typed_ir.L lx, Script_typed_ir.L ly) =>
        dep_compare_comparable tl k lx ly
      | (Script_typed_ir.L _, Script_typed_ir.R _) => (-1)
      | (Script_typed_ir.R _, Script_typed_ir.L _) => 1
      | (Script_typed_ir.R rx, Script_typed_ir.R ry) =>
        dep_compare_comparable tr k rx ry
      end
    | Ty.Lambda _ _, _, _ => apply 0 k
    | Ty.Option ty, _, _ =>
      match (x, y) with
      | (None, None) => apply 0 k
      | (None, Some _) => (-1)
      | (Some _, None) => 1
      | (Some x, Some y) =>
        dep_compare_comparable ty k x y
      end
    | Ty.List _, _, _ => apply 0 k
    | Ty.Set_ _, _, _ => apply 0 k
    | Ty.Map _ _, _, _ => apply 0 k
    | Ty.Big_map _ _, _, _ => apply 0 k
    | Ty.Contract _, _, _ => apply 0 k
    | Ty.Sapling_transaction, _, _ => apply 0 k
    | Ty.Sapling_transaction_deprecated, _, _ => apply 0 k
    | Ty.Sapling_state, _, _ => apply 0 k
    | Ty.Operation, _, _ => apply 0 k
    | Ty.Chain_id, _, _ =>
      apply (Script_typed_ir.Script_chain_id.compare x y) k
    | Ty.Never, _, _ => apply 0 k
    | Ty.Bls12_381_g1, _, _ => apply 0 k
    | Ty.Bls12_381_g2, _, _ => apply 0 k
    | Ty.Bls12_381_fr, _, _ => apply 0 k
    | Ty.Ticket _, _, _ => apply 0 k
    | Ty.Chest_key, _, _ => apply 0 k
    | Ty.Chest, _, _ => apply 0 k
    end.
End Compare_comparable.

(** A simulation of [compare_comparable]. *)
Definition dep_compare_comparable (kind : Ty.t) :
  Ty.to_Set kind -> Ty.to_Set kind -> int :=
  Compare_comparable.dep_compare_comparable kind 0.
