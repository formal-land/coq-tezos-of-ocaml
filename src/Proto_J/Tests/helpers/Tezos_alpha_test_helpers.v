Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Alpha_context.
Require TezosOfOCaml.Proto_J.Tez_repr.

Module Assert.
  Parameter pp_print_list :
    forall {a : Set},
    (Format.formatter -> a -> unit) -> Format.formatter -> list a -> unit.
End Assert.

Module Block.
  Module t.
    Record record : Set := Build {
      hash : Block_hash.t;
      header : Block_header.t;
      operations : list Alpha_context.Operation.packed;
      context : Context.t;
  }.
  End t.
  Definition t := t.record.

  Inductive baker_policy : Set :=
  | By_round : int -> baker_policy
  | By_account : public_key_hash -> baker_policy
  | Excluding : list public_key_hash -> baker_policy.
End Block.

Module Context.
  Parameter init :
    (* rng_state *) option Empty_set ->
    (* commitments *) option (list Commitment.t) ->
    (* initial_balances *) option (list int64) ->
    (* consensus_threshold *) option int ->
    (* min_proposal_quorum *) option int32 ->
    (* bootstrap_contracts *) option (list Empty_set) ->
    (* level *) option int32 ->
    (* cost_per_byte *) option Tez_repr.t ->
    (* liquidity_baking_subsidy *) option Tez_repr.t ->
    (* endorsing_reward_per_slot *) option Tez_repr.t ->
    (* baking_reward_bonus_per_slot *) option Tez_repr.t ->
    (* baking_reward_fixed_portion *) option Tez_repr.t ->
    (* origination_size *) option int ->
    (* blocks_per_cycle *) option int32 ->
    (* cycles_per_voting_period *) option int32 ->
    (* tx_rollup_enable *) option bool ->
    (* tx_rollup_sunset_level *) option int32 ->
    (* tx_rollup_origination_size *) option int ->
    (* sc_rollup_enable *) option bool ->
    int ->
    M? (Block.t * list Alpha_context.Contract.t).
End Context.

Module Incremental.
  Parameter t : Set.

  Definition incremental : Set := t.

  Parameter alpha_ctxt : incremental -> Alpha_context.context.

  Parameter begin_construction :
    (* timestamp *) option int64 ->
    (* seed_nonce_hash *) option Nonce_hash.t ->
    (* mempool_mode *) option bool ->
    (* policy *) option Block.baker_policy ->
    Block.t ->
    M? incremental.
End Incremental.
