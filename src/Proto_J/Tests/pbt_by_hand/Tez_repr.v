Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Tez_repr.
Require TezosOfOCaml.Proto_J.Proofs.Tez_repr.

Definition tez_to_z (tez : Tez_repr.t) : Z.t :=
  Tez_repr.to_mutez tez.

Definition z_in_mutez_bounds (z : Z.t) : bool :=
  (0 <=? z) && (z <=? Int64.max_int).

Definition compare (c' : Z.t) (c : M? Tez_repr.t) : bool :=
  match z_in_mutez_bounds c', c with
  | true, Pervasives.Ok c => c' =? Tez_repr.to_mutez c
  | true, Pervasives.Error _ => false
  | false, Pervasives.Ok _ => false
  | false, Pervasives.Error _ => true
  end.

Definition prop_binop (f : Tez_repr.t -> Tez_repr.t -> M? Tez_repr.t)
  (f' : Z.t -> Z.t -> Z.t) (a_b : Tez_repr.t * Tez_repr.t) : bool :=
  let '(a, b) := a_b in
  compare (f' (tez_to_z a) (tez_to_z b)) (f a b).

Definition prop_binop64 (f : Tez_repr.t -> int64 -> M? Tez_repr.t)
  (f' : Z.t -> Z.t -> Z.t) (a_b : Tez_repr.t * int64) : bool :=
  let '(a, b) := a_b in
  compare (f' (tez_to_z a) b) (f a b).

(** Verify that the test [test_coherent_mul] is always valid. *)
Lemma test_coherent_mul (a : Tez_repr.t) (b : int64) :
  Tez_repr.Valid.t a ->
  Int64.Valid.non_negative b ->
  prop_binop64 Tez_repr.op_starquestion Z.mul (a, b) = true.
Proof.
  intros.
  unfold prop_binop64, compare, z_in_mutez_bounds.
  destruct a as [a].
  destruct ((_ && _)%bool) eqn:?.
  { destruct Tez_repr.op_starquestion eqn:?.
    { rewrite <- (Tez_repr.op_starquestion_eq (t := Tez_repr.to_mutez t)); trivial.
      { lia. }
      { sauto lq: on. (* Found using the [best] tactic from Coq Hammer. *) }
    }
    { simpl in *.
      destruct (_ <? _) eqn:?.
      { lia. }
      { destruct (_ =? _) eqn:?.
        { discriminate. }
        { destruct (_ >? _) eqn:?.
          { unfold "/i64" in *.
            rewrite Int64.normalize_identity in * by nia.
            nia.
          }
          { discriminate. }
        }
      }
    }
  }
  { destruct Tez_repr.op_starquestion eqn:?.
    { simpl in *.
      destruct (_ <? _) eqn:?.
      { lia. }
      { destruct (_ =? _) eqn:?.
        { lia. }
        { destruct (_ >? _) eqn:?.
          { discriminate. }
          { unfold "/i64" in *.
            rewrite Int64.normalize_identity in * by nia.
            nia.
          }
        }
      }
    }
    { trivial. }
  }
Qed.

(** Verify that the test [test_coherent_sub] is always valid. *)
Lemma test_coherent_sub (a : Tez_repr.t) (b : Tez_repr.t) :
  Tez_repr.Valid.t a ->
  Tez_repr.Valid.t b ->
  prop_binop Tez_repr.op_minusquestion Z.sub (a, b) = true.
Proof.
  intros.
  unfold prop_binop, compare, z_in_mutez_bounds.
  destruct a as [a].
  destruct b as [b].
  destruct ((_ && _)%bool) eqn:?.
  { destruct Tez_repr.op_minusquestion eqn:?.
    { simpl.
      unfold Tez_repr.to_mutez.
      destruct t.
      pose proof (Tez_repr.op_minusquestion_eq a b H H0).
      rewrite Heqt in H1.
      lia.
    }
    { simpl in *.
      destruct (_ <=? _) eqn:?.
      { destruct (b <=? a) eqn:?.
        { discriminate. }
        { nia. }
      }
      { trivial. }
      }
    }
    { destruct Tez_repr.op_minusquestion eqn:?.
      { simpl in *.
        { destruct (_ <=? _) eqn:?.
          { lia. }
          { destruct (b <=? a) eqn:?.
            { lia. }
            { discriminate. } 
          }
        }
      }
      { lia. }
    }      
Qed.

(** Verify that [test_coherent_add] is always valid. *)
Lemma test_coherent_add (a: Tez_repr.t) (b: Tez_repr.t) :
  Tez_repr.Valid.t a ->
  Tez_repr.Valid.t b ->
  prop_binop Tez_repr.op_plusquestion Z.add (a, b) = true.
Proof.
  intros.
  unfold prop_binop, compare, z_in_mutez_bounds.
  destruct a as [a].
  destruct b as [b].
  simpl.
  destruct ((_ && _)%bool) eqn:?.
  { destruct (_ <? _) eqn:?; simpl.
    { lia. }
    { lia. }
  } 
  { destruct (_ <? _) eqn:?; simpl.
    { trivial. }
    { lia. }
  }
Qed.

(** Verify that [test_coherent_div] is always valid. *)
Lemma test_coherent_div (a: Tez_repr.t) (b : int64) :
  Tez_repr.Valid.t a ->
  Int64.Valid.positive b ->
  prop_binop64 Tez_repr.op_divquestion Z.div (a, b) = true.
Proof.
  intros.
  unfold prop_binop64, compare, z_in_mutez_bounds.
  destruct a as [a].
  simpl.
  destruct ((_ && _)%bool) eqn:?.
  { destruct (_ <=? 0) eqn:?; simpl.
    { lia. }
    { lia. }
  }
  { destruct (_ <=? 0) eqn:?; simpl.
    { trivial. }
    { nia. }
  }
Qed.
