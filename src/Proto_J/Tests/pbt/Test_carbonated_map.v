Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Proofs.Carbonated_map.
Require TezosOfOCaml.Proto_J.Tests.helpers.Alcotest.
Require TezosOfOCaml.Proto_J.Tests.helpers.Lib_test.
Require TezosOfOCaml.Proto_J.Tests.helpers.QCheck2.
Require TezosOfOCaml.Proto_J.Tests.helpers.Tezos_base.
Require TezosOfOCaml.Proto_J.Tests.helpers.Tezos_alpha_test_helpers.
Require TezosOfOCaml.Proto_J.Alpha_context.
Require TezosOfOCaml.Proto_J.Carbonated_map.

Definition wrap {A : Set} (m_value : M? A) : M? A :=
  wrap_tzresult m_value.

Definition new_ctxt (function_parameter : unit)
  : M? Alpha_context.context :=
  let '_ := function_parameter in
  let op_letstar {A C : Set}
    (m_value : M? A) (f_value : A -> M? C) : M? C :=
    Tezos_base.TzPervasives.Error_monad.Legacy_monad_globals.op_gtgteqquestion
      m_value f_value in
  op_letstar
    (Tezos_alpha_test_helpers.Context.init None None None None None None None
      None None None None None None None None None None None None 1)
    (fun function_parameter =>
      let '(block, _) := function_parameter in
      op_letstar
        (Tezos_alpha_test_helpers.Incremental.begin_construction None None None
          None block)
        (fun incr =>
          Tezos_base.TzPervasives.Error_monad.Legacy_monad_globals._return
            (Tezos_alpha_test_helpers.Incremental.alpha_ctxt incr))).

Module Compare_int.
  Definition t : Set := int.
  
  Definition compare : int -> int -> int := Int.compare.
  
  Definition compare_cost {A : Set} (function_parameter : A)
    : Saturation_repr.t :=
    let '_ := function_parameter in
    Saturation_repr.safe_int 10.
  
  Definition module :=
    {|
      Carbonated_map.COMPARABLE.compare := compare;
      Carbonated_map.COMPARABLE.compare_cost :=
        compare_cost
    |}.
End Compare_int.
Definition Compare_int := Compare_int.module.

Definition CM := Carbonated_map.Make Compare_int.

Definition unsafe_new_context (function_parameter : unit)
  : Alpha_context.context :=
  let '_ := function_parameter in
  Result.value_f (new_ctxt tt)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Pervasives.failwith "Failed to create context").

Definition int_map_gen
  : QCheck2.Gen.t (CM.(Carbonated_map.S.t) int) :=
  let ctxt := unsafe_new_context tt in
  QCheck2.Gen.map
    (fun (kvs : list (int * int)) =>
      let merge_overlap {A : Set} (ctxt : A) (x_value : int) (y_value : int)
        : M? (int * A) :=
        return? ((Z.add x_value y_value), ctxt) in
      match
        CM.(Carbonated_map.S.of_list) ctxt
          merge_overlap kvs with
      | Pervasives.Ok (map, _) => map
      | Pervasives.Error _ => Pervasives.failwith "Failed to construct map"
      end)
    (QCheck2.Gen.small_list
      (QCheck2.Gen.pair_value QCheck2.Gen.small_nat QCheck2.Gen.small_nat)).

Definition pp_int_map
  (fmt : Format.formatter)
  (map : CM.(Carbonated_map.S.t) int) : unit :=
  let pp :=
    Tezos_alpha_test_helpers.Assert.pp_print_list
      (fun (fmt : Format.formatter) =>
        fun (function_parameter : int * int) =>
          let '(k_value, v_value) := function_parameter in
          Format.fprintf fmt
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.Char_literal "(" % char
                (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_d
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.No_precision
                  (CamlinternalFormatBasics.String_literal ", "
                    (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_d
                      CamlinternalFormatBasics.No_padding
                      CamlinternalFormatBasics.No_precision
                      (CamlinternalFormatBasics.Char_literal ")" % char
                        CamlinternalFormatBasics.End_of_format))))) "(%d, %d)")
            k_value v_value) in
  Format.fprintf fmt
    (CamlinternalFormatBasics.Format
      (CamlinternalFormatBasics.Alpha CamlinternalFormatBasics.End_of_format)
      "%a") pp
    ((fun x_1 =>
      Result.value_f x_1
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          (* ❌ Assert instruction is not handled. *)
          assert (list (int * int)) false))
      (let op_letstar {A C : Set}
        (m_value : M? A) (f_value : A -> M? C) : M? C :=
        Tezos_base.TzPervasives.Error_monad.Legacy_monad_globals.op_gtgteqquestion
          m_value f_value in
      op_letstar (new_ctxt tt)
        (fun ctxt =>
          op_letstar
            (wrap
              (CM.(Carbonated_map.S.to_list)
                ctxt map))
            (fun function_parameter =>
              let '(kvs, _) := function_parameter in
              Tezos_base.TzPervasives.Error_monad.Legacy_monad_globals._return
                kvs)))).

Definition int_map_test
  (name : string)
  (f_value :
    CM.(Carbonated_map.S.t) int -> M? bool)
  : QCheck2.Test.t :=
  QCheck2.Test.make None (Some 100) None None None (Some name)
    (Some
      (Format.asprintf
        (CamlinternalFormatBasics.Format
          (CamlinternalFormatBasics.Alpha CamlinternalFormatBasics.End_of_format)
          "%a") pp_int_map)) None None int_map_gen
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      match f_value map with
      | Pervasives.Ok b_value => b_value
      | Pervasives.Error _ => false
      end).

Definition int_map_pair_test
  (name : string)
  (f_value :
    CM.(Carbonated_map.S.t) int ->
    CM.(Carbonated_map.S.t) int -> M? bool)
  : QCheck2.Test.t :=
  QCheck2.Test.make None (Some 100) None None None (Some name)
    (Some
      (fun (function_parameter :
        CM.(Carbonated_map.S.t) int *
          CM.(Carbonated_map.S.t) int) =>
        let '(map1, map2) := function_parameter in
        Format.asprintf
          (CamlinternalFormatBasics.Format
            (CamlinternalFormatBasics.Char_literal "(" % char
              (CamlinternalFormatBasics.Alpha
                (CamlinternalFormatBasics.String_literal ", "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.Char_literal ")" % char
                      CamlinternalFormatBasics.End_of_format))))) "(%a, %a)")
          pp_int_map map1 pp_int_map map2)) None None
    (QCheck2.Gen.pair_value int_map_gen int_map_gen)
    (fun (function_parameter :
      CM.(Carbonated_map.S.t) int *
        CM.(Carbonated_map.S.t) int) =>
      let '(map1, map2) := function_parameter in
      match f_value map1 map2 with
      | Pervasives.Ok b_value => b_value
      | Pervasives.Error _ => false
      end).

Definition unit_test (name : string) (f_value : unit -> M? bool)
  : QCheck2.Test.t :=
  QCheck2.Test.make None (Some 1) None None None (Some name) None None None
    (QCheck2.Gen._return tt)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      match f_value tt with
      | Pervasives.Ok b_value => b_value
      | _ => false
      end).

Definition op_letstar {A C : Set} : M? A -> (A -> M? C) -> M? C :=
  Result.bind.

Definition dummy_fail {A : Set} : M? A :=
  Result.error_value
    (Error_monad.trace_of_error
      (Build_extensible "Dummy_error" unit tt)).

Definition assert_map_contains {A : Set}
  (ctxt : Alpha_context.context)
  (map : CM.(Carbonated_map.S.t) A)
  (expected : list (int * A))
  : M? bool :=
  op_letstar
    (CM.(Carbonated_map.S.to_list) ctxt map)
    (fun function_parameter =>
      let '(kvs, _ctxt) := function_parameter in
      Pervasives.Ok
        (Compare.polymorphic_eq
          (List.sort Compare.polymorphic_compare kvs)
          (List.sort Compare.polymorphic_compare expected))).

Definition assert_equal_map {A : Set}
  (ctxt : Alpha_context.context)
  (map : CM.(Carbonated_map.S.t) A)
  (expected : CM.(Carbonated_map.S.t) A)
  : M? bool :=
  op_letstar
    (CM.(Carbonated_map.S.to_list) ctxt expected)
    (fun function_parameter =>
      let '(kvs, ctxt) := function_parameter in
      assert_map_contains ctxt map kvs).

(** Test that the size of an empty map is 0. *)
Definition test_empty : QCheck2.Test.t :=
  unit_test "Size of empty map is 0"
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Pervasives.Ok
        (Compare.polymorphic_eq
          (CM.(Carbonated_map.S.size_value)
            (a := string) (* This could be any type *)
            CM.(Carbonated_map.S.empty)) 0)).

(** Test adding a new element *)
Definition test_update_add : QCheck2.Test.t :=
  unit_test "Update add"
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.of_list) ctxt
          (fun (function_parameter :
            Alpha_context.context) =>
            let '_ := function_parameter in
            fun (function_parameter : int) =>
              let '_ := function_parameter in
              fun (function_parameter : int) =>
                let '_ := function_parameter in
                dummy_fail) [ (1, 1); (2, 2); (3, 3) ])
        (fun function_parameter =>
          let '(map, ctxt) := function_parameter in
          let update_replace {A : Set}
            (ctxt : Alpha_context.context) (key : int)
            (value : A)
            (map : CM.(Carbonated_map.S.t) A)
            : Error_monad.tzresult
              (CM.(Carbonated_map.S.t) A *
                Alpha_context.context) :=
            CM.(Carbonated_map.S.update) ctxt key
              (fun (ctxt : Alpha_context.context) =>
                fun (function_parameter : option A) =>
                  let '_ := function_parameter in
                  Pervasives.Ok ((Some value), ctxt)) map in
          op_letstar (update_replace ctxt 4 4 map)
            (fun function_parameter =>
              let '(map, ctxt) := function_parameter in
              assert_map_contains ctxt map [ (1, 1); (2, 2); (3, 3); (4, 4) ]))).

(** Test replacing an existing element. *)
Definition test_update_replace : QCheck2.Test.t :=
  unit_test "Update replace"
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.of_list) ctxt
          (fun (function_parameter :
            Alpha_context.context) =>
            let '_ := function_parameter in
            fun (function_parameter : int) =>
              let '_ := function_parameter in
              fun (function_parameter : int) =>
                let '_ := function_parameter in
                dummy_fail) [ (1, 1); (2, 2); (3, 3) ])
        (fun function_parameter =>
          let '(map, ctxt) := function_parameter in
          let update_replace {A : Set}
            (ctxt : Alpha_context.context) (key : int)
            (value : A)
            (map : CM.(Carbonated_map.S.t) A)
            : Error_monad.tzresult
              (CM.(Carbonated_map.S.t) A *
                Alpha_context.context) :=
            CM.(Carbonated_map.S.update) ctxt key
              (fun (ctxt : Alpha_context.context) =>
                fun (function_parameter : option A) =>
                  let '_ := function_parameter in
                  Pervasives.Ok ((Some value), ctxt)) map in
          op_letstar (update_replace ctxt 1 42 map)
            (fun function_parameter =>
              let '(map, ctxt) := function_parameter in
              assert_map_contains ctxt map [ (1, 42); (2, 2); (3, 3) ]))).

(** Test merging when ignoring new overlapping keys. *)
Definition test_merge_overlaps_left : QCheck2.Test.t :=
  unit_test "Merge overlap keep existing"
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.of_list) ctxt
          (fun (ctxt : Alpha_context.context) =>
            fun (_left : int) =>
              fun (function_parameter : int) =>
                let '_ := function_parameter in
                Pervasives.Ok (_left, ctxt))
          [ (1, 1); (2, 2); (3, 3); (1, 11); (2, 22); (3, 33); (4, 44) ])
        (fun function_parameter =>
          let '(map, ctxt) := function_parameter in
          assert_map_contains ctxt map [ (1, 1); (2, 2); (3, 3); (4, 44) ])).

(** Test merging when replacing the element of a new overlapping key. *)
Definition test_merge_overlaps_right : QCheck2.Test.t :=
  unit_test "Merge overlap replace"
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.of_list) ctxt
          (fun (ctxt : Alpha_context.context) =>
            fun (function_parameter : int) =>
              let '_ := function_parameter in
              fun (_right : int) => Pervasives.Ok (_right, ctxt))
          [ (1, 1); (2, 2); (3, 3); (1, 11); (2, 22); (3, 33); (4, 44) ])
        (fun function_parameter =>
          let '(map, ctxt) := function_parameter in
          assert_map_contains ctxt map [ (1, 11); (2, 22); (3, 33); (4, 44) ])).

(** Test merging when combining elements of overlapping keys. *)
Definition test_merge_overlaps_add : QCheck2.Test.t :=
  unit_test "Merge overlap by adding"
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.of_list) ctxt
          (fun (ctxt : Alpha_context.context) =>
            fun (_left : int) =>
              fun (_right : int) => Pervasives.Ok ((Z.add _left _right), ctxt))
          [ (1, 1); (2, 2); (3, 3); (1, 1); (2, 2); (3, 3); (4, 4) ])
        (fun function_parameter =>
          let '(map, ctxt) := function_parameter in
          assert_map_contains ctxt map [ (1, 2); (2, 4); (3, 6); (4, 4) ])).

(** Test update with merging elements of new and existing keys by adding them. *)
Definition test_update_merge : QCheck2.Test.t :=
  unit_test "Update with merge add"
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.of_list) ctxt
          (fun (function_parameter :
            Alpha_context.context) =>
            let '_ := function_parameter in
            fun (function_parameter : int) =>
              let '_ := function_parameter in
              fun (function_parameter : int) =>
                let '_ := function_parameter in
                dummy_fail) [ (1, 1); (2, 2); (3, 3) ])
        (fun function_parameter =>
          let '(map, ctxt) := function_parameter in
          let update_merge
            (ctxt : Alpha_context.context) (key : int)
            (new_value : int)
            (map : CM.(Carbonated_map.S.t) int)
            : Error_monad.tzresult
              (CM.(Carbonated_map.S.t) int *
                Alpha_context.context) :=
            CM.(Carbonated_map.S.update) ctxt key
              (fun (ctxt : Alpha_context.context) =>
                fun (existing : option int) =>
                  match existing with
                  | None => Pervasives.Ok ((Some new_value), ctxt)
                  | Some old_value =>
                    Pervasives.Ok ((Some (Z.add new_value old_value)), ctxt)
                  end) map in
          op_letstar (update_merge ctxt 1 1 map)
            (fun function_parameter =>
              let '(map, ctxt) := function_parameter in
              op_letstar (update_merge ctxt 4 4 map)
                (fun function_parameter =>
                  let '(map, ctxt) := function_parameter in
                  assert_map_contains ctxt map
                    [ (1, 2); (2, 2); (3, 3); (4, 4) ])))).

(** Test merging two maps when keeping the original value for overlapping keys. *)
Definition test_merge_map_keep_existing : QCheck2.Test.t :=
  unit_test "Merge overlap keep existing"
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.of_list) ctxt
          (fun (function_parameter :
            Alpha_context.context) =>
            let '_ := function_parameter in
            fun (function_parameter : string) =>
              let '_ := function_parameter in
              fun (function_parameter : string) =>
                let '_ := function_parameter in
                dummy_fail) [ (1, "a"); (2, "b"); (3, "c") ])
        (fun function_parameter =>
          let '(map1, ctxt) := function_parameter in
          op_letstar
            (CM.(Carbonated_map.S.of_list) ctxt
              (fun (function_parameter :
                Alpha_context.context) =>
                let '_ := function_parameter in
                fun (function_parameter : string) =>
                  let '_ := function_parameter in
                  fun (function_parameter : string) =>
                    let '_ := function_parameter in
                    dummy_fail) [ (2, "b'"); (3, "c'"); (4, "d'") ])
            (fun function_parameter =>
              let '(map2, ctxt) := function_parameter in
              op_letstar
                (CM.(Carbonated_map.S.merge) ctxt
                  (fun (ctxt : Alpha_context.context)
                    =>
                    fun (_left : string) =>
                      fun (function_parameter : string) =>
                        let '_ := function_parameter in
                        Pervasives.Ok (_left, ctxt)) map1 map2)
                (fun function_parameter =>
                  let '(map, ctxt) := function_parameter in
                  assert_map_contains ctxt map
                    [ (1, "a"); (2, "b"); (3, "c"); (4, "d'") ])))).

(** Test merging two maps when replacing the value for overlapping keys. *)
Definition test_merge_map_replace_existing : QCheck2.Test.t :=
  unit_test "Merge overlap replace existing"
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.of_list) ctxt
          (fun (function_parameter :
            Alpha_context.context) =>
            let '_ := function_parameter in
            fun (function_parameter : string) =>
              let '_ := function_parameter in
              fun (function_parameter : string) =>
                let '_ := function_parameter in
                dummy_fail) [ (1, "a"); (2, "b"); (3, "c") ])
        (fun function_parameter =>
          let '(map1, ctxt) := function_parameter in
          op_letstar
            (CM.(Carbonated_map.S.of_list) ctxt
              (fun (function_parameter :
                Alpha_context.context) =>
                let '_ := function_parameter in
                fun (function_parameter : string) =>
                  let '_ := function_parameter in
                  fun (function_parameter : string) =>
                    let '_ := function_parameter in
                    dummy_fail) [ (2, "b'"); (3, "c'"); (4, "d'") ])
            (fun function_parameter =>
              let '(map2, ctxt) := function_parameter in
              op_letstar
                (CM.(Carbonated_map.S.merge) ctxt
                  (fun (ctxt : Alpha_context.context)
                    =>
                    fun (function_parameter : string) =>
                      let '_ := function_parameter in
                      fun (_right : string) => Pervasives.Ok (_right, ctxt)) map1
                  map2)
                (fun function_parameter =>
                  let '(map, ctxt) := function_parameter in
                  assert_map_contains ctxt map
                    [ (1, "a"); (2, "b'"); (3, "c'"); (4, "d'") ])))).

(** Test deleting existing and non-existing keys. *)
Definition test_update_delete : QCheck2.Test.t :=
  unit_test "Update delete"
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.of_list) ctxt
          (fun (function_parameter :
            Alpha_context.context) =>
            let '_ := function_parameter in
            fun (function_parameter : int) =>
              let '_ := function_parameter in
              fun (function_parameter : int) =>
                let '_ := function_parameter in
                dummy_fail) [ (1, 1); (2, 2); (3, 3) ])
        (fun function_parameter =>
          let '(map, ctxt) := function_parameter in
          let delete {A : Set}
            (ctxt : Alpha_context.context) (key : int)
            (map : CM.(Carbonated_map.S.t) A)
            : Error_monad.tzresult
              (CM.(Carbonated_map.S.t) A *
                Alpha_context.context) :=
            CM.(Carbonated_map.S.update) ctxt key
              (fun (ctxt : Alpha_context.context) =>
                fun (function_parameter : option A) =>
                  let '_ := function_parameter in
                  Pervasives.Ok (None, ctxt)) map in
          op_letstar (delete ctxt 1 map)
            (fun function_parameter =>
              let '(map, ctxt) := function_parameter in
              op_letstar (delete ctxt 4 map)
                (fun function_parameter =>
                  let '(map, ctxt) := function_parameter in
                  assert_map_contains ctxt map [ (2, 2); (3, 3) ])))).

(** Test that merging [empty] with a map returns the same map. *)
Definition test_empty_left_identity_for_merge : QCheck2.Test.t :=
  int_map_test "Empty map is left identity for merge"
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.merge) ctxt
          (fun (function_parameter :
            Alpha_context.context) =>
            let '_ := function_parameter in
            fun (function_parameter : int) =>
              let '_ := function_parameter in
              fun (function_parameter : int) =>
                let '_ := function_parameter in
                dummy_fail) map
          CM.(Carbonated_map.S.empty))
        (fun function_parameter =>
          let '(map', ctxt) := function_parameter in
          assert_equal_map ctxt map map')).

(** Test that merging a map with [empty] returns the same map. *)
Definition test_empty_right_identity_for_merge : QCheck2.Test.t :=
  int_map_test "Empty map is right identity for merge"
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.merge) ctxt
          (fun (function_parameter :
            Alpha_context.context) =>
            let '_ := function_parameter in
            fun (function_parameter : int) =>
              let '_ := function_parameter in
              fun (function_parameter : int) =>
                let '_ := function_parameter in
                dummy_fail)
          CM.(Carbonated_map.S.empty) map)
        (fun function_parameter =>
          let '(map', ctxt) := function_parameter in
          assert_equal_map ctxt map map')).

(** Test that [size] returns the number of key value pairs of a map. *)
Definition test_size : QCheck2.Test.t :=
  int_map_test "Size returns the number of elements"
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.to_list) ctxt map)
        (fun function_parameter =>
          let '(kvs, _) := function_parameter in
          return?
            (Compare.List_length_with.op_eq kvs
              (CM.(Carbonated_map.S.size_value) map)))).

(** Test that all keys of a map are found. *)
Definition test_find_existing : QCheck2.Test.t :=
  int_map_test "Find all elements"
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.to_list) ctxt map)
        (fun function_parameter =>
          let '(kvs, _) := function_parameter in
          op_letstar
            (Tezos_base.TzPervasives.List.fold_left_e
              (fun (ctxt : Alpha_context.context) =>
                fun (function_parameter : int * int) =>
                  let '(k_value, v_value) := function_parameter in
                  op_letstar
                    (CM.(Carbonated_map.S.find)
                      ctxt k_value map)
                    (fun function_parameter =>
                      let '(v_opt, ctxt) := function_parameter in
                      match
                        (v_opt,
                          match v_opt with
                          | Some v' => Compare.polymorphic_eq v_value v'
                          | _ => false
                          end) with
                      | (Some v', true) => Pervasives.Ok ctxt
                      | (_, _) => dummy_fail
                      end)) ctxt kvs)
            (fun function_parameter =>
              let '_ := function_parameter in
              Pervasives.Ok true))).

(** Test that find returns [None] for non-existing keys. *)
Definition test_find_non_existing : QCheck2.Test.t :=
  int_map_test "Should not find non-existing"
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.to_list) ctxt map)
        (fun function_parameter =>
          let '(kvs, _) := function_parameter in
          let key := 42 in
          op_letstar
            (CM.(Carbonated_map.S.find) ctxt key
              map)
            (fun function_parameter =>
              let '(v_opt, _) := function_parameter in
              match
                Tezos_base.TzPervasives.List.find_opt
                  (fun (function_parameter : int * int) =>
                    let '(k_value, _) := function_parameter in
                    Compare.polymorphic_eq k_value key) kvs with
              | Some (_, value) => Pervasives.Ok (Compare.polymorphic_eq (Some value) v_opt)
              | None => Pervasives.Ok (Compare.polymorphic_eq None v_opt)
              end))).

(** Test that [to_list] followed by [of_list] returns the same map. *)
Definition test_to_list_of_list : QCheck2.Test.t :=
  int_map_test "To-list/of-list roundtrip"
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      let ctxt := unsafe_new_context tt in
      let merge_overlap {A : Set} (ctxt : A) (x_value : int) (y_value : int)
        : M? (int * A) :=
        return? ((Z.add x_value y_value), ctxt) in
      op_letstar
        (CM.(Carbonated_map.S.to_list) ctxt map)
        (fun function_parameter =>
          let '(kvs, ctxt) := function_parameter in
          op_letstar
            (CM.(Carbonated_map.S.of_list) ctxt
              merge_overlap kvs)
            (fun function_parameter =>
              let '(map', ctxt) := function_parameter in
              assert_equal_map ctxt map map'))).

(** Test that merging two maps is equivalent to merging the concatenated
    key-value lists of both maps. *)
Definition test_merge_against_list : QCheck2.Test.t :=
  int_map_pair_test "Merge compared with list operation"
    (fun (map1 : CM.(Carbonated_map.S.t) int) =>
      fun (map2 : CM.(Carbonated_map.S.t) int) =>
        let ctxt := unsafe_new_context tt in
        let merge_overlap {A : Set} (ctxt : A) (x_value : int) (y_value : int)
          : M? (int * A) :=
          Pervasives.Ok ((Z.add x_value y_value), ctxt) in
        op_letstar
          (CM.(Carbonated_map.S.to_list) ctxt map1)
          (fun function_parameter =>
            let '(kvs1, ctxt) := function_parameter in
            op_letstar
              (CM.(Carbonated_map.S.to_list) ctxt
                map2)
              (fun function_parameter =>
                let '(kvs2, ctxt) := function_parameter in
                op_letstar
                  (CM.(Carbonated_map.S.merge)
                    ctxt merge_overlap map1 map2)
                  (fun function_parameter =>
                    let '(map_merged1, ctxt) := function_parameter in
                    op_letstar
                      (CM.(Carbonated_map.S.of_list)
                        ctxt merge_overlap (List.app kvs1 kvs2))
                      (fun function_parameter =>
                        let '(map_merged2, ctxt) := function_parameter in
                        assert_equal_map ctxt map_merged1 map_merged2))))).

(** Test that merging a map with itself does not alter its size. *)
Definition test_size_merge_self : QCheck2.Test.t :=
  int_map_test "Size should not change when map is merging with itself"
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      let ctxt := unsafe_new_context tt in
      let size1 := CM.(Carbonated_map.S.size_value) map
        in
      op_letstar
        (CM.(Carbonated_map.S.merge) ctxt
          (fun (ctxt : Alpha_context.context) =>
            fun (_left : int) =>
              fun (_right : int) => Pervasives.Ok ((Z.add _left _right), ctxt)) map
          map)
        (fun function_parameter =>
          let '(map2, _) := function_parameter in
          let size2 :=
            CM.(Carbonated_map.S.size_value) map2 in
          Pervasives.Ok (Compare.polymorphic_eq size1 size2))).

(** Test that merging with a failing merge operation yields an error. *)
Definition test_merge_fail : QCheck2.Test.t :=
  int_map_test "Merging with failing merge-overlap"
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      let ctxt := unsafe_new_context tt in
      return?
        match
          ((CM.(Carbonated_map.S.merge) ctxt
            (fun (function_parameter :
              Alpha_context.context) =>
              let '_ := function_parameter in
              fun (function_parameter : int) =>
                let '_ := function_parameter in
                fun (function_parameter : int) =>
                  let '_ := function_parameter in
                  dummy_fail) map map),
            match
              CM.(Carbonated_map.S.merge) ctxt
                (fun (function_parameter :
                  Alpha_context.context) =>
                  let '_ := function_parameter in
                  fun (function_parameter : int) =>
                    let '_ := function_parameter in
                    fun (function_parameter : int) =>
                      let '_ := function_parameter in
                      dummy_fail) map map with
            | Pervasives.Ok _ =>
              Compare.polymorphic_eq
                (CM.(Carbonated_map.S.size_value) map) 0
            | _ => false
            end) with
        | (Pervasives.Ok _, true) => true
        | (Pervasives.Ok _, _) => false
        | (Pervasives.Error _, _) => true
        end).

(** Test that adding one key-value pair to a map increases its size by one iff
    the key already exists. *)
Definition test_size_add_one : QCheck2.Test.t :=
  int_map_test "Add a new element increases size by one"
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      let ctxt := unsafe_new_context tt in
      let key := 42 in
      op_letstar
        (CM.(Carbonated_map.S.find) ctxt key map)
        (fun function_parameter =>
          let '(val_opt, ctxt) := function_parameter in
          op_letstar
            (CM.(Carbonated_map.S.update) ctxt key
              (fun (ctxt : Alpha_context.context) =>
                fun (existing : option int) =>
                  match existing with
                  | None => Pervasives.Ok ((Some 42), ctxt)
                  | Some old_value => Pervasives.Ok ((Some old_value), ctxt)
                  end) map)
            (fun function_parameter =>
              let '(map', _ctxt) := function_parameter in
              let size :=
                CM.(Carbonated_map.S.size_value) map in
              let size' :=
                CM.(Carbonated_map.S.size_value) map' in
              match val_opt with
              | None => Pervasives.Ok (Compare.polymorphic_eq size' (Z.add size 1))
              | Some _ => Pervasives.Ok (Compare.polymorphic_eq size' size)
              end))).

(** Test that mapping over a map is equivalent to mapping over the list of
    key-value pairs and reconstructing the map. That is, the following diagram
    commutes:

    [map] ----to_list---> [list]
      |                     |
    [map f]              [List.map f]
      |                     |
      v                     v
    [map] --- to_list --> [list] *)
Definition test_map : QCheck2.Test.t :=
  int_map_test "Test that map commutes with mapping over list"
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.to_list) ctxt map)
        (fun function_parameter =>
          let '(kvs, ctxt) := function_parameter in
          op_letstar
            (CM.(Carbonated_map.S.map) ctxt
              (fun (ctxt : Alpha_context.context) =>
                fun (function_parameter : int) =>
                  let '_ := function_parameter in
                  fun (x_value : int) => Pervasives.Ok ((Z.add x_value 1), ctxt))
              map)
            (fun function_parameter =>
              let '(map', ctxt) := function_parameter in
              let kvs' :=
                Tezos_base.TzPervasives.List.map
                  (fun (function_parameter : int * int) =>
                    let '(k_value, v_value) := function_parameter in
                    (k_value, (Z.add v_value 1))) kvs in
              assert_map_contains ctxt map' kvs'))).

(** Test that folding over an empty map does not invoke the accumulator
    function. *)
Definition test_fold_empty : QCheck2.Test.t :=
  unit_test "Fold empty"
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.fold) ctxt
          (fun (_ctxt : Alpha_context.context) =>
            fun (_acc : int) => fun (_k : int) => fun _v => dummy_fail) 0
          (CM.(Carbonated_map.S.empty)
            (a := string) (* This could be any type *)))
        (fun function_parameter =>
          let '(x_value, _) := function_parameter in
          Pervasives.Ok (Compare.polymorphic_eq x_value 0))).

(** Test that folding over a map is equivalent to folding over the corresponding
    list of key-value pairs. That is, the following diagram commutes:

    [map] -- to_list --> [list]
      |                    |
    [fold f z]      [List.fold_left f z]
      |                    |
     res <----- id -----> res *)
Definition test_fold : QCheck2.Test.t :=
  int_map_test "Test that fold commutes with folding over a list"
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.to_list) ctxt map)
        (fun function_parameter =>
          let '(kvs, ctxt) := function_parameter in
          let sum :=
            Tezos_base.TzPervasives.List.fold_left
              (fun (sum : int) =>
                fun (function_parameter : int * int) =>
                  let '(k_value, v_value) := function_parameter in
                  Z.add (Z.add k_value v_value) sum) 0 kvs in
          op_letstar
            (CM.(Carbonated_map.S.fold) ctxt
              (fun (ctxt : Alpha_context.context) =>
                fun (sum : int) =>
                  fun (k_value : int) =>
                    fun (v_value : int) =>
                      Pervasives.Ok ((Z.add (Z.add k_value v_value) sum), ctxt)) 0
              map)
            (fun function_parameter =>
              let '(sum', _) := function_parameter in
              Pervasives.Ok (Compare.polymorphic_eq sum sum')))).

(** Test that all key-value pairs can be collected by a fold. And that the
    order is the same as for [to_list]. *)
Definition test_fold_to_list : QCheck2.Test.t :=
  int_map_test "Test that fold collecting the elements agrees with to-list"
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.to_list) ctxt map)
        (fun function_parameter =>
          let '(kvs, ctxt) := function_parameter in
          op_letstar
            (CM.(Carbonated_map.S.fold) ctxt
              (fun (ctxt : Alpha_context.context) =>
                fun (kvs : list (int * int)) =>
                  fun (k_value : int) =>
                    fun (v_value : int) =>
                      Pervasives.Ok ((cons (k_value, v_value) kvs), ctxt)) nil map)
            (fun function_parameter =>
              let '(kvs', _) := function_parameter in
              Pervasives.Ok (Compare.polymorphic_eq kvs (Tezos_base.TzPervasives.List.rev kvs'))))).

(** Test that mapping with a failing function fails iff the list is non-empty. *)
Definition test_map_fail : QCheck2.Test.t :=
  int_map_test "Test map with failing function"
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      let ctxt := unsafe_new_context tt in
      return?
        match
          ((CM.(Carbonated_map.S.map) (b := string) (* Could be any type *) ctxt
            (fun (_ctxt : Alpha_context.context) =>
              fun (_key : int) => fun (_val : int) =>
                dummy_fail) map),
            match
              CM.(Carbonated_map.S.map) (b := string) (* Could be any type *) ctxt
                (fun (_ctxt : Alpha_context.context) =>
                  fun (_key : int) => fun (_val : int) =>
                    dummy_fail) map with
            | Pervasives.Ok _ =>
              Compare.polymorphic_eq
                (CM.(Carbonated_map.S.size_value) map) 0
            | _ => false
            end) with
        | (Pervasives.Ok _, true) => true
        | (Pervasives.Error _, _) => true
        | (Pervasives.Ok _, _) => false
        end).

(** Test that removing an existing key from a map decreases its size by one. *)
Definition test_size_remove_one : QCheck2.Test.t :=
  int_map_test "Remove new element decreases size by one"
    (fun (map : CM.(Carbonated_map.S.t) int) =>
      let ctxt := unsafe_new_context tt in
      op_letstar
        (CM.(Carbonated_map.S.to_list) ctxt map)
        (fun function_parameter =>
          let '(kvs, ctxt) := function_parameter in
          let key :=
            match kvs with
            | cons (k_value, _) _ => k_value
            | _ => 42
            end in
          op_letstar
            (CM.(Carbonated_map.S.find) ctxt key
              map)
            (fun function_parameter =>
              let '(val_opt, ctxt) := function_parameter in
              op_letstar
                (CM.(Carbonated_map.S.update) ctxt
                  key
                  (fun (ctxt : Alpha_context.context)
                    =>
                    fun (function_parameter : option int) =>
                      let '_ := function_parameter in
                      Pervasives.Ok (None, ctxt)) map)
                (fun function_parameter =>
                  let '(map', _ctxt) := function_parameter in
                  let size :=
                    CM.(Carbonated_map.S.size_value) map
                    in
                  let size' :=
                    CM.(Carbonated_map.S.size_value)
                      map' in
                  match val_opt with
                  | None => Pervasives.Ok (Compare.polymorphic_eq size' size)
                  | Some _ => Pervasives.Ok (Compare.polymorphic_eq size' (Z.sub size 1))
                  end)))).

Definition tests : list QCheck2.Test.t :=
  [
    test_empty;
    test_size;
    test_to_list_of_list;
    test_empty_left_identity_for_merge;
    test_empty_right_identity_for_merge;
    test_size_merge_self;
    test_size_add_one;
    test_size_remove_one;
    test_merge_against_list;
    test_merge_overlaps_left;
    test_merge_overlaps_right;
    test_merge_overlaps_add;
    test_merge_fail;
    test_merge_map_keep_existing;
    test_merge_map_replace_existing;
    test_find_non_existing;
    test_find_existing;
    test_update_add;
    test_update_merge;
    test_update_delete;
    test_map;
    test_fold_empty;
    test_fold;
    test_fold_to_list;
    test_map_fail
  ].

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  let rand :=
    Random.State.make
      (* ❌ Arrays not handled. *)
      [ 322392893; 71287309; 397060904 ] in
  Alcotest.run None None None None None None None None None None None None
    "protocol > pbt > carbonated map"
    [
      ("Carbonated map",
        (Lib_test.Qcheck_helpers.qcheck_wrap None None (Some rand) tests))
    ].

(** ** Proofs *)

(** Consuming gas from [unsafe_new_context] does not change the context. This
    means that the gas mode is "unlimited" and the gas level negative. *)
Axiom unsafe_new_ctxt_gas_infinite : forall cost,
  Alpha_context.Gas.consume (unsafe_new_context tt) cost =
  return? (unsafe_new_context tt).

(** The [of_list] operation terminates with [unsafe_new_context]. *)
Lemma of_list_terminates {a : Set} f l :
  match CM.(Carbonated_map.S.of_list) (a := a) (unsafe_new_context tt) f l with
  | Pervasives.Ok _ => True
  | Pervasives.Error _ => False
  end.
Admitted.

(** The generator [int_map_gen] generates valid maps. *)
Lemma int_map_gen_is_valid map :
  int_map_gen map = true ->
  Carbonated_map.Make.Valid.t map.
Proof.
  unfold int_map_gen.
  intros H.
  pose proof (QCheck2.Gen.map_spec _ _ _ H) as H_map.
  destruct H_map as [l [H_l H_map]].
  rewrite <- H_map; clear H_map; simpl.
  pose proof (QCheck2.Gen.list_value_spec _ _ H_l).
  match goal with
  | |- context[Carbonated_map.Make.of_list (H := ?H) ?ctxt ?merge_overlap ?l] =>
    pose proof (
      Carbonated_map.Make.of_list_is_valid (H := H) ctxt merge_overlap l
    );
    pose proof (of_list_terminates merge_overlap l)
  end.
  simpl in *.
  destruct Carbonated_map.Make.of_list; [|contradiction].
  hauto lq: on.
Qed.

(** [test_empty] is valid. *)
Lemma test_empty_proof : test_empty.
Proof.
  unfold test_empty, unit_test, QCheck2.Test.make.
  intros.
  apply Compare.polymorphic_eq_true.
  reflexivity.
Qed.

(** [test_size] is valid. *)
Lemma test_size_proof : test_size.
Proof.
  unfold test_size, int_map_test, QCheck2.Test.make.
  intros map H.
  pose proof (int_map_gen_is_valid _ H) as H_map.
  simpl.
  unfold Carbonated_map.Make.to_list, Carbonated_map.Make.M,
    Carbonated_map.Make.size_value.
  rewrite unsafe_new_ctxt_gas_infinite; simpl.
  rewrite <- H_map.
  destruct map as [map size].
  rewrite Compare.List_length_with.op_eq_eq; simpl.
  { apply Z.eqb_refl. }
  { apply List.length_is_valid. }
Qed.

(** [test_to_list_of_list] is valid. *)
Lemma test_to_list_of_list_proof :
  test_to_list_of_list.
Proof.
Admitted.

(** [test_empty_left_identity_for_merge] is valid. *)
Lemma test_empty_left_identity_for_merge_proof :
  test_empty_left_identity_for_merge.
Proof.
Admitted.

(** [test_empty_right_identity_for_merge] is valid. *)
Lemma test_empty_right_identity_for_merge_proof :
  test_empty_right_identity_for_merge.
Proof.
Admitted.

(** [test_size_merge_self] is valid. *)
Lemma test_size_merge_self_proof :
  test_size_merge_self.
Proof.
Admitted.

(** [test_size_add_one] is valid. *)
Lemma test_size_add_one_proof :
  test_size_add_one.
Proof.
Admitted.

(** [test_size_remove_one] is valid. *)
Lemma test_size_remove_one_proof :
  test_size_remove_one.
Proof.
Admitted.

(** [test_merge_against_list] is valid. *)
Lemma test_merge_against_list_proof :
  test_merge_against_list.
Proof.
Admitted.

(** [test_merge_overlaps_left] is valid. *)
Lemma test_merge_overlaps_left_proof :
  test_merge_overlaps_left.
Proof.
Admitted.

(** [test_merge_overlaps_right] is valid. *)
Lemma test_merge_overlaps_right_proof :
  test_merge_overlaps_right.
Proof.
Admitted.

(** [test_merge_overlaps_add] is valid. *)
Lemma test_merge_overlaps_add_proof :
  test_merge_overlaps_add.
Proof.
Admitted.

(** [test_merge_fail] is valid. *)
Lemma test_merge_fail_proof :
  test_merge_fail.
Proof.
Admitted.

(** [test_merge_map_keep_existing] is valid. *)
Lemma test_merge_map_keep_existing_proof :
  test_merge_map_keep_existing.
Proof.
Admitted.

(** [test_merge_map_replace_existing] is valid. *)
Lemma test_merge_map_replace_existing_proof :
  test_merge_map_replace_existing.
Proof.
Admitted.

(** [test_find_non_existing] is valid. *)
Lemma test_find_non_existing_proof :
  test_find_non_existing.
Proof.
Admitted.

(** [test_find_existing] is valid. *)
Lemma test_find_existing_proof :
  test_find_existing.
Proof.
Admitted.

(** [test_update_add] is valid. *)
Lemma test_update_add_proof :
  test_update_add.
Proof.
Admitted.

(** [test_update_merge] is valid. *)
Lemma test_update_merge_proof :
  test_update_merge.
Proof.
Admitted.

(** [test_update_delete] is valid. *)
Lemma test_update_delete_proof :
  test_update_delete.
Proof.
Admitted.

(** [test_map] is valid. *)
Lemma test_map_proof :
  test_map.
Proof.
Admitted.

(** [test_fold_empty] is valid. *)
Lemma test_fold_empty_proof :
  test_fold_empty.
Proof.
Admitted.

(** [test_fold] is valid. *)
Lemma test_fold_proof :
  test_fold.
Proof.
Admitted.

(** [test_fold_to_list] is valid. *)
Lemma test_fold_to_list_proof :
  test_fold_to_list.
Proof.
Admitted.

(** [test_map_fail] is valid. *)
Lemma test_map_fail_proof :
  test_map_fail.
Proof.
Admitted.

(** All the tests are valid. *)
Lemma tests_proof : List.Forall id tests.
Proof.
  repeat constructor; unfold id.
  { apply test_empty_proof. }
  { apply test_size_proof. }
  { apply test_to_list_of_list_proof. }
  { apply test_empty_left_identity_for_merge_proof. }
  { apply test_empty_right_identity_for_merge_proof. }
  { apply test_size_merge_self_proof. }
  { apply test_size_add_one_proof. }
  { apply test_size_remove_one_proof. }
  { apply test_merge_against_list_proof. }
  { apply test_merge_overlaps_left_proof. }
  { apply test_merge_overlaps_right_proof. }
  { apply test_merge_overlaps_add_proof. }
  { apply test_merge_fail_proof. }
  { apply test_merge_map_keep_existing_proof. }
  { apply test_merge_map_replace_existing_proof. }
  { apply test_find_non_existing_proof. }
  { apply test_find_existing_proof. }
  { apply test_update_add_proof. }
  { apply test_update_merge_proof. }
  { apply test_update_delete_proof. }
  { apply test_map_proof. }
  { apply test_fold_empty_proof. }
  { apply test_fold_proof. }
  { apply test_fold_to_list_proof. }
  { apply test_map_fail_proof. }
Qed.
