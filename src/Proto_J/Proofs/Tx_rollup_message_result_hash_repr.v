Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Tx_rollup_message_result_hash_repr.

Require TezosOfOCaml.Environment.V5.Proofs.Blake2B.
Require TezosOfOCaml.Environment.V5.Proofs.Compare.
Require TezosOfOCaml.Environment.V5.Proofs.Data_encoding.

Module Tx_rollup_message_result_hash_repr.
  (* @TODO *)
  Axiom encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True) Tx_rollup_message_result_hash_repr.encoding.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Tx_rollup_message_result_hash_repr.

(** [compare] function is valid *)
Lemma compare_is_valid :
  Compare.Valid.t (fun _ => True) id Tx_rollup_message_result_hash_repr.compare.
Proof.
  apply Blake2B.Make_is_valid.
Qed.
#[global] Hint Resolve compare_is_valid : Compare_db.
