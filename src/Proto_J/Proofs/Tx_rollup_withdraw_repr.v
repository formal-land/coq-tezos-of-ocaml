Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Tx_rollup_withdraw_repr.

Require Import TezosOfOCaml.Environment.V5.Proofs.Data_encoding.
Require Import TezosOfOCaml.Environment.V5.Proofs.Signature.
Require Import TezosOfOCaml.Proto_J.Proofs.Ticket_hash_repr.
Require Import TezosOfOCaml.Proto_J.Proofs.Tx_rollup_l2_qty.

Module Valid.
  Import Tx_rollup_withdraw_repr.order.

  Record t (x : Tx_rollup_withdraw_repr.t) : Prop := {
    amount : Tx_rollup_l2_qty.Valid.t x.(amount);
  }.
End Valid.

Lemma encoding_is_valid :
  Data_encoding.Valid.t Valid.t Tx_rollup_withdraw_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
