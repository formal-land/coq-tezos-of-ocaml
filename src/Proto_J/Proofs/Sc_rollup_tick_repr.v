Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Sc_rollup_tick_repr.

Require TezosOfOCaml.Environment.V5.Proofs.Compare.
Require TezosOfOCaml.Environment.V5.Proofs.Data_encoding.

(** The Valid value of Sc_rollup_rick_repr. *)
Module Valid.
  Definition t (x : Z.t) : Prop := 0 <= x.
End Valid.

(** The encoding [Sc_rollup_rick_repr.encoding] is valid. *)
Lemma encoding_is_valid :
  Data_encoding.Valid.t Valid.t Sc_rollup_tick_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

(** [compare] function is valid *)
Lemma compare_is_valid :
  Compare.Valid.t (fun _ => True) id Sc_rollup_tick_repr.compare.
Proof. Compare.valid_auto. Qed.
#[global] Hint Resolve compare_is_valid : Compare_db.
