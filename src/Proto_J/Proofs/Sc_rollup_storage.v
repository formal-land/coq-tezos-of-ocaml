Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Sc_rollup_storage.

Require TezosOfOCaml.Environment.V5.Proofs.Pervasives.

(* @TODO this seems to be a very non-interesting property *)
Axiom originate_is_valid : forall {ctxt} {kind} {boot_sector},
  match Sc_rollup_storage.originate ctxt kind boot_sector with
  | Pervasives.Ok (_, size, _) => Pervasives.Int.Valid.non_negative size
  | Pervasives.Error _ => True
  end.
