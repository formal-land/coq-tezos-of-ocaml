Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Ticket_token.

Require TezosOfOCaml.Proto_J.Proofs.Ticket_scanner.

Lemma token_and_amount_of_ex_ticket_is_valid ex_ticket :
  Ticket_scanner.Ex_ticket.Valid.t ex_ticket ->
  let '(_, Alpha_context.Script_int.Num_tag amount) :=
      Ticket_token.token_and_amount_of_ex_ticket ex_ticket in
  0 <= amount.
  intros [? ? [? ? []] H]; apply H.
Qed.
