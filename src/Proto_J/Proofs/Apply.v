Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Apply.
Require Import TezosOfOCaml.Environment.V5.Proofs.Data_encoding.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Apply.denunciation_kind_encoding.
Proof.
    Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.