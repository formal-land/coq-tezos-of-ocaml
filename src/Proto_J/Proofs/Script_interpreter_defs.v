Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Script_interpreter_defs.

Require TezosOfOCaml.Proto_J.Proofs.Saturation_repr.
Require TezosOfOCaml.Proto_J.Proofs.Script_typed_ir.
Require TezosOfOCaml.Proto_J.Proofs.Script_ir_translator.
Require Import TezosOfOCaml.Proto_J.Simulations.Script_family.
Require Import TezosOfOCaml.Proto_J.Simulations.Script_typed_ir.
Require TezosOfOCaml.Proto_J.Simulations.Script_interpreter_defs.

(** Our definition behaves as the original OCaml code. *)
Lemma dep_cost_of_instr_eq {s f}
  (i : With_family.kinstr s f)
  (accu : With_family.stack_ty_to_dep_Set_head s)
  (stack : With_family.stack_ty_to_dep_Set_tail s) :
  Script_interpreter_defs.dep_cost_of_instr i (accu, stack) =
  Script_interpreter_defs.cost_of_instr
    (With_family.to_kinstr i)
    (With_family.to_stack_value_head accu)
    (With_family.to_stack_value_tail stack).
Proof.
  dep_destruct i;
    unfold Script_interpreter_defs.cost_of_instr; simpl;
    try rewrite cast_eval;
    cbn in *;
    unfold Script_typed_ir.comparable_ty;
    destruct_all Script_typed_ir.never;
    Tactics.destruct_pairs;
    try rewrite_cast_exists;
    try match goal with
    | s : _ |- context[cast_exists ?T _] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end;
    (* I do not know why but this [cbn] optimizes heavily
       the [reflexivity] tactic. *)
    cbn;
    reflexivity.
Qed.

(** The cost of an instruction is valid in saturated arithmetic. *)
Lemma dep_cost_of_instr_is_valid {s f}
  (i : With_family.kinstr s f)
  (dep_accu_stack : With_family.stack_ty_to_dep_Set s) :
  Saturation_repr.Valid.t
    (Script_interpreter_defs.dep_cost_of_instr i dep_accu_stack).
Proof.
  destruct i; simpl;
    apply axiom. (*TODO: fix this*)
    (* now apply Saturation_repr.Valid.decide.*)
Qed.

(** The cost of an instruction is not zero, except for a few instructions. *)
Lemma dep_cost_of_instr_is_not_zero {s f}
  (i : With_family.kinstr s f)
  (dep_accu_stack : With_family.stack_ty_to_dep_Set s) :
  Script_interpreter_defs.dep_cost_of_instr i dep_accu_stack <> 0.
Admitted.
  (* TODO: fix, currently getting universe issues *)
  (* match i with *)
  (* | With_family.IFailwith _ _ _ => True *)
  (* | _ => (dep_cost_of_instr i accu_stack) <> 0 *)
  (* end *)
  (* ******* *)
  (* String below eats about 14GB of memory and I kill it *)
  (* I'm not 100% sure but I think it started after changes I made in dep_cost_of_instr *)
  (* ******* *)
  (* destruct i, accu_stack; simpl; cbv; trivial; try congruence; *)
  (* apply axiom. (*TODO*) *)
(* Qed. *)

(** The simulation [dep_consume_instr] is valid. *)
Lemma dep_consume_instr_eq {s f}
  (local_gas_counter : Local_gas_counter.local_gas_counter)
  (i : With_family.kinstr s f)
  (accu : With_family.stack_ty_to_dep_Set_head s)
  (stack : With_family.stack_ty_to_dep_Set_tail s) :
  Script_interpreter_defs.dep_consume_instr local_gas_counter i (accu, stack) =
  Script_interpreter_defs.consume_instr
    local_gas_counter
    (With_family.to_kinstr i)
    (With_family.to_stack_value_head accu)
    (With_family.to_stack_value_tail stack).
Proof.
  unfold Script_interpreter_defs.dep_consume_instr.
  now rewrite dep_cost_of_instr_eq.
Qed.

(** The simulation [dep_kundip] is valid. *)
Fixpoint dep_kundip_eq {s z u w t}
  (w_value : With_family.stack_prefix_preservation_witness s z u w)
  (accu : With_family.stack_ty_to_dep_Set_head u)
  (stack : With_family.stack_ty_to_dep_Set_tail u)
  (k_value : With_family.kinstr w t) :
  (let '(stack, instr) :=
    Script_interpreter_defs.dep_kundip w_value (accu, stack) k_value in
  (With_family.to_stack_value stack, With_family.to_kinstr instr)) =
  Script_interpreter_defs.kundip
    (With_family.to_stack_prefix_preservation_witness w_value)
    (With_family.to_stack_value_head accu)
    (With_family.to_stack_value_tail stack)
    (With_family.to_kinstr k_value).
Proof.
  destruct w_value; simpl.
  { rewrite cast_eval; reflexivity. }
  { match goal with
    | _ : ?t1, _ : ?t2 |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_2 (T := T)
        (E1 := Stack_ty.to_Set_head u)
        (E2 := Stack_ty.to_Set_tail u)
      )
    end.
    simpl in *; Tactics.destruct_pairs; simpl.
    hauto lq: on.
  }
Qed.

(* TODO *)
(** The simulation [dep_apply] is valid. *)
Lemma dep_apply_eq {a b c : Ty.t}
  (ctxt : Local_gas_counter.outdated_context)
  (gas : Local_gas_counter.local_gas_counter)
  (capture_ty : With_family.ty a) (capture : With_family.ty_to_dep_Set a)
  (lam : With_family.lambda (Ty.Pair a b) c) :
  (let? '(lam, ctxt, gas) :=
    Script_interpreter_defs.dep_apply ctxt gas capture_ty capture lam in
  return? (With_family.to_lambda lam, ctxt, gas)) =
  Script_interpreter_defs.apply ctxt gas
    (With_family.to_ty capture_ty)
    (With_family.to_value capture)
    (With_family.to_lambda lam).
Proof.
Admitted.

(** The simulation [dep_apply] returns a valid result. *)
Lemma dep_apply_is_valid {a b c : Ty.t}
  (ctxt ctxt' : Local_gas_counter.outdated_context)
  (gas gas' : Local_gas_counter.local_gas_counter)
  (capture_ty : With_family.ty a) (capture : With_family.ty_to_dep_Set a)
  (lam : With_family.lambda (Ty.Pair a b) c)
  (lam' : With_family.lambda b c) :
  Script_typed_ir.With_family.Valid.ty capture_ty ->
  Script_typed_ir.With_family.Valid.value capture ->
  Script_typed_ir.With_family.Valid.lambda lam ->
  Script_interpreter_defs.dep_apply ctxt gas capture_ty capture lam =
    return? (lam', ctxt', gas') ->
  Script_typed_ir.With_family.Valid.lambda lam'.
Proof.
Admitted.

(** The simmulation of [dep_unpack] is valid *)
Lemma dep_unpack_eq {a}
  (ctxt : Alpha_context.context)
  (ty : With_family.ty a)
  (b : bytes) :
  (let? '(value, ctxt) := Script_interpreter_defs.dep_unpack ctxt ty b in
  return? (Option.map With_family.to_value value, ctxt)) =
  Script_interpreter_defs.unpack ctxt (With_family.to_ty ty) b.
Proof.
  unfold Script_interpreter_defs.dep_unpack.
  unfold Script_interpreter_defs.unpack.
  ez destruct Alpha_context.Gas.consume.
  destruct (_ && _); [|reflexivity].
  destruct Data_encoding.Binary.of_string_opt.
  { rewrite <- Script_ir_translator.dep_parse_data_eq.
    destruct Script_ir_translator.dep_parse_data; simpl.
    { now Tactics.destruct_pairs. }
    { now destruct Alpha_context.Gas.consume. }
  }
  { now destruct Alpha_context.Gas.consume. }
Qed.

(** The simulation [dep_unpack] only generates valid values. *)
Lemma dep_unpack_is_valid {a}
  (ctxt : Alpha_context.context)
  (ty : With_family.ty a)
  (b : bytes)
  value ctxt' :
  Script_interpreter_defs.dep_unpack ctxt ty b = return? (Some value, ctxt') ->
  Script_typed_ir.With_family.Valid.value value.
Proof.
Admitted.
