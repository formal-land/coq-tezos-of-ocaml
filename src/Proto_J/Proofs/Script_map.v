Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Script_map.

Require TezosOfOCaml.Environment.V5.Proofs.Map.
Require TezosOfOCaml.Proto_J.Proofs.Gas_comparable_input_size.
Require TezosOfOCaml.Proto_J.Proofs.Script_comparable.
Require TezosOfOCaml.Proto_J.Proofs.Script_family.
Require TezosOfOCaml.Proto_J.Proofs.Script_typed_ir.
Require Import TezosOfOCaml.Proto_J.Simulations.Script_family.
Require TezosOfOCaml.Proto_J.Simulations.Script_map.
Require Import TezosOfOCaml.Proto_J.Simulations.Script_typed_ir.

(** The simulation [dep_empty_from] is valid. *)
Lemma dep_empty_from_eq {a b c : Ty.t}
  (m : With_family.map a (With_family.ty_to_dep_Set b)) :
  With_family.to_map (
    Script_map.dep_empty_from (c := With_family.ty_to_dep_Set c) m
  ) =
  Script_map.empty_from (With_family.to_map m).
Proof.
  reflexivity.
Qed.

(** The simulation [dep_empty] is valid. *)
Lemma dep_empty_eq {a b : Ty.t} (ty : With_family.ty a) :
  Script_typed_ir.With_family.is_Comparable ty ->
  With_family.to_map (ty_v := b) (Script_map.dep_empty ty) =
  Script_map.empty (With_family.to_ty ty).
Proof.
  intros.
  unfold With_family.to_map, With_family.to_map_aux, With_family.Script_Map,
    Script_map.dep_empty, Script_map.empty.
  simpl.
  repeat f_equal;
    repeat (
      apply FunctionalExtensionality.functional_extensionality_dep; intro
    );
    try apply Gas_comparable_input_size.dep_size_of_comparable_value_eq;
    now rewrite (Script_comparable.dep_compare_comparable_eq _ ty).
Qed.

(** Like [dep_empty_eq] but here we use [to_map_aux] *)
Lemma dep_empty_eq' {a: Ty.t} {value : Set} (ty : With_family.ty a) :
  Script_typed_ir.With_family.is_Comparable ty ->
  With_family.to_map_aux 
    id (ty_k := a) (v1 := value) (Script_map.dep_empty ty) 
  =
  Script_map.empty (With_family.to_ty ty).
Proof.
  intros.
  unfold With_family.to_map_aux, With_family.Script_Map,
    Script_map.dep_empty, Script_map.empty.
  simpl.
  repeat f_equal;
    repeat (
      apply FunctionalExtensionality.functional_extensionality_dep; intro
    );
    try apply Gas_comparable_input_size.dep_size_of_comparable_value_eq;
    now rewrite (Script_comparable.dep_compare_comparable_eq _ ty).
Qed.

(** The simulation [dep_get] is valid. *)
Lemma dep_get_eq {key value : Ty.t}
  (k : With_family.ty_to_dep_Set key)
  (x : With_family.map key (With_family.ty_to_dep_Set value)) :
  (let* value := Script_map.dep_get k x in
  Some (With_family.to_value value)) =
  Script_map.get (With_family.to_value k) (With_family.to_map x).
Proof.
  unfold Script_map.dep_get, Script_map.get.
  simpl.
  unfold With_family.Script_Map.
  now rewrite Map.find_map.
Qed.

(** The simulation [dep_update] is valid. *)
Lemma dep_update_eq {a b : Ty.t}
  (k : With_family.ty_to_dep_Set a)
  (v : option (With_family.ty_to_dep_Set b))
  (x : With_family.map a (With_family.ty_to_dep_Set b)) :
  With_family.to_map (Script_map.dep_update k v x) =
  Script_map.update
    (With_family.to_value k)
    (Option.map With_family.to_value v)
    (With_family.to_map x).
Proof.
  destruct v; simpl;
    unfold With_family.to_map, With_family.to_map_aux, With_family.Script_Map;
    repeat f_equal;
    try (now rewrite Map.add_map);
    try (now rewrite Map.remove_map).
  { rewrite Map.cardinal_add_find.
    rewrite <- Map.find_map.
    hauto q: on.
  }
  { rewrite Map.cardinal_remove_find.
    rewrite <- Map.find_map.
    hauto q: on.
  }
Qed.

(** Like [dep_update_eq] but here we use [to_map_aux] *)
Lemma dep_update_eq' {a : Ty.t} {value : Set}
  (k : With_family.ty_to_dep_Set a)
  (v : option value)
  (x : With_family.map a value) :
  With_family.to_map_aux id (Script_map.dep_update k v x) =
  Script_map.update
    (With_family.to_value k)
    v
    (With_family.to_map_aux id x).
Proof.
  destruct v; simpl;
    unfold With_family.to_map, With_family.to_map_aux, With_family.Script_Map;
    repeat f_equal;
    try (now rewrite Map.add_map);
    try (now rewrite Map.remove_map).
  {
    rewrite <- Map.add_map; reflexivity.
  }
  { rewrite Map.cardinal_add_find.
    rewrite <- Map.find_map.
    hauto q: on.
  }
  { rewrite Map.cardinal_remove_find.
    rewrite <- Map.find_map.
    hauto q: on.
  }
Qed.

(** The simulation [dep_mem] is valid. *)
Lemma dep_mem_eq {elt value : Ty.t}
  (k : With_family.ty_to_dep_Set elt)
  (x : With_family.map elt (With_family.ty_to_dep_Set value)) :
  Script_map.dep_mem k x =
  Script_map.mem (With_family.to_value k) (With_family.to_map x).
Proof.
  unfold Script_map.dep_mem, Script_map.mem.
  simpl.
  unfold With_family.Script_Map.
  rewrite Map.mem_from_find.
  rewrite <- Map.find_map.
  hauto lq: on.
Qed.

(** Like [dep_mem_eq] but using [to_map_aux id] *)
Lemma dep_mem_eq' {elt : Ty.t} {value}
  (k : With_family.ty_to_dep_Set elt)
  (x : With_family.map elt value) :
  Script_map.dep_mem k x =
  Script_map.mem (With_family.to_value k) (With_family.to_map_aux id x).
Proof.
  unfold Script_map.dep_mem, Script_map.mem.
  simpl.
  unfold With_family.Script_Map.
  rewrite Map.mem_from_find.
  rewrite <- Map.find_map.
  hauto lq: on.
Qed.

(** The simulation [dep_fold] is valid. *)
Lemma dep_fold_eq {elt value : Ty.t} {acc : Set}
  (dep_f :
    With_family.ty_to_dep_Set elt -> With_family.ty_to_dep_Set value -> acc ->
    acc)
  (f : Ty.to_Set elt -> Ty.to_Set value -> acc -> acc)
  (x : With_family.map elt (With_family.ty_to_dep_Set value))
  (init : acc) :
  Script_family.Ty.is_Comparable elt ->
  (forall k v init,
    dep_f k v init = f (With_family.to_value k) (With_family.to_value v) init
  ) ->
  Script_map.dep_fold dep_f x init =
  Script_map.fold f (With_family.to_map x) init.
Proof.
  intros H_elt H_f.
  unfold Script_map.dep_fold, Script_map.fold.
  simpl.
  unfold With_family.Script_Map.
  rewrite Map.fold_map.
  f_equal.
  repeat (apply FunctionalExtensionality.functional_extensionality_dep; intro).
  match goal with
  | |- context[With_family.of_value ?v] =>
    pose proof (Script_typed_ir.With_family.to_value_of_value v H_elt)
  end.
  hauto lq: on.
Qed.

(** Like [dep_fold_eq] but here [value] is [Set] and not [Ty.t] *)
Lemma dep_fold_eq' {elt : Ty.t} {value acc : Set}
  (dep_f :
    With_family.ty_to_dep_Set elt -> value -> acc -> acc)
  (f : Ty.to_Set elt -> value -> acc -> acc)
  (x : With_family.map elt value)
  (init : acc) :
  Script_family.Ty.is_Comparable elt ->
  (forall k v init,
      dep_f k v init = f (With_family.to_value k) v init
  ) ->
  Script_map.dep_fold dep_f x init =
  Script_map.fold f (With_family.to_map_aux id x) init.
Proof.
  intros H_elt H_extensionality.
  unfold Script_map.dep_fold, Script_map.fold.
  simpl.
  unfold With_family.Script_Map; simpl.
  rewrite Map.fold_map.
  f_equal.
  repeat (apply FunctionalExtensionality.functional_extensionality_dep; intro).
  pose proof (Script_typed_ir.With_family.to_value_of_value x0 H_elt).
  step; [|easy].
  hauto lq: on.
Qed.

(** The simulation [dep_fold_es] is valid. *)
Lemma dep_fold_es_eq {elt value : Ty.t} {acc : Set}
  (dep_f :
    With_family.ty_to_dep_Set elt -> With_family.ty_to_dep_Set value -> acc ->
    M? acc)
  (f : Ty.to_Set elt -> Ty.to_Set value -> acc -> M? acc)
  (x : With_family.map elt (With_family.ty_to_dep_Set value))
  (init : acc) :
  Script_family.Ty.is_Comparable elt ->
  (forall k v init,
    dep_f k v init = f (With_family.to_value k) (With_family.to_value v) init
  ) ->
  Script_map.dep_fold_es dep_f x init =
  Script_map.fold_es f (With_family.to_map x) init.
Proof.
  intros.
  unfold Script_map.dep_fold_es.
  apply dep_fold_eq; sauto lq: on.
Qed.

(** Like [dep_fold_es_eq] but here [value] is [Set] and not [Ty.t] *)
Lemma dep_fold_es_eq' {elt : Ty.t} {value acc : Set}
  (dep_f :
    With_family.ty_to_dep_Set elt -> value -> acc -> M? acc)
  (f : Ty.to_Set elt -> value -> acc -> M? acc)
  (x : With_family.map elt value)
  (init : acc) :
  Script_family.Ty.is_Comparable elt ->
  (forall k v init,
      dep_f k v init = f (With_family.to_value k) v init
  ) ->
  Script_map.dep_fold_es dep_f x init =
  Script_map.fold_es f (With_family.to_map_aux id x) init.
Proof.
  intros.
  unfold Script_map.dep_fold_es.
  apply dep_fold_eq'; sauto lq: on.
Qed.

(** The simulation [dep_size_value] is valid. *)
Lemma dep_size_value_eq {elt value : Ty.t}
  (x : With_family.map elt (With_family.ty_to_dep_Set value)) :
  Script_map.dep_size_value x =
  Script_map.size_value (With_family.to_map x).
Proof.
  reflexivity.
Qed.

(** If [map] [k] and [v] is valid then [(With_family.Script_Map _).(S.add) k v map] is valid *)
Lemma dep_map_add (tyk tyv : Ty.t) (map : With_family.map tyk (With_family.ty_to_dep_Set tyv)) 
  (k : With_family.ty_to_dep_Set tyk)
  (v : With_family.ty_to_dep_Set tyv)
  (H_valid_map : Script_typed_ir.With_family.Valid.map map)
  (H_valid_key : Script_typed_ir.With_family.Valid.value k)
  (H_valid_value : Script_typed_ir.With_family.Valid.value v) :
  Script_typed_ir.With_family.Valid.map ((With_family.Script_Map tyk).(S.add) (With_family.to_value k) v map).
Proof.
  unfold With_family.Script_Map.
  with_strategy transparent [Map.Make] unfold Map.Make; simpl.
  induction map as [|elm map IHmap].
  { unfold Script_typed_ir.With_family.Valid.map. simpl.
    split; [apply H_valid_map|].
    { constructor; [|constructor].
      split; [|assumption].
      rewrite Script_typed_ir.With_family.of_value_to_value
        by apply H_valid_map.
      now simpl. 
    }
  }  
  { simpl. step. step.
    { unfold Script_typed_ir.With_family.Valid.map. simpl.
      split; [apply H_valid_map|].
      unfold With_family.Script_Map. 
      with_strategy transparent [Map.Make] unfold Map.Make; simpl;
        unfold Make.bindings.
      constructor.
      { split; [|assumption].
        rewrite Script_typed_ir.With_family.of_value_to_value
          by apply H_valid_map; simpl.
        easy. }
      { sauto b: on. } 
    }
    { unfold Script_typed_ir.With_family.Valid.map. simpl.
      split; [apply H_valid_map|].
      unfold With_family.Script_Map. 
      with_strategy transparent [Map.Make] unfold Map.Make; simpl;
        unfold Make.bindings. constructor.
      { split; [|assumption].
        now rewrite Script_typed_ir.With_family.of_value_to_value
          by apply H_valid_map; simpl. }
      { apply H_valid_map. }
    }
    { rewrite <- Heqp in *. unfold Script_typed_ir.With_family.Valid.map in *.
      simpl in *; split; [easy|].
      unfold With_family.Script_Map in * .
      destruct H_valid_map as [H_comparable_key H_valid_map].
      assert (H_bindings_step : forall {k v : Set} (map : list (k * v)) (elm : k * v) (Ord : COMPARABLE (t := k)),
        (Map.Make Ord).(S.bindings) (elm :: map) = elm :: (Map.Make Ord).(S.bindings) map)
        by admit.
      apply List.Forall_cons in H_valid_map.
      admit.
      (* TODO apply the IH *)
    }
Admitted.
