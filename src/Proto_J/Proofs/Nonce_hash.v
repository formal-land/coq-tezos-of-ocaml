Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Nonce_hash.

Require TezosOfOCaml.Environment.V5.Proofs.Blake2B.
Require TezosOfOCaml.Proto_J.Proofs.Path_encoding.

Lemma H_is_valid : S.HASH.Valid.t (fun _ => True) Nonce_hash.H.
  apply Blake2B.Make_is_valid.
Qed.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Nonce_hash.encoding.
  apply H_is_valid.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

(** [compare] function is valid *)
Lemma compare_is_valid :
  Compare.Valid.t (fun _ => True) id Nonce_hash.compare.
Proof.
  apply H_is_valid.
Qed.
#[global] Hint Resolve compare_is_valid : Compare_db.

Lemma Path_encoding_Make_hex_include_is_valid
  : Path_encoding.S.Valid.t Nonce_hash.Path_encoding_Make_hex_include.
  apply Path_encoding.Make_hex_is_valid.
  constructor; apply H_is_valid.
Qed.
#[global] Hint Resolve Path_encoding_Make_hex_include_is_valid : Data_encoding_db.
