Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Block_payload_hash.

Require TezosOfOCaml.Environment.V5.Proofs.Blake2B.
Require TezosOfOCaml.Environment.V5.Proofs.Compare.
Require TezosOfOCaml.Environment.V5.Proofs.Data_encoding.

Lemma Blake2B_Make_include_is_valid :
  S.HASH.Valid.t (fun _ => True) Block_payload_hash.Blake2B_Make_include.
Proof.
  apply Blake2B.Make_is_valid.
Qed.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Block_payload_hash.encoding.
  apply Blake2B_Make_include_is_valid.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

(** [compare] function is valid *)
Lemma compare_is_valid :
  Compare.Valid.t (fun _ => True) id Block_payload_hash.compare.
Proof. apply Blake2B_Make_include_is_valid. Qed.
#[global] Hint Resolve compare_is_valid : Compare_db.
