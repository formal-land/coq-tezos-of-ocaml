Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require Import TezosOfOCaml.Proto_J.Script_tc_context.

Require Import TezosOfOCaml.Proto_J.Simulations.Script_tc_context.
Require Import TezosOfOCaml.Proto_J.Simulations.Script_typed_ir.

Lemma dep_toplevel_value_eq {st pt}
  (storage : With_family.ty st)
  (param : With_family.ty pt) entrypoints
  : to_t (dep_toplevel_value storage param entrypoints)
    = toplevel_value
      (With_family.to_ty storage)
      (With_family.to_ty param)
      entrypoints.
Proof.
  reflexivity.
Qed.
