Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Script_interpreter.

Require TezosOfOCaml.Environment.V5.Proofs.List.
Require TezosOfOCaml.Proto_J.Proofs.Script_interpreter_defs.
Require TezosOfOCaml.Proto_J.Proofs.Script_map.
Require TezosOfOCaml.Proto_J.Proofs.Script_set.
Require TezosOfOCaml.Proto_J.Proofs.Script_typed_ir.
Require TezosOfOCaml.Proto_J.Proofs.Script_ir_translator.
Require TezosOfOCaml.Proto_J.Proofs.Tez_repr.
Require Import TezosOfOCaml.Proto_J.Simulations.Script_family.
Require TezosOfOCaml.Proto_J.Simulations.Script_interpreter.
Require Import TezosOfOCaml.Proto_J.Simulations.Script_typed_ir.

(** This [Opaque] seems to be necessary to terminate the evaluation. *)
Opaque Local_gas_counter.local_gas_counter_and_outdated_context.

(** This [Opaque] is mainly to clarify the output as the details of these
    functions are not really important. *)
Opaque Script_interpreter_defs.consume_instr
  Script_interpreter_defs.dep_consume_instr
  Script_interpreter_defs.consume_control.

(** Helper tactic. *)
Ltac dep_eq_case :=
  simpl;
  (
    try (
      match goal with
      | |- context[
          Script_interpreter_defs.dep_consume_instr _ _ (?accu, ?stack)
        ] =>
          rewrite (Script_interpreter_defs.dep_consume_instr_eq _ _ accu stack)
      end;
      simpl;
      destruct Script_interpreter_defs.consume_instr in |- *
    );
    try destruct (Script_interpreter_defs.consume_control _ _)
  ); [|discriminate];
  cbn in *;
  try rewrite cast_eval;
  try rewrite_cast_exists.

(** Helper tactic to apply [dep_step_eq] with the right parameters. *)
Ltac apply_dep_step_eq dep_step_eq :=
  match goal with
  | |- context [Script_interpreter.dep_step (s := ?s) _ _ _ ?i ?ks (?accu, ?stack)] =>
    match goal with
    | |- context [Script_interpreter.step _ _ ?i' ?ks' ?accu' ?stack'] =>
      replace i' with (With_family.to_kinstr i); [
      replace ks' with (With_family.to_continuation ks); [
      replace accu' with (With_family.to_stack_value_head (tys := s) accu); [
      replace stack' with (With_family.to_stack_value_tail (tys := s) stack); [
      apply dep_step_eq; simpl; try easy
      |]|]|]|];
      trivial
    end
  end.

(** Helper tactic to apply [dep_next_eq] with the right parameters. *)
Ltac apply_dep_next_eq dep_next_eq :=
  match goal with
  | |- context [Script_interpreter.dep_next (s := ?s) _ _ _ ?ks (?accu, ?stack)] =>
    match goal with
    | |- context [Script_interpreter.next _ _ ?ks' ?accu' ?stack'] =>
      replace ks' with (With_family.to_continuation ks); [
      replace accu' with (With_family.to_stack_value_head (tys := s) accu); [
      replace stack' with (With_family.to_stack_value_tail (tys := s) stack); [
      apply dep_next_eq; simpl; try easy
      |]|]|];
      trivial
    end
  end.

(** Verify that our definition [dep_step] is equivalent to the original
    function [step]. For unknown reasons, disabling the guard checking is
    needed. Maybe because of local recursive functions in the [step]
    function.

    To do the proof, we always:
    (1) start with a [grep] to explicit the instruction case we are handling;
    (2) run [dep_eq_case];
    (3) remove missing existential casts, and write the body of the proof;
    (4) run the tactics [apply_dep_step_eq dep_step_eq] or
        [apply_dep_next_eq dep_next_eq] to call the recursive hypothesis.
*)
#[bypass_check(guard)]
Fixpoint dep_step_eq {s t f}
  (fuel : nat)
  (g_value :
    Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (i_value : With_family.kinstr s t)
  (ks : With_family.continuation t f)
  (accu : With_family.stack_ty_to_dep_Set_head s)
  (stack : With_family.stack_ty_to_dep_Set_tail s)
  (dep_output_accu_stack : With_family.stack_ty_to_dep_Set f)
  (output_accu : Stack_ty.to_Set_head f)
  (output_stack : Stack_ty.to_Set_tail f)
  (dep_output_ctxt output_ctxt : Local_gas_counter.outdated_context)
  (dep_output_gas output_gas : Local_gas_counter.local_gas_counter)
  (H_i_value : Script_typed_ir.With_family.Valid.kinstr i_value)
  (H_ks : Script_typed_ir.With_family.Valid.continuation ks)
  (H_accu : Script_typed_ir.With_family.Valid.stack_value_head accu)
  (H_stack : Script_typed_ir.With_family.Valid.stack_value_tail stack)
  (H_step_constants : Script_typed_ir.Step_constants.Valid.t
     (snd g_value))
  {struct fuel} :
  Script_interpreter.dep_step
    fuel g_value gas i_value ks (accu, stack) =
    Pervasives.Ok (
      dep_output_accu_stack,
      dep_output_ctxt,
      dep_output_gas
    ) ->
  Script_interpreter.step
    g_value gas
      (With_family.to_kinstr i_value)
      (With_family.to_continuation ks)
      (With_family.to_stack_value_head accu)
      (With_family.to_stack_value_tail stack) =
    Pervasives.Ok (
      output_accu,
      output_stack,
      output_ctxt,
      output_gas
    ) ->
  With_family.to_stack_value dep_output_accu_stack =
  (output_accu, output_stack)

with dep_next_eq {s f}
  (fuel : nat)
  (g_value :
    Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (ks : With_family.continuation s f)
  (accu : With_family.stack_ty_to_dep_Set_head s)
  (stack : With_family.stack_ty_to_dep_Set_tail s)
  (dep_output_accu_stack : With_family.stack_ty_to_dep_Set f)
  (output_accu : Stack_ty.to_Set_head f)
  (output_stack : Stack_ty.to_Set_tail f)
  (dep_output_ctxt output_ctxt : Local_gas_counter.outdated_context)
  (dep_output_gas output_gas : Local_gas_counter.local_gas_counter)
  (H_ks : Script_typed_ir.With_family.Valid.continuation ks)
  (H_accu : Script_typed_ir.With_family.Valid.stack_value_head accu)
  (H_stack : Script_typed_ir.With_family.Valid.stack_value_tail stack)
  (H_step_constants : Script_typed_ir.Step_constants.Valid.t
     (snd g_value))
  {struct fuel} :
  Script_interpreter.dep_next
    fuel g_value gas ks (accu, stack) =
    Pervasives.Ok (
      dep_output_accu_stack,
      dep_output_ctxt,
      dep_output_gas
    ) ->
  Script_interpreter.next
    g_value gas
    (With_family.to_continuation ks)
    (With_family.to_stack_value_head accu)
    (With_family.to_stack_value_tail stack) =
    Pervasives.Ok (
      output_accu,
      output_stack,
      output_ctxt,
      output_gas
    ) ->
    With_family.to_stack_value dep_output_accu_stack =
    (output_accu, output_stack).
Proof.
{ destruct fuel, g_value, gas; [discriminate|]; dep_destruct i_value.
  { grep @With_family.IDrop.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IDup.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISwap.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IConst.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICons_pair.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICar.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICdr.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IUnpair.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICons_some.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICons_none.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Ty.to_Set b))
    end.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IIf_none.
    dep_eq_case.
    step;
      simpl in *; Tactics.destruct_pairs;
      apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IOpt_map.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_2 (T := T)
        (E1 := Ty.to_Set a) (E2 := Ty.to_Set b))
    end.
    step; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICons_left.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Ty.to_Set b))
    end.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICons_right.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Ty.to_Set a))
    end.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IIf_left.
    dep_eq_case.
    step; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICons_list.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    now constructor.
  }
  { grep @With_family.INil.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Ty.to_Set b))
    end.
    apply_dep_step_eq dep_step_eq.
    constructor.
  }
  { grep @With_family.IIf_cons.
    dep_eq_case.
    step;
      simpl in *; Tactics.destruct_pairs;
      inversion H_accu;
      apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IList_map.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_4 (T := T)
        (E1 := Stack_ty.to_Set_head s)
        (E2 := Stack_ty.to_Set_tail s)
        (E3 := Ty.to_Set a)
        (E4 := Ty.to_Set b)
      )
    end.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_next_eq dep_next_eq; hauto l: on.
  }
  { grep @With_family.IList_iter.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_next_eq dep_next_eq.
  }
  { grep @With_family.IList_size.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
    simpl.
    unfold Script_int_repr.N.Valid.t.
    lia.
  }
  { grep @With_family.IEmpty_set.
    with_strategy opaque [Script_set.dep_empty] dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Ty.to_Set b))
    end.
    apply_dep_step_eq dep_step_eq.
    { hauto l: on
        use: Script_typed_ir.With_family.is_Comparable_implies_family.
    }
    { now apply Script_set.dep_empty_eq. }
  }
  { grep @With_family.ISet_iter.
    with_strategy opaque [Script_set.dep_fold] dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_next_eq dep_next_eq.
    { split; [easy|].
      split; [|easy].
      unfold Script_set.dep_fold; simpl.
      with_strategy transparent [Map.Make] unfold Map.Make; simpl.
      simpl. unfold rev, rev', Make.fold; simpl; rewrite <- rev_alt.
      match goal with 
      | |- context [fold_left ?f _ _] =>
          set (fold_f := f)
      end.
      assert (H_rev_fold_left_fold_rigth_eq : forall lst,
          Lists.List.rev (fold_left fold_f nil lst) = 
          (fold_right (fun a b => fold_f b a) lst nil)).
      { intros. 
        let t := type of lst in 
        evar (e : t).
        instantiate (e := [(_, tt)]).
        intros. unfold fold_left, fold_right.
        rewrite <- fold_left_rev_right.
        assert (H_coq_of_ocaml_list : forall {a b : Set} f (l' : list a) (init : b),
          CoqOfOCaml.List.fold_right f l' init = Lists.List.fold_right f init l')
          by (induction l'; scongruence).
        rewrite H_coq_of_ocaml_list.
        induction lst; [reflexivity|].
        simpl. rewrite <- IHlst.
        match goal with 
        | |- context [Lists.List.fold_right ?f _ _] =>
            set (f' := f)
        end.
        now assert (
          Lists.List.rev (Lists.List.fold_right f' nil (Lists.List.rev lst ++ [a0])) =
          fold_f (Lists.List.rev (Lists.List.fold_right f' nil (Lists.List.rev lst))) a0) as TODO by admit.
      }
      rewrite H_rev_fold_left_fold_rigth_eq.
      subst fold_f.
      induction accu; [constructor|].
      simpl in *. step.
      destruct (With_family.of_value _) eqn:?.
      { constructor. 
        { match goal with
          | H_Forall : Forall _
            (Datatypes.fst (?k, _) :: List.map Datatypes.fst _),
            H_of_value : With_family.of_value ?k = Some _ |- _ =>
            simpl in H_Forall; apply List.Forall_head in H_Forall;
            rewrite H_of_value in H_Forall; now simpl in H_Forall
          end.
        }
        { match goal with 
          | IH : _ -> ?T,
            H_Forall : Forall _ (Datatypes.fst (_, _) :: _)  |- ?T =>
              apply IH; now apply List.Forall_cons in H_Forall
          end.
        }
      }
      { match goal with 
        | H_of_value : With_family.of_value _ = None |- _ => 
            now apply Script_typed_ir.With_family.of_value_comparable_none_neq
              in H_of_value; [|assumption]
        end. }
    }
    { rewrite Script_set.dep_fold_eq
        with (f1 := fun k l =>
          match With_family.of_value k with
          | Some k => k :: l
          | None => l
          end
        );
        try assumption.
      { unfold Script_interpreter_defs.id.
        simpl; f_equal.
        unfold List.rev, Lists.List.rev'.
        repeat rewrite <- List.rev_alt.
        unfold List.map; rewrite List.map_rev; f_equal.
        with_strategy transparent [Map.Make] unfold Map.Make; simpl.
        unfold Make.fold, List.fold_left.
        apply List.rev_ind with (l := accu); simpl; [reflexivity|].
        intros [key value] ? H_ind.
        repeat rewrite List.fold_left_app; simpl.
        rewrite <- H_ind.
        pose proof (Script_typed_ir.With_family.to_value_of_value key) as H_inv.
        destruct With_family.of_value.
        { now rewrite <- H_inv. }
        { easy. }
      }
      { now intros; rewrite Script_typed_ir.With_family.of_value_to_value. }
    }
  }
  { grep @With_family.ISet_mem.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISet_update.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    { now apply Script_set.dep_update_is_valid. }
    { apply Script_set.dep_update_eq. }
  }
  { grep @With_family.ISet_size.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    simpl.
    unfold Script_int_repr.N.Valid.t; lia.
  }
  { grep @With_family.IEmpty_map.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_2 (T := T)
        (E1 := Ty.to_Set b) (E2 := Ty.to_Set c))
    end.
    rewrite <- Script_map.dep_empty_eq by easy.
    apply_dep_step_eq dep_step_eq.
    simpl.
    hauto l: on use: Script_typed_ir.With_family.is_Comparable_implies_family.
  }
  { grep @With_family.IMap_map.
    dep_eq_case.
    fold Ty.to_Set_aux in *.
    simpl in *; Tactics.destruct_pairs.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_5 (T := T) (E5 := Ty.to_Set c))
    end.
    change (With_family.to_map_aux With_family.to_value accu) with (With_family.to_map accu).
    erewrite <- Script_map.dep_fold_eq; [|easy|admit (* TODO *)].
    epose proof Script_map.dep_empty_from_eq as H_empty_from.
    unfold With_family.to_map in H_empty_from.
    replace (With_family.to_map accu) with (With_family.to_map_aux With_family.to_value accu)
      by reflexivity.
    erewrite <- H_empty_from.
    admit.
  }
  { grep @With_family.IMap_iter.
    dep_eq_case.
    admit.
  }
  { grep @With_family.IMap_mem.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    rewrite Script_map.dep_mem_eq.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMap_get.
    dep_eq_case.
    epose proof Script_map.dep_get_eq as H_mem.
    unfold With_family.to_map in H_mem.
    rewrite <- H_mem.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    simpl.
    (* @TODO Use the fact that all the values of the map are valid to conclude
       that the found element is valid too. *)
    admit.
  }
  { grep @With_family.IMap_update.
    dep_eq_case.
    epose proof Script_map.dep_update_eq as H_update.
    unfold With_family.to_map, Option.map in H_update.
    rewrite <- H_update; clear H_update.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    simpl.
    (* @TODO Use the fact that the added element and the current elements are
       valid to conclude that the map is valid. *)
    admit.
  }
  { grep @With_family.IMap_get_and_update.
    dep_eq_case.
    epose proof Script_map.dep_update_eq as H_update.
    unfold With_family.to_map, Option.map in H_update.
    rewrite <- H_update; clear H_update.
    epose proof Script_map.dep_get_eq as H_get.
    unfold With_family.to_map in H_get.
    rewrite <- H_get; clear H_get.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq; repeat split; try assumption; simpl.
    (* @TODO Similar as previous map cases. *)
    all: admit.
  }
  { grep @With_family.IMap_size.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq; repeat split; try assumption; simpl.
    unfold Script_int_repr.N.Valid.t; lia.
  }
  { grep @With_family.IEmpty_big_map.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_2 (T := T)
        (E1 := Ty.to_Set b) (E2 := Ty.to_Set c))
    end.
    admit.
  }
  { grep @With_family.IBig_map_mem.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    admit.
  }
  { grep @With_family.IBig_map_get.
    dep_eq_case.
    (* TODO: remove axiom *)
    admit.
  }
  { grep @With_family.IBig_map_update.
    dep_eq_case.
    (* TODO: remove axiom *)
    admit.
  }
  { grep @With_family.IBig_map_get_and_update.
    dep_eq_case.
    (* TODO: remove axiom *)
    admit.
  }
  { grep @With_family.IConcat_string.
    dep_eq_case.
    unfold Alpha_context.Script_string.concat.
    destruct accu; simpl.
    match goal with
    | |- context [List.map ?f elements] => replace (List.map f elements) with elements
      by (induction elements; hauto q: on)
    end.
    do 2 match goal with
    | |- context [fold_left ?f ?acc ?l] =>
        let name := fresh "fold" in 
        set (name := fold_left f acc l)
    end.
    replace fold with fold0 by
      (subst fold0 fold;
      induction elements; [easy|simpl];
      hauto lq: on).
    set (local_gas := Local_gas_counter.consume _ _).
    destruct local_gas; [simpl|discriminate].
    set (string_tag := Alpha_context.Script_string.String_tag _).
    destruct string_tag.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IConcat_string_pair.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISlice_string.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct accu; simpl.
    unfold of_int.
    destruct_all Script_int_repr.num; simpl.
    match goal with
    | |- context [if ?e then _ else _] =>
        destruct e eqn:H_cond
    end; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IString_size.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq; repeat split; try assumption; simpl.
    unfold Script_int_repr.N.Valid.t; lia.
  }
  { grep @With_family.IConcat_bytes.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct accu; simpl.
    match goal with
    | |- context [List.map ?f elements] => replace (List.map f elements) with elements
      by (induction elements; hauto q: on)
    end.
    do 2 match goal with
    | |- context [fold_left ?f ?acc ?l] =>
        let name := fresh "fold" in 
        set (name := fold_left f acc l)
    end.
    replace fold with fold0 by
      (subst fold0 fold;
      induction elements; [easy|simpl];
      hauto lq: on).
    set (local_gas := Local_gas_counter.consume _ _).
    destruct local_gas; [simpl|discriminate].
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IConcat_bytes_pair.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISlice_bytes.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    destruct accu; simpl.
    unfold of_int.
    destruct_all Script_int_repr.num; simpl.
    match goal with
    | |- context [if ?e then _ else _] =>
        destruct e eqn:H_cond
    end; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IBytes_size.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq; simpl.
    unfold Script_int_repr.N.Valid.t; lia.
  }
  { grep @With_family.IAdd_seconds_to_timestamp.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAdd_timestamp_to_seconds.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISub_timestamp_seconds.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IDiff_timestamps.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAdd_tez.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    simpl; step; simpl; [|discriminate].
    apply_dep_step_eq dep_step_eq; repeat split; try assumption; simpl.
    match goal with
    | H_eq : Tez_repr.op_plusquestion ?t1 ?t2 = _ |- _ =>
      pose proof (Tez_repr.op_plusquestion_is_valid (t1 := t1) (t2 := t2))
        as H_tez_valid;
      rewrite H_eq in H_tez_valid
    end.
    hauto drew: off.
  }
  { grep @With_family.ISub_tez.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    unfold Alpha_context.Tez.sub_opt.
    destruct_all Tez_repr.t; simpl.
    destruct (_ <=? _) eqn:?; apply_dep_step_eq dep_step_eq; simpl.
    lia.
  }
  { grep @With_family.ISub_tez_legacy.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    simpl; step; simpl; [|discriminate].
    apply_dep_step_eq dep_step_eq.
    simpl.
    match goal with
    | H : Tez_repr.op_minusquestion ?t1 ?t2 = _ |- _ =>
      pose proof (@Tez_repr.op_minusquestion_is_valid t1 t2) as H_valid;
      rewrite H in H_valid
    end.
    hauto lq: on.
  }
  { grep @With_family.IMul_teznat.
    dep_eq_case; cbn in *.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      erewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    Tactics.destruct_pairs. simpl.
    destruct (Alpha_context.Script_int.to_int64 _) eqn:H_to_int64; [|sauto q:on].
    destruct (Alpha_context.Tez.op_starquestion _ _) eqn:H_op_starquestion; [|scongruence].
    simpl.
    apply_dep_step_eq dep_step_eq.
    revert H_to_int64.
    unfold Alpha_context.Script_int.to_int64.
    unfold Option.catch.
    step.
    unfold to_int64.
    destruct (_ && _)%bool eqn:?;
      [rewrite Option.catch_no_errors | rewrite Option.catch_raise];
      [|discriminate].
    intro H_eq_Some.
    match goal with
    | H : context[Tez_repr.op_starquestion ?a ?b] |- _ =>
      pose proof (Tez_repr.op_starquestion_is_valid a b) as H_star_valid;
      rewrite H in H_star_valid
    end.
    apply H_star_valid.
    { lia. }
    { symmetry in H_eq_Some; inversion_clear H_eq_Some.
      lia.
    }
  }
  { grep @With_family.IMul_nattez.
    dep_eq_case.
    step.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      erewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    destruct (Alpha_context.Script_int.to_int64 _) eqn:H_to_int64; [|discriminate]; simpl.
    destruct (Alpha_context.Tez.op_starquestion _ _) eqn:H_op_starquestion; [|discriminate]; simpl.
    apply_dep_step_eq dep_step_eq.
    revert H_to_int64.
    unfold Alpha_context.Script_int.to_int64.
    unfold Option.catch.
    step.
    unfold to_int64.
    destruct (_ && _)%bool eqn:?;
      [rewrite Option.catch_no_errors | rewrite Option.catch_raise];
      [|discriminate].
    intro H_eq_Some.
    match goal with
    | H : context[Tez_repr.op_starquestion ?a ?b] |- _ =>
      pose proof (Tez_repr.op_starquestion_is_valid a b) as H_star_valid;
      rewrite H in H_star_valid
    end.
    apply H_star_valid.
    { easy. }
    { symmetry in H_eq_Some; inversion_clear H_eq_Some.
      lia.
    }
  }
  { grep @With_family.IEdiv_teznat.
    admit. (* We verify this property for the version K of the protocol. *)
  }
  { grep @With_family.IEdiv_tez.
    admit. (* We verify this property for the version K of the protocol. *)
  }
  { grep @With_family.IOr.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAnd.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IXor.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct_all bool; simpl; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.INot.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IIs_nat.
    dep_eq_case.
    destruct Script_int_repr.is_nat; apply_dep_step_eq dep_step_eq.
    admit.
  }
  { grep @With_family.INeg.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAbs_int.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
    admit.
  }
  { grep @With_family.IInt_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct_all Script_int_repr.num; simpl.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAdd_int.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAdd_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    admit.
  }
  { grep @With_family.ISub_int.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMul_int.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMul_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    admit.
  }
  { grep @With_family.IEdiv_int.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct Script_int_repr.ediv;
      simpl in *; Tactics.destruct_pairs;
      apply_dep_step_eq dep_step_eq.
    admit.
  }
  { grep @With_family.IEdiv_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct Script_int_repr.ediv_n;
      simpl in *; Tactics.destruct_pairs;
      apply_dep_step_eq dep_step_eq.
    admit.
  }
  { grep @With_family.ILsl_nat.
    dep_eq_case.
    admit.
  }
  { grep @With_family.ILsr_nat.
    dep_eq_case.
    admit.
  }
  { grep @With_family.IOr_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    admit.
  }
  { grep @With_family.IAnd_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    admit.
  }
  { grep @With_family.IAnd_int_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    admit.
  }
  { grep @With_family.IXor_nat.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    admit.
  }
  { grep @With_family.INot_int.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IIf.
    dep_eq_case.
    repeat step;
      simpl in *; Tactics.destruct_pairs;
      apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ILoop.
    dep_eq_case.
    apply_dep_next_eq dep_next_eq.
  }
  { grep @With_family.ILoop_left.
    dep_eq_case.
    apply_dep_next_eq dep_next_eq.
  }
  { grep @With_family.IDip.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IExec.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    match goal with
    | code : With_family.kdescr.skeleton _ _ _ _ |- _ =>
      destruct code; simpl
    end.
    apply_dep_step_eq dep_step_eq; hauto l: on.
  }
  { grep @With_family.IApply.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Script_interpreter_defs.dep_apply_eq.
    destruct Script_interpreter_defs.dep_apply eqn:H_apply; [|discriminate].
    simpl in *; Tactics.destruct_pairs.
    simpl.
    apply_dep_step_eq dep_step_eq.
    now eapply Script_interpreter_defs.dep_apply_is_valid; try apply H_apply.
  }
  { grep @With_family.ILambda.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IFailwith.
    dep_eq_case.
    destruct trace_value; simpl; Tactics.destruct_pairs; discriminate.
  }
  { grep @With_family.ICompare.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    rewrite <- Script_comparable.dep_compare_comparable_eq by admit.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IEq.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.INeq.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ILt.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IGt.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ILe.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IGe.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAddress.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct_all Script_typed_ir.typed_contract.
    step; simpl.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IContract.
    dep_eq_case.
    simpl in *.
    destruct (String.compare _ _ =? 0);
      [|destruct (String.compare _ _ =? 0); [|apply_dep_step_eq dep_step_eq]];
      destruct k;
    simpl in *; Tactics.destruct_pairs;
    rewrite <- Script_ir_translator.dep_parse_contract_for_script_eq by easy;
    step_let?; destruct p;
    destruct (_ c) eqn:H_gas_counter; simpl;
    rewrite H_gas_counter;
    apply_dep_step_eq dep_step_eq;
    step; trivial; step;
    now apply Script_ir_translator.dep_parse_contract_for_script_is_valid in Heqt0.
  }
  { grep @With_family.IView.
    dep_eq_case.
    admit.
  }
  { grep @With_family.ITransfer_tokens.
    dep_eq_case.
    admit.
  }
  { grep @With_family.IImplicit_account.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ICreate_contract.
    dep_eq_case.
    admit.
  }
  { grep @With_family.ISet_delegate.
    dep_eq_case.
    destruct Alpha_context.fresh_internal_nonce; simpl; [|discriminate].
    simpl in *; Tactics.destruct_pairs.
    destruct Local_gas_counter.local_gas_counter_and_outdated_context.
    destruct accu; simpl; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.INow.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMin_block_time.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    admit.
  }
  { grep @With_family.IBalance.
    dep_eq_case.
    destruct Local_gas_counter.local_gas_counter_and_outdated_context.
    apply_dep_step_eq dep_step_eq.
    apply H_step_constants.
  }
  { grep @With_family.ILevel.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    apply H_step_constants.
  }
  { grep @With_family.ICheck_signature.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IHash_key.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IPack.
    dep_eq_case.
    do 2 match goal with
    | |- context [Local_gas_counter.use_gas_counter_in_context _ _ ?f'] =>
        let gas_counter_f := fresh "gas_counter_f" in
        set (gas_counter_f := f'); intros H1 H2; revert H2 H1
    end.
    assert (H_gas_counter_eq : gas_counter_f0 = gas_counter_f).
    { subst gas_counter_f0 gas_counter_f.
      apply FunctionalExtensionality.functional_extensionality; intro.
      now rewrite Script_ir_translator.dep_pack_data_eq. 
    }
    rewrite H_gas_counter_eq.
    destruct (Local_gas_counter.use_gas_counter_in_context _ _ _);
      [simpl|discriminate];
      Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IUnpack.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Ty.to_Set a))
    end.
    unfold Local_gas_counter.use_gas_counter_in_context.
    rewrite <- Script_interpreter_defs.dep_unpack_eq.
    destruct Script_interpreter_defs.dep_unpack eqn:H_unpack_eq; simpl;
      [|discriminate].
    Tactics.destruct_pairs.
    cbn - [Script_interpreter.step].
    apply_dep_step_eq dep_step_eq.
    step; [|trivial].
    apply (Script_interpreter_defs.dep_unpack_is_valid _ _ _ _ _ H_unpack_eq).
  }
  { grep @With_family.IBlake2b.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISha256.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISha512.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISource.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISender.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISelf.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISelf_address.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAmount.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    apply H_step_constants.
  }
  { grep @With_family.ISapling_empty_state.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISapling_verify_update.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct Alpha_context.Sapling.verify_update; simpl; [|discriminate].
    Tactics.destruct_pairs.
    destruct Local_gas_counter.local_gas_counter_and_outdated_context.
    step; Tactics.destruct_pairs; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISapling_verify_update_deprecated.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    admit.
  }
  { grep @With_family.IDig.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      erewrite (cast_exists_eval_5 (T := T))
    end.
    admit.
  }
  { grep @With_family.IDug.
    dep_eq_case.
    admit.
  }
  { grep @With_family.IDipn.
    dep_eq_case.
    admit.
  }
  { grep @With_family.IDropn.
    dep_eq_case.
    admit.
  }
  { grep @With_family.IChainId.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.INever.
    dep_eq_case.
    step.
  }
  { grep @With_family.IVoting_power.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct Alpha_context.Vote.get_voting_power eqn:?; simpl; [|discriminate].
    Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    unfold Script_int_repr.N.Valid.t. lia.
  }
  { grep @With_family.ITotal_voting_power.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    destruct Alpha_context.Vote.get_total_voting_power; simpl; [|discriminate].
    Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    unfold Script_int_repr.N.Valid.t. lia.
  }
  { grep @With_family.IKeccak.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.ISha3.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAdd_bls12_381_g1.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAdd_bls12_381_g2.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IAdd_bls12_381_fr.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMul_bls12_381_g1.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMul_bls12_381_g2.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Stack_ty.to_Set s))
    end.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMul_bls12_381_fr.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMul_bls12_381_z_fr.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IMul_bls12_381_fr_z.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IInt_bls12_381_fr.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.INeg_bls12_381_g1.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.INeg_bls12_381_g2.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.INeg_bls12_381_fr.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IPairing_check_bls12_381.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    set (elements := accu.(Script_typed_ir.boxed_list.elements)).
    replace (List.map _ _) with elements by (induction elements; hauto q: on).
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IComb.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      erewrite (cast_exists_eval_2 (T := T))
    end.
    match goal with
    | |- context[let '(a,b) := (?e _ _ _ _) in
                   Script_interpreter.step _ _ _ _ a b] =>
      set (aux := e)
    end.
    match goal with
    | |- context[Script_interpreter.dep_step _ _ _ _ _ ?v] =>
      match goal with
      | |- context[let '_ := ?a in _] =>
        assert (With_family.to_stack_value v = a) as Hto_stack_value
      end
    end.
    { clear. induction c; cbn; [ now rewrite cast_eval |]. step.
      match goal with
      | |- context[cast_exists ?T ?e] =>
        erewrite (cast_exists_eval_4 (T := T) (x := e))
      end.
      rewrite <- IHc. destruct stack. simpl. rewrite Heqs0.
      now repeat rewrite cast_eval.
    }
    { simpl in *.
      rewrite <- Hto_stack_value.
      match goal with
      | |- context[Script_interpreter.dep_step _ _ _ _ _ ?v] =>
        assert (Script_typed_ir.With_family.Valid.stack_value v)
          as Hvalid_stack_value
      end.
      { revert H_accu H_stack. clear.
        induction c; cbn in *; hauto lq: on.
      }
      destruct Script_interpreter.eval_comb_gadt_witness.
      apply_dep_step_eq dep_step_eq; hauto q: on.
    }
  }
  { grep @With_family.IUncomb.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      erewrite (cast_exists_eval_2 (T := T))
    end.
    match goal with
    | |- context[let '(a,b) := (?e _ _ _ _) in
                   Script_interpreter.step _ _ _ _ a b] =>
      set (aux := e)
    end.
    match goal with
    | |- context[Script_interpreter.dep_step _ _ _ _ _ ?v] =>
      match goal with
      | |- context[let '_ := ?a in _] =>
        assert (With_family.to_stack_value v = a) as Hto_stack_value
      end
    end.
    { clear. induction u0; cbn; [ now rewrite cast_eval |]. step.
      match goal with
      | |- context[cast_exists ?T ?e] =>
        erewrite (cast_exists_eval_4 (T := T) (x := e))
      end.
      simpl in *. rewrite <- IHu0. destruct stack. now rewrite cast_eval.
    }
    { simpl in *.
      rewrite <- Hto_stack_value.
      match goal with
      | |- context[Script_interpreter.dep_step _ _ _ _ _ ?v] =>
        assert (Script_typed_ir.With_family.Valid.stack_value v)
          as Hvalid_stack_value
      end.
      { revert H_accu H_stack. clear.
        induction u0; cbn in *; [ hauto lq: on |]. intros H_accu H_stack.
        destruct accu. specialize IHu0 with t0 stack.
        destruct Script_interpreter.eval_uncomb_gadt_witness. hauto lq: on.
      }
      { destruct Script_interpreter.eval_uncomb_gadt_witness.
        apply_dep_step_eq dep_step_eq; hauto q: on.
      }
    }
  }
  { grep @With_family.IComb_get.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      erewrite (cast_exists_eval_1 (T := T))
    end.
    match goal with
    | |- context[Script_interpreter.step _ _ _ _ (?e _ _ _ _)] =>
      set (aux := e)
    end.
    match goal with
    | |- context[Script_interpreter.dep_step _ _ _ _ _ (?v, _)] =>
      match goal with
      | |- context[Script_interpreter.step _ _ _ _ ?a _] =>
        assert (@With_family.to_stack_value_head (_ :: s) v = a)
          as Hto_stack_value
      end
    end.
    { clear. induction c; cbn; [ now rewrite cast_eval | |]; destruct accu.
      { now match goal with
            | |- context[cast_exists ?T ?e] =>
              erewrite (cast_exists_eval_1 (T := T))
            end.
      }
      { match goal with
        | |- context[cast_exists ?T ?e] =>
          rewrite (cast_exists_eval_2 (T := T))
        end.
        apply IHc.
      }
    }
    { rewrite <- Hto_stack_value. simpl. apply_dep_step_eq dep_step_eq.
      revert H_accu. clear. intros H_accu.
      induction c; cbn; trivial; destruct accu; [| apply IHc ]; hauto lq: on.
    }
  }
  { grep @With_family.IComb_set.
    dep_eq_case. destruct stack. cbn.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      erewrite (cast_exists_eval_3 (T := T))
    end.
    match goal with
    | |- context[Script_interpreter.step _ _ _ _ (?e _ _ _ _ _ _)] =>
      set (aux := e)
    end.
    match goal with
    | |- context[Script_interpreter.dep_step _ _ _ _ _ (?v, _)] =>
      match goal with
      | |- context[Script_interpreter.step _ _ _ _ ?a _] =>
        assert (@With_family.to_stack_value_head (_ :: s) v = a)
          as Hto_stack_value
      end
    end.
    { clear. induction c0; cbn; [ now rewrite cast_eval | |]; destruct t.
      { match goal with
        | |- context[cast_exists ?T ?e] =>
          erewrite (cast_exists_eval_2 (T := T))
        end.
        symmetry. apply cast_eval.
      }
      { match goal with
        | |- context[cast_exists ?T ?e] =>
          erewrite (cast_exists_eval_3 (T := T))
        end.
        rewrite cast_eval. f_equal. apply IHc0.
      }
    }
    { rewrite <- Hto_stack_value. simpl. apply_dep_step_eq dep_step_eq.
      revert H_accu H_stack. clear. intros H_accu H_stack.
      induction c0; cbn; trivial; hauto lq: on.
    }
  }
  { grep @With_family.IDup_n.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      erewrite (cast_exists_eval_1 (T := T))
    end.
    apply_dep_step_eq dep_step_eq.
    { revert H_accu H_stack. clear. induction d; cbn.
      { intuition. }
      { destruct stack. intros. apply IHd; intuition. }
    }
    { clear. induction d; cbn.
      { now match goal with
            | |- context[cast_exists ?T ?e] =>
              erewrite (cast_exists_eval_1 (T := T))
            end.
      }
      { match goal with
        | |- context[cast_exists ?T ?e] =>
          erewrite (cast_exists_eval_2 (T := T))
        end.
        destruct stack. simpl. apply IHd.
      }
    }
  }
  { grep @With_family.ITicket.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_step_eq dep_step_eq.
    split; [now destruct_all Script_int_repr.num|apply H_accu].
  }
  { grep @With_family.IRead_ticket.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
    split; [constructor|].
    destruct H_accu as [H_accu H_accu'].
    split; [apply H_accu'|].
    destruct_all (Script_typed_ir.ticket (With_family.ty_to_dep_Set a)); simpl in * |-.
    destruct_all Alpha_context.Script_int.num.
    lia.
  }
  { grep @With_family.ISplit_ticket.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    simpl.
    destruct (_ =? _); apply_dep_step_eq dep_step_eq.
    do 2 split; try easy;
    destruct_all Alpha_context.Script_int.num; lia.
  }
  { grep @With_family.IJoin_tickets.
    dep_eq_case.
    match goal with
    | |- context[cast_exists ?T ?e] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := Ty.to_Set a))
    end.
    simpl in *; Tactics.destruct_pairs.
    simpl.
    rewrite <- Script_comparable.dep_compare_comparable_eq by easy.
    repeat (step; simpl); apply_dep_step_eq dep_step_eq;
     split; try easy;
      destruct_all (Script_typed_ir.ticket (With_family.ty_to_dep_Set a)); simpl in *;
      destruct_all Alpha_context.Script_int.num; simpl in *; lia.
  }
  { grep @With_family.IOpen_chest.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    simpl.
    repeat step; apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.IHalt.
    dep_eq_case.
    apply_dep_next_eq dep_next_eq.
  }
}
{ destruct fuel, g_value, gas; [discriminate|];
    dep_destruct ks.
  { grep @With_family.KNil.
    dep_eq_case.
    destruct dep_output_accu_stack; simpl.
    congruence.
  }
  { grep @With_family.KCons.
    dep_eq_case.
    apply_dep_step_eq dep_step_eq.
  }
  { grep @With_family.KReturn.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_next_eq dep_next_eq.
  }
  { grep @With_family.KMap_head.
    dep_eq_case.
    apply_dep_next_eq dep_next_eq.
  }
  { grep @With_family.KUndip.
    dep_eq_case.
    apply_dep_next_eq dep_next_eq.
  }
  { grep @With_family.KLoop_in.
    dep_eq_case.
    now destruct_all bool;
      simpl in *; Tactics.destruct_pairs;
      [apply_dep_step_eq dep_step_eq | apply_dep_next_eq dep_next_eq].
  }
  { grep @With_family.KLoop_in_left.
    dep_eq_case.
    now step; [apply_dep_step_eq dep_step_eq | apply_dep_next_eq dep_next_eq].
  }
  { grep @With_family.KIter.
    dep_eq_case.
    step; simpl; [apply_dep_next_eq dep_next_eq | apply_dep_step_eq dep_step_eq]; sauto l: on.
  }
  { grep @With_family.KList_enter_body.
    dep_eq_case.
    destruct l; simpl.
    { rewrite List.rev_map_eq.
      apply_dep_next_eq dep_next_eq; try easy.
      apply List.Forall_P_rev.
      now destruct H_ks as [_ [_ [H_ks _]]].
    }
    { apply_dep_step_eq dep_step_eq. 
      { destruct H_ks as [H_ks1 [H_ks2 [H_ks3 H_ks4]]].
        now apply List.Forall_cons in H_ks2. }
      { destruct H_ks as [_ [H_ks [_ _]]].
        now apply List.Forall_head in H_ks. }
    }
  }
  { grep @With_family.KList_exit_body.
    dep_eq_case.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_next_eq dep_next_eq; hauto l: on.
  }
  { grep @With_family.KMap_enter_body.
    dep_eq_case.
    step; simpl; Tactics.destruct_pairs;
      [apply_dep_next_eq dep_next_eq|].
    apply_dep_step_eq dep_step_eq;
      [|now apply List.Forall_head in H0].
    split; [easy|].
    let t := type of H0 in assert (H0' : t) by assumption.
    apply List.Forall_cons in H0.
    apply List.Forall_head in H0'.
    split; [easy|].
    split; [easy|]. 
    split; [|easy].
    apply H0'. 
  }
  { grep @With_family.KMap_exit_body.
    dep_eq_case.
    match goal with
    | |- context[
        Script_map.update
          (With_family.to_value ?k)
          (Some (With_family.to_value ?v))
          (With_family.to_map ?m)
      ] =>
      pose proof (Script_map.dep_update_eq k (Some v) m) as H_update
    end.
    with_strategy opaque [Script_map.update] simpl in H_update.
    rewrite <- H_update.
    simpl in *; Tactics.destruct_pairs.
    apply_dep_next_eq dep_next_eq; try easy.
    split; [easy|].
    split; [easy|].
    split; [|easy].
    now apply Script_map.dep_map_add.
  }
  { grep @With_family.KView_exit.
    dep_eq_case.
    apply_dep_next_eq dep_next_eq.
  }
}
Admitted.
