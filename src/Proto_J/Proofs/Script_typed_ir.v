Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require Import TezosOfOCaml.Proto_J.Script_typed_ir.

Require TezosOfOCaml.Environment.V5.Proofs.Chain_id.
Require TezosOfOCaml.Environment.V5.Proofs.Data_encoding.
Require TezosOfOCaml.Environment.V5.Proofs.Signature.
Require TezosOfOCaml.Environment.V5.Proofs.Timelock.
Require TezosOfOCaml.Proto_J.Proofs.Indexable.
Require TezosOfOCaml.Proto_J.Proofs.Sapling_repr.
Require TezosOfOCaml.Proto_J.Proofs.Script_family.
Require TezosOfOCaml.Proto_J.Proofs.Script_int_repr.
Require Import TezosOfOCaml.Proto_J.Simulations.Script_family.
Require Import TezosOfOCaml.Proto_J.Simulations.Script_typed_ir.

Module Script_signature.
  Import Proto_J.Script_typed_ir.Script_signature.

  Lemma make_get (s : t) : make (get s) = s.
  Proof.
    now destruct s.
  Qed.

  Lemma get_make (s : Signature.t) : get (make s) = s.
  Proof.
    reflexivity.
  Qed.

  Lemma encoding_is_valid : Data_encoding.Valid.t (fun _ => True) encoding.
  Proof.
    Data_encoding.Valid.data_encoding_auto.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.

  Definition canonize (s : t) : t :=
    make (Signature.canonize (get s)).

  Lemma compare_is_valid : Compare.Valid.t (fun _ => True) canonize compare.
  Proof.
    apply (Compare.equality (Compare.projection get Signature.compare));
      [sauto lq: on|].
    eapply Compare.implies.
    { eapply Compare.projection_is_valid.
      apply Signature.compare_is_valid.
    }
    all: unfold canonize, get; hauto l: on.
  Qed.
End Script_signature.

Module Script_chain_id.
  Import Proto_J.Script_typed_ir.Script_chain_id.

  Lemma compare_is_valid : Compare.Valid.t (fun _ => True) id compare.
  Proof.
    apply (Compare.equality (
      let proj '(Chain_id_tag x) := x in
      Compare.projection proj Chain_id.compare
    )); [sauto q: on|].
    eapply Compare.implies.
    { eapply Compare.projection_is_valid.
      apply Chain_id.compare_is_valid.
    }
    all: sauto q: on.
  Qed.

  Lemma encoding_is_valid : Data_encoding.Valid.t (fun _ => True) encoding.
  Proof.
    Data_encoding.Valid.data_encoding_auto.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Script_chain_id.

Module Script_timelock.
  Import Proto_J.Script_typed_ir.Script_timelock.

  Lemma chest_key_encoding_is_valid : Data_encoding.Valid.t (fun _ => True) chest_key_encoding.
  Proof.
    Data_encoding.Valid.data_encoding_auto.
  Qed.   
  #[global] Hint Resolve chest_key_encoding_is_valid : Data_encoding_db.

  Lemma chest_encoding_is_valid : Data_encoding.Valid.t (fun _ => True) chest_encoding.
  Proof.
    Data_encoding.Valid.data_encoding_auto.
  Qed.   
  #[global] Hint Resolve chest_encoding_is_valid : Data_encoding_db.
  
End Script_timelock.  

Module Step_constants.
  Module Valid.
    (** The validity of the step_constants *)
    Record t (sc : step_constants) : Prop := {
      amount : Tez_repr.Valid.t sc.(step_constants.amount);
      balance : Tez_repr.Valid.t sc.(step_constants.balance);
      level : Script_int_repr.N.Valid.t sc.(step_constants.level);
    }.
  End Valid.
End Step_constants.

Module Ticket.
  Module Valid.
    Import Script_typed_ir.ticket.

    Record t {a : Set} (x : Script_typed_ir.ticket a) : Prop := {
      amount : 0 <= let 'Alpha_context.Script_int.Num_tag x' := x.(amount) in x';
    }.
  End Valid.
End Ticket.

Module With_family.
  (** If a type is comparable. *)
  Fixpoint is_Comparable {t} (ty : With_family.ty t) : Prop :=
    match ty with
    | With_family.Unit_t | With_family.Int_t | With_family.Nat_t
    | With_family.Signature_t | With_family.String_t | With_family.Bytes_t
    | With_family.Mutez_t | With_family.Key_hash_t | With_family.Key_t
    | With_family.Timestamp_t | With_family.Address_t | With_family.Tx_rollup_l2_address_t
    | With_family.Bool_t | With_family.Chain_id_t | With_family.Never_t => True
    | With_family.Pair_t ty1 ty2 meta Dependent_bool.YesYes =>
        is_Comparable ty1 /\ is_Comparable ty2
    | With_family.Union_t ty1 ty2 meta Dependent_bool.YesYes =>
        is_Comparable ty1 /\ is_Comparable ty2
    | With_family.Option_t ty meta Dependent_bool.Yes => is_Comparable ty
    | _ => False
    end.

  (** When a type is comparable then its family too. *)
  Fixpoint is_Comparable_implies_family {t : Ty.t} (ty : With_family.ty t) :
    is_Comparable ty ->
    Script_family.Ty.is_Comparable t.
  Proof.
    intros; destruct ty; simpl in *;
      try easy;
      hauto q: on.
  Qed.

  (** The validity of the various Michelson types. *)
  Module Valid.
    (** The validity of [ty]. *)
    Fixpoint ty {a : Ty.t} (t : With_family.ty a) : Prop :=
      match t with
      | With_family.Unit_t => True
      | With_family.Int_t => True
      | With_family.Nat_t => True
      | With_family.Signature_t => True
      | With_family.String_t => True
      | With_family.Bytes_t => True
      | With_family.Mutez_t => True
      | With_family.Key_hash_t => True
      | With_family.Key_t => True
      | With_family.Timestamp_t => True
      | With_family.Address_t => True
      | With_family.Tx_rollup_l2_address_t => True
      | With_family.Bool_t => True
      | With_family.Pair_t t1 t2 _ _ => ty t1 /\ ty t2
      | With_family.Union_t t1 t2 _ _ => ty t1 /\ ty t2
      | With_family.Lambda_t arg ret _ => ty arg /\ ty ret
      | With_family.Option_t t _ _ => ty t
      | With_family.List_t t _ => ty t
      | With_family.Set_t t _ => With_family.is_Comparable t /\ ty t
      | With_family.Map_t key value _ =>
        With_family.is_Comparable key /\ ty key /\ ty value
      | With_family.Big_map_t key value _ =>
        With_family.is_Comparable key /\ ty key /\ ty value
      | With_family.Contract_t arg _ => ty arg
      | With_family.Sapling_transaction_t _ => True
      | With_family.Sapling_transaction_deprecated_t _ => True
      | With_family.Sapling_state_t _ => True
      | With_family.Operation_t => True
      | With_family.Chain_id_t => True
      | With_family.Never_t => True
      | With_family.Bls12_381_g1_t => True
      | With_family.Bls12_381_g2_t => True
      | With_family.Bls12_381_fr_t => True
      | With_family.Ticket_t t _ => With_family.is_Comparable t /\ ty t
      | With_family.Chest_key_t => True
      | With_family.Chest_t => True
      end.

    (** The validity of [stack_ty]. *)
    Fixpoint stack_ty {s} (stack : With_family.stack_ty s) : Prop :=
      match stack with
      | With_family.Item_t t rest => ty t /\ stack_ty rest
      | With_family.Bot_t => True
      end.

    (** The validity of [kinfo]. *)
    Definition kinfo {s} (info : With_family.kinfo s) : Prop :=
      let 'With_family.Kinfo _ kstack_ty := info in
      stack_ty kstack_ty.

    (** The validity of [view_signature]. *)
    Definition view_signature {a b}
      (signature : With_family.view_signature a b) : Prop :=
      let 'With_family.View_signature _ i_ty o_ty := signature in
      ty i_ty /\ ty o_ty.

    (** The validity of [kinstr]. *)
    #[bypass_check(guard)]
    Fixpoint kinstr {s f} (i : With_family.kinstr s f) {struct i} : Prop :=
      match i with
      (*
        Stack
        -----
      *)
      | With_family.IDrop info k =>
          kinfo info /\ kinstr k
      | With_family.IDup info k =>
          kinfo info /\ kinstr k
      | With_family.ISwap info k =>
          kinfo info /\ kinstr k
      | With_family.IConst info v k =>
          kinfo info /\ value v /\ kinstr k
      (*
        Pairs
        -----
      *)
      | With_family.ICons_pair info k =>
          kinfo info /\ kinstr k
      | With_family.ICar info k =>
          kinfo info /\ kinstr k
      | With_family.ICdr info k =>
          kinfo info /\ kinstr k
      | With_family.IUnpair info k =>
          kinfo info /\ kinstr k
      (*
        Options
        -------
      *)
      | With_family.ICons_some info k =>
          kinfo info /\ kinstr k
      | With_family.ICons_none info k =>
          kinfo info /\ kinstr k
      | With_family.IIf_none info branch_if_none branch_if_some k =>
          kinfo info /\ kinstr branch_if_none /\ kinstr branch_if_some /\ kinstr k
      | With_family.IOpt_map info body k =>
          kinfo info /\ kinstr body /\ kinstr k
      (*
        Unions
        ------
      *)
      | With_family.ICons_left info k =>
          kinfo info /\ kinstr k
      | With_family.ICons_right info k =>
          kinfo info /\ kinstr k
      | With_family.IIf_left info branch_if_left branch_if_right k =>
          kinfo info /\ kinstr branch_if_left /\ kinstr branch_if_right /\ kinstr k
      (*
        Lists
        -----
      *)
      | With_family.ICons_list info k =>
          kinfo info /\ kinstr k
      | With_family.INil info k =>
          kinfo info /\ kinstr k
      | With_family.IIf_cons info branch_if_cons branch_if_nil k =>
          kinfo info /\ kinstr branch_if_cons /\ kinstr branch_if_nil /\ kinstr k
      | With_family.IList_map info body k =>
          kinfo info /\ kinstr body /\ kinstr k
      | With_family.IList_iter info body k =>
          kinfo info /\ kinstr body /\ kinstr k
      | With_family.IList_size info k =>
          kinfo info /\ kinstr k
      (*
      Sets
      ----
        *)
      | With_family.IEmpty_set info t instr =>
          kinfo info /\ ty t /\ is_Comparable t /\ kinstr instr
      | With_family.ISet_iter info instr1 instr2 =>
          kinfo info /\ kinstr instr1 /\ kinstr instr2
      | With_family.ISet_mem info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISet_update info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISet_size info instr =>
          kinfo info /\ kinstr instr
      (*
        Maps
        ----
        *)
      | With_family.IEmpty_map info key instr =>
          kinfo info /\ ty key /\ is_Comparable key /\ kinstr instr
      | With_family.IMap_map info instr1 instr2 =>
          kinfo info /\ kinstr instr1 /\ kinstr instr2
      | With_family.IMap_iter info instr1 instr2 =>
          kinfo info /\ kinstr instr1 /\ kinstr instr2
      | With_family.IMap_mem info instr =>
          kinfo info /\ kinstr instr
      | With_family.IMap_get info instr =>
          kinfo info /\ kinstr instr
      | With_family.IMap_update info instr =>
          kinfo info /\ kinstr instr
      | With_family.IMap_get_and_update info instr =>
          kinfo info /\ kinstr instr
      | With_family.IMap_size info instr =>
          kinfo info /\ kinstr instr
      (*
        Big maps
        --------
        *)
      | With_family.IEmpty_big_map info key val instr =>
          kinfo info /\ ty key /\ ty val /\ kinstr instr
      | With_family.IBig_map_mem info instr =>
          kinfo info /\ kinstr instr
      | With_family.IBig_map_get info instr =>
          kinfo info /\ kinstr instr
      | With_family.IBig_map_update info instr =>
          kinfo info /\ kinstr instr
      | With_family.IBig_map_get_and_update info instr =>
          kinfo info /\ kinstr instr
      (*
        Strings
        -------
        *)
      | With_family.IConcat_string info instr =>
          kinfo info /\ kinstr instr
      | With_family.IConcat_string_pair info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISlice_string info instr =>
          kinfo info /\ kinstr instr
      | With_family.IString_size info instr =>
          kinfo info /\ kinstr instr
      (*
        Bytes
        -----
        *)
      | With_family.IConcat_bytes info instr =>
          kinfo info /\ kinstr instr
      | With_family.IConcat_bytes_pair info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISlice_bytes info instr =>
          kinfo info /\ kinstr instr
      | With_family.IBytes_size info instr =>
          kinfo info /\ kinstr instr
      (*
        Timestamps
        ----------
        *)
      | With_family.IAdd_seconds_to_timestamp info instr =>
          kinfo info /\ kinstr instr
      | With_family.IAdd_timestamp_to_seconds info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISub_timestamp_seconds info instr =>
          kinfo info /\ kinstr instr
      | With_family.IDiff_timestamps info instr =>
          kinfo info /\ kinstr instr
      (*
        Tez
        ---
        *)
      | With_family.IAdd_tez info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISub_tez info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISub_tez_legacy info instr =>
          kinfo info /\ kinstr instr
      | With_family.IMul_teznat info instr =>
          kinfo info /\ kinstr instr
      | With_family.IMul_nattez info instr =>
          kinfo info /\ kinstr instr
      | With_family.IEdiv_teznat info instr =>
          kinfo info /\ kinstr instr
      | With_family.IEdiv_tez info instr =>
          kinfo info /\ kinstr instr
      (*
        Booleans
        --------
      *)
      | With_family.IOr info k =>
          kinfo info /\ kinstr k
      | With_family.IAnd info k =>
          kinfo info /\ kinstr k
      | With_family.IXor info k =>
          kinfo info /\ kinstr k
      | With_family.INot info k =>
          kinfo info /\ kinstr k
      (*
        Integers
        --------
        *)
      | With_family.IIs_nat info instr =>
          kinfo info /\ kinstr instr
      | With_family.INeg info instr =>
          kinfo info /\ kinstr instr
      | With_family.IAbs_int info instr =>
          kinfo info /\ kinstr instr
      | With_family.IInt_nat info instr =>
          kinfo info /\ kinstr instr
      | With_family.IAdd_int info instr =>
          kinfo info /\ kinstr instr
      | With_family.IAdd_nat info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISub_int info instr =>
          kinfo info /\ kinstr instr
      | With_family.IMul_int info instr =>
          kinfo info /\ kinstr instr
      | With_family.IMul_nat info instr =>
          kinfo info /\ kinstr instr
      | With_family.IEdiv_int info instr =>
          kinfo info /\ kinstr instr
      | With_family.IEdiv_nat info instr =>
          kinfo info /\ kinstr instr
      | With_family.ILsl_nat info instr =>
          kinfo info /\ kinstr instr
      | With_family.ILsr_nat info instr =>
          kinfo info /\ kinstr instr
      | With_family.IOr_nat info instr =>
          kinfo info /\ kinstr instr
      | With_family.IAnd_nat info instr =>
          kinfo info /\ kinstr instr
      | With_family.IAnd_int_nat info instr =>
          kinfo info /\ kinstr instr
      | With_family.IXor_nat info instr =>
          kinfo info /\ kinstr instr
      | With_family.INot_int info instr =>
          kinfo info /\ kinstr instr
      (*
        Control
        -------
      *)
      | With_family.IIf info branch_if_true branch_if_false k =>
          kinfo info /\ kinstr branch_if_true /\ kinstr branch_if_false /\ kinstr k
      | With_family.ILoop info body k =>
          kinfo info /\ kinstr body /\ kinstr k
      | With_family.ILoop_left info bl br =>
          kinfo info /\ kinstr bl /\ kinstr br
      | With_family.IDip info b k =>
          kinfo info /\ kinstr b /\ kinstr k
      | With_family.IExec info k =>
          kinfo info /\ kinstr k
      | With_family.IApply info capture_ty k =>
          kinfo info /\ ty capture_ty /\ kinstr k
      | With_family.ILambda info lam k =>
          kinfo info /\ lambda lam /\ kinstr k
      | With_family.IFailwith info kloc tv =>
          kinfo info /\ ty tv
      (*
        Comparison
        ----------
        *)
      | With_family.ICompare info t instr =>
          kinfo info /\ ty t /\ kinstr instr
      (*
        Comparators
        -----------
        *)
      | With_family.IEq info instr =>
          kinfo info /\ kinstr instr
      | With_family.INeq info instr =>
          kinfo info /\ kinstr instr
      | With_family.ILt info instr =>
          kinfo info /\ kinstr instr
      | With_family.IGt info instr =>
          kinfo info /\ kinstr instr
      | With_family.ILe info instr =>
          kinfo info /\ kinstr instr
      | With_family.IGe info instr =>
          kinfo info /\ kinstr instr
      (*
        Protocol
        --------
        *)
      | With_family.IAddress info instr =>
          kinfo info /\ kinstr instr
      | With_family.IContract info t str instr =>
          kinfo info /\ ty t /\ kinstr instr
      | With_family.IView info sign instr =>
          kinfo info /\ view_signature sign /\ kinstr instr
      | With_family.ITransfer_tokens info instr =>
          kinfo info /\ kinstr instr
      | With_family.IImplicit_account info instr =>
          kinfo info /\ kinstr instr
      | With_family.ICreate_contract info storage_type code instr =>
          kinfo info /\ ty storage_type /\ kinstr instr
      | With_family.ISet_delegate info instr =>
          kinfo info /\ kinstr instr
      | With_family.INow info instr =>
          kinfo info /\ kinstr instr
      | With_family.IMin_block_time info instr =>
          kinfo info /\ kinstr instr
      | With_family.IBalance info instr =>
          kinfo info /\ kinstr instr
      | With_family.ILevel info instr =>
          kinfo info /\ kinstr instr
      | With_family.ICheck_signature info instr =>
          kinfo info /\ kinstr instr
      | With_family.IHash_key info instr =>
          kinfo info /\ kinstr instr
      | With_family.IPack info t instr =>        
          kinfo info /\ ty t /\ kinstr instr
      | With_family.IUnpack info t instr =>
          kinfo info /\ ty t /\ kinstr instr
      | With_family.IBlake2b info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISha256 info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISha512 info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISource info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISender info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISelf info t str instr =>
          kinfo info /\ ty t /\ kinstr instr
      | With_family.ISelf_address info instr =>
          kinfo info /\ kinstr instr
      | With_family.IAmount info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISapling_empty_state info sz instr =>
          kinfo info /\ Sapling_repr.Memo_size.Valid.t sz /\ kinstr instr
      | With_family.ISapling_verify_update info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISapling_verify_update_deprecated info instr =>
          kinfo info /\ kinstr instr
      | With_family.IDig info v spref instr =>
          kinfo info /\ Pervasives.Int.Valid.t v /\ kinstr instr
      | With_family.IDug info v spref instr =>
          kinfo info /\ Pervasives.Int.Valid.t v /\ kinstr instr
      | With_family.IDipn info v spref instr1 instr2 =>
          kinfo info /\ Pervasives.Int.Valid.t v /\ kinstr instr1 /\ kinstr instr2
      | With_family.IDropn info v spref instr =>
          kinfo info /\ Pervasives.Int.Valid.t v /\ kinstr instr
      | With_family.IChainId info instr =>
          kinfo info /\ kinstr instr
      | With_family.INever info =>
          kinfo info
      | With_family.IVoting_power info instr =>
          kinfo info /\ kinstr instr
      | With_family.ITotal_voting_power info instr =>
          kinfo info /\ kinstr instr
      | With_family.IKeccak info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISha3 info instr =>
          kinfo info /\ kinstr instr
      | With_family.IAdd_bls12_381_g1 info instr =>
          kinfo info /\ kinstr instr
      | With_family.IAdd_bls12_381_g2 info instr =>
          kinfo info /\ kinstr instr
      | With_family.IAdd_bls12_381_fr info instr =>
          kinfo info /\ kinstr instr
      | With_family.IMul_bls12_381_g1 info instr =>
          kinfo info /\ kinstr instr
      | With_family.IMul_bls12_381_g2 info instr =>
          kinfo info /\ kinstr instr
      | With_family.IMul_bls12_381_fr info instr =>
          kinfo info /\ kinstr instr
      | With_family.IMul_bls12_381_z_fr info instr =>
          kinfo info /\ kinstr instr
      | With_family.IMul_bls12_381_fr_z info instr =>
          kinfo info /\ kinstr instr
      | With_family.IInt_bls12_381_fr info instr =>
          kinfo info /\ kinstr instr
      | With_family.INeg_bls12_381_g1 info instr =>
          kinfo info /\ kinstr instr
      | With_family.INeg_bls12_381_g2 info instr =>
          kinfo info /\ kinstr instr
      | With_family.INeg_bls12_381_fr info instr =>
          kinfo info /\ kinstr instr
      | With_family.IPairing_check_bls12_381 info instr =>
          kinfo info /\ kinstr instr
      | With_family.IComb info v comb instr =>
          kinfo info /\ Pervasives.Int.Valid.t v /\ kinstr instr
      | With_family.IUncomb info v comb instr =>
          kinfo info /\ Pervasives.Int.Valid.t v /\ kinstr instr
      | With_family.IComb_get info v comb instr =>
          kinfo info /\ Pervasives.Int.Valid.t v /\ kinstr instr
      | With_family.IComb_set info v comb instr =>
          kinfo info /\ Pervasives.Int.Valid.t v /\ kinstr instr
      | With_family.IDup_n info v dup_n instr =>
          kinfo info /\ Pervasives.Int.Valid.t v /\ kinstr instr
      | With_family.ITicket info instr =>
          kinfo info /\ kinstr instr
      | With_family.IRead_ticket info instr =>
          kinfo info /\ kinstr instr
      | With_family.ISplit_ticket info instr =>
          kinfo info /\ kinstr instr
      | With_family.IJoin_tickets info t instr =>
          kinfo info /\ ty t /\ kinstr instr /\ Script_typed_ir.With_family.is_Comparable t
      | With_family.IOpen_chest info instr =>
          kinfo info /\ kinstr instr
      (*
        Internal control instructions
        -----------------------------
      *)
      | With_family.IHalt info =>
          kinfo info
      end

    (** The validity of a [lambda]. *)
    with lambda {arg ret} (lam : With_family.lambda arg ret) {struct lam} : Prop :=
        let '(descr, _) := lam in kdescr descr

    (** The validity of a [kdescr]. *)
    with kdescr {s f} (descr : With_family.kdescr s f) {struct descr} : Prop :=
        stack_ty descr.(With_family.kdescr.kbef) /\
        stack_ty descr.(With_family.kdescr.kaft) /\
        kinstr descr.(With_family.kdescr.kinstr)

    (** The validity of a Michelson value. *)
    with value {t : Ty.t} {struct t} :
      With_family.ty_to_dep_Set t -> Prop :=
      match t return With_family.ty_to_dep_Set t -> Prop with
      | Ty.Unit => fun x => True
      | Ty.Num Ty.Num.Int => fun _ => True
      | Ty.Num Ty.Num.Nat => Script_int_repr.N.Valid.t
      | Ty.Signature => fun x => True
      | Ty.String => fun x => True
      | Ty.Bytes => fun x => True
      | Ty.Mutez => fun x => Tez_repr.Valid.t x
      | Ty.Key_hash => fun x => True
      | Ty.Key => fun x => True
      | Ty.Timestamp => fun x => True
      | Ty.Address => fun x => True
      | Ty.Tx_rollup_l2_address => Indexable.Value.Valid.t
      | Ty.Bool => fun x => True
      | Ty.Pair ty1 ty2 => fun '(x1, x2) => @value ty1 x1 /\ @value ty2 x2
      | Ty.Union ty1 ty2 => fun x =>
        match x with
        | Script_typed_ir.L x => @value ty1 x
        | Script_typed_ir.R x => @value ty2 x
        end
      | Ty.Lambda ty_arg ty_res => lambda
      | Ty.Option ty => fun x =>
        match x with
        | None => True
        | Some x => @value ty x
        end
      | Ty.List ty =>
          fun xs => Forall (fun x => @value ty x) xs.(boxed_list.elements)
      | Ty.Set_ ty_k => fun x =>
        Script_family.Ty.is_Comparable ty_k /\
        List.Forall
          (fun k =>
            Option.Forall value (With_family.of_value k))
          ((With_family.Script_Set _).(_Set.S.elements) x)
      | Ty.Map tyk _ => fun x =>
        Script_family.Ty.is_Comparable tyk /\
        List.Forall
          (fun '(k, v) =>
            Option.Forall value (With_family.of_value k) /\ value v)
          ((With_family.Script_Map _).(Map.S.bindings) x)
      | Ty.Big_map _ _ => fun x =>
        List.Forall
          (fun '(_, (k, v)) =>
              value k /\ Option.Forall value v)
          (Big_map_overlay.(Map.S.bindings)
              x.(With_family.big_map.diff).(big_map_overlay.map)) /\
          With_family.is_Comparable x.(With_family.big_map.key_type) /\
          ty x.(With_family.big_map.key_type) /\
          ty x.(With_family.big_map.value_type)
      | Ty.Contract ty => fun '(With_family.Typed_contract ty _) => Valid.ty ty
      | Ty.Sapling_transaction => fun x => True
      | Ty.Sapling_transaction_deprecated => fun x => True
      | Ty.Sapling_state => fun x => True
      | Ty.Operation => fun x => True
      | Ty.Chain_id => fun x => True
      | Ty.Never => fun x => False
      | Ty.Bls12_381_g1 => fun x => True
      | Ty.Bls12_381_g2 => fun x => True
      | Ty.Bls12_381_fr => fun x => True
      | Ty.Ticket ty => fun x =>
          (let 'Alpha_context.Script_int.Num_tag x' := 
            x.(Script_typed_ir.ticket.amount) in
          0 <= x') /\
          value x.(Script_typed_ir.ticket.contents)
      | Ty.Chest_key => fun x => True
      | Ty.Chest => fun x => True
      end.

    (** The validity of a [ticket] *)
    Definition ticket {a} (x : With_family.ty_to_dep_Set (Ty.Ticket a)) : Prop :=
      @value (Ty.Ticket a) x.

    (** The validity of a [set]. *)
    Definition set {k} (x : With_family.set k) : Prop :=
      @value (Ty.Set_ k) x.

    (** The validity of a [map]. *)
    Definition map {k v}
      (x : With_family.map k (With_family.ty_to_dep_Set v)) : Prop :=
      @value (Ty.Map k v) x.

    (** The validity of a [big_map]. *)
    Definition big_map {k v} (x : With_family.big_map k v) : Prop :=
      @value (Ty.Big_map k v) x.

    (** The validity of the head of a stack. *)
    Definition stack_value_head {tys : Stack_ty.t}
      (head : With_family.stack_ty_to_dep_Set_head tys) : Prop :=
      match tys, head with
      | [], _ => True
      | ty :: _, head => @value ty head
      end.

    (** The validity of the tail of a stack. *)
    Fixpoint stack_value_tail {tys : Stack_ty.t}
      (tail : With_family.stack_ty_to_dep_Set_tail tys) : Prop :=
      match tys, tail with
      | [], _ => True
      | _ :: tys, (head, tail) =>
        @stack_value_head tys head /\ @stack_value_tail tys tail
      end.

    (** The validity of the value of a stack. *)
    Definition stack_value {tys : Stack_ty.t}
      (stack : With_family.stack_ty_to_dep_Set tys) : Prop :=
      stack_value_head (fst stack) /\ stack_value_tail (snd stack).

    (** The validity of a [continuation]. *)
    Fixpoint continuation {arg ret} (c : With_family.continuation arg ret) :
      Prop :=
      match c with
      | With_family.KNil =>
        True
      | With_family.KCons instr c =>
        kinstr instr /\ continuation c
      | With_family.KReturn s c =>
        stack_value s /\ continuation c
      | With_family.KMap_head c =>
        continuation c
      | With_family.KUndip b c =>
        value b /\ continuation c
      | With_family.KLoop_in i c =>
        kinstr i /\ continuation c
      | With_family.KLoop_in_left i c =>
        kinstr i /\ continuation c
      | With_family.KIter i xs c =>
        kinstr i /\
        List.Forall value xs /\
        continuation c
      | With_family.KList_enter_body i xs ys n c =>
        kinstr i /\
        List.Forall value xs /\
        List.Forall value ys /\
        continuation c
      | With_family.KList_exit_body i xs ys n c =>
        kinstr i /\
        List.Forall value xs /\
        List.Forall value ys /\
        continuation c
      | With_family.KMap_enter_body i xs m c =>
        kinstr i /\
        List.Forall (fun '(x1, x2) => value x1 /\ value x2) xs /\
        map m /\
        continuation c
      | With_family.KMap_exit_body i xs m x c =>
        kinstr i /\
        List.Forall (fun '(x1, x2) => value x1 /\ value x2) xs /\
        map m /\
        value x /\
        continuation c
      | With_family.KView_exit step c =>
        Step_constants.Valid.t step /\
        continuation c
      end.
  End Valid.

  (** [of_value] is an inverse of [to_value]. *)
  Fixpoint of_value_to_value {ty : Ty.t} (x : With_family.ty_to_dep_Set ty)
    {struct ty} :
    Script_family.Ty.is_Comparable ty ->
    With_family.of_value (With_family.to_value x) = Some x.
  Proof.
    destruct ty; simpl; intros; try easy;
      unfold Option.map; step; trivial;
      repeat (rewrite of_value_to_value; try easy; simpl).
  Qed.

  (** [to_value] is an inverse of [of_value]. *)
  Fixpoint to_value_of_value {ty : Ty.t} (x : Ty.to_Set ty) {struct ty} :
    Script_family.Ty.is_Comparable ty ->
    match With_family.of_value x with
    | Some x' => With_family.to_value x' = x
    | None => False
    end.
  Proof.
    destruct ty; simpl; intros; try easy;
      step;
      repeat match goal with
      | |- context[With_family.of_value ?x] =>
        pose proof (to_value_of_value _ x);
        destruct (With_family.of_value x);
        simpl
      end;
      try easy;
      hauto lq: on.
  Qed.

  (** [to_value] on comparable never returns None *)
  Definition of_value_comparable_none_neq {ty : Ty.t}
    (v : Ty.to_Set ty)
    (H_comparable : Script_family.Ty.is_Comparable ty) :
    With_family.of_value v <> None.
  Proof.
    pose proof (to_value_of_value v) as H_to_value_of_value.
    now destruct (With_family.of_value _).
  Qed.
End With_family.

Module Type_size.
  (** The simulation [dep_check_eq] is valid. *)
  Lemma dep_check_eq_eq {error_trace}
    (error_details : Script_tc_errors.dep_error_details error_trace)
    (x_value : int) (y_value : int) :
    Type_size.dep_check_eq error_details x_value y_value =
      Type_size.check_eq (Script_tc_errors.to_error_details error_details)
        x_value y_value.
  Proof.
    unfold Type_size.dep_check_eq, Type_size.check_eq.
    step; [reflexivity|].
    now destruct error_details; simpl; rewrite cast_eval.
  Qed.
End Type_size.

(** The simulation [dep_kinfo_of_kinstr] is valid. *)
Lemma dep_kinfo_of_kinstr_eq {s f} (i_value : With_family.kinstr s f) :
  With_family.to_kinfo (dep_kinfo_of_kinstr i_value) =
  kinfo_of_kinstr (With_family.to_kinstr i_value).
Proof.
  destruct i_value; reflexivity.
Qed.

(** The simulation [dep_is_comparable] is valid. *)
Lemma dep_is_comparable_eq {a} (ty : With_family.ty a) :
  dep_is_comparable ty = Script_typed_ir.is_comparable (With_family.to_ty ty).
Proof.
  now destruct ty.
Qed.

(** The simulation [dep_pair_t] is valid. *)
Lemma dep_pair_t_eq {tl tr}
  loc_value (l_value : With_family.ty tl) (r_value : With_family.ty tr) :
  (let? ty := dep_pair_t loc_value l_value r_value in
  return? (to_ty_ex_c ty)) =
  Script_typed_ir.pair_t loc_value
    (With_family.to_ty l_value) (With_family.to_ty r_value).
Proof.
  unfold dep_pair_t, Script_typed_ir.pair_t; simpl.
  destruct Script_typed_ir.Type_size.compound2; simpl; [|reflexivity].
  repeat rewrite dep_is_comparable_eq.
  now step.
Qed.

(** We prove that dependent version of [pair_key] gives
    the same result as original [pair_key] function from
    the [Proto_alpha] *)
Lemma dep_pair_key_eq
  loc {tl tr}
  (tyl : With_family.ty tl)
  (tyr : With_family.ty tr) :
  (let? ty := dep_pair_key loc tyl tyr in
  return? (With_family.to_ty ty)) =
  Script_typed_ir.pair_key loc (With_family.to_ty tyl) (With_family.to_ty tyr).
Proof.
  unfold dep_pair_key, Script_typed_ir.pair_key.
  destruct Script_typed_ir.Type_size.compound2 eqn:E1; simpl; trivial.
Qed.

(** We prove that dependent version of [pair_3_key] gives
    the same result as original [pair_3_key] function from 
    the [Proto_alpha] *)
Lemma dep_pair_3_key_eq
  loc {tl tm tr}
  (tyl : With_family.ty tl)
  (tym : With_family.ty tm)
  (tyr : With_family.ty tr) :
  (let? ty := dep_pair_3_key loc tyl tym tyr in
  return? (With_family.to_ty ty)) =
  Script_typed_ir.pair_3_key loc
    (With_family.to_ty tyl)
    (With_family.to_ty tym)
    (With_family.to_ty tyr).
Proof.
  unfold dep_pair_3_key, Script_typed_ir.pair_3_key.
  rewrite <- dep_pair_key_eq.
  destruct dep_pair_key; simpl; [|reflexivity].
  apply dep_pair_key_eq.
Qed.

(** The simulation [dep_option_t] is valid. *)
Lemma dep_option_t_eq {a} loc_value (ty : With_family.ty a) :
  (let? ty := dep_option_t loc_value ty in
  return? (With_family.to_ty ty)) =
  Script_typed_ir.option_t loc_value (With_family.to_ty ty).
Proof.
  unfold dep_option_t, Script_typed_ir.option_t; simpl.
  destruct Script_typed_ir.Type_size.compound1; simpl; [|reflexivity].
  now rewrite dep_is_comparable_eq.
Qed.

(** The simulation [dep_operation_t] is valid. *)
Lemma dep_operation_t_eq :
  With_family.to_ty dep_operation_t = Script_typed_ir.operation_t.
Proof.
  reflexivity.
Qed.

(** The simulation [dep_list_operation_t] is valid. *)
Lemma dep_list_operation_t_eq :
  With_family.to_ty dep_list_operation_t =
  Script_typed_ir.list_operation_t.
Proof.
  reflexivity.
Qed.
