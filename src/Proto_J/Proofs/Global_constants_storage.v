Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V5.
Require TezosOfOCaml.Proto_J.Global_constants_storage.
Require TezosOfOCaml.Proto_J.Contract_repr.
Require TezosOfOCaml.Proto_J.Storage.
Require TezosOfOCaml.Proto_J.Script_repr.

(* @TODO *)
Axiom register_get_eq : forall {ctxt} {value : Script_repr.expr}, 
  letP? '(ctxt, hash, size) := Global_constants_storage.register ctxt value in
  letP? '(ctxt, value'):= Global_constants_storage.get ctxt hash in
  value = value'.
