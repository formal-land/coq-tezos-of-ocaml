(** File generated by coq-of-ocaml *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Dal_slot_repr.
Require TezosOfOCaml.Proto_K.Raw_level_repr.
Require TezosOfOCaml.Proto_K.Sc_rollup_commitment_repr.
Require TezosOfOCaml.Proto_K.Sc_rollup_repr.
Require TezosOfOCaml.Proto_K.Tez_repr.

Inductive sc_rollup_staker_in_game_error : Set :=
| Refuter : Signature.public_key_hash -> sc_rollup_staker_in_game_error
| Defender : Signature.public_key_hash -> sc_rollup_staker_in_game_error
| Both :
  Signature.public_key_hash -> Signature.public_key_hash ->
  sc_rollup_staker_in_game_error.

Module Sc_rollup_staker_funds_too_low.
  Record record : Set := Build {
    staker : Signature.public_key_hash;
    sc_rollup : Sc_rollup_repr.t;
    staker_balance : Tez_repr.t;
    min_expected_balance : Tez_repr.t;
  }.
  Definition with_staker staker (r : record) :=
    Build staker r.(sc_rollup) r.(staker_balance) r.(min_expected_balance).
  Definition with_sc_rollup sc_rollup (r : record) :=
    Build r.(staker) sc_rollup r.(staker_balance) r.(min_expected_balance).
  Definition with_staker_balance staker_balance (r : record) :=
    Build r.(staker) r.(sc_rollup) staker_balance r.(min_expected_balance).
  Definition with_min_expected_balance min_expected_balance (r : record) :=
    Build r.(staker) r.(sc_rollup) r.(staker_balance) min_expected_balance.
End Sc_rollup_staker_funds_too_low.
Definition Sc_rollup_staker_funds_too_low :=
  Sc_rollup_staker_funds_too_low.record.

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_max_number_of_available_messages_reached"
      "Maximum number of available messages reached"
      "Maximum number of available messages reached" None
      Data_encoding.unit_value
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_max_number_of_available_messages_reached"
            then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_max_number_of_available_messages_reached"
          unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_staker_in_game" "Staker is already playing a game"
      "Attempted to start a game where one staker is already busy"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (staker : sc_rollup_staker_in_game_error) =>
            let busy
              (ppf : Format.formatter)
              (function_parameter : sc_rollup_staker_in_game_error) : unit :=
              match function_parameter with
              | Refuter sc =>
                Format.fprintf ppf
                  (CamlinternalFormatBasics.Format
                    (CamlinternalFormatBasics.String_literal "the refuter ("
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal ") is"
                          CamlinternalFormatBasics.End_of_format)))
                    "the refuter (%a) is")
                  Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp) sc
              | Defender sc =>
                Format.fprintf ppf
                  (CamlinternalFormatBasics.Format
                    (CamlinternalFormatBasics.String_literal "the defender ("
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal ") is"
                          CamlinternalFormatBasics.End_of_format)))
                    "the defender (%a) is")
                  Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp) sc
              | Both refuter defender =>
                Format.fprintf ppf
                  (CamlinternalFormatBasics.Format
                    (CamlinternalFormatBasics.String_literal
                      "both the refuter ("
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal
                          ") and the defender ("
                          (CamlinternalFormatBasics.Alpha
                            (CamlinternalFormatBasics.String_literal ") are"
                              CamlinternalFormatBasics.End_of_format)))))
                    "both the refuter (%a) and the defender (%a) are")
                  Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp)
                  refuter
                  Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp)
                  defender
              end in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Attempted to start a game where "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal " already busy."
                      CamlinternalFormatBasics.End_of_format)))
                "Attempted to start a game where %a already busy.") busy staker))
      (Data_encoding.union None
        [
          Data_encoding.case_value "Refuter" None (Data_encoding.Tag 0)
            (Data_encoding.obj1
              (Data_encoding.req None None "refuter"
                Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
            (fun (function_parameter : sc_rollup_staker_in_game_error) =>
              match function_parameter with
              | Refuter sc => Some sc
              | _ => None
              end)
            (fun (sc : Signature.public_key_hash) => Refuter sc);
          Data_encoding.case_value "Defender" None (Data_encoding.Tag 1)
            (Data_encoding.obj1
              (Data_encoding.req None None "defender"
                Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
            (fun (function_parameter : sc_rollup_staker_in_game_error) =>
              match function_parameter with
              | Defender sc => Some sc
              | _ => None
              end)
            (fun (sc : Signature.public_key_hash) => Defender sc);
          Data_encoding.case_value "Both" None (Data_encoding.Tag 2)
            (Data_encoding.obj2
              (Data_encoding.req None None "refuter"
                Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
              (Data_encoding.req None None "defender"
                Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
            (fun (function_parameter : sc_rollup_staker_in_game_error) =>
              match function_parameter with
              | Both refuter defender => Some (refuter, defender)
              | _ => None
              end)
            (fun (function_parameter :
              Signature.public_key_hash * Signature.public_key_hash)
              =>
              let '(refuter, defender) := function_parameter in
              Both refuter defender)
        ])
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_staker_in_game" then
            let x_value := cast sc_rollup_staker_in_game_error payload in
            Some x_value
          else None
        end)
      (fun (x_value : sc_rollup_staker_in_game_error) =>
        Build_extensible "Sc_rollup_staker_in_game"
          sc_rollup_staker_in_game_error x_value) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_timeout_level_not_reached" "Attempt to timeout game too early"
      "Attempt to timeout game too early" None Data_encoding.unit_value
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_timeout_level_not_reached" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_timeout_level_not_reached" unit tt) in
  let description :=
    "Refutation game already started, must play with is_opening_move = false."
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_game_already_started" "Refutation game already started"
      description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.unit_value
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_game_already_started" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_game_already_started" unit tt) in
  let description := "Refutation game does not exist" in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary "Sc_rollup_no_game"
      "Refutation game does not exist" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.unit_value
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_no_game" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_no_game" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary "Sc_rollup_wrong_turn"
      "Attempt to play move but not staker's turn"
      "Attempt to play move but not staker's turn" None Data_encoding.unit_value
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_wrong_turn" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_wrong_turn" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_max_number_of_messages_reached_for_commitment_period"
      "Maximum number of messages reached for commitment period"
      "Maximum number of messages reached for commitment period" None
      Data_encoding.unit_value
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if
            String.eqb tag
              "Sc_rollup_max_number_of_messages_reached_for_commitment_period"
            then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible
          "Sc_rollup_max_number_of_messages_reached_for_commitment_period" unit
          tt) in
  let description := "Attempted to cement a disputed commitment." in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary "Sc_rollup_disputed"
      "Commitment disputed" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_disputed" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_disputed" unit tt) in
  let description := "Attempted to use a rollup that has not been originated."
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_does_not_exist" "Rollup does not exist" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (x_value : Sc_rollup_repr.t) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Rollup "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal " does not exist"
                      CamlinternalFormatBasics.End_of_format)))
                "Rollup %a does not exist") Sc_rollup_repr.pp x_value))
      (Data_encoding.obj1
        (Data_encoding.req None None "rollup" Sc_rollup_repr.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_does_not_exist" then
            let x_value := cast Sc_rollup_repr.t payload in
            Some x_value
          else None
        end)
      (fun (x_value : Sc_rollup_repr.t) =>
        Build_extensible "Sc_rollup_does_not_exist" Sc_rollup_repr.t x_value) in
  let description := "No conflict." in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_no_conflict" "No conflict" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_no_conflict" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_no_conflict" unit tt) in
  let description := "No stakers." in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary "Sc_rollup_no_stakers"
      "No stakers" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_no_stakers" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_no_stakers" unit tt) in
  let description := "Unknown staker." in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary "Sc_rollup_not_staked"
      "Unknown staker" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_not_staked" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_not_staked" unit tt) in
  let description :=
    "Attempted to withdraw while not staked on the last cemented commitment." in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_not_staked_on_lcc" "Rollup not staked on LCC" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_not_staked_on_lcc" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_not_staked_on_lcc" unit tt) in
  let description := "Parent is not cemented." in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_parent_not_lcc" "Parent not cemented" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_parent_not_lcc" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_parent_not_lcc" unit tt) in
  let description := "Can not remove a cemented commitment." in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary "Sc_rollup_remove_lcc"
      "Can not remove cemented" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_remove_lcc" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_remove_lcc" unit tt) in
  let description := "Staker backtracked." in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_staker_backtracked" "Staker backtracked" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_staker_backtracked" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_staker_backtracked" unit tt) in
  let description :=
    "Commitment is too far ahead of the last cemented commitment." in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_too_far_ahead" "Commitment too far ahead" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_too_far_ahead" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_too_far_ahead" unit tt) in
  let description :=
    "Attempted to cement a commitment before its refutation deadline." in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary "Sc_rollup_too_recent"
      "Commitment too recent" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_too_recent" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_too_recent" unit tt) in
  let description := "Unknown commitment." in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_unknown_commitment" "Unknown commitment" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (x_value : Sc_rollup_commitment_repr.Hash.t) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Commitment "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal " does not exist"
                      CamlinternalFormatBasics.End_of_format)))
                "Commitment %a does not exist")
              Sc_rollup_commitment_repr.Hash.pp x_value))
      (Data_encoding.obj1
        (Data_encoding.req None None "commitment"
          Sc_rollup_commitment_repr.Hash.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_unknown_commitment" then
            let x_value := cast Sc_rollup_commitment_repr.Hash.t payload in
            Some x_value
          else None
        end)
      (fun (x_value : Sc_rollup_commitment_repr.Hash.t) =>
        Build_extensible "Sc_rollup_unknown_commitment"
          Sc_rollup_commitment_repr.Hash.t x_value) in
  let description := "Attempted to commit to a bad inbox level." in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_bad_inbox_level" "Committing to a bad inbox level" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_bad_inbox_level" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_bad_inbox_level" unit tt) in
  let description := "Invalid rollup outbox message index" in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_invalid_outbox_message_index"
      "Invalid rollup outbox message index" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_invalid_outbox_message_index" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_invalid_outbox_message_index" unit tt) in
  let description := "Outbox level expired" in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_outbox_level_expired" description description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_outbox_level_expired" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_outbox_level_expired" unit tt) in
  let description := "Outbox message already applied" in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_outbox_message_already_applied" description description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format) "%s") description))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_outbox_message_already_applied" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Sc_rollup_outbox_message_already_applied" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_staker_funds_too_low"
      "Staker does not have enough funds to make a deposit"
      "Staker doesn't have enough funds to make a smart contract rollup deposit."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Signature.public_key_hash * Sc_rollup_repr.t * Tez_repr.t *
              Tez_repr.t) =>
            let '(staker, sc_rollup, staker_balance, min_expected_balance) :=
              function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Staker ("
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      ") doesn't have enough funds to make the deposit for smart contract rollup ("
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal
                          "). Staker's balance is "
                          (CamlinternalFormatBasics.Alpha
                            (CamlinternalFormatBasics.String_literal
                              " while a balance of at least "
                              (CamlinternalFormatBasics.Alpha
                                (CamlinternalFormatBasics.String_literal
                                  " is required."
                                  CamlinternalFormatBasics.End_of_format)))))))))
                "Staker (%a) doesn't have enough funds to make the deposit for smart contract rollup (%a). Staker's balance is %a while a balance of at least %a is required.")
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp) staker
              Sc_rollup_repr.pp sc_rollup Tez_repr.pp staker_balance Tez_repr.pp
              min_expected_balance))
      (Data_encoding.obj4
        (Data_encoding.req None None "staker"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
        (Data_encoding.req None None "sc_rollup" Sc_rollup_repr.encoding)
        (Data_encoding.req None None "staker_balance" Tez_repr.encoding)
        (Data_encoding.req None None "min_expected_balance" Tez_repr.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_staker_funds_too_low" then
            let '{|
              Sc_rollup_staker_funds_too_low.staker := staker;
                Sc_rollup_staker_funds_too_low.sc_rollup := sc_rollup;
                Sc_rollup_staker_funds_too_low.staker_balance := staker_balance;
                Sc_rollup_staker_funds_too_low.min_expected_balance :=
                  min_expected_balance
                |} := cast Sc_rollup_staker_funds_too_low payload in
            Some (staker, sc_rollup, staker_balance, min_expected_balance)
          else None
        end)
      (fun (function_parameter :
        Signature.public_key_hash * Sc_rollup_repr.t * Tez_repr.t * Tez_repr.t)
        =>
        let '(staker, sc_rollup, staker_balance, min_expected_balance) :=
          function_parameter in
        Build_extensible "Sc_rollup_staker_funds_too_low"
          Sc_rollup_staker_funds_too_low
          {| Sc_rollup_staker_funds_too_low.staker := staker;
            Sc_rollup_staker_funds_too_low.sc_rollup := sc_rollup;
            Sc_rollup_staker_funds_too_low.staker_balance := staker_balance;
            Sc_rollup_staker_funds_too_low.min_expected_balance :=
              min_expected_balance; |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "Sc_rollup_dal_slot_already_registered"
      "DAL slot already registered for rollup" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Sc_rollup_repr.t * Dal_slot_repr.Index.t) =>
            let '(rollup, slot_index) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Rollup "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      " is already subscribed to data availability slot "
                      (CamlinternalFormatBasics.Alpha
                        CamlinternalFormatBasics.End_of_format))))
                "Rollup %a is already subscribed to data availability slot %a")
              Sc_rollup_repr.pp rollup Dal_slot_repr.Index.pp slot_index))
      (Data_encoding.obj2
        (Data_encoding.req None None "rollup" Sc_rollup_repr.encoding)
        (Data_encoding.req None None "slot_index" Dal_slot_repr.Index.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Sc_rollup_dal_slot_already_registered" then
            let '(rollup, slot_index) :=
              cast (Sc_rollup_repr.t * Dal_slot_repr.Index.t) payload in
            Some (rollup, slot_index)
          else None
        end)
      (fun (function_parameter : Sc_rollup_repr.t * Dal_slot_repr.Index.t) =>
        let '(rollup, slot_index) := function_parameter in
        Build_extensible "Sc_rollup_dal_slot_already_registered"
          (Sc_rollup_repr.t * Dal_slot_repr.Index.t) (rollup, slot_index)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "Sc_rollup_requested_dal_slot_subscriptions_at_future_level"
      "Requested list of subscribed dal slots at a future level" description
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Raw_level_repr.raw_level * Raw_level_repr.raw_level) =>
            let '(current_level, future_level) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "The list of subscribed dal slot indices has been requested for level "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      ", but the current level is "
                      (CamlinternalFormatBasics.Alpha
                        CamlinternalFormatBasics.End_of_format))))
                "The list of subscribed dal slot indices has been requested for level %a, but the current level is %a")
              Raw_level_repr.pp future_level Raw_level_repr.pp current_level))
      (Data_encoding.obj2
        (Data_encoding.req None None "current_level" Raw_level_repr.encoding)
        (Data_encoding.req None None "future_level" Raw_level_repr.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if
            String.eqb tag
              "Sc_rollup_requested_dal_slot_subscriptions_of_future_level" then
            let '(current_level, future_level) :=
              cast (Raw_level_repr.t * Raw_level_repr.t) payload in
            Some (current_level, future_level)
          else None
        end)
      (fun (function_parameter :
        Raw_level_repr.raw_level * Raw_level_repr.raw_level) =>
        let '(current_level, future_level) := function_parameter in
        Build_extensible
          "Sc_rollup_requested_dal_slot_subscriptions_of_future_level"
          (Raw_level_repr.t * Raw_level_repr.t) (current_level, future_level))
    in
  tt.
