Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Script_typed_ir.

Module Ty.
  Module Num.
    Inductive t : Set :=
    | Int : t
    | Nat : t.
  End Num.

  Inductive t : Set :=
  | Unit : t
  | Num : Num.t -> t
  | Signature : t
  | String : t
  | Bytes : t
  | Mutez : t
  | Key_hash : t
  | Key : t
  | Timestamp : t
  | Address : t
  | Tx_rollup_l2_address : t
  | Bool : t
  | Pair : t -> t -> t
  | Union : t -> t -> t
  | Lambda : t -> t -> t
  | Option : t -> t
  | List : t -> t
  | Set_ : t -> t
  | Map : t -> t -> t
  | Big_map : t -> t -> t
  | Contract : t -> t
  | Sapling_transaction : t
  | Sapling_transaction_deprecated : t
  | Sapling_state : t
  | Operation : t
  | Chain_id : t
  | Never : t
  | Bls12_381_g1 : t
  | Bls12_381_g2 : t
  | Bls12_381_fr : t
  | Ticket : t -> t
  | Chest_key : t
  | Chest : t.

  Module Parametrized_sets.
    (** The cases that differ for the [to_Set] function between the version to
        the [Set] from the OCaml implementation and the [Set] from the
        dependent implementation. *)
    Record t : Type := {
      lambda : Ty.t -> Ty.t -> Set;
      set : Ty.t -> Set -> Set;
      map : Ty.t -> Set -> Set -> Set;
      big_map : Ty.t -> Ty.t -> Set -> Set -> Set;
      contract : Ty.t -> Set;
    }.

    (** The [to_Set] cases for the version for the OCaml implementation. *)
    Definition ocaml : t := {|
      lambda _ _ := Script_typed_ir.lambda;
      set _ a := Script_typed_ir.set a;
      map _ k v := Script_typed_ir.map k v;
      big_map _ _ _ _ := Script_typed_ir.big_map;
      contract _ := Script_typed_ir.typed_contract;
    |}.
  End Parametrized_sets.

  (** Return the [Set] corresponding to a [Ty.t]. This is a generic version
      depending on [parametrized_sets]. *)
  Fixpoint to_Set_aux (parametrized_sets : Parametrized_sets.t) (ty : Ty.t) :
    Set :=
    let to_Set := to_Set_aux parametrized_sets in
    match ty with
    | Unit => unit
    | Num _ => Script_int.num
    | Signature => Script_typed_ir.Script_signature.t
    | String => Script_string.t
    | Bytes => Bytes.t
    | Mutez => Tez_repr.t
    | Key_hash => public_key_hash
    | Key => public_key
    | Timestamp => Script_timestamp.t
    | Address => Script_typed_ir.address
    | Tx_rollup_l2_address => Script_typed_ir.tx_rollup_l2_address
    | Bool => bool
    | Pair ty1 ty2 => to_Set ty1 * to_Set ty2
    | Union ty1 ty2 => Script_typed_ir.union (to_Set ty1) (to_Set ty2)
    | Lambda ty_arg ty_res =>
      parametrized_sets.(Parametrized_sets.lambda) ty_arg ty_res
    | Option ty => option (to_Set ty)
    | List ty => Script_typed_ir.boxed_list (to_Set ty)
    | Set_ k =>
      parametrized_sets.(Parametrized_sets.set) k (to_Set k)
    | Map k v =>
      parametrized_sets.(Parametrized_sets.map) k (to_Set k) (to_Set v)
    | Big_map k v =>
      parametrized_sets.(Parametrized_sets.big_map) k v (to_Set k) (to_Set v)
    | Contract ty => parametrized_sets.(Parametrized_sets.contract) ty
    | Sapling_transaction => Alpha_context.Sapling.transaction
    | Sapling_transaction_deprecated =>
      Alpha_context.Sapling.legacy_transaction
    | Sapling_state => Alpha_context.Sapling.state
    | Operation => Script_typed_ir.operation
    | Chain_id => Script_typed_ir.Script_chain_id.t
    | Never => Script_typed_ir.never
    | Bls12_381_g1 => Script_typed_ir.Script_bls.G1.t
    | Bls12_381_g2 => Script_typed_ir.Script_bls.G2.t
    | Bls12_381_fr => Script_typed_ir.Script_bls.Fr.t
    | Ticket ty => Script_typed_ir.ticket (to_Set ty)
    | Chest_key => Script_typed_ir.Script_timelock.chest_key
    | Chest => Script_typed_ir.Script_timelock.chest
    end.

  (** Return the [Set] corresponding, in the OCaml implementation, to a
      type [Ty.t]. *)
  Definition to_Set : Ty.t -> Set :=
    to_Set_aux Parametrized_sets.ocaml.
End Ty.

Module Stack_ty.
  Definition t : Set := list Ty.t.

  (** We always represent a stack as a couple with a default head value. This
      is to follow the OCaml's convention of always having an [accu] value in
      addition of the rest of the [stack]. Here we get the implementation type
      for the head of a stack. *)
  Definition map_head (f : Ty.t -> Set) (tys : t) : Set :=
    match tys with
    | [] => Script_typed_ir.empty_cell
    | ty :: _ => f ty
    end.
  
  (** The implementation type for the tail of a stack. *)
  Fixpoint map_tail (f : Ty.t -> Set) (tys : t) : Set :=
    match tys with
    | [] => Script_typed_ir.empty_cell
    | _ :: tys => map_head f tys * map_tail f tys
    end.

  Definition to_Set_head : t -> Set :=
    map_head Ty.to_Set.
  Arguments to_Set_head _ /. (* We automatically unfold this definition *)

  Definition to_Set_tail : t -> Set :=
    map_tail Ty.to_Set.
  Arguments to_Set_tail _ /. (* We automatically unfold this definition *)

  (** The implementation type for a stack. It is always syntactically a
      couple, so we can call the [fst] and [snd] operators on it to retrieve
      the [accu] and [stack] element following the convention in
      the OCaml code *)
  Definition to_Set (tys : t) : Set :=
    to_Set_head tys * to_Set_tail tys.
  Arguments to_Set _ /. (* We automatically unfold this definition *)
End Stack_ty.
