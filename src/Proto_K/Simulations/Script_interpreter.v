Require Import Coq.Program.Equality.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Script_interpreter.
Require TezosOfOCaml.Proto_K.Script_timestamp.
Require TezosOfOCaml.Proto_K.Tez_repr.

Require Import TezosOfOCaml.Proto_K.Simulations.Script_family.
Require TezosOfOCaml.Proto_K.Simulations.Script_interpreter_defs.
Require TezosOfOCaml.Proto_K.Simulations.Script_ir_translator.
Require TezosOfOCaml.Proto_K.Simulations.Script_map.
Require TezosOfOCaml.Proto_K.Simulations.Script_big_map.
Require TezosOfOCaml.Proto_K.Simulations.Script_set.
Require Import TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.

Fixpoint eval_comb_gadt_witness {s f} (w : With_family.comb_gadt_witness s f)
  : With_family.stack_ty_to_dep_Set s -> With_family.stack_ty_to_dep_Set f :=
  match w with
  | With_family.Comb_one => fun xs => xs
  | With_family.Comb_succ w' => fun '(x, xs) =>
    let '(y, ys) := eval_comb_gadt_witness w' xs in
    ((x, y), ys)
  end.

Fixpoint eval_uncomb_gadt_witness {s f}
  (w : With_family.uncomb_gadt_witness s f) :
  With_family.stack_ty_to_dep_Set s -> With_family.stack_ty_to_dep_Set f :=
  match w with
  | With_family.Uncomb_one => fun xs => xs
  | With_family.Uncomb_succ w' => fun '((x, y), xs) =>
    (x, eval_uncomb_gadt_witness w' (y, xs))
  end.

Fixpoint eval_comb_get_gadt_witness {a b}
  (w : With_family.comb_get_gadt_witness a b)
  : With_family.ty_to_dep_Set a -> With_family.ty_to_dep_Set b :=
  match w with
  | With_family.Comb_get_zero => fun x => x
  | With_family.Comb_get_one =>
    fun '(x, _) => x
  | With_family.Comb_get_plus_two w' =>
    fun '(_, y) => eval_comb_get_gadt_witness w' y
  end.

Fixpoint eval_comb_set_gadt_witness {a b c}
  (w : With_family.comb_set_gadt_witness a b c)
  : With_family.ty_to_dep_Set a -> With_family.ty_to_dep_Set b ->
    With_family.ty_to_dep_Set c :=
  match w with
  | With_family.Comb_set_zero => fun v _ => v
  | With_family.Comb_set_one => fun v '(_, y) => (v, y)
  | With_family.Comb_set_plus_two w' => fun v '(x, y) =>
      (x, eval_comb_set_gadt_witness w' v y)
  end.

Fixpoint eval_dup_n_gadt_witness {b s}
  (w : With_family.dup_n_gadt_witness s b)
  : With_family.stack_ty_to_dep_Set s -> With_family.ty_to_dep_Set b :=
  match w with
  | With_family.Dup_n_zero =>
      fun '(x, _) => x
  | With_family.Dup_n_succ w' =>
      fun '(_, xs) => eval_dup_n_gadt_witness w' xs
  end.

Fixpoint eval_stack_prefix_preservation_witness {s t u v}
  (w : With_family.stack_prefix_preservation_witness s t u v) :
  (With_family.stack_ty_to_dep_Set s -> With_family.stack_ty_to_dep_Set t) ->
  With_family.stack_ty_to_dep_Set u -> With_family.stack_ty_to_dep_Set v :=
  match w with
  | With_family.KRest =>
      fun f => f
  | With_family.KPrefix _ _ w' =>
      fun f '(x, stack) => (x, eval_stack_prefix_preservation_witness w' f stack)
  end.

Fixpoint eval_sppw_for_IDig {X s t u v}
  (w : With_family.stack_prefix_preservation_witness s t u v) :
  (With_family.stack_ty_to_dep_Set s -> X * With_family.stack_ty_to_dep_Set t) ->
  With_family.stack_ty_to_dep_Set u -> X * With_family.stack_ty_to_dep_Set v :=
  match w with
  | With_family.KRest =>
      fun f => f
  | With_family.KPrefix _ _ w' =>
      fun f '(y, stack) => 
      let '(x, stack') := eval_sppw_for_IDig w' f stack in
      (x, (y, stack'))
  end.

Fixpoint eval_sspw_for_IDropn {u s}
  (w : With_family.stack_prefix_preservation_witness u u s s) :
  With_family.stack_ty_to_dep_Set s ->
  With_family.stack_ty_to_dep_Set u.
  dependent destruction w. (*TODO: remove this?*)
  - exact (fun s => s).
  - exact (fun '(_,s) => eval_sspw_for_IDropn _ _ w s).
Defined.

(** Simulation of [ifailwith]. *)
Definition dep_ifailwith {a : Ty.t} {b : Set}
  (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (kloc : Alpha_context.Script.location)
  (tv : With_family.ty a)
  (accu_value : With_family.ty_to_dep_Set a) :
  M? b :=
  let '(ctxt, _) := g in
  let ctxt := Local_gas_counter.update_context gas ctxt in
  let? '(v_value, _ctxt) :=
    Error_monad.trace_value
      (Build_extensible "Cannot_serialize_failure" unit tt)
      (Script_ir_translator.dep_unparse_data ctxt
        Script_ir_translator.Optimized tv accu_value) in
  let v_value := Micheline.strip_locations v_value in
  let? log := Script_interpreter_defs.get_log None in
  Error_monad.fail
    (Build_extensible "Reject"
      (Alpha_context.Script.location *
        Micheline.canonical Alpha_context.Script.prim *
        option Script_typed_ir.execution_trace)
      (kloc, v_value, log)).

(** Simulation of [step]. *)
Fixpoint dep_step {s t f}
  (fuel : nat)
  (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (i_value : With_family.kinstr s t)
  (ks : With_family.continuation t f)
  (accu_stack : With_family.stack_ty_to_dep_Set s)
  {struct fuel}
  : M? (
    With_family.stack_ty_to_dep_Set f *
    Local_gas_counter.outdated_context *
    Local_gas_counter.local_gas_counter
  ) :=
  match fuel with
  | 0%nat =>
    Error_monad.fail
      (Build_extensible "Out of fuel." unit tt)
  | Datatypes.S fuel' =>
    let '(ctxt, sc) := g in
    match Script_interpreter_defs.dep_consume_instr gas i_value accu_stack with
    | None =>
      Error_monad.fail
        (Build_extensible "Operation_quota_exceeded" unit tt)
    | Some gas =>
      match i_value, accu_stack, ks with
      | With_family.IHalt _, _, _ =>
          dep_next fuel' g gas ks accu_stack
      | With_family.IDrop _ k, (_, s), _ =>
          dep_step fuel' g gas k ks s
      | With_family.IDup _ k, (a, s), _ =>
          dep_step fuel' g gas k ks (a, (a, s))
      | With_family.ISwap _ k, (a, (b, s)), _ =>
          dep_step fuel' g gas k ks (b, (a, s))
      | With_family.IConst _ _ v k, _, _ =>
          dep_step fuel' g gas k ks (v, accu_stack)
      | With_family.ICons_some _ k, (a, s), _ =>
          dep_step fuel' g gas k ks (Some a, s)
      | With_family.ICons_none _ _ k, _, _ =>
          dep_step fuel' g gas k ks (None, accu_stack)
      | With_family.IIf_none _ b_none b_some k, (a, s), _ =>
          match a with
          | None =>
              dep_step fuel' g gas b_none (With_family.KCons k ks) s
          | Some v =>
              dep_step fuel' g gas b_some (With_family.KCons k ks) (v, s)
          end
      | With_family.IOpt_map _ body k, (a, s), _ =>
          match a with
          | None =>
              dep_step fuel' g gas k ks (None, s)
          | Some v =>
              dep_step fuel' g gas body
              (With_family.KMap_head (With_family.KCons k ks))
              (v, s)
          end
      | With_family.ICons_pair _ k,  (a, (b, s)), _ =>
          dep_step fuel' g gas k ks ((a,b), s)
      | With_family.IUnpair _ k, ((a, b) , s), _ =>
          dep_step fuel' g gas k ks (a, (b, s))
      | With_family.ICar _ k, ((a, _), s), _ =>
          dep_step fuel' g gas k ks (a, s)
      | With_family.ICdr _ k, ((_, b), s), _ =>
          dep_step fuel' g gas k ks (b, s)
      | With_family.ICons_left _ _ k, (a, s), _ =>
          dep_step fuel' g gas k ks (Script_typed_ir.L a, s)
      | With_family.ICons_right _ _ k, (a, s), _ =>
          dep_step fuel' g gas k ks (Script_typed_ir.R a, s)
      | With_family.IIf_left _ b_left b_right k, (a, s), _ =>
          match a with
          | Script_typed_ir.L v =>
              dep_step fuel' g gas b_left (With_family.KCons k ks) (v, s)
          | Script_typed_ir.R v =>
              dep_step fuel' g gas b_right (With_family.KCons k ks) (v, s)
          end
      | With_family.ICons_list _ k, (a, (tl, s)), _ =>
          dep_step fuel' g gas k ks (Script_list.cons_value a tl, s)
      | With_family.INil _ _ k, s, _ =>
          dep_step fuel' g gas k ks (Script_list.empty, s)
      | With_family.IIf_cons _ b_cons b_nil k, (a, s), _ =>
          match a.(Script_typed_ir.boxed_list.elements) with
          | [] => dep_step fuel' g gas b_nil (With_family.KCons k ks) s
          | hd :: tl =>
              let tl' :=
                Script_typed_ir.boxed_list.Build _
                  tl (a.(Script_typed_ir.boxed_list.length) -i 1) in
              dep_step fuel' g gas b_cons (With_family.KCons k ks) (hd, (tl', s))
          end
      | With_family.IList_map _ body t k, (a, s), _ =>
          let xs := a.(Script_typed_ir.boxed_list.elements) in
          let ys := nil in
          let len := a.(Script_typed_ir.boxed_list.length) in
          let ks := (With_family.KList_enter_body
            body xs ys t len
            (With_family.KCons k ks)) in
          dep_next fuel' g gas ks s
      | With_family.IList_size _ k, (a, s), _ =>
          let len :=
            Script_int.abs (
              Script_int.of_int a.(Script_typed_ir.boxed_list.length)
            ) in
          dep_step fuel' g gas k ks (len, s)
      | With_family.IList_iter _ t body k, (a, s), _ =>
          let xs := a.(Script_typed_ir.boxed_list.elements) in
          let ks := (With_family.KIter 
            body t xs (With_family.KCons k ks)) in
          dep_next fuel' g gas ks s
      | With_family.IEmpty_set _ ty k, s, _ =>
          let res := Script_set.dep_empty ty in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISet_iter _ t body k, (a,s), _ =>
          let set := a in
          let l :=
            List.rev
              (Script_set.dep_fold (fun e acc => cons e acc) set nil) in
          let ks := (With_family.KIter body t l (With_family.KCons k ks)) in
          dep_next fuel' g gas ks s
      | With_family.ISet_mem _ k, (a, (set, s)), _ =>
          let res := Script_set.dep_mem a set in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISet_update _ k, (a, (pres, (set, s))), _ =>
          let res := Script_set.dep_update a pres set in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISet_size _ k, (a, s), _ =>
          let res := Script_set.dep_size_value a in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IEmpty_map _ tk tv k, s, _ =>
          let res := Script_map.dep_empty tk in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMap_map _ t body k, (a, s), _ =>
          let map := a in
          let xs :=
            List.rev
              (Script_map.dep_fold (fun k v a => (k, v) :: a) map nil) in
          let ys := Script_map.dep_empty_from map in
          let ks :=
            With_family.KMap_enter_body body xs ys t (With_family.KCons k ks) in
          dep_next fuel' g gas ks s
      | With_family.IMap_iter _ t body k, (a, s), _ =>
          let map := a in
          let l_value :=
            List.rev
              (Script_map.dep_fold
                (fun k_value v_value a_value =>
                  (k_value, v_value) :: a_value) map nil) in
          let ks :=
            With_family.KIter body t l_value (With_family.KCons k ks) in
          dep_next fuel' g gas ks s
      | With_family.IMap_mem _ k, (a, (map, s)), _ =>
          let res := Script_map.dep_mem a map in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMap_get _ k, (a, (map, s)), _ =>
          let res := Script_map.dep_get a map in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMap_update _ k, (a, (v, (map, s))), _ =>
          let res := Script_map.dep_update a v map in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMap_get_and_update _ k, (a, (v, (map, s))), _ =>
          let map' := Script_map.dep_update a v map in
          let v' := Script_map.dep_get a map in
          dep_step fuel' g gas k ks (v', (map', s))
      | With_family.IMap_size _ k, (a, s), _ =>
          let res := Script_map.dep_size_value a in
          dep_step fuel' g gas k ks (res, s)
      | @With_family.IEmpty_big_map fk fv _ _ _ tk tv k, s, _ =>
          let ebm := Script_big_map.dep_empty_big_map tk tv in
          dep_step fuel' g gas k ks (ebm, s)
      | With_family.IBig_map_mem _ k, (a, (map, s)), _ =>
          let? '(res, ctxt, gas) :=
            Local_gas_counter.use_gas_counter_in_context ctxt gas
              (fun (ctxt : Alpha_context.context) =>
                Script_big_map.dep_big_map_mem ctxt a map) in
          dep_step fuel' (ctxt,sc) gas k ks (res, s)
      | With_family.IBig_map_get _ k, (a, (map, s)), _ =>
          let? '(res, ctxt, gas) :=
          Local_gas_counter.use_gas_counter_in_context ctxt gas
            (fun (ctxt : Alpha_context.context) =>
              Script_big_map.dep_big_map_get ctxt a map) in
          dep_step fuel' (ctxt,sc) gas k ks (res, s)
      | With_family.IBig_map_update _ k, (a, (mv, (map, s))), _ =>
          let? '(big_map, ctxt, gas) :=
          Local_gas_counter.use_gas_counter_in_context ctxt gas
            (fun (ctxt : Alpha_context.context) =>
              Script_big_map.dep_big_map_update ctxt a mv map)
          in
          dep_step fuel' (ctxt,sc) gas k ks (big_map, s)
      | With_family.IBig_map_get_and_update _ k, (a, (v, (map, s))), _ =>
          let key_value := a in
          let? '((v', map'), ctxt, gas) :=
            Local_gas_counter.use_gas_counter_in_context ctxt gas
              (fun (ctxt : Alpha_context.context) =>
                Script_big_map.dep_big_map_get_and_update ctxt key_value v
                  map) in
          dep_step fuel' (ctxt,sc) gas k ks (v', (map', s))
      | With_family.IAdd_seconds_to_timestamp _ k, (a, (t, s)), _ =>
          let res := Script_timestamp.add_delta t a in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAdd_timestamp_to_seconds _ k, (a, (n, s)), _ =>
          let res := Script_timestamp.add_delta a n in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISub_timestamp_seconds _ k, (a, (x, s)), _ =>
          let res := Script_timestamp.sub_delta a x in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IDiff_timestamps _ k, (a, (t, s)), _ =>
          let res := Script_timestamp.diff_value a t in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IConcat_string_pair _ k, (a, (y, s)), _ =>
          let res := Script_string.concat_pair a y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IConcat_string _ k, (a, s), _ =>
          let total_length :=
            List.fold_left
              (fun acc s => 
                Saturation_repr.add acc
                  (Saturation_repr.safe_int
                    (Script_string.length s)))
              Saturation_repr.zero a.(Script_typed_ir.boxed_list.elements) in
          let? gas :=
            Local_gas_counter.consume gas
              (Script_interpreter_defs.Interp_costs.concat_string total_length) in
          let res :=
            Script_string.concat
              a.(Script_typed_ir.boxed_list.elements) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISlice_string _ k, (a, (len, (s, stack))), _ =>
          let offset := Script_int.to_zint a in
          let s_length := Z.of_int (Script_string.length s) in
          let length := Script_int.to_zint len in
          if (offset <? s_length) && (Z.add offset length <=? s_length)
          then
            let s' :=
              Script_string.sub s (Z.to_int offset) (Z.to_int length) in
            dep_step fuel' g gas k ks (Some s', stack)
          else
            dep_step fuel' g gas k ks (None, stack)
      | With_family.IString_size _ k, (a, s), _ =>
          let res :=
            Script_int.abs (
              Script_int.of_int (Script_string.length a)
            ) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IConcat_bytes_pair _ k, (x, (y, s)), _ =>
          let res := Bytes.cat x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IConcat_bytes _ k, (a, s), _ =>
          let total_length :=
            List.fold_left
              (fun acc s => 
                Saturation_repr.add acc 
                  (Saturation_repr.safe_int 
                    (Bytes.length s))) Saturation_repr.zero
              a.(Script_typed_ir.boxed_list.elements) in
          let? gas :=
            Local_gas_counter.consume gas
              (Script_interpreter_defs.Interp_costs.concat_string total_length) in
          let res :=
            Bytes.concat Bytes.empty a.(Script_typed_ir.boxed_list.elements) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISlice_bytes _ k, (offset, (length, (s, stack))), _ =>
          let s_length := Z.of_int (Bytes.length s) in
          let offset' := Script_int.to_zint offset in
          let length' := Script_int.to_zint length in
          if (offset' <? s_length) && (Z.add offset' length' <=? s_length)
          then
            let s' := Bytes.sub s (Z.to_int offset') (Z.to_int length') in
            dep_step fuel' g gas k ks (Some s', stack)
          else
            dep_step fuel' g gas k ks (None, stack)
      | With_family.IBytes_size _ k, (a, s), _ =>
          let res :=
            Script_int.abs (Script_int.of_int (Bytes.length a)) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAdd_tez _ k, (x, (y, s)), _ =>
          let? z := Tez_repr.op_plusquestion x y in
          dep_step fuel' g gas k ks (z, s)
      | With_family.ISub_tez _ k, (x, (y, s)), _ =>
          let z := Tez_repr.sub_opt x y in
          dep_step fuel' g gas k ks (z, s)
      | With_family.ISub_tez_legacy _ k, (x, (y, s)), _ =>
          let? z := Tez_repr.op_minusquestion x y in
          dep_step fuel' g gas k ks (z, s)
      | With_family.IMul_teznat loc_value k, (x, (y, s)), _ =>
          match Script_int.to_int64 y with
          | None =>
            let? log := Script_interpreter_defs.get_log None in
            Error_monad.fail
              (Build_extensible "Overflow"
                (Alpha_context.Script.location *
                  option Script_typed_ir.execution_trace)
                (loc_value, log))
          | Some y =>
            let? res := Alpha_context.Tez.op_starquestion x y in
            dep_step fuel' g gas k ks (res, s)
          end
      | With_family.IMul_nattez loc_value k, (a, s), _ =>
          let y_value := a in
          let '(x_value, stack_value) := s in
          match Script_int.to_int64 y_value with
          | None =>
            let? log := Script_interpreter_defs.get_log None in
            Error_monad.fail
              (Build_extensible "Overflow"
                (Alpha_context.Script.location *
                  option Script_typed_ir.execution_trace)
                (loc_value, log))
          | Some y_value =>
            let? res := Alpha_context.Tez.op_starquestion x_value y_value in
            dep_step fuel' g gas k ks (res, stack_value)
          end
      | With_family.IOr _ k, (x, (y, s)), _ =>
          dep_step fuel' g gas k ks ((x || y), s)
      | With_family.IAnd _ k, (x, (y, s)), _ =>
          dep_step fuel' g gas k ks ((x && y), s)
      | With_family.IXor _ k, (x, (y, s)), _ =>
          dep_step fuel' g gas k ks ((xorb x y), s)
      | With_family.INot _ k, (a, s), _ =>
          dep_step fuel' g gas k ks (negb a, s)
      | With_family.IIs_nat _ k, (a, s), _ =>
          let res := Script_int.is_nat a in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAbs_int _ k, (a, s), _ =>
          let res := Script_int.abs a in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IInt_nat _ k, (a, s), _ =>
          dep_step fuel' g gas k ks (a, s)
      | With_family.INeg _ k, (a, s), _ =>
          let res := Script_int.neg a in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAdd_int _ k, (x, (y, s)), _ =>
          let res := Script_int.add x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAdd_nat _ k, (x, (y, s)), _ =>
          let res := Script_int.add_n x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISub_int _ k, (x, (y, s)), _ =>
          let res := Script_int.sub x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMul_int _ k, (x, (y, s)), _ =>
          let res := Script_int.mul x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMul_nat _ k, (x, (y, s)), _ =>
          let res := Script_int.mul_n x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IEdiv_teznat _ k, (x, (y, s)), _ =>
          let x' := Script_int.of_int64 (Tez_repr.to_mutez x) in
          let res :=
            match Script_int.ediv x' y with
            | None => None
            | Some (q,r) =>
              match (Script_int.to_int64 q, Script_int.to_int64 r) with
              | (Some q, Some r) =>
                match (Tez_repr.of_mutez q, Tez_repr.of_mutez r) with
                | (Some q', Some r') => Some (q', r')
                | _ => None (*dummy value*)
                end
              | _ => None (* dummy value *)
              end
            end in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IEdiv_tez _ k, (x, (y, s)), _ =>
          let x' :=
            Script_int.abs (Script_int.of_int64 (Tez_repr.to_mutez x)) in
          let y' :=
            Script_int.abs (Script_int.of_int64 (Tez_repr.to_mutez y)) in
          let res :=
            match Script_int.ediv_n x' y' with
            | None => None
            | Some (q,r) =>
                match Script_int.to_int64 r with
                | None => None (*dummy value*)
                | Some r' =>
                    match Tez_repr.of_mutez r' with
                    | None => None (*dummy value*)
                    | Some r'' => Some (q, r'')
                    end
                end
            end in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IEdiv_int _ k, (x, (y, s)), _ =>
          let res := Script_int.ediv x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IEdiv_nat _ k, (x, (y, s)), _ =>
          let res := Script_int.ediv_n x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ILsl_nat loc_value k, (a, s), _ =>
          let x_value := a in
          let '(y_value, stack_value) := s in
          match Script_int.shift_left_n x_value y_value with
          | None =>
            let? log := Script_interpreter_defs.get_log None in
            Error_monad.fail
              (Build_extensible "Overflow"
                (Alpha_context.Script.location *
                  option Script_typed_ir.execution_trace)
                (loc_value, log))
          | Some x_value =>
            dep_step fuel' g gas k ks (x_value, stack_value)
          end
      | With_family.ILsr_nat loc_value k, (a, s), _ =>
          let x_value := a in
          let '(y_value, stack_value) := s in
          match Script_int.shift_right_n x_value y_value with
          | None =>
            let? log := Script_interpreter_defs.get_log None in
            Error_monad.fail
              (Build_extensible "Overflow"
                (Alpha_context.Script.location *
                  option Script_typed_ir.execution_trace)
                (loc_value, log))
          | Some x_value =>
            dep_step fuel' g gas k ks (x_value, stack_value)
          end
      | With_family.IOr_nat _ k, (x, (y, s)), _ =>
          let res := Script_int.logor x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAnd_nat _ k, (x, (y, s)), _ =>
          let res := Script_int.logand x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAnd_int_nat _ k, (x, (y, s)), _ =>
          let res := Script_int.logand x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IXor_nat _ k, (x, (y, s)), _ =>
          let res := Script_int.logxor x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.INot_int _ k, (x, s), _ =>
          let res := Script_int.lognot x in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IIf _ b_true b_false k, (a, s), _ =>
          if a
          then
            dep_step fuel' g gas b_true (With_family.KCons k ks) s
          else
            dep_step fuel' g gas b_false (With_family.KCons k ks) s
      | With_family.ILoop _ body k, s, _ =>
          let ks := (With_family.KLoop_in body (With_family.KCons k ks)) in
          dep_next fuel' g gas ks accu_stack
      | With_family.ILoop_left _ bl br, s, _ =>
          let ks := (With_family.KLoop_in_left bl (With_family.KCons br ks)) in
          dep_next fuel' g gas ks accu_stack
      | With_family.IDip _ b t k, (a, s), _ =>
          let ks := With_family.KUndip a t (With_family.KCons k ks) in
          dep_step fuel' g gas b ks s
      | @With_family.IExec a b s f _ sty k, (arg, (code, stack)), _ =>
          (* We check the type of each definitions to make sure. *)
          let arg : With_family.ty_to_dep_Set a := arg in
          let code : With_family.lambda a b := code in
          let '(code, _) := code in
          let code : With_family.kdescr [a] [b] := code in
          let code : With_family.kinstr [a] [b] :=
            code.(With_family.kdescr.kinstr) in
          let ks : With_family.continuation [_] _ :=
            With_family.KReturn stack sty (With_family.KCons k ks) in
          dep_step fuel' g gas code ks
            (arg, (Script_typed_ir.EmptyCell, Script_typed_ir.EmptyCell))
      | With_family.IApply _ ty k, (capture, (lam, stack)), _ =>
          let? '(lam', ctxt, gas) :=
            Script_interpreter_defs.dep_apply ctxt gas ty capture lam in
          dep_step fuel' (ctxt, sc) gas k ks (lam', stack)
      | With_family.ILambda _ lam k, s, _ =>
          dep_step fuel' g gas k ks (lam, s)
      | With_family.IFailwith kloc tv, (accu_value, _), _ =>
        dep_ifailwith g gas kloc tv accu_value
      | With_family.ICompare _ ty k, (a, (b, s)), _ =>
          let res :=
            Script_int.of_int
              (Script_comparable.dep_compare_comparable _
                (With_family.to_value a) (With_family.to_value b)) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IEq _ k, (a, s), _ =>
          let a' := Script_int.compare a Script_int.zero in
          let a'' := a' =i 0 in
          dep_step fuel' g gas k ks (a'', s)
      | With_family.INeq _ k, (a, s), _ =>
          let a' := Script_int.compare a Script_int.zero in
          let a'' := a' <>i 0 in
          dep_step fuel' g gas k ks (a'', s)
      | With_family.ILt _ k, (a, s), _ =>
          let a' := Script_int.compare a Script_int.zero in
          let a'' := a' <i 0 in
          dep_step fuel' g gas k ks (a'', s)
      | With_family.ILe _ k, (a, s), _ =>
          let a' := Script_int.compare a Script_int.zero in
          let a'' := a' <=i 0 in
          dep_step fuel' g gas k ks (a'', s)
      | With_family.IGt _ k, (a, s), _ =>
          let a' := Script_int.compare a Script_int.zero in
          let a'' := a' >i 0 in
          dep_step fuel' g gas k ks (a'', s)
      | With_family.IGe _ k, (a, s), _ =>
          let a' := Script_int.compare a Script_int.zero in
          let a'' := a' >=i 0 in
          dep_step fuel' g gas k ks (a'', s)
      | With_family.IPack _ ty k, (a, s), _ =>
          let? '(bytes_value, ctxt', gas') :=
            Local_gas_counter.use_gas_counter_in_context ctxt gas
            (fun ct =>
              Script_ir_translator.dep_pack_data ct ty a) in
          dep_step fuel' (ctxt', sc) gas' k ks (bytes_value, s)
      | With_family.IUnpack _ ty k, (a, s), _ =>
          let? '(bytes_value, ctxt', gas') :=
            Local_gas_counter.use_gas_counter_in_context ctxt gas
            (fun ct =>
              Script_interpreter_defs.dep_unpack ct ty a) in
          dep_step fuel' (ctxt', sc) gas' k ks (bytes_value, s)
      | With_family.IAddress _ k, (a, s), _ =>
          let 'With_family.Typed_contract _ address := a in
          dep_step fuel' g gas k ks (address, s)
      | With_family.IContract loc_value t_value entrypoint k_value,
          (addr, s),
          _ =>
          let entrypoint_opt :=
            if
              Alpha_context.Entrypoint.is_default
                addr.(Script_typed_ir.address.entrypoint)
            then
              Some entrypoint
            else
              if Alpha_context.Entrypoint.is_default entrypoint then
                Some addr.(Script_typed_ir.address.entrypoint)
              else
                None in
          match entrypoint_opt with
          | Some entrypoint =>
            let ctxt := Local_gas_counter.update_context gas ctxt in
            let? '(ctxt, maybe_contract) :=
              Script_ir_translator.dep_parse_contract_for_script ctxt
                loc_value t_value
                addr.(Script_typed_ir.address.destination) entrypoint in
            let '(gas, ctxt) :=
              Local_gas_counter.local_gas_counter_and_outdated_context ctxt in
            let accu_value := maybe_contract in
            dep_step fuel' (ctxt, sc) gas k_value ks (accu_value, s)
          | None =>
            dep_step fuel' (ctxt, sc) gas k_value ks (None, s)
          end
      | With_family.ITransfer_tokens loc_value k,
          (p, (amount, (tcontract, s))),
          _ =>
          let 'With_family.Typed_contract arg_ty address := tcontract in
          let '{|
            Script_typed_ir.address.destination := destination;
              Script_typed_ir.address.entrypoint := entrypoint
              |} := address in
          let? '(acc, ctxt', gas') :=
            Script_interpreter_defs.dep_transfer
              (ctxt, sc) gas amount loc_value arg_ty p
              destination entrypoint in
          dep_step fuel' (ctxt', sc) gas' k ks (acc, s)
      | With_family.IImplicit_account _ k, (key_value, s), _ =>
          let arg_ty := With_family.Unit_t in
          let address :=
            {| Script_typed_ir.address.destination :=
              Alpha_context.Destination.Contract
                (Contract_repr.Implicit key_value);
              Script_typed_ir.address.entrypoint :=
                Alpha_context.Entrypoint.default |} in
          let res := With_family.Typed_contract arg_ty address in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IView
        _
        (With_family.View_signature name input_ty output_ty)
        sty
        k_value,
        (accu_value, stack_value),
        _ =>
        axiom "TODO view" (* TODO *)
    | With_family.ICreate_contract _ storage_type code k,
      (delegate, (credit, (init, stack_value))),
      _ =>
        let? '(res, contract, ctxt, gas) :=
          Script_interpreter_defs.create_contract
            g
            gas
            (With_family.to_ty storage_type)
            code
            delegate
            credit
            (With_family.to_value init)
        in
        let destination :=
          Alpha_context.Destination.Contract
            (Contract_repr.Originated contract) in
        let stack_value :=
          ({|
            Script_typed_ir.address.destination := destination;
            Script_typed_ir.address.entrypoint :=
              Alpha_context.Entrypoint.default |}, stack_value) in
        dep_step fuel' (ctxt, sc) gas k ks (res, stack_value)
    | With_family.ISet_delegate _ k, (delegate, s), _ =>
        let operation := Script_typed_ir.Delegation delegate in
        let ctxt := Local_gas_counter.update_context gas ctxt in
        let? '(ctxt, nonce_value) := Alpha_context.fresh_internal_nonce ctxt in
        let piop :=
            Script_typed_ir.Internal_operation
            {|
              Script_typed_ir.internal_operation.source :=
                Contract_repr.Originated
                  sc.(Script_typed_ir.step_constants.self);
              Script_typed_ir.internal_operation.operation := operation;
              Script_typed_ir.internal_operation.nonce := nonce_value |}
          in
        let res :=
          {| Script_typed_ir.operation.piop := piop;
            Script_typed_ir.operation.lazy_storage_diff :=
              (None : option Alpha_context.Lazy_storage.diffs) |} in
        let '(gas, ctxt) :=
          Local_gas_counter.local_gas_counter_and_outdated_context ctxt in
        dep_step fuel' (ctxt, sc) gas k ks (res, s)
     | With_family.IBalance _ k_value, s, _ =>
          let ctxt := Local_gas_counter.update_context gas ctxt in
          let '(gas, ctxt) :=
            Local_gas_counter.local_gas_counter_and_outdated_context ctxt in
          let g_value := (ctxt, sc) in
          dep_step fuel' g_value gas k_value ks
            (sc.(Script_typed_ir.step_constants.balance), s)
      | With_family.ILevel _ k, s, _ =>
          let res := sc.(Script_typed_ir.step_constants.level) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.INow _ k, s, _ =>
          let res := sc.(Script_typed_ir.step_constants.now) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMin_block_time _ k_value, s, _ =>
          let ctxt := Local_gas_counter.update_context gas ctxt in
          let min_block_time :=
            Script_int.abs
              (Script_int.of_int64
                 (Alpha_context.Period.to_seconds
                    (Alpha_context.Constants.minimal_block_delay ctxt))) in
          dep_step fuel' g gas k_value ks (min_block_time, s)
      | With_family.ICheck_signature _ k, (key, (signature, (message, s))), _ =>
          let res :=
            Script_typed_ir.Script_signature.check None key
              signature message in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IHash_key _ k, (key, s), _ =>
          let res :=
            Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) key in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IBlake2b _ k, (bytes, s), _ =>
          let hash := Raw_hashes.blake2b bytes in
          dep_step fuel' g gas k ks (hash, s)
      | With_family.ISha256 _ k, (bytes, s), _ =>
          let hash := Raw_hashes.sha256 bytes in
          dep_step fuel' g gas k ks (hash, s)
      | With_family.ISha512 _ k, (bytes, s), _ =>
          let hash := Raw_hashes.sha512 bytes in
          dep_step fuel' g gas k ks (hash, s)
      | With_family.ISource _ k, s, _ =>
          let destination :=
            Alpha_context.Destination.Contract
              sc.(Script_typed_ir.step_constants.payer) in
          let res :=
            {|
              Script_typed_ir.address.destination := destination;
              Script_typed_ir.address.entrypoint :=
                Alpha_context.Entrypoint.default |} in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISender _ k, s, _ =>
          let destination :=
            Alpha_context.Destination.Contract
              sc.(Script_typed_ir.step_constants.source) in
          let res :=
            {|
              Script_typed_ir.address.destination := destination;
              Script_typed_ir.address.entrypoint :=
                Alpha_context.Entrypoint.default |} in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISelf _ ty entrypoint k, s, _ =>
          let destination :=
            Alpha_context.Destination.Contract
              (Contract_repr.Originated
                sc.(Script_typed_ir.step_constants.self)) in
          let res :=
            With_family.Typed_contract ty {|
              Script_typed_ir.address.destination := destination;
              Script_typed_ir.address.entrypoint := entrypoint
            |} in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISelf_address _ k, s, _ =>
          let destination :=
            Alpha_context.Destination.Contract
              (Contract_repr.Originated
                sc.(Script_typed_ir.step_constants.self)) in
          let res :=
            {|
              Script_typed_ir.address.destination := destination;
              Script_typed_ir.address.entrypoint :=
                Alpha_context.Entrypoint.default |} in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAmount _ k, s, _ =>
          let a := sc.(Script_typed_ir.step_constants.amount) in
          dep_step fuel' g gas k ks (a, s)
      | With_family.IDig _ n w k, stack, _ =>
          let stack' :=
            eval_sppw_for_IDig w (fun x => x) stack in
          dep_step fuel' g gas k ks stack'
      | With_family.IDug _ n w k, (a, stack), _ =>
          let stack' :=
            eval_stack_prefix_preservation_witness w (pair a) stack in
          dep_step fuel' g gas k ks stack'
      | With_family.IDipn _ n w b k, s, _ =>
          let '(s', restore) := Script_interpreter_defs.dep_kundip w s k in
          let ks' := With_family.KCons restore ks in
          dep_step fuel' g gas b ks' s'
      | With_family.IDropn _ n w k, s, _ =>
          let s' := eval_sspw_for_IDropn w s in
          dep_step fuel' g gas k ks s'
      | With_family.ISapling_empty_state _ memo_size k, s, _ =>
          let state := Alpha_context.Sapling.empty_state None memo_size tt in
          dep_step fuel' g gas k ks (state, s)
      | With_family.ISapling_verify_update _ k_value, (accu_value, stack_value), _ =>
          let transaction := accu_value in
          let '(state_value, stack_value) := stack_value in
          let address :=
            Contract_hash.to_b58check sc.(Script_typed_ir.step_constants.self) in
          let sc_chain_id :=
            Script_typed_ir.Script_chain_id.make
              sc.(Script_typed_ir.step_constants.chain_id) in
          let chain_id :=
            Script_typed_ir.Script_chain_id.to_b58check sc_chain_id in
          let anti_replay := Pervasives.op_caret address chain_id in
          let ctxt := Local_gas_counter.update_context gas ctxt in
          let? '(ctxt, balance_state_opt) :=
            Alpha_context.Sapling.verify_update ctxt state_value
              transaction anti_replay in
          let '(gas, ctxt) :=
            Local_gas_counter.local_gas_counter_and_outdated_context ctxt in
          match balance_state_opt with
          | Some (balance, state_value) =>
            let state_value :=
              Some (
                Bytes.of_string
                  transaction.(Sapling.UTXO.transaction.bound_data),
                (Script_int.of_int64 balance, state_value)
              )
              in
            dep_step fuel' (ctxt, sc) gas k_value ks
              (state_value, stack_value)
          | None =>
            dep_step fuel' (ctxt, sc) gas k_value ks
              (None, stack_value)
          end
      | With_family.ISapling_verify_update_deprecated _ k_value,
        (accu_value, stack_value), _ =>
          let transaction := accu_value in
          let '(state_value, stack_value) := stack_value in
          let address :=
            Contract_hash.to_b58check
              sc.(Script_typed_ir.step_constants.self) in
          let sc_chain_id :=
            Script_typed_ir.Script_chain_id.make
              sc.(Script_typed_ir.step_constants.chain_id) in
          let chain_id := Script_typed_ir.Script_chain_id.to_b58check sc_chain_id in
          let anti_replay := Pervasives.op_caret address chain_id in
          let ctxt := Local_gas_counter.update_context gas ctxt in
          let? '(ctxt, balance_state_opt) :=
            Alpha_context.Sapling.Legacy.verify_update ctxt state_value transaction
              anti_replay in
          let '(gas, ctxt) :=
            Local_gas_counter.local_gas_counter_and_outdated_context ctxt in
          match balance_state_opt with
          | Some (balance, state_value) =>
              let state_value :=
                Some ((Script_int.of_int64 balance, state_value)) in
              dep_step
                fuel' (ctxt, sc) gas k_value ks (state_value, stack_value)
          | None =>
              dep_step
                fuel' (ctxt, sc) gas k_value ks (None, stack_value)
          end
      | With_family.IChainId _ k, s, _ =>
          let res :=
            Script_typed_ir.Script_chain_id.make
              sc.(Script_typed_ir.step_constants.chain_id) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.INever _, (n, s), _ =>
          match n with
          end
      | With_family.IVoting_power _ k_value, (key_hash, stack_value), _ =>
          let ctxt := Local_gas_counter.update_context gas ctxt in
          let? '(ctxt, rolls) :=
            Alpha_context.Vote.get_voting_power ctxt key_hash in
          let power :=
            Script_int.abs
              (Script_int.of_int64 rolls) in
          let '(gas, ctxt) :=
            Local_gas_counter.local_gas_counter_and_outdated_context ctxt in
          dep_step fuel' (ctxt, sc) gas k_value ks (power, stack_value)
      | With_family.ITotal_voting_power _ k_value, s, _ =>
          let ctxt := Local_gas_counter.update_context gas ctxt in
          let? '(ctxt, rolls) :=
            Alpha_context.Vote.get_total_voting_power ctxt in
          let power :=
            Script_int.abs
              (Script_int.of_int32 rolls) in
          let '(gas, ctxt) :=
            Local_gas_counter.local_gas_counter_and_outdated_context ctxt in
          let g := (ctxt, sc) in
          dep_step fuel' g gas k_value ks (power, s)
      | With_family.IKeccak _ k, (bytes, s), _ =>
          let hash := Raw_hashes.keccak256 bytes in
          dep_step fuel' g gas k ks (hash, s)
      | With_family.ISha3 _ k, (bytes, s), _ =>
          let hash := Raw_hashes.sha3_256 bytes in
          dep_step fuel' g gas k ks (hash, s)
      | With_family.IAdd_bls12_381_g1 _ k, (x, (y, s)), _ =>
          let res := Script_typed_ir.Script_bls.G1.add x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAdd_bls12_381_g2 _ k, (x, (y, s)), _ =>
          let res := Script_typed_ir.Script_bls.G2.add x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAdd_bls12_381_fr _ k, (x, (y, s)), _ =>
          let res := Script_typed_ir.Script_bls.Fr.add x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMul_bls12_381_g1 _ k, (x, (y, s)), _ =>
          let res := Script_typed_ir.Script_bls.G1.mul x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMul_bls12_381_g2 _ k, (x, (y, s)), _ =>
          let res := Script_typed_ir.Script_bls.G2.mul x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMul_bls12_381_fr _ k, (x, (y, s)), _ =>
          let res := Script_typed_ir.Script_bls.Fr.mul x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMul_bls12_381_fr_z _ k, (x, (y, s)), _ =>
          let x := Script_typed_ir.Script_bls.Fr.of_z (Script_int.to_zint x) in
          let res := Script_typed_ir.Script_bls.Fr.mul x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMul_bls12_381_z_fr _ k, (y, (x, s)), _ =>
          let x := Script_typed_ir.Script_bls.Fr.of_z (Script_int.to_zint x) in
          let res := Script_typed_ir.Script_bls.Fr.mul x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IInt_bls12_381_fr _ k, (x, s), _ =>
          let res := Script_int.of_zint (Script_typed_ir.Script_bls.Fr.to_z x) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.INeg_bls12_381_g1 _ k, (x, s), _ =>
          let res := Script_typed_ir.Script_bls.G1.negate x in
          dep_step fuel' g gas k ks (res, s)
      | With_family.INeg_bls12_381_g2 _ k, (x, s), _ =>
          let res := Script_typed_ir.Script_bls.G2.negate x in
          dep_step fuel' g gas k ks (res, s)
      | With_family.INeg_bls12_381_fr _ k, (x, s), _ =>
          let res := Script_typed_ir.Script_bls.Fr.negate x in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IPairing_check_bls12_381 _ k, (pairs, s), _ =>
          let check :=
          Script_typed_ir.Script_bls.pairing_check
            pairs.(Script_typed_ir.boxed_list.elements) in
          dep_step fuel' g gas k ks (check, s)
      | With_family.IComb _ _ w k, s, _ =>
          let s' := eval_comb_gadt_witness w s in
          dep_step fuel' g gas k ks s'
      | With_family.IUncomb _ _ w k, s, _ =>
          let s' := eval_uncomb_gadt_witness w s in
          dep_step fuel' g gas k ks s'
      | With_family.IComb_get _ _ w k, (a, s), _ =>
          let res := eval_comb_get_gadt_witness w a in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IComb_set _ _ w k, (x, (y, s)), _ =>
          let res := eval_comb_set_gadt_witness w x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IDup_n _ _ w k, s, _ =>
          let res := eval_dup_n_gadt_witness w s in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ITicket _ _ k, (contents, (amount, s)), _ =>
          let ticketer :=
            Contract_repr.Originated sc.(Script_typed_ir.step_constants.self) in
          let res := Script_typed_ir.ticket.Build _ ticketer contents amount in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IRead_ticket _ _ k, (accu_value, stack_value), _ =>
          let '{|
            Script_typed_ir.ticket.ticketer := ticketer;
              Script_typed_ir.ticket.contents := contents;
              Script_typed_ir.ticket.amount := amount
              |} := accu_value in
          let stack_value := (accu_value, stack_value) in
          let destination := Alpha_context.Destination.Contract ticketer in
          let addr :=
            {| Script_typed_ir.address.destination := destination;
              Script_typed_ir.address.entrypoint :=
                Alpha_context.Entrypoint.default |} in
          let accu_value := (addr, (contents, amount)) in
          dep_step fuel' g gas k ks (accu_value, stack_value) 
      | With_family.ISplit_ticket _ k, (ticket, ((amount_a, amount_b), s)), _ =>
          let res :=
          if (Script_int.compare
             (Script_int.add_n amount_a amount_b)
             ticket.(Script_typed_ir.ticket.amount)) =i 0
          then Some ((Script_typed_ir.ticket.with_amount amount_a ticket),
                     (Script_typed_ir.ticket.with_amount amount_b ticket))
          else None in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IJoin_tickets _ ty k,
        ((ticket_a, ticket_b) , s), _ =>
          let res :=
          if ((Alpha_context.Contract.compare
               ticket_a.(Script_typed_ir.ticket.ticketer)
               ticket_b.(Script_typed_ir.ticket.ticketer)) =i 0) &&
             ((Script_comparable.dep_compare_comparable _
               (With_family.to_value ticket_a.(Script_typed_ir.ticket.contents))
               (With_family.to_value ticket_b.(Script_typed_ir.ticket.contents))
              ) =i 0)
          then Some {|
                 Script_typed_ir.ticket.ticketer :=
                   ticket_a.(Script_typed_ir.ticket.ticketer);
                 Script_typed_ir.ticket.contents :=
                   ticket_a.(Script_typed_ir.ticket.contents);
                 Script_typed_ir.ticket.amount :=
                   Script_int.add_n
                     ticket_a.(Script_typed_ir.ticket.amount)
                     ticket_b.(Script_typed_ir.ticket.amount) |}
          else None in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IOpen_chest _ k, (chest_key, (chest, (time_z, s))), _ =>
          let res :=
            match Script_int.to_int time_z with
            | None => Script_typed_ir.R false
            | Some time =>
              match
                Script_typed_ir.Script_timelock.open_chest chest chest_key time
              with
              | Timelock.Correct bytes_value =>
                Script_typed_ir.L bytes_value
              | Timelock.Bogus_cipher => Script_typed_ir.R false
              | Timelock.Bogus_opening => Script_typed_ir.R true
              end
            end in
          dep_step fuel' g gas k ks (res, s)
      end
    end
  end

  with dep_next {s f}
    (fuel : nat)
    (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
    (gas : Local_gas_counter.local_gas_counter)
    (ks : With_family.continuation s f)
    (stack : With_family.stack_ty_to_dep_Set s)
    {struct fuel}
    : M? (
      With_family.stack_ty_to_dep_Set f *
      Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter
    ) :=
    let '(ctxt, _) := g in
    let ks' := With_family.to_continuation ks in
    match fuel with
    | 0%nat =>
      Error_monad.fail
        (Build_extensible "Out of fuel." unit tt)
    | Datatypes.S fuel' =>
      match Script_interpreter_defs.consume_control gas ks' with
      | None => Error_monad.fail
                  (Build_extensible "Operation_quota_exceeded" unit tt)
      | Some gas =>
          match ks, stack with
          | With_family.KNil, stack =>
              Pervasives.Ok (stack, ctxt, gas)
          | With_family.KCons k ks, stack =>
              dep_step fuel' g gas k ks stack
          | With_family.KLoop_in k ks0 as c, (a, s) =>
              if a
              then dep_step fuel' g gas k c s
              else dep_next fuel' g gas ks0 s
          | With_family.KReturn stack' _ ks, (a,_) =>
              dep_next fuel' g gas ks (a, stack')
          | With_family.KMap_head ks, (a, s) =>
              dep_next fuel' g gas ks (Some a, s)
          | With_family.KLoop_in_left k ks0 as c, (a, s) =>
              match a with
              | Script_typed_ir.L v =>
                  dep_step fuel' g gas k c (v, s)
              | Script_typed_ir.R v =>
                  dep_next fuel' g gas ks0 (v, s)
              end
          | With_family.KUndip b _ ks, s =>
              dep_next fuel' g gas ks (b, s)
          | With_family.KIter k t xs ks0, s =>
              match xs with
              | [] => dep_next fuel' g gas ks0 s
              | x :: xs' =>
                let ks' := With_family.KIter k t xs' ks0 in
                dep_step fuel' g gas k ks' (x, s)
              end
          | With_family.KList_enter_body k xs ys ty_value len ks, s =>
              match xs with
              | [] =>
                let ys' :=
                  Script_typed_ir.boxed_list.Build _ (List.rev ys) len in
                dep_next fuel' g gas ks (ys', stack)
              | x :: xs' =>
                let ks' :=
                  With_family.KList_exit_body k xs' ys ty_value len ks in
                dep_step fuel' g gas k ks' (x, stack)
              end
          | With_family.KList_exit_body k xs ys ty_value len ks, (a, s) =>
              let ks' :=
                With_family.KList_enter_body k xs (a :: ys) ty_value len ks in
              dep_next fuel' g gas ks' s
          | With_family.KMap_enter_body body xs ys ty_value ks, s =>
              match xs with
              | [] => dep_next fuel' g gas ks (ys, s)
              | (xk, xv) :: xs =>
                  let ks :=
                    With_family.KMap_exit_body body xs ys xk ty_value ks in
                  dep_step fuel' g gas body ks ((xk, xv), s)
              end
          | With_family.KMap_exit_body k xs ys yk ty_value ks, (a, s) =>
              let ys := Script_map.dep_update yk (Some a) ys in
              let ks' := With_family.KMap_enter_body k xs ys ty_value ks in
              dep_next fuel' g gas ks' s
          | With_family.KView_exit constants' ks, stack =>
              let g' := (ctxt, constants') in
              dep_next fuel' g' gas ks stack
          end
      end
    end.
