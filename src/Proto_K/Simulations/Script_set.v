Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Script_set.

Require Import TezosOfOCaml.Proto_K.Simulations.Script_family.
Require Import TezosOfOCaml.Proto_K.Simulations.Script_typed_ir.
Require Import TezosOfOCaml.Proto_K.Script_int.

(** Simulation of the [empty] function. *)
Definition dep_empty {a : Ty.t} (_ : With_family.ty a) : With_family.set a :=
  (With_family.Script_Set a).(_Set.S.empty).

(** Simulation of the [update] function. *)
Definition dep_update {a : Ty.t} (v : With_family.ty_to_dep_Set a) (b : bool)
  (x : With_family.set a) : With_family.set a :=
  let v := With_family.to_value v in
  if b then
    (With_family.Script_Set a).(_Set.S.add) v x
  else
    (With_family.Script_Set a).(_Set.S.remove) v x.

(** Simulation of the [mem] function. *)
Definition dep_mem {elt : Ty.t} (v : With_family.ty_to_dep_Set elt)
  (x : With_family.set elt) : bool :=
  let v := With_family.to_value v in
  (With_family.Script_Set elt).(_Set.S.mem) v x.

(** Simulation of the [fold] function. *)
Definition dep_fold {elt : Ty.t} {acc : Set}
  (f : With_family.ty_to_dep_Set elt -> acc -> acc) (x : With_family.set elt) :
  acc -> acc :=
  (With_family.Script_Set elt).(_Set.S.fold)
    (fun v acc =>
      match With_family.of_value v with
      | None => acc
      | Some v => f v acc
      end
    )
    x.

(** Simulation of the [size_value] function. *)
Definition dep_size_value {elt : Ty.t} (x : With_family.set elt)
  : Script_int.num :=
  let size := (With_family.Script_Set elt).(_Set.S.cardinal) x in
  Script_int.abs (Script_int.of_int size).
