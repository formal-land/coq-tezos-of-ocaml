Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.

Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_K.Proofs.Contract_repr.
Require TezosOfOCaml.Proto_K.Proofs.Script_repr.
Require TezosOfOCaml.Proto_K.Proofs.Sc_rollup_inbox_repr.

(** The encoding [Sc_rollup_inbox_message_repr.encoding] is valid. *)
Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Sc_rollup_inbox_message_repr.encoding.
Proof.
    Data_encoding.Valid.data_encoding_auto.
Qed.