Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Tx_rollup_reveal_repr.

Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_K.Proofs.Contract_repr.
Require TezosOfOCaml.Proto_K.Proofs.Script_repr.
Require TezosOfOCaml.Proto_K.Proofs.Tx_rollup_l2_qty.

Module Valid.
  Import Tx_rollup_reveal_repr.t.

  Record t (x : Tx_rollup_reveal_repr.t) : Prop := {
    amount : Tx_rollup_l2_qty.Valid.t x.(amount);
  }.
End Valid.

Lemma encoding_is_valid :
  Data_encoding.Valid.t Valid.t Tx_rollup_reveal_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
