Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Environment.V6.Proofs.Utils.

#[local] Fixpoint trace_to_str_list (err : trace _error) : list string :=
  match err with
  | [] => []
  | Build_extensible err' _ msg :: tl => err' :: trace_to_str_list tl
  end.

#[local] Fixpoint in_quadratic (errs errs' : list string) : bool :=
  match errs with
  | [] => false
  | hd :: tl => List.mem String.eqb hd errs' || in_quadratic tl errs'
  end.

#[local] Definition in_ (err : trace _error) (errs : list string) : bool :=
  let err_strs := trace_to_str_list err in
  in_quadratic err_strs errs.

#[local] Definition internal_errors := [
    "Internal_error"].

(** [No_internal_errors.t term] ensures that [term] does *not* reduce to any
    of the errors in the error list [internal_errors] which is local
    to this module. *)
Module No_internal_errors.
  Definition t {a : Set} (x : M? a) : Prop :=
    match x with
    | Pervasives.Ok _ => True
    | Pervasives.Error e => in_ e internal_errors = false
    end.
End No_internal_errors.
