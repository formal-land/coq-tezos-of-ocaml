Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Ticket_token.

Require TezosOfOCaml.Proto_K.Proofs.Ticket_scanner.

Lemma token_and_amount_of_ex_ticket_is_valid ex_ticket :
  Ticket_scanner.Ex_ticket.Valid.t ex_ticket ->
  let '(_, Script_int.Num_tag amount) :=
      Ticket_token.token_and_amount_of_ex_ticket ex_ticket in
  0 <= amount.
Proof.
  intros [? ? [? ? []] H]; apply H.
Qed.
