Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Ticket_storage.

(* @TODO *)
Axiom adjust_balance_get_eq : forall {ctxt} {key} {delta},
  letP? '(_, ctxt) := Ticket_storage.adjust_balance ctxt key delta in
  letP? '(balance, _) := Ticket_storage.get_balance ctxt key in
  match balance with 
  | Some balance => balance = delta
  | None => False (* get must return a balance because it was adjusted !? *)
  end.
