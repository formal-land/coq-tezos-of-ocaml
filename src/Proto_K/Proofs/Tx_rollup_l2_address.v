Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Tx_rollup_l2_address.

Require TezosOfOCaml.Environment.V6.Proofs.Blake2B.
Require TezosOfOCaml.Environment.V6.Proofs.Compare.
Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Environment.V6.Proofs.S.
Require TezosOfOCaml.Proto_K.Proofs.Indexable.

(** The encoding [encoding] is valid. *)
Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Tx_rollup_l2_address.encoding.
Proof.
  apply Blake2B.Make_is_valid.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

(** [compare] function is valid. *)
Lemma compare_is_valid :
  Compare.Valid.t (fun _ => True) id Tx_rollup_l2_address.compare.
Proof.
  apply Blake2B.Make_is_valid.
Qed.
#[global] Hint Resolve compare_is_valid : Compare_db.

Module Indexable.
  Lemma Indexable_Make_include_is_valid :
    Indexable.INDEXABLE.Valid.t
      Tx_rollup_l2_address.Indexable.Indexable_Make_include.
  Proof.
    apply Indexable.Make_is_valid;
    constructor; apply Blake2B.Make_is_valid.
  Qed.

  Lemma encoding_is_valid : Data_encoding.Valid.t Indexable.Valid.t 
    Tx_rollup_l2_address.Indexable.encoding.
  Proof.
    apply Indexable_Make_include_is_valid.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.

  Lemma index_encoding_is_valid :
    Data_encoding.Valid.t Indexable.Index.Valid.t
      Tx_rollup_l2_address.Indexable.index_encoding.
  Proof.
    apply Indexable_Make_include_is_valid.
  Qed.
  #[global] Hint Resolve index_encoding_is_valid : Data_encoding_db.

  Lemma value_encoding_is_valid :
    Data_encoding.Valid.t Indexable.Value.Valid.t
      Tx_rollup_l2_address.Indexable.value_encoding.
  Proof.
    apply Indexable_Make_include_is_valid.
  Qed.
  #[global] Hint Resolve value_encoding_is_valid : Data_encoding_db.

  (** [compare] function is valid *)
  Lemma compare_is_valid :
    Compare.Valid.t (fun _ => True) Indexable.forget
      Tx_rollup_l2_address.Indexable.compare.
  Proof.
    apply Indexable_Make_include_is_valid.
  Qed.
  #[global] Hint Resolve compare_is_valid : Compare_db.

  (** [compare_values] function is valid *)
  Lemma compare_values_is_valid :
    Compare.Valid.t Indexable.Value.Valid.t id
      Tx_rollup_l2_address.Indexable.compare_values.
  Proof.
    apply Indexable_Make_include_is_valid.
  Qed.
  #[global] Hint Resolve compare_values_is_valid : Compare_db.

  (** [compare_indexes] function is valid *)
  Lemma compare_indexes_is_valid :
    Compare.Valid.t Indexable.Index.Valid.t id
      Tx_rollup_l2_address.Indexable.compare_indexes.
  Proof.
    Compare.valid_auto.
    eapply Proofs.Indexable.compare_indexes_is_valid.
    sauto lq: on rew: off.
  Qed.
  #[global] Hint Resolve compare_indexes_is_valid : Compare_db.
End Indexable.
