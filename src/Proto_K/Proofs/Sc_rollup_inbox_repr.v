Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Sc_rollup_inbox_repr.

Require TezosOfOCaml.Environment.V6.Proofs.Blake2B.
Require TezosOfOCaml.Environment.V6.Proofs.Context.
Require TezosOfOCaml.Environment.V6.Proofs.Context_hash.
Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_K.Proofs.Raw_level_repr.
Require TezosOfOCaml.Proto_K.Proofs.Sc_rollup_repr.
Require TezosOfOCaml.Proto_K.Proofs.Skip_list_repr.

(** Validity of [old_level_messages] *)
Module Old_levels_messages.
  Module Valid.
    Record t `{Skip_list_repr.Make.FArgs} 
      (r : Skip_list_repr.Make.cell.record 
         Sc_rollup_inbox_repr.Hash.t Sc_rollup_inbox_repr.Hash.t) 
      : Prop := {
      back_pointers : 
       let bp := r.(Skip_list_repr.Make.cell.back_pointers) in
         bp.(FallbackArray.t.default) = None /\ 
         Forall (fun x => x <> None) bp.(FallbackArray.t.items);
      index : Pervasives.Int31.Valid.t r.(Skip_list_repr.Make.cell.index);
    }.
  End Valid.
End Old_levels_messages.

(** Validity of [Sc_rollup_inbox_repr.t] *)
Module Valid.
  Import Sc_rollup_inbox_repr.t.

  Record t (x : Sc_rollup_inbox_repr.t) : Prop := {
    level : Raw_level_repr.Valid.t x.(level);
    starting_level_of_current_commitment_period : 
      Raw_level_repr.Valid.t x.(starting_level_of_current_commitment_period);
    nb_available_messages : Int64.Valid.t x.(nb_available_messages);
    nb_messages_in_commitment_period : 
       Int64.Valid.t x.(nb_messages_in_commitment_period);
    message_counter : 0 <= x.(message_counter);
    old_levels_messages :  Old_levels_messages.Valid.t x.(old_levels_messages);
  }.
End Valid.

Module Hash.
  (** [hash_encoding] is valid *)
  Lemma encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True)
      Sc_rollup_inbox_repr.Hash.encoding.
  Proof.
    apply Blake2B.Make_is_valid.
  Qed.
End Hash.
#[global] Hint Resolve Hash.encoding_is_valid : Data_encoding_db.

(** [old_levels_messages_encoding] is valid *)
Lemma old_levels_messages_encoding_is_valid :
  Data_encoding.Valid.t 
    Old_levels_messages.Valid.t
    Sc_rollup_inbox_repr.old_levels_messages_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
  intros [x] []; simpl.
  repeat 
    match goal with 
    | |- _ /\ _ => constructor
    end; try easy; [hauto l:on|].
  f_equal.
  match goal with
  | |- context [of_list _ ?f _] =>
      assert (H_some : f = Some)
        by (apply FunctionalExtensionality.functional_extensionality; 
          reflexivity); rewrite H_some; clear H_some
  end.
  rewrite Skip_list_repr.Make.back_pointers_of_list_to_list; [easy| |];
    hauto lq: on.
Qed.
#[global] Hint Resolve old_levels_messages_encoding_is_valid : Data_encoding_db.

(** Functional extensionality on unit functions *)
Axiom functional_extentionality_unit : forall {a : Set} (f1 f2 : unit -> a),
  f1 tt = f2 tt -> f1 = f2.

(** [encoding] is valid *)
Lemma encoding_is_valid :
  Data_encoding.Valid.t Valid.t Sc_rollup_inbox_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
  intros x H_valid.
  repeat (split; try dtauto);
    destruct_all Sc_rollup_inbox_repr.t;
    destruct H_valid; simpl in *; 
    unfold Pervasives.Int.Valid.t in *;
    f_equal.
  now apply functional_extentionality_unit.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

(** Validity of [list Sc_rollup_inbox_repr.history_proof] *)
Module Inclusion_encoding.
  Module Valid.
    Definition t (x : list Sc_rollup_inbox_repr.history_proof) : Prop :=
      Forall Old_levels_messages.Valid.t x.
  End Valid.
End Inclusion_encoding.

(** [inclusion_encoding] is valid *)
Lemma inclusion_encoding_is_valid :
  Data_encoding.Valid.t Inclusion_encoding.Valid.t
    Sc_rollup_inbox_repr.inclusion_proof_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve inclusion_encoding_is_valid : Data_encoding_db.

(** Validity of [Sc_rollup_inbox_repr.Proof.t] *)
Module Proof.
  (** The encoding [Proof.encoding] is valid. *)
  Module Valid.
    Import Sc_rollup_inbox_repr.Proof.t.
    Record t (x : Sc_rollup_inbox_repr.Proof.t) : Prop := {
      skips: Forall (fun '(x1, x2) => 
          Sc_rollup_inbox_repr.Valid.t x1 /\ 
          Inclusion_encoding.Valid.t x2)
        x.(skips);
      level : Sc_rollup_inbox_repr.Valid.t x.(level);
      inc : Inclusion_encoding.Valid.t x.(inc);
    }.
  End Valid.
End Proof.

(** [proof_encoding] is valid *)
Lemma proof_encoding_is_valid :
  Data_encoding.Valid.t Proof.Valid.t Sc_rollup_inbox_repr.Proof.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
  apply Context.Proof_encoding.V1.Tree32_is_valid.
  sauto lq:on.
Qed.
#[global] Hint Resolve proof_encoding_is_valid : Data_encoding_db.
