Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Tx_rollup_inbox_repr.

Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_K.Proofs.Tx_rollup_message_repr.

Module Merkle.
  (** [compare] function is valid *)
  Lemma compare_is_valid :
    Compare.Valid.t (fun _ => True) id Tx_rollup_inbox_repr.Merkle.compare.
  Proof. apply Blake2B.Make_is_valid. Qed.
  #[global] Hint Resolve compare_is_valid : Compare_db.
  
  Lemma root_encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True)
      Tx_rollup_inbox_repr.Merkle.root_encoding.
  Proof.
  Admitted.
  #[global] Hint Resolve root_encoding_is_valid : Data_encoding_db.

  Lemma path_encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True)
      Tx_rollup_inbox_repr.Merkle.path_encoding.
  Proof.
  Admitted.
  #[global] Hint Resolve path_encoding_is_valid : Data_encoding_db.
End Merkle.

Module Valid.
  Import Tx_rollup_inbox_repr.t.

  Record t (x : Tx_rollup_inbox_repr.t) : Prop := {
    inbox_length : Pervasives.Int31.Valid.t x.(inbox_length);
    cumulated_size : Pervasives.Int31.Valid.t x.(cumulated_size);
  }.
End Valid.

Lemma encoding_is_valid : Data_encoding.Valid.t Valid.t
  Tx_rollup_inbox_repr.encoding.
Proof.
Admitted.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
