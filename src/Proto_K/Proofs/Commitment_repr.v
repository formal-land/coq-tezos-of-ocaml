Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.

Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_K.Proofs.Tez_repr.
Require TezosOfOCaml.Proto_K.Proofs.Blinded_public_key_hash.

Require TezosOfOCaml.Proto_K.Commitment_repr.

Module Valid.
  Definition t (x : Commitment_repr.t) : Prop :=
    Tez_repr.Valid.t x.(Commitment_repr.t.amount).
End Valid.

Lemma encoding_is_valid :
  Data_encoding.Valid.t Valid.t Commitment_repr.encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
