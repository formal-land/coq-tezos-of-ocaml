Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Global_constants_storage.
Require TezosOfOCaml.Proto_K.Contract_repr.
Require TezosOfOCaml.Proto_K.Storage.
Require TezosOfOCaml.Proto_K.Script_repr.

(* @TODO *)
Axiom register_get_eq : forall {ctxt} {value : Script_repr.expr}, 
  letP? '(ctxt, hash, size) := Global_constants_storage.register ctxt value in
  letP? '(ctxt, value'):= Global_constants_storage.get ctxt hash in
  value = value'.
