Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Ticket_hash_repr.

Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_K.Proofs.Script_expr_hash.

(** [encoding] function is valid *)
Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Ticket_hash_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

(** [compare] function is valid *)
Lemma compare_is_valid :
  Compare.Valid.t (fun _ => True) id Ticket_hash_repr.compare.
Proof. apply Blake2B.Make_is_valid. Qed.
#[global] Hint Resolve compare_is_valid : Compare_db.
