Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Ticket_scanner.

Require TezosOfOCaml.Proto_K.Proofs.Script_typed_ir.

Module Ex_ticket.
  Module Valid.
    Inductive t : Ticket_scanner.ex_ticket -> Prop :=
    | Intro {a} :
      forall ty ticket,
      Script_typed_ir.Ticket.Valid.t ticket ->
      t (Ticket_scanner.Ex_ticket (a := a) ty ticket).
  End Valid.
End Ex_ticket.
