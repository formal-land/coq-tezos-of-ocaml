Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Sc_rollup_repr.

Require TezosOfOCaml.Environment.V6.Proofs.Blake2B.
Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_K.Proofs.Raw_level_repr.
Require TezosOfOCaml.Proto_K.Proofs.Storage_description.
Require TezosOfOCaml.Environment.V6.Proofs.Bounded.

Module Address.
  Lemma encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True) Sc_rollup_repr.Address.encoding.
    apply Blake2B.Make_is_valid.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.

  (** [compare] function is valid *)
  Lemma compare_is_valid :
    Compare.Valid.t (fun _ => True) id Sc_rollup_repr.Address.compare.
  Proof. apply Blake2B.Make_is_valid. Qed.
  #[global] Hint Resolve compare_is_valid : Compare_db.

  (* TODO: prove if possible *)
  Axiom of_b58_opt_to_b58_eq : forall v,
    Sc_rollup_repr.Address.of_b58check_opt
      (Sc_rollup_repr.Address.to_b58check v) = return* v.

  (* TODO: prove if possible *)
  Axiom to_b58_opt_of_b58_eq : forall b58,
    match Sc_rollup_repr.Address.of_b58check_opt b58 with
    | Some v => Sc_rollup_repr.Address.to_b58check v = b58
    | None => True
    end.

  Lemma rpc_arg_valid : RPC_arg.Valid.t (fun _ => True) Sc_rollup_repr.Address.rpc_arg.
    apply Blake2B.Make_is_valid.
  Qed.
End Address.

Module State_hash.
  Lemma encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True)
      Sc_rollup_repr.State_hash.encoding.
  Proof.
    apply Blake2B.Make_is_valid.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.

  (** [compare] function is valid *)
  Lemma compare_is_valid :
    Compare.Valid.t (fun _ => True) id Sc_rollup_repr.State_hash.compare.
  Proof. apply Blake2B.Make_is_valid. Qed.
  #[global] Hint Resolve compare_is_valid : Compare_db.
End State_hash.

Axiom of_b58_to_b58_eq : forall s,
    Sc_rollup_repr.of_b58check (Sc_rollup_repr.Address.to_b58check s) = return? s.

Axiom to_b58_of_b58_eq : forall b58,
  match Sc_rollup_repr.of_b58check b58 with
  | Pervasives.Ok v => Sc_rollup_repr.Address.to_b58check v = b58
  | Pervasives.Error _ => True
  end.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Sc_rollup_repr.encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros; split; trivial.
  apply of_b58_to_b58_eq.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Lemma rpc_arg_valid : RPC_arg.Valid.t (fun _ => True) Sc_rollup_repr.rpc_arg.
  constructor; intros; simpl.
  - rewrite of_b58_to_b58_eq; reflexivity.
  - destruct (Sc_rollup_repr.of_b58check s) eqn:E; simpl; trivial.
    specialize (to_b58_of_b58_eq s); rewrite E; trivial.
Qed.

Lemma index_path_encoding_is_valid :
  Path_encoding.S.Valid.t
    (Storage_description.INDEX.to_Path Sc_rollup_repr.Index).
Proof.
  constructor;
    unfold
      Sc_rollup_repr.Index,
      Sc_rollup_repr.Index.module,
      Sc_rollup_repr.Index.to_path,
      Sc_rollup_repr.Index.of_path,
      Binary.to_bytes_exn,
      Sc_rollup_repr.Index.path_length; simpl.
  - intros; destruct (Hex.of_bytes _ _); reflexivity.
  - intro v; destruct (Hex.of_bytes _ _) eqn:E;
      rewrite <- E, Hex.to_bytes_of_bytes; clear E; simpl.
    specialize (Data_encoding.Valid.of_bytes_opt_to_bytes_opt encoding_is_valid v I).
    now destruct (Binary.to_bytes_opt _ _ _) eqn:E.
  - intro path; destruct path; trivial; destruct path; trivial.
    destruct (Hex.to_bytes _) eqn:E; simpl; trivial;
    specialize (Hex.of_bytes_to_bytes (Hex s)); rewrite E; intro;
    specialize (Data_encoding.Valid.to_bytes_opt_of_bytes_opt encoding_is_valid b);
    destruct (Binary.of_bytes_opt _ _) eqn:E1; simpl; trivial;
    intro E2; rewrite E2, H; reflexivity.
  - intros; destruct (Hex.of_bytes None _); reflexivity.
Qed.

Module Index.
  (** [compare] function is valid *)
  Lemma compare_is_valid :
    Compare.Valid.t (fun _ => True) id Sc_rollup_repr.Index.compare.
  Proof. apply Blake2B.Make_is_valid. Qed.
  #[global] Hint Resolve compare_is_valid : Compare_db.
End Index.

Module Number_of_messages.
  (** The bounds [Number_of_messages] are valid. *)
  Lemma is_valid : Bounded.Int32.S.Valid.t Sc_rollup_repr.Number_of_messages.
  Proof.
    apply Bounded.Int32.Make_is_valid.
    constructor; simpl; lia.
  Qed.

  (** The corresponding encoding is valid. *)
  Lemma encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True)
      Sc_rollup_repr.Number_of_messages.(Int32.S.encoding).
  Proof.
    apply is_valid.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Number_of_messages.

Module Number_of_ticks.
  (** The bounds [Number_of_ticks] are valid. *)
  Lemma is_valid : Bounded.Int32.S.Valid.t Sc_rollup_repr.Number_of_ticks.
  Proof.
    apply Bounded.Int32.Make_is_valid.
    constructor; simpl; lia.
  Qed.

  (** The corresponding encoding is valid. *)
  Lemma encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True)
      Sc_rollup_repr.Number_of_ticks.(Int32.S.encoding).
  Proof.
    apply is_valid.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Number_of_ticks.
