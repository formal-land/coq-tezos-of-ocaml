Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Ratio_repr.

Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.

(** Validity predicate for [t]. *)
Module Valid.
  Import Ratio_repr.t.

  Record t (x : Ratio_repr.t) : Prop := {
    numerator : Pervasives.UInt16.Valid.t x.(numerator);
    denominator :
      Pervasives.UInt16.Valid.t x.(denominator) /\
      x.(denominator) <> 0;
  }.
End Valid.

(** The encoding [encoding] is valid. *)
Lemma encoding_is_valid : Data_encoding.Valid.t Valid.t Ratio_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
  sauto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
