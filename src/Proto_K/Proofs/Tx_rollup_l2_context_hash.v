Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Tx_rollup_l2_context_hash.

Require TezosOfOCaml.Environment.V6.Proofs.Blake2B.

(** The encoding [encoding] is valid. *)
Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Tx_rollup_l2_context_hash.encoding.
Proof.
  apply Blake2B.Make_is_valid.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

(** [compare] function is valid *)
Lemma compare_is_valid :
  Compare.Valid.t (fun _ => True) id Tx_rollup_l2_context_hash.compare.
Proof.
  apply Blake2B.Make_is_valid.
Qed.
#[global] Hint Resolve compare_is_valid : Compare_db.
