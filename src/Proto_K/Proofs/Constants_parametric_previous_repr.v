Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Constants_parametric_previous_repr.

Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_K.Proofs.Gas_limit_repr.
Require TezosOfOCaml.Proto_K.Proofs.Period_repr.
Require TezosOfOCaml.Proto_K.Proofs.Ratio_repr.
Require TezosOfOCaml.Proto_K.Proofs.State_hash.
Require TezosOfOCaml.Proto_K.Proofs.Tez_repr.

(** The validity predicate for the type [t]. *)
Module Valid.
  Import Constants_parametric_previous_repr.t.

  Record t (x : Constants_parametric_previous_repr.t) : Prop := {
    preserved_cycles :
      Pervasives.UInt8.Valid.t x.(preserved_cycles);
    blocks_per_cycle :
      Int32.Valid.t x.(blocks_per_cycle);
    blocks_per_commitment :
      Int32.Valid.t x.(blocks_per_commitment);
    blocks_per_stake_snapshot :
      Int32.Valid.t x.(blocks_per_stake_snapshot);
    cycles_per_voting_period :
      Int32.Valid.t x.(cycles_per_voting_period);
    hard_gas_limit_per_operation :
      Gas_limit_repr.Arith.Integral.Valid.t x.(hard_gas_limit_per_operation);
    hard_gas_limit_per_block :
      Gas_limit_repr.Arith.Integral.Valid.t x.(hard_gas_limit_per_block);
    proof_of_work_threshold :
      Int64.Valid.t x.(proof_of_work_threshold);
    tokens_per_roll :
      Tez_repr.Valid.t x.(tokens_per_roll);
    seed_nonce_revelation_tip :
      Tez_repr.Valid.t x.(seed_nonce_revelation_tip);
    origination_size :
      Pervasives.Int31.Valid.t x.(origination_size);
    baking_reward_fixed_portion :
      Tez_repr.Valid.t x.(baking_reward_fixed_portion);
    baking_reward_bonus_per_slot :
      Tez_repr.Valid.t x.(baking_reward_bonus_per_slot);
    endorsing_reward_per_slot :
      Tez_repr.Valid.t x.(endorsing_reward_per_slot);
    cost_per_byte :
      Tez_repr.Valid.t x.(cost_per_byte);
    quorum_min :
      Int32.Valid.t x.(quorum_min);
    quorum_max :
      Int32.Valid.t x.(quorum_max);
    min_proposal_quorum :
      Int32.Valid.t x.(min_proposal_quorum);
    liquidity_baking_subsidy :
      Tez_repr.Valid.t x.(liquidity_baking_subsidy);
    liquidity_baking_sunset_level :
      Int32.Valid.t x.(liquidity_baking_sunset_level);
    liquidity_baking_toggle_ema_threshold :
      Int32.Valid.t x.(liquidity_baking_toggle_ema_threshold);
    max_operations_time_to_live :
      Pervasives.Int16.Valid.t x.(max_operations_time_to_live);
    minimal_block_delay :
      Period_repr.Valid.t x.(minimal_block_delay);
    delay_increment_per_round :
      Period_repr.Valid.t x.(delay_increment_per_round);
    minimal_participation_ratio :
      Ratio_repr.Valid.t x.(minimal_participation_ratio);
    consensus_committee_size :
      Pervasives.Int31.Valid.t x.(consensus_committee_size);
    consensus_threshold :
      Pervasives.Int31.Valid.t x.(consensus_threshold);
    max_slashing_period :
      Pervasives.Int31.Valid.t x.(max_slashing_period);
    frozen_deposits_percentage :
      Pervasives.Int31.Valid.t x.(frozen_deposits_percentage);
    double_baking_punishment :
      Tez_repr.Valid.t x.(double_baking_punishment);
    ratio_of_frozen_deposits_slashed_per_double_endorsement :
      Ratio_repr.Valid.t x.(ratio_of_frozen_deposits_slashed_per_double_endorsement);
    cache_script_size :
      Pervasives.Int31.Valid.t x.(cache_script_size);
    cache_stake_distribution_cycles :
      Pervasives.Int8.Valid.t x.(cache_stake_distribution_cycles);
    cache_sampler_state_cycles :
      Pervasives.Int8.Valid.t x.(cache_sampler_state_cycles);
    tx_rollup_origination_size :
      Pervasives.Int31.Valid.t x.(tx_rollup_origination_size);
    tx_rollup_hard_size_limit_per_inbox :
      Pervasives.Int31.Valid.t x.(tx_rollup_hard_size_limit_per_inbox);
    tx_rollup_hard_size_limit_per_message :
      Pervasives.Int31.Valid.t x.(tx_rollup_hard_size_limit_per_message);
    tx_rollup_commitment_bond :
      Tez_repr.Valid.t x.(tx_rollup_commitment_bond);
    tx_rollup_finality_period :
      Pervasives.Int31.Valid.t x.(tx_rollup_finality_period);
    tx_rollup_withdraw_period :
      Pervasives.Int31.Valid.t x.(tx_rollup_withdraw_period);
    tx_rollup_max_inboxes_count :
      Pervasives.Int31.Valid.t x.(tx_rollup_max_inboxes_count);
    tx_rollup_max_messages_per_inbox :
      Pervasives.Int31.Valid.t x.(tx_rollup_max_messages_per_inbox);
    tx_rollup_max_commitments_count :
      Pervasives.Int31.Valid.t x.(tx_rollup_max_commitments_count);
    tx_rollup_cost_per_byte_ema_factor :
      Pervasives.Int31.Valid.t x.(tx_rollup_cost_per_byte_ema_factor);
    tx_rollup_max_ticket_payload_size :
      Pervasives.Int31.Valid.t x.(tx_rollup_max_ticket_payload_size);
    tx_rollup_max_withdrawals_per_batch :
      Pervasives.Int31.Valid.t x.(tx_rollup_max_withdrawals_per_batch);
    tx_rollup_rejection_max_proof_size :
      Pervasives.Int31.Valid.t x.(tx_rollup_rejection_max_proof_size);
    tx_rollup_sunset_level :
      Int32.Valid.t x.(tx_rollup_sunset_level);
    sc_rollup_origination_size :
      Pervasives.Int31.Valid.t x.(sc_rollup_origination_size);
    sc_rollup_challenge_window_in_blocks :
      Pervasives.Int31.Valid.t x.(sc_rollup_challenge_window_in_blocks);
    sc_rollup_max_available_messages :
      Pervasives.Int31.Valid.t x.(sc_rollup_max_available_messages);
  }.
End Valid.

(** The encoding [encoding] is valid. *)
Lemma encoding_is_valid :
  Data_encoding.Valid.t Valid.t Constants_parametric_previous_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
  intros x H.
  intuition; try (destruct H; assumption).
  hauto l: on.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
