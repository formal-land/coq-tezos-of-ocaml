Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.

Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Environment.V6.Proofs.RPC_service.

Require TezosOfOCaml.Proto_K.Tx_rollup_services.

Require TezosOfOCaml.Proto_K.Proofs.Raw_level_repr.
Require TezosOfOCaml.Proto_K.Proofs.Tez_repr.
Require TezosOfOCaml.Proto_K.Proofs.Tx_rollup_state_repr.

Module S.
  Lemma state_value_is_valid :
    RPC_service.Valid.t (fun _ => True) (fun _ => True)
      Tx_rollup_state_repr.Valid.t
      Tx_rollup_services.S.state_value.
  Proof.
    RPC_service.rpc_auto;
    intros [] []; repeat split; trivial.
  Qed.
End S.
