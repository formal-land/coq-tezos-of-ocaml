Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require Import TezosOfOCaml.Environment.V6.Proofs.Bls_signature.

Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_K.Tx_rollup_l2_context_sig.

Module Valid.
  Definition t := Bls_signature.Valid.t.
End Valid.
          
Lemma signature_encoding_is_valid :
  Data_encoding.Valid.t Valid.t Tx_rollup_l2_context_sig.signature_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
  intros.
  unfold Valid.t in H.
  split. trivial.
  pose proof (signature_valid_of_bytes_opt_to_bytes x H).
  rewrite H0. reflexivity. 
Qed.
#[global] Hint Resolve signature_encoding_is_valid : Data_encoding_db.
