Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V6.
Require TezosOfOCaml.Proto_K.Tx_rollup_withdraw_list_hash_repr.

Require TezosOfOCaml.Environment.V6.Proofs.Blake2B.
Require TezosOfOCaml.Environment.V6.Proofs.Compare.
Require TezosOfOCaml.Environment.V6.Proofs.Data_encoding.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True)
    Tx_rollup_withdraw_list_hash_repr.encoding.
Proof.
  apply Blake2B.Make_is_valid.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

(** The [compare] function is valid. *)
Lemma compare_is_valid :
  Compare.Valid.t (fun _ => True) id Tx_rollup_withdraw_list_hash_repr.compare.
Proof.
  apply Blake2B.Make_is_valid.
Qed.
#[global] Hint Resolve compare_is_valid : Compare_db.
