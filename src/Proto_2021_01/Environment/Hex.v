Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_2021_01.Environment.Format.

Inductive t : Set :=
| Hex : string -> t.

Parameter of_char : ascii -> ascii * ascii.

Parameter to_char : ascii -> ascii -> ascii.

Parameter of_string : option (list ascii) -> string -> t.

Parameter to_string : t -> string.

Parameter of_bytes : option (list ascii) -> bytes -> t.

Parameter to_bytes : t -> bytes.

Parameter hexdump_s : option bool -> option bool -> t -> string.

Parameter pp : Format.formatter -> t -> unit.

Parameter show : t -> string.
