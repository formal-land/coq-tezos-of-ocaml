Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_2021_01.Environment.Compare.
Require Proto_2021_01.Environment.List.
Require Proto_2021_01.Environment.S.

(** We define a simple version of maps, using sorted lists. *)
Module Make.
  Class FArgs {t : Set} := {
    Ord : Compare.COMPARABLE (t := t);
  }.
  Arguments Build_FArgs {_}.

  Definition key `{FArgs} : Set := Ord.(Compare.COMPARABLE.t).

  Definition t `{FArgs} (a : Set) : Set := list (key * a).

  Definition compare_keys `{FArgs} (k1 k2 : key) : comparison :=
    let z := Ord.(Compare.COMPARABLE.compare) k1 k2 in
    if Z.eqb z 0 then
      Eq
    else if Z.leb z 0 then
      Lt
    else
      Gt.

  Definition empty `{FArgs} {a : Set} : t a :=
    [].

  Definition is_empty `{FArgs} {a : Set} (m : t a) : bool :=
    match m with
    | [] => true
    | _ :: _ => false
    end.

  Fixpoint mem `{FArgs} {a : Set} (k : key) (m : t a) : bool :=
    match m with
    | [] => false
    | (k', _) :: m =>
      match compare_keys k k' with
      | Lt => false
      | Eq => true
      | Gt => mem k m
      end
    end.

  Fixpoint find_opt `{FArgs} {a : Set} (k : key) (m : t a) : option a :=
    match m with
    | [] => None
    | (k', v') :: m' =>
      match compare_keys k k' with
      | Lt => None
      | Eq => Some v'
      | Gt => find_opt k m'
      end
    end.

  Fixpoint add `{FArgs} {a : Set} (k : key) (v : a) (m : t a) : t a :=
    match m with
    | [] => [(k, v)]
    | (k', v') :: m' =>
      match compare_keys k k' with
      | Lt => (k', v') :: add k v m'
      | Eq => (k, v) :: m'
      | Gt => (k, v) :: m
      end
    end.

  Fixpoint remove `{FArgs} {a : Set} (k : key) (m : t a) : t a :=
    match m with
    | [] => m
    | (k', v') :: m' =>
      match compare_keys k k' with
      | Lt => m
      | Eq => m'
      | Gt => (k', v') :: remove k m'
      end
    end.

  Definition update `{FArgs} {a : Set} (k : key) (f : option a -> option a)
    (m : t a) : t a :=
    match f (find_opt k m) with
    | None => remove k m
    | Some v' => add k v' m
    end.

  Definition singleton `{FArgs} {a : Set} (k : key) (v : a) : t a :=
    [(k, v)].

  Fixpoint pick_opt `{FArgs} {a : Set} (k : key) (m : t a) : option a * t a :=
    match m with
    | [] => (None, m)
    | (k', v') :: m' =>
      match compare_keys k k' with
      | Lt => (None, m)
      | Eq => (Some v', m')
      | Gt =>
        let '(v, m') := pick_opt k m' in
        (v, (k', v') :: m')
      end
    end.

  Fixpoint merge `{FArgs} {a b c : Set}
    (f : key -> option a -> option b -> option c) (m1 : t a) (m2 : t b) : t c :=
    match m1 with
    | [] =>
      List.filter_map
        (fun '(k2, v2) =>
          match f k2 None (Some v2) with
          | None => None
          | Some v => Some (k2, v)
          end
        )
        m2
    | (k1, v1) :: m1 =>
      let '(v2, m2) := pick_opt k1 m2 in
      let m := merge f m1 m2 in
      match f k1 (Some v1) v2 with
      | None => m
      | Some v => add k1 v m
      end
    end.

  Definition union `{FArgs} {a : Set} (f : key -> a -> a -> option a)
    (m1 m2 : t a) : t a :=
    merge
      (fun k v1 v2 =>
        match v1, v2 with
        | None, None => None
        | Some v1, None => Some v1
        | None, Some v2 => Some v2
        | Some v1, Some v2 => f k v1 v2
        end
      )
      m1 m2.

  Fixpoint compare `{FArgs} {a : Set} (f : a -> a -> int) (m1 m2 : t a) : int :=
    match m1, m2 with
    | [], [] => 0
    | _ :: _, [] => 1
    | [], _ :: _ => -1
    | (k1, v1) :: m1, (k2, v2) :: m2 =>
      match compare_keys k1 k2 with
      | Lt => -1
      | Eq =>
        let compare_values := f v1 v2 in
        if Z.eqb compare_values 0 then
          compare f m1 m2
        else
          compare_values
      | Gt => 1
      end
    end.

  Fixpoint equal `{FArgs} {a : Set} (f : a -> a -> bool) (m1 m2 : t a) : bool :=
    match m1, m2 with
    | [], [] => true
    | _ :: _, [] | [], _ :: _ => false
    | (k1, v1) :: m1, (k2, v2) :: m2 =>
      match compare_keys k1 k2 with
      | Eq =>
        if f v1 v2 then
          equal f m1 m2
        else
          false
      | _ => false
      end
    end.

  Fixpoint iter `{FArgs} {a : Set} (f : key -> a -> unit) (m : t a) : unit :=
    match m with
    | [] => tt
    | (k, v) :: m =>
      let _ : unit := f k v in
      iter f m
    end.

  Fixpoint fold `{FArgs} {a b : Set} (f : key -> a -> b -> b) (m : t a)
    (acc : b) : b :=
    match m with
    | [] => acc
    | (k, v) :: m =>
      let acc := f k v acc in
      fold f m acc
    end.

  Fixpoint for_all `{FArgs} {a : Set} (p : key -> a -> bool) (m : t a) : bool :=
    match m with
    | [] => true
    | (k, v) :: m => p k v && for_all p m
    end.

  Fixpoint _exists `{FArgs} {a : Set} (p : key -> a -> bool) (m : t a) : bool :=
    match m with
    | [] => false
    | (k, v) :: m => p k v || _exists p m
    end.

  Fixpoint filter `{FArgs} {a : Set} (p : key -> a -> bool) (m : t a) : t a :=
    match m with
    | [] => m
    | (k, v) :: m =>
      let m := filter p m in
      if p k v then
        (k, v) :: m
      else
        m
    end.

  Fixpoint partition `{FArgs} {a : Set} (p : key -> a -> bool) (m : t a)
    : t a * t a :=
    match m with
    | [] => ([], [])
    | (k, v) :: m =>
      let '(m_true, m_false) := partition p m in
      if p k v then
        ((k, v) :: m_true, m_false)
      else
        (m_true, (k, v) :: m_false)
    end.

  Definition cardinal `{FArgs} {a : Set} (m : t a) : int :=
    List.length m.

  Definition bindings `{FArgs} {a : Set} (m : t a) : list (key * a) :=
    m.

  Definition min_binding_opt `{FArgs} {a : Set} (m : t a) : option (key * a) :=
    match m with
    | [] => None
    | binding :: _ => Some binding
    end.

  Fixpoint max_binding_opt `{FArgs} {a : Set} (m : t a) : option (key * a) :=
    match m with
    | [] => None
    | [binding] => Some binding
    | _ :: (_ :: _) as m => max_binding_opt m
    end.

  Definition choose_opt : forall `{FArgs} {a : Set}, t a -> option (key * a) :=
    @min_binding_opt.

  Fixpoint split `{FArgs} {a : Set} (k : key) (m : t a)
    : t a * option a * t a :=
    match m with
    | [] => ([], None, [])
    | (k', v') :: m' =>
      match compare_keys k k' with
      | Lt => ([], None, m)
      | Eq => ([], Some v', m')
      | Gt =>
        let '(m_lt, v, m_gt) := split k m' in
        ((k', v') :: m_lt, v, m_gt)
      end
    end.

  Fixpoint find_first_opt `{FArgs} {a : Set} (p : key -> bool) (m : t a)
    : option (key * a) :=
    match m with
    | [] => None
    | (k, v) :: m =>
      if p k then
        Some (k, v)
      else
        find_first_opt p m
    end.

  Fixpoint find_last_opt `{FArgs} {a : Set} (p : key -> bool) (m : t a)
    : option (key * a) :=
    match m with
    | [] => None
    | (k, v) :: m =>
      match find_last_opt p m with
      | None =>
        if p k then
          Some (k, v)
        else
          None
      | Some _ as result => result
      end
    end.

  Fixpoint map `{FArgs} {a b : Set} (f : a -> b) (m : t a) : t b :=
    match m with
    | [] => []
    | (k, v) :: m => (k, f v) :: map f m
    end.

  Fixpoint mapi `{FArgs} {a b : Set} (f : key -> a -> b) (m : t a) : t b :=
    match m with
    | [] => []
    | (k, v) :: m => (k, f k v) :: mapi f m
    end.

  Definition functor `(FArgs) :=
    {|
      S.MAP.empty _ := empty;
      S.MAP.is_empty _ := is_empty;
      S.MAP.mem _ := mem;
      S.MAP.add _ := add;
      S.MAP.update _ := update;
      S.MAP.singleton _ := singleton;
      S.MAP.remove _ := remove;
      S.MAP.merge _ _ _:= merge;
      S.MAP.union _ := union;
      S.MAP.compare _ := compare;
      S.MAP.equal _ := equal;
      S.MAP.iter _ := iter;
      S.MAP.fold _ _ := fold;
      S.MAP.for_all _ := for_all;
      S.MAP._exists _ := _exists;
      S.MAP.filter _ := filter;
      S.MAP.partition _ := partition;
      S.MAP.cardinal _ := cardinal;
      S.MAP.bindings _ := bindings;
      S.MAP.min_binding_opt _ := min_binding_opt;
      S.MAP.max_binding_opt _ := max_binding_opt;
      S.MAP.choose_opt _ := choose_opt;
      S.MAP.split _ := split;
      S.MAP.find_opt _ := find_opt;
      S.MAP.find_first_opt _ := find_first_opt;
      S.MAP.find_last_opt _ := find_last_opt;
      S.MAP.map _ _ := map;
      S.MAP.mapi _ _ := mapi;
    |}.
End Make.

Definition Make_t {Ord_t : Set}
  (Ord : Compare.COMPARABLE (t := Ord_t)) :=
  let '_ := Make.Build_FArgs Ord in
  Make.t.

Definition Make {Ord_t : Set}
  (Ord : Compare.COMPARABLE (t := Ord_t))
  : S.MAP (key := Ord.(Compare.COMPARABLE.t)) (t := Make_t Ord) :=
  Make.functor (Make.Build_FArgs Ord).
