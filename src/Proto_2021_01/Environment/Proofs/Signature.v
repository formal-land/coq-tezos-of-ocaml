Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.

Axiom Public_key_hash_compare_refl
: forall public_key_hash,
  Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare)
    public_key_hash public_key_hash = 0.

Axiom Public_key_compare_refl
: forall public_key,
  Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.compare) public_key public_key =
  0.

Axiom compare_refl : forall signature,
  Signature.compare signature signature = 0.

Axiom of_b58check_opt_to_b58check
  : forall signature,
    Signature.of_b58check_opt (Signature.to_b58check signature) =
    Some signature.
