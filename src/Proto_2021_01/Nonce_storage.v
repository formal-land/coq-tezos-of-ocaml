Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Cycle_repr.
Require TezosOfOCaml.Proto_2021_01.Level_repr.
Require TezosOfOCaml.Proto_2021_01.Level_storage.
Require TezosOfOCaml.Proto_2021_01.Nonce_hash.
Require TezosOfOCaml.Proto_2021_01.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Seed_repr.
Require TezosOfOCaml.Proto_2021_01.Storage.
Require TezosOfOCaml.Proto_2021_01.Storage_sigs.

Definition t : Set := Seed_repr.nonce.

Definition nonce : Set := t.

Definition encoding : Data_encoding.t Seed_repr.nonce :=
  Seed_repr.nonce_encoding.

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Branch
      "nonce.too_late_revelation" "Too late nonce revelation"
      "Nonce revelation happens too late"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "This nonce cannot be revealed anymore."
                  CamlinternalFormatBasics.End_of_format)
                "This nonce cannot be revealed anymore.")))
      Data_encoding.unit_value
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Too_late_revelation" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Too_late_revelation" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "nonce.too_early_revelation" "Too early nonce revelation"
      "Nonce revelation happens before cycle end"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "This nonce should not yet be revealed"
                  CamlinternalFormatBasics.End_of_format)
                "This nonce should not yet be revealed")))
      Data_encoding.unit_value
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Too_early_revelation" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Too_early_revelation" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Branch
      "nonce.previously_revealed" "Previously revealed nonce"
      "Duplicated revelation for a nonce."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "This nonce was previously revealed"
                  CamlinternalFormatBasics.End_of_format)
                "This nonce was previously revealed"))) Data_encoding.unit_value
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Previously_revealed_nonce" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Previously_revealed_nonce" unit tt) in
  Error_monad.register_error_kind Error_monad.Branch "nonce.unexpected"
    "Unexpected nonce"
    "The provided nonce is inconsistent with the committed nonce hash."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "This nonce revelation is invalid (inconsistent with the committed hash)"
                CamlinternalFormatBasics.End_of_format)
              "This nonce revelation is invalid (inconsistent with the committed hash)")))
    Data_encoding.unit_value
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Unexpected_nonce" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Unexpected_nonce" unit tt).

Definition get_unrevealed (ctxt : Raw_context.t) (level : Level_repr.t)
  : M=? Storage.Seed.unrevealed_nonce :=
  let cur_level := Level_storage.current ctxt in
  match Cycle_repr.pred cur_level.(Level_repr.t.cycle) with
  | None => Error_monad.fail (Build_extensible "Too_early_revelation" unit tt)
  | Some revealed_cycle =>
    if Cycle_repr.op_lt revealed_cycle level.(Level_repr.t.cycle) then
      Error_monad.fail (Build_extensible "Too_early_revelation" unit tt)
    else
      if Cycle_repr.op_lt level.(Level_repr.t.cycle) revealed_cycle then
        Error_monad.fail (Build_extensible "Too_late_revelation" unit tt)
      else
        let=? function_parameter :=
          Storage.Seed.Nonce.(Storage_sigs.Non_iterable_indexed_data_storage.get)
            ctxt level in
        match function_parameter with
        | Storage.Cycle.Revealed _ =>
          Error_monad.fail
            (Build_extensible "Previously_revealed_nonce" unit tt)
        | Storage.Cycle.Unrevealed status => Error_monad._return status
        end
  end.

Definition record_hash
  (ctxt : Raw_context.t) (unrevealed : Storage.Seed.unrevealed_nonce)
  : M=? Raw_context.t :=
  let level := Level_storage.current ctxt in
  Storage.Seed.Nonce.(Storage_sigs.Non_iterable_indexed_data_storage.init_value)
    ctxt level (Storage.Cycle.Unrevealed unrevealed).

Definition reveal
  (ctxt : Raw_context.t) (level : Level_repr.t) (nonce_value : Seed_repr.nonce)
  : M=? Raw_context.t :=
  let=? unrevealed := get_unrevealed ctxt level in
  let=? '_ :=
    return=
      (Error_monad.error_unless
        (Seed_repr.check_hash nonce_value
          unrevealed.(Storage.Cycle.unrevealed_nonce.nonce_hash))
        (Build_extensible "Unexpected_nonce" unit tt)) in
  Storage.Seed.Nonce.(Storage_sigs.Non_iterable_indexed_data_storage.set) ctxt
    level (Storage.Cycle.Revealed nonce_value).

Definition unrevealed : Set := Storage.Seed.unrevealed_nonce.

Definition status : Set := Storage.Seed.nonce_status.

Definition get
  : Raw_context.t -> Level_repr.t -> M=? Storage.Seed.nonce_status :=
  Storage.Seed.Nonce.(Storage_sigs.Non_iterable_indexed_data_storage.get).

Definition of_bytes : bytes -> M? Seed_repr.nonce := Seed_repr.make_nonce.

Definition hash_value : Seed_repr.nonce -> Nonce_hash.t := Seed_repr.hash_value.

Definition check_hash : Seed_repr.nonce -> Nonce_hash.t -> bool :=
  Seed_repr.check_hash.
