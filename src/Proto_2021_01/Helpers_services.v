Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Apply.
Require TezosOfOCaml.Proto_2021_01.Apply_results.
Require TezosOfOCaml.Proto_2021_01.Constants_repr.
Require TezosOfOCaml.Proto_2021_01.Contract_services.
Require TezosOfOCaml.Proto_2021_01.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_2021_01.Nonce_hash.
Require TezosOfOCaml.Proto_2021_01.Script_interpreter.
Require TezosOfOCaml.Proto_2021_01.Script_ir_translator.
Require TezosOfOCaml.Proto_2021_01.Script_tc_errors.
Require TezosOfOCaml.Proto_2021_01.Script_tc_errors_registration.
Require TezosOfOCaml.Proto_2021_01.Script_typed_ir.
Require TezosOfOCaml.Proto_2021_01.Services_registration.

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  Error_monad.register_error_kind Error_monad.Branch "operation.cannot_parse"
    "Cannot parse operation"
    "The operation is ill-formed or for another protocol version"
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "The operation cannot be parsed"
                CamlinternalFormatBasics.End_of_format)
              "The operation cannot be parsed"))) Data_encoding.unit_value
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Cannot_parse_operation" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Cannot_parse_operation" unit tt).

Definition parse_operation (op : Alpha_context.Operation.raw)
  : M? Alpha_context.packed_operation :=
  match
    Data_encoding.Binary.of_bytes Alpha_context.Operation.protocol_data_encoding
      op.(Operation.t.proto) with
  | Some protocol_data =>
    return?
      {| Alpha_context.packed_operation.shell := op.(Operation.t.shell);
        Alpha_context.packed_operation.protocol_data := protocol_data |}
  | None =>
    Error_monad.error_value (Build_extensible "Cannot_parse_operation" unit tt)
  end.

Definition path : RPC_path.path Updater.rpc_context Updater.rpc_context :=
  RPC_path.op_div RPC_path.open_root "helpers".

Module Scripts.
  Module S.
    Definition path : RPC_path.path Updater.rpc_context Updater.rpc_context :=
      RPC_path.op_div path "scripts".
    
    Definition run_code_input_encoding
      : Data_encoding.encoding
        (Alpha_context.Script.expr * Alpha_context.Script.expr *
          Alpha_context.Script.expr * Alpha_context.Tez.t * Alpha_context.Tez.t
          * Chain_id.t * option Alpha_context.Contract.t *
          option Alpha_context.Contract.t *
          option Alpha_context.Gas.Arith.integral * string) :=
      Data_encoding.obj10
        (Data_encoding.req None None "script" Alpha_context.Script.expr_encoding)
        (Data_encoding.req None None "storage"
          Alpha_context.Script.expr_encoding)
        (Data_encoding.req None None "input" Alpha_context.Script.expr_encoding)
        (Data_encoding.req None None "amount" Alpha_context.Tez.encoding)
        (Data_encoding.req None None "balance" Alpha_context.Tez.encoding)
        (Data_encoding.req None None "chain_id" Chain_id.encoding)
        (Data_encoding.opt None None "source" Alpha_context.Contract.encoding)
        (Data_encoding.opt None None "payer" Alpha_context.Contract.encoding)
        (Data_encoding.opt None None "gas"
          Alpha_context.Gas.Arith.z_integral_encoding)
        (Data_encoding.dft None None "entrypoint" Data_encoding.string_value
          "default").
    
    Definition trace_encoding
      : Data_encoding.encoding
        (list
          (Alpha_context.Script.location * Alpha_context.Gas.t *
            list (Alpha_context.Script.expr * option string))) :=
      (let arg := Data_encoding.def "scripted.trace" in
      fun (eta :
        Data_encoding.encoding
          (list
            (Alpha_context.Script.location * Alpha_context.Gas.t *
              list (Alpha_context.Script.expr * option string)))) =>
        arg None None eta)
        ((let arg := Data_encoding.list_value in
        fun (eta :
          Data_encoding.encoding
            (Alpha_context.Script.location * Alpha_context.Gas.t *
              list (Alpha_context.Script.expr * option string))) => arg None eta)
          (Data_encoding.obj3
            (Data_encoding.req None None "location"
              Alpha_context.Script.location_encoding)
            (Data_encoding.req None None "gas" Alpha_context.Gas.encoding)
            (Data_encoding.req None None "stack"
              (Data_encoding.list_value None
                (Data_encoding.obj2
                  (Data_encoding.req None None "item"
                    Alpha_context.Script.expr_encoding)
                  (Data_encoding.opt None None "annot"
                    Data_encoding.string_value)))))).
    
    Definition run_code
      : RPC_service.service Updater.rpc_context Updater.rpc_context unit
        (Alpha_context.Script.expr * Alpha_context.Script.expr *
          Alpha_context.Script.expr * Alpha_context.Tez.t * Alpha_context.Tez.t
          * Chain_id.t * option Alpha_context.Contract.t *
          option Alpha_context.Contract.t *
          option Alpha_context.Gas.Arith.integral * string)
        (Alpha_context.Script.expr *
          list Alpha_context.packed_internal_operation *
          option Alpha_context.Lazy_storage.diffs) :=
      RPC_service.post_service
        (Some "Run a piece of code in the current context") RPC_query.empty
        run_code_input_encoding
        (Data_encoding.conv
          (fun (function_parameter :
            Alpha_context.Script.expr *
              list Alpha_context.packed_internal_operation *
              option Alpha_context.Lazy_storage.diffs) =>
            let '(storage_value, operations, lazy_storage_diff) :=
              function_parameter in
            (storage_value, operations, lazy_storage_diff, lazy_storage_diff))
          (fun (function_parameter :
            Alpha_context.Script.expr *
              list Alpha_context.packed_internal_operation *
              option Alpha_context.Lazy_storage.diffs *
              option Alpha_context.Lazy_storage.diffs) =>
            let
              '(storage_value, operations, legacy_lazy_storage_diff,
                lazy_storage_diff) := function_parameter in
            let lazy_storage_diff :=
              Option.first_some lazy_storage_diff legacy_lazy_storage_diff in
            (storage_value, operations, lazy_storage_diff)) None
          (Data_encoding.obj4
            (Data_encoding.req None None "storage"
              Alpha_context.Script.expr_encoding)
            (Data_encoding.req None None "operations"
              (Data_encoding.list_value None
                Alpha_context.Operation.internal_operation_encoding))
            (Data_encoding.opt None None "big_map_diff"
              Alpha_context.Lazy_storage.legacy_big_map_diff_encoding)
            (Data_encoding.opt None None "lazy_storage_diff"
              Alpha_context.Lazy_storage.encoding)))
        (RPC_path.op_div path "run_code").
    
    Definition trace_code
      : RPC_service.service Updater.rpc_context Updater.rpc_context unit
        (Alpha_context.Script.expr * Alpha_context.Script.expr *
          Alpha_context.Script.expr * Alpha_context.Tez.t * Alpha_context.Tez.t
          * Chain_id.t * option Alpha_context.Contract.t *
          option Alpha_context.Contract.t *
          option Alpha_context.Gas.Arith.integral * string)
        (Alpha_context.Script.expr *
          list Alpha_context.packed_internal_operation *
          list
            (Alpha_context.Script.location * Alpha_context.Gas.t *
              list (Alpha_context.Script.expr * option string)) *
          option Alpha_context.Lazy_storage.diffs) :=
      RPC_service.post_service
        (Some "Run a piece of code in the current context, keeping a trace")
        RPC_query.empty run_code_input_encoding
        (Data_encoding.conv
          (fun (function_parameter :
            Alpha_context.Script.expr *
              list Alpha_context.packed_internal_operation *
              list
                (Alpha_context.Script.location * Alpha_context.Gas.t *
                  list (Alpha_context.Script.expr * option string)) *
              option Alpha_context.Lazy_storage.diffs) =>
            let '(storage_value, operations, trace_value, lazy_storage_diff) :=
              function_parameter in
            (storage_value, operations, trace_value, lazy_storage_diff,
              lazy_storage_diff))
          (fun (function_parameter :
            Alpha_context.Script.expr *
              list Alpha_context.packed_internal_operation *
              list
                (Alpha_context.Script.location * Alpha_context.Gas.t *
                  list (Alpha_context.Script.expr * option string)) *
              option Alpha_context.Lazy_storage.diffs *
              option Alpha_context.Lazy_storage.diffs) =>
            let
              '(storage_value, operations, trace_value,
                legacy_lazy_storage_diff, lazy_storage_diff) :=
              function_parameter in
            let lazy_storage_diff :=
              Option.first_some lazy_storage_diff legacy_lazy_storage_diff in
            (storage_value, operations, trace_value, lazy_storage_diff)) None
          (Data_encoding.obj5
            (Data_encoding.req None None "storage"
              Alpha_context.Script.expr_encoding)
            (Data_encoding.req None None "operations"
              (Data_encoding.list_value None
                Alpha_context.Operation.internal_operation_encoding))
            (Data_encoding.req None None "trace" trace_encoding)
            (Data_encoding.opt None None "big_map_diff"
              Alpha_context.Lazy_storage.legacy_big_map_diff_encoding)
            (Data_encoding.opt None None "lazy_storage_diff"
              Alpha_context.Lazy_storage.encoding)))
        (RPC_path.op_div path "trace_code").
    
    Definition typecheck_code
      : RPC_service.service Updater.rpc_context Updater.rpc_context unit
        (Alpha_context.Script.expr * option Alpha_context.Gas.Arith.integral *
          option bool)
        (list
          (Alpha_context.Script.location *
            (list (Alpha_context.Script.expr * list string) *
              list (Alpha_context.Script.expr * list string))) *
          Alpha_context.Gas.t) :=
      RPC_service.post_service
        (Some "Typecheck a piece of code in the current context")
        RPC_query.empty
        (Data_encoding.obj3
          (Data_encoding.req None None "program"
            Alpha_context.Script.expr_encoding)
          (Data_encoding.opt None None "gas"
            Alpha_context.Gas.Arith.z_integral_encoding)
          (Data_encoding.opt None None "legacy" Data_encoding.bool_value))
        (Data_encoding.obj2
          (Data_encoding.req None None "type_map"
            Script_tc_errors_registration.type_map_enc)
          (Data_encoding.req None None "gas" Alpha_context.Gas.encoding))
        (RPC_path.op_div path "typecheck_code").
    
    Definition typecheck_data
      : RPC_service.service Updater.rpc_context Updater.rpc_context unit
        (Alpha_context.Script.expr * Alpha_context.Script.expr *
          option Alpha_context.Gas.Arith.integral * option bool)
        Alpha_context.Gas.t :=
      RPC_service.post_service
        (Some
          "Check that some data expression is well formed and of a given type in the current context")
        RPC_query.empty
        (Data_encoding.obj4
          (Data_encoding.req None None "data" Alpha_context.Script.expr_encoding)
          (Data_encoding.req None None "type" Alpha_context.Script.expr_encoding)
          (Data_encoding.opt None None "gas"
            Alpha_context.Gas.Arith.z_integral_encoding)
          (Data_encoding.opt None None "legacy" Data_encoding.bool_value))
        (Data_encoding.obj1
          (Data_encoding.req None None "gas" Alpha_context.Gas.encoding))
        (RPC_path.op_div path "typecheck_data").
    
    Definition pack_data
      : RPC_service.service Updater.rpc_context Updater.rpc_context unit
        (Alpha_context.Script.expr * Alpha_context.Script.expr *
          option Alpha_context.Gas.Arith.integral) (bytes * Alpha_context.Gas.t) :=
      RPC_service.post_service
        (Some
          "Computes the serialized version of some data expression using the same algorithm as script instruction PACK")
        RPC_query.empty
        (Data_encoding.obj3
          (Data_encoding.req None None "data" Alpha_context.Script.expr_encoding)
          (Data_encoding.req None None "type" Alpha_context.Script.expr_encoding)
          (Data_encoding.opt None None "gas"
            Alpha_context.Gas.Arith.z_integral_encoding))
        (Data_encoding.obj2
          (Data_encoding.req None None "packed" Data_encoding.bytes_value)
          (Data_encoding.req None None "gas" Alpha_context.Gas.encoding))
        (RPC_path.op_div path "pack_data").
    
    Definition run_operation
      : RPC_service.service Updater.rpc_context Updater.rpc_context unit
        (Alpha_context.Operation.packed * Chain_id.t)
        (Alpha_context.Operation.packed_protocol_data *
          Apply_results.packed_operation_metadata) :=
      RPC_service.post_service
        (Some "Run an operation without signature checks") RPC_query.empty
        (Data_encoding.obj2
          (Data_encoding.req None None "operation"
            Alpha_context.Operation.encoding)
          (Data_encoding.req None None "chain_id" Chain_id.encoding))
        Apply_results.operation_data_and_metadata_encoding
        (RPC_path.op_div path "run_operation").
    
    Definition entrypoint_type
      : RPC_service.service Updater.rpc_context Updater.rpc_context unit
        (Alpha_context.Script.expr * string) Alpha_context.Script.expr :=
      RPC_service.post_service (Some "Return the type of the given entrypoint")
        RPC_query.empty
        (Data_encoding.obj2
          (Data_encoding.req None None "script"
            Alpha_context.Script.expr_encoding)
          (Data_encoding.dft None None "entrypoint" Data_encoding.string_value
            "default"))
        (Data_encoding.obj1
          (Data_encoding.req None None "entrypoint_type"
            Alpha_context.Script.expr_encoding))
        (RPC_path.op_div path "entrypoint").
    
    Definition list_entrypoints
      : RPC_service.service Updater.rpc_context Updater.rpc_context unit
        Alpha_context.Script.expr
        (list (list Michelson_v1_primitives.prim) *
          list (string * Alpha_context.Script.expr)) :=
      RPC_service.post_service
        (Some "Return the list of entrypoints of the given script")
        RPC_query.empty
        (Data_encoding.obj1
          (Data_encoding.req None None "script"
            Alpha_context.Script.expr_encoding))
        (Data_encoding.obj2
          (Data_encoding.dft None None "unreachable"
            (Data_encoding.list_value None
              (Data_encoding.obj1
                (Data_encoding.req None None "path"
                  (Data_encoding.list_value None
                    Michelson_v1_primitives.prim_encoding)))) nil)
          (Data_encoding.req None None "entrypoints"
            (Data_encoding.assoc Alpha_context.Script.expr_encoding)))
        (RPC_path.op_div path "entrypoints").
  End S.
  
  Module Traced_interpreter.
    (** Init function; without side-effects in Coq *)
    Definition init_module : unit :=
      Error_monad.register_error_kind Error_monad.Temporary
        "michelson_v1.cannot_serialize_log"
        "Not enough gas to serialize execution trace"
        "Execution trace with stacks was to big to be serialized with the provided gas"
        None Data_encoding.empty
        (fun (function_parameter : Error_monad._error) =>
          match function_parameter with
          | Build_extensible tag _ payload =>
            if String.eqb tag "Cannot_serialize_log" then
              Some tt
            else None
          end)
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Build_extensible "Cannot_serialize_log" unit tt).
    
    Inductive log_element : Set :=
    | Log : forall {a : Set},
      Alpha_context.context -> Alpha_context.Script.location -> a ->
      Script_typed_ir.stack_ty -> log_element.
    
    Definition unparse_stack {A : Set}
      (ctxt : Alpha_context.context)
      (function_parameter : A * Script_typed_ir.stack_ty)
      : M=? (list (Alpha_context.Script.expr * option string)) :=
      let '(stack_value, stack_ty) := function_parameter in
      let ctxt := Alpha_context.Gas.set_unlimited ctxt in
      let fix unparse_stack {a : Set}
        (stack_ty : Script_typed_ir.stack_ty) (stack_value : a)
        {struct stack_ty}
        : M=? (list (Alpha_context.Script.expr * option string)) :=
        match (stack_ty, stack_value) with
        | (Script_typed_ir.Empty_t, _) => Error_monad.return_nil
        | (Script_typed_ir.Item_t ty rest_ty, x) =>
          let 'existT _ [__0, __1] [x, rest_ty, ty] :=
            cast_exists (Es := [Set ** Set])
              (fun '[__0, __1] =>
                [__0 * __1 ** Script_typed_ir.stack_ty ** Script_typed_ir.ty])
              [x, rest_ty, ty] in
          let '(v, rest) := x in
          let=? '(data, _ctxt) :=
            Script_ir_translator.unparse_data ctxt Script_ir_translator.Readable
              ty v in
          let=? rest := unparse_stack rest_ty rest in
          return=?
            (let annot {C : Set} : option C :=
              None in
            let data := Micheline.strip_locations data in
            cons (data, annot) rest)
        end in
      unparse_stack stack_ty stack_value.
    
    Module Trace_logger.
      Definition log : Pervasives.ref (list log_element) :=
        Pervasives.ref_value nil.
      
      Definition log_interp {A : Set}
        (ctxt : Alpha_context.context) (descr_value : Script_typed_ir.descr)
        (stack_value : A) : unit :=
        Pervasives.op_coloneq log
          (cons
            (Log ctxt descr_value.(Script_typed_ir.descr.loc) stack_value
              descr_value.(Script_typed_ir.descr.bef))
            (Pervasives.op_exclamation log)).
      
      Definition log_entry {A B C : Set} (_ctxt : A) (_descr : B) (_stack : C)
        : unit := tt.
      
      Definition log_exit {A : Set}
        (ctxt : Alpha_context.context) (descr_value : Script_typed_ir.descr)
        (stack_value : A) : unit :=
        Pervasives.op_coloneq log
          (cons
            (Log ctxt descr_value.(Script_typed_ir.descr.loc) stack_value
              descr_value.(Script_typed_ir.descr.aft))
            (Pervasives.op_exclamation log)).
      
      Definition get_log (function_parameter : unit)
        : M=?
          (option
            (list
              (Alpha_context.Script.location * Alpha_context.Gas.t *
                list (Alpha_context.Script.expr * option string)))) :=
        let '_ := function_parameter in
        let=? res :=
          Error_monad.map_s
            (fun (function_parameter : log_element) =>
              let 'Log ctxt loc stack_value stack_ty := function_parameter in
              let 'existT _ __Log_'a [stack_ty, stack_value, loc, ctxt] :=
                existT (A := Set)
                  (fun __Log_'a =>
                    [Script_typed_ir.stack_ty ** __Log_'a **
                      Alpha_context.Script.location ** Alpha_context.context]) _
                  [stack_ty, stack_value, loc, ctxt] in
              let=? stack_value :=
                Error_monad.trace_value
                  (Build_extensible "Cannot_serialize_log" unit tt)
                  (unparse_stack ctxt (stack_value, stack_ty)) in
              Error_monad._return
                (loc, (Alpha_context.Gas.level ctxt), stack_value))
            (Pervasives.op_exclamation log) in
        Error_monad._return (Some (List.rev res)).
      
      Definition module :=
        {|
          Script_interpreter.STEP_LOGGER.log_interp _ := log_interp;
          Script_interpreter.STEP_LOGGER.log_entry _ := log_entry;
          Script_interpreter.STEP_LOGGER.log_exit _ := log_exit;
          Script_interpreter.STEP_LOGGER.get_log := get_log
        |}.
    End Trace_logger.
    Definition Trace_logger : Script_interpreter.STEP_LOGGER :=
      Trace_logger.module.
    
    Definition execute
      (ctxt : Alpha_context.t) (mode : Script_ir_translator.unparsing_mode)
      (step_constants : Script_interpreter.step_constants)
      (script : Alpha_context.Script.t) (entrypoint : string)
      (parameter : Alpha_context.Script.expr)
      : M=?
        (Script_interpreter.execution_result *
          Script_interpreter.execution_trace) :=
      let Logger := Trace_logger in
      let logger := existS (A := unit) (fun _ => _) tt Logger in
      let=? '{|
        Script_interpreter.execution_result.ctxt := ctxt;
          Script_interpreter.execution_result.storage := storage_value;
          Script_interpreter.execution_result.lazy_storage_diff :=
            lazy_storage_diff;
          Script_interpreter.execution_result.operations := operations
          |} :=
        Script_interpreter.execute (Some logger) ctxt mode step_constants script
          entrypoint parameter true in
      let=? trace_value := Logger.(Script_interpreter.STEP_LOGGER.get_log) tt in
      let trace_value := Option.value trace_value nil in
      return=?
        ({| Script_interpreter.execution_result.ctxt := ctxt;
          Script_interpreter.execution_result.storage := storage_value;
          Script_interpreter.execution_result.lazy_storage_diff :=
            lazy_storage_diff;
          Script_interpreter.execution_result.operations := operations |},
          trace_value).
  End Traced_interpreter.
  
  Definition raw_typecheck_data
    (legacy : bool) (ctxt : Alpha_context.context)
    (function_parameter : Alpha_context.Script.expr * Alpha_context.Script.expr)
    : M=? Alpha_context.context :=
    let '(data, exp_ty) := function_parameter in
    let=? '(Script_ir_translator.Ex_ty exp_ty, ctxt) :=
      return=
        (Error_monad.record_trace
          (Build_extensible "Ill_formed_type"
            (option string * Alpha_context.Script.expr *
              Alpha_context.Script.location) (None, exp_ty, 0))
          (Script_ir_translator.parse_parameter_ty ctxt legacy
            (Micheline.root exp_ty))) in
    let 'existT _ __Ex_ty_'a [ctxt, exp_ty] :=
      cast_exists (Es := Set)
        (fun __Ex_ty_'a => [Alpha_context.context ** Script_typed_ir.ty])
        [ctxt, exp_ty] in
    let=? '(_, ctxt) :=
      Error_monad.trace_eval
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Lwt._return
            (let? '(exp_ty, _ctxt) :=
              Script_ir_translator.serialize_ty_for_error ctxt exp_ty in
            return?
              (Build_extensible "Ill_typed_data"
                (option string * Alpha_context.Script.expr *
                  Alpha_context.Script.expr) (None, data, exp_ty))))
        (let allow_forged := true in
        ((Script_ir_translator.parse_data None ctxt legacy allow_forged exp_ty
          (Micheline.root data)) : M=? (__Ex_ty_'a * Alpha_context.context))) in
    return=? ctxt.
  
  Definition register (function_parameter : unit) : unit :=
    let '_ := function_parameter in
    let originate_dummy_contract
      (ctxt : Alpha_context.context) (script : Alpha_context.Script.t)
      (balance : Alpha_context.Tez.t)
      : M=? (Alpha_context.context * Alpha_context.Contract.t) :=
      let ctxt :=
        Alpha_context.Contract.init_origination_nonce ctxt Operation_hash.zero
        in
      let=? '(ctxt, dummy_contract) :=
        Lwt._return
          (Alpha_context.Contract.fresh_contract_from_current_nonce ctxt) in
      let=? ctxt :=
        Alpha_context.Contract.originate ctxt dummy_contract balance
          (script, None) None in
      Error_monad._return (ctxt, dummy_contract) in
    let '_ :=
      Services_registration.register0 S.run_code
        (fun (ctxt : Alpha_context.t) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter :
              Alpha_context.Script.expr * Alpha_context.Script.expr *
                Alpha_context.Script.expr * Alpha_context.Tez.t *
                Alpha_context.Tez.t * Chain_id.t *
                option Alpha_context.Contract.t *
                option Alpha_context.Contract.t *
                option Alpha_context.Gas.Arith.integral * string) =>
              let
                '(code, storage_value, parameter, amount, balance, chain_id,
                  source, payer, gas, entrypoint) := function_parameter in
              let storage_value :=
                Alpha_context.Script.lazy_expr_value storage_value in
              let code := Alpha_context.Script.lazy_expr_value code in
              let=? '(ctxt, dummy_contract) :=
                originate_dummy_contract ctxt
                  {| Alpha_context.Script.t.code := code;
                    Alpha_context.Script.t.storage := storage_value |} balance
                in
              let '(source, payer) :=
                match (source, payer) with
                | (Some source, Some payer) => (source, payer)
                | (Some source, None) => (source, source)
                | (None, Some payer) => (payer, payer)
                | (None, None) => (dummy_contract, dummy_contract)
                end in
              let gas :=
                match gas with
                | Some gas => gas
                | None =>
                  Alpha_context.Constants.hard_gas_limit_per_operation ctxt
                end in
              let ctxt := Alpha_context.Gas.set_limit ctxt gas in
              let step_constants :=
                {| Script_interpreter.step_constants.source := source;
                  Script_interpreter.step_constants.payer := payer;
                  Script_interpreter.step_constants.self := dummy_contract;
                  Script_interpreter.step_constants.amount := amount;
                  Script_interpreter.step_constants.chain_id := chain_id |} in
              let=? '{|
                Script_interpreter.execution_result.storage := storage_value;
                  Script_interpreter.execution_result.lazy_storage_diff :=
                    lazy_storage_diff;
                  Script_interpreter.execution_result.operations := operations
                  |} :=
                Script_interpreter.execute None ctxt
                  Script_ir_translator.Readable step_constants
                  {| Alpha_context.Script.t.code := code;
                    Alpha_context.Script.t.storage := storage_value |}
                  entrypoint parameter true in
              return=? (storage_value, operations, lazy_storage_diff)) in
    let '_ :=
      Services_registration.register0 S.trace_code
        (fun (ctxt : Alpha_context.t) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter :
              Alpha_context.Script.expr * Alpha_context.Script.expr *
                Alpha_context.Script.expr * Alpha_context.Tez.t *
                Alpha_context.Tez.t * Chain_id.t *
                option Alpha_context.Contract.t *
                option Alpha_context.Contract.t *
                option Alpha_context.Gas.Arith.integral * string) =>
              let
                '(code, storage_value, parameter, amount, balance, chain_id,
                  source, payer, gas, entrypoint) := function_parameter in
              let storage_value :=
                Alpha_context.Script.lazy_expr_value storage_value in
              let code := Alpha_context.Script.lazy_expr_value code in
              let=? '(ctxt, dummy_contract) :=
                originate_dummy_contract ctxt
                  {| Alpha_context.Script.t.code := code;
                    Alpha_context.Script.t.storage := storage_value |} balance
                in
              let '(source, payer) :=
                match (source, payer) with
                | (Some source, Some payer) => (source, payer)
                | (Some source, None) => (source, source)
                | (None, Some payer) => (payer, payer)
                | (None, None) => (dummy_contract, dummy_contract)
                end in
              let gas :=
                match gas with
                | Some gas => gas
                | None =>
                  Alpha_context.Constants.hard_gas_limit_per_operation ctxt
                end in
              let ctxt := Alpha_context.Gas.set_limit ctxt gas in
              let step_constants :=
                {| Script_interpreter.step_constants.source := source;
                  Script_interpreter.step_constants.payer := payer;
                  Script_interpreter.step_constants.self := dummy_contract;
                  Script_interpreter.step_constants.amount := amount;
                  Script_interpreter.step_constants.chain_id := chain_id |} in
              let=?
                '({|
                  Script_interpreter.execution_result.storage := storage_value;
                    Script_interpreter.execution_result.lazy_storage_diff :=
                      lazy_storage_diff;
                    Script_interpreter.execution_result.operations := operations
                    |}, trace_value) :=
                Traced_interpreter.execute ctxt Script_ir_translator.Readable
                  step_constants
                  {| Alpha_context.Script.t.code := code;
                    Alpha_context.Script.t.storage := storage_value |}
                  entrypoint parameter in
              return=?
                (storage_value, operations, trace_value, lazy_storage_diff)) in
    let '_ :=
      Services_registration.register0 S.typecheck_code
        (fun (ctxt : Alpha_context.t) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter :
              Alpha_context.Script.expr *
                option Alpha_context.Gas.Arith.integral * option bool) =>
              let '(expr, maybe_gas, legacy) := function_parameter in
              let legacy := Option.value legacy false in
              let ctxt :=
                match maybe_gas with
                | None => Alpha_context.Gas.set_unlimited ctxt
                | Some gas => Alpha_context.Gas.set_limit ctxt gas
                end in
              let=? '(res, ctxt) :=
                Script_ir_translator.typecheck_code legacy ctxt expr in
              return=? (res, (Alpha_context.Gas.level ctxt))) in
    let '_ :=
      Services_registration.register0 S.typecheck_data
        (fun (ctxt : Alpha_context.t) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter :
              Alpha_context.Script.expr * Alpha_context.Script.expr *
                option Alpha_context.Gas.Arith.integral * option bool) =>
              let '(data, ty, maybe_gas, legacy) := function_parameter in
              let legacy := Option.value legacy false in
              let ctxt :=
                match maybe_gas with
                | None => Alpha_context.Gas.set_unlimited ctxt
                | Some gas => Alpha_context.Gas.set_limit ctxt gas
                end in
              let=? ctxt := raw_typecheck_data legacy ctxt (data, ty) in
              return=? (Alpha_context.Gas.level ctxt)) in
    let '_ :=
      Services_registration.register0 S.pack_data
        (fun (ctxt : Alpha_context.t) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter :
              Alpha_context.Script.expr * Alpha_context.Script.expr *
                option Alpha_context.Gas.Arith.integral) =>
              let '(expr, typ, maybe_gas) := function_parameter in
              let ctxt :=
                match maybe_gas with
                | None => Alpha_context.Gas.set_unlimited ctxt
                | Some gas => Alpha_context.Gas.set_limit ctxt gas
                end in
              let=? '(Script_ir_translator.Ex_ty typ, ctxt) :=
                return=
                  (Script_ir_translator.parse_packable_ty ctxt true
                    (Micheline.root typ)) in
              let=? '(data, ctxt) :=
                Script_ir_translator.parse_data None ctxt true true typ
                  (Micheline.root expr) in
              let=? '(bytes_value, ctxt) :=
                Script_ir_translator.pack_data ctxt typ data in
              return=? (bytes_value, (Alpha_context.Gas.level ctxt))) in
    let '_ :=
      Services_registration.register0 S.run_operation
        (fun (ctxt : Alpha_context.t) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter :
              Alpha_context.Operation.packed * Chain_id.t) =>
              let
                '({|
                  Alpha_context.packed_operation.shell := shell;
                    Alpha_context.packed_operation.protocol_data :=
                      Alpha_context.Operation_data protocol_data
                    |}, chain_id) := function_parameter in
              let partial_precheck_manager_contents
                (ctxt : Alpha_context.context) (op : Alpha_context.contents)
                : M=? Alpha_context.context :=
                match op with
                |
                  Alpha_context.Manager_operation {|
                    Alpha_context.contents.Manager_operation.source := source;
                      Alpha_context.contents.Manager_operation.fee := fee;
                      Alpha_context.contents.Manager_operation.counter :=
                        counter;
                      Alpha_context.contents.Manager_operation.operation :=
                        operation;
                      Alpha_context.contents.Manager_operation.gas_limit :=
                        gas_limit;
                      Alpha_context.contents.Manager_operation.storage_limit :=
                        storage_limit
                      |} =>
                  let=? '_ :=
                    return= (Alpha_context.Gas.check_limit ctxt gas_limit) in
                  let ctxt := Alpha_context.Gas.set_limit ctxt gas_limit in
                  let=? '_ :=
                    return=
                      (Alpha_context.Fees.check_storage_limit ctxt storage_limit)
                    in
                  let=? '_ :=
                    Alpha_context.Contract.must_be_allocated ctxt
                      (Alpha_context.Contract.implicit_contract source) in
                  let=? '_ :=
                    Alpha_context.Contract.check_counter_increment ctxt source
                      counter in
                  let=? ctxt :=
                    match operation with
                    | Alpha_context.Reveal pk =>
                      Alpha_context.Contract.reveal_manager_key ctxt source pk
                    |
                      Alpha_context.Transaction {|
                        Alpha_context.manager_operation.Transaction.parameters := parameters
                          |} =>
                      let arg_bytes :=
                        Data_encoding.Binary.to_bytes_exn
                          Alpha_context.Script.lazy_expr_encoding parameters in
                      let arg :=
                        match
                          Data_encoding.Binary.of_bytes
                            Alpha_context.Script.lazy_expr_encoding arg_bytes
                          with
                        | Some arg => arg
                        | None =>
                          (* ❌ Assert instruction is not handled. *)
                          assert Alpha_context.Script.lazy_expr false
                        end in
                      Lwt._return
                        (Error_monad.record_trace
                          (Build_extensible
                            "Gas_quota_exceeded_init_deserialize" unit tt)
                          (let? '_ :=
                            Alpha_context.Gas.check_enough ctxt
                              (Alpha_context.Script.minimal_deserialize_cost arg)
                            in
                          let? '(_arg, ctxt) :=
                            Alpha_context.Script.force_decode_in_context ctxt
                              arg in
                          return? ctxt))
                    |
                      Alpha_context.Origination {|
                        Alpha_context.manager_operation.Origination.script := script
                          |} =>
                      let script_bytes :=
                        Data_encoding.Binary.to_bytes_exn
                          Alpha_context.Script.encoding script in
                      let script :=
                        match
                          Data_encoding.Binary.of_bytes
                            Alpha_context.Script.encoding script_bytes with
                        | Some script => script
                        | None =>
                          (* ❌ Assert instruction is not handled. *)
                          assert Alpha_context.Script.t false
                        end in
                      Lwt._return
                        (Error_monad.record_trace
                          (Build_extensible
                            "Gas_quota_exceeded_init_deserialize" unit tt)
                          (let? '_ :=
                            Alpha_context.Gas.check_enough ctxt
                              (Alpha_context.Gas.op_plusat
                                (Alpha_context.Script.minimal_deserialize_cost
                                  script.(Alpha_context.Script.t.code))
                                (Alpha_context.Script.minimal_deserialize_cost
                                  script.(Alpha_context.Script.t.storage))) in
                          let? '(_code, ctxt) :=
                            Alpha_context.Script.force_decode_in_context ctxt
                              script.(Alpha_context.Script.t.code) in
                          let? '(_storage, ctxt) :=
                            Alpha_context.Script.force_decode_in_context ctxt
                              script.(Alpha_context.Script.t.storage) in
                          return? ctxt))
                    | _ => Error_monad._return ctxt
                    end in
                  let=? _public_key :=
                    Alpha_context.Contract.get_manager_key ctxt source in
                  let=? ctxt :=
                    Alpha_context.Contract.increment_counter ctxt source in
                  Alpha_context.Contract.spend ctxt
                    (Alpha_context.Contract.implicit_contract source) fee
                | _ => unreachable_gadt_branch
                end in
              let fix partial_precheck_manager_contents_list
                (ctxt : Alpha_context.t)
                (contents_list : Alpha_context.contents_list)
                : M=? Alpha_context.context :=
                match contents_list with
                |
                  Alpha_context.Single
                    ((Alpha_context.Manager_operation _) as op) =>
                  partial_precheck_manager_contents ctxt op
                |
                  Alpha_context.Cons ((Alpha_context.Manager_operation _) as op)
                    rest =>
                  let=? ctxt := partial_precheck_manager_contents ctxt op in
                  partial_precheck_manager_contents_list ctxt rest
                | _ => unreachable_gadt_branch
                end in
              let ret (contents : Apply_results.contents_result_list)
                : Alpha_context.packed_protocol_data *
                  Apply_results.packed_operation_metadata :=
                ((Alpha_context.Operation_data protocol_data),
                  (Apply_results.Operation_metadata
                    {| Apply_results.operation_metadata.contents := contents |}))
                in
              let operation :=
                {| Alpha_context.operation.shell := shell;
                  Alpha_context.operation.protocol_data := protocol_data |} in
              let hash_value :=
                Alpha_context.Operation.hash_value
                  {| Alpha_context.operation.shell := shell;
                    Alpha_context.operation.protocol_data := protocol_data |} in
              let ctxt :=
                Alpha_context.Contract.init_origination_nonce ctxt hash_value in
              let baker :=
                Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.zero) in
              match protocol_data.(Alpha_context.protocol_data.contents) with
              | (Alpha_context.Single (Alpha_context.Manager_operation _)) as op
                =>
                let=? ctxt := partial_precheck_manager_contents_list ctxt op in
                let= '(_ctxt, result_value) :=
                  Apply.apply_manager_contents_list ctxt
                    Script_ir_translator.Optimized baker chain_id op in
                return=? (ret result_value)
              | (Alpha_context.Cons (Alpha_context.Manager_operation _) _) as op
                =>
                let=? ctxt := partial_precheck_manager_contents_list ctxt op in
                let= '(_ctxt, result_value) :=
                  Apply.apply_manager_contents_list ctxt
                    Script_ir_translator.Optimized baker chain_id op in
                return=? (ret result_value)
              | _ =>
                let=? '(_ctxt, result_value) :=
                  Apply.apply_contents_list ctxt chain_id
                    Script_ir_translator.Optimized
                    shell.(Operation.shell_header.branch) baker operation
                    operation.(Alpha_context.operation.protocol_data).(Alpha_context.protocol_data.contents)
                  in
                return=? (ret result_value)
              end) in
    let '_ :=
      Services_registration.register0 S.entrypoint_type
        (fun (ctxt : Alpha_context.t) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : Alpha_context.Script.expr * string) =>
              let '(expr, entrypoint) := function_parameter in
              let ctxt := Alpha_context.Gas.set_unlimited ctxt in
              let legacy := false in
              Lwt._return
                (let? '(_f, Script_ir_translator.Ex_ty ty) :=
                  let? '{|
                    Script_ir_translator.toplevel.arg_type := arg_type;
                      Script_ir_translator.toplevel.root_name := root_name
                      |} := Script_ir_translator.parse_toplevel legacy expr in
                  let?
                    '(Script_ir_translator.Ex_parameter_ty_entrypoints arg_type
                      entrypoints, _) :=
                    Script_ir_translator.parse_parameter_ty_and_entrypoints ctxt
                      legacy root_name arg_type in
                  Script_ir_translator.find_entrypoint arg_type entrypoints
                    entrypoint in
                let? '(ty_node, _) := Script_ir_translator.unparse_ty ctxt ty in
                return? (Micheline.strip_locations ty_node))) in
    Services_registration.register0 S.list_entrypoints
      (fun (ctxt : Alpha_context.t) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          fun (expr : Alpha_context.Script.expr) =>
            let ctxt := Alpha_context.Gas.set_unlimited ctxt in
            let legacy := false in
            Lwt._return
              (let? '{|
                Script_ir_translator.toplevel.arg_type := arg_type;
                  Script_ir_translator.toplevel.root_name := root_name
                  |} := Script_ir_translator.parse_toplevel legacy expr in
              let?
                '(Script_ir_translator.Ex_parameter_ty_entrypoints arg_type
                  entrypoints, _) :=
                Script_ir_translator.parse_parameter_ty_and_entrypoints ctxt
                  legacy root_name arg_type in
              let? '(unreachable_entrypoint, map) :=
                Script_ir_translator.list_entrypoints ctxt arg_type entrypoints
                in
              return?
                (unreachable_entrypoint,
                  (Script_ir_translator.Entrypoints_map.(S.MAP.fold)
                    (fun (entry : string) =>
                      fun (function_parameter :
                        list Michelson_v1_primitives.prim *
                          Micheline.node Alpha_context.Script.location
                            Alpha_context.Script.prim) =>
                        let '(_, ty) := function_parameter in
                        fun (acc_value :
                          list (string * Alpha_context.Script.expr)) =>
                          cons (entry, (Micheline.strip_locations ty)) acc_value)
                    map nil)))).
  
  Definition run_code {A : Set}
    (ctxt : RPC_context.simple A) (block : A)
    (gas : option Alpha_context.Gas.Arith.integral)
    (op_staroptstar : option string)
    : Alpha_context.Script.expr -> Alpha_context.Script.expr ->
    Alpha_context.Script.expr -> Alpha_context.Tez.t -> Alpha_context.Tez.t ->
    Chain_id.t -> option Alpha_context.Contract.t ->
    option Alpha_context.Contract.t ->
    M=
      (Error_monad.shell_tzresult
        (Alpha_context.Script.expr *
          list Alpha_context.packed_internal_operation *
          option Alpha_context.Lazy_storage.diffs)) :=
    let entrypoint :=
      match op_staroptstar with
      | Some op_starsthstar => op_starsthstar
      | None => "default"
      end in
    fun (script : Alpha_context.Script.expr) =>
      fun (storage_value : Alpha_context.Script.expr) =>
        fun (input : Alpha_context.Script.expr) =>
          fun (amount : Alpha_context.Tez.t) =>
            fun (balance : Alpha_context.Tez.t) =>
              fun (chain_id : Chain_id.t) =>
                fun (source : option Alpha_context.Contract.t) =>
                  fun (payer : option Alpha_context.Contract.t) =>
                    RPC_context.make_call0 S.run_code ctxt block tt
                      (script, storage_value, input, amount, balance, chain_id,
                        source, payer, gas, entrypoint).
  
  Definition trace_code {A : Set}
    (ctxt : RPC_context.simple A) (block : A)
    (gas : option Alpha_context.Gas.Arith.integral)
    (op_staroptstar : option string)
    : Alpha_context.Script.expr -> Alpha_context.Script.expr ->
    Alpha_context.Script.expr -> Alpha_context.Tez.t -> Alpha_context.Tez.t ->
    Chain_id.t -> option Alpha_context.Contract.t ->
    option Alpha_context.Contract.t ->
    M=
      (Error_monad.shell_tzresult
        (Alpha_context.Script.expr *
          list Alpha_context.packed_internal_operation *
          list
            (Alpha_context.Script.location * Alpha_context.Gas.t *
              list (Alpha_context.Script.expr * option string)) *
          option Alpha_context.Lazy_storage.diffs)) :=
    let entrypoint :=
      match op_staroptstar with
      | Some op_starsthstar => op_starsthstar
      | None => "default"
      end in
    fun (script : Alpha_context.Script.expr) =>
      fun (storage_value : Alpha_context.Script.expr) =>
        fun (input : Alpha_context.Script.expr) =>
          fun (amount : Alpha_context.Tez.t) =>
            fun (balance : Alpha_context.Tez.t) =>
              fun (chain_id : Chain_id.t) =>
                fun (source : option Alpha_context.Contract.t) =>
                  fun (payer : option Alpha_context.Contract.t) =>
                    RPC_context.make_call0 S.trace_code ctxt block tt
                      (script, storage_value, input, amount, balance, chain_id,
                        source, payer, gas, entrypoint).
  
  Definition typecheck_code {A : Set}
    (ctxt : RPC_context.simple A) (block : A)
    (gas : option Alpha_context.Gas.Arith.integral) (legacy : option bool)
    (script : Alpha_context.Script.expr)
    : M=
      (Error_monad.shell_tzresult
        (list
          (Alpha_context.Script.location *
            (list (Alpha_context.Script.expr * list string) *
              list (Alpha_context.Script.expr * list string))) *
          Alpha_context.Gas.t)) :=
    RPC_context.make_call0 S.typecheck_code ctxt block tt (script, gas, legacy).
  
  Definition typecheck_data {A : Set}
    (ctxt : RPC_context.simple A) (block : A)
    (gas : option Alpha_context.Gas.Arith.integral) (legacy : option bool)
    (data : Alpha_context.Script.expr) (ty : Alpha_context.Script.expr)
    : M= (Error_monad.shell_tzresult Alpha_context.Gas.t) :=
    RPC_context.make_call0 S.typecheck_data ctxt block tt
      (data, ty, gas, legacy).
  
  Definition pack_data {A : Set}
    (ctxt : RPC_context.simple A) (block : A)
    (gas : option Alpha_context.Gas.Arith.integral)
    (data : Alpha_context.Script.expr) (ty : Alpha_context.Script.expr)
    : M= (Error_monad.shell_tzresult (bytes * Alpha_context.Gas.t)) :=
    RPC_context.make_call0 S.pack_data ctxt block tt (data, ty, gas).
  
  Definition run_operation {A : Set}
    (ctxt : RPC_context.simple A) (block : A)
    (op : Alpha_context.Operation.packed) (chain_id : Chain_id.t)
    : M=
      (Error_monad.shell_tzresult
        (Alpha_context.Operation.packed_protocol_data *
          Apply_results.packed_operation_metadata)) :=
    RPC_context.make_call0 S.run_operation ctxt block tt (op, chain_id).
  
  Definition entrypoint_type {A : Set}
    (ctxt : RPC_context.simple A) (block : A)
    (script : Alpha_context.Script.expr) (entrypoint : string)
    : M= (Error_monad.shell_tzresult Alpha_context.Script.expr) :=
    RPC_context.make_call0 S.entrypoint_type ctxt block tt (script, entrypoint).
  
  Definition list_entrypoints {A : Set}
    (ctxt : RPC_context.simple A) (block : A)
    (script : Alpha_context.Script.expr)
    : M=
      (Error_monad.shell_tzresult
        (list (list Michelson_v1_primitives.prim) *
          list (string * Alpha_context.Script.expr))) :=
    RPC_context.make_call0 S.list_entrypoints ctxt block tt script.
End Scripts.

Module Forge.
  Module S.
    Definition path : RPC_path.path Updater.rpc_context Updater.rpc_context :=
      RPC_path.op_div path "forge".
    
    Definition operations
      : RPC_service.service Updater.rpc_context Updater.rpc_context unit
        (Operation.shell_header * Alpha_context.packed_contents_list) bytes :=
      RPC_service.post_service (Some "Forge an operation") RPC_query.empty
        Alpha_context.Operation.unsigned_encoding Data_encoding.bytes_value
        (RPC_path.op_div path "operations").
    
    Definition empty_proof_of_work_nonce : bytes :=
      Bytes.make Constants_repr.proof_of_work_nonce_size "000" % char.
    
    Definition protocol_data
      : RPC_service.service Updater.rpc_context Updater.rpc_context unit
        (int * option Nonce_hash.t * bytes) bytes :=
      RPC_service.post_service
        (Some "Forge the protocol-specific part of a block header")
        RPC_query.empty
        (Data_encoding.obj3
          (Data_encoding.req None None "priority" Data_encoding.uint16)
          (Data_encoding.opt None None "nonce_hash" Nonce_hash.encoding)
          (Data_encoding.dft None None "proof_of_work_nonce"
            (Data_encoding.Fixed.bytes_value
              Alpha_context.Constants.proof_of_work_nonce_size)
            empty_proof_of_work_nonce))
        (Data_encoding.obj1
          (Data_encoding.req None None "protocol_data" Data_encoding.bytes_value))
        (RPC_path.op_div path "protocol_data").
  End S.
  
  Definition register (function_parameter : unit) : unit :=
    let '_ := function_parameter in
    let '_ :=
      Services_registration.register0_noctxt S.operations
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          fun (function_parameter :
            Operation.shell_header * Alpha_context.packed_contents_list) =>
            let '(shell, proto) := function_parameter in
            Error_monad._return
              (Data_encoding.Binary.to_bytes_exn
                Alpha_context.Operation.unsigned_encoding (shell, proto))) in
    Services_registration.register0_noctxt S.protocol_data
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        fun (function_parameter : int * option Nonce_hash.t * bytes) =>
          let '(priority, seed_nonce_hash, proof_of_work_nonce) :=
            function_parameter in
          Error_monad._return
            (Data_encoding.Binary.to_bytes_exn
              Alpha_context.Block_header.contents_encoding
              {| Alpha_context.Block_header.contents.priority := priority;
                Alpha_context.Block_header.contents.seed_nonce_hash :=
                  seed_nonce_hash;
                Alpha_context.Block_header.contents.proof_of_work_nonce :=
                  proof_of_work_nonce |})).
  
  Module Manager.
    Definition operations {A : Set}
      (ctxt : RPC_context.simple A) (block : A) (branch : Block_hash.t)
      (source : Alpha_context.public_key_hash)
      (sourcePubKey : option Signature.public_key)
      (counter : Alpha_context.counter) (fee : Alpha_context.Tez.tez)
      (gas_limit : Alpha_context.Gas.Arith.integral) (storage_limit : Z.t)
      (operations : list Alpha_context.packed_manager_operation)
      : M=
        (Pervasives.result bytes
          (Error_monad.shell_trace Error_monad.shell_error)) :=
      let= function_parameter := Contract_services.manager_key ctxt block source
        in
      match function_parameter with
      | Pervasives.Error err => Lwt._return (Pervasives.Error err)
      | Pervasives.Ok revealed =>
        let ops :=
          List.map
            (fun (function_parameter : Alpha_context.packed_manager_operation)
              =>
              let 'Alpha_context.Manager operation := function_parameter in
              Alpha_context.Contents
                (Alpha_context.Manager_operation
                  {| Alpha_context.contents.Manager_operation.source := source;
                    Alpha_context.contents.Manager_operation.fee := fee;
                    Alpha_context.contents.Manager_operation.counter := counter;
                    Alpha_context.contents.Manager_operation.operation :=
                      operation;
                    Alpha_context.contents.Manager_operation.gas_limit :=
                      gas_limit;
                    Alpha_context.contents.Manager_operation.storage_limit :=
                      storage_limit |})) operations in
        let ops :=
          match (sourcePubKey, revealed) with
          | ((None, _) | (_, Some _)) => ops
          | (Some pk, None) =>
            let operation := Alpha_context.Reveal pk in
            cons
              (Alpha_context.Contents
                (Alpha_context.Manager_operation
                  {| Alpha_context.contents.Manager_operation.source := source;
                    Alpha_context.contents.Manager_operation.fee := fee;
                    Alpha_context.contents.Manager_operation.counter := counter;
                    Alpha_context.contents.Manager_operation.operation :=
                      operation;
                    Alpha_context.contents.Manager_operation.gas_limit :=
                      gas_limit;
                    Alpha_context.contents.Manager_operation.storage_limit :=
                      storage_limit |})) ops
          end in
        RPC_context.make_call0 S.operations ctxt block tt
          ({| Operation.shell_header.branch := branch |},
            (Alpha_context.Operation.of_list ops))
      end.
    
    Definition reveal {A : Set}
      (ctxt : RPC_context.simple A) (block : A) (branch : Block_hash.t)
      (source : Alpha_context.public_key_hash)
      (sourcePubKey : Signature.public_key) (counter : Alpha_context.counter)
      (fee : Alpha_context.Tez.tez) (function_parameter : unit)
      : M=
        (Pervasives.result bytes
          (Error_monad.shell_trace Error_monad.shell_error)) :=
      let '_ := function_parameter in
      operations ctxt block branch source (Some sourcePubKey) counter fee
        Alpha_context.Gas.Arith.zero Z.zero nil.
    
    Definition transaction {A : Set}
      (ctxt : RPC_context.simple A) (block : A) (branch : Block_hash.t)
      (source : Alpha_context.public_key_hash)
      (sourcePubKey : option Signature.public_key)
      (counter : Alpha_context.counter) (amount : Alpha_context.Tez.tez)
      (destination : Alpha_context.Contract.contract)
      (op_staroptstar : option string)
      : option Alpha_context.Script.expr -> Alpha_context.Gas.Arith.integral ->
      Z.t -> Alpha_context.Tez.tez -> unit ->
      M=
        (Pervasives.result bytes
          (Error_monad.shell_trace Error_monad.shell_error)) :=
      let entrypoint :=
        match op_staroptstar with
        | Some op_starsthstar => op_starsthstar
        | None => "default"
        end in
      fun (parameters : option Alpha_context.Script.expr) =>
        fun (gas_limit : Alpha_context.Gas.Arith.integral) =>
          fun (storage_limit : Z.t) =>
            fun (fee : Alpha_context.Tez.tez) =>
              fun (function_parameter : unit) =>
                let '_ := function_parameter in
                let parameters :=
                  Option.fold Alpha_context.Script.unit_parameter
                    Alpha_context.Script.lazy_expr_value parameters in
                operations ctxt block branch source sourcePubKey counter fee
                  gas_limit storage_limit
                  [
                    Alpha_context.Manager
                      (Alpha_context.Transaction
                        {|
                          Alpha_context.manager_operation.Transaction.amount
                            :=
                            amount;
                          Alpha_context.manager_operation.Transaction.parameters
                            :=
                            parameters;
                          Alpha_context.manager_operation.Transaction.entrypoint
                            :=
                            entrypoint;
                          Alpha_context.manager_operation.Transaction.destination
                            :=
                            destination
                          |})
                  ].
    
    Definition origination {A : Set}
      (ctxt : RPC_context.simple A) (block : A) (branch : Block_hash.t)
      (source : Alpha_context.public_key_hash)
      (sourcePubKey : option Signature.public_key)
      (counter : Alpha_context.counter) (balance : Alpha_context.Tez.tez)
      (delegatePubKey : option Signature.public_key_hash)
      (script : Alpha_context.Script.t)
      (gas_limit : Alpha_context.Gas.Arith.integral) (storage_limit : Z.t)
      (fee : Alpha_context.Tez.tez) (function_parameter : unit)
      : M=
        (Pervasives.result bytes
          (Error_monad.shell_trace Error_monad.shell_error)) :=
      let '_ := function_parameter in
      operations ctxt block branch source sourcePubKey counter fee gas_limit
        storage_limit
        [
          Alpha_context.Manager
            (Alpha_context.Origination
              {|
                Alpha_context.manager_operation.Origination.delegate
                  := delegatePubKey;
                Alpha_context.manager_operation.Origination.script
                  := script;
                Alpha_context.manager_operation.Origination.credit
                  := balance;
                Alpha_context.manager_operation.Origination.preorigination
                  := None |})
        ].
    
    Definition delegation {A : Set}
      (ctxt : RPC_context.simple A) (block : A) (branch : Block_hash.t)
      (source : Alpha_context.public_key_hash)
      (sourcePubKey : option Signature.public_key)
      (counter : Alpha_context.counter) (fee : Alpha_context.Tez.tez)
      (delegate : option Signature.public_key_hash)
      : M=
        (Pervasives.result bytes
          (Error_monad.shell_trace Error_monad.shell_error)) :=
      operations ctxt block branch source sourcePubKey counter fee
        Alpha_context.Gas.Arith.zero Z.zero
        [ Alpha_context.Manager (Alpha_context.Delegation delegate) ].
  End Manager.
  
  Definition operation {A : Set}
    (ctxt : RPC_context.simple A) (block : A) (branch : Block_hash.t)
    (operation : Alpha_context.contents)
    : M= (Error_monad.shell_tzresult bytes) :=
    RPC_context.make_call0 S.operations ctxt block tt
      ({| Operation.shell_header.branch := branch |},
        (Alpha_context.Contents_list (Alpha_context.Single operation))).
  
  Definition endorsement {A : Set}
    (ctxt : RPC_context.simple A) (b_value : A) (branch : Block_hash.t)
    (level : Alpha_context.Raw_level.t) (function_parameter : unit)
    : M= (Error_monad.shell_tzresult bytes) :=
    let '_ := function_parameter in
    operation ctxt b_value branch
      (Alpha_context.Endorsement
        {| Alpha_context.contents.Endorsement.level := level |}).
  
  Definition proposals {A : Set}
    (ctxt : RPC_context.simple A) (b_value : A) (branch : Block_hash.t)
    (source : Signature.public_key_hash) (period : int32)
    (proposals : list Protocol_hash.t) (function_parameter : unit)
    : M= (Error_monad.shell_tzresult bytes) :=
    let '_ := function_parameter in
    operation ctxt b_value branch
      (Alpha_context.Proposals
        {| Alpha_context.contents.Proposals.source := source;
          Alpha_context.contents.Proposals.period := period;
          Alpha_context.contents.Proposals.proposals := proposals |}).
  
  Definition ballot {A : Set}
    (ctxt : RPC_context.simple A) (b_value : A) (branch : Block_hash.t)
    (source : Signature.public_key_hash) (period : int32)
    (proposal : Protocol_hash.t) (ballot : Alpha_context.Vote.ballot)
    (function_parameter : unit) : M= (Error_monad.shell_tzresult bytes) :=
    let '_ := function_parameter in
    operation ctxt b_value branch
      (Alpha_context.Ballot
        {| Alpha_context.contents.Ballot.source := source;
          Alpha_context.contents.Ballot.period := period;
          Alpha_context.contents.Ballot.proposal := proposal;
          Alpha_context.contents.Ballot.ballot := ballot |}).
  
  Definition seed_nonce_revelation {A : Set}
    (ctxt : RPC_context.simple A) (block : A) (branch : Block_hash.t)
    (level : Alpha_context.Raw_level.t) (nonce_value : Alpha_context.Nonce.t)
    (function_parameter : unit) : M= (Error_monad.shell_tzresult bytes) :=
    let '_ := function_parameter in
    operation ctxt block branch
      (Alpha_context.Seed_nonce_revelation
        {| Alpha_context.contents.Seed_nonce_revelation.level := level;
          Alpha_context.contents.Seed_nonce_revelation.nonce := nonce_value |}).
  
  Definition double_baking_evidence {A : Set}
    (ctxt : RPC_context.simple A) (block : A) (branch : Block_hash.t)
    (bh1 : Alpha_context.Block_header.t) (bh2 : Alpha_context.Block_header.t)
    (function_parameter : unit) : M= (Error_monad.shell_tzresult bytes) :=
    let '_ := function_parameter in
    operation ctxt block branch
      (Alpha_context.Double_baking_evidence
        {| Alpha_context.contents.Double_baking_evidence.bh1 := bh1;
          Alpha_context.contents.Double_baking_evidence.bh2 := bh2 |}).
  
  Definition double_endorsement_evidence {A : Set}
    (ctxt : RPC_context.simple A) (block : A) (branch : Block_hash.t)
    (op1 : Alpha_context.operation) (op2 : Alpha_context.operation)
    (function_parameter : unit) : M= (Error_monad.shell_tzresult bytes) :=
    let '_ := function_parameter in
    operation ctxt block branch
      (Alpha_context.Double_endorsement_evidence
        {| Alpha_context.contents.Double_endorsement_evidence.op1 := op1;
          Alpha_context.contents.Double_endorsement_evidence.op2 := op2 |}).
  
  Definition empty_proof_of_work_nonce : bytes :=
    Bytes.make Constants_repr.proof_of_work_nonce_size "000" % char.
  
  Definition protocol_data {A : Set}
    (ctxt : RPC_context.simple A) (block : A) (priority : int)
    (seed_nonce_hash : option Nonce_hash.t) (op_staroptstar : option bytes)
    : unit -> M= (Error_monad.shell_tzresult bytes) :=
    let proof_of_work_nonce :=
      match op_staroptstar with
      | Some op_starsthstar => op_starsthstar
      | None => empty_proof_of_work_nonce
      end in
    fun (function_parameter : unit) =>
      let '_ := function_parameter in
      RPC_context.make_call0 S.protocol_data ctxt block tt
        (priority, seed_nonce_hash, proof_of_work_nonce).
End Forge.

Module Parse.
  Module S.
    Definition path : RPC_path.path Updater.rpc_context Updater.rpc_context :=
      RPC_path.op_div path "parse".
    
    Definition operations
      : RPC_service.service Updater.rpc_context Updater.rpc_context unit
        (list Alpha_context.Operation.raw * option bool)
        (list Alpha_context.Operation.packed) :=
      RPC_service.post_service (Some "Parse operations") RPC_query.empty
        (Data_encoding.obj2
          (Data_encoding.req None None "operations"
            (Data_encoding.list_value None
              (Data_encoding.dynamic_size None
                Alpha_context.Operation.raw_encoding)))
          (Data_encoding.opt None None "check_signature"
            Data_encoding.bool_value))
        (Data_encoding.list_value None
          (Data_encoding.dynamic_size None Alpha_context.Operation.encoding))
        (RPC_path.op_div path "operations").
    
    Definition block
      : RPC_service.service Updater.rpc_context Updater.rpc_context unit
        Alpha_context.Block_header.raw Alpha_context.Block_header.protocol_data :=
      RPC_service.post_service (Some "Parse a block") RPC_query.empty
        Alpha_context.Block_header.raw_encoding
        Alpha_context.Block_header.protocol_data_encoding
        (RPC_path.op_div path "block").
  End S.
  
  Definition parse_protocol_data (protocol_data : bytes)
    : Alpha_context.Block_header.protocol_data :=
    match
      Data_encoding.Binary.of_bytes
        Alpha_context.Block_header.protocol_data_encoding protocol_data with
    | None => Pervasives.failwith "Cant_parse_protocol_data"
    | Some protocol_data => protocol_data
    end.
  
  Definition register (function_parameter : unit) : unit :=
    let '_ := function_parameter in
    let '_ :=
      Services_registration.register0 S.operations
        (fun (_ctxt : Alpha_context.t) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter :
              list Alpha_context.Operation.raw * option bool) =>
              let '(operations, check) := function_parameter in
              Error_monad.map_s
                (fun (raw_value : Alpha_context.Operation.raw) =>
                  let=? op := return= (parse_operation raw_value) in
                  let=? '_ :=
                    match check with
                    | Some true => Error_monad.return_unit
                    | (Some false | None) => Error_monad.return_unit
                    end in
                  return=? op) operations) in
    Services_registration.register0_noctxt S.block
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        fun (raw_block : Alpha_context.Block_header.raw) =>
          Error_monad._return
            (parse_protocol_data raw_block.(Block_header.t.protocol_data))).
  
  Definition operations {A : Set}
    (ctxt : RPC_context.simple A) (block : A) (check : option bool)
    (operations : list Alpha_context.Operation.raw)
    : M= (Error_monad.shell_tzresult (list Alpha_context.Operation.packed)) :=
    RPC_context.make_call0 S.operations ctxt block tt (operations, check).
  
  Definition block {A : Set}
    (ctxt : RPC_context.simple A) (block : A)
    (shell : Block_header.shell_header) (protocol_data : bytes)
    : M= (Error_monad.shell_tzresult Alpha_context.Block_header.protocol_data) :=
    RPC_context.make_call0 S.block ctxt block tt
      {| Block_header.t.shell := shell;
        Block_header.t.protocol_data := protocol_data |}.
End Parse.

Module S.
  Module level_query.
    Record record : Set := Build {
      offset : int32 }.
    Definition with_offset offset (r : record) :=
      Build offset.
  End level_query.
  Definition level_query := level_query.record.
  
  Definition level_query_value : RPC_query.t level_query :=
    RPC_query.seal
      (RPC_query.op_pipeplus
        (RPC_query.query_value
          (fun (offset : int32) => {| level_query.offset := offset |}))
        (RPC_query.field_value None "offset" RPC_arg.int32_value 0
          (fun (t_value : level_query) => t_value.(level_query.offset)))).
  
  Definition current_level
    : RPC_service.service Updater.rpc_context Updater.rpc_context level_query
      unit Alpha_context.Level.compat_t :=
    RPC_service.get_service
      (Some
        "Returns the level of the interrogated block, or the one of a block located `offset` blocks after in the chain (or before when negative). For instance, the next block if `offset` is 1.")
      level_query_value Alpha_context.Level.compat_encoding
      (RPC_path.op_div path "current_level").
  
  Definition levels_in_current_cycle
    : RPC_service.service Updater.rpc_context Updater.rpc_context level_query
      unit (Alpha_context.Raw_level.t * Alpha_context.Raw_level.t) :=
    RPC_service.get_service (Some "Levels of a cycle") level_query_value
      (Data_encoding.obj2
        (Data_encoding.req None None "first" Alpha_context.Raw_level.encoding)
        (Data_encoding.req None None "last" Alpha_context.Raw_level.encoding))
      (RPC_path.op_div path "levels_in_current_cycle").
End S.

Definition register (function_parameter : unit) : unit :=
  let '_ := function_parameter in
  let '_ := Scripts.register tt in
  let '_ := Forge.register tt in
  let '_ := Parse.register tt in
  let '_ :=
    Services_registration.register0 S.current_level
      (fun (ctxt : Alpha_context.t) =>
        fun (q : S.level_query) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            let level :=
              Alpha_context.Level.from_raw ctxt (Some q.(S.level_query.offset))
                (Alpha_context.Level.current ctxt).(Alpha_context.Level.t.level)
              in
            let=? '{|
              Alpha_context.Voting_period.info.voting_period := voting_period;
                Alpha_context.Voting_period.info.remaining := remaining
                |} :=
              Alpha_context.Voting_period.get_rpc_fixed_current_info ctxt in
            let blocks_per_voting_period :=
              Alpha_context.Constants.blocks_per_voting_period ctxt in
            let div_rem :=
              Int32.rem q.(S.level_query.offset) blocks_per_voting_period in
            let index_offset :=
              (q.(S.level_query.offset) /i32 blocks_per_voting_period) +i32
              (if div_rem >i32 remaining then
                1
              else
                0) in
            let voting_period_index :=
              voting_period.(Alpha_context.Voting_period.voting_period.index)
              +i32 index_offset in
            let start_position :=
              voting_period.(Alpha_context.Voting_period.voting_period.start_position)
              +i32 (index_offset *i32 blocks_per_voting_period) in
            let voting_period_position :=
              level.(Alpha_context.Level.t.level_position) -i32 start_position
              in
            return=?
              (Alpha_context.Level.to_deprecated_type level voting_period_index
                voting_period_position)) in
  Services_registration.register0 S.levels_in_current_cycle
    (fun (ctxt : Alpha_context.t) =>
      fun (q : S.level_query) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          let levels :=
            Alpha_context.Level.levels_in_current_cycle ctxt
              (Some q.(S.level_query.offset)) tt in
          match levels with
          | [] => Pervasives.raise (Build_extensible "Not_found" unit tt)
          | _ =>
            let first := List.hd (List.rev levels) in
            let last := List.hd levels in
            Error_monad._return
              (first.(Alpha_context.Level.t.level),
                last.(Alpha_context.Level.t.level))
          end).

Definition current_level {A : Set}
  (ctxt : RPC_context.simple A) (op_staroptstar : option int32)
  : A -> M= (Error_monad.shell_tzresult Alpha_context.Level.compat_t) :=
  let offset :=
    match op_staroptstar with
    | Some op_starsthstar => op_starsthstar
    | None => 0
    end in
  fun (block : A) =>
    RPC_context.make_call0 S.current_level ctxt block
      {| S.level_query.offset := offset |} tt.

Definition levels_in_current_cycle {A : Set}
  (ctxt : RPC_context.simple A) (op_staroptstar : option int32)
  : A ->
  M=
    (Error_monad.shell_tzresult
      (Alpha_context.Raw_level.t * Alpha_context.Raw_level.t)) :=
  let offset :=
    match op_staroptstar with
    | Some op_starsthstar => op_starsthstar
    | None => 0
    end in
  fun (block : A) =>
    RPC_context.make_call0 S.levels_in_current_cycle ctxt block
      {| S.level_query.offset := offset |} tt.
