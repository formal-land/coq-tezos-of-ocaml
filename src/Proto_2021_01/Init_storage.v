Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Bootstrap_storage.
Require TezosOfOCaml.Proto_2021_01.Commitment_storage.
Require TezosOfOCaml.Proto_2021_01.Contract_storage.
Require TezosOfOCaml.Proto_2021_01.Lazy_storage_diff.
Require TezosOfOCaml.Proto_2021_01.Level_repr.
Require TezosOfOCaml.Proto_2021_01.Level_storage.
Require TezosOfOCaml.Proto_2021_01.Parameters_repr.
Require TezosOfOCaml.Proto_2021_01.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Receipt_repr.
Require TezosOfOCaml.Proto_2021_01.Roll_storage.
Require TezosOfOCaml.Proto_2021_01.Script_repr.
Require TezosOfOCaml.Proto_2021_01.Seed_storage.
Require TezosOfOCaml.Proto_2021_01.Storage.
Require TezosOfOCaml.Proto_2021_01.Storage_sigs.
Require TezosOfOCaml.Proto_2021_01.Vote_storage.

Definition prepare_first_block
  (ctxt : Context.t)
  (typecheck :
    Raw_context.t -> Script_repr.t ->
    M=? ((Script_repr.t * option Lazy_storage_diff.diffs) * Raw_context.t))
  (level : int32) (timestamp : Time.t) (fitness : Fitness.t)
  : M=? Raw_context.t :=
  let=? '(previous_protocol, ctxt) :=
    Raw_context.prepare_first_block level timestamp fitness ctxt in
  match previous_protocol with
  | Raw_context.Genesis param =>
    let=? ctxt :=
      Commitment_storage.init_value ctxt param.(Parameters_repr.t.commitments)
      in
    let=? ctxt := Roll_storage.init_value ctxt in
    let=? ctxt := Seed_storage.init_value ctxt in
    let=? ctxt := Contract_storage.init_value ctxt in
    let=? ctxt :=
      Bootstrap_storage.init_value ctxt typecheck
        param.(Parameters_repr.t.security_deposit_ramp_up_cycles)
        param.(Parameters_repr.t.no_reward_cycles)
        param.(Parameters_repr.t.bootstrap_accounts)
        param.(Parameters_repr.t.bootstrap_contracts) in
    let=? ctxt := Roll_storage.init_first_cycles ctxt in
    let=? ctxt :=
      Vote_storage.init_value ctxt
        (Level_storage.current ctxt).(Level_repr.t.level_position) in
    let=? ctxt :=
      Storage.Block_priority.(Storage.GET_SET_INIT.init_value) ctxt 0 in
    Vote_storage.update_listings ctxt
  | Raw_context.Edo_008 =>
    let balance_updates {A : Set} : list A :=
      nil in
    Storage.Pending_migration_balance_updates.(Storage_sigs.Single_data_storage.init_value)
      ctxt balance_updates
  end.

Definition prepare
  (ctxt : Context.t) (level : Int32.t) (predecessor_timestamp : Time.t)
  (timestamp : Time.t) (fitness : Fitness.t)
  : M=? (Raw_context.t * Receipt_repr.balance_updates) :=
  let=? ctxt :=
    Raw_context.prepare level predecessor_timestamp timestamp fitness ctxt in
  let=? function_parameter :=
    Storage.Pending_migration_balance_updates.(Storage_sigs.Single_data_storage.get_option)
      ctxt in
  match function_parameter with
  | Some balance_updates =>
    let= ctxt :=
      Storage.Pending_migration_balance_updates.(Storage_sigs.Single_data_storage.remove)
        ctxt in
    Error_monad._return (ctxt, balance_updates)
  | None => Error_monad._return (ctxt, nil)
  end.
