Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Period_repr.

Include Time.

Definition time : Set := t.

(** Init function; without side-effects in Coq *)
Definition init_module_repr : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent "timestamp_add"
      "Timestamp add" "Overflow when adding timestamps."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Overflow when adding timestamps."
                  CamlinternalFormatBasics.End_of_format)
                "Overflow when adding timestamps."))) Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Timestamp_add" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Timestamp_add" unit tt) in
  Error_monad.register_error_kind Error_monad.Permanent "timestamp_sub"
    "Timestamp sub" "Subtracting timestamps resulted in negative period."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "Subtracting timestamps resulted in negative period."
                CamlinternalFormatBasics.End_of_format)
              "Subtracting timestamps resulted in negative period.")))
    Data_encoding.empty
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Timestamp_sub" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Timestamp_sub" unit tt).

Definition of_seconds_string (s : string) : option t :=
  Option.map of_seconds (Int64.of_string_opt s).

Definition to_seconds_string (s : t) : string := Int64.to_string (to_seconds s).

Definition pp : Format.formatter -> t -> unit := pp_hum.

Definition op_plusquestion (x : t) (y : Period_repr.period) : M? t :=
  (* ❌ Try-with are not handled *)
  try_with (fun _ => return? (add x (Period_repr.to_seconds y)))
    (fun _ => Error_monad.error_value (Build_extensible "Timestamp_add" unit tt)).

Definition op_minusquestion (x : t) (y : t) : M? Period_repr.period :=
  Error_monad.record_trace (Build_extensible "Timestamp_sub" unit tt)
    (Period_repr.of_seconds (diff_value x y)).
