Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.
Unset Positivity Checking.

Require Import TezosOfOCaml.Proto_2021_01.Environment.

Definition lazyt (a : Set) : Set := unit -> a.

Inductive lazy_list_t (a : Set) : Set :=
| LCons : a -> lazyt (M=? (lazy_list_t a)) -> lazy_list_t a.

Arguments LCons {_}.

Definition lazy_list (a : Set) : Set := M=? (lazy_list_t a).

Fixpoint op_minusminusgt (i : int) (j : int) {struct i} : list int :=
  if i >i j then
    nil
  else
    cons i (op_minusminusgt (Pervasives.succ i) j).

Fixpoint op_minusminusminusgt (i : int32) (j : int32) {struct i} : list int32 :=
  if i >i32 j then
    nil
  else
    cons i (op_minusminusminusgt (Int32.succ i) j).

Module Split.
  Fixpoint do_slashes
    (delim : ascii) (path : string) (l_value : int) (acc_value : list string)
    (limit : int) (i : int) {struct i} : list string :=
    if i >=i l_value then
      List.rev acc_value
    else
      if Compare.Char.(Compare.S.op_eq) (String.get path i) delim then
        do_slashes delim path l_value acc_value limit (i +i 1)
      else
        do_split delim path l_value acc_value limit i
  
  with do_split
    (delim : ascii) (path : string) (l_value : int) (acc_value : list string)
    (limit : int) (i : int) {struct i} : list string :=
    if limit <=i 0 then
      if i =i l_value then
        List.rev acc_value
      else
        List.rev (cons (String.sub path i (l_value -i i)) acc_value)
    else
      do_component delim path l_value acc_value (Pervasives.pred limit) i i
  
  with do_component
    (delim : ascii) (path : string) (l_value : int) (acc_value : list string)
    (limit : int) (i : int) (j : int) {struct j} : list string :=
    if j >=i l_value then
      if i =i j then
        List.rev acc_value
      else
        List.rev (cons (String.sub path i (j -i i)) acc_value)
    else
      if Compare.Char.(Compare.S.op_eq) (String.get path j) delim then
        do_slashes delim path l_value
          (cons (String.sub path i (j -i i)) acc_value) limit j
      else
        do_component delim path l_value acc_value limit i (j +i 1).
End Split.

Definition split (delim : ascii) (op_staroptstar : option int)
  : string -> list string :=
  let limit :=
    match op_staroptstar with
    | Some op_starsthstar => op_starsthstar
    | None => Pervasives.max_int
    end in
  fun (path : string) =>
    let l_value := String.length path in
    if limit >i 0 then
      Split.do_slashes delim path l_value nil limit 0
    else
      [ path ].

Definition pp_print_paragraph (ppf : Format.formatter) (description : string)
  : unit :=
  Format.fprintf ppf
    (CamlinternalFormatBasics.Format
      (CamlinternalFormatBasics.Formatting_gen
        (CamlinternalFormatBasics.Open_box
          (CamlinternalFormatBasics.Format
            CamlinternalFormatBasics.End_of_format ""))
        (CamlinternalFormatBasics.Alpha
          (CamlinternalFormatBasics.Formatting_lit
            CamlinternalFormatBasics.Close_box
            CamlinternalFormatBasics.End_of_format))) "@[%a@]")
    (Format.pp_print_list (Some Format.pp_print_space) Format.pp_print_string)
    (split " " % char None description).

Definition take {A : Set} (n : int) (l_value : list A)
  : option (list A * list A) :=
  let fix loop {B : Set} (acc_value : list B) (n : int) (xs : list B)
    : option (list B * list B) :=
    if n <=i 0 then
      Some ((List.rev acc_value), xs)
    else
      match xs with
      | [] => None
      | cons x xs => loop (cons x acc_value) (n -i 1) xs
      end in
  loop nil n l_value.

Definition remove_prefix (prefix : string) (s : string) : option string :=
  let x := String.length prefix in
  let n := String.length s in
  if
    (n >=i x) && (Compare.String.(Compare.S.op_eq) (String.sub s 0 x) prefix)
  then
    Some (String.sub s x (n -i x))
  else
    None.

Fixpoint remove_elem_from_list {A : Set}
  (nb : int) (function_parameter : list A) : list A :=
  match
    (function_parameter,
      match function_parameter with
      | (cons _ _) as l_value => nb <=i 0
      | _ => false
      end) with
  | ([], _) => nil
  | ((cons _ _) as l_value, true) => l_value
  | (cons _ tl, _) => remove_elem_from_list (nb -i 1) tl
  end.

Definition result_of_exception {A : Set} (e : unit -> A)
  : Pervasives.result A extensible_type :=
  (* ❌ Try-with are not handled *)
  try_with (fun _ => Pervasives.Ok (e tt))
    (fun exn_value => Pervasives.Error exn_value).
