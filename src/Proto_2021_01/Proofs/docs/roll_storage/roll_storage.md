> Verification of the functional properties of the rolls storage in the economic protocol

Goal: verification of the extracted code by `coq-of-ocaml` on roll storage.

From: 4 January, 2021 to 5 February, 2021

By: Ly Kim Quyen

[TOC]

## Proposed

- [x] `clear_cycle`
- [ ] `init_first_cycles ` (this function is called in `init_storage.v`, so I do not prove it at this stage)
- [x] `cycle_end`
- [x] `snapshot_rolls_for_cycle`
- [x] `snapshot_rolls`
- [x] `freeze_rolls_for_cycle`
- [x] `set_inactive` (non-termination function)
- [x] `set_active` (non-termination function)
- [x] `is_inactive` 
- [x] `fresh_roll`
- [x] `get_limbo_roll`
- [x] `add_amount`  (note: non-termination function)
- [x] `remove_amount` (note: non-termination function)

Gitlab: https://gitlab.com/formal-land/coq-tezos-of-ocaml/-/tree/quyen@roll_proofs_update_master/src/Proto_2021_01/Proofs

The work has been presented at the Verif-NL team meeting on the 4-Feb-2021.

## Implementation

### Details of the implementation

**Notes**: The properties in `Redefine_functions` and `Delegate_redefine` are **on model functions** and **not on `coq-of-ocaml`** elaborated functions. Together with their pre-conditions. Also, there is no proof that the redefine functions mentioned are equivalence to the original one.

#### In the Section `Redefine_functions`

| Functions                          | Properties                                | Meaning                                                      |
| ---------------------------------- | ----------------------------------------- | ------------------------------------------------------------ |
| -`get_rolls`<br/>-`traverse_rolls` | a.`get_rolls_nil`<br/>b. `get_rolls_some` | a. `get_rolls` returns an empty list in the case of None. <br/> b. `get_rolls` calls the function `traverse_rolls` in the Some case. Prove the equivalent of the context of `get_rolls` and `traverse_rolls` . |
| `count_rolls`                      | `count_rolls_empty`                       | `count_rolls` return 0 in the case of None.                  |

Summary of Lemmas 

```
Lemma get_rolls_nil (ctxt: Raw_context.t) (delegate: Signature.public_key_hash) :
 delegate_roll_list ctxt delegate = return=? None ->
 Roll_storage.get_rolls ctxt delegate = Error_monad.return_nil.

Lemma get_rolls_some (ctxt: Raw_context.t) (delegate: Signature.public_key_hash)
      (head_roll: int) :
  (delegate_roll_list ctxt delegate = (return=? Some head_roll)) ->
   get_rolls ctxt delegate = traverse_rolls ctxt head_roll.
  
Lemma count_rolls_empty (ctxt: Raw_context.t) (delegate: Signature.public_key_hash) :
  (delegate_roll_list ctxt delegate = return=? None ->
   count_rolls ctxt delegate = return=? 0).
```

#### In the Module `Delegate_redefine`

- `add_amount`: given a context, a delegate account, and an amount tez. Manipulate on the amount tez, the storage, and returns an update of the context. The original extracted code has a local recursive definition inside the function. The `Coq` compiler could not guess the decreasing variable, `coq-of-ocaml` uses the `Uset Guard Checking` to disable the positivity guard checking and/or the non-termination checking so that the compiler in `Coq` will accept this function at compile time. 

  Currently, `coq-of-ocaml` will raise a warning whenever it encounters such a case. It is then up to a developer to decide how to write function(s) that safer.

  The current solution for `add_amount` is to define top-level functions, then prove their properties accordingly, change the local `fixpoint` to a fold over list. It is then divided into 3 sub-functions: `add_amount_aux`, `add_and_create_roll`, `add_amount`. The next step is proving that the redefine `add_amount` is equivalent to the `add_amount` that `coq-of-ocaml` generated. At this stage of the project, I do not have the proof of it yet. 

  Redefine functions that encounter the unsupported features of  `coq-of-ocaml` directly in `Coq` is the first step. The next step is having the equivalent proof between these functions and their original one, then we can do the modification in the `OCaml` source code. Using the `coq-of-ocaml` to extract this new `OCaml` code back into `Coq`, then check that if the function(s) generated pass the `Coq` compiler without using the `Uset Guard Checking`, and then write the proves from this newly extracted code. 

  Once these steps are archived, one needs to write tests for the modified `OCaml` in `OCaml` (or some other test platform) to check that these modifications do not change the behavior or performance, etc. of the original code, also with the other functions that call from/to these functions as well.

  Below is an explanation of each redefine functions:

  - `add_amount_aux`: this is the replacement of the local recursive in the original function. The two properties that have been proved for this function are `add_amount_aux_sum` and `add_amount_aux_eq_ctxt`. 
    - The first lemma prove that given two amounts `a1, a2`, apply the `add_amount_aux` on `a1` and `a2`, the total of the new amount of `a1` and `a2` is equal to apply the `add_amount_aux` on the total of `a1 + a2`.
    - The second lemma prove the equivalent of the context. Given two amounts `a1` and `a2`, if `a1 = a2`, then apply the `add_amount_aux` on `a1` is equal to the context apply the `add_amount_aux` on `a2`.
  - `add_and_create_roll`: this function calls the function `add_amount_aux`, then checking the condition on the storage of delegate rolls whether or not there are some rolls. In the case that it has some rolls, then it will call the function to map the newly updated context into active delegate storage (function `active_delegate_with_rolls`). 
    - The lemma `add_and_create_roll_eq_ctxt` prove that in the case of some, the context returns from the function `add_and_create_roll` is equal to the context returns from the function `active_delegate_with_rolls`.
  - `add_amount`: this is a final redefine function. Check that if the delegate in the context has the initial value, and it checks the condition of the delegate account. In case if this delegate is active then it will call the computation on amount and storage, in this case, it will call the function `add_and_create_roll` above.
    - `add_amount_eq_ctxt`: prove that the context returns from the function `add_amount` is equal to the context returns from the function `add_and_create_roll` in the case the check at the inactive delegate returns false.

| Functions             | Properties                                               |
| --------------------- | -------------------------------------------------------- |
| `add_amount_aux`      | a. `add_amount_aux_sum` <br/>b. `add_amount_aux_eq_ctxt` |
| `add_and_create_roll` | `add_and_create_roll_eq_ctxt`                            |
| `add_amount`          | `add_amount_eq_ctxt`                                     |

These lemmas below are a simpler version of the original one in `Coq`,  I remove all their preconditions:

```
Lemma add_amount_aux_sum (ctxt: Raw_context.t)
        (delegate: Signature.public_key_hash)
        (pub: Signature.public_key)
        (a1 a2 change : Tez_repr.t) :
(*preconditions*)
...
Error_monad.terminates_lwt (
        let=? '(new_a1, _) := add_amount_aux ctxt delegate a1 in
        let=? '(new_a2, _) := add_amount_aux ctxt delegate a2 in
        let total_a1_a2 := new_a1 +i64 new_a2 in
        let=? '(total, _) := add_amount_aux ctxt delegate (a1 +i64 a2) in
        return=? (Tez_repr.op_eq total_a1_a2 total)).

Lemma add_amount_aux_eq_ctxt (ctxt: Raw_context.t)
        (delegate: Signature.public_key_hash)
        (a1 a2: Tez_repr.t) :
     Tez_repr.equal a1 a2 = true ->
     let ctxt1 := add_amount_aux ctxt delegate a1 in
     let ctxt2 := add_amount_aux ctxt delegate a2 in
     ctxt1 = ctxt2.
     
Lemma add_and_create_roll_eq_ctxt (ctxt: Raw_context.t)
        (delegate: Signature.public_key_hash)
        (amount new_amount: Tez_repr.t) roll :
   (*preconditions*)
    ...
    let add_create_ok :=
        add_and_create_roll ctxt delegate amount = Return (Pervasives.Ok ctxt)
    in
    let active_ok :=
        active_delegate_with_rolls ctxt delegate = Return (Pervasives.Ok ctxt)
    in
    add_create_ok = active_ok.
    
Lemma add_amount_eq_ctxt (ctxt: Raw_context.t) (delegate: Signature.public_key_hash)
        (amount : Tez_repr.t) :
    (*preconditions*)
    ...
    let add_amount_ok := add_amount ctxt delegate amount = Return (Pervasives.Ok ctxt) in
    let add_and_create_roll_ok := add_and_create_roll ctxt delegate amount = Return (Pervasives.Ok ctxt) in
    add_amount_ok = add_and_create_roll_ok.
```

- `remove_amount`:  given a context, a delegate account, and an amount of tez. Manipulate on the amount of tez, the storage, and returns an update of context. This function also has the local recursive as in the `add_amount`. Using the same solution as done in `add_amount`. It is then divided into 4 sub-functions: `fold_aux`, `remove_amount_aux`, `remove_amount_tez`, `remove_amount`
  - `fold_aux`: this is the replacement of the local recursive.
  - `remove_amount_aux`: this function calls the function `fold_aux` to do the computation, check the delegate status is inactive or not, and check if the delegate has some rolls or not. 
    - The lemma `remove_amount_aux_sub`: given two amounts `a1`, `a2`, apply the `remove_amount_aux` on `a1` and `a2`, the subtraction of new amount of `a1` and `a2` is equal to apply the `remove_amount_aux` on the subtraction of `a1 - a2`.
  - `remove_amount_tez`: this function calls `remove_amount_aux` and then computes the new change with the change and the context returns from the function `remove_amount_aux`. 
    - The lemma `remove_amount_tez_sub`: given two amounts `a1`, `a2`, apply the `remove_amount_tez` on `a1` and `a2`, the subtraction of new amount of `a1` and `a2` is equal to apply the `remove_amount_tez` on the subtraction of `a1 - a2`.
  - `remove_amount`: this is a final redefine function. This function calls the function `remove_amount_tez`. 
    - The lemma `remove_amount_eq_ctxt`: prove that the context returns from the function `remove_amount` is equal to the context returns from the function `remove_amount_tez` together with the context update when setting the new context, new amount into storage of delegate.
    - The lemma `remove_amount_eq_tez_ctxt`: prove the equivalent of the context, given two amounts `a1` and `a2`, if `a1 = a2`, then apply the `remove_amount` on `a1` is equal to the context apply the `remove_amount` on `a2`.

| Functions           | Properties                                                   |
| ------------------- | ------------------------------------------------------------ |
| `fold_aux`          |                                                              |
| `remove_amount_aux` | `remove_amount_aux_sub`                                      |
| `remove_amount_tez` | `remove_amount_tez_sub`                                      |
| `remove_amount`     | a. `remove_amount_eq_ctxt` <br/>b. `remove_amount_eq_tez_ctxt` |

Summary of lemmas 

```
Lemma remove_amount_aux_sub (ctxt: Raw_context.t)
        (delegate: Signature.public_key_hash)
        (a1 a2 change: Tez_repr.t) roll :
  (*preconditions*)
  ...
   Error_monad.terminates_lwt
      (let=? '(_, new_a1) := remove_amount_aux ctxt delegate a1 in
       let=? '(_, new_a2) := remove_amount_aux ctxt delegate a2 in
       let sub_a1_a2 := new_a1 -i64 new_a2 in
       let=? '(_, new_sub) := remove_amount_aux ctxt delegate (a1 -i64 a2) in
       return=? (Tez_repr.op_eq sub_a1_a2 new_sub)).
       
Lemma remove_amount_tez_sub (ctxt: Raw_context.t) (delegate: Signature.public_key_hash)
        (a1 a2 change: Tez_repr.t) roll :
    (*preconditions*)
    ...
     Error_monad.terminates_lwt (
        let=? sub_a1_a2 :=
           let=? '(_, new_a1) := remove_amount_tez ctxt delegate a1 in
           let=? '(_, new_a2) := remove_amount_tez ctxt delegate a2 in
           return=? (new_a1 -i64 new_a2) 
        in
        let=? '(_, sub) := remove_amount_tez ctxt delegate (a1 -i64 a2) in
        return=? (Tez_repr.op_eq sub_a1_a2 sub)).
 
Lemma remove_amount_eq_ctxt (ctxt: Raw_context.t) (delegate: Signature.public_key_hash)
        (amount: Tez_repr.t) :
    Error_monad.terminates_lwt (
        let=? '(ctxt, change) := remove_amount_tez ctxt delegate amount in
        set_delegate_change ctxt delegate change) =
    Error_monad.terminates_lwt (remove_amount ctxt delegate amount).
    
Lemma remove_amount_eq_tez_ctxt (ctxt: Raw_context.t)
        (delegate: Signature.public_key_hash)
        (a1 a2: Tez_repr.t) :
    Tez_repr.equal a1 a2 = true ->
    let ctxt1 := remove_amount ctxt delegate a1 in
    let ctxt2 := remove_amount ctxt delegate a2 in
    ctxt1 = ctxt2.
```

- `set_inactive`: given a context, a delegate account. This function also has the local recursive function. It is then divided into 2 sub-functions: `set_inactive_aux` and `set_inactive`.

  - `set_inactive_aux`: this is the replacement of the local recursive. 
    - The lemma `set_inactive_aux_eq_ctxt`: prove the context returns from the function `set_inactive_aux` is equal to the context returns from the function `Roll_storage.Delegate.pop_roll_from_delegate` (this is an elaborate function from the `coq-of-ocaml`). For the computation on tez, I do not write the prove for it, it would have the same properties as `add_amount` or `remove_amount`.
    - `set_inactive`: it is a final redefine function. I do not write the prove for it. It would be proving the equivalent of the context.

  Summary of lemma

  ```
   Lemma set_inactive_aux_eq_ctxt (ctxt: Raw_context.t)
          (delegate: Signature.public_key_hash)
          (tokens_per_roll change: Tez_repr.t) roll :
          (*preconditions*)
          ...
      Error_monad.terminates_lwt (set_inactive_aux ctxt delegate tokens_per_roll change) =
      Error_monad.terminates_lwt (
          let=? '(_, ctxt) := Roll_storage.Delegate.pop_roll_from_delegate ctxt delegate in
          let=? change1 := return= (Tez_repr.op_plusquestion change tokens_per_roll) in
          return=? (ctxt, change1)). 
  ```

- `set_active`: given a context, a delegate account. This function also has the local recursive function. It is then divided into 3 sub-functions: `set_active_aux`, `expiration_aux`, `set_active`.

  - `set_active_aux`: this is the replacement of the local recursive. I do not write the prove for this function yet. The properties are similar to the properties for `add_amount`.
- `expiration_aux`: this function takes a context, a delegate, and does the computation on cycle. It returns an update of the cycle. It has two cases, the none case and some case. 
  
  - The lemma `expiration_aux_none`: it is the case when this function returns none case, prove the cycle is equal to the cycle computed in the function. 
  - The lemma `expiration_aux_some`: prove the cycle is equal to the cycle computed in the function in the case the condition for the delegate checking its inactive returns true. For the inactive returns false case, I do not write the prove for it yet. 
  - `set_active`: this is a final redefine function. This function calls the function `set_active_aux` and `expiration_aux`. I do not write any lemma for this function. It would prove the context returns from `set_active` has context that is equal to all the functions that it calls.

| Functions        | Properties                                             |
| ---------------- | ------------------------------------------------------ |
| `set_active_aux` |                                                        |
| `expiration_aux` | a. `expiration_aux_none` <br/>b. `expiration_aux_some` |
| `set_active`     |                                                        |

Summary of lemmas

```
Lemma expiration_aux_none (ctxt: Raw_context.t) (delegate: Signature.public_key_hash) :
    (*preconditions*)
    ...
    let c := Cycle_repr.add current_cycle (1 +i (2 *i preserved_cycles)) in
    expiration_aux ctxt delegate = (return=? c).
    
Lemma expiration_aux_some (ctxt: Raw_context.t)
        (delegate: Signature.public_key_hash)
        (current_exp : Cycle_repr.t) :
    (*preconditions*)
    ...
    let c := Cycle_repr.max current_exp updated in
    expiration_aux ctxt delegate = (return=? c).
```

#### In the Module `Delegate` 

**Note**: the functions in this module are on `coq-of-ocaml` elaborated functions, except the functions `fresh_roll_limbo` is a model function from `fresh_roll`, and `cycle_end_simpl` from `cycle_end`.

| Functions                                          | Properties                                      | Meaning                                                      |
| -------------------------------------------------- | ----------------------------------------------- | ------------------------------------------------------------ |
| - `fresh_roll_limbo` <br/>- `get_limbo_roll`       | `fresh_limbo_roll`                              | Prove the equivalent in the context <br/>between two functions `fresh_roll_limbo` and `get_limbo_roll` |
| `ensure_inited`                                    | `ensure_inited_not_mem`                         | Prove the case the condition returns false                   |
| `is_inactive`                                      | a. `is_inactive_none`<br/>b. `is_inactive_some` | a. Prove the function `is_inactive` is false in a none case <br/>b. Prove the function `is_inactive` is false in a some case |
| - `snapshot_rolls`<br/>-`snapshot_rolls_for_cycle` | `snapshot_rolls_snapshot_rolls_for_cycle`       | Prove the equivalent in the context <br/>between two functions |
| - `cycle_end`<br/>- `freeze_rolls_for_cycle`       | `cycle_end`                                     | Prove the equivalent in the context <br/>between two functions |
| `cycle_end_simpl`                                  | `cycle_end_clear_cycle`                         | Prove the case it returns some case and its context is equal to the context of function `clear_cycle` |
| `op_plusquestion`                                  | `op_plusquestion_false`                         | This is a property for `Tez_repr.v`                          |

Summary of lemmas

```
Lemma fresh_limbo_roll (ctxt: Raw_context.t) :
    limbo_none ctxt ->
    Error_monad.terminates_lwt (fresh_roll_limbo ctxt) =
    Error_monad.terminates_lwt (Roll_storage.Delegate.get_limbo_roll ctxt).

Lemma ensure_inited_not_mem (ctxt: Raw_context.t) (delegate: Signature.public_key_hash) :
    is_delegate_change ctxt delegate = (return= false) ->
    Roll_storage.Delegate.ensure_inited ctxt delegate =
    Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.__init_value)
                                   ctxt delegate Tez_repr.zero.
                                   
Lemma is_inactive_none (ctxt: Raw_context.t) (delegate: Signature.public_key_hash) :
    inactive_delegate ctxt delegate = (return= false) ->
    Delegate_redefine.delegate_desactivation ctxt delegate = (return=? None) ->
    (Roll_storage.Delegate.is_inactive ctxt delegate = return=? false).
    
Lemma is_inactive_some (ctxt: Raw_context.t)
        (delegate: Signature.public_key_hash)
        (last_active_cycle : Cycle_repr.t) :
    Delegate_redefine.delegate_desactivation ctxt delegate  =
    Return (Pervasives.Ok (Some last_active_cycle)) ->
    some_delegate_desactivation ctxt delegate last_active_cycle = Return (Pervasives.Ok false) ->
    inactive_delegate ctxt delegate = (return= false) ->
    (Roll_storage.Delegate.is_inactive ctxt delegate = return=? false).
    
Lemma snapshot_rolls_snapshot_rolls_for_cycle (ctxt: Raw_context.t) (cycle: Cycle_repr.t) :
  let ctxt' := Roll_storage.snapshot_rolls_for_cycle ctxt cycle in
  let ctxt'' := Roll_storage.snapshot_rolls ctxt in
  cycle =
  (Cycle_repr.add
     (Raw_context.current_level ctxt).(Level_repr.t.cycle)
                                        (Constants_storage.preserved_cycles ctxt +i 2)) ->
  Error_monad.terminates_lwt ctxt' -> Error_monad.terminates_lwt ctxt'' ->
  ctxt' = ctxt''.
  
Lemma cycle_end_clear_cycle (ctxt: Raw_context.t) (last_cycle: Cycle_repr.cycle) :
  let ctxt' := cycle_end_simpl ctxt last_cycle in
  let preserved := Constants_storage.preserved_cycles ctxt in
  forall cleared_cycle, Cycle_repr.sub last_cycle preserved = Some cleared_cycle ->
  let ctxt'' := Roll_storage.clear_cycle ctxt cleared_cycle in
  Error_monad.terminates_lwt ctxt' -> Error_monad.terminates_lwt ctxt'' ->
  ctxt' = ctxt''.
  
Lemma cycle_end (ctxt: Raw_context.t)
      (cleared_cycle last_cycle: Cycle_repr.cycle) :
  let preserved := Constants_storage.preserved_cycles ctxt in
  (Cycle_repr.sub last_cycle preserved = Some cleared_cycle) ->
  Error_monad.terminates_lwt (
      let=? ' ctxt0 := Roll_storage.clear_cycle ctxt cleared_cycle in
      let=? ' ctxt1 := Roll_storage.freeze_rolls_for_cycle
                         ctxt0
                         (Cycle_repr.add last_cycle (preserved +i 1))
      in
      Storage.Roll.Snapshot_for_cycle.(Storage_sigs.Indexed_data_storage.__init_value)
                                        ctxt1
                                        (Cycle_repr.succ
                                           (Cycle_repr.succ
                                              (Cycle_repr.add last_cycle
                                                              (preserved +i 1))))
                                        0)
  = Error_monad.terminates_lwt (Roll_storage.cycle_end ctxt last_cycle).
  
Lemma op_plusquestion_false (t1 t2: Tez_repr.t) :
  let total := t1 +i64 t2 in
  Tez_repr.op_lt total t1 = false ->
  Tez_repr.op_plusquestion t1 t2 = (return? total).
```

### Discussion

#### Issues encountered and contributions 

- Non-termination functions

  One of the decisions for the extracted code in `coq-of-ocaml` is, it disables the positivity checking of inductive types and the termination checking of `Fixpoint`s by using the `Uset Guard Checking`. 

  In roll storage these functions are not able to prove their properties because of the setting:

  `set_inactive, set_active, add_amount, remove_amount, count_rolls`

  To deal with the disabling of the termination check: there is no real solution for it. One of the workarounds for this is by define in `Coq` a function at the top level. More details can be found at the minutes meeting: https://codimd.nomadic-labs.com/s/s4KfnpHsa

- Others

  - Preconditions 

    - Error monad: assume the preconditions in the properties to be able to prove the `Error` cases.
    - Assume the preconditions to prove some specific properties.

  - Data types

    The data type in roll storage, more specifically the type of `Cycle_repr.t` is `Int32.v`, and the type of `Tez_repr.t` is `Int64.v` those files are generated from `src/lib_protocol_environment/sigs/v2.ml` as `Parameter`. 

    The current choice for the data types is assumed as type `Z` in `Coq`. I defined the definition of `Int64.v` from `Parameter` to `Definition` of type `Z`. The reason for this choice is that at this stage, we do not take into account the issues related to data types differences, for instance, the correctness when converting one type to another type, overflow addition, etc. This is an issue that one needs to take into account to make sure the faithfulness of the original source code.

  - Update of the extracted code

    In the time of working on the roll storage, there is an update in the extracted code, base on the improvement of the source code of `coq-of-ocaml` (remove the existential variables), also there is an update to the protocol 2021-02-01. It leads to generate a new extracted code. It means that the proofs for the roll storage with the old extracted code may not be relevant anymore. This is an objective of the project to test whether or not the proofs will require a lot of effort to adapt to the update or not.

    For the case of roll storage, I managed to move the proofs of the old source code to the updated one. This work requires less effort because the updated extracted code does not change much at the file `Roll_storage.v`.

  - `OCaml` source code:

    - Discover the code that is implemented but not use, the module `Single_carbonated_data_storage` in storage `storage_sigs.ml`. https://gitlab.com/nomadic-labs/tezos/-/issues/332

  - Tips on proving 

    - Properties inside function

      A function defines in the roll storage normally carry several computations. To make it simpler to prove all the properties inside a function, I define each condition as a sub-function and then proof its properties. For instance, the case of the function `cycle_end`, the condition `last_cycle - preserved_cycle` (`last_cycle >= preserved_cycle`) if there is any cycle left, it will call the function `clear_cycle` to delete these cycles.
  
      ```
      Definition cycle_end_simpl (ctxt: Raw_context.t) (last_cycle: Cycle_repr.cycle) :=
        let preserved := Constants_storage.preserved_cycles in
        let=? ctxt :=
          match Cycle_repr.sub last_cycle preserved with
          | None => return=? ctxt
          | Some cleared_cycle => Roll_storage.clear_cycle ctxt cleared_cycle
          end
        in
        return=? ctxt.
       
       Lemma cycle_end_clear_cycle (ctxt: Raw_context.t) (last_cycle: Cycle_repr.cycle) :
        let ctxt' := cycle_end_simpl ctxt last_cycle in
        let preserved := Constants_storage.preserved_cycles ctxt in
      forall cleared_cycle, Cycle_repr.sub last_cycle preserved = Some cleared_cycle ->
                         let ctxt'' := Roll_storage.clear_cycle ctxt cleared_cycle in
      Error_monad.terminates_lwt ctxt' -> Error_monad.terminates_lwt ctxt'' ->
        ctxt' = ctxt''.
      ```
      
    - Case analysis: Sometimes while calling the `destruct`, `simpl`, or `case_eq` it can take a very long time, or it is not able to perform the tactic, and raise an error for instance: 
    
      ```
    ctxt : Raw_context.t
      delegate : public_key_hash
    head_roll : int
      t : Roll_repr.t
    The term "ctxt" has type "Raw_context.t" while it is expected to have type
       "(|?H.(@Storage_functors.Make_indexed_subcontext.C)|).(Raw_context.T.t)".
      ```
  
      The reason sometimes the expression is in the lambda,  but it can also be another reason which I do not encounter it yet. To avoid this, one way is to define functions that call the storage for instance: `get, set, init`, etc. as a function. For instance: 
    
      ```
    Definition active_delegate_del (ctxt: Raw_context.t)
                   (delegate: Signature.public_key_hash) :=
       Storage.Active_delegates_with_rolls.(Storage_sigs.Data_set_storage.del)                              ctxt delegate.
      ```
      
  

#### What can we do from here?

##### Verify the codes in the economic protocol

The current work for verification using the extracted code from `coq-of-ocaml` are: 

- `Script_ir_translator` is a Michelson type-checker that is called when creating a new smart contract and `Script_interpreter` is the interpreter that is called when transferring tokens to a new smart contract. This is a work in progress by Guillaume Claret for the Mi-Cho-Coq team.
- The `Storage` module and storage functors. The type `Context.t` of the store is complex as it involves serialization/deserialization and accesses to sub-parts of the key-value `Raw_context.t`.  So the goal is to have the abstraction of the whole storage showing that `Context.t` store is equivalent to the simpler type, with a standard record type. This is a work in progress of Guillaume Claret. 
- The `Environment`: extracted from `src/lib_protocol_environment/sigs/v2.ml` . Currently, `Error_monad.v`, `Lwt.v`, `Context.v`, `Data_encoding.v`, `Option.v`, `Protocol_hash.v`  are a work in progress whenever one encounters functions that need to be proved. 
- The module `_*.repr.v`:  These module abstracts define the data types used by the protocol. 
  - File `Gas_limit_repr.v`. The proofs that have been done for this file was on the old extracted code. Currently, there was an optimization of the file `gas_limit_repr.ml` in Tezos source code. The proofs need to be updated to the newly extracted code. It is a to-do work for me.  
  - `Saturation_repr.v`: the work started recently by Guillaume Claret and Arvid.
- The module `*_storage.v`: these modules ensuring the internal consistency of the context structure. 
  - `Level_storage.v ` : the proofs have been done in the old extracted code. It needs to be updated to the newly updated extracted code. This is a to-do work for me.
  - `Roll_storage.v`: work of this project.
  - `Vote_storage.v`: work in progress by Guillaume Claret.

Below is a list of files in the folder `proto_alpha/lib_protocol` that can be proved in the perspective of the correctness of functions.

- `*.repr.v`: 
  - `Operation_repr.v`: note that this file has the `Unset Guard Checking` declaration, which means that one will encounter the non-termination function(s) or the positivity condition. One way is to use the workaround proposed or another workaround to adapt to the specific function(s) that one encounter. It can be at the Inductive types (`Inductive ... with .. `). There are also several `Axiom` that is assumed for some function.
  - `Period_repr.v`
  - `Tez_repr.v`: this file has `Uset Guard Checking` 
  
- `*_storage.v`: 
  - `Bootstrap_storage.v`
  - `Constants_storage.v`
  - `Delegate_storage.v`:  in my opinion, one can choose to work on this as the next step of the work on `Rolls_storage.v`, because it is related to `Roll_storage.v`,  `Level_repr.v`, `Tez_repr.v`, etc. 
  - `Fees_storage.v`
  - `Fitness_storage.v`
  - `Init_storage.v`
  - `Nonce_storage.v`
  - `Sapling_storage.v`: this file has `Unset Guard Checking`
  - `Seed_storage.v`
  - `Voting_period_storage.v`

- `Storage_cost.v` : call from the `Gas_limit_repr.v`

- `Voting_period_repr.v`: call from the `Level_repr.v`

- From `Apply.v`

  - `Admendment.v`
  - `Baking.v`:  this file has `Unset Guard Checking`
  - `Main.v`
  - `Raw_context.v`:  this file has `Unset Guard Checking`

##### Unsupported features of `coq-of-ocaml`

 Below is a list of files in the `protocol_alpha/lib_protocol` that have the unsupported features for GADT:

- `apply.ml`
- `contract_storage.ml`
- `lazy_storage_diff.ml`
- `lazy_storage_kind.ml`

Below is a list of files that have the unsupported features for without guard checking:

- `apply.ml`
- `apply_result.ml`
- `baking.ml`
- `contract_repr.ml`
- `delegate_services.ml`
- `fixed_point_repr.ml`
- `legacy_script_support_repr.ml`
- `level_storage.ml`
- `michelson_v1_gas.ml`
- `misc.ml`
- `operation_repr.ml`
- `raw_context.ml`
- `roll_storage.ml`
- `sapling_storage.ml`
- `script_interpreter.ml`
- `script_ir_annot.ml`
- `script_ir_translator.ml`
- `script_repr.ml`
- `seed_repr.ml`
- `storage_description.ml`
- `storage_functors.ml`
- `tez_repr.ml`

Below is a list of file that have the unsupported features for without positivity checking:

- `misc.ml`
- `storage_description.ml`

##### Optimize OCaml source code

For the functions that were identified as non-termination function. We could modify those functions in the Tezos source code a.k.a `OCaml` code. Below is the list of files in `protocol_alpha/lib_protocol` that have the local recursive definitions:

- `baking.ml`
- `delegate_services.ml`
- `level_storage.ml`
- `misc.ml`
- `roll_storage.ml`
- `script_interpreter.ml`
- `seed_repr.ml`
- `storage_description.ml`
- `storage_functors.ml`

One of the issue while writing the prove for the generated functions, more precisely in the economic protocol, there are functions combine many sub-functions (some functions that have ~100 LOC). The idea is to balance those functions by find parts of the function that have a clear role and define a clear name for that function. Then it will become more compact and clearer. The first ambitious objective is "no more function of more than 100 lines". Below is the observation of files in the `protocol_alpha/lib_protocol` that have functions which long LOC:

- `apply.ml`
- `script_ir_translator.ml`
- `apply_results.ml`
- `bootstrap_storage.ml`
- `contract_service.ml`
- `contract_storage.ml`
- `delegate_storage.ml`
- `helpers_services.ml`
- `main.ml`
- `script_interpreter.ml`

For both modifications on functions, it is also required to test that those functions pass all the current test platforms.


## Conclusion

In this work, I have finished all the proves for the functions that are proposed in the project. Plus some properties that were not mentioned in the project. Redefine functions that did not pass the termination check of `Coq` compiler. Update the proofs to adapt to the newly updated the extracted code due to the optimization of the `coq-of-ocaml` and the update protocol of the Tezos source code. This work also shows that one can use the extracted code from the tool `coq-of-ocaml` to perform proof in `Coq`. This work is a contribution to the project `coq-tezos-of-ocaml`: https://gitlab.com/formal-land/coq-tezos-of-ocaml

As mentioned in the section concepts (https://clarus.github.io/coq-of-ocaml/docs/introduction) of `coq-of-ocaml`, there are several unsupported features of the tool for instance: the non-termination functions that were disabled to bypass the `Coq `compiler. Check the side-effects, GADTs, universe type, integer type, etc.

For the future, in my opinion, one can use the extracted code to prove the correctness of functions for those files mentioned above or any other libraries in the Tezos source code, for instance, the shell library, etc.. But one needs to have in mind that there will be unpredictable issues that one may encounter during writing proofs, there is also require knowledge about the tool `coq-of-ocaml`, dealing with problems that may be related to the tool and/or sometimes the problems in `Coq`, the Tezos source code defines functions that carry multiple computations in one function, that causes the complexity while writing the proof for it, etc. 

Proving the extracted code as it is the way to show that the `Coq` code that extracted from the `coq-of-ocaml` is provable, but it is hard to reason about the specification that the Mustard have, and it is also not able to show the proof in the `Coq` is relevant with the `OCaml` code in a sense of the faithfulness of the data types, semantics, etc. One solution would be to extract the `Coq` code back to `OCaml` and run the test suite.

As mentioned above for the continued work on proving the correctness of functions for the extracted code generated from `coq-of-ocaml`, when all the functions, in this case is the `lib_protocol` have been proved then one can work on the data types to convert them back to their original types, and/or one can take the specification from Mustard and use the extracted code to prove the properties proposed in Mustard together with Axioms, etc.
