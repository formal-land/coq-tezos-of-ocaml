Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Sapling_repr.

Module Memo_size.
  Definition is_valid (size : Sapling_repr.Memo_size.t) : bool :=
    (Z.zero <=Z size) && (size <=Z Sapling_repr.Memo_size.max_uint16_z).

  Lemma parse_unparse size (H : is_valid size = true)
    : Sapling_repr.Memo_size.parse_z
        (Sapling_repr.Memo_size.unparse_to_z size) =
      Pervasives.Ok size.
    unfold Sapling_repr.Memo_size.unparse_to_z, Sapling_repr.Memo_size.parse_z.
    unfold is_valid, of_int, op_andand in *.
    now rewrite H.
  Qed.
End Memo_size.
