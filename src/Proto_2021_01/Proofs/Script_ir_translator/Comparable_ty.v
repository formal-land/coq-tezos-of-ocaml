(** In this file we verify some properties about the comparable types in the
    translator. *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Script_ir_translator.

Require Import TezosOfOCaml.Proto_2021_01.Proofs.Utils.
Require TezosOfOCaml.Proto_2021_01.Proofs.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Proofs.Script_typed_ir.

(** When we unparse to a pair we always have at least two parameters for the
    Micheline construct [Prim] (maybe more, in case of flattening). *)
Fixpoint unparse_comparable_ty_pair_at_least_two comparable_ty :
  match Script_ir_translator.unparse_comparable_ty comparable_ty with
  | Prim _ Michelson_v1_primitives.T_pair ps _ =>
    match ps with
    | [] | [_] => False
    | _ => True
    end
  | _ => True
  end.
  destruct comparable_ty; simpl; trivial.
  case_eq (Script_ir_translator.unparse_comparable_ty comparable_ty2);
    intros; trivial.
  destruct p; trivial.
  destruct a; trivial.
  destruct l0; trivial.
  assert (H_fix := unparse_comparable_ty_pair_at_least_two comparable_ty2).
  now rewrite H in H_fix.
Qed.

(** We always unparse to a Micheline construct [Prim] with a location of
    [(-1)]. *)
Lemma unparse_comparable_ty_location comparable_ty :
  match Script_ir_translator.unparse_comparable_ty comparable_ty with
  | Prim l _ _ _ => l = (-1)
  | _ => False
  end.
  destruct comparable_ty; simpl; try reflexivity.
  destruct (Script_ir_translator.unparse_comparable_ty comparable_ty2);
    try destruct p;
    try destruct a;
    reflexivity.
Qed.

(** We show that unparsing followed by parsing is like the identity. We write
    the proof for the unlimited gas case, which we think does not remove much
    for the generality of the property. *)
Fixpoint parse_unparse_comparable_ty ctxt ty
  : let unlimited_ctxt := Raw_context.with_unlimited_gas ctxt in
    (let node := Script_ir_translator.unparse_comparable_ty ty in
    Script_ir_translator.parse_comparable_ty unlimited_ctxt node) =
    return? (Script_ir_translator.Ex_comparable_ty ty, unlimited_ctxt).
  destruct ty; simpl; try reflexivity.
  { step Script_typed_ir.Pair_key.
    case_eq (Script_ir_translator.unparse_comparable_ty ty2); intros;
      rewrite <- H;
      try destruct p;
      try destruct a;
      try (
        simpl;
        repeat (rewrite parse_unparse_comparable_ty; simpl);
        trivial
      ).
    assert (H_l := unparse_comparable_ty_location ty2).
    rewrite H in H_l.
    rewrite H_l in H.
    rewrite <- H.
    assert (H_two := unparse_comparable_ty_pair_at_least_two ty2).
    rewrite H in H_two.
    do 2 (destruct l0; try easy).
    now repeat (rewrite parse_unparse_comparable_ty; simpl).
  }
  { step Script_typed_ir.Union_key.
    now repeat (rewrite parse_unparse_comparable_ty; simpl).
  }
  { step Script_typed_ir.Option_key.
    now (rewrite parse_unparse_comparable_ty; simpl).
  }
Qed.

(** The equality function over comparable types always success when applied to
    the same term. *)
Fixpoint eq_implies_comparable_ty_eq ctxt ty
  : let unlimited_ctxt := Raw_context.with_unlimited_gas ctxt in
    Script_ir_translator.comparable_ty_eq unlimited_ctxt ty ty =
    return? (Script_ir_translator.Eq, unlimited_ctxt).
  destruct ty; simpl;
    repeat (rewrite eq_implies_comparable_ty_eq; simpl);
    reflexivity.
Qed.

Ltac apply_comparable_ty_eq_implies_eq comparable_ty_eq_implies_eq :=
  match goal with
  | |- context [ Script_ir_translator.comparable_ty_eq ?ctxt ?ty1 ?ty2 ] =>
    let H := fresh "H" in
    destruct (Script_ir_translator.comparable_ty_eq ctxt ty1 ty2)
      as [[[]]|] eqn:H;
      simpl; trivial;
    let H_hyp := fresh "H_hyp" in
    eassert (H_hyp := comparable_ty_eq_implies_eq _ ty1 ty2);
    let H_ty := fresh "H_ty" in
    let H_ctxt := fresh "H_ctxt" in
    simpl in H_hyp; rewrite H in H_hyp;
      destruct H_hyp as [H_ty H_ctxt];
    rewrite H_ctxt
  end.

(** When the equality function over comparable types succeeds then the two
    parameters are equal for the Coq's equality. *)
Fixpoint comparable_ty_eq_implies_eq ctxt ty1 ty2
  : let unlimited_ctxt := Raw_context.with_unlimited_gas ctxt in
    match Script_ir_translator.comparable_ty_eq unlimited_ctxt ty1 ty2 with
    | Pervasives.Ok (_, ctxt') => ty1 = ty2 /\ ctxt' = unlimited_ctxt
    | _ => True
    end.
  destruct ty1; simpl; destruct ty2; simpl; trivial;
    repeat (
      try destruct (Script_ir_translator.serialize_ty_for_error _ _); simpl;
      try destruct p; simpl; trivial
    );
    try (now split);
    repeat apply_comparable_ty_eq_implies_eq comparable_ty_eq_implies_eq;
    now (split; congruence).
Qed.
