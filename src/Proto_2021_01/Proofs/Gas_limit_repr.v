(*
Require Import OCaml.OCaml.

Local Open Scope Z_scope.
Require TezosOfOCaml.Proto_2021_01.Gas_limit_repr.
Require TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Fixed_point_repr.
Require TezosOfOCaml.Proto_2021_01.Proofs.Utils.

Import Environment.

Notation "@0" := Fixed_point_repr.Make.zero.
Infix "@-" := Fixed_point_repr.Make.sub (at level 50, left associativity).
Infix "@<" := Fixed_point_repr.Make.op_lt (at level 70, no associativity).
Infix "@>" := Fixed_point_repr.Make.op_gt (at level 70, no associativity).
Infix "@>=" := Fixed_point_repr.Make.op_gteq (at level 70, no associativity).

Definition is_positive_gas_cost
           (cost: Gas_limit_repr.cost) : Prop :=
  let gas := Gas_limit_repr.cost_to_milligas cost in
  (|Gas_limit_repr.Arith|).(Fixed_point_repr.Full.op_gt)
                             gas
                             (|Gas_limit_repr.Arith|).(Fixed_point_repr.Full.zero) = true.

Lemma is_positive_gas_cost_sum
      (cost1 cost2: Gas_limit_repr.cost) :
  is_positive_gas_cost cost1 ->
  is_positive_gas_cost cost2 ->
  is_positive_gas_cost (Gas_limit_repr.op_plusat cost1 cost2).
Proof.
  intros H1 H2. unfold Gas_limit_repr.op_plusat; simpl.
  unfold is_positive_gas_cost in *; simpl in *. 
  unfold Fixed_point_repr.Make.op_gt in *; simpl in *.
  rewrite Z.sub_0_r in *.  
  rewrite Z.gtb_lt in *.
  apply Z.add_pos_pos; simpl; auto; auto.
Qed.

Lemma raw_consume_sum 
      (block_gas: (|Gas_limit_repr.Arith|).(Fixed_point_repr.Full.t))
      (operation_gas: Gas_limit_repr.t)
      (cost1 cost2: Gas_limit_repr.cost)
      (H1: is_positive_gas_cost cost1)
      (H2: is_positive_gas_cost cost2):
  let raw_consume :=
      (let? '(block_gas, operation_gas) :=
          Gas_limit_repr.raw_consume block_gas operation_gas cost1
       in
       Gas_limit_repr.raw_consume block_gas operation_gas cost2) in 
  let cost := Gas_limit_repr.op_plusat cost1 cost2 in
  let raw_consume' := Gas_limit_repr.raw_consume block_gas operation_gas cost in
  Utils.terminates raw_consume ->  Utils.terminates raw_consume' ->
  raw_consume = raw_consume'.

Proof.
  destruct operation_gas; simpl. easy.
  rewrite is_positive_gas_cost_sum.  
  (** cost1 > 0 *)
  unfold is_positive_gas_cost in H1; simpl in H1.
  rewrite H1.
  (* remaining: l - cost1 < 0: error branch *)  
  destruct (Gas_limit_repr.ConstructorRecords_t.t.Limited.remaining
              l @-
              Gas_limit_repr.cost_to_milligas cost1 @< @0);
    simpl; try Utils.impossible_branch.
  (* block_remaining: block_gas - cost1 < 0: error branch *)
  destruct (block_gas @- Gas_limit_repr.cost_to_milligas cost1 @< @0);
    simpl; try Utils.impossible_branch.
  (** cost2 > 0 *)
  unfold is_positive_gas_cost in H2; simpl in H2. rewrite H2.
  (* l - cost1 - cost2 < 0: error branch *)
  destruct (Gas_limit_repr.ConstructorRecords_t.t.Limited.remaining
              l @- Gas_limit_repr.cost_to_milligas cost1 @-
              Gas_limit_repr.cost_to_milligas cost2 @< @0);
    simpl; try Utils.impossible_branch. 
  (* block_remaining: block_gas - cost1 - cost2 < 0: error branch *)
  unfold Gas_limit_repr.op_plusat.
  unfold Gas_limit_repr.cost_to_milligas; simpl.
  unfold Fixed_point_repr.Make.unsafe_fp.
  destruct (block_gas @- cost1 @- cost2 @< @0); simpl; try Utils.impossible_branch.
  (* l - (cost1 + cost2) < 0: error branch *)
  destruct (Gas_limit_repr.ConstructorRecords_t.t.Limited.remaining l @- Z.add cost1 cost2 @< @0);
    simpl; try Utils.impossible_branch.
  (* block_gas - (cost1 + cost2) < 0: error branch *)
  destruct (block_gas @- Z.add cost1 cost2 @< @0); simpl; try Utils.impossible_branch.
  do 2 rewrite Z.sub_add_distr. auto.
  apply H1.
  apply H2.
Qed.

Lemma raw_consume_sum_free
    (block_gas: (|Gas_limit_repr.Arith|).(Fixed_point_repr.Full.t))
      (operation_gas: Gas_limit_repr.t)
      (cost1: Gas_limit_repr.cost)
      (H1: is_positive_gas_cost cost1) : 
  let raw_consume :=
      (let? '(block_gas, operation_gas) :=
          Gas_limit_repr.raw_consume block_gas operation_gas cost1
       in
       Gas_limit_repr.raw_consume block_gas operation_gas Gas_limit_repr.free) in
  let cost := Gas_limit_repr.op_plusat cost1 Z.zero in
  let raw_consume' := Gas_limit_repr.raw_consume block_gas operation_gas cost in
  Utils.terminates raw_consume -> Utils.terminates raw_consume' ->
  raw_consume = raw_consume'.

Proof.
  destruct operation_gas; simpl; auto.
  (* cost1 > 0 *)
  unfold is_positive_gas_cost in H1; simpl in H1.
  rewrite H1.
  (* remaining: l - cost1 < 0: error branch *)
  destruct (Gas_limit_repr.ConstructorRecords_t.t.Limited.remaining
              l @-
              Gas_limit_repr.cost_to_milligas cost1 @< @0);
    simpl; try Utils.impossible_branch.
  (* block_remaining: block_gas - cost1 < 0: error branch *)
  destruct (block_gas @- Gas_limit_repr.cost_to_milligas cost1 @< @0);
    simpl; try Utils.impossible_branch.
  (* cost1 + 0 > 0 *)
  unfold Gas_limit_repr.op_plusat.
  rewrite Z.add_0_r.
  rewrite H1.
  (* l - cost1 < 0: error branch *)
  destruct (Gas_limit_repr.ConstructorRecords_t.t.Limited.remaining
              l @- Gas_limit_repr.cost_to_milligas cost1 @<
              @0); simpl; try Utils.impossible_branch.
  unfold Gas_limit_repr.cost_to_milligas; simpl;
  unfold Fixed_point_repr.Make.unsafe_fp. 
  destruct ( block_gas @- cost1 @< @0); simpl; try Utils.impossible_branch.
Qed.

Definition is_positive_block_remaining `{Fixed_point_repr.Make.FArgs}
           (block_gas: (|Gas_limit_repr.Arith|).(Fixed_point_repr.Full.t))
           (gas: Gas_limit_repr.cost) : Prop :=
  let block_remaining := block_gas @- gas in
  block_remaining @>= @0 = true.

Definition is_positive_operation_remaining `{Fixed_point_repr.Make.FArgs}
           (operation_gas: Gas_limit_repr.t)
           (gas: Gas_limit_repr.cost) :=
    match operation_gas with
    | Gas_limit_repr.Unaccounted => False   
    | Gas_limit_repr.Limited {| Gas_limit_repr.t.Limited.remaining := remaining |} =>
      let op_remaining := remaining @- gas in
      op_remaining @>= @0 = true
    end.

Definition wf_raw_consume `{Fixed_point_repr.Make.FArgs}
           (block_gas: (|Gas_limit_repr.Arith|).(Fixed_point_repr.Full.t))
           (operation_gas: Gas_limit_repr.t)
           (cost: Gas_limit_repr.cost) :=
  let gas := Gas_limit_repr.cost_to_milligas cost in
  is_positive_gas_cost cost /\
  is_positive_block_remaining block_gas gas /\
  is_positive_operation_remaining operation_gas gas.
*)
