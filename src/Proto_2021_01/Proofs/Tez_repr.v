Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Import Environment.

Require TezosOfOCaml.Proto_2021_01.Tez_repr.


(*** Tez_repr.op_plusquestion *)

Lemma op_plusquestion_false (t1 t2 : Tez_repr.t) :
  let total := t1 +i64 t2 in
  Tez_repr.op_lt total t1 = false ->
  Tez_repr.op_plusquestion t1 t2 = (return? total).

Proof.
  unfold Tez_repr.op_plusquestion.
  now destruct (Tez_repr.op_lt (t1 +i64 t2) t1).
Qed.
