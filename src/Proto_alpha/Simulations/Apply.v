Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Apply.

Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_family.
Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_typed_ir.

(** Simulation of the type [execution_arg]. *)
Inductive dep_execution_arg (loc : Set) : Set :=
| Dep_typed_arg {a} :
  loc -> With_family.ty a -> With_family.ty_to_dep_Set a ->
  dep_execution_arg loc
| Dep_untyped_arg :
  Script_repr.expr ->
  dep_execution_arg loc.
Arguments Dep_typed_arg {_ _}.
Arguments Dep_untyped_arg {_}.

(** Conversion back to the type [execution_arg]. *)
Definition to_execution_arg {loc : Set} (x : dep_execution_arg loc) :
  Apply.execution_arg loc :=
  match x with
  | Dep_typed_arg loc ty v =>
    Apply.Typed_arg loc (With_family.to_ty ty) (With_family.to_value v)
  | Dep_untyped_arg script => Apply.Untyped_arg script
  end.
