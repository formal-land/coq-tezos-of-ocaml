Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_withdraw_repr.

Require TezosOfOCaml.Environment.V8.Proofs.Data_encoding.
Require TezosOfOCaml.Environment.V8.Proofs.Signature.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_hash_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_l2_qty.

Module order.
  Module Valid.
    Import TezosOfOCaml.Proto_alpha.Tx_rollup_withdraw_repr.order.

    (** Validity predicate for the [Tx_rollup_withdraw_repr.t] type *)
    Record t (x : Tx_rollup_withdraw_repr.t) : Prop := {
      amount : Tx_rollup_l2_qty.Valid.t x.(amount);
    }.
  End Valid.
End order.

(** The encoding [encoding] is valid *)
Lemma encoding_is_valid :
  Data_encoding.Valid.t order.Valid.t Tx_rollup_withdraw_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.