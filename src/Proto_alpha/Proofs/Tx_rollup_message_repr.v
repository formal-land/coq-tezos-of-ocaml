Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_message_repr.

Require TezosOfOCaml.Environment.V8.Proofs.Data_encoding.
Require TezosOfOCaml.Environment.V8.Proofs.Blake2B.
Require TezosOfOCaml.Environment.V8.Proofs.Int64.
Require TezosOfOCaml.Environment.V8.Proofs.S.
Require TezosOfOCaml.Environment.V8.Proofs.Signature.
Require TezosOfOCaml.Environment.V8.Proofs.String.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_expr_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_l2_address.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_l2_qty.

Module deposit.
  Module Valid.
    Import Proto_alpha.Tx_rollup_message_repr.deposit.

    (** Validity predicate for the type [deposit]. *)
    Record t (p : Tx_rollup_message_repr.deposit) : Prop := {
      destination : Indexable.Value.Valid.t p.(destination);
      amount : Tx_rollup_l2_qty.Valid.t p.(amount);
    }.
  End Valid.
End deposit.

(** The encoding [deposit_encoding] is valid. *)
Lemma deposit_encoding_is_valid : Data_encoding.Valid.t deposit.Valid.t
  Tx_rollup_message_repr.deposit_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve deposit_encoding_is_valid : Data_encoding_db.

Module Valid.
  (** Validity predicate for the type [t]. *)
  Definition t (x : Tx_rollup_message_repr.t) : Prop :=
    match x with
    | Tx_rollup_message_repr.Batch s => String.Int_length.t s
    | Tx_rollup_message_repr.Deposit d => deposit.Valid.t d
    end.
End Valid.

(** The encoding [encoding] is valid. *)
Lemma encoding_is_valid : Data_encoding.Valid.t Valid.t
  Tx_rollup_message_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

(** The function [size_value] is valid. *)
Lemma size_value_is_valid message :
  Valid.t message ->
  Pervasives.Int.Valid.t (Tx_rollup_message_repr.size_value message).
Proof.
  intros.
  destruct message; simpl in *.
  { pose proof (String.length_is_valid s).
    lia.
  }
  { lia. }
Qed.
