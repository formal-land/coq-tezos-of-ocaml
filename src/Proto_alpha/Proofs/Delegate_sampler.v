Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Delegate_sampler.

Require Import TezosOfOCaml.Proto_alpha.Proofs.Error.
Require TezosOfOCaml.Proto_alpha.Proofs.Raw_context.
Require TezosOfOCaml.Proto_alpha.Proofs.Sampler.
Require TezosOfOCaml.Proto_alpha.Proofs.Seed_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Slot_repr.
Require TezosOfOCaml.Proto_alpha.Simulations.Raw_context.

Module Delegate_sampler_state.
  Module Cache.
    Import Cache_repr.INTERFACE.
    (** [Delegate_sampler.Delegate_sampler_state.Cache.(find)] is valid *)
    Axiom find_is_valid
      : forall (ctxt : Proto_alpha.Raw_context.t)
          (identifier : string),
        letP? _ := Delegate_sampler.Delegate_sampler_state.Cache
                     .(Cache_repr.INTERFACE.find)
                        ctxt identifier in True.
  End Cache.

  (** [Delegate_sampler.Delegate_sampler_state.get] is valid *)
  Lemma get_is_valid
    (ctxt : Proto_alpha.Raw_context.t)
    (cycle : Cycle_repr.t)
    : letP? _ := Delegate_sampler.Delegate_sampler_state.get ctxt cycle in
      True.
  Proof. Admitted.
End Delegate_sampler_state.

Module Random.
  (** [Delegate_sampler.Range.sampler_for_cycle] is_valid *)
  Lemma sampler_for_cycle_is_valid
    (ctxt : Proto_alpha.Raw_context.t)
    (cycle : Cycle_repr.t)
    : Raw_context.Valid.t ctxt ->
      Cycle_repr.Valid.t cycle ->
      letP? '(ctxt, seed, _) :=
        Delegate_sampler.Random.sampler_for_cycle ctxt cycle in
      Raw_context.Valid.t ctxt /\ Seed_repr.Seed.Valid.t seed.
  Proof. Admitted.

  (** [Delegate_sampler.Range.owner] is valid *)
  Lemma owner_is_valid
    (ctxt : Proto_alpha.Raw_context.t)
    (level : Level_repr.t)
    (slot : Slot_repr.t)
    : Raw_context.Valid.t ctxt ->
      Level_repr.Valid.t level ->
      Slot_repr.Valid.t slot ->
      letP? '(ctxt, _) := Delegate_sampler.Random.owner ctxt level slot in
      Raw_context.Valid.t ctxt.
  Proof.
    intros Hv_ctxt Hv_lvl Hv_slot.
    unfold Delegate_sampler.Random.owner.
    eapply split_letP; [
      apply Random.sampler_for_cycle_is_valid; trivial; apply Hv_lvl
    |].
    intros [[] ?] [].
    apply split_letP_triv; [apply Sampler.sample_is_valid|];
      trivial.
  Qed.
End Random.

(** [Delegate_sampler.slot_owner] has no internal errors *)
Lemma slot_owner_is_valid
  (ctxt : Proto_alpha.Raw_context.t)
  (level : Level_repr.t)
  (slot : Slot_repr.t)
  : Raw_context.Valid.t ctxt ->
    Level_repr.Valid.t level ->
    Slot_repr.Valid.t slot ->
    letP? '(ctxt, _) := Delegate_sampler.slot_owner ctxt level slot in
    Raw_context.Valid.t ctxt.
Proof.
  intros.
  unfold Delegate_sampler.slot_owner.
  apply Random.owner_is_valid; trivial.
Qed.

(** [Delegate_sampler.baking_rights_owner] is valid *)
Lemma baking_rights_owner_is_valid
  (ctxt : Proto_alpha.Raw_context.t)
  (level : Level_repr.t)
  (round : Round_repr.t)
  : Raw_context.Valid.t ctxt ->
    Level_repr.Valid.t level ->
    Round_repr.Valid.t round ->
    letP? '(ctxt, slot, _) :=
      Delegate_sampler.baking_rights_owner ctxt level round in
    Raw_context.Valid.t ctxt /\ Slot_repr.Valid.t slot.
Proof. Admitted.
