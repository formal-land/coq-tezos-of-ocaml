Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Environment.V8.Proofs.Block_hash.
Require TezosOfOCaml.Environment.V8.Proofs.Time.

Require TezosOfOCaml.Environment.V8.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Sc_rollup_inbox_repr.

(** The encoding [internal_inbox_message_encoding] is valid. *)
Lemma internal_inbox_message_encoding_is_valid :
  Data_encoding.Valid.t
    (fun _ => True) Sc_rollup_inbox_message_repr.internal_inbox_message_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.

#[global] Hint Resolve internal_inbox_message_encoding_is_valid : Data_encoding_db.

(** The encoding [Sc_rollup_inbox_message_repr.encoding] is valid. *)
Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Sc_rollup_inbox_message_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Admitted.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

