Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Ticket_hash_repr.

Require TezosOfOCaml.Environment.V8.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_expr_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Storage_description.

(** [encoding] function is valid *)
Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Ticket_hash_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

(** [compare] function is valid *)
Lemma compare_is_valid :
  Compare.Valid.t (fun _ => True) id Ticket_hash_repr.compare.
Proof.
  apply Blake2B.Make_is_valid.
Qed.
#[global] Hint Resolve compare_is_valid : Compare_db.

(** The index [Index] is valid. *)
Lemma Index_is_valid :
  Storage_description.INDEX.Valid.t (fun _ => True) Ticket_hash_repr.Index.
Proof.
  constructor; simpl; try apply Blake2B.Make_is_valid.
  apply Path_encoding.Make_hex_is_valid.
  constructor; apply Blake2B.Make_is_valid.
Qed.
