Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Sc_rollup_outbox_message_repr.

Require TezosOfOCaml.Environment.V8.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Entrypoint_repr.

Module Transaction.

  (** Validity predicate for transaction. *)
  Module Valid.
    Import Sc_rollup_outbox_message_repr.transaction.

    Record t (r : record)  : Prop := {
      entrypoint : Entrypoint_repr.Valid.t r.(entrypoint);
    }.
  End Valid.
End Transaction.

(** [transaction_encoding] is valid *)
Lemma transaction_encoding_is_valid :
  Data_encoding.Valid.t Transaction.Valid.t Sc_rollup_outbox_message_repr.transaction_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve transaction_encoding_is_valid : Data_encoding_db.

Module Valid.
  Import Sc_rollup_outbox_message_repr.
  Definition t (r : t) : Prop :=
    let 'Atomic_transaction_batch r := r in
    Forall Transaction.Valid.t r.(t.Atomic_transaction_batch.transactions).
End Valid.

(** [encoding] is valid *)
Lemma encoding_is_valid :
  Data_encoding.Valid.t Valid.t Sc_rollup_outbox_message_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
