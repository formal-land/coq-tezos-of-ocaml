Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Contract_hash.

Require TezosOfOCaml.Environment.V8.Proofs.Blake2B.
Require TezosOfOCaml.Proto_alpha.Proofs.Path_encoding.

Lemma H_is_valid : S.HASH.Valid.t (fun _ => True) Contract_hash.H.
  apply Blake2B.Make_is_valid.
Qed.

Lemma compare_is_valid :
  Compare.Valid.t (fun _ => True) id Contract_hash.compare.
Proof.
  apply H_is_valid.
Qed.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Contract_hash.encoding.
  apply H_is_valid.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Lemma Path_encoding_Make_hex_include_is_valid
  : Path_encoding.S.Valid.t Contract_hash.Path_encoding_Make_hex_include.
  apply Path_encoding.Make_hex_is_valid.
  constructor; apply H_is_valid.
Qed.
