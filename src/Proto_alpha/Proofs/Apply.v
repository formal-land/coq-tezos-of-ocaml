Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Apply.

Require TezosOfOCaml.Proto_alpha.Proofs.Alpha_services.
Require TezosOfOCaml.Proto_alpha.Proofs.Cache_memory_helpers.
Require TezosOfOCaml.Proto_alpha.Proofs.Cache_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Constants_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_manager_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Dal_apply.
Require TezosOfOCaml.Proto_alpha.Proofs.Delegate_consensus_key.
Require TezosOfOCaml.Proto_alpha.Proofs.Delegate_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Fees_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Global_constants_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Michelson_v1_gas.
Require TezosOfOCaml.Proto_alpha.Proofs.Raw_context.
Require TezosOfOCaml.Proto_alpha.Proofs.Receipt_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Sc_rollup_inbox_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_cache.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_interpreter.
Require TezosOfOCaml.Proto_alpha.Proofs.Sc_rollup_operations.
Require TezosOfOCaml.Proto_alpha.Proofs.Sc_rollup_refutation_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Sc_rollup_stake_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Sc_rollup_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_ir_translator.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_ir_unparser.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir_size.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir_size_costs.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_accounting.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_balance_key.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_scanner.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_token.
Require TezosOfOCaml.Proto_alpha.Proofs.Token.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_commitment_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_gas.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_hash_builder.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_inbox_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_l2_verifier.
(* Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_parameters. *)
(* Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_ticket. *)
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_reveal_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_reveal_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_state_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_state_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_withdraw_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Voting_period_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Zk_rollup_apply.
(* Require TezosOfOCaml.Proto_alpha.Simulations.Apply. *)
(* Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_family. *)
(* Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_typed_ir. *)

Import Error.Tactics_letP.

(** The function [assert_tx_rollup_feature_enabled] is valid. *)
Lemma assert_tx_rollup_feature_enabled_is_valid ctxt :
  Raw_context.Valid.t ctxt ->
  letP? _ := Apply.assert_tx_rollup_feature_enabled ctxt in
  True.
Proof.
  intros H.
  Raw_context.Valid.destruct_rewrite H.
  unfold Apply.assert_tx_rollup_feature_enabled.
  eapply Error.split_letP; [
    apply Raw_level_repr.of_int32_is_valid;
    apply H_sim_ctxt |
    intros
  ].
  unfold Error_monad.error_when, Error_monad.error_unless.
  repeat (step; simpl); easy.
Qed.

(** The function [assert_sc_rollup_feature_enabled] is valid. *)
Lemma assert_sc_rollup_feature_enabled_is_valid ctxt :
  Raw_context.Valid.t ctxt ->
  letP? _ := Apply.assert_sc_rollup_feature_enabled ctxt in
  True.
Proof.
  unfold Apply.assert_sc_rollup_feature_enabled.
  unfold Error_monad.error_unless.
  now step.
Qed.

(** The function [update_script_storage_and_ticket_balances] is valid. *)
Lemma update_script_storage_and_ticket_balances_is_valid
  ctxt self storage_value lazy_storage_diff ticket_diffs operations :
  Raw_context.Valid.t ctxt ->
  Ticket_token_map.Valid.t (fun _ => True) ticket_diffs ->
  List.Forall Script_typed_ir.packed_internal_operation.Valid.t operations ->
  letP? '(_, ctxt) :=
    Apply.update_script_storage_and_ticket_balances
      ctxt self storage_value lazy_storage_diff ticket_diffs
      (List.map With_family.to_packed_internal_operation operations) in
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_ticket_diffs H_operations.
  unfold Apply.update_script_storage_and_ticket_balances.
  eapply Error.split_letP. {
    now apply Contract_storage.update_script_storage_is_valid.
  }
  clear ctxt H_ctxt; intros ctxt H_ctxt.
  now apply Ticket_accounting.update_ticket_balances_is_valid.
Qed.

(** The function [apply_delegation] is valid. *)
Lemma apply_delegation_is_valid {A : Set}
  ctxt source delegate before_operation :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _, _) :=
    Apply.apply_delegation (A := A)
      ctxt source delegate before_operation in
  Raw_context.Valid.t ctxt.
Proof.
  intros H.
  Raw_context.Valid.destruct_rewrite H.
  unfold Apply.apply_delegation.
  eapply Error.split_letP; [
    now apply Delegate_storage.Contract.set_is_valid |
    easy
  ].
Qed.

(** The function [apply_transaction_to_implicit] is valid. *)
Lemma apply_transaction_to_implicit_is_valid {A : Set}
  ctxt source amount pkh before_operation :
  Raw_context.Valid.t ctxt ->
  Tez_repr.Valid.t amount ->
  letP? '(ctxt, _, _) :=
    Apply.apply_transaction_to_implicit (A := A)
      ctxt source amount pkh before_operation in
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_amount.
  Raw_context.Valid.destruct_rewrite H_ctxt.
  unfold Apply.apply_transaction_to_implicit.
  unfold Error_monad.error_when.
  destruct Tez_repr.op_eq; simpl; [easy|].
  eapply Error.split_letP; [
    now apply Token.transfer_is_valid|].
  intros [ctxt' ?] H_ctxt'; simpl.
  easy.
Qed.

(** The function [apply_transaction_to_smart_contract] is valid. *)
Lemma apply_transaction_to_smart_contract_is_valid
  ctxt source contract_hash amount entrypoint before_operation payer chain_id
  internal parameter :
  Raw_context.Valid.t ctxt ->
  Tez_repr.Valid.t amount ->
  Raw_context.Valid.t before_operation ->
  letP? '(ctxt, _, _) :=
    Apply.apply_transaction_to_smart_contract
      ctxt
      source contract_hash amount entrypoint before_operation payer chain_id
      internal
      (Apply.to_execution_arg parameter) in
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_amount H_before_operation.
  unfold Apply.apply_transaction_to_smart_contract.
  eapply Error.split_letP; [
    now apply Token.transfer_is_valid|].
  clear ctxt H_ctxt.
  intros [ctxt ?] H_ctxt.
  eapply Error.split_letP; [
    now apply Script_cache.find_is_valid|].
  clear ctxt H_ctxt.
  intros [[ctxt cache_key] script] H_ctxt.
  destruct script as [[script script_ir]|]; simpl; [|easy].
  eapply Error.split_letP; [
    now apply Contract_storage.get_balance_is_valid|].
  intros balance H_balance.
  apply Error.split_letP
    with (P := fun '(_, ctxt) => Raw_context.Valid.t ctxt). {
    destruct parameter; simpl.
    { rewrite_cast_exists.
      apply Script_interpreter.execute_with_typed_parameter_is_valid.
      assumption.
    }
    { apply Script_interpreter.execute_is_valid. assumption.
    }
  }
  clear ctxt H_ctxt.
  intros [execution_result ctxt] H_ctxt.
  destruct execution_result.
  assert (exists dep_operations,
    List.map With_family.to_packed_internal_operation dep_operations =
      operations /\
    List.Forall Script_typed_ir.packed_internal_operation.Valid.t
      dep_operations
  ) as H_operations.
  {
    (* Locate  With_family.to_internal_operation. *)
    admit.
  }
  destruct H_operations
    as [dep_operations [H_dep_operations_eq H_dep_operations]].
  rewrite <- H_dep_operations_eq.
  eapply Error.split_letP. {
    apply update_script_storage_and_ticket_balances_is_valid; try assumption.
    admit.
  }
  clear ctxt H_ctxt.
  intros [? ctxt] H_ctxt.
  eapply Error.split_letP; [
    now apply Ticket_storage.adjust_storage_space_is_valid|].
  clear ctxt H_ctxt.
  intros [? ctxt] H_ctxt.
  eapply Error.split_letP; [
    now apply Fees_storage.record_paid_storage_space_is_valid|].
  clear ctxt H_ctxt.
  intros [[ctxt ?] ?] H_ctxt.
  eapply Error.split_letP; [
    now apply Contract_storage.originated_from_current_nonce_is_valid|].
  intros.
  eapply Error.split_letP; [
    now apply Script_cache.update_is_valid|].
  clear ctxt H_ctxt.
  intros ctxt H_ctxt.
  simpl.
  trivial.
Admitted.

(** The function [apply_transaction_to_tx_rollup] is valid. *)
Lemma apply_transaction_to_tx_rollup_is_valid {a : Ty.t} {B : Set} :
  let t : Ty.t := Ty.Pair (Ty.Ticket a) Ty.Tx_rollup_l2_address in
  forall ctxt
    (parameters_ty : With_family.ty t)
    (parameters : With_family.ty_to_dep_Set t)
    payer dst_rollup since,
  Raw_context.Valid.t ctxt ->
  Script_typed_ir.With_family.Valid.ty parameters_ty ->
  Script_typed_ir.With_family.Valid.value parameters ->
  Raw_context.Valid.t since ->
  letP? '(ctxt, _, _) :=
    Apply.apply_transaction_to_tx_rollup (B := B)
      ctxt
      (With_family.to_ty parameters_ty) (With_family.to_value parameters)
      payer dst_rollup since in
  Raw_context.Valid.t ctxt.
Proof.
  intros * H_ctxt H_parameters_ty H_parameters H_since.
  unfold Apply.apply_transaction_to_tx_rollup.
  eapply Error.split_letP; [
    now apply assert_tx_rollup_feature_enabled_is_valid|].
  intros _ _.
  match goal with
  | |- context[
      Tx_rollup_parameters.get_deposit_parameters
        (With_family.to_ty ?ty) (With_family.to_value ?value)
    ] =>
    pose proof (
      Tx_rollup_parameters.get_deposit_parameters_is_valid
        ty value H_parameters_ty H_parameters
    ) as H_deposit_parameters
  end.
  destruct Tx_rollup_parameters.get_deposit_parameters.
  destruct H_deposit_parameters as [H_ex_ticket]; simpl in H_ex_ticket.
  eapply Error.split_letP; [
    now apply Ticket_scanner.ex_ticket_size_is_valid|].
  clear ctxt H_ctxt.
  intros [ctxt ticket_size] [H_ctxt H_ticket_size].
  unfold Error_monad.fail_when.
  destruct (_ >i _); simpl; [easy|].
  pose proof (
    Ticket_token.token_and_amount_of_ex_ticket_is_valid ex_ticket H_ex_ticket
  ) as H_token_and_amount.
  destruct Ticket_token.token_and_amount_of_ex_ticket
    as [ex_token ticket_amount].
  destruct H_token_and_amount as [H_ex_token H_ticket_amount].
  eapply Error.split_letP; [
    now apply Ticket_balance_key.of_ex_token_is_valid|].
  clear ctxt H_ctxt.
  intros [ticket_hash ctxt] H_ctxt.
  match goal with
  | |- context[Script_int.to_int64 ?x] =>
    pose proof (Script_int.to_int64_is_valid x) as H_x
  end.
  destruct Script_int.to_int64; simpl; [|easy].
  eapply Error.split_letP; [
    destruct Tx_rollup_l2_qty.of_int64 eqn:H_of_int64; simpl; [|easy];
    apply (Tx_rollup_l2_qty.of_int64_is_valid _ _ H_x H_of_int64) |
  ].
  clear ticket_amount H_ticket_amount.
  intros ticket_amount H_ticket_amount.
  destruct Tx_rollup_l2_qty.op_lteq; simpl; [easy|].
  eapply Error.split_letP; [
    now apply Tx_rollup_state_storage.get_is_valid|].
  clear ctxt H_ctxt.
  intros [ctxt state_value] [H_ctxt H_state_value].
  eapply Error.split_letP; [
    apply Tx_rollup_state_repr.burn_cost_is_valid; simpl; lia|].
  intros cost H_cost.
  eapply Error.split_letP; [
    now apply Token.transfer_is_valid|].
  clear ctxt H_ctxt.
  intros [ctxt balance_updates] H_ctxt.
  eapply Error.split_letP; [
    now apply Tx_rollup_inbox_storage.append_message_is_valid|].
  clear ctxt state_value H_ctxt H_state_value.
  intros [[ctxt state_value] paid_storage_size_diff] [H_ctxt H_state_value].
  eapply Error.split_letP; [
    now apply Tx_rollup_state_storage.update_is_valid|].
  trivial.
Qed.

(** The function [apply_origination] is valid. *)
Lemma apply_origination_is_valid {a : Ty.t} {B : Set}
  ctxt storage_type storage_value unparsed_code contract_hash delegate source
  credit before_operation :
  Raw_context.Valid.t ctxt ->
  Script_typed_ir.With_family.Valid.ty storage_type ->
  Script_typed_ir.With_family.Valid.value storage_value ->
  Tez_repr.Valid.t credit ->
  letP? '(ctxt, _, _) :=
    Apply.apply_origination (A := Ty.to_Set a) (B := B)
      ctxt
      (With_family.to_ty (a:=a) storage_type) (With_family.to_value storage_value)
      unparsed_code contract_hash delegate source
      credit
      before_operation in
  Raw_context.Valid.t ctxt.
Proof.
  intros. unfold Apply.apply_origination.
  eapply Error.split_letP;
  [ apply Script_ir_translator.collect_lazy_storage_is_valid ; assumption |].
  clear H ctxt. intros [[storage lazy_storage] ctxt] H.
  eapply Error.split_letP ;
  [ apply Script_ir_translator.extract_lazy_storage_diff_is_valid ;
    assumption |].
  clear H ctxt storage lazy_storage. intros [[storage lazy_storage] ctxt]
    [H [dep_storage_value [ H_dep_storage_value_ex H_dep_storage_value_val] ]].
  rewrite H_dep_storage_value_ex. eapply Error.split_letP ;
    [ eapply Script_ir_translator.unparse_data_is_valid ; assumption|].
  clear ctxt H.
  intros [code ctxt] H. eapply Error.split_letP ; [
    apply Script_ir_translator.unparse_code_is_valid ; assumption|].
  clear ctxt H. intros [code0 ctxt] H. eapply Error.split_letP ; [
    apply Contract_storage.raw_originate_is_valid ; assumption |].
  clear ctxt H. intros ctxt H.
  eapply Error.split_letP;  [
    destruct delegate ; [ apply Delegate_storage.Contract.init_value_is_valid ;
    assumption| assumption ]|].
  clear ctxt H.
  intros ctxt H.
  esplit_letP; [
    apply Token.transfer_is_valid ; assumption |].
  iesplit_letP; [
    apply Fees_storage.record_paid_storage_space_is_valid; easy |].
  i_des_pairs. assumption.
Qed.

(** The function [apply_internal_operation_contents] is valid. *)
(* It looks to me like problem is in the definition of
   [to_value/to_kdescr/..] but I'm not sure how to explain that
   it worked before and stopped to work now after the changes
   in the defintion of [ty_to_dep_Set] for [operation].
   So, I had to use bypass_check(guard) here. *)
#[bypass_check(guard)]
Lemma apply_internal_operation_contents_is_valid
  ctxt_before_op payer source chain_id operation :
  Raw_context.Valid.t ctxt_before_op ->
  Script_typed_ir.internal_operation_contents.Valid.t operation ->
  letP? '(ctxt, _, _) :=
    Apply.apply_internal_operation_contents
      ctxt_before_op payer source chain_id
        (With_family.to_internal_operation_contents operation) in
  Raw_context.Valid.t ctxt.
Proof.
  intros H_before_op H_op.
  unfold Apply.apply_internal_operation_contents.
  eapply Error.split_letP ;
  [ apply Contract_storage.must_exist_is_valid ; assumption|].
  intros _ _. eapply Error.split_letP;
  [ apply Alpha_context.Gas.consume_is_valid ; [assumption | easy]|].
    intros ctxt H. destruct operation eqn:op_eq ; try inversion H_op.
  { simpl. eapply Error.split_letP ;
    [ apply Apply.apply_transaction_to_implicit_is_valid; assumption|].
    clear ctxt H. i_des_pairs. assumption. }
  { eapply Error.split_letP with (P := fun '(ctxt,_,_) =>
    Raw_context.Valid.t ctxt ).
    let Hx := fresh "Hx" in
    match goal with
    | |- context [ Apply.Typed_arg _ _ (?to_value_ ?arg_ _) ] =>
        assert(Hx : to_value_ arg_ t3 = With_family.to_value t3)
        by reflexivity;
        rewrite Hx; clear Hx
    end.
    destruct H_op as [Hamount [[loc Hloc]] ] ; rewrite Hloc .
    apply Apply.apply_transaction_to_smart_contract_is_valid ; assumption.
    i_des_pairs; assumption.
  }
  { apply apply_transaction_to_tx_rollup_is_valid ; assumption. }
  { esplit_letP ;
    [apply assert_sc_rollup_feature_enabled_is_valid ; assumption|].
    intros _ _. eapply Error.split_letP with (P:= fun x => True);
    [destruct source;  constructor|]. (* generated name!*)
    intros contract _. esplit_letP ; [
    apply Sc_rollup_inbox_storage.add_internal_message_is_valid ; assumption|].
    i_des_pairs.
    assumption. }
  { assumption. }
  { destruct H_op as [? [? ?]]. (** tactic? *)
    eapply Error.split_letP ; [
      apply apply_origination_is_valid ; assumption |].
    i_des_pairs. assumption. }
  { eapply Error.split_letP ; [ apply apply_delegation_is_valid ;
    assumption |]. i_des_pairs. assumption. }
Qed.

(** The function [apply_manager_operation] is valid. *)
Lemma apply_manager_operation_is_valid
  ctxt_before_op source chain_id operation :
  Raw_context.Valid.t ctxt_before_op ->
  Operation_repr.manager_operation.Valid.t operation ->
  letP? '(ctxt, _, pack_intern_op_list) :=
    Apply.apply_manager_operation
      ctxt_before_op source chain_id operation in
  Raw_context.Valid.t ctxt.
  (** @TODO include this post-condition *)
  (* /\ Forall Script_typed_ir.packed_internal_operation.Valid.t pack_intern_op_list. *)
Proof.
(** @TODO make the proof more robust (remove calls to generated names)
  *)
  intros H_bef_op H_op.
  unfold Apply.apply_manager_operation.
  esplit_letP ;
    [apply Contract_storage.must_exist_is_valid ; assumption |].
  intros _ _.
  esplit_letP ;
    [apply Alpha_context.Gas.consume_is_valid ;
      [ assumption | apply Michelson_v1_gas.Cost_of.manager_operation_is_valid ]
    |].
  intros ctxt H_ctxt.
  destruct operation eqn:op_eq ; try inversion H_op.
  (** Proto_alpha: 28 cases *)
  { grep Operation_repr.Reveal.
    esplit_letP ;
    [ apply Contract_storage.must_be_allocated_is_valid ; assumption |].
    intros _ _.
    iesplit_letP ; [
      apply Contract_manager_storage.reveal_manager_key_is_valid ;
      assumption |].
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Transaction.
    destruct t eqn:H_t, destination eqn:H_dest.
    { esplit_letP ; [
      apply Alpha_context.Script.force_decode_in_context_is_valid ;
      assumption|].
      intros [expr ?] ?.
      eapply Error.split_letP with (P := fun x => True).
      destruct (root_value expr) eqn:eq_root_val.
      { easy. } { easy. } { easy. }  (** 3 times [easy] to speed up *)
      { destruct p0 eqn:eq_p0 ; try easy ; destruct l ;
        (** generated named! *)   (* generated name!*)
         [ constructor | try easy ]. }
      { easy. }
      { intros _ _.
        apply Error.split_letP with (fun x => True) ; [
          destruct (Alpha_context.Entrypoint.is_default entrypoint0) ;
          constructor|].  (** generated name*)
        intros _ _.
        esplit_letP ; [
          apply apply_transaction_to_implicit_is_valid ; assumption|].
        i_des_pairs. assumption.
      }
    }
    { esplit_letP ; [
        apply Alpha_context.Script.force_decode_in_context_is_valid ;
        assumption|].
      intros [expr ?] ?.
      esplit_letP.
      { replace (Apply.Untyped_arg expr) with
        (@Apply.to_execution_arg Alpha_context.Script.location
          (Apply.Dep_untyped_arg expr)).
        apply apply_transaction_to_smart_contract_is_valid ; try assumption.
        reflexivity. }
      i_des_pairs. assumption.
    }
  }
  { grep @Operation_repr.Origination.
    esplit_letP ;
    [apply Contract_storage.fresh_contract_from_current_nonce_is_valid
    ; assumption |].
    iesplit_letP ;
      [ apply Alpha_context.Script.force_decode_in_context_is_valid ;
        assumption|].
    iesplit_letP ; [
      apply Alpha_context.Script.force_decode_in_context_is_valid ;
      assumption|].
    iesplit_letP ; [
      apply Script_ir_translator.parse_script_is_valid ; assumption|].
    intros [expr' ctxt'] [Hexpr' H'].
    destruct H'.
    destruct dep_script eqn:eq_dep_script. (* generated names *)
    (** [s_comm] is a key argument since the injectivity of constructor does not
        hold with impredicative [Set]. *)
    specialize (Script_ir_translator.ex_script.Ex_script_simulation_commutes s)
      as s_comm.
    destruct s eqn:eq_s.
    destruct dep_script_valid.
    simpl in *.
    subst.
    eapply Error.split_letP with (fun  '(_,ctxt) => Raw_context.Valid.t ctxt). {
      apply Error_monad.letP_Build_extensible_elim ; [ easy |].
      apply Script_ir_translator.parse_views_is_valid. assumption. }
    i_des_pairs.
    eapply Error.split_letP with (fun '(x,_,_) => Raw_context.Valid.t x). {
      inversion Hexpr'. destruct H3 as [? [? _]].
      apply apply_origination_is_valid ; try easy. }
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Delegation.
    esplit_letP ; [ apply apply_delegation_is_valid ; assumption |].
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Register_global_constant.
    esplit_letP ; [
      apply Alpha_context.Script.force_decode_in_context_is_valid ;
      assumption|].
  intros [expr ctxt'] H'. simpl.
  eapply Error.split_letP
    with (fun '(ctxt,address,size_value) => Raw_context.Valid.t ctxt);  [
    apply Global_constants_storage.register_is_valid; assumption|].
  clear ctxt' H'. intros [[ctxt' address] size_value] H'. assumption.
  }
  { grep @Alpha_context.Set_deposits_limit.
    eapply Error.split_letP with (fun x => True).
    { destruct o.
      destruct Alpha_context.Tez.op_gt ;
      [ unfold Error_monad.error_value ; reflexivity | constructor ].
        constructor.
    }
    intros _ _.
    eapply Error.split_letP with (fun x => True). {
      unfold error_unless.
      destruct (Alpha_context.Delegate.registered ctxt source) ;
        [ constructor |].
      unfold Error_monad.error_value. reflexivity.
    }
    intros _ _. simpl.
    apply Delegate_storage.set_frozen_deposits_limit_is_valid ; assumption.
  }
  { grep Operation_repr.Increase_paid_storage.
    esplit_letP ;
      [ apply Contract_storage.increase_paid_storage_is_valid ; assumption |].
   iesplit_letP ; [
      apply Fees_storage.burn_storage_increase_fees_is_valid  ; assumption |].
   i_des_pairs. easy.
  }
  { grep Operation_repr.Update_consensus_key.
    eapply Error.split_letP with (fun x => True). {
      unfold error_unless.
      destruct (Alpha_context.Delegate.registered _ _) ;
      [ constructor |].
      unfold Error_monad.error_value. reflexivity. }
    intros _ _.
    esplit_letP ; [
      apply Delegate_consensus_key.register_update_is_valid ; assumption |].
    intros ; assumption.
  }
  { grep @Operation_repr.Tx_rollup_origination.
    esplit_letP; [
      apply Tx_rollup_storage.originate_is_valid ; assumption |].
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Tx_rollup_submit_batch.
    esplit_letP ; [apply Tx_rollup_gas.hash_cost_is_valid ;
      assumption |].
    intros gas Hgas.
    eapply Error.split_letP with (fun ctxt => Raw_context.Valid.t ctxt) ; [
      apply Alpha_context.Gas.consume_is_valid; assumption |].
    intros ctxt' H'.
    eapply Error.split_letP with (fun '(ctxt, state ) =>
    Raw_context.Valid.t ctxt /\ Tx_rollup_state_repr.Valid.t state).
    { destruct t eqn:eq_t. apply Tx_rollup_state_storage.get_is_valid.
      assumption. }
    iesplit_letP.
    { apply Tx_rollup_inbox_storage.append_message_is_valid. assumption.
      assumption. destruct t. assumption. }
    iesplit_letP.
    { simpl. apply Tx_rollup_state_repr.burn_cost_is_valid; try assumption.
        apply String.length_is_valid. assumption.
    }
    iesplit_letP ; [
      apply Token.transfer_is_valid; assumption |].
    clear ctxt H_ctxt.
    iesplit_letP ; [
      apply Tx_rollup_state_storage.update_is_valid ; easy |].
    intros. assumption.
  }
  { grep @Operation_repr.Tx_rollup_commit.
    esplit_letP ; [
      apply Tx_rollup_state_storage.get_is_valid ; assumption |].
    clear ctxt H_ctxt.
    intros [ctxt state] [H Hstate].
    eapply Error.split_letP  with (fun '(ctxt, _) =>
      Raw_context.Valid.t ctxt).
    { esplit_letP
    ; [
        apply Tx_rollup_commitment_storage.has_bond_is_valid; assumption |].
      clear ctxt H state Hstate.
      intros [ctxt pending] H.
      destruct pending ; [assumption |].
      apply Token.weak_transfer_is_valid; [assumption |].
      apply Constants_storage.tx_rollup_commitment_bond_is_valid. assumption.
    }
    clear ctxt H.
    intros [ctxt balance_updates] H.
    esplit_letP ; [
      apply Tx_rollup_commitment_storage.add_commitment_is_valid ;
      assumption |].
    clear ctxt H state Hstate.
    intros [[ctxt state] to_slash] [H Hstate].
    eapply Error.split_letP with (fun '(ctxt , _ ) => Raw_context.Valid.t ctxt).
    { destruct to_slash as [pkh |] eqn:eq_to_slash.
      { esplit_letP ; [
          apply Tx_rollup_commitment_storage.slash_bond_is_valid ;
          assumption |].
        clear ctxt H.
        intros [ctxt slashed] H.
        destruct slashed.
        { esplit_letP ; [ apply Token.balance_is_valid ;
            assumption |].
          i_des_pairs.
          apply Token.weak_transfer_is_valid ; assumption.
        }
        { assumption. }
      }
      { assumption. }
    }
    iesplit_letP ;
      [ apply Tx_rollup_state_storage.update_is_valid ; assumption |].
    intros. assumption.
  }
  { grep @Operation_repr.Tx_rollup_return_bond.
    esplit_letP ; [
      apply Tx_rollup_commitment_storage.remove_bond_is_valid ; assumption |].
    iesplit_letP ; [
      apply Token.balance_is_valid ; assumption |].
    iesplit_letP ; [apply Token.weak_transfer_is_valid ; assumption |].
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Tx_rollup_finalize_commitment.
    esplit_letP ; [
      now apply Tx_rollup_state_storage.get_is_valid|].
    iesplit_letP ; [
      apply Tx_rollup_commitment_storage.finalize_commitment_is_valid ;
      assumption |].
    iesplit_letP ; [
      apply Tx_rollup_state_storage.update_is_valid ;  assumption |].
    intros. assumption.
  }
  { grep @Operation_repr.Tx_rollup_remove_commitment.
    esplit_letP ; [
      now apply Tx_rollup_state_storage.get_is_valid|].
    iesplit_letP ; [
      apply Tx_rollup_commitment_storage.remove_commitment_is_valid ;
      assumption |].
    iesplit_letP ; [
      apply Tx_rollup_state_storage.update_is_valid ;  assumption |].
    intros. assumption.
  }
  { grep @Operation_repr.Tx_rollup_rejection.
    esplit_letP ; [
      apply Tx_rollup_state_storage.get_is_valid ; assumption |].
    iesplit_letP ; [
      apply Tx_rollup_state_repr.check_level_can_be_rejected_is_valid ; easy |].
    intros _ _.
    esplit_letP. {
      apply Tx_rollup_commitment_storage.get_is_valid ; try assumption.
    }
    i_des_pairs.
    eapply Error.split_letP with (fun _ => True).
    { unfold error_when.
      unfold error_when.  destruct t eqn:eq_t, t3. simpl.
      (** generated names!*)
      (* 1. Fore some reason, [destruct ( _ || _).] doesn't work
        (makes vscoq loop)  *)
      (* 2. Moreover, this destruct takes time. Is there a way to make it
            more efficient? *)
      destruct ((message_position0 <i 0)
      || (commitment
          .(@Alpha_context.Tx_rollup_commitment.template.messages _)
          .(Alpha_context.Tx_rollup_commitment.Compact.excerpt.count) <=i
          message_position0))%bool.
      { reflexivity. }
      { constructor. }
    }
    intros _ _.
    esplit_letP.
    { apply Tx_rollup_inbox_storage.check_message_hash_is_valid ; simpl in *.
      { assumption. } { easy. }
      { destruct t. simpl in *. lia. }
      { lia. }
    }
    iesplit_letP.
    { apply Tx_rollup_commitment_storage.check_agreed_and_disputed_results_is_valid ;
      try assumption.
      destruct t. simpl in *. lia.
  (** [lia] takes time. An explicit proof with
      [Pervasives.Int.Valid.valid_non_negative_implies_valid] could be
      quicker. *)
    }
    intros ctxt3 H3.
    step ; [| constructor].
    iesplit_letP ; [
      apply Tx_rollup_l2_verifier.verify_proof_is_valid ; assumption |].
    iesplit_letP.
    { apply Tx_rollup_commitment_storage.reject_commitment_is_valid ;
      assumption. }
    iesplit_letP ; [
      apply Tx_rollup_commitment_storage.slash_bond_is_valid ;
      assumption |].
    i_des_pairs.
    eapply Error.split_letP with (fun '(ctxt,_) => Raw_context.Valid.t ctxt).
    { destruct b.
      { esplit_letP ; [ apply Token.balance_is_valid ;
        assumption |].
        i_des_pairs.
        eapply Error.split_letP with (fun x => Tez_repr.Valid.t x).
        { destruct_all Tez_repr.t.
          unfold bind_prop. simpl. lia.
  (** [lia]just above is slow. *)
        }
        iesplit_letP ; [
          apply Token.weak_transfer_is_valid ; assumption |].
        iesplit_letP ; [
          apply Token.weak_transfer_is_valid ; assumption |].
        i_des_pairs. assumption.
      }
      { assumption. }
    }
    iesplit_letP ; [
      apply Tx_rollup_state_storage.update_is_valid ; assumption |].
    intros. assumption.
  }
  { grep @Operation_repr.Tx_rollup_dispatch_tickets.
    esplit_letP ; [
      now apply Tx_rollup_state_storage.get_is_valid|].
    iesplit_letP.
      { apply Tx_rollup_commitment_storage.get_finalized_is_valid; assumption. }
    iesplit_letP.
    { apply Tx_rollup_reveal_storage.mem_is_valid ; assumption. }
      i_des_pairs.
      eapply Error.split_letP with (fun _ => True) ; [
        unfold error_when; destruct b ; easy |].
    intros _ _.
    apply Error.split_letP with (fun '(_ , l , ctxt ) =>
      (Forall (fun '(o,ex_token) => Tx_rollup_withdraw_repr.order.Valid.t o /\
        Ticket_token.ex_token.Valid.t ex_token) l) /\
      Raw_context.Valid.t ctxt).
      (** @TODO The above predicate (used in [List.fold_left_e]
        is perhaps implicitly a validity predicate of something *)
    { destruct t eqn:eq_t.
      apply List.fold_left_e_is_valid
        with (P_b :=fun fun_parameter =>
          Tx_rollup_reveal_repr.Valid.t fun_parameter).
      { i_des_pairs. (*  Tx_rollup_reveal_repr.Valid.t. for the [item] whic appears *)
        apply Error.split_letP with (fun _ => True); [
          unfold error_when ; destruct Tx_rollup_l2_qty.op_lteq; constructor |].
        intros _ _.
        iesplit_letP ; [
          apply Tx_rollup_ticket.parse_ticket_is_valid ; [assumption] |].
        iesplit_letP ; [
          inversion H4 ;  (* i.e. [Tx_rollup_reveal_repr.Valid.t item)]  *)
          apply Tx_rollup_ticket.make_withdraw_order_is_valid ; assumption |].
        i_des_pairs. simpl.
        split ; [| assumption].
        constructor ; [| assumption].
        split ; assumption.
      }
      { split ; [ constructor | assumption]. }
      { assumption. }
    }
    iesplit_letP ; [
      apply Tx_rollup_hash_builder.withdraw_list_is_valid ; assumption |].
    iesplit_letP. {
      apply Tx_rollup_commitment_storage.check_message_result_is_valid ;
       assumption. }
    iesplit_letP; [
      apply Tx_rollup_reveal_storage.record_is_valid;
      assumption |].
    i_des_pairs.
    eapply Error.split_letP with (fun '(ctxt,_) => Raw_context.Valid.t ctxt). {
      apply List.fold_left_e_is_valid
        with (P_b := fun '(o,ex_token) =>
        Tx_rollup_withdraw_repr.order.Valid.t o /\
        Ticket_token.ex_token.Valid.t ex_token) ; try assumption.
      intros [ctxt8 z8] [o ex_token] H8 [Ho H_ex_token].
      eapply Error.split_letP with (fun _ => True). {
        destruct (value_e) eqn:eq_value_e ; [constructor |].
        unfold value_e in eq_value_e.
        destruct (Ticket_amount.of_zint) eqn:eq_tick_am ; [
          inversion eq_value_e |].
        rewrite <- eq_value_e. constructor.
      }
      intros amount _.
      esplit_letP ; [
        apply Ticket_balance_key.of_ex_token_is_valid; assumption |].
      iesplit_letP ; [
        apply Tx_rollup_ticket.transfer_ticket_with_hashes_is_valid ;
        assumption |].
        i_des_pairs. assumption.
    }

    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Transfer_ticket.
    step ; [constructor |].
    destruct t eqn:eq_t.
    simpl in amount.
    esplit_letP . {
      apply Tx_rollup_ticket.parse_ticket_and_operation_is_valid ; assumption.
    }
    iesplit_letP ; [
      apply  Tx_rollup_ticket.transfer_ticket_is_valid ; assumption |].
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Dal_publish_slot_header.
    iesplit_letP; [
       apply Dal_apply.apply_publish_slot_header_is_valid; assumption|].
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Sc_rollup_originate.
    iesplit_letP ; [
      apply Sc_rollup_operations.originate_is_valid ; assumption|].
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Sc_rollup_add_messages.
    esplit_letP ; [
      apply Sc_rollup_inbox_storage.add_external_messages_is_valid;
      assumption|].
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Sc_rollup_cement.
    esplit_letP ; [
      apply Sc_rollup_stake_storage.cement_commitment_is_valid; assumption |].
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Sc_rollup_publish.
    iesplit_letP ; [
      apply Sc_rollup_stake_storage.publish_commitment_is_valid ; assumption |].
      i_des_pairs. assumption.
  }
  { destruct s eqn:eq_s.
    eapply Error.split_letP with (fun '(_,ctxt)
      =>  Raw_context.Valid.t ctxt
    ). {
    (** @TODO perhaps add later a post-condition of the form:
        [Option.Forall Sc_rollup_game_repr.game_result.Valid.t game_result]
        on the first element of the output pair *)
      step. {
        apply Sc_rollup_refutation_storage.game_move_is_valid ; assumption.
      }
      { esplit_letP ; [
          apply Sc_rollup_refutation_storage.start_game_is_valid ;
          assumption |].
        i_des_pairs. assumption.
      }
    }
    i_des_pairs.
    apply Error.split_letP with (fun '(status, ctxt, l0) =>
      Raw_context.Valid.t ctxt /\
      Forall (fun '(status,balance_update,update_origin) =>
      Receipt_repr.balance_update.Valid.t balance_update) l0
    ). { destruct o as [game_res |].
         { apply Sc_rollup_refutation_storage.apply_game_result_is_valid.
           assumption. }
         { split ; [ assumption | constructor]. }
    }
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Sc_rollup_timeout.
    iesplit_letP ; [
      apply Sc_rollup_refutation_storage.timeout_is_valid ; assumption |].
    iesplit_letP ; [
      apply Sc_rollup_refutation_storage.apply_game_result_is_valid ;
      assumption |].
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Sc_rollup_execute_outbox_message.
    iesplit_letP ; [
      apply Sc_rollup_operations.execute_outbox_message_is_valid ;
      assumption |].
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Sc_rollup_recover_bond.
    iesplit_letP ; [
      apply Sc_rollup_stake_storage.withdraw_stake_is_valid; assumption |].
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Sc_rollup_dal_slot_subscribe.
    iesplit_letP; [
      apply Sc_rollup_storage.Dal_slot.subscribe_is_valid; assumption |].
    i_des_pairs. assumption.
  }
  { grep @Operation_repr.Zk_rollup_origination.
    apply Zk_rollup_apply.originate_is_valid ; try assumption.
    constructor ; try assumption ; constructor.
  }
  { grep @Operation_repr.Zk_rollup_publish.
    eapply Zk_rollup_apply.publish_is_valid ; assumption.
  }
Qed.

Module success_or_failure.
  (** Validity of the type [success_or_failure]. *)
  Module Valid.
    Definition t (x : Apply.success_or_failure) : Prop :=
      match x with
      | Apply.Success ctxt => Raw_context.Valid.t ctxt
      | Apply.Failure => True
      end.
  End Valid.
End success_or_failure.

(** @TODO spec *)
(** @TODO place in the right spot
*)
Module fees_updated_contents.
  (** The validy predicate for the type [Apply.fees_updated_contents] *)
  Module Valid.
    Record t (kind : Operation_repr.Operation_kind.t)
      (x : Apply.fees_updated_contents) : Prop := {
      contents :
          Operation_repr.Valid.contents kind
            x.(Apply.fees_updated_contents.contents);
      balance_updates :
        Receipt_repr.balance_updates.Valid.t
          x.(Apply.fees_updated_contents.balance_updates);
  }.
  End Valid.
End fees_updated_contents.

Module fees_updated_contents_list.
  Fixpoint Forall P (l : Apply.fees_updated_contents_list)  : Prop :=
    match l with
    | Apply.FeesUpdatedCons x0 l0 =>
        P x0 /\ Forall P l0
    | Apply.FeesUpdatedSingle x0 => P x0
    end.

  (** The validity predicate for the list type
      [Apply.fees_updated_contents_list], which has specific construcotrs *)
  Module Valid.
    Definition t (kind : Operation_repr.Operation_kind.t)
      : Apply.fees_updated_contents_list -> Prop :=
      Forall (fees_updated_contents.Valid.t kind).
  End Valid.
End fees_updated_contents_list.

#[bypass_check(guard)]
Fixpoint apply_internal_operations_fix
(ctxt : Alpha_context.context)
(payer : Alpha_context.public_key_hash)
(chain_id : Chain_id.t)
    (applied : list Apply_internal_results.packed_internal_operation_result)
    (worklist : list Script_typed_ir.packed_internal_operation) {struct worklist}
    : Apply.success_or_failure *
      list Apply_internal_results.packed_internal_operation_result :=
    match worklist with
    | [] => ((Apply.Success ctxt), (List.rev applied))
    |
      cons
        (Script_typed_ir.Internal_operation
          ({|
            Script_typed_ir.internal_operation.source := source;
              Script_typed_ir.internal_operation.operation := operation;
              Script_typed_ir.internal_operation.nonce := nonce_value
              |} as op)) rest =>
      let function_parameter :=
        if Alpha_context.internal_nonce_already_recorded ctxt nonce_value then
          let op_res := Apply_internal_results.internal_operation_value op in
          Error_monad.fail
            (Build_extensible "Internal_operation_replay"
              Apply_internal_results.packed_internal_operation
              (Apply_internal_results.Internal_operation op_res))
        else
          let ctxt := Alpha_context.record_internal_nonce ctxt nonce_value in
          Apply.apply_internal_operation_contents ctxt payer source chain_id operation
        in
      match function_parameter with
      | Pervasives.Error errors =>
        let result_value :=
          Apply_internal_results.pack_internal_operation_result op
            (Apply_operation_result.Failed
              (Script_typed_ir.manager_kind
                op.(Script_typed_ir.internal_operation.operation)) errors) in
        let skipped :=
          List.rev_map
            (fun (function_parameter :
              Script_typed_ir.packed_internal_operation) =>
              let 'Script_typed_ir.Internal_operation op := function_parameter
                in
              Apply_internal_results.pack_internal_operation_result op
                (Apply_operation_result.Skipped
                  (Script_typed_ir.manager_kind
                    op.(Script_typed_ir.internal_operation.operation)))) rest in
        (Apply.Failure,
          (List.rev (Pervasives.op_at skipped (cons result_value applied))))
      | Pervasives.Ok (ctxt, result_value, emitted) =>
       apply_internal_operations_fix ctxt payer chain_id
          (cons
            (Apply_internal_results.pack_internal_operation_result op
              (Apply_operation_result.Applied result_value)) applied)
          (Pervasives.op_at emitted rest)
      end
    end.
(** @Q @REVIEWS Should I "close" #[bypass_check(guard)]?  *)

Lemma apply_internal_operations_fix_unfold
(ctxt : Alpha_context.context) (payer : Alpha_context.public_key_hash)
  (chain_id : Chain_id.t) (ops : list Script_typed_ir.packed_internal_operation) :
  Apply.apply_internal_operations ctxt payer chain_id ops =
    apply_internal_operations_fix ctxt payer chain_id nil ops.
Proof.
   (** Proto_alpha: this lemma is difficult to prove because the recursive call
      of this function (called [apply] in the ml code) may be on a
      structurally  *bigger* last parameter [emited ++ rest], cf.:
      [ apply ctxt
          (cons
            (Apply_internal_results.pack_internal_operation_result op
              (Apply_operation_result.Applied result_value)) applied)
          (Pervasives.op_at emitted rest)]
  *)
  generalize dependent chain_id.
  generalize dependent payer.
  induction ops as [ |op ops IH]; [ reflexivity |].  (** @TODO name *)
  intros payer chain_id.
  unfold Apply.apply_internal_operations.
  unfold
   apply_internal_operations_fix.
  Fail rewrite <- IH.
Admitted.
  (* induction ops.
  unfold Apply.apply_internal_operations , apply_internal_operations_fix.
  simpl. hnf.
  match goal with | [ |- ?lhs0 = ?rhs0] => pose lhs0 as lhs; pose rhs0 as rhs
  end.
  hnf in lhs.  in rhs. Print nil.
  Print   Proto_alpha.Raw_context.t.record.

  assert (HK : lhs = ((Apply.Success ctxt), nil)).
  unfold lhs.
  reflexivity.
cbv. *)
(*
Check apply_internal_operations_fix.
Check Apply.apply_internal_operation_contents. *)

(** The internal fixpoint of [apply_internal_operations] is valid. *)
Lemma apply_internal_operations_fix_is_valid ctxt payer chain_id applied ops :
  Raw_context.Valid.t ctxt ->
  (Forall Script_typed_ir.packed_internal_operation.Valid.t ops) ->
  let '(result, _) :=
    apply_internal_operations_fix
      ctxt payer chain_id applied (List.map
        With_family.to_packed_internal_operation ops)
  in success_or_failure.Valid.t result.
Proof.
  (** Proto_alpha: this lemma is difficult to prove because the recursive call
      of this function (called [apply] in the ml code) may be on a
      structurally  *bigger* last parameter [emited ++ rest], cf.:
      [ apply ctxt
          (cons
            (Apply_internal_results.pack_internal_operation_result op
              (Apply_operation_result.Applied result_value)) applied)
          (Pervasives.op_at emitted rest)]
  *)
  (* intros H Hops.
  generalize dependent applied.
  generalize dependent ctxt.
  induction ops as [ |op0 ops0 IH] ; [
    unfold apply_internal_operations_fix ; intros ; assumption |].
  intros ctxt H applied.
  unfold apply_internal_operations_fix in IH.
  inversion Hops as [| op0' ops0' Hop0' Hops0' [ op0_eq ops0_eq]].
  subst.
  simpl.
  destruct op0 eqn:eq_op0. (* here appears [operation : With_family.internal_operation_contents]
    and [source: Contract_repr.t] *)
    destruct Hop0' as [Hoperation Hnonce]. simpl in Hoperation. simpl in Hnonce.
  destruct (Alpha_context.internal_nonce_already_recorded ctxt) eqn:b_eq ; [
    constructor |].
  simpl.
  (* apply apply_internal_operation_contents_is_valid.  *)

  destruct (Apply.apply_internal_operation_contents
  (Alpha_context.record_internal_nonce ctxt nonce) payer source chain_id
  (With_family.to_internal_operation_contents operation)) as [p|] eqn:eq_p ;
  try easy.
   specialize (apply_internal_operation_contents_is_valid (Alpha_context.record_internal_nonce
   ctxt nonce) payer source chain_id
   operation) as H_aux0. rewrite eq_p in H_aux0. simpl in H_aux0.
   Locate record_internal_nonce_is_valid. (* nothing *)
    Print Alpha_context.record_internal_nonce.
    Print  Raw_context.record_internal_nonce.
    (* Raw_context.update_internal_nonces_used *)
    (* Proto_alpha.Raw_context.t -> Int.t -> Proto_alpha.Raw_context.t *)
   Locate update_internal_nonces_used_is_valid. (*nothing *)
   Locate internal_nonces_used_is_valid. (* nothing *)
    Print Proto_alpha.Raw_context.back.internal_nonces_used.

   destruct p as [[ctxt0 result_value] emitted].
   specialize (IH Hops0' ctxt0) as H0.

  (* @TODO : work on H_aux0, use Hoperation + new lemma to write
       this will get us Raw_context.Valid.t ctxt0
  *)
   assert (H_aux1 : Raw_context.Valid.t (Alpha_context.record_internal_nonce ctxt nonce) ).
   admit.
   apply H_aux0 in H_aux1.
   Print op_at.

   apply H0
   with (applied := (Apply_internal_results.pack_internal_operation_result
   (With_family.to_internal_operation
      {|
        With_family.internal_operation.source := source;
        With_family.internal_operation.operation := operation;
        With_family.internal_operation.nonce := nonce
      |}) (Apply_operation_result.Applied result_value) :: applied) )
      in H_aux1.*)
Admitted.

(** The function [apply_internal_operations] is valid. *)
(** @TODO Present suitably the statements *)
Lemma apply_internal_operations_is_valid ctxt payer chain_id ops :
  Raw_context.Valid.t ctxt ->
  (Forall Script_typed_ir.packed_internal_operation.Valid.t ops) ->
  let '(result, packed_internal_operation_result_list) :=
    Apply.apply_internal_operations
      ctxt payer chain_id (List.map  With_family.to_packed_internal_operation ops) in
  success_or_failure.Valid.t result.
Proof.
  intros H Hops.
  rewrite apply_internal_operations_fix_unfold.
  apply apply_internal_operations_fix_is_valid ; assumption.
Qed.



(** The function [burn_transaction_storage_fees] is valid. *)
Lemma burn_transaction_storage_fees_is_valid ctxt trr storage_limit payer :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _, _) :=
    Apply.burn_transaction_storage_fees
      ctxt trr storage_limit payer in
  Raw_context.Valid.t ctxt.
Proof.
  intros.
  unfold Apply.burn_transaction_storage_fees.
  destruct trr as [payload | payload | ? | ?] eqn:eq_trr.
  { grep @Apply_internal_results.Transaction_to_contract_result.
    esplit_letP ; [
      apply Fees_storage.burn_storage_fees_is_valid ; assumption |].
    i_des_pairs.
    eapply Error.split_letP with (fun '(ctxt,_,updates) =>
      Raw_context.Valid.t ctxt /\
      Receipt_repr.balance_updates.Valid.t updates).
    { destruct payload eqn:payload_eq.
      destruct allocated_destination_contract. (* generated name *)
      { apply Fees_storage.burn_origination_fees_is_valid ; assumption. }
      { split ; [ assumption | constructor]. }
    }
    i_des_pairs. assumption.
  }
  { grep @Apply_internal_results.Transaction_to_tx_rollup_result.
    iesplit_letP ; [
      apply Fees_storage.burn_storage_fees_is_valid ; assumption |].
    i_des_pairs. assumption.
  }
  { grep @Apply_internal_results.Transaction_to_sc_rollup_result.
    assumption. }
  { grep @Apply_internal_results.Transaction_to_zk_rollup_result.
    iesplit_letP ; [
      apply Fees_storage.burn_storage_fees_is_valid ; assumption |].
    i_des_pairs.
    assumption.
   }
Qed.

(** The function [burn_origination_storage_fees] is valid. *)
Lemma burn_origination_storage_fees_is_valid ctxt params storage_limit payer :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _, _) :=
    Apply.burn_origination_storage_fees
      ctxt params storage_limit payer in
  Raw_context.Valid.t ctxt.
Proof.
  intros H.
  unfold Apply.burn_origination_storage_fees.
  esplit_letP ; [
    apply Fees_storage.burn_storage_fees_is_valid ; assumption |].
  iesplit_letP ; [
    apply Fees_storage.burn_origination_fees_is_valid ; assumption |].
  i_des_pairs ; assumption.
Qed.

(** The function [burn_manager_storage_fees] is valid. *)
Lemma burn_manager_storage_fees_is_valid ctxt smopr storage_limit payer :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _, _) :=
    Apply.burn_manager_storage_fees
      ctxt smopr storage_limit payer in
  Raw_context.Valid.t ctxt.
Proof.
  intro H.
  unfold Apply.burn_manager_storage_fees.
  destruct smopr
  as [ transaction_result | origination_result |
      payload | payload | payload | payload | payload | payload |
      payload | payload | payload | payload | payload | payload |
      payload | payload | payload | payload | payload | payload |
      payload | payload | payload | payload | payload | payload |
      payload | payload | ?]
      eqn:eq_smopr ;
      try assumption ;
      try (esplit_letP ; [
        apply Fees_storage.burn_storage_fees_is_valid ; assumption |
        i_des_pairs ; assumption]).
  (** Proto_L: [assumption] solves 18 cases out of 28 and the second
      [try] solves 9 more cases. *)
  { grep @Apply_results.Transaction_result.
    esplit_letP ; [
      apply Apply.burn_transaction_storage_fees_is_valid ; assumption |].
    i_des_pairs. assumption.
  }
  { grep @Apply_results.Origination_result.
    esplit_letP ; [
      apply burn_origination_storage_fees_is_valid ; assumption |].
    i_des_pairs ; assumption.
  }
Qed.

(** The function [burn_internal_storage_fees] is valid. *)
Lemma burn_internal_storage_fees_is_valid ctxt smopr storage_limit payer :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _, _) :=
    Apply.burn_internal_storage_fees
      ctxt smopr storage_limit payer in
  Raw_context.Valid.t ctxt.
Proof.
  intro H.
  unfold Apply.burn_internal_storage_fees.
  destruct smopr as [transaction_result | origination_result | unused | unused ]
    eqn:eq_smopr ; try assumption.
  { grep @Apply_internal_results.ITransaction_result.
    esplit_letP ; [
      apply Apply.burn_transaction_storage_fees_is_valid ; assumption |].
    i_des_pairs. assumption.
  }
  { grep @Apply_internal_results.IOrigination_result.
    esplit_letP ; [
      apply Apply.burn_origination_storage_fees_is_valid ; assumption |].
    i_des_pairs. assumption.
  }
Qed.

Import Match_spec.

(** The function [apply_manager_contents] is valid. *)
Lemma apply_manager_contents_is_valid ctxt chain_id op :
  Raw_context.Valid.t ctxt ->
  Operation_repr.Valid.contents
    Operation_repr.Operation_kind.Manager_operation op ->
  let '(succ, manager_op_res, pack_i_op_list) :=
    Apply.apply_manager_contents ctxt chain_id op in
  success_or_failure.Valid.t succ.
Proof.
  intros H H_op.
  unfold Apply.apply_manager_contents.
  destruct op eqn:eq_op; try easy.
  (** @TODO optimize code *)
  destruct m eqn:eq_m.
  set (ctxt0 := Alpha_context.Gas.set_limit _ _).
  assert (H0 : Raw_context.Valid.t ctxt0).
  { apply Alpha_context.Gas.set_limit_is_valid; [assumption|].
    inversion H_op. destruct H1; cbn in *.
    red in gas_limit0; Tactics.destruct_conjs; lia.
  }
  destruct (Apply.apply_manager_operation)
      as [app_man_op0|] eqn:eq_app_man_op0 ; [|constructor].
  destruct app_man_op0 as [[ctxt1 pack_internal_op_list] pack_intern_op_list1].
  assert (H1 : Raw_context.Valid.t ctxt1).
  { pose proof apply_manager_operation_is_valid as H'.
    specialize (H' ctxt0 source chain_id operation ltac:(assumption)).
    destruct (Apply.apply_manager_operation _ _ _ _); cbn in *;
      [|discriminate].
    Tactics.destruct_pairs.
    revert H'.
    replace c with ctxt1 by scongruence. intros.
    apply H'.
    sauto lq: on.
  }
  set (app_intern_op := Apply.apply_internal_operations _ _ _ _) .
    (* as  app_intern_op eqn:res_app_intern_op. *)
    (* [s internal_op_results ] *)
  (* assert (H_app_intern_op :
    let '(result,_) := app_intern_op
    in success_or_failure.Valid.t result).
    unfold app_intern_op. *)
    (* Check apply_internal_operations_is_valid. *)
    (* Fail apply apply_internal_operations_is_valid ; assumption. *)
  (* unfold fold_left_es in res_fold_left_es. *)
  (** @TODO prove that outputs are correct. *)
  (* unfold fold_left_es. *)
  (* specialize (List.fold_left_e_is_valid
    (fun '(ctxt,z,l) => Raw_context.Valid.t ctxt)
    (fun _ => True )
    f (ctxt2, storage_limit0, nil) internal_op_results)
    as lem1.
  change Alpha_context.context with Proto_alpha.Raw_context.t in *.
 *)
Admitted.

(** [contents_effects] is an internal (non-recursive) function of
    [take_fees_rec]
*)
Definition contents_effects (ctxt : Alpha_context.context)
  (contents : Operation_repr.contents)
      : M? (Alpha_context.context * Apply.fees_updated_contents) :=
      match contents with
      |
        Alpha_context.Manager_operation {|
          Alpha_context.contents.Manager_operation.source := source;
            Alpha_context.contents.Manager_operation.fee := fee;
            Alpha_context.contents.Manager_operation.gas_limit := gas_limit
            |} =>
        let? ctxt := Alpha_context.Gas.consume_limit_in_block ctxt gas_limit in
        let? ctxt := Alpha_context.Contract.increment_counter ctxt source in
        Error_monad.Lwt_tzresult_syntax.op_letplus
          (Alpha_context.Token.transfer None ctxt
            (Alpha_context.Token.Source_container
              (Alpha_context.Token.Contract (Contract_repr.Implicit source)))
            (Alpha_context.Token.Sink_container Alpha_context.Token.Block_fees)
            fee)
          (fun function_parameter =>
            let '(ctxt, balance_updates) := function_parameter in
            (ctxt,
              {| Apply.fees_updated_contents.contents := contents;
                Apply.fees_updated_contents.balance_updates := balance_updates; |}))
      | _ => unreachable_gadt_branch
      end.
(*
Print Operation_repr.Activate_account.
Locate Operation_repr.Contents.
(* a kind is given to each constructor *)

Print Error_monad.Lwt_tzresult_syntax.op_letplus. *)

Lemma contents_effects_is_valid ctxt contents :
  Raw_context.Valid.t ctxt ->
  (** @TODO specify better condition. *)
  Operation_repr.Valid.contents
    Operation_repr.Operation_kind.Manager_operation
    contents ->
  letP? '(ctxt, fees ) := contents_effects ctxt contents in
  Raw_context.Valid.t ctxt /\
    fees_updated_contents.Valid.t
      Operation_repr.Operation_kind.Manager_operation fees.
Proof.
  intros H Hcontents.
  unfold contents_effects.
  destruct contents eqn:eq_contents; inversion Hcontents.
  iesplit_letP. {
    apply Alpha_context.Gas.consume_limit_in_block_is_valid; try easy.
    inversion H1. red in gas_limit. Tactics.destruct_conjs.
    destruct m; cbn in *. lia.
  }
  iesplit_letP ; [
    apply Contract_storage.increment_counter_is_valid;  assumption |].
  iesplit_letP. {
    apply Token.transfer_is_valid; [assumption|].
    now inversion H1.
  }
  i_des_pairs. split ; [assumption |].
  constructor ; [assumption |].
  unfold Receipt_repr.balance_updates.Valid.t. simpl.
  unfold Receipt_repr.balance_updates.Valid.t in H4.
  assumption.
Qed.

(** Proto_alpha: [take_fees_rec] is an internal fixpoint of [take_fees]. *)
(** @Q validity predicate on contents_lists*)
Fixpoint take_fees_rec
(ctxt : Alpha_context.context) (contents_list : Alpha_context.contents_list)
: M? (Alpha_context.context * Apply.fees_updated_contents_list) :=
match contents_list with
| Alpha_context.Single contents =>
  Error_monad.Lwt_tzresult_syntax.op_letplus (contents_effects ctxt contents)
    (fun function_parameter =>
      let '(ctxt, fees_updated_contents) := function_parameter in
      (ctxt, (Apply.FeesUpdatedSingle fees_updated_contents)))
| Alpha_context.Cons contents rest =>
  let? '(ctxt, fees_updated_contents) := contents_effects ctxt contents in
  Error_monad.Lwt_tzresult_syntax.op_letplus (take_fees_rec ctxt rest)
    (fun function_parameter =>
      let '(ctxt, result_rest) := function_parameter in
      (ctxt, (Apply.FeesUpdatedCons fees_updated_contents result_rest)))
end.

(** Allows unfolding the internal fixpoint of [take_fees]. *)
Lemma take_fees_fix_unfold ctxt contents_list :
  Apply.take_fees ctxt contents_list =
    Error_monad.record_trace (
      Build_extensible "Error_while_taking_fees" unit tt
    )
    (take_fees_rec ctxt contents_list).
Proof.
  reflexivity.
Qed.

(** The internal fixpoint [take_fees] is valid. *)
Lemma take_fees_rec_is_valid ctxt contents_list :
  Raw_context.Valid.t ctxt ->
  Operation_repr.Valid.contents_list
    Operation_repr.Operation_kind.Manager_operation
    contents_list ->
  letP? '(ctxt, fees_update_contents) :=
    take_fees_rec ctxt contents_list in
  Raw_context.Valid.t ctxt /\
    fees_updated_contents_list.Valid.t
      Operation_repr.Operation_kind.Manager_operation
      fees_update_contents.
Proof.
  (** @TODO specify names *)
  intros H Hcontents_list.
  generalize dependent ctxt.
  unfold take_fees_rec.
  induction contents_list.
  { unfold take_fees_rec.
    unfold Lwt_tzresult_syntax.op_letplus.
    iesplit_letP. {
    apply contents_effects_is_valid.
      { assumption. }
      { destruct c eqn:eq_c; inversion Hcontents_list; inversion H2; assumption. }
    }
    i_des_pairs. split ;
    assumption.
  }
  { iesplit_letP.
    { apply contents_effects_is_valid ; try assumption.
      { inversion Hcontents_list. assumption. }
    }
    i_des_pairs.
    eapply Error.split_letP
      with (fun '(ctxt, fees_update_contents) =>
        Raw_context.Valid.t ctxt /\
        fees_updated_contents_list.Valid.t
          Operation_repr.Operation_kind.Manager_operation
          fees_update_contents
      ).
    {
      inversion Hcontents_list.
      { fold take_fees_rec.
        fold take_fees_rec in H3.
        hauto lq: on.
      }
    }
    i_des_pairs. simpl. split ; [assumption|].
    split ; assumption.
  }
Qed.

(** The function [take_fees] is valid. *)
Lemma take_fees_is_valid ctxt contents_list :
  Raw_context.Valid.t ctxt ->
  Operation_repr.Valid.contents_list
    Operation_repr.Operation_kind.Manager_operation
    contents_list ->
  letP? '(ctxt, fees_update_contents) :=
    Apply.take_fees ctxt contents_list in
  Raw_context.Valid.t ctxt /\
    fees_updated_contents_list.Valid.t
      Operation_repr.Operation_kind.Manager_operation
      fees_update_contents.
Proof.
  intros H Hcontents_list.
  rewrite take_fees_fix_unfold.
  apply Error_monad.letP_Build_extensible_elim ; [reflexivity |].
  apply take_fees_rec_is_valid ; assumption.
Qed.

(** The function [apply_manager_contents_list_rec] is valid. *)
Lemma apply_manager_contents_list_rec_is_valid
  ctxt payload_producer chain_id fees_updated_contents_list :
  Raw_context.Valid.t ctxt ->
  fees_updated_contents_list.Valid.t
    Operation_repr.Operation_kind.Manager_operation
    fees_updated_contents_list ->
  let '(result, _) :=
    Apply.apply_manager_contents_list_rec
      ctxt
      payload_producer chain_id fees_updated_contents_list in
  success_or_failure.Valid.t result.
Proof.
  Opaque Apply.apply_manager_contents.
  intros H Hfees_u_contents_l.
  unfold Apply.apply_manager_contents_list_rec.
  (** [ctxt] changes over in the different recursive calls of
       [apply_manager_contents_list_rec], so we quantify it below. *)
  generalize dependent ctxt.
  induction fees_updated_contents_list as [fu_contents | fu_contents rest IH];
  intros ctxt H.
  { destruct fu_contents as [contents balance_updates] eqn:f_contents_eq.
    destruct contents eqn:eq_contents;
      try (destruct Hfees_u_contents_l; cbn in *;
           inversion contents0).
    destruct (Apply.apply_manager_contents ctxt chain_id
      (Alpha_context.Manager_operation m))
      as [[ctxt_result operation_result] internal_operation_results] eqn:eq_res0.
    specialize (apply_manager_contents_is_valid ctxt chain_id
      (Alpha_context.Manager_operation m)) as res1.
    rewrite eq_res0 in res1.
    apply res1.
    { assumption. }
    { scongruence. }
  }
  { destruct fu_contents as [contents balance_updates] eqn:fu_contents_eq.
    inversion Hfees_u_contents_l as [Hfu_contents Hrest].
    inversion Hfu_contents as [Hcontents Hb_updates].
    destruct contents eqn:eq_contents; cbn in *; inversion Hcontents.
    destruct (Apply.apply_manager_contents ctxt chain_id
      (Alpha_context.Manager_operation m)) as [[s op_res] i_op_ress] eqn:res0.
    specialize (apply_manager_contents_is_valid ctxt chain_id
      (Alpha_context.Manager_operation m)) as res1.
    apply res1 in H; [|now constructor].
    rewrite res0 in H.
    destruct s eqn:eq_s ; [| easy].
    fold Apply.apply_manager_contents_list_rec.
    fold Apply.apply_manager_contents_list_rec in IH.
    fold fees_updated_contents_list.Valid.t in Hrest.
    destruct (Apply.apply_manager_contents_list_rec c payload_producer chain_id
    rest) as [ctxt_result results] eqn:eq_res2.
    hauto lq: on.
  }
  Transparent Apply.apply_manager_contents.
Qed.

Module mode.
  (** Validity of the type [mode]. *)
  Module Valid.
    Definition t (x : Apply.mode) : Prop :=
      match x with
      | Apply.Full_construction {|
          Apply.mode.Full_construction.round := round;
          Apply.mode.Full_construction.predecessor_level := predecessor_level;
          Apply.mode.Full_construction.predecessor_round := predecessor_round;
        |} =>
        Round_repr.Valid.t round /\
        Level_repr.Valid.t predecessor_level /\
        Round_repr.Valid.t predecessor_round
      | _ => True
      end.
  End Valid.
End mode.

Module application_state.
  Import Proto_alpha.Apply.application_state.

  (** Validity of the type [application_state]. *)
  (** @TODO add at least field on the validity of the field
      [implicit_operations_results :
        list Apply_results.packed_successful_manager_operation_result]
      when a validity predicate on this type is specified.
        *)
  Module Valid.
    Record t (x : Apply.application_state) : Prop := {
      ctxt : Raw_context.Valid.t x.(ctxt);
      mode : mode.Valid.t x.(mode);
      op_count : Pervasives.Int.Valid.non_negative x.(op_count);
      migration_balance_updates :
        Receipt_repr.balance_updates.Valid.t x.(migration_balance_updates)
    }.
  End Valid.
End application_state.

(** The function [record_operation] is valid. *)
Lemma record_operation_is_valid ctxt hash_value operation :
  Raw_context.Valid.t ctxt ->
  let ctxt :=
    Apply.record_operation ctxt hash_value operation in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

(** The function [record_preendorsement] is valid. *)
Lemma record_preendorsement_is_valid ctxt mode content :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _) :=
    Apply.record_preendorsement ctxt mode content in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

(** The function [record_endorsement] is valid. *)
Lemma record_endorsement_is_valid ctxt mode content :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _) :=
    Apply.record_endorsement ctxt mode content in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

(** The function [apply_manager_contents_list] is valid. *)
Lemma apply_manager_contents_list_is_valid
  ctxt payload_producer chain_id fees_updated_contents_list :
  Raw_context.Valid.t ctxt ->
  let '(ctxt, _) :=
    Apply.apply_manager_contents_list
      ctxt
      payload_producer chain_id fees_updated_contents_list in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

(** The function [apply_manager_operations] is valid. *)
Lemma apply_manager_operations_is_valid
  ctxt payload_producer chain_id mempool_mode contents_list :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _) :=
    Apply.apply_manager_operations
      ctxt
      payload_producer chain_id mempool_mode contents_list in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

(** The function [punish_delegate] is valid. *)
Lemma punish_delegate_is_valid
  ctxt delegate level mistake mk_result payload_producer :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _) :=
    Apply.punish_delegate
      ctxt delegate level mistake mk_result payload_producer in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

(** The function [punish_double_endorsement_or_preendorsement] is valid. *)
Lemma punish_double_endorsement_or_preendorsement_is_valid
  ctxt op1 payload_producer :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _) :=
    Apply.punish_double_endorsement_or_preendorsement
      ctxt op1 payload_producer in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

(** The function [punish_double_baking] is valid. *)
Lemma punish_double_baking_is_valid ctxt bh1 payload_producer :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _) := Apply.punish_double_baking ctxt bh1 payload_producer in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

(** The function [apply_contents_list] is valid. *)
Lemma apply_contents_list_is_valid
  ctxt chain_id mode payload_producer contents_list :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _) :=
    Apply.apply_contents_list
      ctxt chain_id mode payload_producer contents_list in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

(** The function [apply_operation] is valid. *)
Lemma apply_operation_is_valid
  application_state_value operation_hash operation :
  letP? '(state, _) :=
    Apply.apply_operation application_state_value operation_hash operation in
  application_state.Valid.t state.
Proof.
Admitted.

(** The function [may_start_new_cycle] is valid. *)
Lemma may_start_new_cycle_is_valid ctxt :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _, _) := Apply.may_start_new_cycle ctxt in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

(** The function [apply_liquidity_baking_subsidy] is valid. *)
Lemma apply_liquidity_baking_subsidy_is_valid ctxt toggle_vote :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _, _) :=
    Apply.apply_liquidity_baking_subsidy ctxt toggle_vote in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

(** The function [are_endorsements_required] is valid. *)
Lemma are_endorsements_required_is_valid ctxt level :
  Raw_context.Valid.t ctxt ->
  letP? _ := Apply.are_endorsements_required ctxt level in
  True.
Proof.
Admitted.

(** The function [record_endorsing_participation] is valid. *)
Lemma record_endorsing_participation_is_valid ctxt :
  Raw_context.Valid.t ctxt ->
  letP? ctxt := Apply.record_endorsing_participation ctxt in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

(** The function [begin_application] is valid. *)
Lemma begin_application_is_valid
  ctxt chain_id migration_balance_updates migration_operation_results
  predecessor_fitness block_header :
  Raw_context.Valid.t ctxt ->
  letP? state :=
    Apply.begin_application
      ctxt chain_id migration_balance_updates migration_operation_results
      predecessor_fitness block_header in
  application_state.Valid.t state.
Proof.
Admitted.

(** The function [begin_full_construction] is valid. *)
Lemma begin_full_construction_is_valid
  ctxt chain_id migration_balance_updates migration_operation_results
  predecessor_timestamp predecessor_level predecessor_round predecessor
  timestamp block_data_contents :
  Raw_context.Valid.t ctxt ->
  letP? state :=
    Apply.begin_full_construction
      ctxt chain_id migration_balance_updates migration_operation_results
      predecessor_timestamp predecessor_level predecessor_round predecessor
      timestamp block_data_contents in
  application_state.Valid.t state.
Proof.
Admitted.

(** The function [begin_partial_construction] is valid. *)
Lemma begin_partial_construction_is_valid
  ctxt chain_id migration_balance_updates migration_operation_results
  predecessor_level predecessor_fitness :
  Raw_context.Valid.t ctxt ->
  letP? state :=
    Apply.begin_partial_construction
      ctxt chain_id migration_balance_updates migration_operation_results
      predecessor_level predecessor_fitness in
  application_state.Valid.t state.
Proof.
Admitted.

(** The function [finalize_application] is valid. *)
Lemma finalize_application_is_valid
  ctxt block_data_contents round predecessor liquidity_baking_toggle_ema
  implicit_operations_results migration_balance_updates block_producer
  payload_producer :
  Raw_context.Valid.t ctxt ->
  letP? '(ctxt, _) :=
    Apply.finalize_application
      ctxt
      block_data_contents round predecessor liquidity_baking_toggle_ema
      implicit_operations_results migration_balance_updates block_producer
      payload_producer in
  Raw_context.Valid.t ctxt.
Proof.
Admitted.

(** The function [finalize_with_commit_message] is valid. *)
Lemma finalize_with_commit_message_is_valid
  ctxt cache_nonce fitness round op_count :
  Raw_context.Valid.t ctxt ->
  letP? validation_result :=
    Apply.finalize_with_commit_message
      ctxt cache_nonce fitness round op_count in
  True.
Proof.
  unfold Apply.finalize_with_commit_message.
  match goal with
  | |- context[let? x := ?e in return? x] =>
    replace (let? x := e in return? x) with e by now destruct e
  end.
  cbn.
  unfold Raw_context.Cache.sync.
  rewrite Context.Cache.sync_eq.
  apply Alpha_context.finalize_is_valid.
Qed.

(** The function [finalize_block] is valid. *)
Lemma finalize_block_is_valid
  application_state_value shell_header_opt :
  application_state.Valid.t application_state_value ->
  letP? _ :=
    Apply.finalize_block application_state_value shell_header_opt in
  True.
Proof.
  intros H.
  unfold Apply.finalize_block.
  destruct application_state_value.
  destruct H; cbn.
  step.
  { eapply Error.split_letP; [
      apply finalize_application_is_valid; apply ctxt0 |
      intros [ctxt' ?] ?
    ].
    Raw_context.Valid.destruct_rewrite H.
    eapply Error.split_letP; [
      now apply finalize_with_commit_message_is_valid |
      easy
    ].
  }
  { apply Error.split_letP with (fun _ => True). {
      destruct value_e eqn:eq_value_e ; [constructor |].
      rewrite <- eq_value_e.
      destruct shell_header_opt ; constructor.
    }
    intros ? _.
    esplit_letP. {
      apply Fitness_repr.create_is_valid.
      { apply Level_storage.current_is_valid. assumption. }
      { (** We clear all the hypotheses on the fields of
            [application_state_value], except the one on its context.
        *)
        clear mode0 Heqm op_count0 migration_balance_updates0.
        unfold
        Option.map,
        Raw_context.Consensus.locked_round_evidence,
        Raw_context.Raw_consensus.locked_round_evidence.
        simpl in ctxt0.
        destruct ctxt0 as [[fp standalone] [H_to_ctxt [Hsim ?]]].
        inversion Hsim. simpl in *.
        rewrite H_to_ctxt. cbn in *.
        destruct config; simpl in *.
        destruct fp; cbn in *.
        destruct consensus0; cbn in *.
        destruct consensus; cbn in *.
        unfold Option.Forall in locked_round_evidence0.
        destruct locked_round_evidence eqn:eq_locked_round_evidence ;
          constructor ; destruct p eqn:eq_p ;
          destruct locked_round_evidence0 as [Hnonneg Hleqmax] ;
          inversion Hnonneg ; assumption.
      }
      { hfcrush. }
      { hfcrush. }
    }
    iesplit_letP;   [now apply finalize_application_is_valid|].
    intros [ctxt' ?] H_ctxt'.
    eapply Error.split_letP; [
      now apply finalize_with_commit_message_is_valid |
      easy
    ].
  }
  (* { destruct shell_header_opt; simpl; [|easy].
    eapply Error.split_letP.
    { destruct application_state_value.
      Raw_context.Valid.destruct_rewrite ctxt; simpl in *.
      apply Fitness_repr.create_is_valid;
        try rewrite H_sim_ctxt_eq;
        try apply H_sim_ctxt;
        try apply mode.
      unfold
        Option.map,
        Raw_context.Consensus.locked_round_evidence,
        Raw_context.Raw_consensus.locked_round_evidence;
        cbn.
      destruct H_sim_ctxt, back, consensus.
      step; simpl in *; [|easy].
      now step.
    }
    { intros.
      eapply Error.split_letP; [now apply finalize_application_is_valid|].
      intros [ctxt' ?] H_ctxt'.
      eapply Error.split_letP; [
        now apply finalize_with_commit_message_is_valid |
        easy
      ].
    }
  } *)
  { eapply Error.split_letP;
      [now apply Voting_period_storage.get_rpc_current_info_is_valid|].
    intros.
    eapply Error.split_letP; [
      now apply Alpha_context.finalize_is_valid |
      easy
    ].
  }
Qed.

(** The function [value_of_key] is valid. *)
Lemma value_of_key_is_valid ctxt k_value :
  Raw_context.Valid.t ctxt ->
  letP? _ := Apply.value_of_key ctxt k_value in
  True.
Proof.
  intros H_ctxt.
  unfold Apply.value_of_key.
  now apply Cache_repr.Admin.value_of_key_is_valid.
Qed.
