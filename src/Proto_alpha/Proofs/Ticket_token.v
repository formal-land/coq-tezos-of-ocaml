Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Ticket_token.

Require TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_scanner.
Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_family.

Import Error.Tactics_letP.

Module ex_token.
  (** The validity of the type [ex_token]. *)
  Module Valid.
    Inductive t : Ticket_token.ex_token -> Prop :=
    | Intro {a : Ty.t}
      (ticketer : Contract_repr.t)
      (contents_type : Script_typed_ir.ty )
      (contents : Ty.to_Set a) :
      a = Script_typed_ir.ty.Valid.to_Ty_t contents_type ->
      Script_typed_ir.ty.Comparable.t contents_type ->
      Script_typed_ir.ty.Valid.t contents_type ->
      Script_typed_ir.Valid.value a contents ->
      t (Ticket_token.Ex_token {|
        Ticket_token.ex_token.Ex_token.ticketer :=
          ticketer;
        Ticket_token.ex_token.Ex_token.contents_type :=
          contents_type;
          (* With_family.to_ty contents_type; *)
        Ticket_token.ex_token.Ex_token.contents :=
           contents;
      |}).
  End Valid.
End ex_token.

(** The function [token_and_amount_of_ex_ticket] is valid. *)
Lemma token_and_amount_of_ex_ticket_is_valid ex_ticket :
  Ticket_scanner.ex_ticket.Valid.t ex_ticket ->
  let '(ex_token, ticket_amount) :=
      Ticket_token.token_and_amount_of_ex_ticket ex_ticket in
  ex_token.Valid.t ex_token /\ Ticket_amount.Valid.t ticket_amount.
Proof.
  intros [ty Hticket Hcom Hty Hticket_valid].
  dep_destruct Hticket_valid.
  unfold Ticket_token.token_and_amount_of_ex_ticket.
  split ; [| assumption ].
  constructor ; try assumption ; try reflexivity.
Qed.
