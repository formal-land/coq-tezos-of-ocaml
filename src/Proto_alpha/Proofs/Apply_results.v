Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Apply_results.

Require TezosOfOCaml.Environment.V8.Proofs.Data_encoding.

Require TezosOfOCaml.Proto_alpha.Proofs.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Level_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Nonce_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Operation_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Receipt_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Sc_rollup_inbox_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Sc_rollup_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_expr_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Voting_period_repr.

(*
Lemma either_refl (A : Set) (v : option A) : either v v = v.
  destruct v; reflexivity.
Qed.

Module Manager_operation_and_result.
  Import Apply_results.successful_manager_operation_result.  
  Import Operation_repr.internal_operation.
  Import Operation_repr.manager_operation.
  
  Module Valid.
    Definition t (op : Operation_repr.manager_operation)
                 (res : Apply_results.manager_operation_result) :=
      match op with
      | Operation_repr.Reveal p =>
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Reveal_result r =>
            Gas_limit_repr.Is_roundable.t r.(Reveal_result.consumed_gas)
          | _    => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Reveal_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Transaction t =>
         match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Transaction_result tr =>
            Receipt_repr.Balance_updates.Valid.t
              tr.(Transaction_result.balance_updates) /\
            Gas_limit_repr.Is_roundable.t tr.(Transaction_result.consumed_gas) /\
            Tez_repr.Valid.t t.(Transaction.amount)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Transaction_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Origination _ =>
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Origination_result o =>
            Receipt_repr.Balance_updates.Valid.t
              o.(Origination_result.balance_updates) /\
            Gas_limit_repr.Is_roundable.t o.(Origination_result.consumed_gas)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Origination_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Delegation _ => 
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Delegation_result d =>
            Gas_limit_repr.Is_roundable.t d.(Delegation_result.consumed_gas)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Delegation_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Register_global_constant _ =>
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Register_global_constant_result r =>
            Receipt_repr.Balance_updates.Valid.t
              r.(Register_global_constant_result.balance_updates) /\
            Gas_limit_repr.Is_roundable.t r.(Register_global_constant_result.consumed_gas)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Register_global_constant_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Set_deposits_limit _ =>
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Set_deposits_limit_result r =>
             Gas_limit_repr.Is_roundable.t r.(Set_deposits_limit_result.consumed_gas)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Set_deposits_limit_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Tx_rollup_origination =>
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Tx_rollup_origination_result r =>
            Receipt_repr.Balance_updates.Valid.t
              r.(Tx_rollup_origination_result.balance_updates) /\
            Gas_limit_repr.Is_roundable.t r.(Tx_rollup_origination_result.consumed_gas)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Tx_rollup_origination_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Sc_rollup_originate _ =>
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Sc_rollup_originate_result r =>
            Receipt_repr.Balance_updates.Valid.t
              r.(Sc_rollup_originate_result.balance_updates) /\
            Gas_limit_repr.Is_roundable.t r.(Sc_rollup_originate_result.consumed_gas)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Sc_rollup_originate_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Sc_rollup_add_messages _ =>
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Sc_rollup_add_messages_result r =>
            Sc_rollup_inbox.Valid.t r.(Sc_rollup_add_messages_result.inbox_after) /\
            Gas_limit_repr.Is_roundable.t r.(Sc_rollup_add_messages_result.consumed_gas)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Sc_rollup_add_messages_manager_kind => True
          | _ => False
          end
        end
      end.
  End Valid.
End Manager_operation_and_result.

Module Internal_operation_result.
  Import Apply_results.successful_manager_operation_result.  
  Import Operation_repr.internal_operation.
  
  Module Valid.
    Record t (x : Apply_results.packed_internal_operation_result ) : Prop := {
      v_nonce :
        let 'Apply_results.Internal_operation_result int_op _ := x in
        Pervasives.UInt16.Valid.t int_op.(nonce);
      v_internal_operation :
        let 'Apply_results.Internal_operation_result int_op man_op_r := x in
        Operation_repr.Packed_manager_operation.Valid.t
          (Operation_repr.Manager int_op.(operation));
      v_manager_operation_result :
        let 'Apply_results.Internal_operation_result int_op man_op_r := x in
        Manager_operation_and_result.Valid.t int_op.(operation) man_op_r
    }.
  End Valid.

  #[local] Ltac split_and := match goal with | |- context [_ /\ _] => split end.
  #[local] Ltac split_and_true := match goal with | |- context [True /\ _] => split end.

  Lemma internal_operation_result_encoding_is_valid :
    Data_encoding.Valid.t
      Valid.t Apply_results.internal_operation_result_encoding.
      Data_encoding.Valid.data_encoding_auto.
    intros [[src op nonce] m] [[Hn1 Hn2]]; simpl in *;
    destruct op eqn:Eo, m eqn:Em; repeat split; trivial;
    simpl in v_manager_operation_result;
    repeat first
      [ apply v_manager_operation_result
      | apply v_internal_operation
      | match goal with
        | H : Saturation_repr.Strictly_valid.t _ /\
              Gas_limit_repr.Is_rounded.t _ |- _ => destruct H as
                [v_manager_operation_result is_rounded]
        | H1 : _ /\ _ = None,
          H2 : Operation_repr
        .manager_operation.Origination_skeleton _ _ _ _
          |- Apply_results.Internal_operation_result _ _ =
            Apply_results.Internal_operation_result _ _
          => destruct H2, H1; simpl in *; subst; reflexivity
        | |- context [either ?x ?x] => rewrite either_refl; reflexivity
        | |- match ?o with Some _ => True | None => True end =>
          destruct o; trivial
        | H : match ?s with _ => _ end |- _ =>
            destruct s; try tauto; repeat split_and_true; trivial
        | |- context
              [ if Script_repr.is_unit_parameter _ &&
                   (Entrypoint_repr.is_default _) then _ else _ ] =>
          destruct (Script_repr.is_unit_parameter _) eqn:Eunt;
          [destruct (Entrypoint_repr.is_default _) eqn:Edef|];
          simpl;repeat split;trivial
        | Hs : Script_repr.is_unit_parameter _ = true,
               He : Entrypoint_repr.is_default _ = true |- _ =>
          unfold Entrypoint_repr.default; simpl;
          destruct (Entrypoint_repr.default_condition_implies
                      _ _ (andb_true_intro (conj Hs He))) as [?E1 ?E2];
          rewrite <- E1, <- E2; reflexivity
        end
      | simpl in *
      ]; try (apply List.Forall_True; trivial);

      repeat match goal with 
             | |- _ /\ _ => split
             | |- Gas_limit_repr.Is_rounded.t _ => 
                apply Gas_limit_repr.Arith.ceil_is_rounded;
                assumption
             | |- Saturation_repr.Valid.t (Alpha_context.Gas.Arith.ceil _) =>
               apply Gas_limit_repr.Arith.ceil_is_valid;
               apply Saturation_repr.Strictly_valid.implies_valid;
               apply Gas_limit_repr.Is_roundable.implies_strictly_valid;
               easy
             | |- Saturation_repr.Strictly_valid.t _ =>
                apply Gas_limit_repr.Is_roundable.implies_strictly_valid;
                easy
             | |- Gas_limit_repr.Is_rounded.t (Alpha_context.Gas.Arith.ceil _) =>
               apply Gas_limit_repr.Arith.ceil_is_rounded; 
              apply v_manager_operation_result
             | |- Receipt_repr.Balance_updates.Valid.t _ => 
                apply v_manager_operation_result
             end; tauto.
  Qed.
  #[global] Hint Resolve
   internal_operation_result_encoding_is_valid : Data_encoding_db.
End Internal_operation_result.

Module Successful_manager_operation_result.
  Import Apply_results.successful_manager_operation_result.

  Module Valid.
    Definition
      t (x : Apply_results.packed_successful_manager_operation_result) : Prop :=
      let 'Apply_results.Successful_manager_result r := x in
      match r with
      | Apply_results.Reveal_result r =>
        Gas_limit_repr.Is_roundable.t r.(Reveal_result.consumed_gas)
      | Apply_results.Transaction_result t =>
        Receipt_repr.Balance_updates.Valid.t
          t.(Transaction_result.balance_updates) /\
        Gas_limit_repr.Is_roundable.t t.(Transaction_result.consumed_gas)
      | Apply_results.Origination_result o =>
        Receipt_repr.Balance_updates.Valid.t
          o.(Origination_result.balance_updates) /\
        Gas_limit_repr.Is_roundable.t o.(Origination_result.consumed_gas)
      | Apply_results.Delegation_result d =>
        Gas_limit_repr.Is_roundable.t d.(Delegation_result.consumed_gas)
      | Apply_results.Register_global_constant_result _ => False
      | Apply_results.Set_deposits_limit_result s =>
        Gas_limit_repr.Is_roundable.t s.(Set_deposits_limit_result.consumed_gas)
      | Apply_results.Sc_rollup_originate_result s =>
        Receipt_repr.Balance_updates.Valid.t
          s.(Sc_rollup_originate_result.balance_updates) /\
        Gas_limit_repr.Is_roundable.t s.(Sc_rollup_originate_result.consumed_gas)
      | Apply_results.Sc_rollup_add_messages_result _ => False
      | Apply_results.Tx_rollup_origination_result _ => False
      end.
  End Valid.

  Lemma successful_manager_operation_result_encoding_is_valid :
    Data_encoding.Valid.t Valid.t
      Apply_results.successful_manager_operation_result_encoding.
      Data_encoding.Valid.data_encoding_auto.
    intros [op] Hvalid; destruct op;
    repeat match goal with 
             | |- _ /\ _ => split
             | |- match ?e with Some _ => True | None => True end => destruct e; trivial
             | |- Forall _ _ => apply List.Forall_True; trivial
             | |- Gas_limit_repr.Is_rounded.t _ => 
                apply Gas_limit_repr.Arith.ceil_is_rounded; apply Hvalid
             | |- Saturation_repr.Valid.t (Alpha_context.Gas.Arith.ceil _) =>
               apply Gas_limit_repr.Arith.ceil_is_valid;
               apply Saturation_repr.Strictly_valid.implies_valid;
               apply Gas_limit_repr.Is_roundable.implies_strictly_valid;
               apply Hvalid
             | |- Saturation_repr.Strictly_valid.t _ =>
                apply Gas_limit_repr.Is_roundable.implies_strictly_valid;
                apply Hvalid
             | |- Gas_limit_repr.Is_rounded.t (Alpha_context.Gas.Arith.ceil _) =>
               apply Gas_limit_repr.Arith.ceil_is_rounded; 
              apply v_manager_operation_result
             | |- Receipt_repr.Balance_updates.Valid.t _ => 
                apply v_manager_operation_result
             | |- Apply_results.Successful_manager_result _ = _ => sauto
             | _ => apply Hvalid
             end; trivial.
  Qed.
  #[global] Hint Resolve
   successful_manager_operation_result_encoding_is_valid : Data_encoding_db.
End Successful_manager_operation_result.

Module Contents_result.
  Import Apply_results.contents_result.
  Import Apply_results.successful_manager_operation_result.

  Module Valid.
    Definition t (x : Apply_results.packed_contents_result) : Prop :=
      let 'Apply_results.Contents_result r := x in
      match r with
      | Apply_results.Preendorsement_result p =>
        Receipt_repr.Balance_updates.Valid.t
          p.(Preendorsement_result.balance_updates) /\
        Pervasives.Int31.Valid.t p.(Preendorsement_result.preendorsement_power)
      | Apply_results.Endorsement_result e =>
        Receipt_repr.Balance_updates.Valid.t
          e.(Endorsement_result.balance_updates) /\
        Pervasives.Int31.Valid.t e.(Endorsement_result.endorsement_power)
      | Apply_results.Seed_nonce_revelation_result b |
        Apply_results.Double_endorsement_evidence_result b |
        Apply_results.Double_preendorsement_evidence_result b |
        Apply_results.Double_baking_evidence_result b |
        Apply_results.Activate_account_result b
        => Receipt_repr.Balance_updates.Valid.t b
      | Apply_results.Proposals_result | Apply_results.Ballot_result => True
      | Apply_results.Manager_operation_result mr =>
        Receipt_repr.Balance_updates.Valid.t
          mr.(Manager_operation_result.balance_updates) /\
        Forall Internal_operation_result.Valid.t
               mr.(Manager_operation_result.internal_operation_results) /\
        match mr.(Manager_operation_result.operation_result) with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Reveal_result r =>
            Gas_limit_repr.Is_roundable.t r.(Reveal_result.consumed_gas)
          | Apply_results.Transaction_result t =>
            Receipt_repr.Balance_updates.Valid.t
              t.(Transaction_result.balance_updates) /\
            Gas_limit_repr.Is_roundable.t t.(Transaction_result.consumed_gas)
          | Apply_results.Origination_result o =>
            Receipt_repr.Balance_updates.Valid.t
              o.(Origination_result.balance_updates) /\
            Gas_limit_repr.Is_roundable.t o.(Origination_result.consumed_gas)
          | Apply_results.Delegation_result d =>
            Gas_limit_repr.Is_roundable.t d.(Delegation_result.consumed_gas)
          | Apply_results.Register_global_constant_result r =>
            Receipt_repr.Balance_updates.Valid.t
              r.(Register_global_constant_result.balance_updates) /\
            Gas_limit_repr.Is_roundable.t r.(Register_global_constant_result.consumed_gas)
          | Apply_results.Set_deposits_limit_result s =>
            Gas_limit_repr.Is_roundable.t s.(Set_deposits_limit_result.consumed_gas)
          | Apply_results.Tx_rollup_origination_result t =>
            Receipt_repr.Balance_updates.Valid.t
              t.(Tx_rollup_origination_result.balance_updates) /\
            Gas_limit_repr.Is_roundable.t t.(Tx_rollup_origination_result.consumed_gas)
          | Apply_results.Sc_rollup_originate_result s =>
            Receipt_repr.Balance_updates.Valid.t
              s.(Sc_rollup_originate_result.balance_updates) /\
            Gas_limit_repr.Is_roundable.t s.(Sc_rollup_originate_result.consumed_gas)
          | Apply_results.Sc_rollup_add_messages_result s =>
            Sc_rollup_inbox.Valid.t s.(Sc_rollup_add_messages_result.inbox_after) /\
            Gas_limit_repr.Is_roundable.t s.(Sc_rollup_add_messages_result.consumed_gas)
          end
        | Apply_results.Failed _ _ | Apply_results.Skipped _ => True
        end
      end.
  End Valid.

  (* @TODO explicit names for m and s *)
  Lemma contents_result_encoding_is_valid :
    Data_encoding.Valid.t Valid.t Apply_results.contents_result_encoding.
    Data_encoding.Valid.data_encoding_auto.
    intros [cr] Hvalid; destruct cr; repeat split; trivial;
      repeat (apply Gas_limit_repr.Arith.ceil_is_valid || apply Hvalid).
    destruct m; destruct operation_result; simpl.
    { destruct s; simpl; trivial; 
      repeat match goal with 
             | |- _ /\ _ => split
             | |- match ?e with Some _ => True | None => True end => destruct e; trivial
             | |- Forall _ _ => apply List.Forall_True; easy
             | |- Gas_limit_repr.Is_rounded.t _ => 
               apply Gas_limit_repr.Arith.ceil_is_rounded; apply Hvalid
             | |- Saturation_repr.Valid.t (Alpha_context.Gas.Arith.ceil _) =>
               apply Gas_limit_repr.Arith.ceil_is_valid;
               apply Saturation_repr.Strictly_valid.implies_valid;
               apply Gas_limit_repr.Is_roundable.implies_strictly_valid;
               apply Hvalid
             | |- Saturation_repr.Strictly_valid.t _ =>
               apply Gas_limit_repr.Is_roundable.implies_strictly_valid;
               apply Hvalid
             | |- Gas_limit_repr.Is_rounded.t (Alpha_context.Gas.Arith.ceil _) =>
               apply Gas_limit_repr.Arith.ceil_is_rounded; 
               apply v_manager_operation_result
             | |- Receipt_repr.Balance_updates.Valid.t _ => 
               apply v_manager_operation_result
             | |- Apply_results.Applied _ = _ => sauto
             | _ => apply Hvalid
             end; trivial. }
      { destruct s, o; simpl;
        repeat match goal with 
               | |- _ /\ _ => split
               | |- match ?e with Some _ => True | None => True end => destruct e; trivial
               | |- Forall _ _ => apply List.Forall_True; easy
               | |- Gas_limit_repr.Is_rounded.t _ => 
                 apply Gas_limit_repr.Arith.ceil_is_rounded; apply Hvalid
               | |- Saturation_repr.Valid.t (Alpha_context.Gas.Arith.ceil _) =>
                 apply Gas_limit_repr.Arith.ceil_is_valid;
                 apply Saturation_repr.Strictly_valid.implies_valid;
                 apply Gas_limit_repr.Is_roundable.implies_strictly_valid;
                 apply Hvalid
               | |- Saturation_repr.Strictly_valid.t _ =>
                 apply Gas_limit_repr.Is_roundable.implies_strictly_valid;
                 apply Hvalid
               | |- Apply_results.Backtracked _ _ = _ => sauto
               | _ => apply Hvalid
               end; trivial. }

      all: destruct m; simpl in *; sauto lq: on.

      (* Here we make sure that we solved all the goals, even if we will not
         check for the final term as it takes too much time for now. *)
      all: fail.
    Admitted.
  #[global] Hint Resolve contents_result_encoding_is_valid : Data_encoding_db.
End Contents_result.

Module Contents_and_result.
  Import Apply_results.contents_result.  
  Import Apply_results.successful_manager_operation_result.  
  Import Operation_repr.contents.
  Import Operation_repr.internal_operation.
  Import Operation_repr.manager_operation.
  
  Module Valid.
    Definition t2 (op : Operation_repr.manager_operation)
               (res : Apply_results.manager_operation_result) :=
      match op with
      | Operation_repr.Reveal p =>
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Reveal_result r =>
            Gas_limit_repr.Is_roundable.t r.(Reveal_result.consumed_gas)
          | _    => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Reveal_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Transaction t  =>
        Tez_repr.Valid.t t.(Transaction.amount) /\
        Entrypoint_repr.Valid.t t.(Transaction.entrypoint) /\
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
           match s with
           | Apply_results.Transaction_result tr =>
             Receipt_repr.Balance_updates.Valid.t
               tr.(Transaction_result.balance_updates) /\
             Gas_limit_repr.Is_roundable.t tr.(Transaction_result.consumed_gas) /\
             t.(Transaction.parameters) = Script_repr.unit_parameter /\
             t.(Transaction.entrypoint) = Entrypoint_repr.default
           | _ => False
           end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Transaction_manager_kind => 
            t.(Transaction.parameters) = Script_repr.unit_parameter /\
            t.(Transaction.entrypoint) = Entrypoint_repr.default
          | _ => False
          end
        end
      | Operation_repr.Origination op =>
        Tez_repr.Valid.t op.(Operation_repr.manager_operation.Origination.credit) /\
        op.(Operation_repr.manager_operation.Origination.preorigination) = None /\
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Origination_result o =>
            Receipt_repr.Balance_updates.Valid.t
              o.(Origination_result.balance_updates) /\
            Gas_limit_repr.Is_roundable.t o.(Origination_result.consumed_gas)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Origination_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Delegation _ => 
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Delegation_result d =>
            Gas_limit_repr.Is_roundable.t d.(Delegation_result.consumed_gas)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Delegation_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Register_global_constant _ =>
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Register_global_constant_result r =>
            Receipt_repr.Balance_updates.Valid.t
              r.(Register_global_constant_result.balance_updates) /\
            Gas_limit_repr.Is_roundable.t r.(Register_global_constant_result.consumed_gas)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Register_global_constant_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Set_deposits_limit lm =>
        match lm with
        | Some v => Tez_repr.Valid.t v
        | None => True
        end /\
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Set_deposits_limit_result r =>
            Gas_limit_repr.Is_roundable.t r.(Set_deposits_limit_result.consumed_gas)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Set_deposits_limit_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Tx_rollup_origination =>
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Tx_rollup_origination_result r =>
            Receipt_repr.Balance_updates.Valid.t
              r.(Tx_rollup_origination_result.balance_updates) /\
            Gas_limit_repr.Is_roundable.t r.(Tx_rollup_origination_result.consumed_gas)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Tx_rollup_origination_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Sc_rollup_originate _ =>
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Sc_rollup_originate_result r =>
            Receipt_repr.Balance_updates.Valid.t
              r.(Sc_rollup_originate_result.balance_updates) /\
            Gas_limit_repr.Is_roundable.t r.(Sc_rollup_originate_result.consumed_gas)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Sc_rollup_originate_manager_kind => True
          | _ => False
          end
        end
      | Operation_repr.Sc_rollup_add_messages _ =>
        match res with
        | Apply_results.Applied s | Apply_results.Backtracked s _ =>
          match s with
          | Apply_results.Sc_rollup_add_messages_result r =>
            Sc_rollup_inbox.Valid.t r.(Sc_rollup_add_messages_result.inbox_after) /\
            Gas_limit_repr.Is_roundable.t r.(Sc_rollup_add_messages_result.consumed_gas)
          | _ => False
          end
        | Apply_results.Failed m _ | Apply_results.Skipped m =>
          match m with
          | Operation_repr.Kind.Sc_rollup_add_messages_manager_kind => True
          | _ => False
          end
        end
      end.

    Definition t (x : Apply_results.packed_contents_and_result) : Prop :=
      let 'Apply_results.Contents_and_result opc cr := x in
      match opc with
      | Operation_repr.Preendorsement c =>
        Operation_repr.Consensus_content.Valid.t c /\
        match cr with
        | Apply_results.Preendorsement_result p =>
          Receipt_repr.Balance_updates.Valid.t
            p.(Preendorsement_result.balance_updates) /\
          Pervasives.Int31.Valid.t
            p.(Preendorsement_result.preendorsement_power)
        | _ => False
        end
      | Operation_repr.Endorsement c =>
        Operation_repr.Consensus_content.Valid.t c /\
        match cr with
        | Apply_results.Endorsement_result e =>
          Receipt_repr.Balance_updates.Valid.t
            e.(Endorsement_result.balance_updates) /\
          Pervasives.Int31.Valid.t
            e.(Endorsement_result.endorsement_power)
        | _ => False
        end
      | Operation_repr.Seed_nonce_revelation s =>
        match cr with
        | Apply_results.Seed_nonce_revelation_result b => 
          Raw_level_repr.Valid.t s.(Seed_nonce_revelation.level) /\
          Seed_repr.Nonce.Valid.t s.(Seed_nonce_revelation.nonce) /\
          Receipt_repr.Balance_updates.Valid.t b
        | _ => False
        end
      | Operation_repr.Double_preendorsement_evidence d =>
        match cr with
        | Apply_results.Double_preendorsement_evidence_result b =>
          Operation_repr.Preendorsement_operation.Valid.t
            d.(Double_preendorsement_evidence.op1) /\
          Operation_repr.Preendorsement_operation.Valid.t
            d.(Double_preendorsement_evidence.op2) /\
          Receipt_repr.Balance_updates.Valid.t b 
        | _ => False
        end
      | Operation_repr.Double_endorsement_evidence d =>
        match cr with
        | Apply_results.Double_endorsement_evidence_result b =>
          Operation_repr.Endorsement_operation.Valid.t
            d.(Double_endorsement_evidence.op1) /\
          Operation_repr.Endorsement_operation.Valid.t
            d.(Double_endorsement_evidence.op2) /\
          Receipt_repr.Balance_updates.Valid.t b
        | _ => False
        end
      | Operation_repr.Double_baking_evidence d =>
        match cr with
        | Apply_results.Double_baking_evidence_result b =>
          Block_header_repr.Protocol_data.Valid.t
            d.(Double_baking_evidence.bh1)
             .(Block_header_repr.t.protocol_data) /\
          Block_header_repr.Protocol_data.Valid.t
            d.(Double_baking_evidence.bh2)
             .(Block_header_repr.t.protocol_data) /\
          Receipt_repr.Balance_updates.Valid.t b
        | _ => False
        end
      | Operation_repr.Activate_account a =>
        match cr with
        | Apply_results.Activate_account_result b =>
          Blinded_public_key_hash.Activation_code.Valid.t
            a.(Activate_account.activation_code) /\
          Receipt_repr.Balance_updates.Valid.t b
        | _ => False
        end
      | Operation_repr.Proposals p =>
        match cr with
        | Apply_results.Proposals_result =>
          Int32.Valid.t p.(Proposals.period) /\
          Forall (fun _ : Protocol_hash.t => True)
                 p.(Proposals.proposals)
        | _ => False
        end
      | Operation_repr.Ballot b =>
        match cr with
        | Apply_results.Ballot_result => Int32.Valid.t b.(Ballot.period)
        | _ => False
        end
      | Operation_repr.Failing_noop _ => False
      | Operation_repr.Manager_operation mo =>
        match cr with
        | Apply_results.Manager_operation_result mor =>
          let
            '{| Manager_operation.source := source;
                Manager_operation.fee := fee;
                Manager_operation.counter := counter;
                Manager_operation.operation := operation;
                Manager_operation.gas_limit := gas_limit;
                Manager_operation.storage_limit := storage_limit |} := mo in
          let
            '{| Manager_operation_result.balance_updates := balance_updates;
                Manager_operation_result.operation_result := operation_result;
                Manager_operation_result.internal_operation_results :=
                  internal_operation_results |} := mor in
          let 'Tez_repr.Tez_tag fee' := fee in
          Saturation_repr.Strictly_valid.t fee' /\
          Saturation_repr.Strictly_valid.t counter /\
          Saturation_repr.Strictly_valid.t storage_limit /\
          Saturation_repr.Strictly_valid.t gas_limit /\
          Gas_limit_repr.Is_rounded.t gas_limit /\
          Receipt_repr.Balance_updates.Valid.t balance_updates /\
          Forall Internal_operation_result.Valid.t internal_operation_results /\
          t2 operation operation_result
        | _ => False
        end
      end.
  End Valid.

  Lemma contents_and_result_encoding_is_valid :
    Data_encoding.Valid.t Valid.t Apply_results.contents_and_result_encoding.
    Data_encoding.Valid.data_encoding_auto.
    intros [opc cr] Hvalid.
    
    destruct
      opc as [ | | | | | | | | | | mo ], cr as [ | | | | | | | | | mo_res ];
      simpl in Hvalid; try dtauto.

    (* destruct
      opc as [ | | | | | | | | | | mo ], cr as [ | | | | | | | | | mo_res ];
      repeat split; trivial; try apply Hvalid; try (simpl in Hvalid; tauto). *)
    (* destruct
      mo as [ ? ? ? operation ? ? ], mo_res as [? operation_result ?];
      destruct operation eqn:Eo, operation_result eqn:Em;
    simpl in *;
    repeat match goal with 
                | |- _ /\ _ => split
                | |- Tez_repr.Valid.t _ => Utils.tezos_z_easy
                | |- 0 <= _ => Utils.tezos_z_easy
                | |- Saturation_repr.Valid.t _ => Utils.tezos_z_easy
                | |- Gas_limit_repr.Is_rounded.t _ => 
                  apply Gas_limit_repr.Arith.ceil_is_rounded; apply Hvalid
                | |- Saturation_repr.Valid.t (Alpha_context.Gas.Arith.ceil _) =>
                 apply Gas_limit_repr.Arith.ceil_is_valid;
                 apply Saturation_repr.Strictly_valid.implies_valid;
                 apply Gas_limit_repr.Is_roundable.implies_strictly_valid;
                 apply Hvalid
                | |- Saturation_repr.Strictly_valid.t _ =>
                   apply Gas_limit_repr.Is_roundable.implies_strictly_valid;
                   apply Hvalid
                | |- match ?e with Some _ => True | None => True end => destruct e; trivial
                | |- Forall _ _ => apply List.Forall_True; easy
                | |- match (if ?e then _ else _) with _ => _ end => destruct e; hauto lq: on 
                | |- Apply_results.Applied _ = _ => sauto
                | |- Apply_results.Backtracked _ _ = _ => sauto
                | |- Apply_results.Failed _ _ = _ => sauto
                | |- Apply_results.Skipped _ = _ => sauto
                | |- Apply_results.Contents_and_result _ _ = _ => sauto
                | s : Apply_results.successful_manager_operation_result |- _ => destruct s
                | _ => apply Hvalid
                end; trivial. *)
  Admitted.
  #[global] Hint Resolve contents_and_result_encoding_is_valid : Data_encoding_db.
End Contents_and_result.

Module Contents_result_list.
  Module Valid.
    Definition isManagerOperationResult
               (x : Apply_results.contents_result) : Prop :=
      match x with
      | Apply_results.Manager_operation_result _ => True
      | _ => False
      end.

    Fixpoint t (c : Apply_results.contents_result_list) : Prop :=
      match c with
      | Apply_results.Single_result res => True
      | Apply_results.Cons_result res rls =>
          isManagerOperationResult res /\
            match rls with
            | Apply_results.Single_result x => isManagerOperationResult x
            | Apply_results.Cons_result _ _ => t rls
            end
      end.
  End Valid.
End Contents_result_list.

Module Packed_contents_result_list.
  Module Valid.
    Definition t
      (x : Apply_results.packed_contents_result_list) : Prop :=
      Forall Contents_result.Valid.t
             (Apply_results.packed_contents_result_list_to_list x) /\
         let '(Apply_results.Contents_result_list c) := x in
         match c with
         | Apply_results.Single_result res => True
         | Apply_results.Cons_result res rls =>
         Contents_result_list.Valid.isManagerOperationResult res /\
            match rls with
            | Apply_results.Single_result x =>
                Contents_result_list.Valid.isManagerOperationResult x
            | Apply_results.Cons_result _ _ =>
                Contents_result_list.Valid.t rls
            end
         end.
  End Valid.        
End Packed_contents_result_list.

Lemma of_list_to_list : forall c,
      match c with
      | Apply_results.Contents_result_list c1 => Contents_result_list.Valid.t c1
      end ->
      Apply_results.packed_contents_result_list_of_list
        (Apply_results.packed_contents_result_list_to_list c) = Pervasives.Ok c.
    intros.
    destruct c as [c]. 
    induction c as [ | c cs IH ]; auto.
    simpl in *.
    destruct cs; simpl in *.
    destruct c; simpl in *; try tauto.
    destruct c0 eqn:Ec; simpl in *; try tauto.
    destruct H. specialize (IH H0). rewrite IH; clear IH.
    simpl. destruct c; simpl in *; try destruct H. reflexivity.
Qed.

Lemma contents_result_list_encoding_is_valid :
  Data_encoding.Valid.t
    Packed_contents_result_list.Valid.t
    Apply_results.contents_result_list_encoding.
  Data_encoding.Valid.data_encoding_auto. 
  intros x H; split; auto.
  apply H. 
  apply of_list_to_list; destruct x;
  unfold Contents_result_list.Valid.t; destruct c; auto;
  split; apply H. 
Qed.
#[global] Hint Resolve
 contents_result_list_encoding_is_valid : Data_encoding_db.

Module Contents_and_result_list.
  Module Valid.
    Definition isManagerOperationOp (op :  Alpha_context.contents) : Prop :=
      match op with
      | Alpha_context.Operation.Manager_operation _ => True
      | _ => False
      end.

    Fixpoint t (c : Apply_results.contents_and_result_list) : Prop :=
      match c with
      | Apply_results.Single_and_result op res => True
      | Apply_results.Cons_and_result op res rls =>
          Contents_and_result_list.Valid.isManagerOperationOp op /\
            Contents_result_list.Valid.isManagerOperationResult res /\
            match rls with
            | Apply_results.Single_and_result x' x =>
                Contents_result_list.Valid.isManagerOperationResult x /\
                Contents_and_result_list.Valid.isManagerOperationOp x'
            | Apply_results.Cons_and_result _ _ _ => t rls
            end
      end.
  End Valid.
End Contents_and_result_list.

Module Packed_contents_and_result_list.
  Module Valid.
    Definition t
      (x : Apply_results.packed_contents_and_result_list) : Prop :=
      Forall Contents_and_result.Valid.t
             (Apply_results.packed_contents_and_result_list_to_list x) /\
      let '(Apply_results.Contents_and_result_list c) := x in
        match c with
        | Apply_results.Single_and_result op res => True
        | Apply_results.Cons_and_result op res rls =>
          Contents_and_result_list.Valid.isManagerOperationOp op /\
          Contents_result_list.Valid.isManagerOperationResult res /\
          match rls with
          | Apply_results.Single_and_result x' x =>
              Contents_result_list.Valid.isManagerOperationResult x /\
              Contents_and_result_list.Valid.isManagerOperationOp x'
          | Apply_results.Cons_and_result _ _ _ =>
              Contents_and_result_list.Valid.t rls
          end
        end.         
  End Valid.
End Packed_contents_and_result_list.

Lemma of_list_to_list_and : forall c,
    match c with
    | Apply_results.Contents_and_result_list c1 =>
        Contents_and_result_list.Valid.t c1
    end ->
  Apply_results.packed_contents_and_result_list_of_list
    (Apply_results.packed_contents_and_result_list_to_list c) = Pervasives.Ok c.
  intros.
  destruct c as [c]. 
  induction c as [ | op c cs IH ]; auto.
  simpl in *.
  destruct cs eqn:Dcs; simpl in *.
  destruct H as [managerOp [managerC [mangerC1 managerOp1]]].
  destruct op eqn:Dop; simpl in managerOp; destruct managerOp. 
  destruct c0 eqn:C0op; simpl in managerOp1; destruct managerOp1.
  reflexivity. 
  destruct H as [managerOp [managerC IHarg]].
  specialize (IH IHarg). rewrite IH; clear IH.
  simpl.
  destruct op eqn:Dop; simpl in managerOp; destruct managerOp.
  reflexivity.
Qed.

Lemma contents_and_result_list_encoding_is_valid :
  Data_encoding.Valid.t
    Packed_contents_and_result_list.Valid.t  
    Apply_results.contents_and_result_list_encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros. split. apply H.
  apply of_list_to_list_and.
  destruct x. 
  unfold Contents_and_result_list.Valid.t. destruct c. auto.
  split; apply H.
Qed.
#[global] Hint Resolve contents_and_result_list_encoding_is_valid :
  Data_encoding_db.

Module Operation_metadata_encoding_is_valid.
  Module Valid.
    Definition t
               (x : Apply_results.packed_operation_metadata) : Prop :=
      match x with
      | Apply_results.Operation_metadata x' =>
          Packed_contents_result_list.Valid.t
            (Apply_results.Contents_result_list
               x'.(Apply_results.operation_metadata.contents))
      | _ => True
      end.
  End Valid.  
End Operation_metadata_encoding_is_valid.

Lemma operation_metadata_encoding_is_valid :
  Data_encoding.Valid.t Operation_metadata_encoding_is_valid.Valid.t
    Apply_results.operation_metadata_encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.   
#[global] Hint Resolve operation_metadata_encoding_is_valid : Data_encoding_db.

Module Operation_data_and_metadata.
  Import Alpha_context.protocol_data.
  Module Valid.
    Definition p
      ( x : (Alpha_context.packed_protocol_data *
             Apply_results.packed_operation_metadata)) : Prop :=
      let (x1,x2) := x in
      let '(Alpha_context.Operation_data x1) := x1 in
      let '{| contents := contents1;
              signature := signature1; |} := x1 in
      match x2 with
      | Apply_results.Operation_metadata x2 =>
          let
            '{| Apply_results.operation_metadata.contents := contents2; |}
            := x2 in
          match Apply_results.kind_equal_list contents1 contents2 with
          | Some Apply_results.Eq =>
              let
                '(op_contents, res_contents) :=
                Apply_results.unpack_contents_list
                  (Apply_results.pack_contents_list contents1 contents2) in
              (op_contents = contents1) /\ res_contents = contents2 /\
                match Apply_results.pack_contents_list contents1 contents2 with
                | Apply_results.Single_and_result _ _ => True
                | Apply_results.Cons_and_result c1 c2 c3 =>
                    Contents_and_result_list.Valid.isManagerOperationOp c1 /\
                      Contents_result_list.Valid.isManagerOperationResult c2 /\
                      match c3 with
                      | Apply_results.Single_and_result x' x =>
                          Contents_result_list
                            .Valid.isManagerOperationResult x /\
                          Contents_and_result_list
                            .Valid.isManagerOperationOp x'
                      | Apply_results.Cons_and_result _ _ _ =>
                          Contents_and_result_list.Valid.t c3
                      end
                end   
          | None => False
          end /\
            Forall Contents_and_result.Valid.t
    (Apply_results.packed_contents_and_result_list_to_list
       (Apply_results.Contents_and_result_list
          (Apply_results.pack_contents_list contents1 contents2)))
      | Apply_results.No_operation_metadata => False
      end.
  End Valid.
End Operation_data_and_metadata.

Lemma operation_data_and_metadata_encoding_is_valid :
  Data_encoding.Valid.t
    Operation_data_and_metadata.Valid.p
  Apply_results.operation_data_and_metadata_encoding. 
  Data_encoding.Valid.data_encoding_auto. 
  intros x H.
  destruct x as [[[contents1 signature1]] [ [contents2] | ]]; simpl;
    simpl in H;  try destruct (Apply_results.kind_equal_list _ _) as [e | ];
    try destruct e; try destruct ( Apply_results.unpack_contents_list _);
    try destruct H.
  repeat split; trivial; destruct signature1; trivial; subst. 
  destruct (Apply_results.pack_contents_list contents1 contents2) eqn:C1C2;
    auto; apply H. apply H.
  destruct H as [Hc [Hc0Hres]]; subst; reflexivity.
  destruct H as [Hc [Hc0 Hres]]; subst; reflexivity.  
  destruct H. 
Qed.   

#[global] Hint Resolve
 operation_data_and_metadata_encoding_is_valid : Data_encoding_db. 

Module Block_metadata.  
  Module Valid.
    Import Apply_results.block_metadata.
    
    Definition t (b : Apply_results.block_metadata) : Prop :=
      let '{| level_info := level_info;
              balance_updates := balance_updates;
              voting_period_info := voting_period_info;
              liquidity_baking_escape_ema := escape_ema;
             implicit_operations_results := implicit_operations_results;
             consumed_gas := consumed_gas;
           |} := b in
         Raw_level_repr.Valid.t level_info.(Alpha_context.Level.t.level) /\
         Level_repr.Valid.t level_info /\
         Voting_period_repr.Info.Valid.t voting_period_info /\
         Receipt_repr.Balance_updates.Valid.t balance_updates /\
         Int32.Valid.t escape_ema /\
         Saturation_repr.Strictly_valid.t consumed_gas /\
         Forall Successful_manager_operation_result.Valid.t
                  implicit_operations_results.
      
  End Valid.   
End Block_metadata.

Lemma block_metadata_encoding_is_valid :
  Data_encoding.Valid.t Block_metadata.Valid.t
    Apply_results.block_metadata_encoding.
  Data_encoding.Valid.data_encoding_auto. 
  intros bm H; repeat split; auto; destruct bm;
    destruct nonce_hash; trivial; apply H.
Qed. 
#[global] Hint Resolve block_metadata_encoding_is_valid : Data_encoding_db.
*)
