Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Sc_rollup_dissection_chunk_repr.

(* @TODO *)
(** The function [default_check] is valid *)
Lemma default_check_is_valid i chunk1 chunk2 chunks :
  letP? x := Sc_rollup_dissection_chunk_repr.default_check
     i chunk1 chunk2 chunks in
  True.
Proof.
Admitted.
