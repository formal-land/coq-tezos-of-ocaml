Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V8.
Require TezosOfOCaml.Proto_alpha.Ticket_operations_diff.

Require TezosOfOCaml.Proto_alpha.Proofs.Carbonated_map.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_scanner.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_token.
Require TezosOfOCaml.Proto_alpha.Proofs.Ticket_token_map.
(* Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_parameters. *)
Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_family.

Module ticket_transfer.
  Module Valid.
    Import Proto_alpha.Ticket_operations_diff.ticket_transfer.

    (** Validity predicate for the type [ticket_transfer]. *)
    Record t (x : Ticket_operations_diff.ticket_transfer) : Prop := {
      tickets : List.Forall Ticket_scanner.ex_ticket.Valid.t x.(tickets);
    }.
  End Valid.
End ticket_transfer.

Module ticket_token_diff.
  Module Valid.
    Import Proto_alpha.Ticket_operations_diff.ticket_token_diff.

    (** Validity predicate for the type [ticket_token_diff]. *)
    Record t (x : Ticket_operations_diff.ticket_token_diff) : Prop := {
      ticket_token : Ticket_token.ex_token.Valid.t x.(ticket_token);
      total_amount : Ticket_amount.Valid.t x.(total_amount);
      destinations :
        List.Forall
          (fun '(_, amount) => Script_int.n_num.Valid.t amount) x.(destinations);
    }.
  End Valid.
End ticket_token_diff.

Module Destination_map.
  Module Valid.
    (** Validity predicate for the type [t] with a certain domain [P] on [a]. *)
    Definition t {a : Set} (P : a -> Prop)
      (x : Ticket_operations_diff.Destination_map.(Carbonated_map.S.t) a) :
      Prop :=
      Carbonated_map.Make.Valid.t (fun _ => True) P x.
  End Valid.
End Destination_map.

(** Properties for the type [Ticket_token_map.t] applied
    on [Destination_map]. *)
Module Token_map_destination_map.
  Module Valid.
    (** Validity predicate. *)
    (**  The internal contract-indexed map cannot be empty, as specified
         in the ml file@Proto_L (but strangely, not in the mli). *)
    Definition t
      (x :
        Ticket_token_map.t (
          Ticket_operations_diff.Destination_map.(Carbonated_map.S.t)
            Script_int.num)) :
            Prop :=
            Ticket_token_map.Valid.t (Destination_map.Valid.t Ticket_amount.Valid.t)
              x.
            (* /\ x.(Carbonated_map.Make_builder.t.map) <> []. *)

  End Valid.
End Token_map_destination_map.

(* Speed-up the proofs by preventing evaluation in the map definition. *)
#[global] Opaque Ticket_operations_diff.Destination_map.

Module Ticket_token_map.
  (** The function [add] is valid. *)
  Lemma add_is_valid ctxt ticket_token destination amount map :
    Raw_context.Valid.t ctxt ->
    Ticket_token.ex_token.Valid.t ticket_token ->
    Ticket_amount.Valid.t amount ->
    Token_map_destination_map.Valid.t map ->
    letP? '(map, ctxt) :=
      Ticket_operations_diff.Ticket_token_map.add
        ctxt ticket_token destination amount map in
    Token_map_destination_map.Valid.t map /\
    Raw_context.Valid.t ctxt.
  Proof.
    intros H_ctxt H_ticket_token H_amount H_map.
    (* [H_map H_map_non_empty]. *)
    unfold Ticket_operations_diff.Ticket_token_map.add.
    (* 2022/11/22: comments in this proof to be used in a future MR *)
     (*  Check @Error.follow_letP. *)
    (*  forall (a : Set) (x : M? a) (P Q : a -> Prop),
       (forall x0 : a, P x0 -> Q x0) ->
       (letP? ' x0 := x in P x0) -> letP? ' x0 := x in Q x0 *)
    (* apply Error.follow_letP with
      (P := fun '(map,ctxt) =>
         Token_map_destination_map.Valid.t map /\ Raw_context.Valid.t ctxt )
      (Q := fun '(map,ctxt) => (Ticket_token_map.Valid.t (Destination_map.Valid.t Ticket_amount.Valid.t) map) /\
      Raw_context.Valid.t ctxt).    *)
         (* ; [
         trivial |]. *)
         apply Ticket_token_map.update_is_valid; try assumption.
         intros.
         destruct value ; simpl in *.
    (* specialize  (@Error.follow_letP _
    (
      Ticket_operations_diff.Ticket_token_map.update ctxt ticket_token
      (fun (ctxt0 : Alpha_context.context)
         (old_val : M* Ticket_operations_diff.Destination_map
                       .(Carbonated_map.S.t) Ticket_amount.t) =>
       match old_val with
       | Some destination_map =>
           let? ' (destination_map0, ctxt1)
           := Ticket_operations_diff.Destination_map.(Carbonated_map.S.update)
              ctxt0 destination
              (fun (ctxt1 : Alpha_context.context)
                 (prev_amt_opt : M* Ticket_amount.t) =>
               match prev_amt_opt with
               | Some prev_amount =>
                   let? ' ctxt2
                   := Alpha_context.Gas.consume ctxt1
                        (Ticket_costs.add_int_cost prev_amount amount)
                   in return? (Some (Ticket_amount.add prev_amount amount),
                              ctxt2)
               | None => return? (Some amount, ctxt1)
               end) destination_map in return? (Some destination_map0, ctxt1)
       | None =>
           return? (Some
                      (Ticket_operations_diff.Destination_map
                       .(Carbonated_map.S.singleton) destination amount),
                   ctxt0)
       end) map
    )

    (fun '(map,ctxt) =>
    Token_map_destination_map.Valid.t map /\ Raw_context.Valid.t ctxt )
    (fun '(map,ctxt) => (Ticket_token_map.Valid.t (Destination_map.Valid.t Ticket_amount.Valid.t) map) /\
    Raw_context.Valid.t ctxt)) as kik.
    *)
    { eapply Error.split_letP. {
        apply Carbonated_map.Make.update_is_valid with
          (P_ctxt := Raw_context.Valid.t)
          (P_key := fun _ => True)
          (P_value := Ticket_amount.Valid.t); try easy.
        intros.
        destruct value; simpl in *; [|easy].
        eapply Error.split_letP. {
          apply Raw_context.consume_gas_is_valid; try assumption.
          apply Ticket_costs.add_int_cost_is_valid.
        }
        clear ctxt H_ctxt; intros ctxt H_ctxt.
        { simpl.
        split; [|assumption].
        now apply Ticket_amount.add_is_valid. }
      }
      clear ctxt H_ctxt; intros [destination_map ctxt] [H_destination_map H_ctxt].
      simpl.
      split ; [| assumption].
      assumption.
    }
    { split; try assumption.
      apply Carbonated_map.Make.singleton_is_valid ; [constructor |].
      assumption.
    }
  Qed.
End Ticket_token_map.

(** The function [tickets_of_transaction] is valid. *)
Lemma tickets_of_transaction_is_valid {a : Ty.t}
  ctxt destination
  parameters_ty
  parameters :
  Raw_context.Valid.t ctxt ->
  Script_typed_ir.ty.Valid.t parameters_ty ->
  a = Script_typed_ir.ty.Valid.to_Ty_t parameters_ty ->
  Script_typed_ir.Valid.value a parameters ->
  letP? '(transfer, ctxt) :=
    Ticket_operations_diff.tickets_of_transaction
      ctxt destination parameters_ty parameters in
  Option.Forall ticket_transfer.Valid.t transfer /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_parameters_ty Ha H_parameters.
  unfold Ticket_operations_diff.tickets_of_transaction.
  eapply Error.split_letP ; [
    apply (@Ticket_scanner.type_has_tickets_is_valid a) ; assumption |].
  clear ctxt H_ctxt;
    intros [has_tickets_value ctxt] [H_has_tickets_value [H_ctxt ty_value_eq]].
  destruct has_tickets_value as [ht ty_value] eqn:has_tickets_value_eq.
  eapply Error.split_letP with (fun '(acc, ctxt)
    => Ticket_scanner.Ticket_collection.accumulator.Valid.t acc /\
    Raw_context.Valid.t ctxt ).
  { specialize (@Ticket_scanner.tickets_of_value_is_valid a ctxt true
    (Ticket_scanner.Has_tickets ht ty_value)  ht ty_value)
      as tickets_of_value_aux.
    subst ty_value.
    apply tickets_of_value_aux ; try reflexivity ; try assumption.
  }
  clear ctxt H_ctxt; intros [tickets ctxt] [H_tickets H_ctxt].
  simpl.
  easy.
Qed.

(** The function [tickets_of_origination] is valid. *)
Lemma tickets_of_origination_is_valid {a : Ty.t}
  ctxt preorigination storage_type
  storage_value :
  a = Script_typed_ir.ty.Valid.to_Ty_t storage_type ->
  Script_typed_ir.Valid.value a storage_value ->
  Raw_context.Valid.t ctxt ->
  Script_typed_ir.ty.Valid.t storage_type ->
  letP? '(transfer, ctxt) :=
    Ticket_operations_diff.tickets_of_origination ctxt preorigination
      storage_type storage_value
  in Option.Forall ticket_transfer.Valid.t transfer /\
  Raw_context.Valid.t ctxt.
Proof.
  intros Ha H_storage_value H_ctxt H_storage_type.
  unfold Ticket_operations_diff.tickets_of_origination.
  eapply Error.split_letP. {
    now apply (@Ticket_scanner.type_has_tickets_is_valid a).
  }
  clear ctxt H_ctxt;
    intros [has_tickets_value ctxt] [H_has_tickets_value H0].
  destruct has_tickets_value as [ht ty_value] eqn:has_tickets_value_eq.
  destruct H0 as [H ty_value_eq].
  subst ty_value.
  eapply Error.split_letP. {
    symmetry in has_tickets_value_eq.
    specialize (@Ticket_scanner.tickets_of_value_is_valid a ctxt true
    (Ticket_scanner.Has_tickets ht storage_type)
    ht storage_type eq_refl) as Haux0.
    specialize (Haux0 Ha storage_value) as Haux1.
    apply Haux1 ; try assumption.
  }
  clear ctxt H; intros [tickets ctxt] [H_tickets H_ctxt].
  simpl.
  easy.
Qed.

(** The function [tickets_of_operation] is valid. *)
(** @TODO: redefine [Script_typed_ir.packed_internal_operation.Valid.t] *)
Lemma tickets_of_operation_is_valid ctxt operation :
  Raw_context.Valid.t ctxt ->
  (* Script_typed_ir.packed_internal_operation.Valid.t operation -> *)
  letP? '(transfer, ctxt) :=
    Ticket_operations_diff.tickets_of_operation ctxt
      operation in
  Option.Forall ticket_transfer.Valid.t transfer /\
  Raw_context.Valid.t ctxt.
Proof.
  (** We leave the former proof with simulations as a future hint. *)
  (* intros H_ctxt H_operation.
  unfold Ticket_operations_diff.tickets_of_operation.
  destruct operation as [destination ioc nonce]; simpl.
  destruct H_operation; simpl in *.
  destruct ioc eqn:?; simpl in *; try easy.
  { grep @With_family.Transaction_to_smart_contract.
    now apply Ticket_operations_diff.tickets_of_transaction_is_valid.
  }
  { grep @With_family.Transaction_to_tx_rollup.
    split; [|assumption].
    repeat try (constructor; simpl).
    now apply Tx_rollup_parameters.get_deposit_parameters_is_valid.
  }
  { grep @With_family.Transaction_to_sc_rollup.
    now apply Ticket_operations_diff.tickets_of_transaction_is_valid.
  }
  { grep @With_family.Origination.
    now apply Ticket_operations_diff.tickets_of_origination_is_valid.
  } *)
Admitted.

(** The function [add_transfer_to_token_map] is valid. *)
Lemma add_transfer_to_token_map_is_valid ctxt token_map transfer :
  Raw_context.Valid.t ctxt ->
  Token_map_destination_map.Valid.t token_map ->
  ticket_transfer.Valid.t transfer ->
  letP? '(map, ctxt) :=
    Ticket_operations_diff.add_transfer_to_token_map ctxt token_map transfer in
  Token_map_destination_map.Valid.t map /\
  Raw_context.Valid.t ctxt.
Proof.
  intros H_ctxt H_token_map H_transfer.
  unfold Ticket_operations_diff.add_transfer_to_token_map.
  destruct H_transfer.
  apply List.fold_left_e_is_valid with
    (P_b := Ticket_scanner.ex_ticket.Valid.t);
    try tauto.
  clear; intros [token_map ctxt] item [H_token_map H_ctxt] H_item.
  pose proof (Ticket_token.token_and_amount_of_ex_ticket_is_valid item)
    as H_token_and_amount.
  destruct Ticket_token.token_and_amount_of_ex_ticket.
  apply Ticket_operations_diff.Ticket_token_map.add_is_valid ; try assumption.
  { apply H_token_and_amount. assumption. }
  { apply H_token_and_amount in H_item. destruct H_item.
    assumption. }
Qed.

(** The function [ticket_token_map_of_operations] is valid. *)
Lemma ticket_token_map_of_operations_is_valid
  ctxt ops :
  Raw_context.Valid.t ctxt ->
  (* List.Forall Script_typed_ir.packed_internal_operation.Valid.t ops -> *)
  letP? '(map, ctxt) :=
    Ticket_operations_diff.ticket_token_map_of_operations ctxt ops in
  Token_map_destination_map.Valid.t map /\
  Raw_context.Valid.t ctxt.
Proof.
  (** We leave the former proof with simulations as a future hint. *)
  (* intros H_ctxt H_ops.
  unfold Ticket_operations_diff.ticket_token_map_of_operations.
  eapply List.fold_left_e_is_valid with
    (P_b := fun op =>
      exists dep_op,
      With_family.to_packed_internal_operation dep_op = op /\
      Script_typed_ir.internal_operation.Valid.t dep_op
    ).
  { clear ctxt H_ctxt; clear;
      intros [token_map ctxt] item;
      intros [H_token_map H_ctxt] [dep_op [H_dep_op_eq H_dep_op]].
    rewrite <- H_dep_op_eq.
    eapply Error.split_letP. {
      now apply tickets_of_operation_is_valid.
    }
    clear ctxt H_ctxt; intros [res ctxt] [H_res H_ctxt].
    destruct res; simpl in *; [|easy].
    now apply add_transfer_to_token_map_is_valid.
  }
  { split; [|assumption].
    apply Ticket_token_map.empty_is_valid.
  }
  { induction ops; simpl; inversion_clear H_ops; constructor.
    { now eexists; split; [reflexivity|]. }
    { now apply IHops. }
  } *)
Admitted.

(** The function [ticket_diffs_of_operations] is valid. *)
Lemma ticket_diffs_of_operations_is_valid
  ctxt ops :
  Raw_context.Valid.t ctxt ->
  (* List.Forall Script_typed_ir.packed_internal_operation.Valid.t ops -> *)
  letP? '(diffs, ctxt) :=
    Ticket_operations_diff.ticket_diffs_of_operations ctxt ops in
  List.Forall ticket_token_diff.Valid.t diffs /\
  Raw_context.Valid.t ctxt.
Proof.
  (** We leave the former proof with simulations as a future hint. *)
  intros H_ctxt.
  unfold Ticket_operations_diff.ticket_diffs_of_operations.
  eapply Error.split_letP. {
    now apply Ticket_operations_diff.ticket_token_map_of_operations_is_valid.
  }
  clear; intros [token_map ctxt] [H_token_map H_ctx].
  eapply Ticket_token_map.fold_is_valid;
    try exact H_token_map;
    try assumption;
    try constructor.
  clear; intros ctxt acc token value H_ctxt H_acc H_token H_value.
  eapply Error.split_letP. {
    eapply Carbonated_map.Make.fold_is_valid with
      (P_ctxt := Raw_context.Valid.t)
      (P_state := Ticket_amount.Valid.t);
      try apply H_value; (** HERE *)
      try easy.
    simpl.
    clear; intros ctxt total_amount ? amount H_ctxt H_total_amount ? H_amount.
    eapply Error.split_letP. {
      apply Raw_context.consume_gas_is_valid; try assumption.
      apply Ticket_costs.add_int_cost_is_valid.
    }
    clear ctxt H_ctxt; intros ctxt H_ctxt.
    simpl.
    split; try assumption.
    apply Ticket_amount.add_is_valid ; try assumption.
    (** We currently need to prove that [0] is strictly positive...
        The validity lemma for some [fold_left] function must probably be
        adapted to enable using validity conditions which are satisfied
        only *after* the first iteration of the loop.
    *)
      admit.
  }
  clear ctxt H_ctxt; intros [total_amount ctxt] [H_total_amount H_ctxt].
  eapply Error.split_letP. {
    apply Carbonated_map.Make.to_list_is_valid;
      try apply H_ctxt;
      try apply H_value.
  }
  clear ctxt H_ctxt; intros [destination ctxt] [H_destination H_ctxt].
  simpl.
  split; try assumption.
  repeat constructor; try easy; simpl.
  eapply List.Forall_impl; try apply H_destination.
Admitted.
