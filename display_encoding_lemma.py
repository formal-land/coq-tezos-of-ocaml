# To generate `src/Proto_alpha/encoding.md` (not run as it slows down the CI system)
import re
import pathlib
import itertools
import pandas as pd

def check_type(data, regexp):
    lines = data.split("\n")
    proof_chunk = []
    flag = 0
    sectional_list = {
        "PROVED": [],
        "ADMITTED": []
    }

    for line in lines:
        if flag == 0:
            t = re.search(regexp, line, re.M)
            if t is not None:
                proof_chunk.append(line)
                flag = 1
            else:
                continue
        elif flag == 1:
            if re.search("Qed", line):
                proof_chunk.append(line)
                sectional_list["PROVED"].append(proof_chunk[0])
                proof_chunk = []
                flag = 0
            elif re.search("Admitted", line):
                proof_chunk.append(line)
                sectional_list["ADMITTED"].append(proof_chunk[0])
                proof_chunk = []
                flag = 0
            else:
                proof_chunk.append(line)
        else:
            continue
    
    return sectional_list

def flatten(lst):
    return list(itertools.chain(*lst))

def get_regexp_in_file(file, regexp):
    proof_type = ["PROVED", "ADMITTED"]
    sectional_list = {
        "PROVED": [],
        "ADMITTED": []
    }

    with open(file) as f:
        data = f.read()
        proofs = check_type(data, regexp)
        # print(proofs)
        if len(proofs[proof_type[0]]) > 0:
            sectional_list[proof_type[0]] = proofs[proof_type[0]]
        elif len(proofs[proof_type[1]]) > 0:
            sectional_list[proof_type[1]] = proofs[proof_type[1]]
        else:
            pass
        f.close()
    
    return sectional_list

def get_occurrences_in_file_list(cwd, file_list, regexp):
    d = dict()
    for file in file_list:
        temp_dict = get_regexp_in_file(file, regexp)
        if len(temp_dict["PROVED"]) > 0 or len(temp_dict["ADMITTED"]) > 0:
            d[str(file.relative_to(str(cwd)))] = temp_dict
    return d


cwd = pathlib.Path.cwd()
lemma_path = cwd.joinpath("src").joinpath("Proto_alpha").joinpath("Proofs")

lemma_list = lemma_path.glob('*.v')


lemma_list = get_occurrences_in_file_list(cwd, lemma_list, r"Lemma.*encoding_is_valid*")


df = pd.DataFrame(lemma_list)
dfT = df.transpose()
output = dfT.to_markdown()

output_path = cwd.joinpath("src").joinpath("Proto_alpha").joinpath("encoding.md")
with open(output_path, "w") as out_file:
    out_file.write(output)
