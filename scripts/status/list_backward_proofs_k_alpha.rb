output = ""

# Interpreter
begin
  output << "## [Script_interpreter.v](/docs/proto_k_alpha/proofs/script_interpreter)\n"
  output << "### Proofs\n"

  proofs = File.read("src/Proto_K_alpha/Proofs/Script_interpreter.v")
  body = ""
  index = 0
  nb_successful = 0
  instructions = proofs.to_enum(:scan, /grep @OldIR\.With_family\.(\w+)\./).map {Regexp.last_match}

  # We check if an instruction is correct by checking if there is an "admit"
  # before the next instruction or end of the whole proof.
  for instruction in instructions do
    next_admit = instruction.post_match.match(/(admit)|(apply axiom)/)
    next_grep_or_end = instruction.post_match.match(/(grep)|(Admitted)|(Qed)/)
    next_admit_offset = next_admit && next_admit.offset(0)[0]
    next_grep_or_end_offset = next_grep_or_end && next_grep_or_end.offset(0)[0]
    index += 1
    is_complete =
      !next_admit_offset || next_admit_offset > next_grep_or_end_offset
    nb_successful += is_complete ? 1 : 0
    body << "#{index}. #{instruction[1]} #{is_complete ? "🟢" : "🔴"}\n"
  end

  $progress = 100 * nb_successful / index
  output << "#{$progress}%\n"
  output << body
  output << "\n"
end

File.open("src/Status/Proto_K_alpha_backward_compatibility.md", "w") do |file|
  file << output
end

# Output the score for the homepage
puts $progress
