# coding: utf-8
output = "The list of files together with the number of **(compare lemmas / compare definitions)**.\n"
output << "The script to generate this file is in [scripts/list_compare_functions_k.rb](https://gitlab.com/formal-land/coq-tezos-of-ocaml/-/blob/master/scripts/list_compare_functions_k.rb).\n\n"

Dir.glob("src/Proto_K/*.v").sort.each do |path|
  module_name = path["src/Proto_K/".size..-1]
  unless ["Environment_modules.v", "Michelson_v1_gas.v"].include?(module_name) then
    content = File.read(path)
    compares = content.scan(/Definition (compare\w*)\W/).map {|result| result[0]}
    if compares.size != 0 then
      proof_path = "src/Proto_K/Proofs/" + module_name
      proof_content = File.exists?(proof_path) ? File.read(proof_path) : ""
      compare_lemmas = proof_content.scan(/Lemma (compare\w*_is_valid)\W/).map {|result| result[0]}
      is_complete = compare_lemmas.size == compares.size
      color = is_complete ? "🟢" : "🔴"
      output << "#### #{color} [#{module_name}](/docs/proto_alpha/#{module_name.downcase[0..-3]}) (#{compare_lemmas.size} / #{compares.size})\n"
      output << "Compare definitions:\n"
      compares.each do |compare|
        output << "* `#{compare}`\n"
      end
      output << "\n"
      if compare_lemmas.size != 0 then
        output << "Compare lemmas:\n"
        compare_lemmas.each do |compare|
          output << "* `#{compare}`\n"
        end
        output << "\n"
      end
    end
  end
end

File.open("src/Status/compare_k.md", "w") do |file|
  file << output
end
