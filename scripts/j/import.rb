# Import the protocol using `coq-of-ocaml`
require 'parallel'

# Use mtime to decide what files to generate, useful during development. 
#
# * WARNING : THIS IS NOT SAFE IN GENERAL *
# 
# The changes in an ml file may propagate on the types of the translation
# of another files, so use it wisely and only during development.
# If you are in doubt you can remove the .json file to
# run the full translation again or unset the env variable
# COQ_TEZOS_OF_OCAML_IMPORT_USE_MTIME to disable it
def filter_by_mtime(files)
  require 'json'
  require 'time'
  mtime = files.map {|fil| [fil, File.stat(fil).mtime.to_i]}.to_h
  mtime_file = "#{ENV['HOME']}/.coq-tezos-of-ocaml.mtime.json"
  File.write(mtime_file, "{}") unless File.file?(mtime_file)
  oldmtime = JSON.parse(File.read(mtime_file))
  new_files = files.filter do |file|
    not oldmtime.key?(file) or mtime[file] > oldmtime[file]
  end
  File.write(mtime_file, mtime.to_json)
  new_files
end

if ARGV.size < 3 then
  puts "Usage:"
  puts "  ruby import.rb tezos_path protocol_path target_path"
  exit(1)
end

tezos_path, protocol_path, target_path = ARGV
full_protocol_path = File.join(tezos_path, protocol_path)

# Environment
ml_path = File.join(tezos_path, "_build", "default", "src", "lib_protocol_environment", "sigs", "v5.ml")
ml_content = File.read(ml_path, encoding: "UTF-8")
mli_content = ml_content.split("\n")[8..-2].select {|line| line[0] != "#"}.join("\n")
File.open("environment.mli", "w") do |file|
  file << mli_content
end
command = "coq-of-ocaml -config config-environment.json environment.mli"
system(command)

# Protocol
mli_files = [
]
protocol_files =
  Dir.glob(File.join(full_protocol_path, "*.ml")) +
  mli_files.map {|path| File.join(full_protocol_path, path)}

if ENV.key?('COQ_TEZOS_OF_OCAML_IMPORT_USE_MTIME')
  protocol_files = filter_by_mtime(protocol_files)
end

Parallel.each(protocol_files.sort) do |ocaml_file_name|
  command = "cd #{full_protocol_path} && coq-of-ocaml -config #{File.expand_path("config.json")} #{File.basename(ocaml_file_name)} "
  system(command) if command.include?("")
end

# Patching
system("ruby", "patch.rb", target_path, full_protocol_path)

# Copying
unless Dir.glob("#{full_protocol_path}/*.v").empty?
  system("rsync --checksum #{full_protocol_path}/*.v #{target_path}")
  system("test -n \"#{full_protocol_path}/*.v\" && rm #{full_protocol_path}/*.v")
end

# Renaming the environment
system("rsync --checksum Environment_mli.v #{File.join(target_path, "..", "Environment", "V5_modules.v")}")
system("rm Environment_mli.v")
