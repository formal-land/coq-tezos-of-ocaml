## This script generate the specification of the storage it
## generates two files from the `context` variable below, which
## is the specification of the storage. To update it you need
## to look at the `src/Proto_alpha/Storage.v` file and try to
## guess the fields:

## name: Usually the name of the module
## path: You will see a `let name := ["foo"; "bar"] in the Storage.v
##       the path is this list, in this case you would use `["foo",
##       "bar"]. This is used to construct the error messages, but at
##       the time of this writing it didn't impact the proofs so far,
##       if the name is not obvious you can put a guess and see where
##       it leads you to.
## value: The storage is simulated by maps (and other types for
##        simpler storages), this is the value type of the map
## type: This is the type of the storage, we have a finite set of
##       options here like, Indexed_data_storage, Single_data_storage.
##       Its value will determine what kind of simulation will be
##       used, it may be a map, a nested map a simple option, etc. If
##       it is "Sub-store" then the "stores" field needs to be a list
##       of stores, this is how we represent the storage nesting. If
##       it is "Indexed" the the simulation is a nested map and
##       "store" field will specify the type of the inner map.
## index: This is regarding the type of the key of the map, this is
##        required if the storage is indexed, i.e., if its simulation
##        is a map.
## skip: This optional key can be setted to a string and in this case
##       the generation of this store will be skipped and a TODO
##       comment will be inserted in the simulation with the contents
##       of the string of this key. The string should be a reason
##       telling why this was disabled.
## header: Some stores may depend on arbitrary code. You can add
##         arbitrary code in here (as a string) and it will be
##         inserted before the simulation definition in a way that you
##         can reference these definitions in `index` value, see the
##         `context` variable for examples


## How to generated the specification
## ----------------------------------

## ruby scripts/alpha/generate_context.rb \
##   -c src/Proto_alpha/Simulations/Raw_context_generated.v \
##   -s src/Proto_alpha/Proofs/Storage_generated.v


## How to update this file
## -----------------------

## If compilation is failing the first thing you can try is put "skip"
## in the stores that are failing. Next you should look at the
## `Storage.v` file and inspect if the stores that are failing
## changed, if they are there at all. You then use the contents
## `Storage.v` to update the `context` variable below run the script
## and try to compile, and repeat until everything is compiling again.
## For the `index` key usually is obvious what it should be but
## sometimes is not, you can trial and error here because if it's
## wrong it will not compile, the same should work for the `type` and
## `value` keys.
##
## This script will generate two files, one for the simulation of the
## storage which is a big record and other for the storage axioms.
##
## For the simulation all the code is in this script, it is simple, it
## will iterate over the `context` variable and accumulate a string
## that is written to a file at the end. If a new store type arrives
## in the protocol update we need to add code to generate the propper
## simulation. The entry point is the `context_of_json` function.
##
## For the axioms it will use ERB (Ruby templating) [1] to generate
## the axioms. The templates are at scripts/alpha/templates/storage.
## For each `type` in the `context` variable, we have one template
## which is named with the type that it represents. When `type` is
## `Indexed` we gather the template from the `Indexed/` folder. The
## class `StorageTemplate` will load these files and render it, then
## as in the simulated context we accumulate everything in a string
## and write to a file at the end. If a new type of store comes up in
## the protocol update we need to write a new template. The easier way
## is to do the first by hand in the `Storage_generated.v` file, make
## sure it compiles, then copy and paste in the `.erb` file and
## replace the names with the `<%= foo =>` tags, where `foo` is a
## method in the `StorageTemplate` class. The entry point is for the
## storage generation is the `storage_axioms_content` function.
##
## [1] https://docs.ruby-lang.org/en/2.7.0/ERB.html

require 'erb'
require 'optparse'
require 'stringio'

## The specification of the storage
context = [
  {
    name: "Block_round",
    type: "Simple_single_data_storage",
    value: "Round_repr.t",
    path: ["block_round"],
  },
  {
    name: "Tenderbake",
    type: "Sub-store",
    stores: [
      {
        name: "First_level_of_protocol",
        type: "Single_data_storage",
        value: "Raw_level_repr.t",
        path: ["first_level_of_protocol"]
      },
      {
        name: "Endorsement_branch",
        type: "Single_data_storage",
        value: "Storage.Tenderbake.Branch.t",
        path: ["endorsement_branch"]
      },
      {
        name: "Grand_parent_branch",
        type: "Single_data_storage",
        value: "Storage.Tenderbake.Branch.t",
        path: ["grand_parent_branch"]
      }
    ]
  },
  {
    name: "Contract",
    type: "Sub-store",
    stores: [
      {
        name: "Global_counter",
        type: "Simple_single_data_storage",
        value: "Z.t",
        path: ["global_counter"],
      },
      {
        name: "Spendable_balance",
        type: "Indexed_data_storage_with_local_context",
        index: "(of_description_index Contract_repr.Index)",
        value: "Tez_repr.t",
        path: ["balance"],
      },
      {
        name: "Missed_endorsements",
        type: "Indexed_data_storage",
        index: "(of_description_index Contract_repr.Index)",
        value: "Storage.Missed_endorsements_info.t",
        path: ["missed_endorsements"]
      },
      {
        name: "Manager",
        type: "Indexed_data_storage_with_local_context",
        index: "(of_description_index Contract_repr.Index)",
        value: "Manager_repr.t",
        path: ["manager"]
      },
      {
        name: "Consensus_key",
        type: "Indexed_data_storage",
        index: "(of_description_index Contract_repr.Index)",
        value: "Signature.public_key",
        path: ["consensus_keys", "active"],
      },
      {
        name: "Pending_consensus_keys",
        type: "Indexed",
        index: "(of_description_index Contract_repr.Index)",
        path: ["consensus_key", "pendings"],
        store: {
          type: "Indexed_data_storage",
          index: "(of_description_index Cycle_repr.Index)",
          value: "Signature.public_key"
        }
      },
      {
        name: "Delegate",
        type: "Indexed_data_storage",
        index: "(of_description_index Contract_repr.Index)",
        value: "Signature.public_key_hash",
        path: ["delegate"]
      },
      {
        name: "Inactive_delegate",
        type: "Data_set_storage",
        index: "(of_description_index Contract_repr.Index)",
        path: ["inactive_delegate"],
      },
      {
        name: "Delegate_last_cycle_before_deactivation",
        type: "Indexed_data_storage",
        index: "(of_description_index Contract_repr.Index)",
        value: "Cycle_repr.t",
        path: ["delegate_desactivation"]
      },
      {
        name: "Delegated",
        type: "Indexed",
        index: "(of_description_index Contract_repr.Index)",
        path: ["delegated"],
        store: {
          type: "Data_set_storage",
          index: "(of_description_index Contract_repr.Index)"
        }
      },
      {
        name: "Counter",
        type: "Indexed_data_storage_with_local_context",
        index: "(of_description_index Contract_repr.Index)",
        value: "Z.t",
        path: ["counter"]
      },
      {
        name: "Code",
        type: "Non_iterable_indexed_carbonated_data_storage",
        index: "(of_description_index Contract_repr.Index)",
        value: "Script_repr.lazy_expr",
        path: ["code"]
      },
      {
        name: "Storage",
        type: "Non_iterable_indexed_carbonated_data_storage",
        index: "(of_description_index Contract_repr.Index)",
        value: "Script_repr.lazy_expr",
        path: ["storage"]
      },
      {
        name: "Paid_storage_space",
        type: "Indexed_data_storage",
        index: "(of_description_index Contract_repr.Index)",
        value: "Z.t",
        path: ["paid_bytes"]
      },
      {
        name: "Used_storage_space",
        type: "Indexed_data_storage",
        index: "(of_description_index Contract_repr.Index)",
        value: "Z.t",
        path: ["used_bytes"]
      },
      {
        name: "Frozen_deposits",
        type: "Indexed_data_storage",
        index: "(of_description_index Contract_repr.Index)",
        value: "Storage.Deposits.t",
        path: ["frozen_deposits"]
      },
      {
        name: "Frozen_bonds",
        type: "Indexed",
        index: "(of_description_index Contract_repr.Index)",
        path: ["frozen_bonds"],
        store: {
          type: "Non_iterable_indexed_carbonated_data_storage",
          index: "(of_description_index Bond_id_repr.Index)",
          value: "Tez_repr.t",
        }
      },
      {
        name: "Total_frozen_bonds",
        type: "Indexed_data_storage",
        index: "(of_description_index Contract_repr.Index)",
        value: "Tez_repr.t",
        path: ["total_frozen_bonds"]
      },
    ],
  },
  {
    name: "Global_constants",
    type: "Sub-store",
    stores: [
      {
        name: "Map",
        type: "Non_iterable_indexed_carbonated_data_storage",
        index: "Script_expr_hash.Path_encoding_Make_hex_include",
        value: "Script_repr.expr",
        path: ["global_constant"],
      },
    ]
  },
  {
    name: "Big_map",
    type: "Sub-store",
    stores: [
      {
        name: "Next",
        type: "Sub-store",
        stores: [{
          name: "Storage",
          type: "Single_data_storage",
          value: "Z.t",
          path: ["next"],
        }]
      },
      {
        name: "Total_bytes",
        type: "Indexed_data_storage_with_local_context",
        index: "(of_lazy_storage_kind_id_index Storage.Big_map.Index)",
        value: "Z.t",
        path: ["total_bytes"]
      },
      {
        name: "Key_type",
        type: "Indexed_data_storage",
        index: "(of_lazy_storage_kind_id_index Storage.Big_map.Index)",
        value: "Script_repr.expr",
        path: ["key_type"]
      },
      {
        name: "Value_type",
        type: "Indexed_data_storage",
        index: "(of_lazy_storage_kind_id_index Storage.Big_map.Index)",
        value: "Script_repr.expr",
        path: ["value_type"]
      },
      {
        name: "Contents",
        type: "Indexed",
        path: ["contents"],
        index: "(of_lazy_storage_kind_id_index Storage.Big_map.Index)",
        store: {
          type: "Indexed_carbonated_data_storage",
          index: "Script_expr_hash.Path_encoding_Make_hex_include",
          value: "Script_repr.expr",
        }
      },
    ],
  },
  {
    name: "Sapling",
    type: "Sub-store",
    stores: [
      {
        name: "Next",
        type: "Sub-store",
        stores: [{
          name: "Storage",
          type: "Single_data_storage",
          value: "Z.t",
          path: ["next"]
        }],
      },
      {
        name: "Total_bytes",
        type: "Indexed_data_storage_with_local_context",
        index: "(of_lazy_storage_kind_id_index Storage.Sapling.Index)",
        value: "Z.t",
        path: ["total_bytes"],
      },
      {
        name: "Commitments_size",
        type: "Indexed",
        path: ["commitment_size"],
        index: "(of_lazy_storage_kind_id_index Storage.Sapling.Index)",
        store: {
          type: "Single_data_storage",
          value: "Int64.t"
        }
      },
      {
        name: "Memo_size",
        path: ["memo_size"],
        type: "Indexed",
        index: "(of_lazy_storage_kind_id_index Storage.Sapling.Index)",
        store: {
          type: "Single_data_storage",
          value: "Sapling_repr.Memo_size.t",
        }
      },
      {
        name: "Commitments",
        type: "Indexed",
        index: "(of_functors_index Sapling_Commitments_Index)",
        path: ["commitments"],
        store: {
          type: "Non_iterable_indexed_carbonated_data_storage",
          index: "(of_lazy_storage_kind_id_index Storage.Sapling.Index)",
          value: "Sapling.Hash.t",
        },
        header: <<END,
Definition Sapling_Commitments_Index := (Storage.Make_index
  (let t : Set := int64 in
  let rpc_arg :=
    let construct := Int64.to_string in
    let destruct (hash_value : string)
      : Pervasives.result int64 string :=
      Result.of_option "Cannot parse node position"
        (Int64.of_string_opt hash_value) in
    RPC_arg.make
      (Some "The position of a node in a sapling commitment tree")
      "sapling_node_position" destruct construct tt in
  let encoding :=
    Data_encoding.def "sapling_node_position"
      (Some "Sapling node position")
      (Some "The position of a node in a sapling commitment tree")
      Data_encoding.int64_value in
  let compare := Compare.Int64.(Compare.S.compare) in
  let path_length := return? 1 in
  let to_path (c_value : int64) (l_value : list string) : list string :=
    cons (Int64.to_string c_value) l_value in
  let of_path (function_parameter : list string) : M? option int64 :=
    match function_parameter with
    | cons c_value [] => return? Int64.of_string_opt c_value
    | _ => return? None
    end in
  {|
    Storage_description.INDEX.to_path := to_path;
    Storage_description.INDEX.of_path := of_path;
    Storage_description.INDEX.path_length := path_length;
    Storage_description.INDEX.rpc_arg := rpc_arg;
    Storage_description.INDEX.encoding := encoding;
    Storage_description.INDEX.compare := compare
  |})).
END
      },
      {
        name: "Ciphertexts",
        type: "Indexed",
        index: "(of_functors_index Sapling_Ciphertexts_Index)",
        path: ["ciphertexts"],
        store: {
          type: "Non_iterable_indexed_carbonated_data_storage",
          index: "(of_lazy_storage_kind_id_index Storage.Sapling.Index)",
          value: "Sapling.Ciphertext.t",
        },
        header: <<END,
Definition Sapling_Ciphertexts_Index := Storage.Make_index
  (let t : Set := int64 in
  let rpc_arg :=
    let construct := Int64.to_string in
    let destruct (hash_value : string)
      : Pervasives.result int64 string :=
      Result.of_option "Cannot parse ciphertext position"
        (Int64.of_string_opt hash_value) in
    RPC_arg.make (Some "The position of a sapling ciphertext")
      "sapling_ciphertext_position" destruct construct tt in
  let encoding :=
    Data_encoding.def "sapling_ciphertext_position"
      (Some "Sapling ciphertext position")
      (Some "The position of a sapling ciphertext")
      Data_encoding.int64_value in
  let compare := Compare.Int64.(Compare.S.compare) in
  let path_length := return? 1 in
  let to_path (c_value : int64) (l_value : list string) : list string :=
    cons (Int64.to_string c_value) l_value in
  let of_path (function_parameter : list string) : M? option int64 :=
    match function_parameter with
    | cons c_value [] => return? Int64.of_string_opt c_value
    | _ => return? None
    end in
  {|
    Storage_description.INDEX.to_path := to_path;
    Storage_description.INDEX.of_path := of_path;
    Storage_description.INDEX.path_length := path_length;
    Storage_description.INDEX.rpc_arg := rpc_arg;
    Storage_description.INDEX.encoding := encoding;
    Storage_description.INDEX.compare := compare
  |}).
END
      },
      {
        name: "Nullifiers_size",
        type: "Indexed",
        index: "(of_lazy_storage_kind_id_index Storage.Sapling.Index)",
        path: ["nullifiers_size"],
        store: {
          type: "Single_data_storage",
          value: "Int64.t",
        }
      },
      {
        name: "Nullifiers_ordered",
        type: "Indexed",
        index: "(of_functors_index Sapling_Nullifier_Index)",
        path: ["nullifiers_ordered"],
        store: {
          type: "Non_iterable_indexed_data_storage",
          index: "(of_lazy_storage_kind_id_index Storage.Sapling.Index)",
          value: "Sapling.Nullifier.t",
        },
        header: <<END,
Definition Sapling_Nullifier_Index := Storage.Make_index
  (let t : Set := int64 in
  let rpc_arg :=
    let construct := Int64.to_string in
    let destruct (hash_value : string)
      : Pervasives.result int64 string :=
      Result.of_option "Cannot parse nullifier position"
        (Int64.of_string_opt hash_value) in
    RPC_arg.make (Some "A sapling nullifier position")
      "sapling_nullifier_position" destruct construct tt in
  let encoding :=
    Data_encoding.def "sapling_nullifier_position"
      (Some "Sapling nullifier position")
      (Some "Sapling nullifier position") Data_encoding.int64_value in
  let compare := Compare.Int64.(Compare.S.compare) in
  let path_length := return? 1 in
  let to_path (c_value : int64) (l_value : list string) : list string :=
    cons (Int64.to_string c_value) l_value in
  let of_path (function_parameter : list string) : M? option int64 :=
    match function_parameter with
    | cons c_value [] => return? Int64.of_string_opt c_value
    | _ => return? None
    end in
  {|
    Storage_description.INDEX.to_path := to_path;
    Storage_description.INDEX.of_path := of_path;
    Storage_description.INDEX.path_length := path_length;
    Storage_description.INDEX.rpc_arg := rpc_arg;
    Storage_description.INDEX.encoding := encoding;
    Storage_description.INDEX.compare := compare
  |}).
END
      },
      {
        name: "Nullifiers_hashed",
        type: "Indexed",
        index: "(of_lazy_storage_kind_id_index Storage.Sapling.Index)",
        path: ["nullifiers_hashed"],
        store: {
          type: "Carbonated_data_set_storage",
          index: "(of_functors_index Sapling_Nullifiers_hashed_Index)",
          value: "Sapling.Nullifier.t",
        },
        header: <<END,
Definition Sapling_Nullifiers_hashed_Index := (Storage.Make_index
  (let t : Set := Sapling.Nullifier.t in
  let encoding := Sapling.Nullifier.encoding in
  let of_string (hexstring : string)
    : Pervasives.result Sapling.Nullifier.t string :=
    Result.of_option "Cannot parse sapling nullifier"
      (Option.bind (Hex.to_bytes (Hex.Hex hexstring))
        (Data_encoding.Binary.of_bytes_opt encoding)) in
  let to_string (nf : Sapling.Nullifier.t) : string :=
    let b_value := Data_encoding.Binary.to_bytes_exn None encoding nf in
    let 'Hex.Hex hexstring := Hex.of_bytes None b_value in
    hexstring in
  let rpc_arg :=
    RPC_arg.make (Some "A sapling nullifier") "sapling_nullifier"
      of_string to_string tt in
  let compare := Sapling.Nullifier.compare in
  let path_length := return? 1 in
  let to_path (c_value : Sapling.Nullifier.t) (l_value : list string)
    : list string :=
    cons (to_string c_value) l_value in
  let of_path (function_parameter : list string)
    : M? option Sapling.Nullifier.t :=
    match function_parameter with
    | cons c_value [] => return? Result.to_option (of_string c_value)
    | _ => return? None
    end in
  {|
    Storage_description.INDEX.to_path := to_path;
    Storage_description.INDEX.of_path := of_path;
    Storage_description.INDEX.path_length := path_length;
    Storage_description.INDEX.rpc_arg := rpc_arg;
    Storage_description.INDEX.encoding := encoding;
    Storage_description.INDEX.compare := compare
  |})).
END
      },
      {
        name: "Roots",
        type: "Indexed",
        index: "(of_functors_index Sapling_Roots_Index)",
        path: ["roots"],
        store: {
          type: "Non_iterable_indexed_data_storage",
          index: "(of_lazy_storage_kind_id_index Storage.Sapling.Index)",
          value: "Sapling.Hash.t",
        },
        header: <<END,
Definition Sapling_Roots_Index := (Storage.Make_index
  (let t : Set := int32 in
  let rpc_arg :=
    let construct := Int32.to_string in
    let destruct (hash_value : string)
      : Pervasives.result int32 string :=
      Result.of_option "Cannot parse nullifier position"
        (Int32.of_string_opt hash_value) in
    RPC_arg.make (Some "A sapling root") "sapling_root" destruct
      construct tt in
  let encoding :=
    Data_encoding.def "sapling_root" (Some "Sapling root")
      (Some "Sapling root") Data_encoding.int32_value in
  let compare := Compare.Int32.(Compare.S.compare) in
  let path_length := return? 1 in
  let to_path (c_value : int32) (l_value : list string) : list string :=
    cons (Int32.to_string c_value) l_value in
  let of_path (function_parameter : list string) : M? option int32 :=
    match function_parameter with
    | cons c_value [] => return? Int32.of_string_opt c_value
    | _ => return? None
    end in
  {|
    Storage_description.INDEX.to_path := to_path;
    Storage_description.INDEX.of_path := of_path;
    Storage_description.INDEX.path_length := path_length;
    Storage_description.INDEX.rpc_arg := rpc_arg;
    Storage_description.INDEX.encoding := encoding;
    Storage_description.INDEX.compare := compare
  |})).
END
      },
      {
        name: "Roots_pos",
        type: "Indexed",
        index: "(of_lazy_storage_kind_id_index Storage.Sapling.Index)",
        path: ["roots_pos"],
        store: {
          type: "Single_data_storage",
          value: "Int32.t",
        }
      },
      {
        name: "Roots_level",
        type: "Indexed",
        index: "(of_lazy_storage_kind_id_index Storage.Sapling.Index)",
        path: ["roots_level"],
        store: {
          type: "Single_data_storage",
          value: "Raw_level_repr.t",
        }
      },
    ],
  },
  {
    name: "Delegates",
    type: "Data_set_storage",
    index: "(of_functors_index Storage.Public_key_hash_index)",
    path: ["delegates"],
  },
  {
    name: "Consensus_keys",
    type: "Data_set_storage",
    index: "(of_functors_index Storage.Public_key_hash_index)",
    path: ["consensus_keys"],
  },
  {
    name: "Cycle",
    type: "Sub-store",
    stores: [
      {
        name: "Slashed_deposits",
        type: "Indexed",
        path: ["slashed_deposits"],
        index: "(of_description_index Cycle_repr.Index)",
        store: {
          type: "Indexed_data_storage",
          index: "(of_functors_index (Storage_functors.Pair (Storage.Make_index Raw_level_repr.Index) Storage.Public_key_hash_index))",
          value: "Storage.Slashed_level.t",
        }
      },
      {
        name: "Selected_stake_distribution",
        type: "Indexed_data_storage_with_local_context",
        value: "(list (Signature.public_key_hash * Tez_repr.t))",
        index: "(of_description_index Cycle_repr.Index)",
        path: ["selected_stake_distribution"],
      },
      {
        name: "Total_active_stake",
        type: "Indexed_data_storage",
        value: "Tez_repr.t",
        index: "(of_description_index Cycle_repr.Index)",
        path: ["total_active_stake"],
      },
      {
        name: "Delegate_sampler_state",
        type: "Indexed_data_storage",
        index: "(of_description_index Cycle_repr.Index)",
        value: "(Sampler.t Raw_context.consensus_pk)",
        path: ["delegate_sampler_state"]
      },
      {
        name: "Nonce",
        type: "Indexed",
        index: "(of_description_index Raw_level_repr.Index)",
        path: ["nonces"],
        store: {
          type: "Indexed_data_storage",
          index: "(of_description_index Cycle_repr.Index)",
          value: "Storage.Seed.nonce_status",
        },
      },
      {
        name: "Seed",
        type: "Indexed_data_storage_with_local_context",
        index: "(of_description_index Cycle_repr.Index)",
        value: "Seed_repr.seed",
        path: ["random_seed"],
      }
    ],
  },
  {
    name: "Stake",
    type: "Sub-store",
    stores: [
      {
        name: "Staking_balance",
        type: "Indexed_data_snapshotable_storage",
        index: "(of_functors_index Storage.Public_key_hash_index)",
        value: "Tez_repr.t",
        path: ["staking_balance"],
      },
      {
        name: "Active_delegates_with_minimal_stake",
        type: "Indexed_data_snapshotable_storage",
        index: "(of_functors_index Storage.Public_key_hash_index)",
        value: "unit",
        path: ["active_delegate_with_one_roll"],
      },
      {
        name: "Last_snapshot",
        type: "Single_data_storage",
        value: "Storage.Encoding.UInt16.t",
        path: ["last_snapshot"],
      },
    ],
  },
  {
    name: "Vote",
    type: "Sub-store",
    stores: [
      {
        name: "Pred_period_kind",
        type: "Single_data_storage",
        value: "Voting_period_repr.kind",
        path: ["pred_period_kind"],
      },
      {
        name: "Current_period",
        type: "Single_data_storage",
        value: "Voting_period_repr.t",
        path: ["current_period"],
      },
      {
        name: "Participation_ema",
        type: "Single_data_storage",
        value: "Int32.t",
        path: ["participation_ema"],
      },
      {
        name: "Current_proposal",
        type: "Single_data_storage",
        value: "Protocol_hash.t",
        path: ["current_proposal"],
      },
      {
        name: "Voting_power_in_listings",
        type: "Single_data_storage",
        value: "Int64.t",
        path: ["voting_power_in_listings"],
      },
      {
        name: "Listings",
        type: "Indexed_data_storage",
        index: "(of_functors_index Storage.Public_key_hash_index)",
        value: "Int64.t",
        path: ["listings"],
      },
      {
        name: "Proposals",
        type: "Data_set_storage",
        value: "Storage.Protocol_hash.t",
        index: "(of_functors_index Voting_Proposals_Index)",
        path: ["proposals"],
        header: <<END,
Definition Voting_Proposals_Index := (Storage_functors.Pair
  (Storage.Make_index
    {|
      Storage_description.INDEX.to_path :=
        Storage.Protocol_hash_with_path_encoding.to_path;
      Storage_description.INDEX.of_path :=
        Storage.Protocol_hash_with_path_encoding.of_path;
      Storage_description.INDEX.path_length :=
        Storage.Protocol_hash_with_path_encoding.path_length;
      Storage_description.INDEX.rpc_arg :=
        Storage.Protocol_hash_with_path_encoding.rpc_arg;
      Storage_description.INDEX.encoding :=
        Storage.Protocol_hash_with_path_encoding.encoding;
      Storage_description.INDEX.compare :=
        Storage.Protocol_hash_with_path_encoding.compare
    |}) Storage.Public_key_hash_index).
END
      },
      {
        name: "Proposals_count",
        type: "Indexed_data_storage",
        index: "(of_functors_index Storage.Public_key_hash_index)",
        value: "Storage.Encoding.UInt16.t",
        path: ["proposals_count"],
      },
      {
        name: "Ballots",
        type: "Indexed_data_storage",
        index: "(of_functors_index Storage.Public_key_hash_index)",
        value: "Vote_repr.ballot",
        path: ["ballots"],
      },
    ],
  },
  {
    name: "Seed_status",
    type: "Single_data_storage",
    value: "Seed_repr.seed_status",
    path: ["seed_status"],
  },
  {
    name: "Seed",
    type: "Sub-store",
    stores: [
      {
        # This is a custom module inside Storage.v we need a template
        # to be able to simulate it
        name: "Nonce",
        type: "Non_iterable_indexed_data_storage",
        value: "Storage.Seed.nonce_status",
        index: "(of_description_index Cycle_repr.Index)",
        skip: "Custom storage module",
        path: ["nonces"]
      },
      {
        name: "VDF_setup",
        type: "Single_data_storage",
        value: "Seed_repr.vdf_setup",
        path: ["vdf_challenge"],
      },
    ]
  },
  {
    name: "Commitments",
    type: "Indexed_data_storage",
    index: "(of_description_index Blinded_public_key_hash.Index)",
    value: "Tez_repr.t",
    path: ["commitments"],
  },
  {
    name: "Ramp_up",
    type: "Sub-store",
    stores: [
      {
        name: "Rewards",
        type: "Indexed_data_storage",
        index: "(of_description_index Cycle_repr.Index)",
        value: "Storage.Ramp_up.reward",
        path: ["ramp_up", "rewards"]
      }
    ]
  },
  {
    name: "Pending_migration",
    type: "Sub-store",
    stores: [
      {
        name: "Balance_updates",
        type: "Single_data_storage",
        value: "Receipt_repr.balance_updates",
        path: ["pending_migration_balance_updates"],
      },
      {
        name: "Operation_results",
        type: "Single_data_storage",
        value: "(list Migration_repr.origination_result)",
        path: ["pending_migration_operation_results"],
      },
    ]
  },
  {
    name: "Liquidity_baking",
    type: "Sub-store",
    stores: [
      {
        name: "Toggle_ema",
        type: "Single_data_storage",
        value: "Int32.t",
        path: ["liquidity_baking_escape_ema"]
      },
      {
        name: "Cpmm_address",
        type: "Single_data_storage",
        value: "Contract_hash.t",
        path: ["liquidity_baking_cpmm_address"]
      },
    ]
  },
  {
    name: "Ticket_balance",
    type: "Sub-store",
    stores: [
      {
        name: "Paid_storage_space",
        type: "Single_data_storage",
        value: "Z.t",
        path: ["paid_bytes"],
      },
      {
        name: "Used_storage_space",
        type: "Single_data_storage",
        value: "Z.t",
        path: ["used_bytes"],
      },
      {
        name: "Table",
        type: "Non_iterable_indexed_carbonated_data_storage",
        index: "(of_functors_index Storage.Ticket_balance.Index)",
        value: "Z.t",
        path: ["table"],
      },
    ]
  },
  {
    name: "Tx_rollup",
    type: "Sub-store",
    stores: [
      {
        name: "State",
        type: "Non_iterable_indexed_carbonated_data_storage",
        index: "(of_description_index Tx_rollup_repr.Index)",
        value: "Tx_rollup_state_repr.t",
        path: ["tx_rollup"],
      },
      {
        name: "Inbox",
        type: "Indexed",
        index: "(of_description_index Tx_rollup_repr.Index)",
        path: ["inbox"],
        store: {
          type: "Non_iterable_indexed_carbonated_data_storage",
          index: "(of_description_index Tx_rollup_level_repr.Index)",
          value: "Tx_rollup_inbox_repr.t",
        }
      },
      {
        name: "Revealed_withdrawals",
        type: "Indexed",
        path: ["withdrawals"],
        index: "(of_description_index Tx_rollup_repr.Index)",
        store: {
          type: "Non_iterable_indexed_carbonated_data_storage",
          index: "(of_description_index Tx_rollup_level_repr.Index)",
          value: "Bitset.t",
        }
      },
      {
        name: "Commitment",
        type: "Indexed",
        index: "(of_description_index Tx_rollup_repr.Index)",
        path: ["commitment"],
        store: {
          type: "Non_iterable_indexed_carbonated_data_storage",
          index: "(of_description_index Tx_rollup_level_repr.Index)",
          value: "Tx_rollup_commitment_repr.Submitted_commitment.t",
        }
      },
      {
        name: "Commitment_bond",
        type: "Indexed",
        index: "(of_description_index Tx_rollup_repr.Index)",
        path: ["commitment"],
        store: {
          type: "Non_iterable_indexed_carbonated_data_storage",
          index: "(of_functors_index Storage.Public_key_hash_index)",
          value: "int",
        }
      },
    ]
  },
  {
    name: "Sc_rollup",
    type: "Sub-store",
    stores: [
      {
        name: "PVM_kind",
        type: "Non_iterable_indexed_carbonated_data_storage",
        index: "(of_description_index Sc_rollup_repr.Index)",
        value: "Sc_rollups.Kind.t",
        path: ["kind"],
      },
      {
        name: "Parameters_type",
        type: "Non_iterable_indexed_carbonated_data_storage",
        index: "(of_description_index Sc_rollup_repr.Index)",
        value: "Script_repr.lazy_expr",
        path: ["parameters_type"],
      },
      {
        name: "Genesis_info",
        type: "Non_iterable_indexed_carbonated_data_storage",
        index: "(of_description_index Sc_rollup_repr.Index)",
        value: "Sc_rollup_commitment_repr.genesis_info",
        path: ["genesis_info"],
      },
      {
        name: "Inbox",
        type: "Single_data_storage",
        value: "Sc_rollup_inbox_repr.t",
        path: ["sc_rollup_inbox"],
      },
      {
        name: "Last_cemented_commitment",
        type: "Non_iterable_indexed_carbonated_data_storage",
        index: "(of_description_index Sc_rollup_repr.Index)",
        value: "Sc_rollup_commitment_repr.Hash.t",
        path: ["last_cemented_commitment"],
      },
      {
        name: "Stakers",
        type: "Indexed",
        index: "(of_description_index Sc_rollup_repr.Index)",
        path: ["stakers"],
        store: {
          type: "Indexed_carbonated_data_storage",
          index: "(of_functors_index Storage.Public_key_hash_index)",
          value: "Sc_rollup_commitment_repr.Hash.t",
        },
      },
      {
        name: "Staker_count",
        type: "Non_iterable_indexed_carbonated_data_storage",
        index: "(of_description_index Sc_rollup_repr.Index)",
        value: "int32",
        path: ["staker_count"],
      },
      {
        name: "Commitments",
        type: "Indexed",
        index: "(of_description_index Sc_rollup_repr.Index)",
        path: ["commitments"],
        store: {
          type: "Non_iterable_indexed_carbonated_data_storage",
          index: "Sc_rollup_commitment_repr.Hash.Path_encoding_Make_hex_include",
          value: "Sc_rollup_commitment_repr.t",
        }
      },
      {
        name: "Commitment_stake_count",
        type: "Indexed",
        index: "(of_description_index Sc_rollup_repr.Index)",
        path: ["commitment_stake_count"],
        store: {
          type: "Non_iterable_indexed_carbonated_data_storage",
          index: "Sc_rollup_commitment_repr.Hash.Path_encoding_Make_hex_include",
          value: "int32",
        }
      },
      {
        name: "Commitment_first_publication_level",
        type: "Indexed",
        index: "(of_description_index Sc_rollup_repr.Index)",
        path: ["commitment_first_publication_level"],
        store: {
          index: "(of_description_index Raw_level_repr.Index)",
          type: "Non_iterable_indexed_carbonated_data_storage",
          value: "Raw_level_repr.t",
        }
      },
      {
        name: "Commitment_added",
        type: "Indexed",
        index: "(of_description_index Sc_rollup_repr.Index)",
        path: ["commitment_added"],
        store: {
          index: "Sc_rollup_commitment_repr.Hash.Path_encoding_Make_hex_include",
          type: "Non_iterable_indexed_carbonated_data_storage",
          value: "Raw_level_repr.t",
        }
      },
      {
        name: "Game_versioned",
        type: "Indexed",
        index: "(of_description_index Sc_rollup_repr.Index)",
        path: ["game_versioned"],
        store: {
          type: "Indexed_carbonated_data_storage",
          index: "(of_functors_index Sc_rollup_Game_versioned_and_timeout_Index)",
          value: "Sc_rollup_game_repr.versioned",
        },
        header: <<END
Definition Sc_rollup_Game_versioned_and_timeout_Index := (Storage.Make_index
  {|
    Storage_description.INDEX.to_path := Sc_rollup_game_repr.Index.to_path;
    Storage_description.INDEX.of_path := Sc_rollup_game_repr.Index.of_path;
    Storage_description.INDEX.path_length :=
      Sc_rollup_game_repr.Index.path_length;
    Storage_description.INDEX.rpc_arg := Sc_rollup_game_repr.Index.rpc_arg;
    Storage_description.INDEX.encoding :=
      Sc_rollup_game_repr.Index.encoding;
    Storage_description.INDEX.compare := Sc_rollup_game_repr.Index.compare
  |}).
END
      },
      {
        name: "Game",
        type: "Indexed",
        index: "(of_description_index Sc_rollup_repr.Index)",
        path: ["game"],
        store: {
          type: "Non_iterable_indexed_carbonated_data_storage",
          index: "(of_functors_index Sc_rollup_Game_versioned_and_timeout_Index)",
          value: "Sc_rollup_game_repr.t",
        }
      },
      {
        name: "Game_timeout",
        type: "Indexed",
        index: "(of_description_index Sc_rollup_repr.Index)",
        path: ["game_timeout"],
        store: {
          type: "Non_iterable_indexed_carbonated_data_storage",
          index: "(of_functors_index Sc_rollup_Game_versioned_and_timeout_Index)",
          value: "Sc_rollup_game_repr.timeout",
        },
      },
      {
        name: "Opponent",
        type: "Indexed",
        index: "(of_description_index Sc_rollup_repr.Index)",
        path: ["opponent"],
        store: {
          type: "Non_iterable_indexed_carbonated_data_storage",
          index: "(of_functors_index Storage.Public_key_hash_index)",
          value: "Signature.public_key_hash",
        }
      },
      {
        name: "Applied_outbox_messages",
        type: "Indexed",
        index: "(of_description_index Sc_rollup_repr.Index)",
        path: ["applied_outbox_messages"],
        store: {
          type: "Non_iterable_indexed_carbonated_data_storage",
          index: "(of_description_index Storage.Sc_rollup.Level_index)",
          value: "Storage.Sc_rollup.Bitset_and_level.t",
        }
      },
    ],
  },
  {
    name: "Dal",
    type: "Sub-store",
    stores: [
      {
        name: "Slot",
        type: "Sub-store",
        stores: [
          {
            name: "Headers",
            type: "Non_iterable_indexed_data_storage",
            index: "(of_description_index Raw_level_repr.Index)",
            path: ["slot_headers"],
            value: "(list Dal_slot_repr.Header.t)",
          },
          {
            name: "History",
            type: "Single_data_storage",
            path: ["slot_headers_history"],
            value: "Dal_slot_repr.History.t",
          },
        ]
      },
    ],
  },
  {
    name: "Zk_rollup",
    type: "Sub-store",
    stores: [
      {
        name: "Account",
        type: "Non_iterable_indexed_carbonated_data_storage",
        index: "(of_description_index Zk_rollup_repr.Index)",
        value: "Zk_rollup_account_repr.t",
        path: ["account"],
      },
      {
        name: "Pending_list",
        type: "Non_iterable_indexed_carbonated_data_storage",
        index: "(of_description_index Zk_rollup_repr.Index)",
        value: "Zk_rollup_repr.pending_list",
        path: ["pending_list"],
      },
      {
        name: "Pending_operation",
        type: "Indexed",
        index: "(of_description_index Zk_rollup_repr.Index)",
        path: ["pending_operations"],
        store: {
          type: "Non_iterable_indexed_carbonated_data_storage",
          index: "(of_functors_index Zk_rollup_Pending_operation_Index)",
          value: "(Zk_rollup_operation_repr.t * option Ticket_hash_repr.t)",
        },
        header: <<END,
Definition Zk_rollup_Pending_operation_Index := (Storage.Make_index
  (let t : Set := int64 in
  let rpc_arg :=
    let construct := Int64.to_string in
    let destruct (hash_value : string)
      : Pervasives.result int64 string :=
      Result.of_option "Cannot parse pending operation position"
        (Int64.of_string_opt hash_value) in
    RPC_arg.make
      (Some "The position of an operation in a pending operations list")
      "zkru_pending_op_position" destruct construct tt in
  let encoding :=
    Data_encoding.def "zkru_pending_op_position"
      (Some "Zkru pending operation position")
      (Some "The position of an operation in a pending operations list")
      (Data_encoding.Compact.make (Some (Variant.Build "Uint8" unit tt))
        Data_encoding.Compact.int64_value) in
  let compare := Compare.Int64.(Compare.S.compare) in
  let path_length := return? 1 in
  let to_path (c_value : int64) (l_value : list string) : list string :=
    cons (Int64.to_string c_value) l_value in
  let of_path (function_parameter : list string) : M? option int64 :=
    match function_parameter with
    | cons c_value [] => return? Int64.of_string_opt c_value
    | _ => return? None
    end in
  {|
    Storage_description.INDEX.to_path := to_path;
    Storage_description.INDEX.of_path := of_path;
    Storage_description.INDEX.path_length := path_length;
    Storage_description.INDEX.rpc_arg := rpc_arg;
    Storage_description.INDEX.encoding := encoding;
    Storage_description.INDEX.compare := compare
  |})).
END
      },
    ],
  }
]

# In a class to be able to access from StorageTemplate
class Utils; end
def Utils.indent(content)
  content.split("\n").collect {|line| "  " + line}.join("\n") + "\n"
end

def Utils.n_indent(n, content)
  if n == 0 then
    content
  else
    n_indent(n - 1, indent(content))
  end
end

def indexed_data_storage(parens, index, value)
  content = "Storage_sigs.Indexed_data_storage.t\n" +
  "  #{index}\n" +
  "  #{value}"
  return "(%s);" % content if parens
  content + ";"
end

def data_set_storage(parens, index)
  content = "Storage_sigs.Data_set_storage.t\n" +
  "  #{index}"
  return "(%s);" % content if parens
  content + ";"
end

def context_of_json_aux(entry, record_buf, deps_buf, parents)
  if entry[:skip]
    record_buf <<
      "  (* @TODO #{entry[:name]} skipped: #{entry[:skip]} *)\n"
  else
    key = (parents + [entry[:name]]).join("_")
    deps_buf << "#{entry[:header]}\n" if entry.key? :header
    case entry[:type]
    when "Single_data_storage", "Simple_single_data_storage"
      record_buf <<
        "  #{key} :\n" +
        "    Storage_sigs.Single_data_storage.t #{entry[:value]};\n"
    when
      "Indexed_data_storage",
      "Non_iterable_indexed_carbonated_data_storage",
      "Indexed_data_snapshotable_storage",
      "Non_iterable_indexed_data_storage",
      "Indexed_carbonated_data_storage",
      "Indexed_data_storage_with_local_context"
      record_buf << "  #{key} :\n"
      record_buf << Utils.n_indent(
        2,
        indexed_data_storage(false, entry[:index], entry[:value])
      )
    when "Data_set_storage"
      record_buf << "  #{key} :\n"
      record_buf << Utils.n_indent(
        2,
        Utils.indent(data_set_storage(false, entry[:index]))
      )
    when "Custom"
      record_buf << "  #{key} :\n"
      record_buf <<
        Utils.n_indent(3, entry[:simulation_type]).rstrip + ";\n"
    when "Indexed"
      record_buf <<
        "  #{key} :\n" +
        "    Storage_sigs.Indexed_map.t\n" +
        "      #{entry[:index]}\n"
      case entry[:store][:type]
      when
        "Indexed_data_storage",
        "Non_iterable_indexed_carbonated_data_storage",
        "Non_iterable_indexed_data_storage",
        "Indexed_carbonated_data_storage",
        "Indexed_data_snapshotable_storage",
        "Indexed_data_storage_with_local_context"
        record_buf << Utils.n_indent(
          3,
          indexed_data_storage(
            true, entry[:store][:index], entry[:store][:value])
        )
      when "Data_set_storage", "Carbonated_data_set_storage"
        record_buf << Utils.n_indent(
          3,
          data_set_storage(true, entry[:store][:index])
        )
      when "Single_data_storage"
        record_buf << Utils.n_indent(
          3,
          entry[:store][:value] + ";"
        )
      else
        fail "invalid entry type #{entry}"
      end
    when "Sub-store"
      parent = entry[:name]
      entry[:stores].each do |entry|
        context_of_json_aux(
          entry, record_buf, deps_buf, parents + [parent])
      end
    else
      fail "invalid entry type #{entry}"
    end
  end
end

def context_of_json(context)
  rec_buf = StringIO.new
  deps_buf = StringIO.new
  parents = []
  context.each do |entry|
    context_of_json_aux(entry, rec_buf, deps_buf, parents)
  end

  return <<-END.rstrip
#{deps_buf.string}
Record t : Set := {
  config : Raw_context_aux.config;
  standalone : Raw_context_aux.standalone;
#{rec_buf.string}
}.
  END
end

def wrapup_context(middle)
  <<EOF
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_alpha.Simulations.Storage_description.
Require TezosOfOCaml.Proto_alpha.Simulations.Storage_functors.
Require TezosOfOCaml.Proto_alpha.Simulations.Storage_sigs.
Require Export TezosOfOCaml.Proto_alpha.Simulations.Raw_context_aux.
Require TezosOfOCaml.Proto_alpha.Storage.

(** This file is automatically generated, do not edit it by hand *)

(** ** Simulation of the [Context.t] type from the environment

    We define a simulation of the key-value store of Tezos using
    non-serialized types and high-level OCaml data structures such as
    sets and maps. This file could go into the environment folder, but
    it is specific to a protocol version so we keep it there. *)

(** Shorthand for conversion from [Storage_description.INDEX] to
    [Path_encoding.S]. *)
Definition of_description_index {t : Set} :=
  Storage_description.INDEX.to_Path_encoding_S (t := t).

(** Shorthand for conversion from [Storage_functors.INDEX] to
    [Path_encoding.S]. *)
Definition of_functors_index {t : Set} {ipath : Set -> Set} :=
  Storage_functors.INDEX.to_Path_encoding_S (t := t) (ipath := ipath).

(** Shorthand for conversion from [Lazy_storage_kind.ID _] to
    [Path_encoding.S]. *)
Definition of_lazy_storage_kind_id_index {t : Set}
  (r : Lazy_storage_kind.ID (t := t)) :=
  of_functors_index (Storage.Make_index {|
    Storage_description.INDEX.to_path :=
      r.(Lazy_storage_kind.ID.to_path);
    Storage_description.INDEX.of_path :=
      r.(Lazy_storage_kind.ID.of_path);
    Storage_description.INDEX.path_length :=
      r.(Lazy_storage_kind.ID.path_length);
    Storage_description.INDEX.rpc_arg :=
      r.(Lazy_storage_kind.ID.rpc_arg);
    Storage_description.INDEX.encoding :=
      r.(Lazy_storage_kind.ID.encoding);
    Storage_description.INDEX.compare :=
      r.(Lazy_storage_kind.ID.compare)
  |}).

#{middle}
EOF
end

# STORAGE generation

class StorageTemplate
  attr_reader :type, :value, :context, :path, :entry

  def initialize(context, entry)
    @context = context + [entry[:name]]
    @type = entry[:type]
    @value = entry[:value]
    @index = entry[:index]
    @path = normalize_path(entry[:path])
    @entry = entry
  end

  def render
    ERB.new(template_contents, nil, "%<>").result(binding)
  end

  private

  def name
    @context.last
  end

  def index
    return normalized_index(@index) if is_inlined_index?(@index)
    @index
  end

  def subindex
    fail "not an Indexed storage" if type != "Indexed"
    @subindex ||= @entry[:store][:index]
    return normalized_index(@subindex) if is_inlined_index?(@subindex)
    @subindex
  end

  def storage_path
    "Storage.#{@context.join '.'}"
  end

  def subtype
    fail "not an Indexed storage" if type != "Indexed"

    @entry[:store][:type]
  end

  def indent(content)
    Utils.indent(content)
  end

  def n_indent(n, content)
    Utils.n_indent(n, content)
  end

  def context_key
    @context.join("_")
  end

  def set_definition
    <<END
Definition set (ctxt : Raw_context.t) value :=
  let ctxt' := ctxt <| Raw_context.#{context_key} := value |> in
  (Raw_context.to_t ctxt').
Arguments set _ _ /.
END
  end

  def get_definition
    <<END
Definition get (ctxt : Raw_context.t) :=
  ctxt
    .(Raw_context.#{context_key}).
Arguments get _ /.
END
  end

  def is_inlined_index?(index)
    index =~ /\(of_\w+_index \w+\)/
  end

  def normalized_index(index)
    index.gsub(/\(of_(\w+)_index (.*)\)/,
               "(of_\\1_index Raw_context.\\2)")
  end

  def normalize_path(path)
    path&.map {|s| '"%s"' % s}&.join(";")
  end

  def template_path
    case type
    when "Custom"
      File.expand_path(__dir__) +
        "/templates/storage/Custom/#{entry[:template]}.erb"
    when "Indexed"
      File.expand_path(__dir__) +
        "/templates/storage/Indexed/#{subtype}.erb"
    else
      File.expand_path(__dir__) +
        "/templates/storage/#{type}.erb"
    end
  end

  def template_contents
    File.read(template_path)
  end
end

def storage_entries(entries, parents: [], &block)
  entries.map do |entry|
    if entry[:skip]
      "(* @TODO Skipped #{entry[:name]} : #{entry[:skip]} *)\n"
    else
      case entry[:type]
      when "Sub-store"
        mods = Utils.indent(
          storage_entries(
            entry[:stores],
            parents: parents + [entry[:name]], &block)).rstrip
        <<END
Module #{entry[:name]}.
#{mods}
End #{entry[:name]}.
END
      else
        StorageTemplate.new(parents, entry).render
      end
    end
  # rescue StandardError => e
  #     STDERR.puts("ERROR: #{e}")
  end.join("\n")
end

def storage_wrapup(middle)
  <<END
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

(** This file is automatically generated, do not edit it by hand *)

Require Import TezosOfOCaml.Environment.V7.
Require TezosOfOCaml.Proto_alpha.Storage.

Require TezosOfOCaml.Proto_alpha.Simulations.Raw_context.

(* abreviations to avoid implicits inference errors *)
Notation of_functors_index := Raw_context.of_functors_index.
Notation of_description_index := Raw_context.of_description_index.
Notation of_lazy_storage_kind_id_index :=
  Raw_context.of_lazy_storage_kind_id_index.

#{middle}
END

end

def storage_axioms_content(context)
  content = storage_entries(context)
  storage_wrapup(content)
end

def parse_options
  options = {
  }
  OptionParser.new do |opts|
    opts.banner = <<~EOS
        Usage: ruby generate_context.rb [options]

        Generate the specification of storage from a json"
    EOS
    opts.on("-s", "--storage STORAGE", "The storage output path") \
    do |opt|
      options[:storage_path] = opt
    end
    opts.on("-c",
            "--context CONTEXT", "The context output path) do |opt|") \
    do |opt|
      options[:context_path] = opt
    end
  end.parse!
  options
end

def cleanup_whitespace(contents)
  contents
    .gsub(/\t/, "  ")
    .gsub(/\s+$/, "\n")
    .rstrip
end

def write_file(path, contents)
  File.write(path, cleanup_whitespace(contents))
  puts "#{path} written"
end

def context_iter(context, parents: [], &block)
  context.each do |entry|
    case entry[:type]
    when "Sub-store"
      context_iter(
        entry[:stores], parents: parents +
                        [entry[:name]], &block)
    else
      block.call(entry, parents)
    end
  end
end

def show_status(context)
  i = 0
  skipped = 0
  context_iter(context) do |entry, parents|
    i += 1
    if entry[:skip]
      if entry[:type] == "Sub-store"
        skipped += entry[:stores].length
      else
        skipped += 1
        puts "Skipped: #{parents.join('.')}.#{entry[:name]} " +
          "#{entry[:skip]}"
      end
    end
  end

  puts ("Status: %d/%d, %d%% complete" %
        [i - skipped, i, 100 - (skipped * 100) / i.to_f])
end

def main(context)
  opts = parse_options
  if opts[:context_path]
    context_content = wrapup_context(context_of_json(context))
    write_file(opts[:context_path], context_content)
  end
  if opts[:storage_path]
    write_file(opts[:storage_path], storage_axioms_content(context))
  end
  show_status(context)
end

main(context)
